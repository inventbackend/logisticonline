
package modelKojaConfirmTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:BILLING_ConfirmTransactionResponse")
    @Expose
    private Ns1BILLINGConfirmTransactionResponse ns1BILLINGConfirmTransactionResponse;

    public Ns1BILLINGConfirmTransactionResponse getNs1BILLINGConfirmTransactionResponse() {
        return ns1BILLINGConfirmTransactionResponse;
    }

    public void setNs1BILLINGConfirmTransactionResponse(Ns1BILLINGConfirmTransactionResponse ns1BILLINGConfirmTransactionResponse) {
        this.ns1BILLINGConfirmTransactionResponse = ns1BILLINGConfirmTransactionResponse;
    }

}
