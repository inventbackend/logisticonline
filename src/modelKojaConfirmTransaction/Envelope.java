
package modelKojaConfirmTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Envelope {

    @SerializedName("SOAP-ENV:Envelope")
    @Expose
    private SOAPENVEnvelope sOAPENVEnvelope;

    public SOAPENVEnvelope getSOAPENVEnvelope() {
        return sOAPENVEnvelope;
    }

    public void setSOAPENVEnvelope(SOAPENVEnvelope sOAPENVEnvelope) {
        this.sOAPENVEnvelope = sOAPENVEnvelope;
    }

}
