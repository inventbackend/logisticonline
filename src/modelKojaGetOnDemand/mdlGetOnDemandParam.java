package modelKojaGetOnDemand;

public class mdlGetOnDemandParam {

	public String orderID;
	public String documentNo;
	public String peb;
	public String documentDate;
	public String npwp;
	public String STATUS;
	public String MESSAGE;
	public Integer exportLimit;
	
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getPeb() {
		return peb;
	}
	public void setPeb(String peb) {
		this.peb = peb;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public Integer getExportLimit() {
		return exportLimit;
	}
	public void setExportLimit(Integer exportLimit) {
		this.exportLimit = exportLimit;
	}
	
}
