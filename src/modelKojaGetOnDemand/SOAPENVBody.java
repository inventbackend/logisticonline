
package modelKojaGetOnDemand;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:CUSTOMS_GetDocumentTPSOnlineResponse")
    @Expose
    private Ns1CUSTOMSGetDocumentTPSOnlineResponse ns1CUSTOMSGetDocumentTPSOnlineResponse;

    public Ns1CUSTOMSGetDocumentTPSOnlineResponse getNs1CUSTOMSGetDocumentTPSOnlineResponse() {
        return ns1CUSTOMSGetDocumentTPSOnlineResponse;
    }

    public void setNs1CUSTOMSGetDocumentTPSOnlineResponse(Ns1CUSTOMSGetDocumentTPSOnlineResponse ns1CUSTOMSGetDocumentTPSOnlineResponse) {
        this.ns1CUSTOMSGetDocumentTPSOnlineResponse = ns1CUSTOMSGetDocumentTPSOnlineResponse;
    }

}
