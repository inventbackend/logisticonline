package modelKojaGetCms;

import java.util.List;

public class mdlGetCms {

	public String STATUS;
	public List<String> CMS_NO;
	public List<String> PRINT_CMS_DATETIME;
	public List<String> LS_LEVEL_1;
	public List<String> LS_LEVEL_2;
	public List<String> LS_LEVEL_3;
	public List<String> LS_LEVEL_4;
	public List<String> CUR_OWNER;
	public List<String> CNTR_STATUS;
	public List<String> CNTR_SIZE;
	public List<String> CNTR_TYPE;
	public List<String> GROSS_WEIGHT_DOCUMENTED;
	public String LINK;
	
	public String cmsNo;
	public String cmsLink;
	public String printCmsDateTime;
	public String locationStuffing;
	public String curOwner;
	public String containerStatus;
	public String containerSize;
	public String containerType;
	public Integer grossWeightDocumented;
	public String vehicleNumber;
	public String tidNumber;
	public String containerNumber;
	public String vendorOrderDetailID;
}
