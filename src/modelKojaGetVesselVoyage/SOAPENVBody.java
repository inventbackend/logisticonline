
package modelKojaGetVesselVoyage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:MAIN_GetVesselVoyageResponse")
    @Expose
    private Ns1MAINGetVesselVoyageResponse ns1MAINGetVesselVoyageResponse;

    public Ns1MAINGetVesselVoyageResponse getNs1MAINGetVesselVoyageResponse() {
        return ns1MAINGetVesselVoyageResponse;
    }

    public void setNs1MAINGetVesselVoyageResponse(Ns1MAINGetVesselVoyageResponse ns1MAINGetVesselVoyageResponse) {
        this.ns1MAINGetVesselVoyageResponse = ns1MAINGetVesselVoyageResponse;
    }

}
