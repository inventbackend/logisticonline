package modelKojaGetVesselVoyage;

public class mdlKojaVesselVoyage {

	public String STATUS;
	public String MESSAGE;
	public String[] VOYID_COMPANY_CODE;
	public String[] VOYID_VESSEL_CODE;
	public String[] VOYID_VOYAGE_CODE;
	public String[] VOYAGE_STATUS;
	public String[] ARRIVAL_DATETIME;
	public String[] DEPARTURE_DATETIME;
	public String[] VESSEL_NAME;
	public String[] POD;
	public String[] POD_NAME;
	public Integer[] EXPORT_LIMIT;
	public String TerminalID;
	public String[] DOCUMENT_CLOSING_DATETIME;
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String[] getVOYID_COMPANY_CODE() {
		return VOYID_COMPANY_CODE;
	}
	public void setVOYID_COMPANY_CODE(String[] vOYID_COMPANY_CODE) {
		VOYID_COMPANY_CODE = vOYID_COMPANY_CODE;
	}
	public String[] getVOYID_VESSEL_CODE() {
		return VOYID_VESSEL_CODE;
	}
	public void setVOYID_VESSEL_CODE(String[] vOYID_VESSEL_CODE) {
		VOYID_VESSEL_CODE = vOYID_VESSEL_CODE;
	}
	public String[] getVOYID_VOYAGE_CODE() {
		return VOYID_VOYAGE_CODE;
	}
	public void setVOYID_VOYAGE_CODE(String[] vOYID_VOYAGE_CODE) {
		VOYID_VOYAGE_CODE = vOYID_VOYAGE_CODE;
	}
	public String[] getVOYAGE_STATUS() {
		return VOYAGE_STATUS;
	}
	public void setVOYAGE_STATUS(String[] vOYAGE_STATUS) {
		VOYAGE_STATUS = vOYAGE_STATUS;
	}
	public String[] getARRIVAL_DATETIME() {
		return ARRIVAL_DATETIME;
	}
	public void setARRIVAL_DATETIME(String[] aRRIVAL_DATETIME) {
		ARRIVAL_DATETIME = aRRIVAL_DATETIME;
	}
	public String[] getDEPARTURE_DATETIME() {
		return DEPARTURE_DATETIME;
	}
	public void setDEPARTURE_DATETIME(String[] dEPARTURE_DATETIME) {
		DEPARTURE_DATETIME = dEPARTURE_DATETIME;
	}
	public String[] getVESSEL_NAME() {
		return VESSEL_NAME;
	}
	public void setVESSEL_NAME(String[] vESSEL_NAME) {
		VESSEL_NAME = vESSEL_NAME;
	}
	public String[] getPOD() {
		return POD;
	}
	public void setPOD(String[] pOD) {
		POD = pOD;
	}
	public String[] getPOD_NAME() {
		return POD_NAME;
	}
	public void setPOD_NAME(String[] pOD_NAME) {
		POD_NAME = pOD_NAME;
	}
	public String getTerminalID() {
		return TerminalID;
	}
	public void setTerminalID(String terminalID) {
		TerminalID = terminalID;
	}
	public Integer[] getEXPORT_LIMIT() {
		return EXPORT_LIMIT;
	}
	public void setEXPORT_LIMIT(Integer[] eXPORT_LIMIT) {
		EXPORT_LIMIT = eXPORT_LIMIT;
	}
	public String[] getDOCUMENT_CLOSING_DATETIME() {
		return DOCUMENT_CLOSING_DATETIME;
	}
	public void setDOCUMENT_CLOSING_DATETIME(String[] dOCUMENT_CLOSING_DATETIME) {
		DOCUMENT_CLOSING_DATETIME = dOCUMENT_CLOSING_DATETIME;
	}
	
}
