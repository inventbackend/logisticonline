package modelKojaAssociateContainer;

public class mdlAssociatedData {

	public Boolean STATUS;
	public String MESSAGE;
	public String TID;
	public String TIX;
	public String CONTAINER;
	public String CONTAINER_STATUS;
	public String VESSEL_CODE;
	public String VOYAGE_CODE;
	public String VALID_THRU;
	public String POD1;
	public String POD2;
	public String PHONE;
	public String CONTACT;
	public String ISO;
	public Integer WEIGHT;
	public String CUSTOM_DOC;
	public String CUSTOMER;
	public String ETB;
	public String IMO_CODE;
	public String TRANSACTION_TYPE;
	public String BAT_DATE;
	public String BAT;
	public String INV_NBR;
	public String OWNER;
	
	public Boolean getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(Boolean sTATUS) {
		STATUS = sTATUS;
	}
	public String getTID() {
		return TID;
	}
	public void setTID(String tID) {
		TID = tID;
	}
	public String getCONTAINER() {
		return CONTAINER;
	}
	public void setCONTAINER(String cONTAINER) {
		CONTAINER = cONTAINER;
	}
	public String getCONTAINER_STATUS() {
		return CONTAINER_STATUS;
	}
	public void setCONTAINER_STATUS(String cONTAINER_STATUS) {
		CONTAINER_STATUS = cONTAINER_STATUS;
	}
	public String getVESSEL_CODE() {
		return VESSEL_CODE;
	}
	public void setVESSEL_CODE(String vESSEL_CODE) {
		VESSEL_CODE = vESSEL_CODE;
	}
	public String getVOYAGE_CODE() {
		return VOYAGE_CODE;
	}
	public void setVOYAGE_CODE(String vOYAGE_CODE) {
		VOYAGE_CODE = vOYAGE_CODE;
	}
	public String getVALID_THRU() {
		return VALID_THRU;
	}
	public void setVALID_THRU(String vALID_THRU) {
		VALID_THRU = vALID_THRU;
	}
	public String getPOD1() {
		return POD1;
	}
	public void setPOD1(String pOD1) {
		POD1 = pOD1;
	}
	public String getPOD2() {
		return POD2;
	}
	public void setPOD2(String pOD2) {
		POD2 = pOD2;
	}
	public String getCONTACT() {
		return CONTACT;
	}
	public void setCONTACT(String cONTACT) {
		CONTACT = cONTACT;
	}
	public String getISO() {
		return ISO;
	}
	public void setISO(String iSO) {
		ISO = iSO;
	}
	public Integer getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(Integer wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getCUSTOM_DOC() {
		return CUSTOM_DOC;
	}
	public void setCUSTOM_DOC(String cUSTOM_DOC) {
		CUSTOM_DOC = cUSTOM_DOC;
	}
	public String getCUSTOMER() {
		return CUSTOMER;
	}
	public void setCUSTOMER(String cUSTOMER) {
		CUSTOMER = cUSTOMER;
	}
	public String getETB() {
		return ETB;
	}
	public void setETB(String eTB) {
		ETB = eTB;
	}
	public String getIMO_CODE() {
		return IMO_CODE;
	}
	public void setIMO_CODE(String iMO_CODE) {
		IMO_CODE = iMO_CODE;
	}
	public String getTRANSACTION_TYPE() {
		return TRANSACTION_TYPE;
	}
	public void setTRANSACTION_TYPE(String tRANSACTION_TYPE) {
		TRANSACTION_TYPE = tRANSACTION_TYPE;
	}
	public String getBAT_DATE() {
		return BAT_DATE;
	}
	public void setBAT_DATE(String bAT_DATE) {
		BAT_DATE = bAT_DATE;
	}
	public String getBAT() {
		return BAT;
	}
	public void setBAT(String bAT) {
		BAT = bAT;
	}
	public String getINV_NBR() {
		return INV_NBR;
	}
	public void setINV_NBR(String iNV_NBR) {
		INV_NBR = iNV_NBR;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String getTIX() {
		return TIX;
	}
	public void setTIX(String tIX) {
		TIX = tIX;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}
	public String getOWNER() {
		return OWNER;
	}
	public void setOWNER(String oWNER) {
		OWNER = oWNER;
	}
	
}
