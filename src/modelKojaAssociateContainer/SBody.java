
package modelKojaAssociateContainer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

    @SerializedName("ns2:associateResponse")
    @Expose
    private Ns2AssociateResponse ns2AssociateResponse;

    public Ns2AssociateResponse getNs2AssociateResponse() {
        return ns2AssociateResponse;
    }

    public void setNs2AssociateResponse(Ns2AssociateResponse ns2AssociateResponse) {
        this.ns2AssociateResponse = ns2AssociateResponse;
    }

}
