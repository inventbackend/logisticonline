package modelKojaAssociateContainer;

import java.util.List;

public class mdlData {

	public String MESSAGE;
	public List<mdlAssociatedData> ASSOCIATE_DATA;
	
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public List<mdlAssociatedData> getASSOCIATED_DATA() {
		return ASSOCIATE_DATA;
	}
	public void setASSOCIATED_DATA(List<mdlAssociatedData> aSSOCIATED_DATA) {
		ASSOCIATE_DATA = aSSOCIATED_DATA;
	}
	
}
