package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlContainerType;

public class ContainerTypeAdapter {

	public static List<model.mdlContainerType> LoadContainerType (String user) {
		List<model.mdlContainerType> listContainerType = new ArrayList<model.mdlContainerType>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		
		try {
			sql = "{call sp_LoadContainerType()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadContainerType", user);

			while(jrs.next()){
				model.mdlContainerType containerType = new model.mdlContainerType();
				containerType.setContainerTypeID(jrs.getString("ContainerTypeID"));
				containerType.setDescription(jrs.getString("Description"));
				containerType.setIso(jrs.getString("IsoCode"));
				containerType.setTare(jrs.getInt("TareWeight"));
				listContainerType.add(containerType);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadContainerType", sql , user);
		}

		return listContainerType;
	}

	public static model.mdlContainerType LoadContainerTypeByID (String lContainerTypeID, String user) {
		model.mdlContainerType mdlContainerType = new model.mdlContainerType();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadContainerTypeByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lContainerTypeID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadContainerTypeByID", user);

			while(jrs.next()){
				mdlContainerType.setContainerTypeID(jrs.getString("ContainerTypeID"));
				mdlContainerType.setDescription(jrs.getString("Description"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadContainerTypeByID", sql , user);
		}

		return mdlContainerType;
	}

	public static String InsertContainerType(mdlContainerType mdlContainerType, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String lResult = "";

		try{
			mdlContainerType CheckDuplicateContainerType = LoadContainerTypeByID(mdlContainerType.getContainerTypeID(), user);
			if(CheckDuplicateContainerType.getContainerTypeID() == null){
				String newContainerTypeID = CreateContainerTypeID(user);
				sql = "{call sp_InsertContainerType2(?,?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newContainerTypeID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlContainerType.getDescription()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlContainerType.getIso()));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlContainerType.getTare()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertContainerType", user);

				lResult = success == true ? "Success Insert Container Type" : "Database Error";
			}
			//if duplicate
			else{
				lResult = "Container Type sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertContainerType", sql , user);
			lResult = "Database Error";
		}

		return lResult;
	}

	public static String UpdateContainerType(mdlContainerType mdlContainerType, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String lResult = "";

		try{
			sql = "{call sp_UpdateContainerType2(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlContainerType.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlContainerType.getDescription()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlContainerType.getIso()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlContainerType.getTare()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateContainerType", user);

			lResult = success == true ? "Success Update Container Type" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateContainerType", sql , user);
			lResult = "Database Error";
		}

		return lResult;
	}

	public static String DeleteContainerType(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String lResult = "";

		try{
			sql = "{call sp_DeleteContainerType(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteContainerType", user);

			lResult = success == true ? "Success Delete Container Type" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteContainerType", sql , user);
			lResult = "Database Error";
		}

		return lResult;
	}

	public static String CreateContainerTypeID(String user){
		String lastContainerTypeID = GetLastContainerTypeID(user);
		String stringIncrement = "0001";

		if ((lastContainerTypeID != null) && (!lastContainerTypeID.equals("")) ){
			String[] partsContainerTypeID = lastContainerTypeID.split("-");
			//get last running number
			String partNumber = partsContainerTypeID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("CNTP-").append(stringIncrement);
		String ContainerTypeID = sb.toString();
		return ContainerTypeID;
	}

	public static String GetLastContainerTypeID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String ContainerTypeID = "";

		try {
			sql = "{call sp_GetLastContainerTypeID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastContainerTypeID", user);

			while(jrs.next()){
				ContainerTypeID = jrs.getString("ContainerTypeID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastContainerTypeID", sql , user);
		}

		return ContainerTypeID;
	}
}
