package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlTier;

public class TierAdapter {
	public static List<model.mdlTier> LoadTier (String user) {
		List<model.mdlTier> listTier = new ArrayList<model.mdlTier>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadTier()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadTier", user);

			while(jrs.next()){
				model.mdlTier tier = new model.mdlTier();
				tier.setTierID(jrs.getString("TierID"));
				tier.setDescription(jrs.getString("Description"));
				tier.setLongDescription(jrs.getString("LongDescription"));
				listTier.add(tier);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTier", sql , user);
		}

		return listTier;
	}

	public static model.mdlTier LoadTierByID (String lTierID, String user) {
		model.mdlTier mdlTier = new model.mdlTier();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadTierByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lTierID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTierByID", user);

			while(jrs.next()){
				mdlTier.setTierID(jrs.getString("TierID"));
				mdlTier.setDescription(jrs.getString("Description"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTierByID", sql , user);
		}

		return mdlTier;
	}

	public static String InsertTier(mdlTier mdlTier, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlTier CheckDuplicateTier = LoadTierByID(mdlTier.getTierID(),user);
			if(CheckDuplicateTier.getTierID() == null){
				String newTierID = CreateTierID(user);
				sql = "{call sp_InsertTier(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newTierID) );
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTier.getDescription() ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTier.getLongDescription() ));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertTier",user);

				result = success == true ? "Success Insert Tier" : "Database Error";
			}
			//if duplicate
			else{
				result = "Tier sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertTier", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateTier(mdlTier mdlTier, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateTier(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTier.getTierID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTier.getDescription() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTier.getLongDescription() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateTier", user);

			result = success == true ? "Success Update Tier" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTier", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteTier(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteTier(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteTier", user);

			result = success == true ? "Success Delete Tier" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTier", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateTierID(String user){
		String lastTierID = GetLastTierID(user);
		String stringIncrement = "0001";

		if ((lastTierID != null) && (!lastTierID.equals("")) ){
			String[] partsTierID = lastTierID.split("-");
			//get last running number
			String partNumber = partsTierID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("TIER-").append(stringIncrement);
		String TierID = sb.toString();
		return TierID;
	}

	public static String GetLastTierID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String TierID = "";

		try {
			sql = "{call sp_GetLastTierID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastTierID", user);

			while(jrs.next()){
				TierID = jrs.getString("TierID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastTierID", sql , user);
		}

		return TierID;
	}
}
