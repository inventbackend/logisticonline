package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlQueryExecute;
import model.mdlVendorOrderDetail;
import modelKojaAssociateContainer.mdlAssociatedData;
import modelKojaContainerTracking.mdlContainerData;

public class VendorOrderDetailAdapter {

	public static List<model.mdlVendorOrderDetail> LoadVendorOrderDetailByID(String vendorOrderID, String user) {
		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadVendorOrderDetailByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDetailByID", user);

			while(jrs.next()){
				model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
				mdlVendorOrderDetail.setVendorOrderDetailID(jrs.getString("VendorOrderDetailID"));
				mdlVendorOrderDetail.setVendorOrderID(jrs.getString("VendorOrderID"));
				mdlVendorOrderDetail.setDriverID(jrs.getString("DriverID"));
				mdlVendorOrderDetail.setDriverName(jrs.getString(4));
				mdlVendorOrderDetail.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlVendorOrderDetail.setVendorDetailStatus(jrs.getInt("VendorDetailStatus"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlVendorOrderDetail.setSealNumber(jrs.getString("SealNumber"));
				mdlVendorOrderDetail.setCreatedBy(jrs.getString("CreatedBy"));
				mdlVendorOrderDetail.setUpdatedBy(jrs.getString("UpdatedBy"));
				mdlVendorOrderDetail.setCancellationReason(jrs.getString("CancelReason"));
				mdlVendorOrderDetail.setCustomer(jrs.getString(12));
				mdlVendorOrderDetail.setPic(jrs.getString("PIC"));
				mdlVendorOrderDetail.setAddress(jrs.getString(14));
				mdlVendorOrderDetail.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlVendorOrderDetail.setShippingInstruction(jrs.getString("ShippingInvoiceID"));
				mdlVendorOrderDetail.setTotalParty(jrs.getInt("TotalQuantity"));
				mdlVendorOrderDetail.setVesselVoyage(jrs.getString("VesselName") + " / " + jrs.getString("VoyageNo"));
				mdlVendorOrderDetail.setStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlVendorOrderDetail.setPort(jrs.getString("PortName") + " - " + jrs.getString("PortUTC"));
				mdlVendorOrderDetail.setCommodity(jrs.getString("ItemDescription"));
				mdlVendorOrderDetail.setTimeInDepo(ConvertDateTimeHelper.formatDate(jrs.getString("DepoIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setTimeOutDepo(ConvertDateTimeHelper.formatDate(jrs.getString("DepoOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setTimeInFactory(ConvertDateTimeHelper.formatDate(jrs.getString("FactoryIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setTimeOutFactory(ConvertDateTimeHelper.formatDate(jrs.getString("FactoryOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setTimeInPort(ConvertDateTimeHelper.formatDate(jrs.getString("PortIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setTimeOutPort(ConvertDateTimeHelper.formatDate(jrs.getString("PortOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlVendorOrderDetail.setYellowcard(jrs.getString("YellowCard"));
				listVendorOrderDetail.add(mdlVendorOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByID", sql , user);
		}
		return listVendorOrderDetail;
	}
	
	public static List<model.mdlVendorOrderDetail> LoadNonCancelVendorOrderDetailByID(String vendorOrderID, String user) {
		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadNonCancelByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadNonCancelVendorOrderDetailByID", user);

			while(jrs.next()){
				model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
				mdlVendorOrderDetail.setVendorOrderDetailID(jrs.getString("VendorOrderDetailID"));
				mdlVendorOrderDetail.setVendorOrderID(jrs.getString("VendorOrderID"));
				mdlVendorOrderDetail.setDriverID(jrs.getString("DriverID"));
				mdlVendorOrderDetail.setDriverName(jrs.getString(4));
				mdlVendorOrderDetail.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlVendorOrderDetail.setVendorDetailStatus(jrs.getInt("VendorDetailStatus"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlVendorOrderDetail.setSealNumber(jrs.getString("SealNumber"));
				mdlVendorOrderDetail.setCreatedBy(jrs.getString("CreatedBy"));
				mdlVendorOrderDetail.setUpdatedBy(jrs.getString("UpdatedBy"));
				listVendorOrderDetail.add(mdlVendorOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNonCancelVendorOrderDetailByID", sql , user);
		}
		return listVendorOrderDetail;
	}

	public static String InsertVendorOrderDetail(model.mdlVendorOrderDetail lParam, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		String result = "";
		CachedRowSet rowset = null;
		Integer driverRegistered = 0; //driver registered on vendor order
		String driverAvailability = "";
		
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction;
		
		try{
			//cek apakah driver/no kendaraan yang diassign sedang dalam proses pengantaran atau tidak
			sql = "{call sp_VendorOrderDetailCheckAvailableDriver2(?,?)}";
			/*listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorOrderID() ));*/
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getDriverID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVehicleNumber() ));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckDriverAvailability", user);
			while(rowset.next()){
				if(rowset.getInt(1) == 0)
					driverAvailability = "Yes";
				else
					driverAvailability = "No";
			}
			
			if(driverAvailability.equals("Yes")){
				//get the registered driver count by vendor order
				listParam = new ArrayList<model.mdlQueryExecute>();
				rowset = null;
				sql = "{call sp_GetRegisteredDriverByVendorOrder(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorOrderID() ));
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetRegisteredDriverByVendorOrder", user);
				//Globals.gCommand = sql;
				while(rowset.next()){
					driverRegistered = rowset.getInt(1) + 1;
				}
				
				//insert vendor order detail process
					//store 'InsertVendorOrderDetail' parameter for transaction
					String newVendorOrderDetailID = CreateVendorOrderDetailID(user);
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_InsertVendorOrderDetail(?,?,?,?,?, ?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", newVendorOrderDetailID ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorOrderID() ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getDriverID() ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVehicleNumber() ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getContainerNumber() ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getSealNumber() ));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getCreatedBy() ));
					//Globals.gCommand = mdlQueryTransaction.sql;
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				
				//check if all driver for vendor order has been assigned or not
					//if has been fully assigned
					if(driverRegistered == lParam.getDriverTargetAmount()){
						//store 'UpdateVendorOrderStatus' parameter for transaction
						mdlQueryTransaction = new model.mdlQueryTransaction();
						mdlQueryTransaction.sql = "{call sp_UpdateVendorOrderStatus(?,?)}";
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorOrderID() ));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 1 ));
						//Globals.gCommand = mdlQueryTransaction.sql;
						sql = mdlQueryTransaction.sql;
						listmdlQueryTransaction.add(mdlQueryTransaction);
						
						//check if all driver for customer order detail has been assigned or not
						Integer custOrderDetailQty = 0; //customer order detail target qty
						Integer driverRegisteredOnCustomerOrderDetail = 0; //driver registered on customer order detail
							//get the target quantity of customer order detail
							listParam = new ArrayList<model.mdlQueryExecute>();
							rowset = null;
							sql = "{call sp_LoadOrderManagementDetailByDetailID(?)}";
							listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderDetailID() ));
							rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetCustomerOrderDetailTargetQty", user);
							//Globals.gCommand = sql;
							while(rowset.next()){
								custOrderDetailQty = rowset.getInt("Quantity");
							}
							
							//get the registered driver count by customer order detail
							listParam = new ArrayList<model.mdlQueryExecute>();
							rowset = null;
							sql = "{call sp_GetRegisteredDriverByCustomerOrderDetail(?)}";
							listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderDetailID() ));
							rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetRegisteredDriverByCustomerOrderDetail", user);
							//Globals.gCommand = sql;
							while(rowset.next()){
								driverRegisteredOnCustomerOrderDetail = rowset.getInt(1) + 1;
							}
							
							//checking process
							//if driver for customer order detail has been fully assigned
							if(driverRegisteredOnCustomerOrderDetail == custOrderDetailQty){
								//store 'UpdateCustomerOrderDetailStatus' parameter for transaction
								mdlQueryTransaction = new model.mdlQueryTransaction();
								mdlQueryTransaction.sql = "{call sp_UpdateOrderDetailStatusByID(?,?,?,?)}";
								mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderDetailID() ));
								mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 3 ));
								mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
								mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
								//Globals.gCommand = mdlQueryTransaction.sql;
								listmdlQueryTransaction.add(mdlQueryTransaction);
								
								//check if all driver for customer order header has been assigned or not
								Integer custOrderHeaderQty = 0; //customer order header target qty
								Integer driverRegisteredOnCustomerOrderHeader = 0; //driver registered on customer order header
									//get the target quantity of customer order header
									listParam = new ArrayList<model.mdlQueryExecute>();
									rowset = null;
									sql = "{call sp_LoadOrderManagementByID(?)}";
									listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderID() ));
									rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetCustomerOrderHeaderTargetQty", user);
									//Globals.gCommand = sql;
									while(rowset.next()){
										custOrderHeaderQty = rowset.getInt("TotalQuantity");
									}
									
									//get the registered driver count by customer order header
									listParam = new ArrayList<model.mdlQueryExecute>();
									rowset = null;
									sql = "{call sp_GetRegisteredDriverByCustomerOrderHeader(?)}";
									listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderID() ));
									rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetRegisteredDriverByCustomerOrderHeader", user);
									//Globals.gCommand = sql;
									while(rowset.next()){
										driverRegisteredOnCustomerOrderHeader = rowset.getInt(1) + 1;
									}
									
									//checking process
									//if driver for customer order header has been fully assigned
									if(driverRegisteredOnCustomerOrderHeader == custOrderHeaderQty){
										//store 'UpdateCustomerOrderHeaderStatus' parameter for transaction
										mdlQueryTransaction = new model.mdlQueryTransaction();
										mdlQueryTransaction.sql = "{call sp_UpdateOrderStatus(?,?)}";
										mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderID() ));
										mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 3 ));
										//Globals.gCommand = mdlQueryTransaction.sql;
										listmdlQueryTransaction.add(mdlQueryTransaction);
									}
									//if driver for customer order header has not been fully assigned, nothing's happened
							}
							//if driver for customer order detail has not been fully assigned, nothing's happened
					}
					//if driver for vendor order has not been fully assigned, nothing's happened
				
				success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertVendorOrderDetail", user);
				result = success == true ? "Success Insert Vendor Order Detail" : "Database Error";
			}
			else{
				result = "Driver/Vehicle is not available because it may be in assignment progress";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendorOrderDetail", sql , user);
			result = "Database Error";
		}
		return result;
	}

	public static String Update(model.mdlVendorOrderDetail mdlVendorOrderDetail, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			sql = "{call sp_UpdateVendorOrderDetail(?,?,?,?,?, ?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getVendorOrderDetailID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getVendorOrderID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getDriverID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getVehicleNumber() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getContainerNumber() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getSealNumber() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getUpdatedBy() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorOrderDetail", user);

			result = success == true ? "Success Update Vendor Order Detail" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorOrderDetail", sql , user);
			result = "Database Error";
		}
		return result;
	}
	
	public static String UpdateVendorOrderDetail(model.mdlVendorOrderDetail mdlVendorOrderDetail, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String result = "";
		String sql = "";
		CachedRowSet rowset = null;
		CachedRowSet rowset2 = null;
		String driverAvailability = "Yes";
		String vehicleAvailability = "Yes";
		try{
			String lastDriver = VendorOrderDetailAdapter.LoadVendorOrderDetailByDetailID(mdlVendorOrderDetail.vendorOrderDetailID, user).driverID;
			String lastVehicle = VendorOrderDetailAdapter.LoadVendorOrderDetailByDetailID(mdlVendorOrderDetail.vendorOrderDetailID, user).vehicleNumber;
			
			if(!lastDriver.equals(mdlVendorOrderDetail.getDriverID())){
				//cek apakah driver yang diassign sedang dalam proses pengantaran atau tidak
				sql = "{call sp_VendorOrderDetailCheckAvailableDriver(?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getVendorOrderID() ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getDriverID() ));
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckDriverAvailability", user);
				while(rowset.next()){
					if(rowset.getInt(1) == 0)
						driverAvailability = "Yes";
					else
						driverAvailability = "No";
				}
			}
			
			if(!lastVehicle.equals(mdlVendorOrderDetail.getVehicleNumber())){
				listParam = new ArrayList<model.mdlQueryExecute>();
				//cek apakah vehicle yang diassign sedang dalam proses pengantaran atau tidak
				sql = "{call sp_VendorOrderDetailCheckAvailableVehicle(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlVendorOrderDetail.getVehicleNumber() ));
				rowset2 = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckVehicleAvailability", user);
				while(rowset2.next()){
					if(rowset2.getInt(1) == 0){
						vehicleAvailability = "Yes";
						
						//cek sudah pernah associate atau belum
						String gatePass = LoadVendorOrderDetailByDetailID(mdlVendorOrderDetail.getVendorOrderDetailID(), user).yellowcard;
						if(!gatePass.equals("")){
							//kalau sudah pernah, simpan old vehicle number untuk proses reassociate nanti
							UpdateOldVehicleNumber(mdlVendorOrderDetail.getVendorOrderDetailID(), lastVehicle, user);
						}
					}
					else
						vehicleAvailability = "No";
				}
			}
			
			if(driverAvailability.equals("Yes") && vehicleAvailability.equals("Yes")){
				if(!lastDriver.equals(mdlVendorOrderDetail.getDriverID())){
					PushNotifAdapter.PushNotification("assign", mdlVendorOrderDetail.getDriverID(), user);
				}
				result = Update(mdlVendorOrderDetail, user);
				if(result.equals("Success Update Vendor Order Detail")){
					if(!mdlVendorOrderDetail.getContainerNumber().equals(""))
						PushNotifAdapter.PushNotification("containernumber", mdlVendorOrderDetail.getDriverID(), user);
				}
			}
			else
				result = "Driver/Vehicle is not available because it may be in assignment progress";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorOrderDetail", sql , user);
			result = "Database Error";
		}
		return result;
	}

	public static String DeleteVendorOrderDetail(String vendorOrderID, String vendorOrderDetailID, String driverID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		Boolean allowDelete = AllowDeleteVendorOrderDetail(vendorOrderID, user);

		if (allowDelete){
			try{
				sql = "{call sp_DeleteVendorOrderDetail(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteVendorOrderDetail", user);

				result = success == true ? "Success Delete Vendor Order Detail" : "Database Error";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteVendorOrderDetail", sql , user);
				result = "Database Error";
			}
		}else{
			result = "Can only delete vendor order detail with vendor order status \"Picked\"";
		}
		return result;
	}

	public static Boolean AllowDeleteVendorOrderDetail(String vendorOrderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean allow = false;
		CachedRowSet jrs = null;
		Integer orderStatus = null;

		try{
			sql = "{call sp_AllowDeleteVendorOrderDetail(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "AllowDeleteVendorOrderDetail", user);
			while(jrs.next()){
				orderStatus = jrs.getInt("VendorStatus");
			}
			if (orderStatus == 0){
				allow = true;
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "AllowDeleteVendorOrderDetail", sql , user);
		}

		return allow;
	}

	public static String CreateVendorOrderDetailID(String user){
		String lastVendorOrderDetailID = GetLastVendorOrderDetailID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "000001";

		if ((lastVendorOrderDetailID != null) && (!lastVendorOrderDetailID.equals("")) ){
			String[] partsVendorOrderDetailID = lastVendorOrderDetailID.split("-");
			String partDate = partsVendorOrderDetailID[1];
			String partNumber = partsVendorOrderDetailID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("VODT-").append(datetime).append("-").append(stringInc);
		String vendorOrderDetailID = sb.toString();
		return vendorOrderDetailID;
	}

	public static String GetLastVendorOrderDetailID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String vendorOrderDetailID = "";

		try {
			sql = "{call sp_GetLastVendorOrderDetailID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastVendorOrderDetailID", user);

			while(jrs.next()){
				vendorOrderDetailID = jrs.getString("VendorOrderDetailID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastVendorOrderDetailID", sql , user);
		}

		return vendorOrderDetailID;
	}

	public static Boolean AllowAddVendorOrderDetail(String vendorOrderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Boolean allow = false;
		try {
			sql = "{call sp_AllowAddVendorOrderDetail(?,@allowAdd)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "AllowAddVendorOrderDetail", user);

			while(jrs.next()){
				allow = jrs.getBoolean("allowAdd");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "AllowAddVendorOrderDetail", sql , user);
		}

		return allow;
	}

	public static List<model.mdlVendorOrderDetail> LoadVendorOrderDetailByCustomerOrder(String CustomerOrderDetailID, String user) {
		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadVendorOrderDetailByCustomerOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerOrderDetailID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDetailByCustomerOrder", user);

			while(jrs.next()){
				model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
				mdlVendorOrderDetail.setDriverID(jrs.getString("DriverID"));
				mdlVendorOrderDetail.setDriverName(jrs.getString("DriverName"));
				mdlVendorOrderDetail.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlVendorOrderDetail.setVendorDetailStatus(jrs.getInt("VendorDetailStatus"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlVendorOrderDetail.setSealNumber(jrs.getString("SealNumber"));
				mdlVendorOrderDetail.setVendorID(jrs.getString("VendorID"));
				mdlVendorOrderDetail.setVendorName(jrs.getString("Name"));
				mdlVendorOrderDetail.setTpsShpLocationID(jrs.getString("Tps_ShpContainerLocationID"));
				mdlVendorOrderDetail.setTpsShpLocation(jrs.getString("Tps_ShpContainerLocation"));
				mdlVendorOrderDetail.setTpsShpMovementDate(jrs.getString("Tps_ShpContainerMovementDate"));
				if(jrs.getString("Tps_EirLink") == null)
					mdlVendorOrderDetail.setTpsEir("");
				else
					mdlVendorOrderDetail.setTpsEir(jrs.getString("Tps_EirLink"));
				
				listVendorOrderDetail.add(mdlVendorOrderDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByCustomerOrder", sql , user);
		}
		return listVendorOrderDetail;
	}
	
	public static List<model.mdlDeliveryOrder> LoadVendorOrderDetailByOrder(String OrderID, String user) {
		List<model.mdlDeliveryOrder> listDO = new ArrayList<model.mdlDeliveryOrder>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", OrderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDetailByOrder", user);

			while(jrs.next()){
				model.mdlDeliveryOrder mdlDO = new model.mdlDeliveryOrder();
				mdlDO.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlDO.setDoNumber(jrs.getString("VendorOrderDetailID"));
				mdlDO.setCustomerName(jrs.getString(3));
				mdlDO.setAddress(jrs.getString("Address"));
				mdlDO.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlDO.setSiNumber(jrs.getString("ShippingInvoiceID"));
				mdlDO.setTotalParty(jrs.getInt("TotalQuantity"));
				mdlDO.setVesselVoyage(jrs.getString("VesselName") + "/" + jrs.getString("VoyageNo"));
				mdlDO.setStuffingDate(adapter.ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlDO.setSealNumber(jrs.getString("SealNumber"));
				mdlDO.setPort(jrs.getString("PortName") + " - " + jrs.getString("PortUTC"));
				mdlDO.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlDO.setDriverName(jrs.getString("DriverName"));
				mdlDO.setDriverID(jrs.getString("DriverID"));
				mdlDO.setCommodity(jrs.getString("ItemDescription"));
				mdlDO.setCustomerPIC(jrs.getString("PIC"));

				mdlDO.setTimeInDepo(ConvertDateTimeHelper.formatDate(jrs.getString("DepoIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTimeOutDepo(ConvertDateTimeHelper.formatDate(jrs.getString("DepoOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTimeInFactory(ConvertDateTimeHelper.formatDate(jrs.getString("FactoryIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTimeOutFactory(ConvertDateTimeHelper.formatDate(jrs.getString("FactoryOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTimeInPort(ConvertDateTimeHelper.formatDate(jrs.getString("PortIn"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTimeOutPort(ConvertDateTimeHelper.formatDate(jrs.getString("PortOut"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				mdlDO.setTotalWeight(jrs.getString("totalWeight"));
				
				mdlDO.setTidNumber(jrs.getString(22));
				
				if(jrs.getString(31) == null)
					mdlDO.setOldTidNumber(jrs.getString(22));
				else
					mdlDO.setOldTidNumber(jrs.getString(31));
				
				mdlDO.setKojaInvoiceNo(jrs.getString("Tps_InvoiceNo"));
				
				listDO.add(mdlDO);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByOrder", sql , user);
		}
		return listDO;
	}
	
	public static model.mdlVendorOrderDetail LoadVendorOrderDetailByDetailID(String vendorOrderDetailID, String user) {
		model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadByDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderDetailID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDetailByDetailID", user);

			while(jrs.next()){
				mdlVendorOrderDetail.setVendorOrderDetailID(jrs.getString("VendorOrderDetailID"));
				mdlVendorOrderDetail.setVendorOrderID(jrs.getString("VendorOrderID"));
				mdlVendorOrderDetail.setDriverID(jrs.getString("DriverID"));
				mdlVendorOrderDetail.setDriverName(jrs.getString(4));
				mdlVendorOrderDetail.setVehicleNumber(jrs.getString("VehicleNumber"));
				mdlVendorOrderDetail.setVendorDetailStatus(jrs.getInt("VendorDetailStatus"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
				mdlVendorOrderDetail.setSealNumber(jrs.getString("SealNumber"));
				mdlVendorOrderDetail.setCreatedBy(jrs.getString("CreatedBy"));
				mdlVendorOrderDetail.setUpdatedBy(jrs.getString("UpdatedBy"));
				mdlVendorOrderDetail.setCancellationReason(jrs.getString("CancelReason"));
				mdlVendorOrderDetail.setOrderID(jrs.getString("OrderManagementID"));
				mdlVendorOrderDetail.setYellowcard(jrs.getString("YellowCard"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByDetailID", sql , user);
		}
		return mdlVendorOrderDetail;
	}
	
	public static model.mdlVendorOrderDetail LoadDriverByTIDAssociate(String orderID, String tidNumber, String user) {
		model.mdlVendorOrderDetail vodt = new model.mdlVendorOrderDetail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_DriverGetByTIDAssociate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", tidNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDriverByTIDAssociate", user);

			while(jrs.next()){
				vodt.driverID = jrs.getString("DriverID");
				vodt.vendorOrderDetailID = jrs.getString("VendorOrderDetailID");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDriverByTIDAssociate", sql , user);
		}
		return vodt;
	}
	
	public static void UpdateVendorOrderDetailTps(mdlAssociatedData param, String vodtID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String gatepass = param.TID+"\t"+param.CONTAINER+"\t"+param.TIX+"%"+
						  param.CONTAINER_STATUS+"%"+param.VESSEL_CODE+"%"+
						  param.VOYAGE_CODE+"%"+param.VALID_THRU+"%"+param.POD1+"%"+
						  param.POD2+"%"+param.PHONE.split("/")[0];
		/*String result = "";*/
		try{
			sql = "{call sp_VendorOrderDetailUpdateTps2(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vodtID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getVALID_THRU() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getPOD1() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getPOD2() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getISO() ));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getWEIGHT() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getCUSTOM_DOC() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getETB() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getIMO_CODE() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTRANSACTION_TYPE() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getBAT() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getBAT_DATE() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", gatepass ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getOWNER() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorOrderDetailTps", user);

			/*result = success == true ? "Success Update Vendor Order Detail Tps Data" : "Database Error";*/
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorOrderDetailTps", sql , user);
			/*result = "Database Error";*/
		}
		return;
	}
	
	public static String UpdateOldVehicleNumber(String vodtid, String oldVehicleNumber, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			sql = "{call sp_VodtUpdateOldVehicleNumber(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", oldVehicleNumber ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vodtid ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOldVehicleNumber", user);

			result = success == true ? "Success Update Old Vehicle Number" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOldVehicleNumber", sql , user);
			result = "Database Error";
		}
		return result;
	}
	
	public static void UpdateVendorOrderDetailStatus(String vodtID, String user){
		String sql = "";
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction;
		String dateNow = LocalDateTime.now().toString();
		
		try{
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", vodtID));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", 11));
			mdlQueryTransaction.sql = "{call sp_VendorOrderDetailUpdateStatus(?,?,?)}";
			sql = mdlQueryTransaction.sql;
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "UpdateVendorOrderDetailStatus", user);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorOrderDetailStatus", sql , user);
		}
		return;
	}
	
	public static model.mdlVendorOrderDetail LoadVendorOrderDetailByContainer(String containerNumber, String user) {
//		List<model.mdlVendorOrderDetail> listVendorOrderDetail = new ArrayList<model.mdlVendorOrderDetail>();
		model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadByContainer(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", containerNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDetailByContainer", user);

			while(jrs.next()){
				mdlVendorOrderDetail.setVendorOrderDetailID(jrs.getString("VendorOrderDetailID"));
				mdlVendorOrderDetail.setContainerNumber(jrs.getString("ContainerNumber"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDetailByContainer", sql , user);
		}
		return mdlVendorOrderDetail;
	}
	
	public static void UpdateVendorOrderDetailShippingLocation(mdlContainerData param, String vodtID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call sp_VendorOrderDetailUpdateShippingLocation(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vodtID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getVESSEL_LOCATION() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getLOCATION_ID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getSTACK_ON_VESSEL() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorOrderDetailShippingLocation", user);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorOrderDetailShippingLocation", sql , user);
		}
		return;
	}
	
	public static void UpdateKojaCms(modelKojaGetCms.mdlGetCms param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		try{
			sql = "{call ws_CmsLinkUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LINK ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CmsLinkUpdate", param.vendorOrderDetailID);

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CmsLinkUpdate", sql , param.vendorOrderDetailID);
		}

		return;
	}
	
	public static void UpdateKojaEir(modelKojaGetEir.mdlGetEir param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		try{
			sql = "{call ws_EirLinkUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.LINK ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_EirLinkUpdate", param.vendorOrderDetailID);

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_EirLinkUpdate", sql , param.vendorOrderDetailID);
		}

		return;
	}
	
}
