package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlCustomer;
import model.mdlFactory;
import model.mdlVendor;
import model.mdlVendorDriver;
import model.mdlVendorTier;
import model.mdlVendorVehicle;

public class VendorAdapter {

	public static List<model.mdlCustomer> LoadUnregisteredVendor(String user) {
		//seharusnya pakai mdlVendor, tapi di sini pakai mdlCustomer untuk kebutuhan getUserServlet
		List<model.mdlCustomer> listVendor = new ArrayList<model.mdlCustomer>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadUnregisteredVendor()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadUnregisteredVendor", user);

			while(jrs.next()){
				model.mdlCustomer vendor = new model.mdlCustomer();
				vendor.setCustomerID(jrs.getString("VendorID"));
				vendor.setCustomerName(jrs.getString("Name"));
				vendor.setCustomerAddress(jrs.getString("Address"));
				listVendor.add(vendor);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadUnregisteredVendor", sql , user);
		}

		return listVendor;
	}
	
	public static String CreateVendorID(String user){
		String lastVendorID = getLastVendorID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "0001";

		if ((lastVendorID != null) && (!lastVendorID.equals("")) ){
			String[] partsVendorID = lastVendorID.split("-");
			String partDate = partsVendorID[1];
			String partNumber = partsVendorID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("VEND-").append(datetime).append("-").append(stringInc);
		String VendorID = sb.toString();
		return VendorID;
	}
	
	public static String CreateVendorDriverID(String user){
		String lastVendorDriverID = getLastVendorDriverID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "0001";

		if ((lastVendorDriverID != null) && (!lastVendorDriverID.equals("")) ){
			String[] partsVendorDriverID = lastVendorDriverID.split("-");
			String partDate = partsVendorDriverID[1];
			String partNumber = partsVendorDriverID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("DRVR-").append(datetime).append("-").append(stringInc);
		String DriverID = sb.toString();
		return DriverID;
	}
	
	public static String getLastVendorID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String VendorID = "";
		try{
			sql = "{call sp_getLastVendorID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastVendorID", user);

			while(rowset.next())
			{
				VendorID = rowset.getString("VendorID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastVendorID", sql, user);
		}

		return VendorID;
	}
	
	public static String getLastVendorDriverID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String DriverID = "";
		try{
			sql = "{call sp_getLastVendorDriverID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastVendorDriverID", user);

			while(rowset.next())
			{
				DriverID = rowset.getString("DriverID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastVendorDriverID", sql, user);
		}

		return DriverID;
	}
	
	public static String RegisterVendor(model.mdlVendor lParam)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean check = false;
		String result = "";

		try{
			sql = "{call sp_CheckUsername(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckUsername", lParam.getEmail());
			//Globals.gCommand = sql;

			while(rowset.next()){
				check = true;
			}

			//if no duplicate username
			if(check==false)
			{
				Boolean success = false;
				List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
				model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();

				//store 'InsertVendor' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertVendor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorName));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.SIUJPT));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.GarageAddress));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAccount));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.province));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));
				//Globals.gCommand = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);

				//store 'InsertUser' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertUser(?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "R003"));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
				//Globals.gCommand = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
				
				for(String tierID : lParam.getTier()){
					//store 'InsertVendorTier' parameter for transaction
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_InsertVendorTier(?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", tierID));
					//Globals.gCommand = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				}

				success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "RegisterVendor", lParam.getEmail());
				if(success)
					result = "Success Register Vendor";
				else
					result = "Database error";
			}
			//if duplicate
			else {
				result = "The username already exist";
			}
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "RegisterVendor", sql , lParam.getEmail());
			result = "Database error";
		}

		return result;
	}

	public static List<model.mdlVendorTier> LoadVendorTierByVendor(String VendorID, String user){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlVendorTier> listVendorTier = new ArrayList<model.mdlVendorTier>();
		try{
			sql = "{call sp_LoadVendorTierByVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorTierByVendor", user);
			
			while(rowset.next())
			{
				model.mdlVendorTier mdlVendorTier = new model.mdlVendorTier();
				mdlVendorTier.VendorID = rowset.getString("VendorID");
				mdlVendorTier.VendorName = rowset.getString("Name");
				mdlVendorTier.TierID = rowset.getString("TierID");
				mdlVendorTier.TierName = rowset.getString("Description");
				listVendorTier.add(mdlVendorTier);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorTierByVendor", sql, user);
		}
		
		return listVendorTier;
	}

	public static String InsertVendorTier(mdlVendorTier lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertVendorTier(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TierID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVendorTier", user);

			result = success == true ? "Success Insert Vendor Tier" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendorTier", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteVendorTier(String VendorID, String TierID, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteVendorTier(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", TierID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteVendorTier", user);

			result = success == true ? "Success Delete Vendor Tier" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteVendorTier", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static List<model.mdlVendor> LoadVendor(String user) {
		List<model.mdlVendor> listVendor = new ArrayList<model.mdlVendor>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadVendor()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadVendor", user);

			while(jrs.next()){
				model.mdlVendor vendor = new model.mdlVendor();
				vendor.setVendorID(jrs.getString("VendorID"));
				vendor.setVendorName(jrs.getString("Name"));
				vendor.setPIC(jrs.getString("PIC"));
				vendor.setAddress(jrs.getString("Address"));
				vendor.setEmail(jrs.getString("Email"));
				vendor.setOfficePhone(jrs.getString("OfficePhone"));
				vendor.setMobilePhone(jrs.getString("MobilePhone"));
				vendor.setNPWP(jrs.getString("NPWP"));
				vendor.setSIUJPT(jrs.getString("SIUJPT"));
				vendor.setGarageAddress(jrs.getString("GarageAddress"));
				vendor.setBankName(jrs.getString("BankName"));
				vendor.setBankAccount(jrs.getString("BankAccount"));
				vendor.setBankBranch(jrs.getString("BankBranch"));
				vendor.setProvince(jrs.getString("Province"));
				vendor.setPostalCode(jrs.getString("PostalCode"));
				listVendor.add(vendor);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendor", sql , user);
		}

		return listVendor;
	}

	public static List<model.mdlVendor> LoadVendorByID(String lVendorID) {
		List<model.mdlVendor> listVendor = new ArrayList<model.mdlVendor>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_LoadVendorByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorByID", lVendorID);

			while(jrs.next()){
				model.mdlVendor vendor = new model.mdlVendor();
				vendor.setVendorID(jrs.getString("VendorID"));
				vendor.setVendorName(jrs.getString("Name"));
				vendor.setPIC(jrs.getString("PIC"));
				vendor.setAddress(jrs.getString("Address"));
				vendor.setEmail(jrs.getString("Email"));
				vendor.setOfficePhone(jrs.getString("OfficePhone"));
				vendor.setMobilePhone(jrs.getString("MobilePhone"));
				vendor.setNPWP(jrs.getString("NPWP"));
				vendor.setSIUJPT(jrs.getString("SIUJPT"));
				vendor.setGarageAddress(jrs.getString("GarageAddress"));
				vendor.setBankName(jrs.getString("BankName"));
				vendor.setBankAccount(jrs.getString("BankAccount"));
				vendor.setBankBranch(jrs.getString("BankBranch"));
				vendor.setProvince(jrs.getString("Province"));
				vendor.setPostalCode(jrs.getString("PostalCode"));
				listVendor.add(vendor);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorByID", sql , lVendorID);
		}

		return listVendor;
	}
	
	public static String DeleteVendor(String lVendorID, String user)
	{
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{
			//store 'DeleteVendorTier' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteVendorTierByVendor(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			//store 'DeleteVendor' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteVendor(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			//store 'DeleteUser' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteUserByID(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "DeleteVendor", user);
			if(success)
				result = "Success Delete Vendor";
			else
				result = "Database error";
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "DeleteVendor", sql , user);
			result = "Database error";
		}

		return result;
	}

	public static String InsertVendor(mdlVendor lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			if(lParam.VendorID.contentEquals(""))
				lParam.VendorID = VendorAdapter.CreateVendorID(user);
			
			sql = "{call sp_InsertVendor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.SIUJPT));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.GarageAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAccount));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.province));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVendor", user);

			result = success == true ? "Success Insert Vendor" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendor", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateVendor(mdlVendor lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateVendor(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.SIUJPT));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.GarageAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAccount));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.province));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendor", user);

			result = success == true ? "Success Update Vendor" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendor", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static List<model.mdlVendorVehicle> LoadVendorVehicleByVendor(String VendorID){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlVendorVehicle> listVendorVehicle = new ArrayList<model.mdlVendorVehicle>();
		try{
			sql = "{call sp_LoadVendorVehicleByVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorVehicleByVendor", VendorID);
			
			while(rowset.next())
			{
				model.mdlVendorVehicle mdlVendorVehicle = new model.mdlVendorVehicle();
				mdlVendorVehicle.VendorID = rowset.getString("VendorID");
				mdlVendorVehicle.VendorName = rowset.getString("Name");
				mdlVendorVehicle.VehicleNumber = rowset.getString("VehicleNumber");
				mdlVendorVehicle.setBrand(rowset.getString("Brand"));
				mdlVendorVehicle.setType(rowset.getString("Type"));
				mdlVendorVehicle.setYear(rowset.getString("Year"));
				mdlVendorVehicle.setTidNumber(rowset.getString("TID_Number"));
				mdlVendorVehicle.setStnkNumber(rowset.getString("STNK_Number"));
				mdlVendorVehicle.setStnkExpired(ConvertDateTimeHelper.formatDate(rowset.getString("STNK_Expired"), "yyyy-MM-dd", "dd MMM yyyy"));
				listVendorVehicle.add(mdlVendorVehicle);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorVehicleByVendor", sql, VendorID);
		}
		
		return listVendorVehicle;
	}
	
	public static List<model.mdlVendorVehicle> LoadVendorVehicle(String user) {
		List<model.mdlVendorVehicle> listVendorVehicle = new ArrayList<model.mdlVendorVehicle>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadVendorVehicle()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadVendorVehicle", user);

			while(jrs.next()){
				model.mdlVendorVehicle vendorVehicle = new model.mdlVendorVehicle();
				vendorVehicle.setVendorID(jrs.getString("VendorID"));
				vendorVehicle.setVendorName(jrs.getString("Name"));
				vendorVehicle.setVehicleNumber(jrs.getString("VehicleNumber"));
				vendorVehicle.setBrand(jrs.getString("Brand"));
				vendorVehicle.setType(jrs.getString("Type"));
				vendorVehicle.setYear(jrs.getString("Year"));
				vendorVehicle.setTidNumber(jrs.getString("TID_Number"));
				vendorVehicle.setStnkNumber(jrs.getString("STNK_Number"));
				vendorVehicle.setStnkExpired(ConvertDateTimeHelper.formatDate(jrs.getString("STNK_Expired"), "yyyy-MM-dd", "dd MMM yyyy"));
				listVendorVehicle.add(vendorVehicle);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorVehicle", sql , user);
		}

		return listVendorVehicle;
	}

	public static String DeleteVendorVehicle(String VendorID, String VehicleNumber, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteVendorVehicle(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", VehicleNumber));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteVendorVehicle", user);

			result = success == true ? "Success Delete Vendor Vehicle" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteVendorVehicle", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String InsertVendorVehicle(model.mdlVendorVehicle lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean check = false;
		String result = "";
		
		try{
			sql = "{call sp_CheckVendorVehicle(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VehicleNumber));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckVendorVehicle", user);
			//Globals.gCommand = sql;

			while(rowset.next()){
				check = true;
			}

			//if no duplicate vendor vehicle
			if(check==false)
			{
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_InsertVendorVehicle(?,?,?,?,?,?,?,?)}";
				//Globals.gCommand = sql;
				Boolean success = false;
				
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VehicleNumber));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.brand));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.type));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.year));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.stnkNumber));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.stnkExpired));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.tidNumber));
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVendorVehicle", user);

				result = success == true ? "Success Insert Vendor Vehicle" : "Database Error";
			}
			//if duplicate
			else {
				result = "The vendor's vehicle already exist";
			}
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertVendorVehicle", sql , user);
	        		result = "Database error";
		}

		return result;
	}
	
	public static String UpdateVendorVehicle(mdlVendorVehicle lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_VendorVehicleUpdate(?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VehicleNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.brand));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.type));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.year));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.stnkNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.stnkExpired));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.tidNumber));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorVehicle", user);

			result = success == true ? "Success Update Vendor Vehicle" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorVehicle", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static List<model.mdlVendorDriver> LoadVendorDriver(String user) {
		List<model.mdlVendorDriver> listVendorDriver = new ArrayList<model.mdlVendorDriver>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadVendorDriver()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadVendorDriver", user);

			while(jrs.next()){
				model.mdlVendorDriver vendorDriver = new model.mdlVendorDriver();
				vendorDriver.setVendorID(jrs.getString("VendorID"));
				vendorDriver.setVendorName(jrs.getString("Name"));
				vendorDriver.setDriverID(jrs.getString("DriverID"));
				vendorDriver.setDriverName(jrs.getString("DriverName"));
				vendorDriver.setDeviceID(jrs.getString("DeviceID"));
				vendorDriver.setDriverUsername(jrs.getString("Username"));
				vendorDriver.setDriverPassword(jrs.getString("Password"));
				listVendorDriver.add(vendorDriver);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorDriver", sql , user);
		}

		return listVendorDriver;
	}
	
	public static List<model.mdlVendorDriver> LoadVendorDriverByVendor(String VendorID){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlVendorDriver> listVendorDriver = new ArrayList<model.mdlVendorDriver>();
		try{
			sql = "{call sp_LoadVendorDriverByVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorDriverByVendor", VendorID);
			
			while(rowset.next())
			{
				model.mdlVendorDriver mdlVendorDriver = new model.mdlVendorDriver();
				mdlVendorDriver.VendorID = rowset.getString("VendorID");
				mdlVendorDriver.VendorName = rowset.getString("Name");
				mdlVendorDriver.DriverID = rowset.getString("DriverID");
				mdlVendorDriver.DriverName = rowset.getString("DriverName");
				mdlVendorDriver.DeviceID = rowset.getString("DeviceID");
				mdlVendorDriver.DriverUsername = rowset.getString("Username");
				mdlVendorDriver.DriverPassword = rowset.getString("Password");
				listVendorDriver.add(mdlVendorDriver);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorDriverByVendor", sql, VendorID);
		}
		
		return listVendorDriver;
	}
	
	public static String DeleteVendorDriver(String VendorID, String DriverID, String user)
	{
		//List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		////Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction;
		String result = "";

		try{
			//store 'DeleteVendorDriver' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteVendorDriver(?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", DriverID ));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'DeleteUserConfig' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UserConfigDelete(?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", DriverID ));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//sql = "{call sp_DeleteVendorDriver(?,?)}";
			//listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			//listParam.add(QueryExecuteAdapter.QueryParam("string", DriverID));
			
			//success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteVendorDriver");
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "DeleteVendorDriver", user);
			result = success == true ? "Success Delete Vendor Driver" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteVendorDriver", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String InsertVendorDriver(mdlVendorDriver lParam, String user)
	{
		//List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction;
		String result = "";
		
		model.mdlUserConfig defaultUserConfig = UserConfigAdapter.LoadDefaultUserConfig(user);
		try{
			//store 'InsertVendorDriver' parameter for transaction
			String VendorDriverID = CreateVendorDriverID(user);
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_InsertVendorDriver(?,?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", VendorDriverID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverName ));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'InsertUserConfig' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UserConfigInsert(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getDeviceID() ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", VendorDriverID ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.IpLocal ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.PortLocal ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.IpPublic ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.PortPublic ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.IpAlternative ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", defaultUserConfig.PortAlternative ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverUsername ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverPassword ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
			//Globals.gCommand = mdlQueryTransaction.sql;
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//sql = "{call sp_InsertVendorDriver(?,?,?)}";
			//listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			//listParam.add(QueryExecuteAdapter.QueryParam("string", CreateVendorDriverID()));
			//listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverName));
			//success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVendorDriver");
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertVendorDriver", user);
			result = success == true ? "Success Insert Vendor Driver" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendorDriver", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateVendorDriver(mdlVendorDriver lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UserConfigUpdate(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DeviceID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverUsername));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverPassword));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DriverID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorDriver", user);

			result = success == true ? "Success Update Vendor Driver" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorDriver", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static List<model.mdlVendor> LoadVendorByOrder(String OrderID, String user){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlVendor> listVendor = new ArrayList<model.mdlVendor>();
		try{
			sql = "{call sp_VendorLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", OrderID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorByOrder", user);
			
			while(rowset.next())
			{
				model.mdlVendor mdlVendor = new model.mdlVendor();
				mdlVendor.VendorID = rowset.getString("VendorID");
				listVendor.add(mdlVendor);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorByOrder", sql, user);
		}
		
		return listVendor;
	}
	
	public static String GetTidNumberByVendorVehicle(String VendorID, String VehicleNumber){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		String tidNumber = "";
		/*List<model.mdlVendorVehicle> listVendorVehicle = new ArrayList<model.mdlVendorVehicle>();*/
		try{
			sql = "{call sp_TidGetByVendorVehicle(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", VehicleNumber));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetTidNumberByVendorVehicle", VendorID);
			
			while(rowset.next())
			{
				tidNumber = rowset.getString("TID_Number");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetTidNumberByVendorVehicle", sql, VendorID);
		}
		
		return tidNumber;
	}
	
}
