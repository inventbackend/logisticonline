package adapter;

import java.util.Base64;

public class Base64Adapter {
	public static String EncriptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] encodedBytes = Base64.getEncoder().encode(lKeyword.getBytes());
			lResult = new String(encodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EncriptBase64", "" , "");
		}
		return lResult;
	}
	
	public static String DecriptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] decodedBytes = Base64.getDecoder().decode(lKeyword);
			lResult = new String(decodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DecriptBase64", "" , "");
		}
		return lResult;
	}

}
