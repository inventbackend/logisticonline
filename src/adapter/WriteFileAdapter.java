package adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import model.Globals;


public class WriteFileAdapter {

	public static String Write(InputStream fileinputstream, String fileoutputstream) {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		String lResult = "";

		try {
			// read this file into InputStream
//			inputStream = new FileInputStream(fileinputstream);
			inputStream = fileinputstream;

			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(new File(fileoutputstream));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			System.out.println("Done!");

		} catch (IOException e) {
			e.printStackTrace();
			lResult = "Error Write File";
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		
		return lResult;
	    }
	
	public static String Delete(String filestream)
    {
		String lResult = "";
    	try{

    		File file = new File(filestream);

    		if(file.delete()){
    			System.out.println(file.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
    	}catch(Exception e){

    		e.printStackTrace();
    		lResult = "Error Delete File";
    	}

    	return lResult;
    }
	
	public static String Write2(InputStream fileinputstream, String paramFilePath, String fileoutputstream) {

        InputStream inputStream = null;
        OutputStream outputStream = null;
        
        File directory = null;
        String lResult = "";

        try {
            // read this file into InputStream
            //inputStream = new FileInputStream(fileinputstream);
            inputStream = fileinputstream;
            
            //initiate variable directory
            directory = new File(paramFilePath.substring(0, paramFilePath.length() - 1));
            
            //check if directory is exist or not, if not, create it first
            if(!directory.exists() )
                directory.mkdirs();
            
            //write the inputStream to a FileOutputStream
            outputStream = new FileOutputStream(new File(fileoutputstream));
            
            //write process
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            System.out.println("Done!");

        } catch (IOException e) {
            e.printStackTrace();
            lResult = "Error Write File";
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        
        return lResult;
    }
	
}
