package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlOrderManagement;
import model.mdlVendorOrderDetail;
import model.mdlYellowCard;

public class YellowCardAdapter {

	public static model.mdlYellowCardParam GetParamBySI(String shippingInstruction, String user) {
		model.mdlYellowCardParam yellowCardParam = new model.mdlYellowCardParam();
		yellowCardParam.orderDetail = new ArrayList<model.mdlOrderManagementDetail>();
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();		
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_OrderManagementLoadBySI(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", shippingInstruction));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetParamBySI - OrderManagementLoadBySI", user);

			while(jrs.next()){				
				yellowCardParam.setOrderManagementID(jrs.getString("OrderManagementID"));
				yellowCardParam.setShippingInstruction(jrs.getString("ShippingInvoiceID"));
				yellowCardParam.setDoShippingLine(jrs.getString("DeliveryOrderID"));
				yellowCardParam.setPeb(jrs.getString("PEB"));
				yellowCardParam.setNpe(jrs.getString("NPE"));
				yellowCardParam.setCustomerName(jrs.getString(9));
				yellowCardParam.setFactoryName(jrs.getString(10));
				yellowCardParam.setOrderType(jrs.getString("OrderType"));
				yellowCardParam.setItemDescription(jrs.getString("ItemDescription"));
				
				listParam = new ArrayList<model.mdlQueryExecute>();	
				CachedRowSet jrs2 = null;
				sql = "{call sp_LoadOrderManagementDetailByID(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", yellowCardParam.getOrderManagementID()));
				jrs2 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetParamBySI - LoadOrderManagementDetailByID", user);
				
				while(jrs2.next()){
					model.mdlOrderManagementDetail orderDetail = new model.mdlOrderManagementDetail();
					orderDetail.vendorOrder = new ArrayList<model.mdlVendorOrderDetail>();
					orderDetail.setOrderManagementDetailID(jrs2.getString("OrderManagementDetailID"));
					orderDetail.setStuffingDate(jrs2.getString("StuffingDate"));
					orderDetail.setContainerTypeDescription(jrs2.getString("Description"));
					orderDetail.setQuantity(jrs2.getInt("Quantity"));
					
					listParam = new ArrayList<model.mdlQueryExecute>();
					CachedRowSet jrs3 = null;
					sql = "{call sp_LoadVendorOrderDetailByCustomerOrder(?)}";
					listParam.add(QueryExecuteAdapter.QueryParam("string", orderDetail.getOrderManagementDetailID()));
					jrs3 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetParamBySI - LoadVendorOrderDetailByCustomerOrder", user);
					
					while(jrs3.next()){
						if(jrs3.getString("YellowCard").equals("") && !jrs3.getString("ContainerNumber").equals("")){
							model.mdlVendorOrderDetail vendorOrder = new model.mdlVendorOrderDetail();
							vendorOrder.setVendorName(jrs3.getString("Name"));
							vendorOrder.setDriverName(jrs3.getString("DriverName"));
							vendorOrder.setVehicleNumber(jrs3.getString("VehicleNumber"));
							vendorOrder.setContainerNumber(jrs3.getString("ContainerNumber"));
							vendorOrder.setSealNumber(jrs3.getString("SealNumber"));
							
							orderDetail.vendorOrder.add(vendorOrder);
						}
					}
					
					yellowCardParam.orderDetail.add(orderDetail);
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetParamBySI", sql , user);
		}

		return yellowCardParam;
	}

	public static model.mdlVendorOrderDetail GetVendorOrderDetailForYellowCard(mdlVendorOrderDetail lParam, String user) {
		model.mdlVendorOrderDetail vendorOrderDetail = new model.mdlVendorOrderDetail();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderDetailLoadForYellowCard(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getShippingInstruction()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getContainerNumber()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVehicleNumber()));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetVendorOrderDetailForYellowCard", user);

			while(jrs.next()){
				vendorOrderDetail.setVendorOrderDetailID(jrs.getString("VendorOrderDetailID"));
				vendorOrderDetail.setDriverID(jrs.getString("DriverID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetVendorOrderDetailForYellowCard", sql , user);
		}
		return vendorOrderDetail;
	}

	public static String UpdateYellowCard(mdlVendorOrderDetail lParam, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_YellowCardUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.vendorOrderDetailID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.yellowcard ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateYellowCard", user);

			result = success == true ? "Success Update Yellow Card" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateYellowCard", sql , user);
			result = "Database Error";
		}

		return result;
	}
}
