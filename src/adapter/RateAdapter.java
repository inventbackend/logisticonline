package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlRate;

public class RateAdapter {
	public static List<model.mdlRate> LoadRate(String user) {
		List<model.mdlRate> listRate = new ArrayList<model.mdlRate>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadRate()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadRate", user);

			while(jrs.next()){
				model.mdlRate rate = new model.mdlRate();
				rate.setRateID(jrs.getString("RateID"));
				rate.setName(jrs.getString("Name"));
				rate.setDistrictID(jrs.getString("DistrictID"));
				rate.setDistrictName(jrs.getString(4));
				rate.setTierID(jrs.getString("TierID"));
				rate.setTierName(jrs.getString(6));
				rate.setContainerTypeID(jrs.getString("ContainerTypeID"));
				rate.setContainerTypeName(jrs.getString(8));
				Double price = jrs.getDouble("Price");
				rate.setPrice(price.intValue());
				rate.setRoleID(jrs.getString("RoleID"));
				rate.setRoleName(jrs.getString(11));
				listRate.add(rate);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadRate", sql , user);
		}

		return listRate;
	}

	public static model.mdlRate LoadRateByID (String lRateID, String user) {
		model.mdlRate mdlRate = new model.mdlRate();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadRateByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRateID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadRateByID", user);

			while(jrs.next()){
				mdlRate.setRateID(jrs.getString("RateID"));
				mdlRate.setName(jrs.getString("Name"));
				mdlRate.setDistrictID(jrs.getString("DistrictID"));
				mdlRate.setDistrictName(jrs.getString(4));
				mdlRate.setTierID(jrs.getString("TierID"));
				mdlRate.setTierName(jrs.getString(6));
				mdlRate.setContainerTypeID(jrs.getString("ContainerTypeID"));
				mdlRate.setContainerTypeName(jrs.getString(8));
				Double price = jrs.getDouble("Price");
				mdlRate.setPrice(price.intValue());
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadRateByID", sql , user);
		}

		return mdlRate;
	}

	public static String InsertRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlRate CheckDuplicateRate = LoadRateByID(mdlRate.getRateID(),user);
			if(CheckDuplicateRate.getRateID() == null){
				String newRateID = CreateRateID(user);
				sql = "{call sp_InsertRate(?,?,?,?, ?,?,?)}";

				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getRoleID()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", newRateID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getName()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getDistrictID()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getTierID()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRate.getPrice()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertRate", user);

				result = success == true ? "Success Insert Rate" : "Database Error";
			}
			//if duplicate
			else{
				result = "Rate sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertRate", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateRate(?,?,?, ?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getRateID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getDistrictID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getTierID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer",mdlRate.getPrice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string",mdlRate.getRoleID()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateRate", user);

			result = success == true ? "Success Update Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateRate", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteRate(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteRate(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteRate", user);

			result = success == true ? "Success Delete Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteRate", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateRateID(String user){
		String lastRateID = GetLastRateID(user);
		String stringIncrement = "0001";

		if ((lastRateID != null) && (!lastRateID.equals("")) ){
			String[] partsRateID = lastRateID.split("-");
			//get last running number
			String partNumber = partsRateID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("RATE-").append(stringIncrement);
		String RateID = sb.toString();
		return RateID;
	}

	public static String GetLastRateID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String RateID = "";

		try {
			sql = "{call sp_GetLastRateID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastRateID", user);

			while(jrs.next()){
				RateID = jrs.getString("RateID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastRateID", sql , user);
		}

		return RateID;
	}

	public static List<model.mdlRate> LoadCustomerFinalRate (String lCustomerID, String lFactoryID, String lDistrictID, String user) {
		List<model.mdlRate> rateList = new ArrayList<model.mdlRate>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadCustomerFinalRate(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID) );
			listParam.add(QueryExecuteAdapter.QueryParam("string", lFactoryID) );
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDistrictID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadCustomerFinalRate", user);

			while(jrs.next()){
				model.mdlRate mdlRate = new model.mdlRate();
				mdlRate.setCustomerID(jrs.getString("CustomerID"));
				mdlRate.setFactoryID(jrs.getString("FactoryID"));
				mdlRate.setContainerTypeID(jrs.getString("ContainerTypeID"));
				mdlRate.setContainerTypeName(jrs.getString("Description"));
				mdlRate.setTierID(jrs.getString("TierID"));
				Double finalPrice = jrs.getDouble("FinalRate");
				mdlRate.setFinalPrice(finalPrice.intValue());
				Double price = jrs.getDouble("Price");
				mdlRate.setPrice(price.intValue());
				rateList.add(mdlRate);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerFinalRate", sql , user);
		}

		return rateList;
	}
	
	public static List<model.mdlRate> LoadVendorFinalRate(String lVendorID, String user) {
		List<model.mdlRate> rateList = new ArrayList<model.mdlRate>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_FinalRateLoadByVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorFinalRate", user);

			while(jrs.next()){
				model.mdlRate mdlRate = new model.mdlRate();
				mdlRate.setVendorID(jrs.getString("VendorID"));
				mdlRate.setDistrictID(jrs.getString("DistrictID"));
				mdlRate.setDistrictName(jrs.getString("Name"));
				mdlRate.setContainerTypeID(jrs.getString("ContainerTypeID"));
				mdlRate.setContainerTypeName(jrs.getString("Description"));
				mdlRate.setTierID(jrs.getString("TierID"));
				
				Double price = jrs.getDouble(7);
				mdlRate.setPrice(price.intValue());
				
				Double finalPrice = jrs.getDouble("FinalRate");
				mdlRate.setFinalPrice(finalPrice.intValue());
				
				rateList.add(mdlRate);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorFinalRate", sql , user);
		}

		return rateList;
	}
	
	public static List<model.mdlContainerType> LoadContainerTypeByDistrictRate (String lDistrictID, String user) {
		List<model.mdlContainerType> ctList = new ArrayList<model.mdlContainerType>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadContainerTypeByDistrictRate(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDistrictID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadContainerTypeByDistrictRate", user);

			while(jrs.next()){
				model.mdlContainerType ct = new model.mdlContainerType();
				ct.setContainerTypeID(jrs.getString("ContainerTypeID"));
				ct.setDescription(jrs.getString("Description"));
				ctList.add(ct);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadContainerTypeByDistrictRate", sql , user);
		}

		return ctList;
	}
	
	public static String InsertCustomerFinalRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertCustomerFinalRate(?,?,?,?)}";

			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getCustomerID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getFactoryID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRate.getFinalPrice()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertCustomerFinalRate", user);

			result = success == true ? "Success Insert Final Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomerFinalRate", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateCustomerFinalRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateCustomerFinalRate(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getCustomerID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getFactoryID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRate.getFinalPrice()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateCustomerFinalRate", user);

			result = success == true ? "Success Update Final Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomerFinalRate", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String InsertVendorFinalRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_VendorFinalRateInsert(?,?,?,?,?)}";

			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getVendorID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getDistrictID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getTierID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRate.getFinalPrice()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVendorFinalRate", user);

			result = success == true ? "Success Insert Final Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVendorFinalRate", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateVendorFinalRate(mdlRate mdlRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_VendorFinalRateUpdate(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getVendorID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getDistrictID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlRate.getTierID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlRate.getFinalPrice()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateVendorFinalRate", user);

			result = success == true ? "Success Update Final Rate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateVendorFinalRate", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
}
