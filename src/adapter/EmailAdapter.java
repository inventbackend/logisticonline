package adapter;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlUser;

import javax.activation.*;

public class EmailAdapter {

	public static void SendEmail(mdlUser lParam, String user)
	{
		String key = "";
	   try {
		// Get the base naming context from web.xml
		  Context web_context = (Context)new InitialContext().lookup("java:comp/env");
		      
		// Recipient's email ID needs to be mentioned.
	      String to = lParam.UserEmail;

	      // Sender's email ID needs to be mentioned	   
	      String from  = (String)web_context.lookup("owner_email");
	      
	      key = "Sending from "+ from +"to "+to;

	      // email host
	      String host = (String)web_context.lookup("host");
	      
	      //web domain
	      String domain = (String)web_context.lookup("domain");
	      
	      //admin contact
	      String contact = (String)web_context.lookup("contact");

	      // Get system properties
	      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	      Properties properties = System.getProperties();

	      // Setup mail server
	      properties.setProperty("mail.smtp.host", host);
	      properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
	      properties.setProperty("mail.smtp.socketFactory.fallback", "false");
	      properties.setProperty("mail.smtp.port", "465");
	      properties.setProperty("mail.smtp.socketFactory.port", "465");
	      properties.put("mail.smtp.auth", "true");
	      properties.put("mail.debug", "true");
	      properties.put("mail.store.protocol", "pop3");
	      properties.put("mail.transport.protocol", "smtp");
	      String username = from;
	      String password = (String)web_context.lookup("owner_email_password");

	      // Get the default Session object.
	      Session session = Session.getDefaultInstance(properties,
                  new Authenticator(){
              protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(username, password);
              }});
	      
	         // Create a default MimeMessage object.
	         MimeMessage message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));

	         // Set To: header field of the header.
	         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

	         // Set Subject: header field
	         message.setSubject("LOGOL Account Notification!");
	         
	         // Send the actual HTML message, as big as you like
	         if(lParam.getIsActive() == 1)
	        	 message.setContent("<h3>Your account has been activated, now you can access LOGOL service at :<br> "
     		 			+ "&nbsp;Web Address : " + domain +"<br>"
      					+ "&nbsp;User name : " + lParam.Username +"<br>"
      					+ "&nbsp;Password : " + lParam.unEncryptedPassword +"<br><br>"
      					+ "Please do not reply to this email. If you need further enquiry with your order please "
      					+ "contact enquiry@logol.co.id</h3>", "text/html");
	         else if(lParam.getIsActive() == 2)
	        	 message.setContent("<h3>Your account has been blocked, please contact enquiry@logol.co.id for further information<br><br>"
	        			 			+ "Please do not reply to this email. If you need further enquiry with your order please "
	        			 			+ "contact enquiry@logol.co.id</h3>", "text/html");
	         else if(lParam.getIsActive() == 3)
	        	 message.setContent("<h3>Your account has been deleted, please contact enquiry@logol.co.id for further information<br><br>"
	        			 			+ "Please do not reply to this email. If you need further enquiry with your order please "
	        			 			+ "contact enquiry@logol.co.id</h3>", "text/html");
	         
	         // Send message
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      } catch (Exception mex) {
	         mex.printStackTrace();
	         LogAdapter.InsertLogExc(mex.toString(), "SendEmail", key , user);
	      }
	}
	
	public static String LoadUserEmail(model.mdlUser lParam, String user){	
		String userEmail = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			//get the email from the customer
			if(lParam.getRole().contentEquals("R002")) {
				sql = "{call sp_LoadCustomerByID(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getUserId() ));
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserEmail", user);	
				while(rowset.next())
				{
					userEmail = rowset.getString("Email");
				}
			}
			//get the email from the vendor
			else if(lParam.getRole().contentEquals("R003")) {
				sql = "{call sp_LoadVendorByID(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getUserId() ));
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserEmail", user);	
				while(rowset.next())
				{
					userEmail = rowset.getString("Email");
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserEmail", sql, user);
		}

		return userEmail;
	}
	
	public static model.mdlUser LoadUserEmailByUserID(String userid, String loginUser){	
		model.mdlUser user = new model.mdlUser();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			//get the email
			sql = "{call sp_LoadEmailByUserID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", userid ));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserEmailByUserID", loginUser);	
			while(rowset.next())
			{
				user.UserEmail = rowset.getString("Email");
				user.Role = rowset.getString("RoleID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserEmailByUserID", sql, loginUser);
		}

		return user;
	}
	
	public static void SendEmailByTemplate(String to, String subject, String content)
	{
		String key = "";
		   try {
			// Get the base naming context from web.xml
			  Context web_context = (Context)new InitialContext().lookup("java:comp/env");
			      

		      // Sender's email ID needs to be mentioned	   
		      String from  = (String)web_context.lookup("owner_email");
		      
		      key = "Sending from "+ from +"to "+to;

		      // email host
		      String host = (String)web_context.lookup("host");

		      // Get system properties
		      String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		      Properties properties = System.getProperties();

		      // Setup mail server
		      properties.setProperty("mail.smtp.host", host);
		      properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		      properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		      properties.setProperty("mail.smtp.port", "465");
		      properties.setProperty("mail.smtp.socketFactory.port", "465");
		      properties.put("mail.smtp.auth", "true");
		      properties.put("mail.debug", "true");
		      properties.put("mail.store.protocol", "pop3");
		      properties.put("mail.transport.protocol", "smtp");
		      String username = from;
		      String password = (String)web_context.lookup("owner_email_password");

			      // Get the default Session object.
			      Session session = Session.getDefaultInstance(properties,
	                  new Authenticator(){
			    	  protected PasswordAuthentication getPasswordAuthentication() {
	                 return new PasswordAuthentication(username, password);
	              }});
		      
		         // Create a default MimeMessage object.
		         MimeMessage message = new MimeMessage(session);

		         // Set From: header field of the header.
		         message.setFrom(new InternetAddress(from));

		         // Set To: header field of the header.
		         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		         // Set Subject: header field
		         message.setSubject(subject);
		         
		         // Send the actual HTML message, as big as you like
	        	 message.setContent(content, "text/html");
		         
		         // Send message
		         Transport.send(message);
		         System.out.println("Sent message successfully....");
		      } catch (Exception mex) {
		         mex.printStackTrace();
		         LogAdapter.InsertLogExc(mex.toString(), "SendEmail", key , "guest");
		      }
		
		
	}
	
}
