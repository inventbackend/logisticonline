package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

public class OrderManagementDetailAdapter {

	/**
	 * this function is to load list of order management detail by using OrderManagementID given in parameter
	 * @param orderManagementID
	 * @return
	 */
	public static List<model.mdlOrderManagementDetail> LoadOrderManagementDetailByID(String orderManagementID, String user) {
		List<model.mdlOrderManagementDetail> listOrderManagementDetail = new ArrayList<model.mdlOrderManagementDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadOrderManagementDetailByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadOrderManagementDetailByID", user);

			while(jrs.next()){
				model.mdlOrderManagementDetail mdlOrderManagementDetail = new model.mdlOrderManagementDetail();
				mdlOrderManagementDetail.setOrderManagementDetailID(jrs.getString("OrderManagementDetailID"));
				mdlOrderManagementDetail.setOrderManagementID(jrs.getString("OrderManagementID"));
				String tempDate = jrs.getString("StuffingDate");
				String stuffingDate = ConvertDateTimeHelper.formatDate(tempDate, "yyyy-MM-dd", "dd MMM yyyy");
				mdlOrderManagementDetail.setStuffingDate(stuffingDate);
				mdlOrderManagementDetail.setTierID(jrs.getString("TierID"));
				mdlOrderManagementDetail.setContainerTypeID(jrs.getString("ContainerTypeID"));
				mdlOrderManagementDetail.setContainerTypeDescription(jrs.getString("Description"));
				mdlOrderManagementDetail.setItemDescription(jrs.getString("ItemDescription"));
				mdlOrderManagementDetail.setOrderStatus(jrs.getString("OrderDetailStatus"));
				mdlOrderManagementDetail.setQuantity(jrs.getInt("Quantity"));
				Double price = jrs.getDouble("Price");
				mdlOrderManagementDetail.setPrice(price.intValue());
				mdlOrderManagementDetail.setRatePrice(price.intValue()/jrs.getInt("Quantity"));
				mdlOrderManagementDetail.setQuantityDelivered(jrs.getInt("QuantityDelivered"));
				Double priceDelivered = jrs.getDouble("PriceDelivered");
				mdlOrderManagementDetail.setPriceDelivered(priceDelivered.intValue());
			    mdlOrderManagementDetail.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlOrderManagementDetail.setUpdatedBy(jrs.getString("UpdatedBy"));

			    listOrderManagementDetail.add(mdlOrderManagementDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOrderManagementDetailByID", sql , user);
		}
		return listOrderManagementDetail;
	}

	public static String InsertOrderManagementDetail(model.mdlOrderManagementDetail mdlOrderManagementDetail, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			String newOrderManagementDetailID = CreateOrderManagementDetailID(user);
			sql = "{call sp_InsertOrderManagementDetail(?,?,?, ?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", newOrderManagementDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getOrderManagementID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getStuffingDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagementDetail.getQuantity()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagementDetail.getPrice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getCreatedBy()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertOrderManagementDetail", user);

			result = success == true ? "Success Insert Order Management Detail" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertOrderManagementDetail", sql , user);
			result = "Database Error";
		}
		return result;
	}

	public static String UpdateOrderManagementDetail(model.mdlOrderManagementDetail mdlOrderManagementDetail, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			sql = "{call sp_UpdateOrderManagementDetail(?,?,?,?, ?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getOrderManagementDetailID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getOrderManagementID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getStuffingDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getContainerTypeID()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagementDetail.getQuantity()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagementDetail.getPrice()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagementDetail.getUpdatedBy()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOrderManagementDetail", user);

			result = success == true ? "Success Update Order Management Detail" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOrderManagementDetail", sql , user);
			result = "Database Error";
		}
		return result;
	}

	public static String DeleteOrderManagementDetail(String orderManagementID, String orderManagementDetailID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		Boolean allowDelete = AllowDeleteOrderManagementDetail(orderManagementID, user);

		if (allowDelete){
			try{
				sql = "{call sp_DeleteOrderManagementDetail(?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementDetailID));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteOrderManagementDetail", user);

				result = success == true ? "Success Delete Order Management Detail" : "Database Error";
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteOrderManagementDetail", sql , user);
				result = "Database Error";
			}
		}else{
			result = "Only can delete order with order status \"New\"";
		}
		return result;
	}

	public static Boolean AllowDeleteOrderManagementDetail(String orderManagementID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean allow = false;
		CachedRowSet jrs = null;
		Integer orderStatus = null;

		try{
			sql = "{call sp_GetOrderStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "AllowDeleteOrderManagementDetail", user);
			while(jrs.next()){
				orderStatus = jrs.getInt("OrderStatus");
			}
			if (orderStatus == 0){
				allow = true;
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "AllowDeleteOrderManagementDetail", sql , user);
		}

		return allow;
	}

	public static String CreateOrderManagementDetailID(String user){
		String lastOrderManagementDetailID = GetLastOrderManagementDetailID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "000001";

		if ((lastOrderManagementDetailID != null) && (!lastOrderManagementDetailID.equals("")) ){
			String[] partsOrderManagementDetailID = lastOrderManagementDetailID.split("-");
			String partDate = partsOrderManagementDetailID[1];
			String partNumber = partsOrderManagementDetailID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("ORDT-").append(datetime).append("-").append(stringInc);
		String OrderManagementDetailID = sb.toString();
		return OrderManagementDetailID;
	}

	public static String GetLastOrderManagementDetailID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String OrderManagementID = "";

		try {
			sql = "{call sp_GetLastOrderManagementDetailID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastOrderManagementDetailID", user);

			while(jrs.next()){
				OrderManagementID = jrs.getString("OrderManagementDetailID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastOrderManagementDetailID", sql , user);
		}

		return OrderManagementID;
	}

	public static model.mdlOrderManagement GetRateParamFromOrder(String orderID, String user){
		CachedRowSet jrs = null;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		model.mdlOrderManagement orderManagement = new model.mdlOrderManagement();

		try {
			sql = "{call sp_GetRateParamFromOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetRateParamFromOrder", user);

			while(jrs.next()){
				orderManagement.setDistrict(jrs.getString("DistrictID"));
				orderManagement.setTier(jrs.getString("TierID"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetRateParamFromOrder", sql , user);
		}

		return orderManagement;
	}

	public static Integer GetOrderManagementDetailRate(String districtID, String tierID, String containerTypeID, String user){
		CachedRowSet jrs = null;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Integer rate = 0;

		try {
			sql = "{call sp_GetOrderManagementDetailRate(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", districtID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", tierID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", containerTypeID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderManagementDetailRate", user);

			while(jrs.next()){
				Double rateDecimal = jrs.getDouble("Price");
				rate = rateDecimal.intValue();
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetOrderManagementDetailRate", sql , user);
		}

		return rate;
	}

	public static Boolean UpdateOrderTotalPrice(String orderManagementID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call sp_UpdateOrderTotalPrice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOrderTotalPrice", user);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOrderTotalPrice", sql , user);
		}
		return success;
	}
}
