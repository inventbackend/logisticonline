package adapter;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlShipping;

public class ShippingAdapter {

	public static List<model.mdlShipping> LoadShipping (String user) {
		List<model.mdlShipping> listShipping = new ArrayList<model.mdlShipping>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadShipping()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadShipping", user);

			while(jrs.next()){
				model.mdlShipping shipping = new model.mdlShipping();
				shipping.setShippingID(jrs.getString("ShippingID"));
				shipping.setName(jrs.getString("Name"));
				shipping.setAddress(jrs.getString("Address"));
				listShipping.add(shipping);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadShipping", sql , user);
		}

		return listShipping;
	}

	public static model.mdlShipping LoadShippingByID (String lShippingID, String user) {
		model.mdlShipping mdlShipping = new model.mdlShipping();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadShippingByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lShippingID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadShippingByID", user);

			while(jrs.next()){
				mdlShipping.setShippingID(jrs.getString("ShippingID"));
				mdlShipping.setName(jrs.getString("Name"));
				mdlShipping.setAddress(jrs.getString("Address"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadShippingByID", sql , user);
		}

		return mdlShipping;
	}

	public static Boolean checkShippingByName (String lShippingName, String user) {
		Boolean check = false;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_ShippingLoadByName(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lShippingName) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadShippingByName", user);

			while(jrs.next()){
				check = true;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadShippingByName", sql , user);
		}

		return check;
	}
	
	public static String InsertShipping(mdlShipping mdlShipping, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlShipping CheckDuplicateShipping = LoadShippingByID(mdlShipping.getShippingID(),user);
			if(CheckDuplicateShipping.getShippingID() == null){
				String newShippingID = CreateShippingID(user);
				sql = "{call sp_InsertShipping(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newShippingID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlShipping.getName()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlShipping.getAddress()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertShipping",user);

				result = success == true ? "Success Insert Shipping" : "Database Error";
			}
			//if duplicate
			else{
				result = "Shipping sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertShipping", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateShipping(mdlShipping mdlShipping, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateShipping(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlShipping.getShippingID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlShipping.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlShipping.getAddress()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateShipping", user);

			result = success == true ? "Success Update Shipping" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateShipping", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteShipping(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteShipping(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteShipping", user);

			result = success == true ? "Success Delete Shipping" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteShipping", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String DeleteShippingByTerminal(String terminal, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_ShippingDeleteByTerminal(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", terminal));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteShippingByTerminal", user);

			result = success == true ? "Success Delete Shipping" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteShippingByTerminal", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateShippingID(String user){
		String lastShippingID = GetLastShippingID(user);
		String stringIncrement = "0001";

		if ((lastShippingID != null) && (!lastShippingID.equals("")) ){
			String[] partsShippingID = lastShippingID.split("-");
			//get last running number
			String partNumber = partsShippingID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("SHIP-").append(stringIncrement);
		String ShippingID = sb.toString();
		return ShippingID;
	}

	public static String GetLastShippingID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String ShippingID = "";

		try {
			sql = "{call sp_GetLastShippingID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastShippingID", user);

			while(jrs.next()){
				ShippingID = jrs.getString("ShippingID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastShippingID", sql , user);
		}

		return ShippingID;
	}
}