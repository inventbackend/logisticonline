package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlDriverImage;

public class DriverImageAdapter {
	
	public static List<mdlDriverImage> getDriverImageByVendorDetailID (String vendorDetailID, String user) {
		List<mdlDriverImage> listDriverImage = new ArrayList<>();
		mdlDriverImage driverImage = new mdlDriverImage();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_GetDriverImageByVendorDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorDetailID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDriverImageByVendorDetailID", user);

			while(jrs.next()){
				driverImage.setPathImage(jrs.getString("Path"));
				driverImage.setType(jrs.getString("Type"));
				listDriverImage.add(driverImage);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDriverImageByVendorDetailID", sql , user);
		}

		return listDriverImage;
	}


}
