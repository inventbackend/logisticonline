package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlDistrict;

public class DistrictAdapter {
	public static List<model.mdlDistrict> LoadDistrict (String user) {
		List<model.mdlDistrict> listDistrict = new ArrayList<model.mdlDistrict>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadDistrict()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadDistrict", user);

			while(jrs.next()){
				model.mdlDistrict district = new model.mdlDistrict();
				district.setDistrictID(jrs.getString("DistrictID"));
				district.setName(jrs.getString("Name"));
				district.setProvince(jrs.getString("Province"));
				listDistrict.add(district);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDistrict", sql , user);
		}

		return listDistrict;
	}
	
	public static List<model.mdlProvince> LoadProvince (String user) {
		List<model.mdlProvince> listProvince = new ArrayList<model.mdlProvince>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadProvince()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadProvince", user);

			while(jrs.next()){
				model.mdlProvince province = new model.mdlProvince();
				province.setProvinceID(jrs.getString("ProvinceID"));
				province.setProvinceName(jrs.getString("ProvinceName"));
				listProvince.add(province);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadProvince", sql , user);
		}

		return listProvince;
	}

	public static model.mdlDistrict LoadDistrictByID (String lDistrictID, String user) {
		model.mdlDistrict mdlDistrict = new model.mdlDistrict();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadDistrictByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDistrictID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDistrictByID", user);

			while(jrs.next()){
				mdlDistrict.setDistrictID(jrs.getString("DistrictID"));
				mdlDistrict.setName(jrs.getString("Name"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDistrictByID", sql , user);
		}

		return mdlDistrict;
	}

	public static String InsertDistrict(mdlDistrict mdlDistrict, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlDistrict CheckDuplicateDistrict = LoadDistrictByID(mdlDistrict.getDistrictID(), user);
			if(CheckDuplicateDistrict.getDistrictID() == null){
				String newDistrictID = CreateDistrictID(user);
				sql = "{call sp_InsertDistrict(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newDistrictID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDistrict.getName()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDistrict.getProvince()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertDistrict", user);

				result = success == true ? "Success Insert District" : "Database Error";
			}
			//if duplicate
			else{
				result = "District sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertDistrict", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateDistrict(mdlDistrict mdlDistrict, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateDistrict(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDistrict.getDistrictID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDistrict.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDistrict.getProvince()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateDistrict", user);

			result = success == true ? "Success Update District" : "";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateDistrict", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteDistrict(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteDistrict(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteDistrict", user);

			result = success == true ? "Success Delete District" : "";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteDistrict", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateDistrictID(String user){
		String lastDistrictID = GetLastDistrictID(user);
		String stringIncrement = "0001";

		if ((lastDistrictID != null) && (!lastDistrictID.equals("")) ){
			String[] partsDistrictID = lastDistrictID.split("-");
			//get last running number
			String partNumber = partsDistrictID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("DIST-").append(stringIncrement);
		String DistrictID = sb.toString();
		return DistrictID;
	}

	public static String GetLastDistrictID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String DistrictID = "";

		try {
			sql = "{call sp_GetLastDistrictID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastDistrictID", user);

			while(jrs.next()){
				DistrictID = jrs.getString("DistrictID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastDistrictID", sql , user);
		}

		return DistrictID;
	}

	public static List<model.mdlDistrict> LoadDistrictByProvince (String lProvinceID, String user) {
		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_DistrictLoadByProvince(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lProvinceID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDistrictByProvince", user);

			while(jrs.next()){
				model.mdlDistrict district = new model.mdlDistrict();
				district.setDistrictID(jrs.getString("DistrictID"));
				district.setName(jrs.getString("Name"));
				districtList.add(district);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDistrictByProvince", sql , user);
		}

		return districtList;
	}
	
}
