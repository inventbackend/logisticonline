 package adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlDriverImage;

public class DashboardAdapter {

	public static model.mdlDashboard LoadDashboardCustomer1(String lCustomerID) {
		model.mdlDashboard dashboard = new model.mdlDashboard();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			/*sql = "{call sp_DashboardCustomer1(?,?)}"; dashboard box atas untuk summary hari ini saja*/
			sql = "{call sp_DashboardCustomer1_v2(?,?)}"; // dashboard box atas untuk summary hari ini dan pending
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardCustomer1", lCustomerID);

			while(jrs.next()){
				dashboard.setTotalOrder(jrs.getInt("totalOrder"));
				dashboard.setTotalOrderOnGoing(jrs.getInt("totalOnGoingOrder"));
				dashboard.setTotalOrderCompleted(jrs.getInt("totalCompletedOrder"));
				dashboard.setTotalOrderUnProcessed(jrs.getInt("totalUnprocessedOrder"));
				dashboard.setTotalGatePass(jrs.getInt("totalGatePass"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardCustomer1", sql , lCustomerID);
		}

		return dashboard;
	}

	public static model.mdlDashboardDetail LoadDashboardCustomerDetail(String lSI, String lCustomerID) {
		model.mdlDashboardDetail dashboard = new model.mdlDashboardDetail();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardDetailCustomer(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardCustomerDetail", lCustomerID);

			while(jrs.next()){
				dashboard.setCustomerOrderAccepted(jrs.getInt("totalAcceptedOrder"));
				dashboard.setDepoIn(jrs.getInt("totalDepoInOrder"));
				dashboard.setDepoProcess(jrs.getInt("totalOnDepoOrder"));
				dashboard.setFactoryIn(jrs.getInt("totalFactoryInOrder"));
				dashboard.setFactoryProcess(jrs.getInt("totalOnFactoryOrder"));
				dashboard.setDeliveredProcess(jrs.getInt("totalOnPortOrder"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardCustomerDetail", sql , lCustomerID);
		}

		return dashboard;
	}
	
	public static model.mdlDashboardDetail LoadDashboardCustomerPendingDetail(String lSI, String lCustomerID) {
		model.mdlDashboardDetail dashboard = new model.mdlDashboardDetail();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardCustomerPendingDetail(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardCustomerPendingDetail", lCustomerID);

			while(jrs.next()){
				dashboard.setCustomerOrderAccepted(jrs.getInt("totalAcceptedOrder"));
				dashboard.setDepoIn(jrs.getInt("totalDepoInOrder"));
				dashboard.setDepoProcess(jrs.getInt("totalOnDepoOrder"));
				dashboard.setFactoryIn(jrs.getInt("totalFactoryInOrder"));
				dashboard.setFactoryProcess(jrs.getInt("totalOnFactoryOrder"));
				dashboard.setDeliveredProcess(jrs.getInt("totalOnPortOrder"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardCustomerPendingDetail", sql , lCustomerID);
		}

		return dashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardCustomerMap(String lCustomerID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardCustomerMap(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardCustomerMap", lCustomerID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				dashboard.setTrackingDate(ConvertDateTimeHelper.formatDate(jrs.getString("TrackingDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss") );
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setVendorName(jrs.getString(4));
				dashboard.setDriverName(jrs.getString("DriverName"));
				if(jrs.getInt("VendorDetailStatus") == 0)
					dashboard.setDriverDeliveryStatus("Assigned");
				else if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				dashboard.setFactoryName(jrs.getString(12));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setPortName(jrs.getString("PortName")+" - "+jrs.getString("PortUTC"));
				dashboard.setDepoName(jrs.getString(16));
				dashboard.setOrderType(jrs.getString("OrderType"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardCustomerMap", sql , lCustomerID);
		}

		return listDashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardCustomerDetailMap(String lSI, String lCustomerID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardCustomerDetailMap(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardCustomerDetailMap", lCustomerID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				dashboard.setTrackingDate(ConvertDateTimeHelper.formatDate(jrs.getString("TrackingDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss") );
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setVendorName(jrs.getString(4));
				dashboard.setDriverName(jrs.getString("DriverName"));
				if(jrs.getInt("VendorDetailStatus") == 0)
					dashboard.setDriverDeliveryStatus("Assigned");
				else if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				dashboard.setFactoryName(jrs.getString(12));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setPortName(jrs.getString("PortName")+" - "+jrs.getString("PortUTC"));
				dashboard.setDepoName(jrs.getString(16));
				dashboard.setOrderType(jrs.getString("OrderType"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardCustomerDetailMap", sql , lCustomerID);
		}

		return listDashboard;
	}
	
	public static model.mdlDashboard LoadDashboardVendor1(String lVendorID) {
		model.mdlDashboard dashboard = new model.mdlDashboard();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			//get available order by vendor tier
			//sql = "{call sp_DashboardVendor1(?,?)}"; 
			
			//get available order by vendor tier and vendor mapping
			sql = "{call sp_DashboardVendor1_v2(?,?)}"; 
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardVendor1", lVendorID);

			while(jrs.next()){
				dashboard.setTotalOrder(jrs.getInt("totalOrder"));
				
				if(jrs.getString("totalAvailableOrder") == null)
					dashboard.setAvailableOrder(0);
				else
					dashboard.setAvailableOrder(jrs.getInt("totalAvailableOrder"));
				
				dashboard.setTotalOrderOnGoing(jrs.getInt("totalOnGoingOrder"));
				dashboard.setTotalOrderCompleted(jrs.getInt("totalCompletedOrder"));
				dashboard.setTotalGatePass(jrs.getInt("totalGatePass"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardVendor1", sql , lVendorID);
		}

		return dashboard;
	}
	
	public static model.mdlDashboard LoadDashboard2(String lRoleID, String lUserID) {
		model.mdlDashboard dashboard = new model.mdlDashboard();
		dashboard.setAvailableOrder(0);
		dashboard.setTotalOrder(0);
		dashboard.setPickedOrder(0);
		dashboard.setUnpickedOrder(0);
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		//get next day date
		/*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 1);
        Date currentDatePlusOne = c.getTime();*/
		
		try {
			/*sql = "{call sp_Dashboard2(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateFormat.format(currentDatePlusOne).toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateFormat.format(currentDatePlusOne).toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID ));*/
			
			sql = "{call sp_OrderManagementDashboard(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, lUserID+" - LoadOrderManagementDashboard", lUserID);

			while(jrs.next()){
				if(lRoleID.contentEquals("R003"))
					dashboard.setAvailableOrder(jrs.getInt(1));
				else{
					dashboard.setTotalOrder(jrs.getInt(1));
					dashboard.setPickedOrder(jrs.getInt(2));
					dashboard.setUnpickedOrder(jrs.getInt(1) - jrs.getInt(2));
				}
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadOrderManagementDashboard", sql , lUserID);
		}

		return dashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardVendorMap(String lVendorID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardVendorMap(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardVendorMap", lVendorID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				dashboard.setTrackingDate(ConvertDateTimeHelper.formatDate(jrs.getString("TrackingDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss") );
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setDriverName(jrs.getString("DriverName"));
				if(jrs.getInt("VendorDetailStatus") == 0)
					dashboard.setDriverDeliveryStatus("Assigned");
				else if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 4)
					dashboard.setDriverDeliveryStatus("Delivered");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				dashboard.setFactoryName(jrs.getString(10));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setPortName(jrs.getString("PortName")+" - "+jrs.getString("PortUTC"));
				dashboard.setDepoName(jrs.getString(14));
				dashboard.setOrderType(jrs.getString("OrderType"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardVendorMap", sql , lVendorID);
		}

		return listDashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardList(String lRoleID, String lUserID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardList(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardList", lUserID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setDriverName(jrs.getString("DriverName"));
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setIsActiveMap(true);
				if(jrs.getInt("VendorDetailStatus") == 0){
					dashboard.setDriverDeliveryStatus("Assigned");
					dashboard.setIsActiveMap(false);
				}
				if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				else if(jrs.getInt("VendorDetailStatus") == 4){
					dashboard.setDriverDeliveryStatus("Delivered");
					if(lRoleID.equals("R002"))
						dashboard.setIsActiveMap(false);
				}
				dashboard.setContainerNumber(jrs.getString("ContainerNumber"));
				dashboard.setSealNumber(jrs.getString("SealNumber"));
				dashboard.setYellowCard(jrs.getString("YellowCard"));
				if(lRoleID.equals("R002"))
					dashboard.setVendorName(jrs.getString(8));
				else
					dashboard.setCustomerName(jrs.getString(8));
				dashboard.setFactoryName(jrs.getString(9));
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setPeb(jrs.getString("PEB"));
				dashboard.setNpe(jrs.getString("NPE"));
				dashboard.setOrderType(jrs.getString("OrderType"));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setUserID(lUserID);
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardList", sql , lUserID);
		}

		return listDashboard;
	}

	public static List<model.mdlDashboard> LoadDashboardBawah(String lRoleID, String lUserID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardBawah(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardBawah", lUserID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setPartaiPerSI(jrs.getInt("Quantity"));
				dashboard.setPartaiSelesaiPerSI(jrs.getInt(5));
				if(lRoleID.equals("R002")){
					dashboard.setPartaiProcessPerSI(jrs.getInt(6));
					dashboard.setPartaiUnprocessPerSI(jrs.getInt(7));
				}
				dashboard.setVendorOrderID(jrs.getString("VendorOrderID"));
				dashboard.setContainerType(jrs.getString("Description"));
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardBawah", sql , lUserID);
		}

		return listDashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardPending(String lRoleID, String lUserID) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardPending(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardPending", lUserID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setPartaiPerSI(jrs.getInt("Quantity"));
				dashboard.setPartaiSelesaiPerSI(jrs.getInt(5));
				dashboard.setPartaiProcessPerSI(jrs.getInt(6));
				dashboard.setPartaiUnprocessPerSI(jrs.getInt(7));
				dashboard.setVendorOrderID(jrs.getString("VendorOrderID"));
				dashboard.setContainerType(jrs.getString("Description"));
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardPending", sql , lUserID);
		}

		return listDashboard;
	}
	
	//param lsi di sini juga sebagai lvendor order
	public static List<model.mdlDashboard> LoadDashboardDetailList(String lRoleID, String lUserID, String lSI) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardBawahDetail(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardDetailList", lUserID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setDriverName(jrs.getString("DriverName"));
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setIsActiveMap(true);
				if(jrs.getInt("VendorDetailStatus") == 0){
					dashboard.setDriverDeliveryStatus("Assigned");
					dashboard.setIsActiveMap(false);
				}
				if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				else if(jrs.getInt("VendorDetailStatus") == 4){
					dashboard.setDriverDeliveryStatus("Delivered");
					if(lRoleID.equals("R002"))
						dashboard.setIsActiveMap(false);
				}
				dashboard.setContainerNumber(jrs.getString("ContainerNumber"));
				dashboard.setSealNumber(jrs.getString("SealNumber"));
				dashboard.setYellowCard(jrs.getString("YellowCard"));
				if(lRoleID.equals("R002"))
					dashboard.setVendorName(jrs.getString(8));
				else
					dashboard.setCustomerName(jrs.getString(8));
				dashboard.setFactoryName(jrs.getString(9));
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setPeb(jrs.getString("PEB"));
				dashboard.setNpe(jrs.getString("NPE"));
				dashboard.setOrderType(jrs.getString("OrderType"));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setUserID(lUserID);
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				dashboard.setDriverOrderAssignmentID(jrs.getString("VendorOrderDetailID"));
				dashboard.setStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMMM yyyy"));
				dashboard.setTrackingDate(ConvertDateTimeHelper.formatDate(jrs.getString("TrackingDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss") );
				dashboard.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				dashboard.setPortName(jrs.getString("PortName")+" - "+jrs.getString("PortUTC"));
				dashboard.setDepoName(jrs.getString(23));
				dashboard.setTpsShpLocationID(jrs.getString("Tps_ShpContainerLocationID"));
				dashboard.setTpsShpLocation(jrs.getString("Tps_ShpContainerLocation"));
				dashboard.setTpsShpMovementDate(jrs.getString("Tps_ShpContainerMovementDate"));
				if(jrs.getString("Tps_EirLink") == null)
					dashboard.setTpsEir("");
				else
					dashboard.setTpsEir(jrs.getString("Tps_EirLink"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardDetailList", sql , lUserID);
		}

		return listDashboard;
	}
	
	public static List<model.mdlDashboard> LoadDashboardPendingDetailList(String lRoleID, String lUserID, String lSI) {
		List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardBawahPendingDetail(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI ));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardPendingDetailList", lUserID);

			while(jrs.next()){
				model.mdlDashboard dashboard = new model.mdlDashboard();
				dashboard.setDriverName(jrs.getString("DriverName"));
				dashboard.setVehicleNumber(jrs.getString("VehicleNumber"));
				dashboard.setIsActiveMap(true);
				if(jrs.getInt("VendorDetailStatus") == 0){
					dashboard.setDriverDeliveryStatus("Assigned");
					dashboard.setIsActiveMap(false);
				}
				if(jrs.getInt("VendorDetailStatus") == 1)
					dashboard.setDriverDeliveryStatus("On Process");
				else if(jrs.getInt("VendorDetailStatus") == 2)
					dashboard.setDriverDeliveryStatus("On Delivery");
				else if(jrs.getInt("VendorDetailStatus") == 3)
					dashboard.setDriverDeliveryStatus("On Port");
				else if(jrs.getInt("VendorDetailStatus") == 5)
					dashboard.setDriverDeliveryStatus("On Factory");
				else if(jrs.getInt("VendorDetailStatus") == 6)
					dashboard.setDriverDeliveryStatus("On Depo");
				else if(jrs.getInt("VendorDetailStatus") == 7)
					dashboard.setDriverDeliveryStatus("Depo Checkout");
				else if(jrs.getInt("VendorDetailStatus") == 4){
					dashboard.setDriverDeliveryStatus("Delivered");
					if(lRoleID.equals("R002"))
						dashboard.setIsActiveMap(false);
				}
				dashboard.setContainerNumber(jrs.getString("ContainerNumber"));
				dashboard.setSealNumber(jrs.getString("SealNumber"));
				dashboard.setYellowCard(jrs.getString("YellowCard"));
				if(lRoleID.equals("R002"))
					dashboard.setVendorName(jrs.getString(8));
				else
					dashboard.setCustomerName(jrs.getString(8));
				dashboard.setFactoryName(jrs.getString(9));
				dashboard.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				dashboard.setPeb(jrs.getString("PEB"));
				dashboard.setNpe(jrs.getString("NPE"));
				dashboard.setOrderType(jrs.getString("OrderType"));
				dashboard.setItemDescription(jrs.getString("ItemDescription"));
				dashboard.setUserID(lUserID);
				dashboard.setLatitude(jrs.getString("Latitude"));
				dashboard.setLongitude(jrs.getString("Longitude"));
				dashboard.setDriverOrderAssignmentID(jrs.getString("VendorOrderDetailID"));
				dashboard.setStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMMM yyyy"));
				dashboard.setTrackingDate(ConvertDateTimeHelper.formatDate(jrs.getString("TrackingDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss") );
				dashboard.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				dashboard.setPortName(jrs.getString("PortName")+" - "+jrs.getString("PortUTC"));
				dashboard.setDepoName(jrs.getString(23));
				List<mdlDriverImage> jsonDriverImage = DriverImageAdapter.getDriverImageByVendorDetailID(jrs.getString("VendorOrderDetailID"), lUserID);
				String json = new Gson().toJson(jsonDriverImage);
				dashboard.setJsonDriverImage(json);
				dashboard.setTpsShpLocationID(jrs.getString("Tps_ShpContainerLocationID"));
				dashboard.setTpsShpLocation(jrs.getString("Tps_ShpContainerLocation"));
				dashboard.setTpsShpMovementDate(jrs.getString("Tps_ShpContainerMovementDate"));
				if(jrs.getString("Tps_EirLink") == null)
					dashboard.setTpsEir("");
				else
					dashboard.setTpsEir(jrs.getString("Tps_EirLink"));
				
				listDashboard.add(dashboard);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardPendingDetailList", sql , lUserID);
		}

		return listDashboard;
	}
	
	public static Integer LoadTotalOrderPerSI(String lSI, String lCustomerID) {
		Integer total = 0;
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_OrderDetailGetTotalQty(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lSI));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTotalOrderPerSI", lCustomerID);

			while(jrs.next()){
				total = jrs.getInt(1);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTotalOrderPerSI", sql , lCustomerID);
		}

		return total;
	}
	
	public static model.mdlDashboardDetail LoadDashboardVendorDetail(String lVendorOrderID, String lVendorID) {
		model.mdlDashboardDetail dashboard = new model.mdlDashboardDetail();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DashboardDetailVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorOrderID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDashboardVendorDetail", lVendorID);

			while(jrs.next()){
				dashboard.setVendorOrderPicked(jrs.getInt("totalAcceptedOrder"));
				dashboard.setDepoIn(jrs.getInt("totalDepoInOrder"));
				dashboard.setDepoProcess(jrs.getInt("totalOnDepoOrder"));
				dashboard.setFactoryProcess(jrs.getInt("totalOnFactoryOrder"));
				dashboard.setDeliveredProcess(jrs.getInt("totalOnPortOrder"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDashboardVendorDetail", sql , lVendorID);
		}

		return dashboard;
	}
	
}
