package adapter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertDateTimeHelper {

	public static String formatDate (String date, String initDateFormat, String endDateFormat) {
		
		String parsedDate ="";
		
		try {
	    Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
	    SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
	    parsedDate = formatter.format(initDate);
		}
		catch(Exception ex)
		{
			
		}
		
		return parsedDate;
	}
	
}
