package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;

import org.apache.log4j.Logger;

import com.sun.rowset.JdbcRowSetImpl;
import java.util.Date;
import model.Globals;

/** Documentation
 * 
 */
public class LogAdapter {
	final static Logger logger = Logger.getLogger(LogAdapter.class);
	
	public static void InsertLogExc(String lException,String lKey,String lQuery,String lCreated_by)
	{
		JdbcRowSet jrs = null;
		String result = "";
		if(lCreated_by==null)
			lCreated_by="";
		
		try 
		{	
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date Created_On = new Date();
			String lCreated_On = df.format(Created_On);
			
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			
			jrs.setCommand("SELECT * FROM log_exception LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("Exception",lException);
			jrs.updateString("Query", lQuery);
			jrs.updateString("Key", lKey);
			jrs.updateString("Created_on", lCreated_On);
			jrs.updateString("Created_by", lCreated_by);
			
			jrs.insertRow();
			
			result = "Success insert log";
		}
		catch (Exception ex){
			logger.error("FAILED. Function : "+lKey+", Query : "+lQuery+", CreatedBy:" + lCreated_by + ", Exception : " + ex.toString(), ex);
		}
		finally {
			try{
				if(jrs!=null)
					jrs.close();
			}
			catch(Exception e) {
				logger.error("FAILED. Function : "+lKey+", Query : close opened connection protocol, CreatedBy:" + lCreated_by + ", Exception : " + e.toString(), e);
			}
		}

		return;
	}
	
}
