package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlReimburse;
import model.mdlTOP;

public class ReimburseAdapter {
	
	public static List<mdlReimburse> LoadReimburseAll (String user) {
		List<mdlReimburse> listReimburse = new ArrayList<mdlReimburse>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadReimburse()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadReimburse", user);

			while(jrs.next()){
				mdlReimburse rem = new mdlReimburse();
				rem.setReimburseID(jrs.getString("ReimburseID"));
				rem.setName(jrs.getString("Name"));
				rem.setDescription(jrs.getString("Description"));
				listReimburse.add(rem);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTOP", sql , user);
		}

		return listReimburse;
	}
	
	public static String DeleteReimburse(String reimburseID, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteReimburse(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", reimburseID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteReimburse", user);

			result = success == true ? "Success Delete Reimburse" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteReimburse", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateReimburse(mdlReimburse mdlReimburse, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateReimburse(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlReimburse.getReimburseID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlReimburse.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("String", mdlReimburse.getDescription()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateReimburse", user);

			result = success == true ? "Success Update Reimburse" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTOP", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static mdlReimburse LoadReimburseByID (String lReimburseID, String user) {
		mdlReimburse mdlReimburse = new mdlReimburse();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadReimburseByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lReimburseID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadReimburseByID", user);

			while(jrs.next()){
				mdlReimburse.setReimburseID(jrs.getString("ReimburseID"));
				mdlReimburse.setName(jrs.getString("Name"));
				mdlReimburse.setDescription(jrs.getString("Description"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadReimburseByID", sql , user);
		}

		return mdlReimburse;
	}
	
	public static String InsertReimburse(mdlReimburse mdlReimburse, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlReimburse CheckDuplicateReimburse = LoadReimburseByID(mdlReimburse.getReimburseID(),user);
			if(CheckDuplicateReimburse.getReimburseID() == null){
				String newReimburseID = CreateReimburseID(user);
				sql = "{call sp_InsertReimburse(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newReimburseID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlReimburse.getName()));
				listParam.add(QueryExecuteAdapter.QueryParam("String", mdlReimburse.getDescription()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertReimburse", user);

				result = success == true ? "Success Insert Reimburse" : "Database Error";
			}
			//if duplicate
			else{
				result = "Reimburse sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertReimburse", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	
	public static String CreateReimburseID(String user){
		String lastReimburseID = GetLastReimburseID(user);
		String stringIncrement = "0001";

		if ((lastReimburseID != null) && (!lastReimburseID.equals("")) ){
			String[] partsReimburseID = lastReimburseID.split("-");
			//get last running number
			String partNumber = partsReimburseID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("REIM-").append(stringIncrement);
		String ReimburseID = sb.toString();
		return ReimburseID;
	}

	public static String GetLastReimburseID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String TOPID = "";

		try {
			sql = "{call sp_GetLastReimburseID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastReimburseID", user);

			while(jrs.next()){
				TOPID = jrs.getString("ReimburseID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastReimburseID", sql , user);
		}

		return TOPID;
	}

}
