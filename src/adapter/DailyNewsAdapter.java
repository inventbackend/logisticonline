package adapter;

import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.Part;
import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;

public class DailyNewsAdapter {
	public static List<model.mdlDailyNews> LoadNews(String vendorID){
		List<model.mdlDailyNews> listNews = new ArrayList<model.mdlDailyNews>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadNews(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadNews", vendorID);

			while(jrs.next()){
				model.mdlDailyNews mdlDailyNews = new model.mdlDailyNews();
				mdlDailyNews.setNewsID(jrs.getString("NewsID"));
				mdlDailyNews.setStartDateValid(ConvertDateTimeHelper.formatDate(jrs.getString("StartDateValid"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlDailyNews.setEndDateValid(ConvertDateTimeHelper.formatDate(jrs.getString("EndDateValid"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlDailyNews.setVendorID(jrs.getString("VendorID"));
				mdlDailyNews.setNewsText(jrs.getString("NewsText"));
				mdlDailyNews.setNewsImage(jrs.getString("NewsImage"));
				listNews.add(mdlDailyNews);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNews", sql , vendorID);
		}
		return listNews;
	}

	public static List<model.mdlDailyNewsDetail> LoadNewsDetail(String newsID, String user){
		List<model.mdlDailyNewsDetail> listNewsDetail = new ArrayList<model.mdlDailyNewsDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadNewsDetail(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", newsID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadNewsDetail", user);

			while(jrs.next()){
				model.mdlDailyNewsDetail mdlDailyNewsDetail = new model.mdlDailyNewsDetail();
				mdlDailyNewsDetail.setNewsID(jrs.getString("NewsID"));
				mdlDailyNewsDetail.setDriverID(jrs.getString("DriverID"));
				mdlDailyNewsDetail.setDriverName(jrs.getString("DriverName"));
				listNewsDetail.add(mdlDailyNewsDetail);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadNewsDetail", sql , user);
		}
		return listNewsDetail;
	}

	public static String InsertNews(model.mdlDailyNews lParam, String[] listDriver)
	{
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";
		try{
			//store 'InsertNews' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_InsertNews(?,?,?,?,?, ?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getStartDateValid()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getEndDateValid()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsText()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsImage()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			if (listDriver.length != 0){
				for (String driver : listDriver){
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_InsertNewsDetail(?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", driver));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				}
			}

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertNews", lParam.getVendorID());
			result = success == true ? "Success Insert Daily News" : "Database Error";
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertVendorOrder", sql , lParam.getVendorID());
			result = "Database error";
		}

		return result;
	}

	public static String UpdateNews(model.mdlDailyNews lParam, String[] listDriver, Boolean updateImage)
	{
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{
			//store 'InsertNews' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UpdateNews(?,?,?,?,?, ?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getStartDateValid()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getEndDateValid()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsText()));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			if (updateImage){
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_UpdateNewsImage(?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsImage()));
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
			}

			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteNewsDetail(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			if (listDriver.length != 0){
				for (String driver : listDriver){
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_InsertNewsDetail(?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getNewsID()));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", driver));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				}
			}

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "UpdateNews", lParam.getVendorID());
			result = success == true ? "Success Update Daily News" : "Database Error";
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertVendorOrder", sql , lParam.getVendorID());
			result = "Database error";
		}

		return result;
	}

	public static String DeleteNews(String newsID, String user){
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{

			//store 'DeleteNews' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteNews(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", newsID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "DeleteNews", user);
			result = success == true ? "Success Delete Daily News" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteNews", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static void DeleteNewsImage(String newsID, String paramFilePath, String paramSeparator, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_DeleteNewsImage(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", newsID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "DeleteNewsImage", user);

			while(jrs.next()){
				String newsImage = jrs.getString("NewsImage");
				Path filePath = Paths.get(newsImage);
				String filename = filePath.getFileName().toString();
				String newFile = paramFilePath + "News" + paramSeparator + newsID + "_" + filename;
				WriteFileAdapter.Delete(newFile);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteNewsImage", sql , user);
		}
	}

	public static String CreateNewsID(String user){
		String lastNewsID = GetLastNewsID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "000001";

		if ((lastNewsID != null) && (!lastNewsID.equals("")) ){
			String[] partsNewsID = lastNewsID.split("-");
			String partDate = partsNewsID[1];
			String partNumber = partsNewsID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("NEWS-").append(datetime).append("-").append(stringInc);
		String newsID = sb.toString();
		return newsID;
	}

	public static String GetLastNewsID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String newsID = "";
		try{
			sql = "{call sp_GetLastNewsID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "GetLastNewsID", user);

			while(rowset.next())
			{
				newsID = rowset.getString("NewsID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetLastNewsID", sql, user);
		}

		return newsID;
	}
}
