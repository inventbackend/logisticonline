package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlContainerType;
import model.mdlCustomer;
import model.mdlOnbehalfNpwp;

public class CustomerAdapter {

	public static List<model.mdlCustomer> LoadCustomer(String user) {
		List<model.mdlCustomer> listCustomer = new ArrayList<model.mdlCustomer>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadCustomer()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadCustomer", user);

			while(jrs.next()){
				model.mdlCustomer customer = new model.mdlCustomer();
				customer.setCustomerID(jrs.getString("CustomerID"));
				customer.setCustomerName(jrs.getString(2));
				customer.setPIC1(jrs.getString("PIC1"));
				customer.setPIC2(jrs.getString("PIC2"));
				customer.setPIC3(jrs.getString("PIC3"));
				customer.setCustomerAddress(jrs.getString("Address"));
				customer.setEmail(jrs.getString("Email"));
				customer.setOfficePhone(jrs.getString("OfficePhone"));
				customer.setMobilePhone(jrs.getString("MobilePhone"));
				customer.setNPWP(jrs.getString("NPWP"));
				customer.setDomicile(jrs.getString("Domicile"));
				customer.setTDP(jrs.getString("TDP"));
				customer.setDistrictID(jrs.getString("DistrictID"));
				customer.setDistrict(jrs.getString(21));
				customer.setBillingAddress(jrs.getString("BillingAddress"));
				customer.setTOPID(jrs.getString("TOPID"));
				customer.setTOP(jrs.getString(22));
				customer.setiCreditLimit(Integer.parseInt(jrs.getString("CreditLimit").replace(".00","")));
				customer.setBankName(jrs.getString("BankName"));
				customer.setBankAcc(jrs.getString("BankAccount"));
				customer.setBankBranch(jrs.getString("BankBranch"));
				customer.setPostalCode(jrs.getString("PostalCode"));
				customer.setPpn(jrs.getInt("PPN"));
				customer.setPph23(jrs.getInt("PPh23"));
				customer.setProductInvoice(jrs.getString("ProductInvoice"));
				customer.setCustomClearance((int) jrs.getDouble("CustomClearance"));
				listCustomer.add(customer);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomer", sql , user);
		}

		return listCustomer;
	}

	public static List<model.mdlCustomer> LoadCustomerByID(String lCustomerID) {
		List<model.mdlCustomer> listCustomer = new ArrayList<model.mdlCustomer>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_LoadCustomerByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadCustomerByID", lCustomerID);

			while(jrs.next()){
				model.mdlCustomer customer = new model.mdlCustomer();
				customer.setCustomerID(jrs.getString("CustomerID"));
				customer.setCustomerName(jrs.getString(2));
				customer.setPIC1(jrs.getString("PIC1"));
				customer.setPIC2(jrs.getString("PIC2"));
				customer.setPIC3(jrs.getString("PIC3"));
				customer.setCustomerAddress(jrs.getString("Address"));
				customer.setEmail(jrs.getString("Email"));
				customer.setOfficePhone(jrs.getString("OfficePhone"));
				customer.setMobilePhone(jrs.getString("MobilePhone"));
				customer.setNPWP(jrs.getString("NPWP"));
				customer.setDomicile(jrs.getString("Domicile"));
				customer.setTDP(jrs.getString("TDP"));
				customer.setDistrict(jrs.getString("DistrictID"));
				customer.setDistrict(jrs.getString(21));
				customer.setBillingAddress(jrs.getString("BillingAddress"));
				customer.setTOPID(jrs.getString("TOPID"));
				customer.setTOP(jrs.getString(22));
				customer.setiCreditLimit(Integer.parseInt(jrs.getString("CreditLimit").replace(".00","")));
				customer.setBankName(jrs.getString("BankName"));
				customer.setBankAcc(jrs.getString("BankAccount"));
				customer.setBankBranch(jrs.getString("BankBranch"));
				customer.setPostalCode(jrs.getString("PostalCode"));
				customer.setIsFirstLogin(jrs.getBoolean("IsFirstLogin"));
				customer.setPpn(jrs.getInt("PPN"));
				customer.setPph23(jrs.getInt("PPh23"));
				customer.setProductInvoice(jrs.getString("ProductInvoice"));
				customer.setCustomClearance((int) jrs.getDouble("CustomClearance"));
				listCustomer.add(customer);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadCustomerByID", sql , lCustomerID);
		}

		return listCustomer;
	}

	public static String CreateCustomerID(String user){
		String lastCustomerID = getLastCustomerID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "0001";

		if ((lastCustomerID != null) && (!lastCustomerID.equals("")) ){
			String[] partsCustomerID = lastCustomerID.split("-");
			String partDate = partsCustomerID[1];
			String partNumber = partsCustomerID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("CUST-").append(datetime).append("-").append(stringInc);
		String CustomerID = sb.toString();
		return CustomerID;
	}

	public static String getLastCustomerID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String CustomerID = "";
		try{
			sql = "{call sp_getLastCustomerID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastCustomerID", user);

			while(rowset.next())
			{
				CustomerID = rowset.getString("CustomerID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastCustomerID", sql, user);
		}

		return CustomerID;
	}

	public static String RegisterCustomer(model.mdlCustomer lParam)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean check = false;
		String lResult = "";

		try{
			sql = "{call sp_CheckUsername(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckUsername", lParam.getEmail());
			//Globals.gCommand = sql;

			while(rowset.next()){
				check = true;
			}

			//if no duplicate username
			if(check==false)
			{
				Boolean success = false;
				List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
				model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();

				//store 'InsertCustomer' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertCustomer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerName));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC1));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC2));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC3));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerAddress));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Domicile));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TDP));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.District));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BillingAddress));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TOP));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("double", lParam.CreditLimit));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAcc));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.ppn));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.pph23));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.productInvoice));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.customClearance));
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);

				//store 'InsertUser' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertUser(?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", "R002"));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);

				success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "RegisterCustomer", lParam.getEmail());
				if(success)
					lResult = "Success Register Customer";
				else
					lResult = "Database error";
			}
			//if duplicate
			else {
				lResult = "The username already exist";
			}
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "RegisterCustomer", sql , lParam.getEmail());
	        lResult = "Database error";
		}

		return lResult;
	}

	public static String InsertCustomer(mdlCustomer lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertCustomer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC1));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC2));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC3));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Domicile));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TDP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.District));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BillingAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TOP));
			listParam.add(QueryExecuteAdapter.QueryParam("double", lParam.CreditLimit));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAcc));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));
			listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.ppn));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.pph23));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.productInvoice));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.customClearance));
			

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertCustomer", user);

			result = success == true ? "Success Insert Customer" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomer", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateCustomer(mdlCustomer lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateCustomer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC1));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC2));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.PIC3));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Email));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.OfficePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.MobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.NPWP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Domicile));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TDP));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.District));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BillingAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TOP));
			listParam.add(QueryExecuteAdapter.QueryParam("double", lParam.CreditLimit));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankAcc));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.BankBranch));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.postalCode));
			listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.ppn));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.pph23));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.productInvoice));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.customClearance));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateCustomer", user);

			result = success == true ? "Success Update Customer" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomer", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteCustomer(String lCustomerID, String user)
	{
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{
			//store 'DeleteFactory' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteFactoryByID(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			//store 'DeleteCustomer' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteCustomer(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			//store 'DeleteUser' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteUserByID(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "DeleteCustomer", user);
			if(success)
				result = "Success Delete Customer";
			else
				result = "Database error";
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "DeleteCustomer", sql , user);
			result = "Database error";
		}

		return result;
	}

	public static model.mdlCustomer LoadSingleCustomer(String CustomerID){
		model.mdlCustomer customer = new model.mdlCustomer();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		try {
			sql = "{call sp_LoadCustomerByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadSingleCustomer", CustomerID);
			while(jrs.next()){
				customer.setCustomerID(jrs.getString("CustomerID"));
				customer.setCustomerName(jrs.getString(2));
				customer.setPIC1(jrs.getString("PIC1"));
				customer.setPIC2(jrs.getString("PIC2"));
				customer.setPIC3(jrs.getString("PIC3"));
				customer.setCustomerAddress(jrs.getString("Address"));
				customer.setEmail(jrs.getString("Email"));
				customer.setOfficePhone(jrs.getString("OfficePhone"));
				customer.setMobilePhone(jrs.getString("MobilePhone"));
				customer.setNPWP(jrs.getString("NPWP"));
				customer.setDomicile(jrs.getString("Domicile"));
				customer.setTDP(jrs.getString("TDP"));
				customer.setDistrict(jrs.getString("DistrictID"));
				customer.setDistrict(jrs.getString(21));
				customer.setBillingAddress(jrs.getString("BillingAddress"));
				customer.setTOPID(jrs.getString("TOPID"));
				customer.setTOP(jrs.getString(22));
				customer.setiCreditLimit(Integer.parseInt(jrs.getString("CreditLimit").replace(".00","")));
				customer.setBankName(jrs.getString("BankName"));
				customer.setBankAcc(jrs.getString("BankAccount"));
				customer.setBankBranch(jrs.getString("BankBranch"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadSingleCustomer", sql , CustomerID);
		}
		return customer;
	}

	public static List<model.mdlCustomer> LoadUnregisteredCustomer(String user) {
		List<model.mdlCustomer> listCustomer = new ArrayList<model.mdlCustomer>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_LoadUnregisteredCustomer()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadUnregisteredCustomer", user);

			while(jrs.next()){
				model.mdlCustomer customer = new model.mdlCustomer();
				customer.setCustomerID(jrs.getString("CustomerID"));
				customer.setCustomerName(jrs.getString("Name"));
				customer.setCustomerAddress(jrs.getString("Address"));
				listCustomer.add(customer);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadUnregisteredCustomer", sql , user);
		}

		return listCustomer;
	}

	public static List<model.mdlOnbehalfNpwp> LoadOnbehalfNpwp(String lCustomerID, String lUser) {
		List<model.mdlOnbehalfNpwp> listData = new ArrayList<model.mdlOnbehalfNpwp>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_OnbehalfNpwpLoadByCustomer(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCustomerID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadOnbehalfNpwp", lUser);

			while(jrs.next()){
				model.mdlOnbehalfNpwp data = new model.mdlOnbehalfNpwp();
				data.setCustomerID(jrs.getString("CustomerID"));
				data.setCustomerName(jrs.getString("Name"));
				data.setOnbehalfNpwp(jrs.getString("Onbehalf_NPWP"));
				data.setOnbehalfCustomer(jrs.getString("Onbehalf"));
				listData.add(data);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadOnbehalfNpwp", sql , lUser);
		}

		return listData;
	}
	
	public static String InsertOnbehalfNpwp(mdlOnbehalfNpwp lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_OnbehalfNpwpInsert(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.onbehalfNpwp));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.onbehalfCustomer));
			

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertOnbehalfNpwp", user);

			result = success == true ? "Success Insert Npwp" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertOnbehalfNpwp", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateOnbehalfNpwp(mdlOnbehalfNpwp lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_OnbehalfNpwpUpdate(?,?,?,?)}";
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.onbehalfNpwp));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.onbehalfCustomer));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.previousNpwp));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOnbehalfNpwp", user);

			result = success == true ? "Success Update Npwp" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOnbehalfNpwp", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
}
