package adapter;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import model.mdlDeliveryOrder;
import model.mdlOrderManagement;
import model.mdlVessel;
import modelKojaGetFD.mdlKojaFD;
import modelKojaGetOnDemand.mdlGetOnDemandParam;
import modelKojaGetVesselVoyage.mdlKojaVesselVoyage;

public class KojaAdapter {

	public static String UpdateSession(String sessionKoja, String custID, String webUser){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_KojaSessionUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", sessionKoja ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", custID ));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateKojaSession", webUser);
			
			result = success == true ? "Update Koja Session Success" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateKojaSession", sql , webUser);
			result = "Database Error";
		}

		return result;
	}
	
	public static void DeleteVesselVoyage(String terminal, String webUser){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		/*Boolean success = false;
		String result = "";*/

		try{
			sql = "{call sp_VesselVoyageDelete(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", terminal ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteVesselVoyage", webUser);
			
			/*result = success == true ? "Update Koja Session Success" : "Database Error";*/
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteVesselVoyage", sql , webUser);
			/*result = "Database Error";*/
		}

		return;
	}
	
	public static void InsertVesselVoyage(mdlVessel vesselParam, String webUser){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		/*Boolean success = false;
		String result = "";*/

		try{
			sql = "{call sp_VesselVoyageInsert(?,?,?,?,?, ?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.terminal ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.vesselCode ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.vesselName ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.voyageCode ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.voyageStatus ));
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.voyageCompany ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.podCode ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.podName ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.voyageArrival ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", vesselParam.voyageDeparture ));
			QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVesselVoyage", webUser);
			
			/*result = success == true ? "Update Koja Session Success" : "Database Error";*/
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVesselVoyage", sql , webUser);
			/*result = "Database Error";*/
		}

		return;
	}
	
	public static void InsertVesselVoyage2(mdlKojaVesselVoyage param, String webUser){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		String[] listVesselCode = param.getVOYID_VESSEL_CODE();
		String[] listVesselName = param.getVESSEL_NAME();
		String[] listVoyageCode = param.getVOYID_VOYAGE_CODE();
		String[] listVoyageStatus = param.getVOYAGE_STATUS();
		String[] listVoyageCompany = param.getVOYID_COMPANY_CODE();
		String[] listPodCode = param.getPOD();
		String[] listPodName = param.getPOD_NAME();
		String[] listArrivalDate = param.getARRIVAL_DATETIME();
		String[] listDepartureDate = param.getDEPARTURE_DATETIME();
		Integer[] listExportLimit = param.getEXPORT_LIMIT();
		String[] listClosingDocDatetime = param.getDOCUMENT_CLOSING_DATETIME();

		try{
			DeleteVesselVoyage(param.getTerminalID(), webUser);
			
			int i = 0;
			for(String id : listVesselCode){
				String vesselCode = id;
				String vesselName = listVesselName[i];
				String voyageCode = listVoyageCode[i];
				String voyageStatus = listVoyageStatus[i];
				String voyageCompany = listVoyageCompany[i];
				String podCode = listPodCode[i];
				String podName = listPodName[i];
				String arrivalDate = listArrivalDate[i];
				String departureDate = listDepartureDate[i];
				Integer exportLimit = listExportLimit[i];
				String closingDocDatetime = listClosingDocDatetime[i];
				
				sql = "{call sp_VesselVoyageInsert3(?,?,?,?,?, ?,?,?,?,?,?,?)}";
				listParam = new ArrayList<model.mdlQueryExecute>();
				listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTerminalID() ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", vesselCode ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", vesselName ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", voyageCode ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", voyageStatus ));
				
				listParam.add(QueryExecuteAdapter.QueryParam("string", voyageCompany ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", podCode ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", podName ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", arrivalDate ));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", exportLimit ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", departureDate ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", closingDocDatetime ));
				QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertVesselVoyage2", webUser);
				
				i++;
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertVesselVoyage2", sql , webUser);
		}

		return;
	}
	
	public static String GetSession(String webUser){
		String session = "";
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_KojaSessionGet()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetKojaSession", webUser);

			while(jrs.next()){
				session = jrs.getString("SessionID");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetKojaSession", sql , webUser);
		}
		return session;
	}
	
	public static String GetVesselVoyageCreatedDate(String terminal, String webUser){
		String date = "";
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		try{
			sql = "{call sp_VesselVoyageGetCreatedDate(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", terminal));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetVesselVoyageCreatedDate", webUser);

			while(jrs.next()){
				date = jrs.getString("CreatedDate");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetVesselVoyageCreatedDate", sql , webUser);
		}
		return date;
	}

	public static List<model.mdlVessel> GetVesselVoyageByTerminal(String terminal, String webUser){
		String date = "";
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlVessel> listVessel = new ArrayList<model.mdlVessel>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		try{
			sql = "{call sp_VesselVoyageGetByTerminal(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", terminal));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetVesselVoyageByTerminal", webUser);

			while(jrs.next()){
				model.mdlVessel mdlVessel = new model.mdlVessel();
				mdlVessel.vesselCode = jrs.getString("VesselCode");
				mdlVessel.vesselName = jrs.getString("VesselName");
				mdlVessel.voyageCode = jrs.getString("VoyageNo");
				mdlVessel.voyageStatus = jrs.getString("VoyageStatus");
				mdlVessel.voyageCompany = jrs.getString("VoyageCo");
				mdlVessel.podCode = jrs.getString("PodCode");
				mdlVessel.podName = jrs.getString("PodName");
				mdlVessel.voyageArrival = jrs.getString("Arrival");
				mdlVessel.voyageDeparture = jrs.getString("Departure");
				mdlVessel.terminal = jrs.getString("TerminalID");
				mdlVessel.exportLimit = jrs.getInt("ExportLimit");
				
				listVessel.add(mdlVessel);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetVesselVoyageByTerminal", sql , webUser);
		}
		return listVessel;
	}
	
	public static model.mdlKojaTransaction GetConfirmTransactionParam(String orderManagementID, String webUser){
		model.mdlKojaTransaction mdlKojaTransaction = new model.mdlKojaTransaction();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		try{
			//set static variable
			mdlKojaTransaction.transactionTypeID = "5";
			mdlKojaTransaction.customsDocumentID = "6";
			
			//get sesseionid and custid that assigned by koja
			sql = "{call sp_KojaSessionGet()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetConfirmTransactionParam - getSession", webUser);
			while(jrs.next()){
				mdlKojaTransaction.sessionID = jrs.getString("SessionID");
				mdlKojaTransaction.customerID = jrs.getString("CustID");
			}
			
			//get order information
			CachedRowSet jrs2 = null;
			sql = "{call sp_KojaLoadOrderInfo(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs2 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetConfirmTransactionParam - getOrderInfo", webUser);
			Integer itemWeight = 0;
			while(jrs2.next()){				
				mdlKojaTransaction.orderID = jrs2.getString("OrderManagementID");
				mdlKojaTransaction.documentShippingNo = jrs2.getString("DeliveryOrderID");
				mdlKojaTransaction.shippingLine = jrs2.getString(2);
				mdlKojaTransaction.noBlAwb = jrs2.getString("DeliveryOrderID");
				mdlKojaTransaction.voyageNo = jrs2.getString("VoyageNo");
				mdlKojaTransaction.voyageCompanyCode = jrs2.getString("VoyageCompany");
				mdlKojaTransaction.documentNo = jrs2.getString("NPE");
				mdlKojaTransaction.vesselID = jrs2.getString("VesselName");
				mdlKojaTransaction.pod = jrs2.getString("Pod");
				mdlKojaTransaction.fd = jrs2.getString("Pod");
				mdlKojaTransaction.pol = jrs2.getString("AreaID");
				mdlKojaTransaction.documentShippingDate = jrs2.getString("ShippingDate");
				mdlKojaTransaction.documentDate = jrs2.getString("npeDate").replace("-", "");
				mdlKojaTransaction.logolCustomerID = jrs2.getString("CustomerID");
				mdlKojaTransaction.logolCustomerName = jrs2.getString(14);
				mdlKojaTransaction.logolCustomerAddress = jrs2.getString("Address");
				mdlKojaTransaction.logolCustomerMail = jrs2.getString("Email");
				mdlKojaTransaction.logolCustomerPhone = jrs2.getString("OfficePhone");
				mdlKojaTransaction.proformaNo = jrs2.getString("Tps_Proforma");
				itemWeight = jrs2.getInt("TotalWeight")/jrs2.getInt("TotalQuantity");
			}
			
			//get order detail information
			CachedRowSet jrs3 = null;
			listParam = new ArrayList<model.mdlQueryExecute>();
			sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs3 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetConfirmTransactionParam - getOrderDetailInfo", webUser);
			String iso = "";
			String containerWeight = "";
			String containerNumber = "";
			while(jrs3.next()){
				iso += '"' + jrs3.getString("IsoCode") + '"' + ",";
				
				Integer containerTareWeight = jrs3.getInt("TareWeight");
				containerWeight += '"' + String.valueOf(containerTareWeight + itemWeight) + '"' + ",";
				
				containerNumber += '"' + jrs3.getString("ContainerNumber").replace(" ", "") + '"' + ",";
			}
			mdlKojaTransaction.isoCode = iso.split(",");
			mdlKojaTransaction.weight = containerWeight.split(",");
			mdlKojaTransaction.containerNumber = containerNumber.split(",");
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetConfirmTransactionParam - exception", sql , webUser);
		}
		return mdlKojaTransaction;
	}
	
	public static model.mdlKojaTransaction GetConfirmTransactionParam2(String orderManagementID, String webUser){
		model.mdlKojaTransaction mdlKojaTransaction = new model.mdlKojaTransaction();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		try{
			//set static variable
			mdlKojaTransaction.transactionTypeID = "5";
			mdlKojaTransaction.customsDocumentID = "6";
			
			//get sesseionid and custid that assigned by koja
			sql = "{call sp_KojaSessionGet()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetConfirmTransactionParam2 - getSession", webUser);
			while(jrs.next()){
				/*mdlKojaTransaction.sessionID = jrs.getString("SessionID");*/
				mdlKojaTransaction.customerID = jrs.getString("CustID");
			}
			
			//get order information
			CachedRowSet jrs2 = null;
			sql = "{call sp_KojaLoadOrderInfo(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs2 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetConfirmTransactionParam2 - getOrderInfo", webUser);
			Integer itemWeight = 0;
			String shippingLine = "";
			String pod = "";
			String pol = "";
			while(jrs2.next()){				
				mdlKojaTransaction.orderID = jrs2.getString("OrderManagementID");
				mdlKojaTransaction.documentShippingNo = jrs2.getString("DeliveryOrderID");
				shippingLine = jrs2.getString(2);
				/*mdlKojaTransaction.shippingLine = jrs2.getString(2);*/
				mdlKojaTransaction.noBlAwb = jrs2.getString("DeliveryOrderID");
				mdlKojaTransaction.voyageNo = jrs2.getString("VoyageNo");
				mdlKojaTransaction.voyageCompanyCode = jrs2.getString("VoyageCompany");
				/*mdlKojaTransaction.documentNo = jrs2.getString("NPE");*/
				mdlKojaTransaction.vesselID = jrs2.getString("VesselName");
				pod = jrs2.getString("Pod");
				/*mdlKojaTransaction.fd = jrs2.getString("Pod");*/
				pol = jrs2.getString("AreaID");
				mdlKojaTransaction.documentShippingDate = jrs2.getString("ShippingDate");
				
				if(jrs2.getString("npeDate") != null)
					mdlKojaTransaction.documentDate = jrs2.getString("npeDate").replace("-", "");
				
				mdlKojaTransaction.logolCustomerID = jrs2.getString("CustomerID");
				mdlKojaTransaction.logolCustomerName = jrs2.getString(14);
				mdlKojaTransaction.logolCustomerAddress = jrs2.getString("Address");
				mdlKojaTransaction.logolCustomerMail = jrs2.getString("Email");
				mdlKojaTransaction.logolCustomerPhone = jrs2.getString("OfficePhone");
				mdlKojaTransaction.proformaNo = jrs2.getString("Tps_Proforma");
				mdlKojaTransaction.npeDoc = jrs2.getString("NPEDoc");
				mdlKojaTransaction.npwp = jrs2.getString("NPWP");
				mdlKojaTransaction.shipperName = jrs2.getString(22);
				itemWeight = jrs2.getInt("TotalWeight")/jrs2.getInt("TotalQuantity");
			}
			
			//get order detail information
			CachedRowSet jrs3 = null;
			listParam = new ArrayList<model.mdlQueryExecute>();
			sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs3 = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetConfirmTransactionParam2 - getOrderDetailInfo", webUser);
			List<String> listShippingLine = new ArrayList<String>();
			List<String> iso = new ArrayList<String>();
			List<String> containerWeight = new ArrayList<String>();
			List<String> containerNumber = new ArrayList<String>();
			List<String> listPod = new ArrayList<String>();
			List<String> listPol = new ArrayList<String>();
			while(jrs3.next()){
				listShippingLine.add(shippingLine);
				iso.add(jrs3.getString("Tps_Iso"));
				
				Integer containerTareWeight = jrs3.getInt("TareWeight");
				containerWeight.add(String.valueOf(containerTareWeight + itemWeight));
				
				containerNumber.add(jrs3.getString("ContainerNumber").replace(" ", ""));
				listPod.add(pod);
				listPol.add(pol);
			}
			Gson gson = new Gson();
			mdlKojaTransaction.shippingLine = gson.toJson(listShippingLine);
			mdlKojaTransaction.sisoCode = gson.toJson(iso);
			mdlKojaTransaction.sWeight = gson.toJson(containerWeight);
			mdlKojaTransaction.sContainerNumber = gson.toJson(containerNumber);
			mdlKojaTransaction.pod = gson.toJson(listPod);
			mdlKojaTransaction.fd = gson.toJson(listPod);
			mdlKojaTransaction.pol = gson.toJson(listPol);
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetConfirmTransactionParam2 - exception", sql , webUser);
		}
		return mdlKojaTransaction;
	}
	
	public static void InsertFD(mdlKojaFD param, String webUser){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		String[] listPortName = param.getPORT_NAME();
		String[] listPortID = param.getPORT_CODE();

		try{
			sql = "{call sp_PortFDDelete(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTerminalID() ));
			Boolean result = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteFD", webUser);
			
			if(result){
				int i = 0;
				for(String portID : listPortID){
					String id = portID;
					String name = listPortName[i];
					
					sql = "{call sp_PortFDInsert(?,?,?,?)}";
					listParam = new ArrayList<model.mdlQueryExecute>();
					listParam.add(QueryExecuteAdapter.QueryParam("string", param.getTerminalID() ));
					listParam.add(QueryExecuteAdapter.QueryParam("string", id ));
					listParam.add(QueryExecuteAdapter.QueryParam("string", name ));
					listParam.add(QueryExecuteAdapter.QueryParam("string", webUser ));
					QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertFD", webUser);
					
					i++;
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertFD", sql , webUser);
		}

		return;
	}
	
	public static Boolean CheckExportLimit(mdlGetOnDemandParam param, String user){
		Boolean check = true;
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		Integer totalOrderContainer = 0;
		try{
			sql = "{call sp_LoadOrderManagementByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getOrderID()));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckExportLimit", user);

			while(jrs.next()){
				totalOrderContainer = jrs.getInt("TotalQuantity");
			}
			
			if(totalOrderContainer > param.getExportLimit()){
				check = false;
				//update status tps to 'exportlimit'/15 function here
            	/*OrderManagementAdapter.UpdateOrderTpsStatus(param.orderID, 15, user);*/
				
				/*List<model.mdlDeliveryOrder> listDO = new ArrayList<model.mdlDeliveryOrder>();
				listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(param.getOrderID(), user);
				
				for(mdlDeliveryOrder DeliveryOrder : listDO){
					VendorOrderDetailAdapter.UpdateVendorOrderDetailStatus(DeliveryOrder.getDoNumber(), user);
					PushNotifAdapter.PushNotification("exportLimitExceed", DeliveryOrder.getDriverID(), user);
				}*/
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckExportLimit", sql , user);
		}
		return check;
	}
	
	public static Boolean CheckVoyageClosingDoc(String voyageClosingDoc, String user){
		Boolean check = true;
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        Date date1 = sdf.parse(LocalDateTime.now().toString().replace("T", " "));
	        Date date2 = sdf.parse(voyageClosingDoc);
	        
			if(date1.after(date2)){
				check = false;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckVoyageClosingDoc", LocalDateTime.now().toString().replace("T", " ") + " - " + voyageClosingDoc, user);
		}
		return check;
	}
	
}
