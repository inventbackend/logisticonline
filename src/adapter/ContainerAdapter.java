package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;

public class ContainerAdapter {

	public static List<String> LoadContainerByOrder(String orderID, String user) {
		List<String> containerNo = new ArrayList<String>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try {
			sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadContainerByOrder", user);

			while(jrs.next()){
				containerNo.add(jrs.getString("ContainerNumber").replace(" ",""));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadContainerByOrder", sql , user);
		}

		return containerNo;
	}
	
	public static List<String> LoadTIDByOrder(String orderID, String user) {
		List<String> tidNumber = new ArrayList<String>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try {
			sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTIDByOrder", user);

			while(jrs.next()){
				tidNumber.add(jrs.getString(22));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTIDByOrder", sql , user);
		}

		return tidNumber;
	}
	
	public static List<String> LoadContainerInKoja(String user) {
		List<String> containerNo = new ArrayList<String>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try {
			sql = "{call sp_GetContainerInKoja()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadContainerInKoja" , user);

			while(jrs.next()){
				containerNo.add(jrs.getString("ContainerNumber").replace(" ",""));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadContainerInKoja", sql , user);
		}

		return containerNo;
	}
	
	public static List<model.mdlVendorOrderDetail> LoadTidByEmptyKojaEir(String user) {
		List<model.mdlVendorOrderDetail> listVodt = new ArrayList<model.mdlVendorOrderDetail>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try {
			sql = "{call sp_TidGetByEmptyEir()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadTidByEmptyKojaEir" , user);

			while(jrs.next()){
				model.mdlVendorOrderDetail vodt = new model.mdlVendorOrderDetail();
				vodt.vendorOrderDetailID = jrs.getString(1);
				vodt.tidNumber = jrs.getString("TID_Number");
				listVodt.add(vodt);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTidByEmptyKojaEir", sql , user);
		}

		return listVodt;
	}
	
	public static List<model.mdlVendorOrderDetail> LoadTidByEmptyKojaCms(String user) {
		List<model.mdlVendorOrderDetail> listVodt = new ArrayList<model.mdlVendorOrderDetail>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try {
			sql = "{call sp_TidGetByEmptyCms()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadTidByEmptyKojaCms" , user);

			while(jrs.next()){
				model.mdlVendorOrderDetail vodt = new model.mdlVendorOrderDetail();
				vodt.vendorOrderDetailID = jrs.getString(1);
				vodt.tidNumber = jrs.getString("TID_Number");
				vodt.containerNumber = jrs.getString("ContainerNumber");
				listVodt.add(vodt);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTidByEmptyKojaCms", sql , user);
		}

		return listVodt;
	}
	
}
