package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlContainerType;
import model.mdlFactory;

public class FactoryAdapter {

	public static String CreateFactoryID(String user){
		String lastFactoryID = getLastFactoryID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "0001";

		if ((lastFactoryID != null) && (!lastFactoryID.equals("")) ){
			String[] partsFactoryID = lastFactoryID.split("-");
			String partDate = partsFactoryID[1];
			String partNumber = partsFactoryID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("FCTR-").append(datetime).append("-").append(stringInc);
		String FactoryID = sb.toString();
		return FactoryID;
	}

	public static String getLastFactoryID(String user){		
		String sql="";
		CachedRowSet rowset = null;
		String FactoryID = "";
		try{
			sql = "{call sp_getLastFactoryID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastFactoryID", user);

			while(rowset.next())
			{
				FactoryID = rowset.getString("FactoryID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastFactoryID", sql, user);
		}
		
		return FactoryID;
	}
	
	public static List<model.mdlFactory> LoadFactoryByCustomer(String CustomerID, String user){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlFactory> listFactory = new ArrayList<model.mdlFactory>();
		try{
			sql = "{call sp_LoadFactoryByCustomer(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadFactoryByCustomer", user);
			
			while(rowset.next())
			{
				model.mdlFactory factory = new model.mdlFactory();
				factory.FactoryID = rowset.getString("FactoryID");
				factory.FactoryName = rowset.getString(2);
				factory.pic = rowset.getString("PIC");
				factory.officePhone = rowset.getString("OfficePhone");
				factory.mobilePhone = rowset.getString("MobilePhone");
				factory.province = rowset.getString("Province");
				factory.Address = rowset.getString("Address").replace("\n", "");
				factory.DistrictID = rowset.getString("DistrictID");
				factory.TierID = rowset.getString("TierID");
				factory.DistrictName = rowset.getString(9);
				factory.TierName = rowset.getString(11);
				listFactory.add(factory);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadFactoryByCustomer", sql, user);
		}
		
		return listFactory;
	}

	public static String RegisterFactory(mdlFactory lParam)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertFactory(?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DistrictID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TierID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "RegisterFactory", lParam.CustomerID);

			result = success == true ? "Success Register Factory" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "RegisterFactory", sql , lParam.getCustomerID());
			result = "Database Error";
		}

		return result;
	}

	public static String InsertFactory(mdlFactory lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertFactory(?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.pic));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.officePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.mobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.province));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DistrictID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TierID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertFactory", user);

			result = success == true ? "Success Insert Factory" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertFactory", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateFactory(mdlFactory lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateFactory(?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.FactoryName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.pic));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.officePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.mobilePhone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.province));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Address));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.DistrictID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.TierID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateFactory", user);

			result = success == true ? "Success Update Factory" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateFactory", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteFactory(String CustomerID, String FactoryID, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteFactory(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", FactoryID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteFactory", user);

			result = success == true ? "Success Delete Factory" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteFactory", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
}
