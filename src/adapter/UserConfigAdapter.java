package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;

public class UserConfigAdapter {

	public static model.mdlUserConfig LoadDefaultUserConfig(String user){		
		String sql="";
		CachedRowSet rowset = null;
		model.mdlUserConfig defaultUserConfig = new model.mdlUserConfig();
		try{
			sql = "{call sp_UserConfigLoadDefault()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "LoadDefaultUserConfig", user);
			while(rowset.next())
			{
				defaultUserConfig.IpLocal = rowset.getString("IpLocal");
				defaultUserConfig.PortLocal = rowset.getString("PortLocal");
				defaultUserConfig.IpPublic = rowset.getString("IpPublic");
				defaultUserConfig.PortPublic = rowset.getString("PortPublic");
				defaultUserConfig.IpAlternative = rowset.getString("IpAlternative");
				defaultUserConfig.PortAlternative = rowset.getString("PortAlternative");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDefaultUserConfig", sql, user);
		}
		
		return defaultUserConfig;
	}
	
}
