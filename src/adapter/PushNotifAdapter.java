package adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;
import model.mdlPush;
import model.mdlVendorOrderDetail;

public class PushNotifAdapter {

	public static void PushNotification(String lProcess, String lDriverID, String user) {
		String apiKey = "AAAAn-MzguU:APA91bFVvKpSgtOj1YGCRDSF_p6N7MUx7iprMgQiwGJiAum3cmuaxHbZw6zWeNQ1QCoygs7Aj2QGdRCB2ZM73V3SoVwbliMPwmkHqFiryNRPPmMm3LzroS15EGXwrUR6y9WihRhYDFY7";
		String notificationID = "";
		String title = "";
		String message = "";
		if(lProcess.contentEquals("assign")){
			title = "Anda mendapatkan tugas baru";
			message = "Silahkan buka app Logol untuk melihat detil";
			notificationID = user+"_PushNewAssignment_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		else if(lProcess.contentEquals("cancel assign")){
			title = "Tugas anda telah dibatalkan";
			message = "";
			notificationID = user+"_PushCancelAssignment_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		else if(lProcess.contentEquals("yellowcard")){
			title = "Gatepass telah terbit";
			message = "Silahkan buka app Logol untuk melihat detil";
			notificationID = user+"_PushNewAssignment_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		else if(lProcess.contentEquals("containernumber")){
			title = "Nomor kontainer anda telah diperbaharui";
			message = "Silahkan buka app Logol untuk melihat detil";
			notificationID = user+"_PushNewAssignment_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		else if(lProcess.split("&&")[0].contentEquals("associateTicket")){
			title = "Gatepass telah terbit";
			message = "Berlaku sampai: "+ lProcess.split("&&")[1];
			notificationID = user+"_PushAssociateTicket_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		else if(lProcess.contentEquals("exportLimitExceed")){
			title = "Ekspor ditunda";
			message = "Mohon diantarkan ke depo penitipan terdekat";
			notificationID = user+"_PushExportLimit_"+lDriverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		}
		
		model.mdlPush mdlPush = new model.mdlPush();
		
		model.mdlNotification mdlNotif = new model.mdlNotification();
		mdlNotif.setTitle(title); 
		mdlNotif.setBody(message);
		
		mdlData mdlData = new model.mdlData();
		mdlData.setTitle(title);
		mdlData.setMsg(message); 
		mdlData.setNotificationid(notificationID);
		List<String> userToken = new ArrayList<String>();
		userToken = LoadTokenByDriverID(lDriverID, user);
		
		mdlPush.setRegistration_ids(userToken);
		mdlPush.setPriority("high");
		mdlPush.setDelay_while_idle(false);
		mdlPush.setContent_available(true);
		mdlPush.setData(mdlData);
		
		PostToGCMGeneral(apiKey, mdlPush);
	}
	
	public static String PostToGCMGeneral(String apiKey, mdlPush content){
		String lResult = "";
		
		try{
	        // 1. URL
	        URL url = new URL("https://fcm.googleapis.com/fcm/send");

	        // 2. Open connection
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	        // 3. Specify POST method
	        conn.setRequestMethod("POST");

	        // 4. Set the headers
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Authorization", "key="+apiKey);

	        conn.setDoOutput(true);
	        
	        Gson gson = new Gson();
	        
	        String json = gson.toJson(content);
	        
	        System.out.println(json);
	        
 
	            // 5. Add JSON data into POST request body

	            //`5.1 Use Jackson object mapper to convert Contnet object into JSON

	            // 5.2 Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            // 5.3 Copy Content "JSON" into
	            //mapper.writeValue(wr, content);
	            wr.write(json.getBytes("UTF-8"));
	            

	            // 5.4 Send the request
	            wr.flush();

	            // 5.5 close
	            wr.close();

	            // 6. Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            // 7. Print result
	            System.out.println(response.toString());
	            //lResult = response.toString();
	            lResult = "Push Success";

	            } catch (MalformedURLException e) {
	            	LogAdapter.InsertLogExc(e.toString(), "PostToGCMGeneral", "PostToGCMGeneral" , "LOGOL");
	                e.printStackTrace();
	                lResult = "Push Failed";
	            } catch (IOException e) {
	            	LogAdapter.InsertLogExc(e.toString(), "PostToGCMGeneral", "PostToGCMGeneral" , "LOGOL");
	                e.printStackTrace();
	                lResult = "Push Failed";
	            }
		
		return lResult;
	}
	
	public static List<String> LoadTokenByDriverID(String lDriverID, String user) {
		List<String> listToken = new ArrayList<String>();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_DriverLoadToken(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDriverID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTokenByDriverID", user);

			while(jrs.next()){
				String Token = "";
				Token = jrs.getString("Androidkey");
				
				listToken.add(Token);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTokenByDriverID", sql , user);
		}

		return listToken;
	}
	
	public static void PushListCancelNotification(List<model.mdlVendorOrderDetail> lParam, String user) {
		for(mdlVendorOrderDetail param : lParam){
			String apiKey = "AAAAn-MzguU:APA91bFVvKpSgtOj1YGCRDSF_p6N7MUx7iprMgQiwGJiAum3cmuaxHbZw6zWeNQ1QCoygs7Aj2QGdRCB2ZM73V3SoVwbliMPwmkHqFiryNRPPmMm3LzroS15EGXwrUR6y9WihRhYDFY7";
			String notificationID = user+"_PushCancelAssignment_"+param.driverID+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
			String title = "Tugas anda telah dibatalkan";
			String message = "";
			
			model.mdlPush mdlPush = new model.mdlPush();
			
			model.mdlNotification mdlNotif = new model.mdlNotification();
			mdlNotif.setTitle(title); 
			mdlNotif.setBody(message);
			
			mdlData mdlData = new model.mdlData();
			mdlData.setTitle(title);
			mdlData.setMsg(message); 
			mdlData.setNotificationid(notificationID);
			List<String> userToken = new ArrayList<String>();
			userToken = LoadTokenByDriverID(param.driverID, user);
			
			mdlPush.setRegistration_ids(userToken);
			mdlPush.setPriority("high");
			mdlPush.setDelay_while_idle(false);
			mdlPush.setContent_available(true);
			mdlPush.setData(mdlData);
			
			PostToGCMGeneral(apiKey, mdlPush);
		}
	}
	
}
