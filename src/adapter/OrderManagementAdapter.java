package adapter;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlOrderManagement;
import model.mdlOrderManagementDetail;
import model.mdlVendorOrderDetail;

public class OrderManagementAdapter {

	public static List<model.mdlOrderManagement> LoadOrderManagement(String user) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadOrderManagement_v2()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadOrderManagement", user);

			while(jrs.next()){
				model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
			    mdlOrderManagement.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
			    mdlOrderManagement.setPeb(jrs.getString("PEB"));
			    mdlOrderManagement.setNpe(jrs.getString("NPE"));
			    mdlOrderManagement.setCustomerID(jrs.getString("CustomerID"));
			    mdlOrderManagement.setCustomerName(jrs.getString(5));
			    mdlOrderManagement.setFactoryID(jrs.getString("FactoryID"));
			    mdlOrderManagement.setFactoryName(jrs.getString(7));
			    mdlOrderManagement.setFactoryAddress(jrs.getString("Address"));
			    mdlOrderManagement.setOrderStatus(jrs.getString("OrderStatus"));
			    mdlOrderManagement.setOrderType(jrs.getString("OrderType"));
			    mdlOrderManagement.setTier(jrs.getString("TierID"));
			    mdlOrderManagement.setTierDescription(jrs.getString("Description"));
			    mdlOrderManagement.setItemDescription(jrs.getString("ItemDescription"));
			    mdlOrderManagement.setPortID(jrs.getString("PortID"));
			    mdlOrderManagement.setPortName(jrs.getString("PortName"));
			    mdlOrderManagement.setPortUTC(jrs.getString("PortUTC"));
			    mdlOrderManagement.setShippingID(jrs.getString("ShippingID"));
			    mdlOrderManagement.setShippingName(jrs.getString(17));
			    mdlOrderManagement.setDepoID(jrs.getString("DepoID"));
			    mdlOrderManagement.setDepoName(jrs.getString(19));
			    mdlOrderManagement.setTotalQuantity(jrs.getInt("TotalQuantity"));
			    Double tempTotalPrice = jrs.getDouble("TotalPrice");
			    mdlOrderManagement.setTotalPrice(tempTotalPrice.intValue());
			    mdlOrderManagement.setTotalQuantityDelivered(jrs.getInt("TotalQuantityDelivered"));
			    Double tempTotalPriceDelivered = jrs.getDouble("TotalPriceDelivered");
			    mdlOrderManagement.setTotalPriceDelivered(tempTotalPriceDelivered.intValue());
			    mdlOrderManagement.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlOrderManagement.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlOrderManagement.setOrderImage(jrs.getString("OrderImage"));
			    mdlOrderManagement.setVoyageNo(jrs.getString("VoyageNo"));
			    mdlOrderManagement.setVesselName(jrs.getString("VesselName"));
			    mdlOrderManagement.setPod(jrs.getString("Pod"));
			    mdlOrderManagement.setItemWeight(Integer.valueOf(jrs.getString("TotalWeight")));
			    mdlOrderManagement.setVoyageCo(jrs.getString("VoyageCompany"));
			    /*String containerSize = getContainerSize(jrs.getString("OrderManagementID") ,user);
			    mdlOrderManagement.setContainerTypeDescription(containerSize);*/
			    /*mdlOrderManagement.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));*/

				listOrderManagement.add(mdlOrderManagement);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOrderManagement", sql , user);
		}

		return listOrderManagement;
	}
	
	public static List<model.mdlOrderManagement> LoadArchive(String user) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_OrderManagementLoadArchive()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadArchive", user);

			while(jrs.next()){
				model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
			    mdlOrderManagement.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
			    mdlOrderManagement.setPeb(jrs.getString("PEB"));
			    mdlOrderManagement.setNpe(jrs.getString("NPE"));
			    mdlOrderManagement.setCustomerID(jrs.getString("CustomerID"));
			    mdlOrderManagement.setCustomerName(jrs.getString(5));
			    mdlOrderManagement.setFactoryID(jrs.getString("FactoryID"));
			    mdlOrderManagement.setFactoryName(jrs.getString(7));
			    mdlOrderManagement.setFactoryAddress(jrs.getString("Address"));
			    mdlOrderManagement.setOrderStatus(jrs.getString("OrderStatus"));
			    mdlOrderManagement.setOrderType(jrs.getString("OrderType"));
			    mdlOrderManagement.setTier(jrs.getString("TierID"));
			    mdlOrderManagement.setTierDescription(jrs.getString("Description"));
			    mdlOrderManagement.setItemDescription(jrs.getString("ItemDescription"));
			    mdlOrderManagement.setPortID(jrs.getString("PortID"));
			    mdlOrderManagement.setPortName(jrs.getString("PortName"));
			    mdlOrderManagement.setPortUTC(jrs.getString("PortUTC"));
			    mdlOrderManagement.setShippingID(jrs.getString("ShippingID"));
			    mdlOrderManagement.setShippingName(jrs.getString(17));
			    mdlOrderManagement.setDepoID(jrs.getString("DepoID"));
			    mdlOrderManagement.setDepoName(jrs.getString(19));
			    mdlOrderManagement.setTotalQuantity(jrs.getInt("TotalQuantity"));
			    Double tempTotalPrice = jrs.getDouble("TotalPrice");
			    mdlOrderManagement.setTotalPrice(tempTotalPrice.intValue());
			    mdlOrderManagement.setTotalQuantityDelivered(jrs.getInt("TotalQuantityDelivered"));
			    Double tempTotalPriceDelivered = jrs.getDouble("TotalPriceDelivered");
			    mdlOrderManagement.setTotalPriceDelivered(tempTotalPriceDelivered.intValue());
			    mdlOrderManagement.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlOrderManagement.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlOrderManagement.setOrderImage(jrs.getString("OrderImage"));
			    mdlOrderManagement.setVoyageNo(jrs.getString("VoyageNo"));
			    mdlOrderManagement.setVesselName(jrs.getString("VesselName"));
			    mdlOrderManagement.setPod(jrs.getString("Pod"));
			    mdlOrderManagement.setItemWeight(Integer.valueOf(jrs.getString("TotalWeight")));
			    mdlOrderManagement.setVoyageCo(jrs.getString("VoyageCompany"));
			    /*String containerSize = getContainerSize(jrs.getString("OrderManagementID") ,user);
			    mdlOrderManagement.setContainerTypeDescription(containerSize);
			    mdlOrderManagement.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));*/

				listOrderManagement.add(mdlOrderManagement);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadArchive", sql , user);
		}

		return listOrderManagement;
	}

	public static List<model.mdlOrderManagement> LoadOrderManagementByCustomer(String customerID) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadOrderManagementByCustomer_v2(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", customerID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadOrderManagementByCustomer", customerID);

			while(jrs.next()){
				model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
			    mdlOrderManagement.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
			    mdlOrderManagement.setPeb(jrs.getString("PEB"));
			    mdlOrderManagement.setNpe(jrs.getString("NPE"));
			    mdlOrderManagement.setCustomerID(jrs.getString("CustomerID"));
			    mdlOrderManagement.setCustomerName(jrs.getString(5));
			    mdlOrderManagement.setFactoryID(jrs.getString("FactoryID"));
			    mdlOrderManagement.setFactoryName(jrs.getString(7));
			    mdlOrderManagement.setFactoryAddress(jrs.getString("Address"));
			    mdlOrderManagement.setOrderStatus(jrs.getString("OrderStatus"));
			    mdlOrderManagement.setOrderType(jrs.getString("OrderType"));
			    mdlOrderManagement.setTier(jrs.getString("TierID"));
			    mdlOrderManagement.setTierDescription(jrs.getString("Description"));
			    mdlOrderManagement.setItemDescription(jrs.getString("ItemDescription"));
			    mdlOrderManagement.setPortID(jrs.getString("PortID"));
			    mdlOrderManagement.setPortName(jrs.getString("PortName"));
			    mdlOrderManagement.setPortUTC(jrs.getString("PortUTC"));
			    mdlOrderManagement.setShippingID(jrs.getString("ShippingID"));
			    mdlOrderManagement.setShippingName(jrs.getString(17));
			    mdlOrderManagement.setDepoID(jrs.getString("DepoID"));
			    mdlOrderManagement.setDepoName(jrs.getString(19));
			    mdlOrderManagement.setTotalQuantity(jrs.getInt("TotalQuantity"));
			    Double tempTotalPrice = jrs.getDouble("TotalPrice");
			    mdlOrderManagement.setTotalPrice(tempTotalPrice.intValue());
			    mdlOrderManagement.setTotalQuantityDelivered(jrs.getInt("TotalQuantityDelivered"));
			    Double tempTotalPriceDelivered = jrs.getDouble("TotalPriceDelivered");
			    mdlOrderManagement.setTotalPriceDelivered(tempTotalPriceDelivered.intValue());
			    mdlOrderManagement.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlOrderManagement.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlOrderManagement.setOrderImage(jrs.getString("OrderImage"));
			    mdlOrderManagement.setVoyageNo(jrs.getString("VoyageNo"));
			    mdlOrderManagement.setVesselName(jrs.getString("VesselName"));
			    mdlOrderManagement.setPod(jrs.getString("Pod"));
			    mdlOrderManagement.setItemWeight(Integer.valueOf(jrs.getString("TotalWeight")));
			    mdlOrderManagement.setVoyageCo(jrs.getString("VoyageCompany"));
			    mdlOrderManagement.setShippingDate(ConvertDateTimeHelper.formatDate(jrs.getString("ShippingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    /*mdlOrderManagement.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlOrderManagement.setContainerTypeDescription(jrs.getString(37));*/
			    
				listOrderManagement.add(mdlOrderManagement);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOrderManagementByCustomer", sql , customerID);
		}
		return listOrderManagement;
	}
	
	public static List<model.mdlOrderManagement> LoadArchiveByCustomer(String customerID) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_OrderManagementLoadArchiveByCustomer(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", customerID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadArchiveByCustomer", customerID);

			while(jrs.next()){
				model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
			    mdlOrderManagement.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
			    mdlOrderManagement.setPeb(jrs.getString("PEB"));
			    mdlOrderManagement.setNpe(jrs.getString("NPE"));
			    mdlOrderManagement.setCustomerID(jrs.getString("CustomerID"));
			    mdlOrderManagement.setCustomerName(jrs.getString(5));
			    mdlOrderManagement.setFactoryID(jrs.getString("FactoryID"));
			    mdlOrderManagement.setFactoryName(jrs.getString(7));
			    mdlOrderManagement.setFactoryAddress(jrs.getString("Address"));
			    mdlOrderManagement.setOrderStatus(jrs.getString("OrderStatus"));
			    mdlOrderManagement.setOrderType(jrs.getString("OrderType"));
			    mdlOrderManagement.setTier(jrs.getString("TierID"));
			    mdlOrderManagement.setTierDescription(jrs.getString("Description"));
			    mdlOrderManagement.setItemDescription(jrs.getString("ItemDescription"));
			    mdlOrderManagement.setPortID(jrs.getString("PortID"));
			    mdlOrderManagement.setPortName(jrs.getString("PortName"));
			    mdlOrderManagement.setPortUTC(jrs.getString("PortUTC"));
			    mdlOrderManagement.setShippingID(jrs.getString("ShippingID"));
			    mdlOrderManagement.setShippingName(jrs.getString(17));
			    mdlOrderManagement.setDepoID(jrs.getString("DepoID"));
			    mdlOrderManagement.setDepoName(jrs.getString(19));
			    mdlOrderManagement.setTotalQuantity(jrs.getInt("TotalQuantity"));
			    Double tempTotalPrice = jrs.getDouble("TotalPrice");
			    mdlOrderManagement.setTotalPrice(tempTotalPrice.intValue());
			    mdlOrderManagement.setTotalQuantityDelivered(jrs.getInt("TotalQuantityDelivered"));
			    Double tempTotalPriceDelivered = jrs.getDouble("TotalPriceDelivered");
			    mdlOrderManagement.setTotalPriceDelivered(tempTotalPriceDelivered.intValue());
			    mdlOrderManagement.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlOrderManagement.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlOrderManagement.setOrderImage(jrs.getString("OrderImage"));
			    mdlOrderManagement.setVoyageNo(jrs.getString("VoyageNo"));
			    mdlOrderManagement.setVesselName(jrs.getString("VesselName"));
			    mdlOrderManagement.setPod(jrs.getString("Pod"));
			    mdlOrderManagement.setItemWeight(Integer.valueOf(jrs.getString("TotalWeight")));
			    mdlOrderManagement.setVoyageCo(jrs.getString("VoyageCompany"));
			    mdlOrderManagement.setShippingDate(ConvertDateTimeHelper.formatDate(jrs.getString("ShippingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlOrderManagement.setTpsLinkInvoice(jrs.getString(36));
			    /*mdlOrderManagement.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlOrderManagement.setContainerTypeDescription(jrs.getString(37));*/
			    
				listOrderManagement.add(mdlOrderManagement);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadArchiveByCustomer", sql , customerID);
		}
		return listOrderManagement;
	}

	public static List<model.mdlOrderManagement> LoadDocumentation(String dateNow, String user) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_DocumentationLoad_v2(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", user));
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDocumentation", user);

			while(jrs.next()){
				model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
			    mdlOrderManagement.setPeb(jrs.getString("PEB"));
			    mdlOrderManagement.setNpe(jrs.getString("NPE"));
			    mdlOrderManagement.setNpeDate(ConvertDateTimeHelper.formatDate(jrs.getString("NPEDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlOrderManagement.setYellowCard(jrs.getString("YellowCardID"));
			    mdlOrderManagement.setPortID(jrs.getString("PortID"));
			    mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
			    mdlOrderManagement.setVoyageExportLimit(0);
			    mdlOrderManagement.setVoyageClosingDoc("");
			    mdlOrderManagement.setNpeDoc(jrs.getString("NPEDoc"));
			    mdlOrderManagement.setNpwp(jrs.getString("NPWP"));

				listOrderManagement.add(mdlOrderManagement);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDocumentation", sql , user);
		}
		return listOrderManagement;
	}
	
	public static List<model.mdlOrderManagement> LoadDocumentationByPol(String dateNow, String pol, String user, String userrole) {
		List<model.mdlOrderManagement> listOrderManagement = new ArrayList<model.mdlOrderManagement>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			if(userrole.equals("R001")){
				sql = "{call sp_DocumentationLoadByPolForAdmin(?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				listParam.add(QueryExecuteAdapter.QueryParam("string", pol));
				jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDocumentationByPol", user);

				while(jrs.next()){
					model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
					mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				    mdlOrderManagement.setPeb(jrs.getString("PEB"));
				    mdlOrderManagement.setNpe(jrs.getString("NPE"));
				    mdlOrderManagement.setNpeDate(ConvertDateTimeHelper.formatDate(jrs.getString("NPEDate"), "yyyy-MM-dd", "dd MMM yyyy"));
				    mdlOrderManagement.setYellowCard(jrs.getString("YellowCardID"));
				    mdlOrderManagement.setTpsTransactionID(jrs.getString("Tps_TxID"));
				    mdlOrderManagement.setTpsProforma(jrs.getString("Tps_Proforma"));
				    mdlOrderManagement.setTpsBillingAmount(jrs.getInt("Tps_BillingAmount"));
				    mdlOrderManagement.setTpsBillingStatus(Integer.valueOf(jrs.getString("Tps_BillingStatus")));
				    mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				    mdlOrderManagement.setTpsInvoiceNo(jrs.getString("Tps_InvoiceNo"));
				    mdlOrderManagement.setVoyageExportLimit(jrs.getInt("ExportLimit"));
				    mdlOrderManagement.setPortID(jrs.getString("PortID"));
				    mdlOrderManagement.setVoyageClosingDoc(jrs.getString("ClosingDoc"));
				    mdlOrderManagement.setNpeDoc(jrs.getString("NPEDoc"));
				    mdlOrderManagement.setTpsLinkInvoice(jrs.getString("Tps_InvoiceLink"));
				    mdlOrderManagement.setNpwp(jrs.getString("NPWP"));

					listOrderManagement.add(mdlOrderManagement);
				}
			}
			else{
				sql = "{call sp_DocumentationLoadByPol(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", user));
				listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
				listParam.add(QueryExecuteAdapter.QueryParam("string", pol));
				jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDocumentationByPol", user);

				while(jrs.next()){
					model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
					mdlOrderManagement.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				    mdlOrderManagement.setPeb(jrs.getString("PEB"));
				    mdlOrderManagement.setNpe(jrs.getString("NPE"));
				    mdlOrderManagement.setNpeDate(ConvertDateTimeHelper.formatDate(jrs.getString("NPEDate"), "yyyy-MM-dd", "dd MMM yyyy"));
				    mdlOrderManagement.setYellowCard(jrs.getString("YellowCardID"));
				    mdlOrderManagement.setTpsTransactionID(jrs.getString("Tps_TxID"));
				    mdlOrderManagement.setTpsProforma(jrs.getString("Tps_Proforma"));
				    mdlOrderManagement.setTpsBillingAmount(jrs.getInt("Tps_BillingAmount"));
				    mdlOrderManagement.setTpsBillingStatus(Integer.valueOf(jrs.getString("Tps_BillingStatus")));
				    mdlOrderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				    mdlOrderManagement.setTpsInvoiceNo(jrs.getString("Tps_InvoiceNo"));
				    mdlOrderManagement.setVoyageExportLimit(jrs.getInt("ExportLimit"));
				    mdlOrderManagement.setPortID(jrs.getString("PortID"));
				    mdlOrderManagement.setVoyageClosingDoc(jrs.getString("ClosingDoc"));
				    mdlOrderManagement.setNpeDoc(jrs.getString("NPEDoc"));
				    mdlOrderManagement.setNpwp(jrs.getString("NPWP"));

					listOrderManagement.add(mdlOrderManagement);
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDocumentationByPol", sql , user);
		}
		return listOrderManagement;
	}
	
	public static String InsertOrderManagement(mdlOrderManagement mdlOrderManagement){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			sql = "{call sp_InsertOrderManagement_v2(?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingInvoiceID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getDeliveryOrderID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getCustomerID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getFactoryID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderType()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTier()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getItemDescription()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPortID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getDepoID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getCreatedBy()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderImage()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPeb()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpe()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVoyageNo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVesselName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPod()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagement.getItemWeight()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVoyageCo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingDate()));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertOrderManagement", mdlOrderManagement.getCustomerID());

			for(String vendorID : mdlOrderManagement.getVendor()){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_InsertMappingOrderVendor(?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertMappingOrderVendor", mdlOrderManagement.getCustomerID());
			}
			
			result = success == true ? "Success Insert Order Management" : "Database Error";

			//set new order management id to global variable to direct page to order management detail
			//Globals.gOrderManagementID = success == true ? mdlOrderManagement.getOrderManagementID() : "";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertOrderManagement", sql , mdlOrderManagement.getCustomerID());
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateOrderManagement(mdlOrderManagement mdlOrderManagement){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteMappingOrderVendor(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID() ));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteMappingOrderVendor", mdlOrderManagement.getCustomerID());

			for(String vendorID : mdlOrderManagement.getVendor()){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_InsertMappingOrderVendor(?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateMappingOrderVendor", mdlOrderManagement.getCustomerID());
			}
			
			listParam = new ArrayList<model.mdlQueryExecute>();
			sql = "{call sp_UpdateOrderManagement_v2(?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingInvoiceID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getDeliveryOrderID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getCustomerID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getFactoryID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderType() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTier() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getItemDescription() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPortID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getDepoID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getUpdatedBy()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderImage()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPeb()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpe()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVoyageNo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVesselName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPod()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagement.getItemWeight()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getVoyageCo()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingDate()));
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOrderManagement", mdlOrderManagement.getCustomerID());
			
			result = success == true ? "Success Update Order Management" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOrderManagement", sql , mdlOrderManagement.getCustomerID());
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateDocumentation(mdlOrderManagement mdlOrderManagement, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DocumentationUpdate(?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getShippingInvoiceID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getPeb()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpe()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpeDate()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpeDoc()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getNpwp()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateDocumentation", user);

			result = success == true ? "Success Update Documentation" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateDocumentation", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CancelOrderManagement(String orderManagementID, String user){
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{
			//store 'CancelOrderManagement' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_CancelOrderManagement(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'CancelOrderManagementDetail' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_CancelOrderManagementDetail(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);

			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "CancelOrderManagement", user);
			result = success == true ? "Success Cancel Order Management" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CancelOrderManagement", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateOrderManagementID(String user, String orderType){
		String lastOrderManagementID = GetLastOrderManagementID(user, orderType);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		//String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		//String stringInc = "000001";
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMM");
		String stringInc = "0001";

		if ((lastOrderManagementID != null) && (!lastOrderManagementID.equals("")) ){
			String[] partsOrderManagementID = lastOrderManagementID.split("/");
			String partDate = partsOrderManagementID[1];
			String partNumber = partsOrderManagementID[2].replace(orderType, "");

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
				//stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		//sb.append("ORHD-").append(datetime).append("-").append(stringInc);
		sb.append("LG/").append(datetime).append("/").append(orderType).append(stringInc);
		String OrderManagementID = sb.toString();
		return OrderManagementID;
	}

	public static String GetLastOrderManagementID(String user, String orderType){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		String OrderManagementID = "";

		try {
			sql = "{call sp_GetLastOrderManagementID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderType ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetLastOrderManagementID", user);

			while(jrs.next()){
				OrderManagementID = jrs.getString("OrderManagementID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastOrderManagementID", sql , user);
		}

		return OrderManagementID;
	}

	public static String CheckOrderStatus(String orderManagementID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Integer orderStatus = 0;
		String statusString = "";

		try {
			sql = "{call sp_GetOrderStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckOrderStatus", user);

			while(jrs.next()){
				orderStatus = jrs.getInt("OrderStatus");
			}

	        switch (orderStatus) {
	            case 0:  statusString = "New";
	                     break;
	            case 1:  statusString = "Committed";
	                     break;
	            case 2:  statusString = "Received";
                		break;
	            case 3:  statusString = "On Process";
	                     break;
	            case 4:  statusString = "Finished";
	                     break;
	            case 10:  statusString = "Cancelled";
	                     break;
	            default: statusString = "";
	                     break;
	        }
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "CheckOrderStatus", sql , user);
		}
		return statusString;
	}

	public static Integer GetOrderStatus(String orderManagementID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Integer orderStatus = 0;

		try {
			sql = "{call sp_GetOrderStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderStatus", user);

			while(jrs.next()){
				orderStatus = jrs.getInt("OrderStatus");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetOrderStatus", sql , user);
		}
		return orderStatus;
	}

	public static Boolean UpdateOrderAndDetailStatus(String orderManagementID, Integer orderStatus, String user){
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";

		try {
			//store 'UpdateOrderStatus' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UpdateOrderStatus(?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", orderStatus));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'UpdateOrderDetailStatus' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UpdateOrderDetailStatus(?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", orderStatus));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "UpdateOrderAndDetailStatus", user);
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOrderAndDetailStatus", sql , user);
		}

		return success;
	}

	public static String GetYellowCard(String shippingInstruction, String user) {
		String token = "";
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			token = APIAdapter.GetEDIToken(user);
			if(token.equals("") || token==null){
				LogAdapter.InsertLogExc("get token failed", "GetYellowCard", sql , user);
				result = "Get Token Failed";
			}
			else{
				model.mdlYellowCardParam yellowCardParam = new model.mdlYellowCardParam();
				yellowCardParam = YellowCardAdapter.GetParamBySI(shippingInstruction, user);
				
				if(yellowCardParam.shippingInstruction == null || yellowCardParam.orderDetail.isEmpty()){
					LogAdapter.InsertLogExc("Get Order Failed", "GetYellowCard", sql , user);
					result = "Get Order Failed";
				}
				else{
					yellowCardParam.setToken(token);
					model.mdlYellowCard yellowCard = new model.mdlYellowCard();
					yellowCard = APIAdapter.GetEDIYellowCard(yellowCardParam, user);
					if(!yellowCard.status){
						LogAdapter.InsertLogExc("Get Yellow Card Failed", "GetYellowCard", sql , user);
						result = yellowCard.getDesc();
					}
					else if(yellowCard.status){
						if(yellowCard.yellowCard != null){
							String SI = yellowCard.getShippingInstruction();
							for(mdlVendorOrderDetail dataYellowCard : yellowCard.yellowCard){
								dataYellowCard.setShippingInstruction(SI);
								model.mdlVendorOrderDetail mdlvendorOrderDetail = new model.mdlVendorOrderDetail();
								String vendorOrderDetailID = "";
								String driverID = "";
								
								mdlvendorOrderDetail = YellowCardAdapter.GetVendorOrderDetailForYellowCard(dataYellowCard, user);
								vendorOrderDetailID = mdlvendorOrderDetail.getVendorOrderDetailID();
								driverID = mdlvendorOrderDetail.getDriverID();
								
								if(vendorOrderDetailID != null && !vendorOrderDetailID.equals("")){
									dataYellowCard.setVendorOrderDetailID(vendorOrderDetailID);
									String updateYCResult = YellowCardAdapter.UpdateYellowCard(dataYellowCard, user);
									if(updateYCResult.equals("Success Update Yellow Card")){
										//-- push notif to mobile
										PushNotifAdapter.PushNotification("yellowcard", driverID, user);
									}
								}
							}
						}
						
						result = "Success Get Yellow Card";
					}
					else{
						LogAdapter.InsertLogExc("Token Expired", "GetYellowCard", sql , user);
						result = "Token Expired";
					}
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetYellowCard", sql , user);
			result = "System Error";
		}

		return result;
	}

	public static void UpdateTpsData(mdlOrderManagement mdlOrderManagement, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_TpsDataUpdate(?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsTransactionID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsTransactionDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsProforma() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsPaymentID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsApprovalCode() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsApprovalDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagement.getTpsBillingAmount() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateTpsData", user);

			result = success == true ? "Success Update Tps Data" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTpsData", sql , user);
			result = "Database Error";
		}

		return;
	}
	
	public static void UpdateTpsDataWithInvoice(mdlOrderManagement mdlOrderManagement, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_TpsDataUpdateWithInvoice2(?,?,?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getOrderManagementID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsTransactionID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsTransactionDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsProforma() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsPaymentID() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsApprovalCode() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsApprovalDate() ));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlOrderManagement.getTpsBillingAmount() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsInvoiceNo() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", user ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlOrderManagement.getTpsLinkInvoice() ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateTpsDataWithInvoice", user);

			result = success == true ? "Success Update Tps Data" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTpsDataWithInvoice", sql , user);
			result = "Database Error";
		}

		return;
	}
	
	public static void UpdateOrderTpsStatus(String orderID, Integer status, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_TpsUpdateStatus(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", status ));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateOrderTpsStatus", user);

			result = success == true ? "Success Update Tps Status" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateOrderTpsStatus", sql , user);
			result = "Database Error";
		}

		return;
	}
	
	public static Integer GetTpsStatus(String orderManagementID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Integer tpsStatus = 0;

		try {
			sql = "{call sp_GetTpsDataByOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetTpsStatus", user);

			while(jrs.next()){
				tpsStatus = jrs.getInt("Tps_BillingStatus");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetTpsStatus", sql , user);
		}
		return tpsStatus;
	}
	
	public static model.mdlOrderManagement LoadTpsDataByOrder(String orderID, String user) {
		model.mdlOrderManagement orderManagement = new model.mdlOrderManagement();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_LoadOrderManagementByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTpsDataByOrder", user);

			while(jrs.next()){
				orderManagement.setTpsTransactionID(jrs.getString("Tps_TxID"));
				orderManagement.setTpsTransactionDate(jrs.getString("Tps_TxDate"));
				orderManagement.setTpsProforma(jrs.getString("Tps_Proforma"));
				orderManagement.setTpsBillingAmount(jrs.getInt("Tps_BillingAmount"));
				orderManagement.setTpsApprovalCode(jrs.getString("Tps_ApprovalCode"));
				orderManagement.setTpsApprovalDate(jrs.getString("Tps_ApprovalDate"));
				orderManagement.setTpsPaymentID(jrs.getString("Tps_PaymentID"));
				orderManagement.setTpsInvoiceNo(jrs.getString("Tps_InvoiceNo"));
				orderManagement.setTpsBillingStatus(jrs.getInt("Tps_BillingStatus"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTpsDataByOrder", sql , user);
		}
		return orderManagement;
	}
	
	public static mdlOrderManagement LoadOrderManagementByID(String orderID, String user) {
		model.mdlOrderManagement orderManagement = new model.mdlOrderManagement();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_LoadOrderManagementByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadOrderManagementByID", user);

			while(jrs.next()){
				orderManagement.setOrderManagementID(jrs.getString("OrderManagementID"));
				orderManagement.setPortID(jrs.getString("PortID"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadOrderManagementByID", sql , user);
		}
		return orderManagement;
	}
	
	public static String getContainerSize(String orderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		String result = "";
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();

		try {
			sql = "{call sp_GetDebitNoteAmountInvoice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDebitNoteAmount", user);
			int i = 1;
			while(jrs.next()){
				if(i > 1) {
					result += ", "+jrs.getString(4);
				}else {
					result += jrs.getString(4);
				}
				i++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return result;
	}
	
}
