package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;
import adapter.Base64Adapter;


public class LoginAdapter {

	public static boolean ValidasiLogin(String lUserId, String lPassword)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		boolean lCheck = false;
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);

		try {
			//check login by main user
			sql = "{call sp_ValidasiLogin(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserId));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "ValidasiLogin", lUserId);

		    lCheck = rowset.next();
		    
		    //check login by staff
		    if(!lCheck){
		    	rowset = null;
		    	sql = "{call sp_Staff_ValidasiLogin(?,?)}";
		    	listParam = new ArrayList<model.mdlQueryExecute>();
				listParam.add(QueryExecuteAdapter.QueryParam("string", lUserId));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));

				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "ValidasiLogin", lUserId);

			    lCheck = rowset.next();
		    }
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "ValidasiLogin", sql , lUserId);
		}

		return lCheck;
	}

//	public static String GetPassword(String lUserId)
//	{
//		String lPassword = "";
//		String lPasswordEnc = "";
//		try {
//			JdbcRowSet jrs = new JdbcRowSetImpl();
//			jrs = database.RowSetAdapter.getJDBCRowSet();
//
//			jrs.setCommand("SELECT Password FROM user_login WHERE UserId = ? LIMIT 1");
//			jrs.setString(1, lUserId);
//			Globals.gCommand = jrs.getCommand();
//			jrs.execute();
//			while(jrs.next()){
//				lPasswordEnc = jrs.getString("Password");
//			}
//
//			lPassword = Base64Adapter.DecriptBase64(lPasswordEnc);
//		    jrs.close();
//		}
//		catch(Exception ex) {
//			LogAdapter.InsertLogExc(ex.toString(), "GetPassword", Globals.gCommand , Globals.user);
//		}
//
//		return lPassword;
//
//	}

}
