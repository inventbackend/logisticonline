package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;


import database.QueryExecuteAdapter;
import model.mdlDebitNoteReimburseDetail;
import model.mdlReimburse;

public class DebitNoteReimburseDetailAdapter {
	
	public static List<mdlDebitNoteReimburseDetail> loadDebitNoteReimburseDetail(String debitNoteNumber){
		List<mdlDebitNoteReimburseDetail> listDebitNoteReimburseDetail = new ArrayList<>();
		List<model.mdlQueryExecute> listParam = new ArrayList<>();
		
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_LoadReimburseDetailByDebitNoteNumber(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "loadDebitNoteReimburseDetail", debitNoteNumber);

			while(jrs.next()){
				mdlDebitNoteReimburseDetail mdlDebitNoteReimburseDetail = new mdlDebitNoteReimburseDetail();
				mdlDebitNoteReimburseDetail.setDebitNoteReimburseID(jrs.getString("DebitNoteReimburseID"));
				mdlDebitNoteReimburseDetail.setNameReimburse(jrs.getString("Name"));
				mdlDebitNoteReimburseDetail.setReimburseAmount((int) jrs.getDouble("ReimburseAmount"));
				listDebitNoteReimburseDetail.add(mdlDebitNoteReimburseDetail);
			}
		}catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "loadDebitNoteReimburseDetail", sql , debitNoteNumber);
		}
		
		return listDebitNoteReimburseDetail;
		
		
		
	}
	
	
	public static String DeleteDebitNoteReimburseDetail(String debitNoteReimburseID, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteDebitNoteReimburseDetail(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteReimburseID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteDebitNoteReimburseDetail", user);

			result = success == true ? "Success Delete Delete Debit Reimburse Detail" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "Delete Debit Note Reimburse Detail", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	
	public static String InsertReimburseDebitNoteReimburseDetail(mdlDebitNoteReimburseDetail debitNoteReimburseDetail, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlDebitNoteReimburseDetail CheckDuplicateReimburse = LoadDebitNoteReimburseDetailByID(debitNoteReimburseDetail.getDebitNoteReimburseID());
			if(CheckDuplicateReimburse.getDebitNoteReimburseID() == null){
				String newReimburseDetailID = CreateDebitNoteReimburseID(user);
				System.out.println(newReimburseDetailID);
				sql = "{call sp_InsertDebitNoteReimburseDetail(?,?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newReimburseDetailID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteReimburseDetail.getDebitNoteNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteReimburseDetail.getReimburseID()));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", debitNoteReimburseDetail.getReimburseAmount()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertReimburseDebitNoteReimburseDetail", user);

				result = success == true ? "Success Insert Debit Note Reimburse Detail" : "Database Error";
			}
			//if duplicate
			else{
				result = "Data sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertReimburseDebitNoteReimburseDetail", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static mdlDebitNoteReimburseDetail LoadDebitNoteReimburseDetailByID(String lDebitNoteReimburseDetailID) {
		mdlDebitNoteReimburseDetail ReimburseDetail = new mdlDebitNoteReimburseDetail();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			sql = "{call sp_LoadDebitNoteReimburseDetailByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDebitNoteReimburseDetailID));

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDebitNoteReimburseDetailByID", lDebitNoteReimburseDetailID);

			while(jrs.next()){
				ReimburseDetail.setDebitNoteReimburseID(jrs.getString("DebitNoteReimburseID"));
				ReimburseDetail.setReimburseAmount(jrs.getInt("ReimburseAmount"));
				ReimburseDetail.setReimburseID(jrs.getString("ReimburseID"));
				ReimburseDetail.setDebitNoteNumber("DebitNoteNumber");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDebitNoteReimburseDetailByID", sql , lDebitNoteReimburseDetailID);
		}

		return ReimburseDetail;
	}
	
	
	public static String CreateDebitNoteReimburseID(String user){
		String lastDebitNoteReimburseDetailID = getLastDebitNoteReimburseDetailID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "0001";

		if ((lastDebitNoteReimburseDetailID != null) && (!lastDebitNoteReimburseDetailID.equals("")) ){
			String[] partsDebitNoteReimburseDetailID = lastDebitNoteReimburseDetailID.split("-");
			String partDate = partsDebitNoteReimburseDetailID[1];
			String partNumber = partsDebitNoteReimburseDetailID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("DNR-").append(datetime).append("-").append(stringInc);
		String debitNoteReimburseDetailID = sb.toString();
		return debitNoteReimburseDetailID;
	}
	
	public static String getLastDebitNoteReimburseDetailID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String debitNoteReimburseDetailID = "";
		try{
			sql = "{call sp_getLastDebitNoteReimburseDetailID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastDebitNoteReimburseDetailID", user);

			while(rowset.next())
			{
				debitNoteReimburseDetailID = rowset.getString("DebitNoteReimburseID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastDebitNoteReimburseDetailID", sql, user);
		}

		return debitNoteReimburseDetailID;
	}

}
