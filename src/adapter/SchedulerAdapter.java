package adapter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import model.mdlKojaParam;
import modelKojaGetLogin.mdlKojaLogin;

public class SchedulerAdapter {

	public static void Scheduler(mdlKojaParam param) {
		Runnable runnable = new Runnable() {
		      public void run() {
		        // task to run goes here
		        APIAdapter.KojaGetFD(param);
		        APIAdapter.KojaGetVesselVoyage(param);
		        /*APIAdapter.KojaContainerTracking(param);*/
		      }
		    };
		    ScheduledExecutorService service = Executors
		                    .newSingleThreadScheduledExecutor();
		    service.scheduleAtFixedRate(runnable, 0, 21600, TimeUnit.SECONDS);
	}
	
	public static void SchedulerContainerTracking(mdlKojaParam param) {
		Runnable runnable = new Runnable() {
		      public void run() {
		        // task to run goes here
		        APIAdapter.KojaContainerTracking(param);
		      }
		    };
		    ScheduledExecutorService service = Executors
		                    .newSingleThreadScheduledExecutor();
		    service.scheduleAtFixedRate(runnable, 0, 7200, TimeUnit.SECONDS);
	}
	
	public static void SchedulerGetCMSEIR(mdlKojaParam param) {
		Runnable runnable = new Runnable() {
		      public void run() {
		        // task to run goes here
		    	APIAdapter.KojaGetCms(param);
		        APIAdapter.KojaGetEir(param);
		      }
		    };
		    ScheduledExecutorService service = Executors
		                    .newSingleThreadScheduledExecutor();
		    service.scheduleAtFixedRate(runnable, 0, 600, TimeUnit.SECONDS);
	}
}
