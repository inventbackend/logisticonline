package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlUser;
import model.mdlUserForgetPassword;

public class UserAdapter {
	
	public static List<model.mdlUser> LoadUser(String user){	
		List<model.mdlUser> listUser = new ArrayList<model.mdlUser>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call sp_LoadUser()}";
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, "LoadUser", user);	
		
			while(rowset.next())
			{
				model.mdlUser User = new model.mdlUser();
				User.Username = rowset.getString("Username");
				User.Password = rowset.getString("Password");
				User.Role = rowset.getString("RoleID");
				User.RoleName = rowset.getString("Name");
				User.IsActive = rowset.getInt("IsActive");
				User.UserId = rowset.getString("UserID");
				listUser.add(User);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUser", sql, user);
		}

		return listUser;
	}
	
	public static List<model.mdlUser> LoadUserByUserID(String lUserID){	
		List<model.mdlUser> listUser = new ArrayList<model.mdlUser>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call sp_LoadUserByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserByUserID", lUserID);	
		
			while(rowset.next())
			{
				model.mdlUser user = new model.mdlUser();
				user.Username = rowset.getString("Username");
				user.Password = rowset.getString("Password");
				user.Role = rowset.getString("RoleID");
				user.RoleName = rowset.getString("Name");
				user.IsActive = rowset.getInt("IsActive");
				user.UserId = rowset.getString("UserID");
				listUser.add(user);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserByUserID", sql, lUserID);
		}

		return listUser;
	}
	
	public static String InsertUser(model.mdlUser lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean check = false;
		String result = "";
		
		try{
			sql = "{call sp_CheckUsername(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckUsername", user);
			//Globals.gCommand = sql;

			while(rowset.next()){
				check = true;
			}

			//if no duplicate username
			if(check==false)
			{
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_InsertUser(?,?,?,?,?)}";
				//Globals.gCommand = sql;
				Boolean success = false;
				
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Role));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.IsActive));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertUser", user);

				result = success == true ? "Success Insert User" : "Database Error";
			}
			//if duplicate
			else {
				result = "The username already exist";
			}
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertUser", sql , user);
			result = "Database error";
		}

		return result;
	}
	
	public static String UpdateUser(model.mdlUser lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateUser(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Role));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.IsActive));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateUser", user);

			result = success == true ? "Success Update User" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteUser(String lUserID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			//sql = "{call sp_DeleteUserByUsername(?)}";
			sql = "{call sp_DeleteUserByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteUser", user);

			result = success == true ? "Success Delete User" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteUser", sql , user);
			result = "Database Error";
		}

		return result;	
	}

//	public static List<model.mdlMenu> LoadRestrictedMenu(String lUserID){	
//		Connection connection = null;
//		PreparedStatement pstm = null;
//		ResultSet rs = null;
//		//Globals.gCommand = "";
//		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
//		try{
//			connection = database.RowSetAdapter.getConnection();
//			pstm = connection.prepareStatement("{call wb_LoadRestrictedMenu(?)}");
//			
//			//insert sql parameter
//			pstm.setString(1,  lUserID);
//			//Globals.gCommand = pstm.toString();
//			pstm.addBatch();
//			pstm.executeBatch();
//			
//			rs = pstm.executeQuery();	
//		
//			while(rs.next())
//			{
//				model.mdlMenu mdlMenu = new model.mdlMenu();				
//				mdlMenu.setMenuID(rs.getString("MenuID"));
//				mdlMenuList.add(mdlMenu);
//			}
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", sql, lUserID);
//		}
//		finally{
//			//close the opened connection
//			try{
//				if (pstm != null) {
//					pstm.close();
//				}
//				if (connection != null) {
//					connection.close();
//				}
//				if (rs != null) {
//					rs.close();
//				}
//				}
//			catch(Exception ex){
//				LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", "close opened connection protocol", lUserID);
//			}
//		}
//
//		return mdlMenuList;
//	}
	
	public static List<model.mdlMenu> LoadAllowedMenuforAccess(String lUserID){	
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
		try{
			sql = "{call sp_LoadRestrictedMenu_2(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadAllowedMenu", lUserID);
		
			while(rowset.next())
			{
				model.mdlMenu mdlMenu = new model.mdlMenu();				
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setRole(rowset.getString("RoleID"));
				mdlMenuList.add(mdlMenu);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadAllowedMenuforAccess", sql, lUserID);
		}

		return mdlMenuList;
	}
	
	public static List<model.mdlMenu> LoadStaffAllowedMenu(String lStaffRole, String user){	
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
		try{
			sql = "{call sp_Staff_LoadAllowedMenu(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lStaffRole));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadStaffAllowedMenu", user);
		
			while(rowset.next())
			{
				model.mdlMenu mdlMenu = new model.mdlMenu();				
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setRole(rowset.getString("RoleID"));
				mdlMenuList.add(mdlMenu);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStaffAllowedMenu", sql, user);
		}

		return mdlMenuList;
	}
	
//	public static String LoadUserArea(String lUserID){	
//		Connection connection = null;
//		PreparedStatement pstm = null;
//		ResultSet rs = null;
//		//Globals.gCommand = "";
//		String AreaID = "";
////		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
//		try{
//			connection = database.RowSetAdapter.getConnection();
//			pstm = connection.prepareStatement("{call wb_LoadUserById(?)}");
//			
//			//insert sql parameter
//			pstm.setString(1,  lUserID);
//			//Globals.gCommand = pstm.toString();
//			pstm.addBatch();
//			pstm.executeBatch();
//			
//			rs = pstm.executeQuery();	
//		
//			while(rs.next())
//			{
//				AreaID = rs.getString("AreaId");
//			}
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", //Globals.gCommand, lUserID);
//		}
//		finally{
//			//close the opened connection
//			try{
//				if (pstm != null) {
//					pstm.close();
//				}
//				if (connection != null) {
//					connection.close();
//				}
//				if (rs != null) {
//					rs.close();
//				}
//				}
//			catch(Exception ex){
//				LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", "close opened connection protocol", lUserID);
//			}
//		}
//
//		return AreaID;
//	}
	
//	public static String LoadUserBrand(String lUserID){	
//		Connection connection = null;
//		PreparedStatement pstm = null;
//		ResultSet rs = null;
//		//Globals.gCommand = "";
//		String BrandID = "";
////		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
//		try{
//			connection = database.RowSetAdapter.getConnection();
//			pstm = connection.prepareStatement("{call wb_LoadUserById(?)}");
//			
//			//insert sql parameter
//			pstm.setString(1,  lUserID);
//			//Globals.gCommand = pstm.toString();
//			pstm.addBatch();
//			pstm.executeBatch();
//			
//			rs = pstm.executeQuery();	
//		
//			while(rs.next())
//			{
//				BrandID = rs.getString("BrandId");
//			}
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "LoadUserBrand", //Globals.gCommand, lUserID);
//		}
//		finally{
//			//close the opened connection
//			try{
//				if (pstm != null) {
//					pstm.close();
//				}
//				if (connection != null) {
//					connection.close();
//				}
//				if (rs != null) {
//					rs.close();
//				}
//				}
//			catch(Exception ex){
//				LogAdapter.InsertLogExc(ex.toString(), "LoadUserBrand", "close opened connection protocol", lUserID);
//			}
//		}
//
//		return BrandID;
//	}

	public static String LoadUserId(String lUsername, String lPassword){	
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		String UserID = "";
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			//check user id from main user
			sql = "{call sp_LoadUserByLogin(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUsername));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserId", lUsername);	
		
			while(rowset.next())
			{
				UserID = rowset.getString("UserID");
			}
			
			//if user id not found, try to check it in staff table
			if(UserID.equals("")){
				rowset = null;
				sql = "{call sp_Staff_LoadUserByLogin(?,?)}";
				listParam = new ArrayList<model.mdlQueryExecute>();
				listParam.add(QueryExecuteAdapter.QueryParam("string", lUsername));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));
				
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserId", lUsername);	
			
				while(rowset.next())
				{
					UserID = rowset.getString("UserID");
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserId", sql, lUsername);
		}

		return UserID;
	}
	
	public static String LoadUserRole(String lUsername, String lPassword){	
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		String UserRole = "";
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			//check role in main user table
			sql = "{call sp_LoadUserByLogin(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUsername));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserRole", lUsername);	
		
			while(rowset.next())
			{
				UserRole = rowset.getString("RoleID");
			}
			
			//if role not found, try to find it in staff table
			if(UserRole.equals("")){
				rowset = null;
				sql = "{call sp_Staff_LoadUserByLogin(?,?)}";
				listParam = new ArrayList<model.mdlQueryExecute>();
				listParam.add(QueryExecuteAdapter.QueryParam("string", lUsername));
				listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));
				
				rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserRole", lUsername);	
			
				while(rowset.next())
				{
					UserRole = rowset.getString("RoleID");
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserRole", sql, lUsername);
		}

		return UserRole;
	}

	public static model.mdlUser LoadStaffByLogin(String lUsername, String lPassword){	
		model.mdlUser staffInfo = new model.mdlUser();
		staffInfo.Staff = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			rowset = null;
			sql = "{call sp_Staff_LoadUserByLogin(?,?)}";
			listParam = new ArrayList<model.mdlQueryExecute>();
			listParam.add(QueryExecuteAdapter.QueryParam("string", lUsername));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lPasswordEnc));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadUserRole", lUsername);	
		
			while(rowset.next())
			{
				staffInfo.UserId = rowset.getString("UserID");
				staffInfo.Staff = rowset.getString("Staff");
				staffInfo.Role = rowset.getString("UserID") + "_" + rowset.getString("Staff");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStaffByLogin", sql, lUsername);
		}

		return staffInfo;
	}
	
	public static List<model.mdlUser> LoadStaffByUser(String UserID, String userAccess){		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlUser> listStaff = new ArrayList<model.mdlUser>();
		try{
			sql = "{call sp_LoadStaffByUser(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", UserID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadStaffByUser", userAccess);
			
			while(rowset.next())
			{
				model.mdlUser staff = new model.mdlUser();
				staff.UserId = rowset.getString("UserID");
				staff.Staff = rowset.getString("Staff");
				staff.Username = rowset.getString("Username");
				staff.IsActive = rowset.getInt("IsActive");
				staff.Password = rowset.getString("Password");
				listStaff.add(staff);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStaffByUser", sql, userAccess);
		}
		
		return listStaff;
	}
	
	public static String InsertStaff(model.mdlUser lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		String result = "";
		
		try{
			listParam = new ArrayList<model.mdlQueryExecute>();
			sql = "{call sp_StaffInsert(?,?,?,?,?)}";
			//Globals.gCommand = sql;
			Boolean success = false;
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Staff));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.IsActive));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));
			
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertStaff", user);

			result = success == true ? "Success Insert Staff" : "Database Error";
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertStaff", sql , user);
			result = "Database error";
		}

		return result;
	}
	
	public static String UpdateStaff(model.mdlUser lParam, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_StaffUpdate(?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Password));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Staff));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.IsActive));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.tempStaff));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId + "_" + lParam.tempStaff));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId + "_" + lParam.Staff));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateStaff", user);

			result = success == true ? "Success Update Staff" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateStaff", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String DeleteStaff(mdlUser lParam, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";
		try{
			sql = "{call sp_StaffDelete(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.Staff));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.UserId + "_" + lParam.Staff));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteStaff", user);

			result = success == true ? "Success Delete Staff" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteStaff", sql , user);
			result = "Database Error";
		}

		return result;	
	}
	
//	public static String LoadUserType(String lUsername, String lPassword){	
//		Connection connection = null;
//		PreparedStatement pstm = null;
//		ResultSet rs = null;
//		//Globals.gCommand = "";
//		String UserType = "";
//		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
//		try{
//			connection = database.RowSetAdapter.getConnection();
//			pstm = connection.prepareStatement("{call wb_LoadUserByLogin(?,?)}");
//			
//			//insert sql parameter
//			pstm.setString(1,  lUsername);
//			pstm.setString(2,  lPasswordEnc);
//			//Globals.gCommand = pstm.toString();
//			pstm.addBatch();
//			pstm.executeBatch();
//			
//			rs = pstm.executeQuery();	
//		
//			while(rs.next())
//			{
//				UserType = rs.getString("Type");
//			}
//		}
//		catch (Exception ex){
//			LogAdapter.InsertLogExc(ex.toString(), "LoadUserType", //Globals.gCommand, lUsername);
//		}
//		finally{
//			//close the opened connection
//			try{
//				if (pstm != null) {
//					pstm.close();
//				}
//				if (connection != null) {
//					connection.close();
//				}
//				if (rs != null) {
//					rs.close();
//				}
//				}
//			catch(Exception ex){
//				LogAdapter.InsertLogExc(ex.toString(), "LoadUserType", "close opened connection protocol", lUsername);
//			}
//		}
//
//		return UserType;
//	}
	
	public static List<mdlUserForgetPassword> checkEmail(String email){
		List<mdlUserForgetPassword> listUser = new ArrayList<mdlUserForgetPassword>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		
		System.out.println(email);
		try{
			rowset = null;
			sql = "{call sp_CheckUserEmail(?)}";
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", email));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "ListUserByForgotPasword", email);	
		
			while(rowset.next())
			{
				mdlUserForgetPassword User = new mdlUserForgetPassword();
				User.UserId = rowset.getString("UserID");
				User.Name = rowset.getString("Name");
				User.Username = rowset.getString("Username");
				User.Email = rowset.getString("Email");
				listUser.add(User);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ListUserByForgotPasword", sql, email);
		}

		return listUser;
	}
	
	public static Boolean changePassword(String password, String userID){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		Boolean success = false;
		Boolean result = false;
		
		try{
			sql = "{call sp_UpdateUserPassword(?,?)}";
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", password));
			listParam.add(QueryExecuteAdapter.QueryParam("string", userID));
			
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateUserPassword", userID);	
		
//			result = success == true ? "Success Update Password" : "Database Error";
			result = success;
			
		} catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateUserPassword", sql, userID);
			result = false;
		}

		return result;
	}
	
}
