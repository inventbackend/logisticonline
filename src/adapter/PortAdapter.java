package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlPort;

public class PortAdapter {
	public static List<model.mdlPort> LoadPort (String user) {
		List<model.mdlPort> listPort = new ArrayList<model.mdlPort>();
		CachedRowSet jrs = null;
		//Globals.gcommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadPort()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadPort", user);

			while(jrs.next()){
				model.mdlPort port = new model.mdlPort();
				port.setPortID(jrs.getString("PortID"));
				port.setPortName(jrs.getString("PortName"));
				port.setPortUTC(jrs.getString("PortUTC"));
				listPort.add(port);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPort", sql , user);
		}

		return listPort;
	}

	public static model.mdlPort LoadPortByID (String lPortID, String user) {
		model.mdlPort mdlPort = new model.mdlPort();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gcommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadPortByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lPortID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadPortByID", user);

			while(jrs.next()){
				mdlPort.setPortID(jrs.getString("PortID"));
				mdlPort.setPortName(jrs.getString("PortName"));
				mdlPort.setPortUTC(jrs.getString("PortUTC"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPortByID", sql , user);
		}

		return mdlPort;
	}

	public static String InsertPort(mdlPort mdlPort, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlPort CheckDuplicatePort = LoadPortByID(mdlPort.getPortID(), user);
			if(CheckDuplicatePort.getPortID() == null){
				String newPortID = CreatePortID(user);
				sql = "{call sp_InsertPort(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newPortID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlPort.getPortName()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlPort.getPortUTC()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertPort", user);

				result = success == true ? "Success Insert Port" : "Database Error";
			}
			//if duplicate
			else{
				result = "Port sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertPort", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdatePort(mdlPort mdlPort, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdatePort(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlPort.getPortID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlPort.getPortName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlPort.getPortUTC()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdatePort", user);

			result = success == true ? "Success Update Port" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdatePort", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeletePort(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeletePort(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeletePort", user);

			result = success == true ? "Success Delete Port" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeletePort", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreatePortID(String user){
		String lastPortID = GetLastPortID(user);
		String stringIncrement = "0001";

		if ((lastPortID != null) && (!lastPortID.equals("")) ){
			String[] partsPortID = lastPortID.split("-");
			//get last running number
			String partNumber = partsPortID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("PORT-").append(stringIncrement);
		String PortID = sb.toString();
		return PortID;
	}

	public static String GetLastPortID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String PortID = "";

		try {
			sql = "{call sp_GetLastPortID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastPortID", user);

			while(jrs.next()){
				PortID = jrs.getString("PortID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastPortID", sql , user);
		}

		return PortID;
	}
}
