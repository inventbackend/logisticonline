package adapter;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.ProductInvoice;
import model.mdlConfirmPaymentDebitNote;
import model.mdlDebitNoteDetail;
import model.mdlDebitNoteReimburseDetail;
import model.mdlDebitNoteSettingInvoiceParam;
import model.mdlProductInvoiceSetting;
import model.mdlReimburse;

public class DebitNoteAdapter {

	public static List<model.mdlDebitNote> LoadDebitNote(String customerID, String dateFrom, String dateTo, String status){
		List<model.mdlDebitNote> listDebitNote = new ArrayList<model.mdlDebitNote>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_DebitNoteLoad(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", status));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDebitNote", customerID);

			while(jrs.next()){
				model.mdlDebitNote mdlDebitNote = new model.mdlDebitNote();
				mdlDebitNote.setPpn(jrs.getInt("PPN"));
				mdlDebitNote.setPph23(jrs.getInt("PPh23"));
				mdlDebitNote.setAdditionalPrice((int) jrs.getDouble("AdditionalPrice"));
				mdlDebitNote.setCustomClearance((int) jrs.getDouble("CustomClearance"));
				mdlDebitNote.setFinalRate((int) jrs.getDouble("FinalRate"));
				mdlDebitNote.setIsReimburse(jrs.getBoolean("isReimburse"));
				mdlDebitNote.setIsFinalInvoice(jrs.getBoolean("isFinalInvoice"));
				mdlDebitNote.setCustomerID(jrs.getString("CustomerID"));
				mdlDebitNote.setCustomerName(jrs.getString("Name"));
				mdlDebitNote.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlDebitNote.setDebitNoteNumber(jrs.getString("DebitNoteNumber"));
				mdlDebitNote.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amount = jrs.getDouble("Amount");
				mdlDebitNote.setAmount(amount.intValue());
				mdlDebitNote.setStatus(jrs.getString("Status"));
				if(jrs.getInt(8) <= 0)
					mdlDebitNote.setOverdue(0);
				else
					mdlDebitNote.setOverdue(jrs.getInt(8));
				if(jrs.getString(9).equals(""))
					mdlDebitNote.setPaymentDate(jrs.getString(9));
				else
					mdlDebitNote.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString(9), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amountPaid = jrs.getDouble(10);
				mdlDebitNote.setAmountPaid(amountPaid.intValue());
				
				//for order detail
				mdlDebitNote.setFactoryAddress(jrs.getString(13));
				mdlDebitNote.setFactoryID(jrs.getString("FactoryID"));
				mdlDebitNote.setFactoryName(jrs.getString(12));
				mdlDebitNote.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				mdlDebitNote.setShippingName(jrs.getString(15));
				mdlDebitNote.setItemDescription(jrs.getString("ItemDescription"));
				mdlDebitNote.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				mdlDebitNote.setVesselName(jrs.getString("VesselName"));
				mdlDebitNote.setVoyageNo(jrs.getString("VoyageNo"));
				mdlDebitNote.setPortName(jrs.getString("PortName") + " - " + jrs.getString("PortUTC"));
				mdlDebitNote.setDepoName(jrs.getString(22));
				mdlDebitNote.setOrderType(jrs.getString("OrderType"));
				mdlDebitNote.setTier(jrs.getString("TierID"));
				
				List<mdlDebitNoteReimburseDetail> debitNoteReimburseDetails = new ArrayList<>();
				List<mdlReimburse> listReimburses = new ArrayList<>();
				List<mdlDebitNoteDetail> listDebitNoteDetail = new ArrayList<>();
				
				debitNoteReimburseDetails = DebitNoteReimburseDetailAdapter.loadDebitNoteReimburseDetail(jrs.getString("DebitNoteNumber"));
				listReimburses = ReimburseAdapter.LoadReimburseAll(customerID);
				listDebitNoteDetail = getDebitNoteDetailByDebitNoteNumber(jrs.getString("DebitNoteNumber"), customerID);
				
				Gson gson = new GsonBuilder().disableHtmlEscaping().create();
				String jsondebitNoteReimburseDetails = gson.toJson(debitNoteReimburseDetails);
				String jsonlistReimburses = gson.toJson(listReimburses);
				String jsonlistDebitNoteDetail = gson.toJson(listDebitNoteDetail);
				
				mdlDebitNote.setDebitNoteReimburseDetails(jsondebitNoteReimburseDetails);
				mdlDebitNote.setReimburses(jsonlistReimburses);
				mdlDebitNote.setDebitNoteNumberDetail(jsonlistDebitNoteDetail.toString());
			    	
				listDebitNote.add(mdlDebitNote);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDebitNote", sql , customerID);
		}
		return listDebitNote;
	}
	
	public static List<model.mdlDebitNote> LoadDebitNoteNotAdmin(String customerID, String dateFrom, String dateTo, String status){
		List<model.mdlDebitNote> listDebitNote = new ArrayList<model.mdlDebitNote>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_DebitNoteLoadNotAdmin(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", status));
			listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDebitNote", customerID);

			while(jrs.next()){
				model.mdlDebitNote mdlDebitNote = new model.mdlDebitNote();
				mdlDebitNote.setPpn(jrs.getInt("PPN"));
				mdlDebitNote.setPph23(jrs.getInt("PPh23"));
				mdlDebitNote.setAdditionalPrice((int) jrs.getDouble("AdditionalPrice"));
				mdlDebitNote.setCustomClearance((int) jrs.getDouble("CustomClearance"));
				mdlDebitNote.setFinalRate((int) jrs.getDouble("FinalRate"));
				mdlDebitNote.setIsReimburse(jrs.getBoolean("isReimburse"));
				mdlDebitNote.setIsFinalInvoice(jrs.getBoolean("isFinalInvoice"));
				mdlDebitNote.setCustomerID(jrs.getString("CustomerID"));
				mdlDebitNote.setCustomerName(jrs.getString("Name"));
				mdlDebitNote.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlDebitNote.setDebitNoteNumber(jrs.getString("DebitNoteNumber"));
				mdlDebitNote.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amount = jrs.getDouble("Amount");
				mdlDebitNote.setAmount(amount.intValue());
				mdlDebitNote.setStatus(jrs.getString("Status"));
				if(jrs.getInt(8) <= 0)
					mdlDebitNote.setOverdue(0);
				else
					mdlDebitNote.setOverdue(jrs.getInt(8));
				if(jrs.getString(9).equals(""))
					mdlDebitNote.setPaymentDate(jrs.getString(9));
				else
					mdlDebitNote.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString(9), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amountPaid = jrs.getDouble(10);
				mdlDebitNote.setAmountPaid(amountPaid.intValue());
				
				//for order detail
				mdlDebitNote.setFactoryAddress(jrs.getString(13));
				mdlDebitNote.setFactoryID(jrs.getString("FactoryID"));
				mdlDebitNote.setFactoryName(jrs.getString(12));
				mdlDebitNote.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				mdlDebitNote.setShippingName(jrs.getString(15));
				mdlDebitNote.setItemDescription(jrs.getString("ItemDescription"));
				mdlDebitNote.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				mdlDebitNote.setVesselName(jrs.getString("VesselName"));
				mdlDebitNote.setVoyageNo(jrs.getString("VoyageNo"));
				mdlDebitNote.setPortName(jrs.getString("PortName") + " - " + jrs.getString("PortUTC"));
				mdlDebitNote.setDepoName(jrs.getString(22));
				mdlDebitNote.setOrderType(jrs.getString("OrderType"));
				mdlDebitNote.setTier(jrs.getString("TierID"));
				
				List<mdlDebitNoteReimburseDetail> debitNoteReimburseDetails = new ArrayList<>();
				List<mdlReimburse> listReimburses = new ArrayList<>();
				debitNoteReimburseDetails = DebitNoteReimburseDetailAdapter.loadDebitNoteReimburseDetail(jrs.getString("DebitNoteNumber"));
				listReimburses = ReimburseAdapter.LoadReimburseAll(customerID);
				
				Gson gson = new Gson();
				String jsondebitNoteReimburseDetails = gson.toJson(debitNoteReimburseDetails);
				String jsonlistReimburses = gson.toJson(listReimburses);
				
				mdlDebitNote.setDebitNoteReimburseDetails(jsondebitNoteReimburseDetails);
				mdlDebitNote.setReimburses(jsonlistReimburses);
			    	
				listDebitNote.add(mdlDebitNote);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDebitNote", sql , customerID);
		}
		return listDebitNote;
	}
	
	public static Integer LoadTotalAmountUnpaid(String customerID, String dateFrom, String dateTo){
		Integer totalAmountUnpaid = 0;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_DebitNoteTotalUnpaid(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTotalAmountUnpaid", customerID);

			while(jrs.next()){
				Double amount = jrs.getDouble(1);
				totalAmountUnpaid = amount.intValue();
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTotalAmountUnpaid", sql , customerID);
		}
		return totalAmountUnpaid;
	}
	
	public static String UpdateStatus(String debitNoteNumber, String status, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DebitNoteStatusUpdate(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", status));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DebitNoteUpdateStatus", user);

			result = success == true ? "Success Update Status" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DebitNoteUpdateStatus", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String ConfirmPayment(String[] debitNoteNumber, String paymentDate, String user)
	{
		List<model.mdlQueryExecute> listParam;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(String dnn : debitNoteNumber){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_DebitNoteConfirmPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", dnn.split("&&")[0]));
				listParam.add(QueryExecuteAdapter.QueryParam("string", paymentDate));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", Integer.valueOf(dnn.split("&&")[1])) );
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPayment", user);
			}

			result = success == true ? "Success Confirm Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String ConfirmFullPayment(List<mdlConfirmPaymentDebitNote> debitNoteNumber, String user)
	{
		List<model.mdlQueryExecute> listParam;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(mdlConfirmPaymentDebitNote dnn : debitNoteNumber){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_DebitNoteConfirmPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", dnn.getDebitNoteNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dnn.getPaymentDate(), "dd MMM yyyy",
						"yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", Integer.valueOf(dnn.getAmountPaid())) );
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPayment", user);
			}

			result = success == true ? "Success Confirm Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String ConfirmPartialPayment(String debitNoteNumber, String paymentDate, Double amountPaid, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DebitNoteConfirmPartialPayment(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", paymentDate ));
			listParam.add(QueryExecuteAdapter.QueryParam("double", amountPaid ));
			
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPartialPayment", user);

			result = success == true ? "Success Confirm Partial Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPartialPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String ConfirmPartialPaymentV2(List<mdlConfirmPaymentDebitNote> debitNoteNumber, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(mdlConfirmPaymentDebitNote dnn : debitNoteNumber){
				sql = "{call sp_DebitNoteConfirmPartialPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", dnn.getDebitNoteNumber() ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dnn.getPaymentDate(), "dd MMM yyyy",
						"yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("double", Double.parseDouble(dnn.getAmountPaid())  ));
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPartialPayment", user);
			}
			

			result = success == true ? "Success Confirm Partial Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPartialPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static model.mdlInvoice LoadInvoice(String debitNoteNumber, String user){
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		int customeClearance = 0, persenPPN = 0, persenPPh23 = 0, totalPPh23=0;
		try{
			sql = "{call sp_DebitNoteLoadInvoice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadInvoice", user);

			while(jrs.next()){
				mdlInvoice.setDebitNoteNumber(jrs.getString("DebitNoteNumber"));
				mdlInvoice.setCustomerID(jrs.getString("CustomerID"));
				mdlInvoice.setCustomer(jrs.getString(4));
				
				String address = "";
				if(jrs.getString("BillingAddress") == null || jrs.getString("BillingAddress").equals(""))
					mdlInvoice.setAddress(jrs.getString("Address"));
				else
					mdlInvoice.setAddress(jrs.getString("BillingAddress"));
				
				mdlInvoice.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlInvoice.setEmail(jrs.getString("Email"));
				mdlInvoice.setOrderID(jrs.getString("OrderManagementID"));
				mdlInvoice.setInvoiceDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setDueDate(ConvertDateTimeHelper.formatDate(jrs.getString(10), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setSiNumber(jrs.getString("ShippingInvoiceID"));
				mdlInvoice.setVesselVoyage(jrs.getString("VesselName") + "/" + jrs.getString("VoyageNo"));
				mdlInvoice.setRoute(jrs.getString(14) + " - " + jrs.getString("PortName") + "(" + jrs.getString("PortUTC") +")");
				
				//get debit note amount detail
				System.out.println("sa"+jrs.getString("OrderManagementID"));
				model.mdlInvoice debitNoteAmountDetail = GetAmountTruckingAndDetail(jrs.getString("OrderManagementID"), user);
				System.out.println(debitNoteAmountDetail.containerQty);
				mdlInvoice.setContainerQty(debitNoteAmountDetail.containerQty);
				mdlInvoice.setDetailTruckingPrice(debitNoteAmountDetail.detailTruckingPrice);
//				mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
				mdlInvoice.setTotalParty(debitNoteAmountDetail.totalParty);
				
				
				persenPPh23 = jrs.getInt("PPh23");
				persenPPN = jrs.getInt("PPN");
				mdlInvoice.setPph23(persenPPh23);
				mdlInvoice.setPpn(persenPPN);
				
				//get invoice config
				List<model.mdlInvoice> listConfig = LoadInvoiceConfig(user);
				
				for(model.mdlInvoice config : listConfig){
					
					if(config.invoiceConfigID.equals("GatePassPrice")) {
//						customeClearance = Integer.valueOf(config.invoiceConfigValue) * mdlInvoice.getTotalParty();
//						mdlInvoice.setCustomClearance(customeClearance);
						
					} else if(config.invoiceConfigID.equals("Pph23")) {
						// merubah pph23 
//						persenPPh23 = Integer.valueOf(config.invoiceConfigValue);
//						pph23 = (((debitNoteAmountDetail.truckingPrice + customeClearance) * persenPPh23)/100);
//						mdlInvoice.setPph23(pph23);
					} else if(config.invoiceConfigID.equals("SignatureName")) {
						mdlInvoice.setSignatureName(config.invoiceConfigValue);
					}
				}
				
				List<mdlDebitNoteReimburseDetail> listReimburseDetail = DebitNoteReimburseDetailAdapter.loadDebitNoteReimburseDetail(debitNoteNumber);
				
				Integer totalReimburse = 0;
				for (mdlDebitNoteReimburseDetail ls : listReimburseDetail) {
					totalReimburse += ls.getReimburseAmount();
				}
				
				mdlInvoice.setAdditionalPrice((int) jrs.getDouble("AdditionalPrice"));
				mdlInvoice.setFinalRate((int) jrs.getDouble("FinalRate"));;
				
				
				mdlProductInvoiceSetting productInvoiceSetting = DebitNoteAdapter.LoadProductInvoiceSetting(debitNoteNumber, user);
				String productInvoiceName = productInvoiceSetting.getProductInvoice();
				
				if(productInvoiceName.equals(ProductInvoice.PACKETA.toString())) {
					mdlInvoice.setProductInvoice("A");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
					
					mdlInvoice.setPpnCustom(((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getCustomClearance() + mdlInvoice.getPpnCustom());
					
					totalPPh23 = (((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPh23)/100);
					mdlInvoice.setTotalPPh23(totalPPh23);
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
	
					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(totalReimburse);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETB.toString())) {
					mdlInvoice.setProductInvoice("B");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETC.toString())) {
					mdlInvoice.setProductInvoice("C");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETD.toString())) {
					mdlInvoice.setProductInvoice("D");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					totalPPh23 = (totalCustomeClearance * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					mdlInvoice.setPpnCustom((totalCustomeClearance * persenPPN)/100);
					
					mdlInvoice.setSubTotal(totalCustomeClearance + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}
				
				
				
				
				
				mdlInvoice.setGrandTotal(mdlInvoice.getTotal() + mdlInvoice.getTotalReimburse() + mdlInvoice.getAdditionalPrice());
				
				//get container number of the order and delivery order detail
				List<model.mdlDeliveryOrder> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(jrs.getString("OrderManagementID"), user);
				List<String> listContainerNumber = new ArrayList<String>();
				for(model.mdlDeliveryOrder componentDO : listDO) {
					String containerNumber = componentDO.containerNumber;
					componentDO.setQuantity(debitNoteAmountDetail.containerQty);
					listContainerNumber.add(containerNumber);
				}
				mdlInvoice.setContainerNumber(listContainerNumber);
				mdlInvoice.setListDO(listDO);
				
				
				
				//get stuffing date of the order
				List<model.mdlOrderManagementDetail> orderDetail = OrderManagementDetailAdapter.LoadOrderManagementDetailByID(jrs.getString("OrderManagementID"), user);
				int i = 0;
				for(model.mdlOrderManagementDetail componentOrderDetail : orderDetail){
					if(i==0)
						mdlInvoice.stuffingDate = componentOrderDetail.getStuffingDate();
					else
						mdlInvoice.stuffingDate += ", " + componentOrderDetail.getStuffingDate();
					
					i++;
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadInvoice", sql , user);
		}
		return mdlInvoice;
	}
	
	public static model.mdlInvoice GetDebitNoteAmount(String orderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		mdlInvoice.truckingPrice = 0;
		mdlInvoice.totalParty = 0;

		try {
			sql = "{call sp_GetDebitNoteAmount(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDebitNoteAmount", user);

			String pattern = "###,###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern);
			String formatCustRate = "";
			
			int i = 0;
			while(jrs.next()){
				if(i==0){
					mdlInvoice.containerQty = jrs.getString(1) + " x " + jrs.getString(4);
					
					formatCustRate = decimalFormat.format(jrs.getDouble(2)).replace(",", ".");
					mdlInvoice.detailTruckingPrice = jrs.getString(1) + " x " + formatCustRate;
				}
				else{
					mdlInvoice.containerQty += ", " + jrs.getString(1) + " x " + jrs.getString(4);
					
					decimalFormat = new DecimalFormat(pattern);
					formatCustRate = decimalFormat.format(jrs.getDouble(2)).replace(",", ".");
					mdlInvoice.detailTruckingPrice += ", " + jrs.getString(1) + " x " + formatCustRate;
				}
				
				Double custRate = jrs.getDouble(2);
				Integer icustRate = custRate.intValue();
				mdlInvoice.truckingPrice += jrs.getInt(1) * icustRate;
				
				mdlInvoice.totalParty += jrs.getInt(1);
				i++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return mdlInvoice;
	}
	
	public static model.mdlInvoice GetDebitNoteAmountInvoice(String orderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		mdlInvoice.truckingPrice = 0;
		mdlInvoice.totalParty = 0;

		try {
			sql = "{call sp_GetDebitNoteAmountInvoice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDebitNoteAmount", user);

			String pattern = "###,###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern);
			String formatCustRate = "";
			
			int i = 0;
			while(jrs.next()){
				if(i==0){
					mdlInvoice.containerQty = jrs.getString(1) + " x " + jrs.getString(4);
					
					formatCustRate = decimalFormat.format(jrs.getDouble(2)).replace(",", ".");
					mdlInvoice.detailTruckingPrice = jrs.getString(1) + " x " + formatCustRate;
				}
				else{
					mdlInvoice.containerQty += ", " + jrs.getString(1) + " x " + jrs.getString(4);
					
					decimalFormat = new DecimalFormat(pattern);
					formatCustRate = decimalFormat.format(jrs.getDouble(2)).replace(",", ".");
					mdlInvoice.detailTruckingPrice += ", " + jrs.getString(1) + " x " + formatCustRate;
				}
				
				Double custRate = jrs.getDouble(2);
				Integer icustRate = custRate.intValue();
				mdlInvoice.truckingPrice += jrs.getInt(1) * icustRate;
				
				mdlInvoice.totalParty += jrs.getInt(1);
				i++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return mdlInvoice;
	}
	
	// khusus untuk debit note
	public static model.mdlInvoice GetAmountTruckingAndDetail(String orderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		CachedRowSet jrs = null;
		String sql = "";
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		mdlInvoice.truckingPrice = 0;
		mdlInvoice.totalParty = 0;

		try {
			sql = "{call sp_GetAmountTruckingAndDetail(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetAmountTruckingAndDetail", user);

			String pattern = "###,###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern);
			String formatCustRate = "";
			
			int i = 0;
			while(jrs.next()){
				
				
				if(i==0){
					mdlInvoice.containerQty = jrs.getInt("Quantity") + " x " + jrs.getString("Description");
					
					formatCustRate = decimalFormat.format(jrs.getDouble("FinalRate")).replace(",", ".");
					mdlInvoice.detailTruckingPrice = jrs.getInt("Quantity") + " x " + formatCustRate;
				}
				else{
					mdlInvoice.containerQty += ", " + jrs.getInt("Quantity") + " x " + jrs.getString("Description");
					
					decimalFormat = new DecimalFormat(pattern);
					formatCustRate = decimalFormat.format(jrs.getDouble("FinalRate")).replace(",", ".");
					mdlInvoice.detailTruckingPrice += ", " + jrs.getInt("Quantity") + " x " + formatCustRate;
				}
				
				Double custRate = jrs.getDouble("FinalRate");
				Integer icustRate = custRate.intValue();
				mdlInvoice.truckingPrice += jrs.getInt("Quantity") * icustRate;
				
				mdlInvoice.totalParty += jrs.getInt("Quantity");
				i++;
			}
			
			
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return mdlInvoice;
	}
	
	public static List<model.mdlInvoice> LoadInvoiceConfig(String user){
		List<model.mdlInvoice> listConfig = new ArrayList<model.mdlInvoice>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_InvoiceConfigLoad()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadInvoiceConfig", user);

			while(jrs.next()){
				model.mdlInvoice config = new model.mdlInvoice();
				config.setInvoiceConfigID(jrs.getString("ID"));
				config.setInvoiceConfigDesc(jrs.getString("Desc"));
				config.setInvoiceConfigValue(jrs.getString("Value"));
			    	
				listConfig.add(config);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadInvoiceConfig", sql , user);
		}
		return listConfig;
	}
	
	public static String UpdateDebitNoteSettingInvoice(mdlDebitNoteSettingInvoiceParam settingParam, String user)
	{
//		String debitNoteNumber, int amount, int isFinalInvoice, int customClearance,
//		int finalRate, int additionalPrice, int ppn, int pph, String user
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateDebitNoteForSetting(?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", settingParam.getDebitNoteNumber()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getAmount()));
			listParam.add(QueryExecuteAdapter.QueryParam("boolean", settingParam.getIsFinalInvoice()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getCustomClearance()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getFinalRate()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getAdditionalPrice()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getPpn()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", settingParam.getPph23()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DebitNoteUpdateStatus", user);

			result = success == true ? "Success Update Setting Invoice" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DebitNoteUpdateStatus", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static mdlProductInvoiceSetting LoadProductInvoiceSetting(String debitNoteNumber, String user){
		mdlProductInvoiceSetting mdlProductInvoiceSetting = new mdlProductInvoiceSetting();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			
			sql = "{call sp_LoadProductInvoiceSetting(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadProductInvoiceSetting", user);

			while(jrs.next()){
				mdlProductInvoiceSetting.setCustomerName(jrs.getString("Name"));
				mdlProductInvoiceSetting.setProductInvoice(jrs.getString("ProductInvoice"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductInvoiceSetting", sql , user);
		}
		return mdlProductInvoiceSetting;
	}
	
	
	public static int amountInvoice(String debitNoteNumber, String user){
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		int customeClearance = 0, persenPPN = 0, persenPPh23 = 0, totalPPh23=0;
		try{
			sql = "{call sp_DebitNoteLoadInvoice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadInvoice", user);

			while(jrs.next()){
				mdlInvoice.setDebitNoteNumber(jrs.getString("DebitNoteNumber"));
				mdlInvoice.setCustomerID(jrs.getString("CustomerID"));
				mdlInvoice.setCustomer(jrs.getString(4));
				
				if(jrs.getString("BillingAddress") == null || jrs.getString("BillingAddress").equals(""))
					mdlInvoice.setAddress(jrs.getString("Address"));
				else
					mdlInvoice.setAddress(jrs.getString("BillingAddress"));
				
				mdlInvoice.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlInvoice.setEmail(jrs.getString("Email"));
				mdlInvoice.setOrderID(jrs.getString("OrderManagementID"));
				mdlInvoice.setInvoiceDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setDueDate(ConvertDateTimeHelper.formatDate(jrs.getString(10), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setSiNumber(jrs.getString("ShippingInvoiceID"));
				mdlInvoice.setVesselVoyage(jrs.getString("VesselName") + "/" + jrs.getString("VoyageNo"));
				mdlInvoice.setRoute(jrs.getString(14) + " - " + jrs.getString("PortName") + "(" + jrs.getString("PortUTC") +")");
				
				model.mdlInvoice debitNoteAmountDetail = GetAmountTruckingAndDetail(jrs.getString("OrderManagementID"), user);
				mdlInvoice.setContainerQty(debitNoteAmountDetail.containerQty);
				mdlInvoice.setDetailTruckingPrice(debitNoteAmountDetail.detailTruckingPrice);
//				mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
				mdlInvoice.setTotalParty(debitNoteAmountDetail.totalParty);
				
				
				persenPPh23 = jrs.getInt("PPh23");
				persenPPN = jrs.getInt("PPN");
				mdlInvoice.setPph23(persenPPh23);
				mdlInvoice.setPpn(persenPPN);
				
				
				
				List<mdlDebitNoteReimburseDetail> listReimburseDetail = DebitNoteReimburseDetailAdapter.loadDebitNoteReimburseDetail(debitNoteNumber);
				
				Integer totalReimburse = 0;
				for (mdlDebitNoteReimburseDetail ls : listReimburseDetail) {
					totalReimburse += ls.getReimburseAmount();
				}
				
				mdlInvoice.setAdditionalPrice((int) jrs.getDouble("AdditionalPrice"));
				mdlInvoice.setFinalRate((int) jrs.getDouble("FinalRate"));;
				
				
				mdlProductInvoiceSetting productInvoiceSetting = DebitNoteAdapter.LoadProductInvoiceSetting(debitNoteNumber, user);
				String productInvoiceName = productInvoiceSetting.getProductInvoice();
				
				if(productInvoiceName.equals(ProductInvoice.PACKETA.toString())) {
					mdlInvoice.setProductInvoice("A");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
					
					mdlInvoice.setPpnCustom(((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getCustomClearance() + mdlInvoice.getPpnCustom());
					
					totalPPh23 = (((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPh23)/100);
					mdlInvoice.setTotalPPh23(totalPPh23);
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
	
					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(totalReimburse);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETB.toString())) {
					mdlInvoice.setProductInvoice("B");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETC.toString())) {
					mdlInvoice.setProductInvoice("C");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETD.toString())) {
					mdlInvoice.setProductInvoice("D");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					totalPPh23 = (totalCustomeClearance * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					mdlInvoice.setPpnCustom((totalCustomeClearance * persenPPN)/100);
					
					mdlInvoice.setSubTotal(totalCustomeClearance + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}
				
				
				
				
				
				mdlInvoice.setGrandTotal(mdlInvoice.getTotal() + mdlInvoice.getTotalReimburse() + mdlInvoice.getAdditionalPrice());
				
				
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "AmountInvoice", sql , user);
		}
		return mdlInvoice.getGrandTotal();
	}
	
	public static List<mdlDebitNoteDetail> getDebitNoteDetailByDebitNoteNumber(String debitNoteNumber, String user){
		List<mdlDebitNoteDetail> listDNDetail = new ArrayList<mdlDebitNoteDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_GetDebitNoteDetailByDebitNoteNumber(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetDebitNoteDetailByDebitNoteNumber", user);
			int no = 1;
			while(jrs.next()){
				mdlDebitNoteDetail dnDetail = new mdlDebitNoteDetail();
				dnDetail.setDebitNoteDetailID(jrs.getString("DebitNoteNumberDetailID"));
				dnDetail.setQuantity(jrs.getInt("Quantity"));
				dnDetail.setFinalRate((int) jrs.getDouble("FinalRate"));
				dnDetail.setDescription(jrs.getString("Description"));
				
				listDNDetail.add(dnDetail);
				no++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "sp_GetDebitNoteDetailByDebitNoteNumber", sql, user);
		}

		return listDNDetail;
	}
	
	public static String updateFinalRateByDebitNoteNumberDetailID(String debitNoteNumberDetailID, Integer finalRate, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateFinalRateByDebitNoteNumberDetailID(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumberDetailID));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", finalRate));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "updateFinalRateByDebitNoteNumberDetailID", user);

			result = success == true ? "Success Update FinalRate" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "updateFinalRateByDebitNoteNumberDetailID", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
}
