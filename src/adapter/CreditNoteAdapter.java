package adapter;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlConfirmPaymentCreditNote;
import model.mdlConfirmPaymentDebitNote;

public class CreditNoteAdapter {

	public static List<model.mdlCreditNote> LoadCreditNote(String vendorID, String dateFrom, String dateTo, String status){
		List<model.mdlCreditNote> listCreditNote = new ArrayList<model.mdlCreditNote>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_CreditNoteLoad(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", status));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadCreditNote", vendorID);

			while(jrs.next()){
				model.mdlCreditNote mdlCreditNote = new model.mdlCreditNote();
				mdlCreditNote.setVendorID(jrs.getString("VendorID"));
				mdlCreditNote.setVendorName(jrs.getString("Name"));
				mdlCreditNote.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlCreditNote.setCreditNoteNumber(jrs.getString("CreditNoteNumber"));
				mdlCreditNote.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amount = jrs.getDouble("Amount");
				mdlCreditNote.setAmount(amount.intValue());
				mdlCreditNote.setStatus(jrs.getString("Status"));
				if(jrs.getString(8).equals(""))
					mdlCreditNote.setPaymentDate(jrs.getString(8));
				else
					mdlCreditNote.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString(8), "yyyy-MM-dd", "dd MMM yyyy"));
				Double amountPaid = jrs.getDouble(9);
				mdlCreditNote.setAmountPaid(amountPaid.intValue());
			    	
				
				listCreditNote.add(mdlCreditNote);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCreditNote", sql , vendorID);
		}
		return listCreditNote;
	}
	
	public static Integer LoadTotalAmountUnpaid(String vendorID, String dateFrom, String dateTo){
		Integer totalAmountUnpaid = 0;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_CreditNoteTotalUnpaid(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTotalAmountUnpaid", vendorID);

			while(jrs.next()){
				Double amount = jrs.getDouble(1);
				totalAmountUnpaid = amount.intValue();
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadTotalAmountUnpaid", sql , vendorID);
		}
		return totalAmountUnpaid;
	}

	public static String ConfirmPayment(String[] creditNoteNumber, String paymentDate, String user)
	{
		List<model.mdlQueryExecute> listParam;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(String cnn : creditNoteNumber){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_CreditNoteConfirmPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", cnn.split("&&")[0]));
				listParam.add(QueryExecuteAdapter.QueryParam("string", paymentDate));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", Integer.valueOf(cnn.split("&&")[1])) );
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPayment", user);
			}

			result = success == true ? "Success Confirm Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String ConfirmFullPayment(List<mdlConfirmPaymentCreditNote> debitNoteNumber, String user)
	{
		List<model.mdlQueryExecute> listParam;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(mdlConfirmPaymentCreditNote cn : debitNoteNumber){
				listParam = new ArrayList<model.mdlQueryExecute>();
				sql = "{call sp_CreditNoteConfirmPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", cn.getCreditNoteNumber()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(cn.getPaymentDate(), "dd MMM yyyy",
						"yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", Integer.valueOf(cn.getAmountPaid())) );
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPayment", user);
			}

			result = success == true ? "Success Confirm Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String ConfirmPartialPayment(String creditNoteNumber, String paymentDate, Double amountPaid, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_CreditNoteConfirmPartialPayment(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", creditNoteNumber ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", paymentDate ));
			listParam.add(QueryExecuteAdapter.QueryParam("double", amountPaid ));
			
			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPartialPayment", user);

			result = success == true ? "Success Confirm Partial Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPartialPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static String ConfirmPartialPaymentV2(List<mdlConfirmPaymentCreditNote> debitNoteNumber, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();;
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			for(mdlConfirmPaymentCreditNote cn : debitNoteNumber){
				sql = "{call sp_CreditNoteConfirmPartialPayment(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", cn.getCreditNoteNumber() ));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(cn.getPaymentDate(), "dd MMM yyyy",
						"yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("double", Double.parseDouble(cn.getAmountPaid())  ));
				
				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ConfirmPartialPayment", user);
			}
			

			result = success == true ? "Success Confirm Partial Payment" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ConfirmPartialPayment", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static model.mdlCreditNote PrintCreditNote(String creditNoteNumber, String user){
		model.mdlCreditNote mdlCreditNote = new model.mdlCreditNote();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		int pph23 = 0, totalTrucking = 0, persenPPh23 = 0;
		try{
			sql = "{call sp_CreditNotePrint(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", creditNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "PrintCreditNote", user);

			while(jrs.next()){
				mdlCreditNote.setCreditNoteNumber(jrs.getString("CreditNoteNumber"));
				mdlCreditNote.setVendorID(jrs.getString("VendorID"));
				mdlCreditNote.setVendorName(jrs.getString(4));
				mdlCreditNote.setAddress(jrs.getString("Address"));
				mdlCreditNote.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlCreditNote.setEmail(jrs.getString("Email"));
				mdlCreditNote.setOrderManagementID(jrs.getString("OrderManagementID"));
				mdlCreditNote.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlCreditNote.setSiNumber(jrs.getString("ShippingInvoiceID"));
				mdlCreditNote.setVesselVoyage(jrs.getString("VesselName") + "/" + jrs.getString("VoyageNo"));
				mdlCreditNote.setRoute(jrs.getString(12) + " - " + jrs.getString("PortName") + "(" + jrs.getString("PortUTC") +")");
				mdlCreditNote.setContainerQty(jrs.getInt("Quantity") + " x " + jrs.getString("Description"));
				
				Double truckingPrice = jrs.getDouble(21);
				Integer iTruckingPrice = truckingPrice.intValue();
				totalTrucking = iTruckingPrice;
				mdlCreditNote.setTruckingPrice(iTruckingPrice * jrs.getInt("Quantity"));
				
				/*Integer pricePerContainer = iTruckingPrice / jrs.getInt("Quantity");*/
				Integer pricePerContainer = iTruckingPrice;
				String pattern = "###,###";
				DecimalFormat decimalFormat = new DecimalFormat(pattern);
				String formatTruckerRate = "";
				formatTruckerRate = decimalFormat.format(pricePerContainer).replace(",", ".");
				mdlCreditNote.setDetailTruckingPrice(jrs.getInt("Quantity") + " x " + formatTruckerRate);
				
				//get invoice config
				List<model.mdlInvoice> listConfig = DebitNoteAdapter.LoadInvoiceConfig(user);
				for(model.mdlInvoice config : listConfig){
					if(config.invoiceConfigID.equals("Pph23")) {
						persenPPh23 = Integer.valueOf(config.invoiceConfigValue);
						pph23 = (mdlCreditNote.getTruckingPrice() * 2)/100;
						mdlCreditNote.setPph23(pph23);
					} else if(config.invoiceConfigID.equals("SignatureName")) {
						mdlCreditNote.setSignatureName(config.invoiceConfigValue);
					}
				}
								
				mdlCreditNote.setGrandTotal(mdlCreditNote.getTruckingPrice() - mdlCreditNote.getPph23());
				
				//get container number of the order
				List<model.mdlVendorOrderDetail> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByID(jrs.getString("VendorOrderID"), user);
				List<String> listContainerNumber = new ArrayList<String>();
				for(model.mdlVendorOrderDetail componentDO : listDO){
					String containerNumber = componentDO.containerNumber;
					componentDO.setQuantity(mdlCreditNote.getContainerQty());
					listContainerNumber.add(containerNumber);
				}		
				mdlCreditNote.setContainerNumber(listContainerNumber);
				mdlCreditNote.setListDO(listDO);
				
				
				mdlCreditNote.setStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "PrintCreditNote", sql , user);
		}
		return mdlCreditNote;
	}
	
}
