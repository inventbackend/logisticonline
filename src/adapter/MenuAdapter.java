package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import database.QueryExecuteAdapter;
import model.Globals;

public class MenuAdapter {

	public static List<model.mdlMenu> LoadMenu(String user)  {
		List<model.mdlQueryExecute> listParam;
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		//Globals.gCommand = "";
		try{
			sql = "{call sp_LoadMenu_lv0()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "LoadMenu", user);
			//Globals.gCommand = sql;
			
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setMenuUrl(rowset.getString("Url"));
				mdlMenu.setType(rowset.getString("Type"));
				mdlMenu.setLevel(rowset.getString("Level"));
				listmdlMenu.add(mdlMenu);
				
				CachedRowSet rowset2 = null;
				listParam = new ArrayList<model.mdlQueryExecute>();
				String sqlLoadMenuLv1 = "{call sp_LoadMenu_lv1(?)}";
				sql = sqlLoadMenuLv1;
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenu.getType()));
				rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, "LoadMenu", user);
				
				while(rowset2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+rowset2.getString("Name"));
					mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
					mdlMenuLv1.setType(rowset2.getString("Type"));
					mdlMenuLv1.setLevel(rowset2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenu", sql, user);
		}
		
		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadMenuByID(String lMenuID,String lCommand, String user) {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		//Globals.gCommand = "";
		
		try{
			sql = "{call sp_LoadMenuById(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lMenuID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadMenuByID", user);
			//Globals.gCommand = sql;
									
			while(rowset.next()){
				
				String menuLevel = rowset.getString("Level");
				CachedRowSet rowset2 = null;
				listParam = new ArrayList<model.mdlQueryExecute>();
				if(menuLevel.contentEquals("0"))
				{
					String sqlLoadMenuLv0 = "{call sp_LoadMenuById_lv0(?)}";
					listParam.add(QueryExecuteAdapter.QueryParam("string", rowset.getString("Type")));

					rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv0, listParam, "LoadMenuByID", user);
					sql = sqlLoadMenuLv0;
					
					while(rowset2.next())
					{
						model.mdlMenu mdlMenuLv0 = new model.mdlMenu();
					
						mdlMenuLv0.setMenuID(rowset2.getString("MenuID"));
						
						if(rowset2.getString("Level").contentEquals("0"))
							mdlMenuLv0.setMenuName(rowset2.getString("Name"));
						else
							mdlMenuLv0.setMenuName("- " + rowset2.getString("Name"));
							
						mdlMenuLv0.setMenuUrl(rowset2.getString("Url"));
						mdlMenuLv0.setType(rowset2.getString("Type"));
						mdlMenuLv0.setLevel(rowset2.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv0);
					}
				}
				else if(menuLevel.contentEquals("1"))
				{
					String sqlLoadMenuLv1 = "";
					if(lCommand.contentEquals("Add"))
					{
						sqlLoadMenuLv1 = "{call sp_LoadMenuById_lv1(?,?)}";
						listParam.add(QueryExecuteAdapter.QueryParam("string", "%" + rowset.getString("MenuID") + "%"));
						listParam.add(QueryExecuteAdapter.QueryParam("string", rowset.getString("Type")));
						
						rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, "LoadMenuByID", user);
					}
					else
					{
						sqlLoadMenuLv1 = "{call sp_LoadMenuById_lv1_1(?)}";
						listParam.add(QueryExecuteAdapter.QueryParam("string", "%" + rowset.getString("MenuID") + "%"));
						
						rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, "LoadMenuByID", user);
					}
					sql = sqlLoadMenuLv1;
					
					while(rowset2.next())
					{
						model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					
						mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
						
						if(rowset2.getString("Level").contentEquals("0"))
							mdlMenuLv1.setMenuName(rowset2.getString("Name"));
						else
							mdlMenuLv1.setMenuName("- " + rowset2.getString("Name"));
							
						mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
						mdlMenuLv1.setType(rowset2.getString("Type"));
						mdlMenuLv1.setLevel(rowset2.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv1);
					}
				}
			}			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenuByID", sql, user);
		}
		
		return listmdlMenu;
	}
	
	public static String InsertUserRule(String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID, String user) 
	{	
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean check = false;
		Boolean success = false;
		String result = "";
			
		try{
			sql = "{call sp_LoadRoleById(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "InsertUserRole", user);
			//Globals.gCommand = sql;
			
			while(rowset.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
					
					//store 'InsertRole' parameter for transaction
					model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_InsertRole(?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleName)));
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
						
					//store 'InsertAccessRole' parameter for transaction
					for(String lmenuid : llistAllMenuID)
					{
						mdlQueryTransaction = new model.mdlQueryTransaction();
						mdlQueryTransaction.sql = "{call sp_InsertAccessRole(?,?,?,?,?)}";
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
						sql = mdlQueryTransaction.sql;
						listmdlQueryTransaction.add(mdlQueryTransaction);
					}
					
					//store 'UpdateIsAccess' parameter for transaction
					for(String lmenuid : llistAllowedMenuID)
					{
						mdlQueryTransaction = new model.mdlQueryTransaction();
						mdlQueryTransaction.sql = "{call sp_UpdateIsAccess(?,?,?,?)}";
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
						sql = mdlQueryTransaction.sql;
						listmdlQueryTransaction.add(mdlQueryTransaction);
					}
						
					//store 'UpdateIsModify' parameter for transaction
					for(String lmenuid : llistEditableMenuID)
					{
						mdlQueryTransaction = new model.mdlQueryTransaction();
						mdlQueryTransaction.sql = "{call sp_UpdateIsModify(?,?,?)}";
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
						mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
						sql = mdlQueryTransaction.sql;
						listmdlQueryTransaction.add(mdlQueryTransaction);
					}
					
					success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertUserRule", user);
					if(success)
						result = "SuccessInsert";
					else
						result = "Database Error";
				}
				//if duplicate
				else {
					result = "The Role Id Already Exist";
				}
		}
		catch (Exception e) {
		        LogAdapter.InsertLogExc(e.toString(), "InsertUserRule", sql , user);
    			result = "Database Error";
			}
			
		return result;
	}
	
	public static String UpdateUserRule( String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID, String user){	
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";
		
		try{
			mdlQueryTransaction.sql = "{call sp_UpdateIsModifyAll(?,?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleName)));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_UpdateIsAccessAll(?,?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "UpdateUserRule", user);
			if(success)
				result = "SuccessUpdate";
			else
				result = "Database Error";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "UpdateUserRule", sql , user);
		    result = "Database Error";
		}
		
		try{
			listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
			//store 'UpdateIsAccess' parameter	for transaction
			if (llistAllowedMenuID.length != 0)
			{
				for(String lmenuid : llistAllowedMenuID)
				{	
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_UpdateIsAccess(?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				}
			}
			
			//store 'UpdateIsModify' parameter for transaction	
			if (llistEditableMenuID.length != 0){
				for(String lmenuid : llistEditableMenuID)
				{
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_UpdateIsModify(?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
					sql = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
				}
			}
			
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "UpdateUserRule", user);
			if(success)
				result = "SuccessUpdate";
			else
				result = "Database Error";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "UpdateUserRule", sql , user);
		    result = "Database Error";
		}
		
		return result;
	}
	
	public static String DeleteUserRule(String lRoleID, String user) 
	{	
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";
			
		try{
			//store 'DeleteRole' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteRole(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'DeleteAccessRole' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteAccessRole(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "DeleteUserRule", user);
			if(success)
				result = "SuccessDelete";
			else
				result = "Database Error";
		}	
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "DeleteUserRule", sql , user);
			result = "Database Error";
		}
			
		return result;
	}

	public static List<model.mdlMenu> LoadAllowedMenu(String lRoleID, String user) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call sp_LoadAllowedMenu(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadAllowedMenu", user);
			//Globals.gCommand = sql.toString();
									
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setMenuUrl(rowset.getString("Url"));
				mdlMenu.setType(rowset.getString("Type"));
				mdlMenu.setLevel(rowset.getString("Level"));
				listmdlMenu.add(mdlMenu);
				
				CachedRowSet rowset2 = null;
				listParam = new ArrayList<model.mdlQueryExecute>();
				String sql2 = "{call sp_LoadAllowedMenu_lv1(?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenu.getType()));
				rowset2 = QueryExecuteAdapter.QueryExecute(sql2, listParam, "LoadAllowedMenu", user);
				sql = sql2;
				
				while(rowset2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+rowset2.getString("Name"));
					mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
					mdlMenuLv1.setType(rowset2.getString("Type"));
					mdlMenuLv1.setLevel(rowset2.getString("Level"));
					listmdlMenu.add(mdlMenuLv1);
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadAllowedMenu", sql , user);
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadCRUDMenu()  {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		try{
			sql = "{call sp_LoadCRUDMenu()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "LoadCRUDMenu", "");
									
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setMenuUrl(rowset.getString("Url"));
				mdlMenu.setType(rowset.getString("Type"));
				mdlMenu.setLevel(rowset.getString("Level"));
				mdlMenu.setCRUD(rowset.getBoolean("CRUD"));
				mdlMenu.setIsModify(false);
				
				listmdlMenu.add(mdlMenu);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCRUDMenu", sql , "");
		}

		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadEditableMenu() {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		
		try{
		//			sql = "{call sp_LoadEditableMenu()}";
		//			rowset = QueryExecuteAdapter.QueryExecute(sql,"LoadEditableMenu");
		//									
		//			while(rowset.next()){
		//				model.mdlMenu mdlMenu = new model.mdlMenu();
		//				mdlMenu.setMenuID(rowset.getString("MenuID"));
		//				mdlMenu.setMenuName(rowset.getString("Name"));
		//				mdlMenu.setIsModify(rowset.getBoolean("IsModify"));
		//				
		//				listmdlMenu.add(mdlMenu);
		//			}
		//			if(listmdlMenu.size() == 0)
		//			{
				listmdlMenu = LoadCRUDMenu();
		//			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", sql , "");
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadEditableMenu(String lRoleID, String user) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		
		try{
			sql = "{call sp_LoadEditableMenubyRoleID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadEditableMenu", user);
			//Globals.gCommand = sql;
									
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setIsModify(rowset.getBoolean("IsModify"));
				
				listmdlMenu.add(mdlMenu);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", sql , user);
		}

		return listmdlMenu;
	}

	public static Boolean CheckMenu(String MenuURL, String UserID)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean CheckMenu = false;
		try{
			
			sql = "{call sp_LoadMenu_IsAccess_Status(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
			listParam.add(QueryExecuteAdapter.QueryParam("string", UserID));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckMenu", UserID);
									
			while(rowset.next()){
				if(rowset.getBoolean("IsAccess")==true)
				{
					CheckMenu = true;
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckMenu", sql , UserID);
		}

		return CheckMenu;
	}
	
	public static Boolean CheckStaffMenu(String MenuURL, String RoleID)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Boolean CheckMenu = false;
		try{
			
			sql = "{call sp_Staff_CheckMenuIsAccess(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
			listParam.add(QueryExecuteAdapter.QueryParam("string", RoleID));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckStaffMenu", RoleID);
									
			while(rowset.next()){
				CheckMenu = true;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckStaffMenu", sql , RoleID);
		}

		return CheckMenu;
	}

	public static String SetMenuButtonStatus(String MenuURL, String UserID)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		//String ButtonStatus = "enabled";
		String ButtonStatus = "";
		try{
			sql = "{call sp_LoadMenu_IsModify_Status(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
			listParam.add(QueryExecuteAdapter.QueryParam("string", UserID));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "SetMenuButtonStatus", UserID);
									
			while(rowset.next()){
				if(rowset.getBoolean("IsModify")==false)
				{
					//ButtonStatus = "disabled";
					ButtonStatus = "none";
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "SetMenuButtonStatus", sql , UserID);
		}

		return ButtonStatus;
	}
	
	public static String SetStaffMenuButtonStatus(String MenuURL, String RoleID)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		String ButtonStatus = "";
		try{
			sql = "{call sp_Staff_CheckMenuIsModify(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
			listParam.add(QueryExecuteAdapter.QueryParam("string", RoleID));
			
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "SetStaffMenuButtonStatus", RoleID);
									
			while(rowset.next()){
				if(rowset.getBoolean("IsModify")==false)
				{
					ButtonStatus = "none";
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "SetStaffMenuButtonStatus", sql , RoleID);
		}

		return ButtonStatus;
	}
	
	public static List<model.mdlMenu> LoadStaffMenu(String userid, String userAccess)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		//Globals.gCommand = "";
		try{
			sql = "{call sp_Staff_LoadMenu_lv0(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", userid));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadStaffMenu", userAccess);
			
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setMenuUrl(rowset.getString("Url"));
				mdlMenu.setType(rowset.getString("Type"));
				mdlMenu.setLevel(rowset.getString("Level"));
				listmdlMenu.add(mdlMenu);
				
				CachedRowSet rowset2 = null;
				listParam = new ArrayList<model.mdlQueryExecute>();
				String sqlLoadMenuLv1 = "{call sp_Staff_LoadMenu_lv1(?,?)}";
				sql = sqlLoadMenuLv1;
				listParam.add(QueryExecuteAdapter.QueryParam("string", userid));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenu.getType()));
				rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, "LoadMenu", userAccess);
				
				while(rowset2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+rowset2.getString("Name"));
					mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
					mdlMenuLv1.setType(rowset2.getString("Type"));
					mdlMenuLv1.setLevel(rowset2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
				}
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStaffMenu", sql, userAccess);
		}
		
		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadStaffEditableMenu(String userid, String userAccess)  {
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		try{
			sql = "{call sp_Staff_LoadEditableMenu(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", userid));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadStaffEditableMenu", userAccess);
			
			while(rowset.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(rowset.getString("MenuID"));
				mdlMenu.setMenuName(rowset.getString("Name"));
				mdlMenu.setMenuUrl(rowset.getString("Url"));
				mdlMenu.setType(rowset.getString("Type"));
				mdlMenu.setLevel(rowset.getString("Level"));
				listmdlMenu.add(mdlMenu);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStaffEditableMenu", sql, userAccess);
		}
		
		return listmdlMenu;
	}
	
	public static String SaveStaffRole(String lRoleID, String[] llistAllowedMenuID, String[] llistEditableMenuID, String user) 
	{	
		String sql="";
		Boolean success = false;
		String result = "";
			
		try{
			List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
			model.mdlQueryTransaction mdlQueryTransaction;
				
			//store 'DeleteAccessRole' parameter for transaction
			mdlQueryTransaction = new model.mdlQueryTransaction();
			mdlQueryTransaction.sql = "{call sp_DeleteAccessRole(?)}";
			mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
			sql = mdlQueryTransaction.sql;
			listmdlQueryTransaction.add(mdlQueryTransaction);
			
			//store 'UpdateIsAccess' parameter for transaction
			for(String lmenuid : llistAllowedMenuID)
			{
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertAccessRole(?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
			}
				
			//store 'UpdateIsModify' parameter for transaction
			for(String lmenuid : llistEditableMenuID)
			{
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_UpdateIsModify(?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
			}
			
			success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "SaveStaffRole", user);
			if(success)
				result = "SuccessSave";
			else
				result = "Database Error";
		}
		catch (Exception e) {
		        LogAdapter.InsertLogExc(e.toString(), "SaveStaffRole", sql , user);
    			result = "Database Error";
			}
			
		return result;
	}
	
}
