package adapter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.apache.poi.ss.formula.functions.Count;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.json.XML;


import com.fasterxml.jackson.xml.XmlMapper;
import org.json.JSONObject;
import org.json.XML;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.mail.imap.protocol.ENVELOPE;

import helper.ConvertDateTimeHelper;
import javafx.geometry.VerticalDirection;
import model.Globals;
import model.mdlDeliveryOrder;
import model.mdlKojaParam;
import model.mdlKojaTransaction;
import model.mdlOrderManagement;
import model.mdlSendBilling;
import model.mdlShipping;
import model.mdlVendorOrderDetail;
import model.mdlYellowCardParam;
import modelKojaAmmendAssociate.mdlAmmendAssociateContainer;
import modelKojaAmmendAssociate.mdlAmmendAssociatedData;
import modelKojaAssociateContainer.mdlAssociateContainer;
import modelKojaAssociateContainer.mdlAssociatedData;
import modelKojaAssociateTicket.mdlAssociateTicketParam;
import modelKojaAssociateTicket.mdlResponseAssociateTicket;
import modelKojaConfirmTransaction.mdlConfirmTransaction;
import modelKojaContainerTracking.mdlContainerData;
import modelKojaContainerTracking.mdlContainerTracking;
import modelKojaGetBilling.mdlGetBilling;
import modelKojaGetBillingDetail.DETAILBILLING;
import modelKojaGetBillingDetail.Ns1BILLINGGetBillingDetailResponse;
import modelKojaGetBillingDetail.Return;
import modelKojaGetBillingDetail.SUMMARYDETAIL;
import modelKojaGetCms.mdlGetCms;
import modelKojaGetCustomer.mdlGetCustomer;
import modelKojaGetDocNpe.mdlGetDocNpe;
import modelKojaGetEir.mdlGetEir;
import modelKojaGetFD.mdlKojaFD;
import modelKojaGetInvoice.mdlGetInvoice;
import modelKojaGetLogin.mdlKojaLogin;
import modelKojaGetOTP.mdlGetOTP;
import modelKojaGetOnDemand.mdlGetOnDemandParam;
import modelKojaGetOwner.mdlOwner;
import modelKojaGetProforma.mdlGetProforma;
import modelKojaGetVesselVoyage.mdlKojaVesselVoyage;
import modelKojaRequestInquiry.Envelope;
import modelKojaRequestInquiry.mdlPaymentFlagging;
import modelKojaRequestInquiry.mdlRequestInquiry;
import modelKojaSaveCustomer.mdlSaveCustomer;
import modelKojaTIDValidation.mdlTIDValidation;
import modelKojaTIDValidation.mdlTruckData;
import modelKojaTpsManual.mdlKojaTpsManual;

public class APIAdapter {

	public static String GetEDIToken(String user){
	    String entityResponse = "";
	    String token = "";
	    String clientID = "";
	    String clientSecret = "";
	    String getTokenUrl = "";
	    model.mdlEDIToken mdlEDIToken = new model.mdlEDIToken();
        
        try {        
        	// Get the base naming context from web.xml
    		Context web_context = (Context)new InitialContext().lookup("java:comp/env");
    		
    		model.mdlEDITokenParam param = new model.mdlEDITokenParam();
    	    param.grant_type = "client_credentials";
    	    param.client_id  = (String)web_context.lookup("param_client_id");
    	    param.client_secret  = (String)web_context.lookup("param_client_secret");
    	    getTokenUrl  = (String)web_context.lookup("param_url_gettoken");
    	    
            Gson gson = new Gson();    
            Client client = Client.create();
            
            WebResource webResource = client.resource(getTokenUrl);
            String json = gson.toJson(param);
            //String appKey = "Bearer " + lParam.Token; // appKey is unique number

        
            ClientResponse response  = webResource.type("application/json").accept("application/json")                                          
                                                  .post(ClientResponse.class,json);        
            
            entityResponse = response.getEntity(String.class);
            
            mdlEDIToken = gson.fromJson(entityResponse, model.mdlEDIToken.class);
            
            token = mdlEDIToken.access_token;
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetEDIToken", getTokenUrl , user);
            token = "";
          }
        
        return token;
     }
	
	public static model.mdlYellowCard GetEDIYellowCard(mdlYellowCardParam param, String user){
	    String entityResponse = "";
	    String token = param.getToken();
	    String getYellowCardUrl = "";
	    model.mdlYellowCard yellowCard = new model.mdlYellowCard();
        
        try {        
        	// Get the base naming context from web.xml
    		Context web_context = (Context)new InitialContext().lookup("java:comp/env");
    		getYellowCardUrl  = (String)web_context.lookup("param_url_getyellowcard");
    	    
            Gson gson = new Gson();    
            Client client = Client.create();
            
            WebResource webResource = client.resource(getYellowCardUrl);
            String json = gson.toJson(param);
            String appKey = "Bearer " + token; // appKey is unique number

        
            ClientResponse response  = webResource.type("application/json").accept("application/json")
            									.header("Authorization", appKey)
                                                  .post(ClientResponse.class,json);        
            
            entityResponse = response.getEntity(String.class);
            
            yellowCard = gson.fromJson(entityResponse, model.mdlYellowCard.class);
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetEDIYellowCard", getYellowCardUrl , user);
            yellowCard = new model.mdlYellowCard();
          }
        
        return yellowCard;
     }
	
	public static void KojaGetLogin(mdlKojaParam param){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:AUTH_GetLoginwsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:AUTH_GetLogin soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param.getPassword()+"</Password>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"PASSWORD\":\""+param.getFstreampassword()+"\",\"USERNAME\":\""+param.getFstreamusername()+"\"}</fStream>"+
			                  "<deviceName xsi:type=\"xsd:string\">"+param.getDeviceName()+"</deviceName>"+
			               "</urn:AUTH_GetLogin>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetLogin.Envelope envelope = new modelKojaGetLogin.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetLogin.Envelope.class);
            
            mdlKojaLogin dataKojaLogin = new mdlKojaLogin();
            dataKojaLogin =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1AUTHGetLoginResponse().getReturn().getContent(), mdlKojaLogin.class);
            if(dataKojaLogin.getSTATUS().equals("FALSE")){
            	param.session = KojaAdapter.GetSession(param.username);
            	
            	KojaGetLogout(param);
            	KojaGetLogin(param);
            }
            else if(dataKojaLogin.getSTATUS().equals("TRUE")){
            	KojaAdapter.UpdateSession(dataKojaLogin.getSESSIONID(), dataKojaLogin.getCUST_ID(), param.username);
            }
            
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetLogin", getServiceUrl + " " + requestBody , param.username);
          }
        
        return;
     }
	
	public static void KojaGetLogout(mdlKojaParam param){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:AUTH_GetLogOutwsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:AUTH_GetLogOut soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param.getPassword()+"</Password>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"SESSIONID\":\""+param.getSession()+"\"}</fStream>"+
			               "</urn:AUTH_GetLogOut>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            /*entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);*/
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetLogout", getServiceUrl + " " + requestBody, param.username);
          }
        
        return;
     }
	
	public static void KojaGetFD(mdlKojaParam param){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param.session = KojaAdapter.GetSession(param.username);
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_GetFDwsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:MAIN_GetFD soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param.getPassword()+"</Password>"+
			                  "<Creator xsi:type=\"xsd:string\">"+param.getSession()+"</Creator>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"PORT_NAME\":\"I\"}</fStream>"+
			               "</urn:MAIN_GetFD>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetFD.Envelope envelope = new modelKojaGetFD.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetFD.Envelope.class);
            
            mdlKojaFD dataKojaFD = new mdlKojaFD();
            dataKojaFD =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAINGetFDResponse().getReturn().getContent(), mdlKojaFD.class);
            if(dataKojaFD.getSTATUS().equals("FALSE")){
            	KojaGetLogin(param);
            	KojaGetFD(param);
            }
            else if(dataKojaFD.getSTATUS().equals("TRUE")){
            	dataKojaFD.setTerminalID("KOJA");
            	KojaAdapter.InsertFD(dataKojaFD, param.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetFD", getServiceUrl + " " + requestBody, param.username);
          }
        
        return;
     }
	
	public static void KojaGetVesselVoyage(mdlKojaParam param){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param.session = KojaAdapter.GetSession(param.username);
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_GetVesselVoyagewsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:MAIN_GetVesselVoyage soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param.getPassword()+"</Password>"+
			                  "<Creator xsi:type=\"xsd:string\">"+param.getSession()+"</Creator>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"CATEGORY_ID\":\"E\",\"VOYID_VOYAGE_CODE\":\"\",\"VOYID_COMPANY_CODE\":\"\",\"VESSEL_NAME\":\"\",\"TERMINAL_ID\":\"KOJA\",\"VOYID_VESSEL_CODE\":\"\"}</fStream>"+
			               "</urn:MAIN_GetVesselVoyage>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetVesselVoyage.Envelope envelope = new modelKojaGetVesselVoyage.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetVesselVoyage.Envelope.class);
            
            mdlKojaVesselVoyage dataKojaVesselVoyage = new mdlKojaVesselVoyage();
            dataKojaVesselVoyage =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAINGetVesselVoyageResponse().getReturn().getContent(), mdlKojaVesselVoyage.class);
            if(dataKojaVesselVoyage.getSTATUS().equals("FALSE") && dataKojaVesselVoyage.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param);
            	KojaGetVesselVoyage(param);
            }
            else if(dataKojaVesselVoyage.getSTATUS().equals("FALSE") && dataKojaVesselVoyage.getMESSAGE().contains("Data tidak ditemukan")){
            	LogAdapter.InsertLogExc(dataKojaVesselVoyage.MESSAGE, "KojaGetVesselVoyage", getServiceUrl + " " + requestBody, param.username);
            }
            else if(dataKojaVesselVoyage.getSTATUS().equals("TRUE")){
            	dataKojaVesselVoyage.setTerminalID("KOJA");
            	KojaAdapter.InsertVesselVoyage2(dataKojaVesselVoyage, param.username);
            	KojaGetOwner(param, dataKojaVesselVoyage);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetVesselVoyage", getServiceUrl + " " + requestBody, param.username);
          }
        
        return;
     }
	
	public static void KojaGetOwner(mdlKojaParam param, mdlKojaVesselVoyage paramVessel){
		String entityResponse = "";
	    String getServiceUrl = "";
	    /*param.session = KojaAdapter.GetSession(param.username);*/
	    
	    String[] listVesselCode = paramVessel.getVOYID_VESSEL_CODE();
	    String[] listVoyageCode = paramVessel.getVOYID_VOYAGE_CODE();
	    int i = 0;
	    
	    /*ShippingAdapter.DeleteShippingByTerminal("KOJA", param.username);*/
	    for(String id : listVesselCode){
	    	String vesselID = id;
	    	String voyageCode = listVoyageCode[i];
	    	
	    	i++;
	    	
	    	String requestBody = "";
	        
	        try {        
	        	// Get the base naming context from web.xml
	        	getServiceUrl  = param.getServiceUrl();
	    	    
	        	Gson gson = new Gson(); 
	            Client client = Client.create();
	            
	            WebResource webResource = client.resource(getServiceUrl);
	            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_GetOwnerwsdl\">"+
				            "<soapenv:Header/>"+
				            "<soapenv:Body>"+
				               "<urn:MAIN_GetOwner soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
				                  "<UserName xsi:type=\"xsd:string\">"+param.getUsername()+"</UserName>"+
				                  "<Password xsi:type=\"xsd:string\">"+param.getPassword()+"</Password>"+
				                  "<Creator xsi:type=\"xsd:string\">"+param.getSession()+"</Creator>"+
				                  "<fStream xsi:type=\"xsd:string\">{\"VESSEL_ID\":\""+vesselID+"\",\"VOYAGE_NO\":\""+voyageCode+"\"}</fStream>"+
				               "</urn:MAIN_GetOwner>"+
				            "</soapenv:Body>"+
				         "</soapenv:Envelope>";
	        
	            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
	                                                  .post(ClientResponse.class,requestBody);        
	            
	            entityResponse = response.getEntity(String.class);
	            JSONObject data = XML.toJSONObject(entityResponse);
	            modelKojaGetOwner.Envelope envelope = new modelKojaGetOwner.Envelope();
	            envelope = gson.fromJson(data.toString(), modelKojaGetOwner.Envelope.class);
	            
	            mdlOwner dataOwner = new mdlOwner();
	            dataOwner =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAINGetOwnerResponse().getReturn().getContent(), mdlOwner.class);
	            if(dataOwner.getSTATUS().equals("TRUE")){
	            	/*dataOwner.setTerminalID("KOJA");*/
	            	
	            	for(String owner : dataOwner.getOWNER()){
	            		Boolean checkExistingOwner = ShippingAdapter.checkShippingByName(owner, param.username);
	            		if(!checkExistingOwner){
	            			mdlShipping shippingData = new model.mdlShipping();
	            			shippingData.setName(owner);
			            	shippingData.setAddress(owner);
			            	ShippingAdapter.InsertShipping(shippingData, param.username);
	            		}
	            	}
	            }
	        } catch (Exception ex) {
	            LogAdapter.InsertLogExc(ex.toString(), "KojaGetOwner", getServiceUrl + " " + requestBody, param.username);
	          }
	    }
	    
        return;
     }
	
	public static String KojaGetOnDemand(mdlKojaParam param1, mdlGetOnDemandParam param2){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    String result = "Sukses";
	    String requestBody = "";
        
        try {        
        	/*param2.npwp = "";
        	param2.npwp = KojaAdapter.GetConfirmTransactionParam2(param2.orderID, param1.username).getNpwp();*/
    	    
    	    if(param2.npwp == null || param2.npwp == ""){
    	    	LogAdapter.InsertLogExc("npwp customer kosong", "KojaGetOnDemand", "KojaGetOnDemand", param1.username);
                result = "npwp eksportir kosong, mohon lengkapi data terlebih dahulu.";
    	    }
    	    else{
    	    	// Get the base naming context from web.xml
            	getServiceUrl  = param1.getServiceUrl();
        	    
            	Gson gson = new Gson(); 
                Client client = Client.create();
                
                WebResource webResource = client.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:CUSTOMS_GetDocumentTPSOnlinewsdl\">"+
    			            "<soapenv:Header/>"+
    			            "<soapenv:Body>"+
    			               "<urn:CUSTOMS_GetDocumentTPSOnline soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    			                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
    			                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
    			                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
    			                  "<fStream xsi:type=\"xsd:string\">{\"DOCUMENT_NO\":\""+param2.getPeb()+"\","
    			                  		+ "\"CUSTOMS_DOCUMENT_ID\":\"6\", \"DOCUMENT_DATE\":\""+param2.getDocumentDate()+"\","
    			                  		+ "\"NPWP\":\""+param2.npwp+"\"}</fStream>"+
    			               "</urn:CUSTOMS_GetDocumentTPSOnline>"+
    			            "</soapenv:Body>"+
    			         "</soapenv:Envelope>";
            
                ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);        
                
                entityResponse = response.getEntity(String.class);
                JSONObject data = XML.toJSONObject(entityResponse);
                modelKojaGetOnDemand.Envelope envelope = new modelKojaGetOnDemand.Envelope();
                envelope = gson.fromJson(data.toString(), modelKojaGetOnDemand.Envelope.class);
                
                mdlGetOnDemandParam mdlGetOnDemand = new mdlGetOnDemandParam();
                mdlGetOnDemand =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1CUSTOMSGetDocumentTPSOnlineResponse().getReturn().getContent(), mdlGetOnDemandParam.class);
                if(mdlGetOnDemand.getMESSAGE().contains("Session expired")){
                	KojaGetLogin(param1);
                	KojaGetOnDemand(param1, param2);
                }
                else if(mdlGetOnDemand.getMESSAGE().contains("Error silakan hubungi administrator") || mdlGetOnDemand.getSTATUS().equals("FALSE")){
                	result = APIAdapter.KojaTpsManual(param1, param2);
                	LogAdapter.InsertLogExc(requestBody, "KojaGetOnDemand - proceed to tpsManual", getServiceUrl, param1.username);
                }
                else if(mdlGetOnDemand.getSTATUS().equals("TRUE")){
                	/*result = "Request success";*/
                	//update status tps to 'on demand' function here
                	OrderManagementAdapter.UpdateOrderTpsStatus(param2.orderID, 2, param1.username);
                }
    	    }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetOnDemand", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static String KojaTpsManual(mdlKojaParam param1, mdlGetOnDemandParam param2){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    String result = "Sukses";
	    String documentDate = ConvertDateTimeHelper.formatDate(param2.getDocumentDate(), "ddMMyyyy", "yyyyMMdd");
        
	    List<String> containerNo = ContainerAdapter.LoadContainerByOrder(param2.orderID, param1.username);
        Gson gsonContainerNo = new Gson();
        String newContainerNo = gsonContainerNo.toJson(containerNo);
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:CUSTOMS_SaveDocumentTPSOnlinewsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:CUSTOMS_SaveDocumentTPSOnline soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
			                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"DOCUMENT_NO\":\""+param2.getDocumentNo()+"\","
			                  		+ "\"CUSTOMS_DOCUMENT_ID\":\"6\", \"DOCUMENT_DATE\":\""+documentDate+"\","
			                  		+ "\"TERMINAL_ID\":\"KOJA\",\"NPWP\":\""+param2.getNpwp()+"\","
			                  				+ "\"NO_CONT\":"+newContainerNo+"}</fStream>"+
			               "</urn:CUSTOMS_SaveDocumentTPSOnline>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaTpsManual.Envelope envelope = new modelKojaTpsManual.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaTpsManual.Envelope.class);
            
            mdlKojaTpsManual mdlTpsManual = new mdlKojaTpsManual();
            mdlTpsManual =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1CUSTOMSSaveDocumentTPSOnlineResponse().getReturn().getContent(), mdlKojaTpsManual.class);
            if(mdlTpsManual.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param1);
            	KojaTpsManual(param1, param2);
            }
            else if(mdlTpsManual.getMESSAGE().contains("Error silakan hubungi administrator")){
            	result = "Upload NPE gagal. " + mdlTpsManual.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaTpsManual", getServiceUrl, param1.username);
            }
            else if(mdlTpsManual.getMESSAGE().contains("Data sudah ada")){
            	result = "Upload NPE gagal. " + mdlTpsManual.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaTpsManual", getServiceUrl, param1.username);
            }
            else if(mdlTpsManual.getMESSAGE().contains("Customer belum terdaftar")){
            	result = "Upload NPE gagal. " + mdlTpsManual.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaTpsManual", getServiceUrl, param1.username);
            }
            else if(mdlTpsManual.getSTATUS().equals("TRUE")){
            	/*result = "Request success";*/
            	//update status tps to 'on demand' function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(param2.orderID, 2, param1.username);
            }
            else{
            	result = "Upload NPE gagal. " + mdlTpsManual.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaTpsManual", getServiceUrl, param1.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaTpsManual", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	//SHIPPER
	public static String KojaMainProcessShipper(mdlKojaParam param1, mdlGetOnDemandParam param2){
		String result = "";
		Integer tpsStatus = OrderManagementAdapter.GetTpsStatus(param2.orderID, param1.username);
		
		if(tpsStatus == 0){
			//get on demand
			result = KojaGetOnDemand(param1, param2);
		}
		else if(tpsStatus == 2){
			//validate tid after on demand(2)
			result = KojaTIDValidation(param1, param2.orderID);
		}
		else if(tpsStatus == 13){
			//get doc npe after validate tid(13)
			result = KojaGetDocNPE(param1, param2.orderID, param2.documentNo);
			if(result.equals("Sukses"))
				result = "Request success. Please wait for confirmation.";
		}
		
		return result;
     }
	
	//ADMIN
	public static String KojaMainProcess(mdlKojaParam param1, String orderID, String npe){
		String result = "";
		Integer tpsStatus = OrderManagementAdapter.GetTpsStatus(orderID, param1.username);
		
		if(tpsStatus == 2){
			//validate tid after on demand(2)
			result = KojaTIDValidation(param1, orderID);
			/*if(result.equals("Sukses"))
				KojaMainProcess(param1, orderID, npe);*/
		}
		else if(tpsStatus == 13){
			//get doc npe after validate tid(13)
			result = KojaGetDocNPE(param1, orderID, npe);
			/*if(result.equals("Sukses"))
				KojaMainProcess(param1, orderID, npe);*/
		}
		else if(tpsStatus == 3){
			//confirm transaction after get doc npe(3)
			result = KojaConfirmTransaction(param1, orderID, npe);
			/*if(result.equals("Sukses"))
				KojaMainProcess(param1, orderID, npe);*/
		}
		else if(tpsStatus == 4){
			//get billing after confirm transaction(4)
			result = KojaGetBilling(param1, orderID);
		}
		else if(tpsStatus == 5){
			//get proforma after get billing(5)
			result = KojaGetProforma(param1, orderID);
		}
		
//		masukan proforma
		
		
		else if(tpsStatus == 6){
			//request inquiry after get proforma(6)
			result = RequestInquiry2(param1, orderID);
		}
		else if(tpsStatus == 7){
			//send billing edii after req inquiry(7)
			result = SendBilling2(param1, orderID);
		}
		else if(tpsStatus == 8){
			//flagging payment after send billing(8)
			result = PaymentFlagging2(param1, orderID);
		}
		else if(tpsStatus == 9){
			//get invoice after payment flagging(9)
			result = KojaGetInvoice(param1, orderID);
			if(result.equals("Sukses"))
				result = "Proses integrasi berhasil. Silahkan melakukan autentikasi pin dan otp.";
		}
//		else if(tpsStatus == 14){
//			//get invoice after payment flagging(9)
//			result = KojaGetBilling(param1, orderID);
//		}
		
		
		
		return result;
     }
	
	public static String KojaTIDValidation(mdlKojaParam param1, String orderID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    String result = "Sukses";
        
	    List<String> tidNumber = ContainerAdapter.LoadTIDByOrder(orderID, param1.username2);
        Gson gson = new Gson();
        String newTIDNumber = gson.toJson(tidNumber);
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl2();
    	    
        	gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:truck_id_validation>"+
			                  "<username>"+param1.getUsername2()+"</username>"+
			                  "<password>"+param1.getPassword2()+"</password>"+
			                  "<fstream>{\"TID\":"+newTIDNumber+"}</fstream>"+
			               "</tpk:truck_id_validation>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaTIDValidation.Envelope envelope = new modelKojaTIDValidation.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaTIDValidation.Envelope.class);
            
            mdlTIDValidation tidValidation = new mdlTIDValidation();
            tidValidation =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2TruckIdValidationResponse().getReturn(), mdlTIDValidation.class);
            Boolean checkValidate = true;
            String resultCheckValidate = "";
            //check tid validation
            for(mdlTruckData td : tidValidation.getDATA().getTRUCK_ID_DATA()){
            	if(!td.getSTATUS()){
            		resultCheckValidate += td.getTID() + " ";
            		checkValidate = false;
            	}
            }
            if(!checkValidate){ //if invalid do this
            	result = "Truck id : "+ resultCheckValidate +"invalid";
            	LogAdapter.InsertLogExc(requestBody, "KojaTIDValidation", getServiceUrl , param1.username2);
            }
            else{ //if valid do this
            	//update status tps to 'validate_tid' function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 13, param1.username2);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaTIDValidation", getServiceUrl , param1.username2);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static Boolean KojaTIDValidationForDriverAssignment(mdlKojaParam param1, String tid){
		String entityResponse = "";
	    String getServiceUrl = "";
	    Boolean checkValidate = true;
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl2();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:truck_id_validation>"+
			                  "<username>"+param1.getUsername2()+"</username>"+
			                  "<password>"+param1.getPassword2()+"</password>"+
			                  "<fstream>{\"TID\":[\""+tid+"\"]}</fstream>"+
			               "</tpk:truck_id_validation>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaTIDValidation.Envelope envelope = new modelKojaTIDValidation.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaTIDValidation.Envelope.class);
            
            mdlTIDValidation tidValidation = new mdlTIDValidation();
            tidValidation =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2TruckIdValidationResponse().getReturn(), mdlTIDValidation.class);
            
            //check tid validation
            for(mdlTruckData td : tidValidation.getDATA().getTRUCK_ID_DATA()){
            	if(!td.getSTATUS()){
            		checkValidate = false;
            	}
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaTIDValidationForDriverAssignment", getServiceUrl , param1.username2);
            checkValidate = false;
          }
        
        return checkValidate;
     }
	
	public static String KojaGetDocNPE(mdlKojaParam param1, String orderID, String npe){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_GetDocumentCustomsNGenwsdl\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<urn:MAIN_GetDocumentCustomsNGen soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
		                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
		                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
		                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
		                  "<fStream xsi:type=\"xsd:string\">{\"DOCUMENT_NO\":\""+npe+"\",\"CUSTOMS_DOCUMENT_ID\":\"6\",\"TRANSACTIONS_TYPE_ID\":\"5\",\"TERMINAL_ID\":\"KOJA\"}</fStream>"+
		               "</urn:MAIN_GetDocumentCustomsNGen>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetDocNpe.Envelope envelope = new modelKojaGetDocNpe.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetDocNpe.Envelope.class);
            
            mdlGetDocNpe docNpe = new mdlGetDocNpe();
            docNpe =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAINGetDocumentCustomsNGenResponse().getReturn().getContent(), mdlGetDocNpe.class);
            if(docNpe.getSTATUS().equals("FALSE") && docNpe.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param1);
            	KojaGetDocNPE(param1, orderID, npe);
            }
            else if(docNpe.getSTATUS().equals("FALSE") && docNpe.getMESSAGE().contains("Data tidak ditemukan")){
            	result = docNpe.getMESSAGE();
            }
            else if(docNpe.getSTATUS().equals("TRUE")){
            	//distinct the array string container from koja
                Set<String> temp = new LinkedHashSet<String>( Arrays.asList( docNpe.getNO_CONT() ) );
                String[] listContainerFromKoja = temp.toArray( new String[temp.size()] );
                
                //get logol server container
                List<String> listContainerFromLogol = ContainerAdapter.LoadContainerByOrder(orderID, param1.username);
                
                //check container is match or not
            	if(listContainerFromKoja.length == listContainerFromLogol.size()){
            		//if valid
            		//update status tps to 'get doc npe' function here
                	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 3, param1.username);
            	}
                else{ //if not valid do this
                	result = "Container's data from TPS and LOGOL do not match. Please contact administrator.";
                	LogAdapter.InsertLogExc(requestBody, "KojaGetDocNPE", getServiceUrl, param1.username);
                }
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetDocNPE", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }

	public static String KojaConfirmTransaction(mdlKojaParam param1, String orderID, String npe){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    
	    mdlKojaTransaction mdlKojaTransaction = new model.mdlKojaTransaction();
	    mdlKojaTransaction = KojaAdapter.GetConfirmTransactionParam2(orderID, param1.username);
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try { 
        	String kojaCustomerID = KojaGetCustomer(mdlKojaTransaction, param1);
        	if(kojaCustomerID.equals("")){
        		result = "Autentikasi data customer gagal. Silahkan hubungi administrator";
        	}
        	else{
        		// Get the base naming context from web.xml
            	getServiceUrl  = param1.getServiceUrl();
        	    
            	Gson gson = new Gson(); 
                Client client = Client.create();
                
                WebResource webResource = client.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_ConfirmTransactionwsdl\">"+
    		            "<soapenv:Header/>"+
    		            "<soapenv:Body>"+
    		               "<urn:BILLING_ConfirmTransaction soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    		                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
    		                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
    		                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
    		                  "<fStream xsi:type=\"xsd:string\">{\"CERTIFICATED_ID\":[\"\"],\"OLD_COMPANY_CODE\":\"\",\"CUST_ID\":\""+kojaCustomerID+"\","
    		                  									+ "\"REFEER_TEMPERATURE\":[\"\"],\"ISO_CODE\":"+mdlKojaTransaction.getSisoCode()+",\"TRANSACTIONS_TYPE_ID\":\""+mdlKojaTransaction.getTransactionTypeID()+"\","
    		                  									+ "\"VOLTAGE_PLUG\":[\"\"],\"TGL_NHI\":\"\",\"OVER_RIGHT\":[\"\"],\"DOCUMENT_SHIPPING_NO\":\""+mdlKojaTransaction.getNoBlAwb()+"\","
    		                  									+ "\"OLD_POD\":[\"\"],\"OLD_VOYAGE_NO\":\"\",\"START_PLUG\":[],\"OVER_LEFT\":[\"\"],\"OWNER\":"+mdlKojaTransaction.getShippingLine()+","
    		                  									+ "\"NO_BL_AWB\":\""+mdlKojaTransaction.noBlAwb+"\",\"WEIGHT_VGM\":[\"\"],\"OLD_NO_CONT\":[\"\"],\"DOCUMENT_SHIPPING_DATE\":\""+mdlKojaTransaction.getDocumentShippingDate()+"\","
    		                  									+ "\"VOYAGE_NO\":\""+mdlKojaTransaction.getVoyageNo()+"\",\"COMPANY_CODE\":\""+mdlKojaTransaction.getVoyageCompanyCode()+"\",\"NPWP_SERTIFICATED\":[\"\"],\"WEIGHT\":"+mdlKojaTransaction.getsWeight()+","
    		                  									+ "\"CERTIFICATED_PIC\":[\"\"],\"OLD_VESSEL_ID\":\"\",\"IMO_CODE\":[\"\"],\"UN_NUMBER\":[\"\"],"
    		                  									+ "\"DOCUMENT_NO\":\""+npe+"\",\"CUSTOMS_DOCUMENT_ID\":\""+mdlKojaTransaction.getCustomsDocumentID()+"\",\"VESSEL_ID\":\""+mdlKojaTransaction.getVesselID()+"\",\"POD\":"+mdlKojaTransaction.getPod()+","
    		                  									+ "\"CUST_SERTIFICATED\":[\"\"],\"OLD_FD\":[\"\"],\"DOCUMENT_DATE\":\""+mdlKojaTransaction.getDocumentDate()+"\",\"STOP_PLUG\":[\"\"],"
    		                  									+ "\"OVER_FRONT\":[\"\"],\"OVER_BACK\":[\"\"],\"CUST_ID_PPJK\":\"\",\"PAID_THRU\":\"\",\"POL\":"+mdlKojaTransaction.getPol()+","
    		                  									+ "\"TGL_BK_SEGEL_NHI\":\"\",\"OLD_INVOICE_NO\":\"\",\"OVER_HEIGHT\":[\"\"],\"FD\":"+mdlKojaTransaction.getFd()+",\"NO_CONT\":"+mdlKojaTransaction.getsContainerNumber()+","
    		                  									+ "\"PHONE_REQ\":\""+param1.getPhone()+"\",\"EMAIL_REQ\":\""+param1.getEmail()+"\",\"ATTCH_DOC\":\""+mdlKojaTransaction.getNpeDoc()+"\"}</fStream>"+
    		               "</urn:BILLING_ConfirmTransaction>"+
    		            "</soapenv:Body>"+
    		         "</soapenv:Envelope>";
            
                ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);        
                
                entityResponse = response.getEntity(String.class);
                JSONObject data = XML.toJSONObject(entityResponse);
                modelKojaConfirmTransaction.Envelope envelope = new modelKojaConfirmTransaction.Envelope();
                envelope = gson.fromJson(data.toString(), modelKojaConfirmTransaction.Envelope.class);
                
                mdlConfirmTransaction resultConfirmTransaction = new mdlConfirmTransaction();
                resultConfirmTransaction =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGConfirmTransactionResponse().getReturn().getContent(), mdlConfirmTransaction.class);
                if(resultConfirmTransaction.getSTATUS().equals("FALSE") && resultConfirmTransaction.getMESSAGE().contains("Session expired")){
                	KojaGetLogin(param1);
                	KojaConfirmTransaction(param1, orderID, npe);
                }
                else if(resultConfirmTransaction.getSTATUS().equals("FALSE") && !resultConfirmTransaction.getMESSAGE().contains("Session expired")){
                	result = resultConfirmTransaction.getMESSAGE();
                	LogAdapter.InsertLogExc(requestBody, "KojaConfirmTransaction", getServiceUrl, param1.username);
                }
                else if(resultConfirmTransaction.getSTATUS().equals("TRUE")){
            		//if valid
            		//update status tps to 'confirm'/4 function here
                	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 4, param1.username);
                	
                	//update tps_transactionid and tps_transactiondate
                	LocalDateTime dateTime = LocalDateTime.now();
        			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddHHmmss");  
        			String txDate = dateTime.format(formatter);  
        			
                	mdlOrderManagement tpsData = new model.mdlOrderManagement();
                	tpsData.setOrderManagementID(orderID);
                	tpsData.setTpsTransactionID(resultConfirmTransaction.getTRANSACTION_ID());
                	tpsData.setTpsTransactionDate(txDate);
                	tpsData.setTpsProforma("");
                	tpsData.setTpsPaymentID("");
                	tpsData.setTpsApprovalCode("");
                	tpsData.setTpsApprovalDate(LocalDateTime.now().toString());
                	tpsData.setTpsBillingAmount(0);
                	OrderManagementAdapter.UpdateTpsData(tpsData, param1.username);
                }
        	}
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaConfirmTransaction", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static String KojaGetCustomer(mdlKojaTransaction mdlKojaTransaction, mdlKojaParam param1){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    String kojaCustomerID = "";
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_GetCustomersNewwsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:MAIN_GetCustomersNew soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
			                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"CUST_TYPE_ID\":\"PPJK\","
			                  		+ "\"DEPO_OBX\":\"\", \"PARAMETER_STATUS\":\"\","
			                  		+ "\"PARAMETER_EQUAL\":\"=\",\"CUST_ID_PPJK\":\"\",\"TERMINAL_ID\":\"KOJA\","
			                  		+ "\"PARAMETER_EQUAL_CUST_TYPE\":\"!=\",\"PARAMETER_VALUE\":\""+mdlKojaTransaction.getNpwp()+"\","
			                  		+ "\"PARAMETER_ID\":\"NPWP\"}</fStream>"+
			               "</urn:MAIN_GetCustomersNew>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetCustomer.Envelope envelope = new modelKojaGetCustomer.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetCustomer.Envelope.class);
            
            mdlGetCustomer mdlGetCustomer = new mdlGetCustomer();
            mdlGetCustomer =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAIN_GetCustomersNewResponse().getReturn().getContent(), mdlGetCustomer.class);
            
            if(mdlGetCustomer.getSTATUS().equals("FALSE")){
            	if(mdlGetCustomer.getMESSAGE() != null){
                	if(mdlGetCustomer.getMESSAGE().contains("Session expired")){
                		KojaGetLogin(param1);
                    	KojaGetCustomer(mdlKojaTransaction, param1);
                	}
                	else{
                		LogAdapter.InsertLogExc(mdlKojaTransaction.logolCustomerName + " " + mdlGetCustomer.getMESSAGE() +", user not found, proceed to save organization", "KojaGetCustomer", getServiceUrl, param1.username);
                    	kojaCustomerID = APIAdapter.KojaSaveCustomer(mdlKojaTransaction, param1);
                	}
                }
            	else{
            		LogAdapter.InsertLogExc(mdlKojaTransaction.logolCustomerName + "get customer error", "KojaGetCustomer", getServiceUrl, param1.username);
                	/*kojaCustomerID = APIAdapter.KojaSaveCustomer(mdlKojaTransaction, param1);*/
            	}
            }
            else if(mdlGetCustomer.getSTATUS().equals("TRUE")){
            	kojaCustomerID = mdlGetCustomer.getID().get(0);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetCustomer", getServiceUrl + " " + requestBody, param1.username);
            kojaCustomerID = "";
          }
        
        return kojaCustomerID;
    }
	
	public static String KojaSaveCustomer(mdlKojaTransaction mdlKojaTransaction, mdlKojaParam param1){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    String kojaCustomerID = "";
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:MAIN_SaveOrganizationwsdl\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<urn:MAIN_SaveOrganization soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
			                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
			                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
			                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
			                  "<fStream xsi:type=\"xsd:string\">{\"EDI_ACCOUNT\":\"\",\"DEPO_OBX\":\"\",\"CHANGED\":\"\",\"END_CONTRACT\":\"\","
			                  		+ "\"PHONE\":\""+mdlKojaTransaction.getLogolCustomerPhone()+"\",\"CERTIFICATOR_SIGNATURE\":\"\",\"CERTIFICATOR_EMAIL\":\"\","
			                  		+ "\"CONTACT_PERSON\":\"\",\"ALTERNATE_NAME\":\"\",\"CUST_TAX_CODE\":\"CTAX10\",\"EMAIL\":\"\",\"CERTIFICATORR_PIC\":\"\","
			                  		+ "\"CLASS_ID\":\"CRP\",\"CREATED\":\"\",\"BEGIN_CONTRACT\":\"\",\"POSTAL_CODE\":\"\",\"REGISTRASI_VGM_DATETIME\":\"\","
			                  		+ "\"CREATOR\":\"\",\"GL_MAP_CODE_2\":\"\",\"GL_MAP_CODE_1\":\"\",\"DEPO_TYPE\":\"\",\"ADDRESS1\":\""+mdlKojaTransaction.getLogolCustomerAddress().substring(0, 70)+"\","
			                  		+ "\"ASO\":\"\",\"REMARK\":\"\",\"PM_CODE\":\"C\",\"ADDRESS3\":\"\",\"AR_CUST_ID\":\"\",\"ADDRESS2\":\"\",\"HICO_FLAG\":\"\",\"CUST_TYPE_ID\":\"CUST\","
			                  		+ "\"WAPU\":\"\",\"VGM_STATUS\":\"\",\"SHIP_LINE_ROUTE_ID\":\"\",\"EMAIL2\":\"\",\"ALTERNATE_ADDRESS\":\"\",\"CITY_ID\":\"NAT\","
			                  		+ "\"CONTRACT_NUMBER\":\"\",\"CHANGER\":\"\",\"ACTIVE_STATUS\":\"\",\"CERTIFICATE_ID\":\"\",\"FAX\":\"\",\"NO_IZIN_VGM\":\"\",\"NPWP\":\""+mdlKojaTransaction.getNpwp()+"\","
			                  		+ "\"CUST_NAME\":\""+mdlKojaTransaction.getShipperName()+"\"}</fStream>"+
			               "</urn:MAIN_SaveOrganization>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaSaveCustomer.Envelope envelope = new modelKojaSaveCustomer.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaSaveCustomer.Envelope.class);
            
            mdlSaveCustomer mdlSaveCustomer = new mdlSaveCustomer();
            mdlSaveCustomer =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1MAIN_SaveOrganizationResponse().getReturn().getContent(), mdlSaveCustomer.class);
            
            if(mdlSaveCustomer.getSTATUS().equals("FALSE")){
            	if(mdlSaveCustomer.getMESSAGE() != null){
                	if(mdlSaveCustomer.getMESSAGE().contains("Session expired")){
                		KojaGetLogin(param1);
                    	KojaSaveCustomer(mdlKojaTransaction, param1);
                	}
                }
            	LogAdapter.InsertLogExc("Save Customer Error", "KojaSaveCustomer", getServiceUrl + requestBody, param1.username);
            }
            else if(mdlSaveCustomer.getSTATUS().equals("TRUE")){
            	kojaCustomerID = mdlSaveCustomer.getCUST_ID();
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaSaveCustomer", getServiceUrl + " " + requestBody, param1.username);
            kojaCustomerID = "";
          }
        
        return kojaCustomerID;
     }
	
	public static String KojaGetBilling(mdlKojaParam param1, String orderID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.getUsername());
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_GetBillingwsdl\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<urn:BILLING_GetBilling soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
		                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
		                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
		                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
		                  "<fStream xsi:type=\"xsd:string\">{\"TRANSACTION_ID\":\""+orderManagement.getTpsTransactionID()+"\"}</fStream>"+
		               "</urn:BILLING_GetBilling>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetBilling.Envelope envelope = new modelKojaGetBilling.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetBilling.Envelope.class);
            
            mdlGetBilling resultGetBilling = new mdlGetBilling();
            resultGetBilling =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetBillingResponse().getReturn().getContent(), mdlGetBilling.class);
            if(resultGetBilling.getSTATUS().equals("FALSE") && resultGetBilling.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param1);
            	KojaGetBilling(param1, orderID);
            }
            else if(resultGetBilling.getSTATUS().equals("FALSE") && !resultGetBilling.getMESSAGE().contains("Session expired")){
            	result = "Gagal. " + resultGetBilling.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaGetBilling", getServiceUrl, param1.username);
            }
            else if(resultGetBilling.getSTATUS().equals("TRUE")){
        		//if valid
        		//update status tps to 'billing'/5 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 5, param1.username);
            	
            	//update tps_proformano
            	mdlOrderManagement tpsData = new model.mdlOrderManagement();
            	tpsData.setOrderManagementID(orderID);
            	tpsData.setTpsTransactionID(orderManagement.getTpsTransactionID());
            	tpsData.setTpsTransactionDate(orderManagement.getTpsTransactionDate());
            	tpsData.setTpsProforma(resultGetBilling.getPROFORMA_INVOICE_NO());
            	tpsData.setTpsPaymentID("");
            	tpsData.setTpsApprovalCode("");
            	tpsData.setTpsApprovalDate(LocalDateTime.now().toString());
            	tpsData.setTpsBillingAmount(0);
            	OrderManagementAdapter.UpdateTpsData(tpsData, param1.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetBilling", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static String KojaGetProforma(mdlKojaParam param1, String orderID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.getUsername());
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_GetProformawsdl\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<urn:BILLING_GetProforma soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
		                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
		                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
		                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
		                  "<fStream xsi:type=\"xsd:string\">{\"TRANSACTION_ID\":\""+orderManagement.getTpsTransactionID()+"\"}</fStream>"+
		               "</urn:BILLING_GetProforma>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetProforma.Envelope envelope = new modelKojaGetProforma.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetProforma.Envelope.class);
            
            mdlGetProforma resultGetProforma = new mdlGetProforma();
            resultGetProforma =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetProformaResponse().getReturn().getContent(), mdlGetProforma.class);
            if(resultGetProforma.getSTATUS().equals("FALSE") && resultGetProforma.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param1);
            	KojaGetProforma(param1, orderID);
            }
            else if(resultGetProforma.getSTATUS().equals("FALSE") && !resultGetProforma.getMESSAGE().contains("Session expired")){
            	result = "Gagal. " + resultGetProforma.getMESSAGE();
            	LogAdapter.InsertLogExc(requestBody, "KojaGetProforma", getServiceUrl, param1.username);
            }
            else if(resultGetProforma.getSTATUS().equals("TRUE")){
        		//if valid
        		//update status tps to 'proforma'/6 function here
            	
            	result = "Sukses, silahkan lanjutkan ke pembayaran dengan menekan tombol inquiry";
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 6, param1.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetProforma", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static model.mdlSendBilling SendBilling(mdlSendBilling param, String user){
	    String entityResponse = "";
	    String getServiceUrl = "";
	    model.mdlSendBilling mdlSendBilling = new model.mdlSendBilling();
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.serviceUrl;
    	    
            Gson gson = new Gson();    
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String json = gson.toJson(param);
        
            ClientResponse response  = webResource.type("application/json").accept("application/json")
                                                  .post(ClientResponse.class,json);        
            
            entityResponse = response.getEntity(String.class);
            
            mdlSendBilling = gson.fromJson(entityResponse, model.mdlSendBilling.class);
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "SendBilling", getServiceUrl , user);
            mdlSendBilling = new model.mdlSendBilling();
          }
        
        return mdlSendBilling;
     }
	
	public static String SendBilling2(mdlKojaParam param, String orderID){
	    String entityResponse = "";
	    String getServiceUrl = "";
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param.getUsername());
	    
	    mdlKojaTransaction shipperData = KojaAdapter.GetConfirmTransactionParam2(orderID, param.getUsername());
	    
	    String result = "Sukses";
	    
	     model.mdlSendBilling mdlSendBilling = new model.mdlSendBilling();
		 mdlSendBilling.serviceUrl = param.getServiceUrl4();
		 mdlSendBilling.user = param.getBillingUser();
		 mdlSendBilling.password = param.getBillingPassword();
		 mdlSendBilling.billerId = Integer.valueOf(param.getBillingBillerID());
		 mdlSendBilling.productId = param.getBillingProduct();
		 mdlSendBilling.channelId = param.getBillingChannel();
		 mdlSendBilling.data = new model.mdlSendBillingData();
		 
		 mdlSendBilling.data.issuerId = 5;
		 mdlSendBilling.data.billingId = orderManagement.getTpsProforma();
		 mdlSendBilling.data.billingDate = LocalDateTime.now().toString().replace("T", " ").substring(0, 19);
		 mdlSendBilling.data.expiredDate = LocalDateTime.now().plusHours(1).toString().replace("T", " ").substring(0, 19);
		 mdlSendBilling.data.billingType = 1;
		 mdlSendBilling.data.customerId = shipperData.logolCustomerID;
		 mdlSendBilling.data.customerName = shipperData.logolCustomerName;
		 mdlSendBilling.data.customerAddress = shipperData.logolCustomerAddress;
		 mdlSendBilling.data.customerMail = shipperData.logolCustomerMail;
		 mdlSendBilling.data.customerPhone = shipperData.logolCustomerPhone;
		 mdlSendBilling.data.billerAccountNo = param.getBillingBillerAccount();
		 mdlSendBilling.data.billerName = param.getBillingBillerName();
		 mdlSendBilling.data.currency = "IDR";
		 mdlSendBilling.data.amount = orderManagement.getTpsBillingAmount();
		 mdlSendBilling.data.tax = 0;
		 mdlSendBilling.data.totalAmount = orderManagement.getTpsBillingAmount();
		 mdlSendBilling.data.interval = 60;
		 /*mdlSendBilling.data.remark = param.getBillingRemark();*/
		 mdlSendBilling.data.remark = orderManagement.getTpsProforma();
		 mdlSendBilling.data.signature = ShaAdapter.getSHA(mdlSendBilling.user + mdlSendBilling.billerId + mdlSendBilling.data.billingId);
		 mdlSendBilling.data.documentNo = "-";
		 mdlSendBilling.data.bankClientId = param.getBillingClientID();
		 mdlSendBilling.data.accountNo = param.getBillingClientAccount();
		 mdlSendBilling.data.accountName = "PT. LOGOL";
		 mdlSendBilling.data.detail = new ArrayList<model.mdlSendBillingDataDetail>();
		 
		 model.mdlSendBillingDataDetail billingDataDetail = new model.mdlSendBillingDataDetail();
		 billingDataDetail.code = "01";
		 billingDataDetail.description = "EXPORT STANDARD";
		 billingDataDetail.nominal = orderManagement.getTpsBillingAmount();
		 mdlSendBilling.data.detail.add(billingDataDetail);
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = mdlSendBilling.serviceUrl;
    	    
            Gson gson = new Gson();    
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String json = gson.toJson(mdlSendBilling);
        
            ClientResponse response  = webResource.type("application/json").accept("application/json")
                                                  .post(ClientResponse.class,json);        
            
            entityResponse = response.getEntity(String.class);
            
            mdlSendBilling responseSendBilling = new model.mdlSendBilling();
            responseSendBilling = gson.fromJson(entityResponse, model.mdlSendBilling.class);
            if(!responseSendBilling.getStatus()){
            	result = responseSendBilling.getErrDesc();
            }
            else{
            	//if valid
        		//update status tps to 'payment'/8 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 8, param.username);
            	
            	//update tps_paymentid, tps_approvalcode, tps_approvaldate
            	mdlOrderManagement tpsData = new model.mdlOrderManagement();
            	tpsData.setOrderManagementID(orderID);
            	tpsData.setTpsTransactionID(orderManagement.getTpsTransactionID());
            	tpsData.setTpsTransactionDate(orderManagement.getTpsTransactionDate());
            	tpsData.setTpsProforma(orderManagement.getTpsProforma());
            	tpsData.setTpsPaymentID(responseSendBilling.getData().getPaymentId());
            	tpsData.setTpsApprovalCode(responseSendBilling.getData().getJournalNo());
            	tpsData.setTpsApprovalDate(responseSendBilling.getData().getJournalDate());
            	tpsData.setTpsBillingAmount(orderManagement.getTpsBillingAmount());
            	OrderManagementAdapter.UpdateTpsData(tpsData, param.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "SendBilling2", getServiceUrl , param.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static mdlRequestInquiry RequestInquiry(mdlRequestInquiry param, String user){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    mdlRequestInquiry requestInquiryResponse = new mdlRequestInquiry();
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ban=\"logol.h2h.billpayment.ws\">"+
       	 		 "<soapenv:Header/>"+
    	 		 "<soapenv:Body>"+
    	 		 	"<ban:inquiry>"+
    	 		 	"<ban:request>"+
    	 		 	"<ban:language>02</ban:language>"+
    	 		 	"<ban:trxDateTime>"+param.getTransactionDateTime()+"</ban:trxDateTime>"+
    	 		 	"<ban:transmissionDateTime>"+param.getRequestDateTime()+"</ban:transmissionDateTime>"+
    	 		 	"<ban:companyCode>"+param.getCompanyCode()+"</ban:companyCode>"+
    	 		 	"<ban:channelID>"+param.getChannelID()+"</ban:channelID>"+
    	 		 	"<ban:billKey1>"+param.getProformaNo()+"</ban:billKey1>"+
    	 		 	"<ban:billKey2></ban:billKey2>"+
    	 		 	"<ban:billKey3></ban:billKey3>"+
    	 		 	"<ban:reference1>"+param.getBankName()+"</ban:reference1>"+
    	 		 	"<ban:reference2></ban:reference2>"+
    	 		 	"<ban:reference3></ban:reference3>"+
    	 		 	"</ban:request>"+
    	 		 	"</ban:inquiry>"+
    	 		 "</soapenv:Body>"+
    	 		 "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            Envelope envelope = new Envelope();
            envelope = gson.fromJson(data.toString(), Envelope.class);
            
			requestInquiryResponse.billingCode = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getBillDetails().getBillDetail().getBillCode();
			requestInquiryResponse.billingDescription = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getBillDetails().getBillDetail().getBillName();
			requestInquiryResponse.billingAmount = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getBillDetails().getBillDetail().getBillAmount();
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "RequestInquiry", getServiceUrl , user);
          }
        
        return requestInquiryResponse;
     }
	
	public static String RequestInquiry2(mdlKojaParam param, String orderID){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param.getUsername());
	    LocalDateTime dateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddHHmmss");  
		String transmissionDate = dateTime.format(formatter);  
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl3();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ban=\"logol.h2h.billpayment.ws\">"+
          	 		 "<soapenv:Header/>"+
       	 		 "<soapenv:Body>"+
       	 		 	"<ban:inquiry>"+
       	 		 	"<ban:request>"+
       	 		 	"<ban:language>02</ban:language>"+
       	 		 	"<ban:trxDateTime>"+orderManagement.getTpsTransactionDate()+"</ban:trxDateTime>"+
       	 		 	"<ban:transmissionDateTime>"+transmissionDate+"</ban:transmissionDateTime>"+
       	 		 	"<ban:companyCode>"+param.getBankCompanyCode()+"</ban:companyCode>"+
       	 		 	"<ban:channelID>"+param.getBankChannelID()+"</ban:channelID>"+
       	 		 	"<ban:billKey1>"+orderManagement.getTpsProforma()+"</ban:billKey1>"+
       	 		 	"<ban:billKey2></ban:billKey2>"+
       	 		 	"<ban:billKey3></ban:billKey3>"+
       	 		 	"<ban:reference1>"+param.getBankName()+"</ban:reference1>"+
       	 		 	"<ban:reference2></ban:reference2>"+
       	 		 	"<ban:reference3></ban:reference3>"+
       	 		 	"</ban:request>"+
       	 		 	"</ban:inquiry>"+
       	 		 "</soapenv:Body>"+
       	 		 "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaRequestInquiry.Envelope envelope = new modelKojaRequestInquiry.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaRequestInquiry.Envelope.class);
            
            mdlRequestInquiry requestInquiryResponse = new mdlRequestInquiry();
            requestInquiryResponse.status = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getStatus().getIsError();
            requestInquiryResponse.message = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getStatus().getStatusDescription();
            
            //untuk case api ini beda, false adalah iserrornya, sehingga jika false maka lanjut proses
            if(requestInquiryResponse.getStatus()){
            	result = requestInquiryResponse.getMessage();
            	LogAdapter.InsertLogExc(requestBody, "RequestInquiry2", getServiceUrl, param.username);
            }
            else if(!requestInquiryResponse.getStatus()){
        		//if valid
        		//update status tps to 'inquiry'/7 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 7, param.username);
            	
            	//update tps_billing amount
            	requestInquiryResponse.billingAmount = envelope.getSEnvelope().getSBody().getInquiryResponse().getInquiryResult().getBillDetails().getBillDetail().getBillAmount();
            	mdlOrderManagement tpsData = new model.mdlOrderManagement();
            	tpsData.setOrderManagementID(orderID);
            	tpsData.setTpsTransactionID(orderManagement.getTpsTransactionID());
            	tpsData.setTpsTransactionDate(orderManagement.getTpsTransactionDate());
            	tpsData.setTpsProforma(orderManagement.getTpsProforma());
            	tpsData.setTpsPaymentID("");
            	tpsData.setTpsApprovalCode("");
            	tpsData.setTpsApprovalDate(LocalDateTime.now().toString());
            	tpsData.setTpsBillingAmount(requestInquiryResponse.getBillingAmount());
            	OrderManagementAdapter.UpdateTpsData(tpsData, param.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "RequestInquiry2", getServiceUrl + " " + requestBody, param.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static void PaymentFlagging(mdlPaymentFlagging param, String user){   
		String entityResponse = "";
	    String getServiceUrl = "";
	   /* mdlPaymentFlagging requestPaymentFlagging = new mdlPaymentFlagging();*/
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ban=\"logol.h2h.billpayment.ws\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		            "<ban:payment>"+
		            "<ban:request>"+
		            "<ban:language>02</ban:language>"+
		            "<ban:trxDateTime>"+param.getTransactionDate()+"</ban:trxDateTime>"+
		            "<ban:transmissionDateTime>"+param.getRequestDate()+"</ban:transmissionDateTime>"+
		            "<ban:companyCode>"+param.getCompanyCode()+"</ban:companyCode>"+
		            "<ban:channelID>"+param.getChannelID()+"</ban:channelID>"+
		            "<ban:billKey1>"+param.getProformaNo()+"</ban:billKey1>"+
		            "<ban:billKey2></ban:billKey2>"+
		            "<ban:billKey3></ban:billKey3>"+
		            "<ban:paidBills>"+
		            "<ban:string></ban:string>"+
		            "</ban:paidBills>"+
		            "<ban:paymentAmount>"+param.getBillingAmount()+"</ban:paymentAmount>"+
		            "<ban:currency>IDR</ban:currency>"+
		            "<ban:transactionID>"+param.getApprovalCode()+"</ban:transactionID>"+
		            "<ban:reference1>"+param.getBankName()+"</ban:reference1>"+
		            "<ban:reference2></ban:reference2>"+
		            "</ban:request>"+
		            "</ban:payment>"+
		            "</soapenv:Body>"+
		            "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            Envelope envelope = new Envelope();
            envelope = gson.fromJson(data.toString(), Envelope.class);
            
			String statusDesc = envelope.getSEnvelope().getSBody().getPaymentResponse().getPaymentResult().getStatus().getStatusDescription();
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "PaymentFlagging", getServiceUrl , user);
          }
        
        return;
     }
	
	public static String PaymentFlagging2(mdlKojaParam param, String orderID){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param.getUsername());
	    LocalDateTime dateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddHHmmss");  
		String transmissionDate = dateTime.format(formatter);  
	    
	    String result = "Sukses";
	    String requestBody = "";
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl3();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ban=\"logol.h2h.billpayment.ws\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		            "<ban:payment>"+
		            "<ban:request>"+
		            "<ban:language>02</ban:language>"+
		            "<ban:trxDateTime>"+orderManagement.getTpsTransactionDate()+"</ban:trxDateTime>"+
		            "<ban:transmissionDateTime>"+transmissionDate+"</ban:transmissionDateTime>"+
		            "<ban:companyCode>"+param.getBankCompanyCode()+"</ban:companyCode>"+
		            "<ban:channelID>"+param.getBankChannelID()+"</ban:channelID>"+
		            "<ban:billKey1>"+orderManagement.getTpsProforma()+"</ban:billKey1>"+
		            "<ban:billKey2></ban:billKey2>"+
		            "<ban:billKey3></ban:billKey3>"+
		            "<ban:paidBills>"+
		            "<ban:string></ban:string>"+
		            "</ban:paidBills>"+
		            "<ban:paymentAmount>"+orderManagement.getTpsBillingAmount()+"</ban:paymentAmount>"+
		            "<ban:currency>IDR</ban:currency>"+
		            "<ban:transactionID>"+orderManagement.getTpsApprovalCode()+"</ban:transactionID>"+
		            "<ban:reference1>"+param.getBankName()+"</ban:reference1>"+
		            "<ban:reference2></ban:reference2>"+
		            "</ban:request>"+
		            "</ban:payment>"+
		            "</soapenv:Body>"+
		            "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            Envelope envelope = new Envelope();
            envelope = gson.fromJson(data.toString(), Envelope.class);
            
			String statusDesc = envelope.getSEnvelope().getSBody().getPaymentResponse().getPaymentResult().getStatus().getStatusDescription();
			if(statusDesc.contains("sukses")){
				//if valid
        		//update status tps to 'flagging'/9 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 9, param.username);
			}
			else{
				result = statusDesc;
				LogAdapter.InsertLogExc(requestBody, "PaymentFlagging2", getServiceUrl, param.username);
			}
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "PaymentFlagging2", getServiceUrl + " " + requestBody, param.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static String KojaGetInvoice(mdlKojaParam param1, String orderID){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.getUsername());
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_GetInvoicewsdl\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<urn:BILLING_GetInvoice soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
		                  "<UserName xsi:type=\"xsd:string\">"+param1.getUsername()+"</UserName>"+
		                  "<Password xsi:type=\"xsd:string\">"+param1.getPassword()+"</Password>"+
		                  "<Creator xsi:type=\"xsd:string\">"+param1.getSession()+"</Creator>"+
		                  "<fStream xsi:type=\"xsd:string\">{\"PROFORMA_INVOICE_NO\":\""+orderManagement.getTpsProforma()+"\"}</fStream>"+
		               "</urn:BILLING_GetInvoice>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetInvoice.Envelope envelope = new modelKojaGetInvoice.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetInvoice.Envelope.class);
            
            mdlGetInvoice resultGetInvoice = new mdlGetInvoice();
            resultGetInvoice =  gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetInvoiceResponse().getReturn().getContent(), mdlGetInvoice.class);
            if(resultGetInvoice.getSTATUS().equals("FALSE") && resultGetInvoice.getMESSAGE().contains("Session expired")){
            	KojaGetLogin(param1);
            	KojaGetInvoice(param1, orderID);
            }
            else if(resultGetInvoice.getSTATUS().equals("FALSE") && !resultGetInvoice.getMESSAGE().contains("Session expired")){
            	result = resultGetInvoice.getMESSAGE();
            	 LogAdapter.InsertLogExc(requestBody, "KojaGetInvoice", getServiceUrl, param1.username);
            }
            else if(resultGetInvoice.getSTATUS().equals("TRUE")){
        		//if valid
        		//update status tps to 'invoice'/10 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 10, param1.username);
            	
            	modelKojaMainGetCustomData.MainGetCustomData customData = GetCustomData(param1, orderManagement.getTpsProforma(), param1.username);
            	String linkInvoice = "";
            	String invoiceNo = resultGetInvoice.getINVOICE_NO();
            	String lastElevenCharInvoiceNo = invoiceNo.substring(invoiceNo.length() - 11);
            	
            	if(customData.getDETAILBILLING().getLINK() != null || !customData.getDETAILBILLING().getLINK().equals("")){
            		linkInvoice = customData.getDETAILBILLING().getLINK();
            		
            		//download url from link proccess
            		Date date = new Date();
        			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        			int year  = localDate.getYear();
        			String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
        			int day   = localDate.getDayOfMonth();
        			String separator = param1.serverFileSeparator;
            		String destinationPath = param1.serverFileLocation + "Invoice" + separator + year + separator + month + separator + day + separator;
            		String finalPath = destinationPath + lastElevenCharInvoiceNo + ".pdf";
            		
            		//initiate variable directory
              		File directory = null;
                    directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
                    
                    //check if directory is exist or not, if not, create it first
                    if(!directory.exists() )
                        directory.mkdirs();
                    
                    InputStream inputStream = new URL(linkInvoice).openStream();
                	Files.copy(inputStream, Paths.get(finalPath), StandardCopyOption.REPLACE_EXISTING);
                	//end of download url from link proccess
            		
            		//change link invoice from koja to local file path 
            		linkInvoice = param1.serverIpport + param1.serverFileAccess + "Invoice" + "/" + year + "/" + month + "/" + day + "/" + lastElevenCharInvoiceNo + ".pdf";
            	}
            	
            	//update tps_invoice no
            	mdlOrderManagement tpsData = new model.mdlOrderManagement();
            	tpsData.setOrderManagementID(orderID);
            	tpsData.setTpsTransactionID(orderManagement.getTpsTransactionID());
            	tpsData.setTpsTransactionDate(orderManagement.getTpsTransactionDate());
            	tpsData.setTpsProforma(orderManagement.getTpsProforma());
            	tpsData.setTpsPaymentID(orderManagement.getTpsPaymentID());
            	tpsData.setTpsApprovalCode(orderManagement.getTpsApprovalCode());
            	tpsData.setTpsApprovalDate(orderManagement.getTpsApprovalDate());
            	tpsData.setTpsBillingAmount(orderManagement.getTpsBillingAmount());
            	tpsData.setTpsInvoiceNo(lastElevenCharInvoiceNo);
            	tpsData.setTpsLinkInvoice(linkInvoice);
            	OrderManagementAdapter.UpdateTpsDataWithInvoice(tpsData, param1.username);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetInvoice", getServiceUrl + " " + requestBody, param1.username);
            result = "Request error. Please contact administrator.";
          }
        
        return result;
     }
	
	public static String KojaGetOTP(mdlKojaParam param1, String orderID, String pin, String container){
		String entityResponse = "";
	    String getServiceUrl = "";
	    /*param1.session = KojaAdapter.GetSession(param1.username);*/
	    
	    mdlOrderManagement orderManagement = new model.mdlOrderManagement();
	    orderManagement = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.getUsername());
	    
	    List<String> containerNumber = ContainerAdapter.LoadContainerByOrder(orderID, param1.username2);
        List<String> invoiceNo = new ArrayList<String>();
	    for(String cn : containerNumber){
	    	invoiceNo.add(orderManagement.getTpsInvoiceNo());
	    }
	    Gson gson = new Gson();
        String newInvoiceNo = gson.toJson(invoiceNo);
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl2();
    	    
        	gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<tpk:sendOTP>"+
		                  "<username>"+param1.getUsername2()+"</username>"+
		                  "<password>"+param1.getPassword2()+"</password>"+
		                  "<fstream>{\"PHONE\":\""+param1.getPhone()+"\",\"CODE\":"+newInvoiceNo+","
		                  		+ "\"PIN\":"+pin+",\"CONTAINER\":"+container+"}</fstream>"+
		               "</tpk:sendOTP>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaGetOTP.Envelope envelope = new modelKojaGetOTP.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetOTP.Envelope.class);
            
            mdlGetOTP resultGetOTP = new mdlGetOTP();
            resultGetOTP =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2SendOTPResponse().getReturn(), mdlGetOTP.class);
            if(!resultGetOTP.getDATA().getSEND_STATUS()){
            	result = "Gagal";
            	LogAdapter.InsertLogExc(requestBody, "KojaGetOTP", getServiceUrl, param1.username2);
            }
            else{
        		//if valid
            	/*result = "Sukses--"+resultGetOTP.getDATA().getOTP();*/
            	result = "Sukses--true";
            	//update status tps to 'getotp'/11 function here
            	//cek apakah sudah melakukan associate
            	Integer orderTpsStatus = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.username2).getTpsBillingStatus();
            	if(orderTpsStatus != 12){ //jika sudah pernah, maka tidak perlu update status 
            		OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 11, param1.username2);
            	}
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetOTP", getServiceUrl + " " + requestBody, param1.username2);
            result = "Gagal";
          }
        
        return result;
     }
	
	//associate dengan metode bulk
	public static String KojaAssociateContainer(mdlKojaParam param1, String orderID, String otp){
		String entityResponse = "";
	    String getServiceUrl = "";
	    param1.session = KojaAdapter.GetSession(param1.username);
	    
	    List<model.mdlDeliveryOrder> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(orderID, param1.username2);
        
	    Gson gson = new Gson();
	    List<String> invoiceNo = new ArrayList<String>();
	    List<String> containerNo = new ArrayList<String>();
	    List<String> sealNo = new ArrayList<String>();
	    List<String> tidNo = new ArrayList<String>();
	    List<String> oldTidNo = new ArrayList<String>();
	    List<String> action = new ArrayList<String>();
	    
	    for(mdlDeliveryOrder orderDetail : listDO){
	    	invoiceNo.add(orderDetail.getKojaInvoiceNo());
	    	containerNo.add(orderDetail.getContainerNumber());
	    	sealNo.add(orderDetail.getSealNumber());
	    	tidNo.add(orderDetail.getTidNumber());
	    	oldTidNo.add(orderDetail.getOldTidNumber());
	    	action.add("P");
	    }
	    
        String newInvoiceNo = gson.toJson(invoiceNo);
        String newContainerNo = gson.toJson(containerNo);
        String newSealNo = gson.toJson(sealNo);
        String newTidNo = gson.toJson(tidNo);
        String newOldTidNo = gson.toJson(oldTidNo);
        String newAction = gson.toJson(action);
	    
	    String result = "Sukses";
	    String requestBody = "";
	    Boolean statusAllRequest = true;
	    
        try {
        	// Get the base naming context from web.xml
        	getServiceUrl  = param1.getServiceUrl2();
        	
        	//cek apakah sudah melakukan associate
        	Integer orderTpsStatus = OrderManagementAdapter.LoadTpsDataByOrder(orderID, param1.username2).getTpsBillingStatus();
        	if(orderTpsStatus == 12){ //jika sudah maka hapus data associate sebelumnya agar bisa reassociate
        		gson = new Gson(); 
            	Client client2 = Client.create();
            	
            	WebResource webResource2 = client2.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
    		            "<soapenv:Header/>"+
    		            "<soapenv:Body>"+
    		               "<tpk:ammendAssociate>"+
    		                  "<username>"+param1.getUsername2()+"</username>"+
    		                  "<password>"+param1.getPassword2()+"</password>"+
    		                  "<fstream>{\"OTP\":\""+otp+"\",\"OLD_TID\":"+newOldTidNo+","
    		                  		+ "\"OLD_CONTAINER\":"+newContainerNo+"}</fstream>"+
    		               "</tpk:ammendAssociate>"+
    		            "</soapenv:Body>"+
    		         "</soapenv:Envelope>";
            
                ClientResponse response2  = webResource2.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);
                
                entityResponse = response2.getEntity(String.class);
                JSONObject dataAmmendAssociate = XML.toJSONObject(entityResponse);
                modelKojaAmmendAssociate.Envelope envelope = new modelKojaAmmendAssociate.Envelope();
                envelope = gson.fromJson(dataAmmendAssociate.toString(), modelKojaAmmendAssociate.Envelope.class);
                
                //cek apakah proses ammend timeout atau tidak dengan metode bulk ammend
                if(envelope.getSEnvelope().getSBody().getNs2AmmentAssociateResponse() == null){
                	//jika timeout, lakukan metode looping ammend
                	KojaAmmendAssociateLooping(param1, orderID, otp);
                }
                else{
                	//jika tidak timeout, lanjut ke proses selanjutnya
                	mdlAmmendAssociateContainer resultAmmendAssociateContainer = new mdlAmmendAssociateContainer();
                    resultAmmendAssociateContainer =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2AmmentAssociateResponse().getReturn(), mdlAmmendAssociateContainer.class);
            	
                    for(mdlAmmendAssociatedData ammendAssoData : resultAmmendAssociateContainer.getDATA().getAMMEND_DATA()){
                    	if(!ammendAssoData.getSTATUS()){
                    		LogAdapter.InsertLogExc("order:" + orderID + ", cont :" + ammendAssoData.getCONTAINER() + ", message:" + ammendAssoData.getMESSAGE(), 
                    				"KojaAmmendAssociate", 
                    				getServiceUrl + " " + requestBody, param1.username2);
                    	}
                    }
                }
        	} 
        	//end of ammend associate/hapus data associate sebelumnya
        	
        	
        	//associate process
        	gson = new Gson(); 
        	Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		               "<tpk:associate>"+
		                  "<username>"+param1.getUsername2()+"</username>"+
		                  "<password>"+param1.getPassword2()+"</password>"+
		                  "<fstream>{\"OTP\":\""+otp+"\",\"CODE\":"+newInvoiceNo+","
		                  		+ "\"TID\":"+newTidNo+",\"CONTAINER\":"+newContainerNo+","
		                  		+ "\"ACTION\":"+newAction+",\"SEAL_NBR\":"+newSealNo+"}</fstream>"+
		               "</tpk:associate>"+
		            "</soapenv:Body>"+
		         "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            modelKojaAssociateContainer.Envelope envelope = new modelKojaAssociateContainer.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaAssociateContainer.Envelope.class);
            
            //cek apakah timeout dengan metode bulk associate
            if(envelope.getSEnvelope().getSBody().getNs2AssociateResponse() == null){
            	//jika timeout, proses dengan metode looping associate
            	result = KojaAssociateContainerLooping(param1, orderID, otp);
            }
            else{
            	//jika tidak, lanjut ke proses selanjutnya
            	mdlAssociateContainer resultAssociateContainer = new mdlAssociateContainer();
                resultAssociateContainer =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2AssociateResponse().getReturn(), mdlAssociateContainer.class);
                
                //cek kalau session otp expired atau tidak
                if(resultAssociateContainer.getDATA().getASSOCIATED_DATA() == null){
                	statusAllRequest = false;
            		LogAdapter.InsertLogExc("order:" + orderID + ", message: otp session end or try to check it again" , 
            				"KojaAssociateContainer", 
            				getServiceUrl + " " + requestBody, param1.username2);
                }
                else{
                	//jika session oke dan data associatenya dapat dari koja, maka lakuin ini
                	for(mdlAssociatedData assoData : resultAssociateContainer.getDATA().getASSOCIATED_DATA()){
                    	if(assoData.getSTATUS()){
                    		//load driver and vodt id by tidnumber
                    		mdlVendorOrderDetail vodt = new model.mdlVendorOrderDetail();
                    		vodt = VendorOrderDetailAdapter.LoadDriverByTIDAssociate(orderID, assoData.getTID(), param1.username2);
                    		
                    		//update tps data for vendor order detail
                    		VendorOrderDetailAdapter.UpdateVendorOrderDetailTps(assoData, vodt.getVendorOrderDetailID(), param1.username2);
                    		
                    		//push notif to driver
                    		PushNotifAdapter.PushNotification("associateTicket&&"+assoData.getVALID_THRU(), 
            						vodt.driverID, 
            						param1.username2);
                    	}
                    	else{
                    		statusAllRequest = false;
                    		LogAdapter.InsertLogExc("order:" + orderID + ", cont :" + assoData.getCONTAINER() + ", message:" + assoData.getMESSAGE(), 
                    				"KojaAssociateContainer", 
                    				getServiceUrl + " " + requestBody, param1.username2);
                    	}
                    }
                }
                
                if(!statusAllRequest){
                	result = "Gagal";
                }
                else{
            		//if valid
                	//update status tps to 'associate'/12 function here
                	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 12, param1.username2);
                }
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaAssociateContainer", getServiceUrl + " " + requestBody, param1.username2);
            result = "Gagal";
          }
        
        return result;
     }
	
	
	//associate dengan metode loop
	public static String KojaAssociateContainerLooping(mdlKojaParam param1, String orderID, String otp){
		String entityResponse = "";
	    String getServiceUrl = "";
	    /*param1.session = KojaAdapter.GetSession(param1.username);*/
	    
	    List<model.mdlDeliveryOrder> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(orderID, param1.username2);
        
	    Gson gson = new Gson();
	    
	    String result = "Sukses";
	    String requestBody = "";
	    
	    try {
		     Boolean statusAllRequest = true;
		     
		     //Get the base naming context from web.xml
	         getServiceUrl  = param1.getServiceUrl2();
			
	         //looping container associate process
		    for(mdlDeliveryOrder orderDetail : listDO){
		    	gson = new Gson(); 
	        	Client client = Client.create();
	        	WebResource webResource = client.resource(getServiceUrl);
	        	
		    	String newInvoiceNo = gson.toJson(orderDetail.getKojaInvoiceNo());
		        String newContainerNo = gson.toJson(orderDetail.getContainerNumber());
		        String newSealNo = gson.toJson(orderDetail.getSealNumber());
		        String newTidNo = gson.toJson(orderDetail.getTidNumber());
		        String newAction = gson.toJson("P");
		        
	            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:associate>"+
			                  "<username>"+param1.getUsername2()+"</username>"+
			                  "<password>"+param1.getPassword2()+"</password>"+
			                  "<fstream>{\"OTP\":\""+otp+"\",\"CODE\":["+newInvoiceNo+"],"
			                  		+ "\"TID\":["+newTidNo+"],\"CONTAINER\":["+newContainerNo+"],"
			                  		+ "\"ACTION\":["+newAction+"],\"SEAL_NBR\":["+newSealNo+"]}</fstream>"+
			               "</tpk:associate>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
	            
	            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                        .post(ClientResponse.class,requestBody); 
	            
	            entityResponse = response.getEntity(String.class);
	            JSONObject data = XML.toJSONObject(entityResponse);
	            modelKojaAssociateContainer.Envelope envelope = new modelKojaAssociateContainer.Envelope();
	            envelope = gson.fromJson(data.toString(), modelKojaAssociateContainer.Envelope.class);
	            
	            mdlAssociateContainer resultAssociateContainer = new mdlAssociateContainer();
	            resultAssociateContainer =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2AssociateResponse().getReturn(), mdlAssociateContainer.class);
		    
	            //cek kalau session otp expired atau tidak
	            if(resultAssociateContainer.getDATA().getASSOCIATED_DATA() == null){
	            	statusAllRequest = false;
	        		LogAdapter.InsertLogExc("order:" + orderID + ", message: otp session end or try to check it again" , 
	        				"KojaAssociateContainerLooping", 
	        				getServiceUrl + " " + requestBody, param1.username2);
	            }
	            else{
	            	//jika session oke dan data associatenya dapat dari koja, maka lakuin ini
	            	for(mdlAssociatedData assoData : resultAssociateContainer.getDATA().getASSOCIATED_DATA()){
	                	if(assoData.getSTATUS()){
	                		//load driver and vodt id by tidnumber
	                		mdlVendorOrderDetail vodt = new model.mdlVendorOrderDetail();
	                		vodt = VendorOrderDetailAdapter.LoadDriverByTIDAssociate(orderID, assoData.getTID(), param1.username2);
	                		
	                		//update tps data for vendor order detail
	                		VendorOrderDetailAdapter.UpdateVendorOrderDetailTps(assoData, vodt.getVendorOrderDetailID(), param1.username2);
	                		
	                		//push notif to driver
	                		PushNotifAdapter.PushNotification("associateTicket&&"+assoData.getVALID_THRU(), 
	        						vodt.driverID, 
	        						param1.username2);
	                	}
	                	else{
	                		statusAllRequest = false;
	                		LogAdapter.InsertLogExc("order:" + orderID + ", cont :" + assoData.getCONTAINER() + ", message:" + assoData.getMESSAGE(), 
	                				"KojaAssociateContainerLooping", 
	                				getServiceUrl + " " + requestBody, param1.username2);
	                	}
	                }
	            }
		    }
		    //end of looping associate process
            
            if(!statusAllRequest){
            	result = "Gagal";
            }
            else{
        		//if valid
            	//update status tps to 'associate'/12 function here
            	OrderManagementAdapter.UpdateOrderTpsStatus(orderID, 12, param1.username2);
            }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaAssociateContainerLooping", getServiceUrl + " " + requestBody, param1.username2);
            result = "Gagal";
          }
        
        return result;
     }
	
	//ammend associate dengan metode loop
	public static void KojaAmmendAssociateLooping(mdlKojaParam param1, String orderID, String otp){
		String entityResponse = "";
	    String getServiceUrl = "";
	    /*param1.session = KojaAdapter.GetSession(param1.username);*/
	    
	    List<model.mdlDeliveryOrder> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(orderID, param1.username2);
	    
	    // Get the base naming context from web.xml
    	getServiceUrl  = param1.getServiceUrl2();
    	String requestBody = "";
	    
	    try {
		    for(mdlDeliveryOrder orderDetail : listDO){
		    	Gson gson = new Gson();
		    	Client client2 = Client.create();
		    	
		        String newContainerNo = gson.toJson(orderDetail.getContainerNumber());
		        String newOldTidNo = gson.toJson(orderDetail.getOldTidNumber());
		        
		        WebResource webResource2 = client2.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
    		            "<soapenv:Header/>"+
    		            "<soapenv:Body>"+
    		               "<tpk:ammendAssociate>"+
    		                  "<username>"+param1.getUsername2()+"</username>"+
    		                  "<password>"+param1.getPassword2()+"</password>"+
    		                  "<fstream>{\"OTP\":\""+otp+"\",\"OLD_TID\":["+newOldTidNo+"],"
    		                  		+ "\"OLD_CONTAINER\":["+newContainerNo+"]}</fstream>"+
    		               "</tpk:ammendAssociate>"+
    		            "</soapenv:Body>"+
    		         "</soapenv:Envelope>";
            
                ClientResponse response2  = webResource2.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);
                
                entityResponse = response2.getEntity(String.class);
                JSONObject dataAmmendAssociate = XML.toJSONObject(entityResponse);
                modelKojaAmmendAssociate.Envelope envelope = new modelKojaAmmendAssociate.Envelope();
                envelope = gson.fromJson(dataAmmendAssociate.toString(), modelKojaAmmendAssociate.Envelope.class);
                
                mdlAmmendAssociateContainer resultAmmendAssociateContainer = new mdlAmmendAssociateContainer();
                resultAmmendAssociateContainer =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2AmmentAssociateResponse().getReturn(), mdlAmmendAssociateContainer.class);
                
                for(mdlAmmendAssociatedData ammendAssoData : resultAmmendAssociateContainer.getDATA().getAMMEND_DATA()){
                	if(!ammendAssoData.getSTATUS()){
                		LogAdapter.InsertLogExc("order:" + orderID + ", cont :" + ammendAssoData.getCONTAINER() + ", message:" + ammendAssoData.getMESSAGE(), 
                				"KojaAmmendAssociateLooping", 
                				getServiceUrl + " " + requestBody, param1.username2);
                	}
                }
		    }   
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaAmmendAssociateLooping", getServiceUrl + " " + requestBody, param1.username2);
          }
        
        return;
     }
	
	//associate metode lama/ sudah tidak dipakai
	public static void AssociateTicket(mdlAssociateTicketParam param, String user){   
		String entityResponse = "";
	    String getServiceUrl = "";
	   /* mdlPaymentFlagging requestPaymentFlagging = new mdlPaymentFlagging();*/
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ass=\"http://associated/\">"+
		            "<soapenv:Header/>"+
		            "<soapenv:Body>"+
		            "<ass:eticket_getData>"+
		            "<USERNAME>"+param.username+"</USERNAME>"+
		            "<PASSWORD>"+param.password+"</PASSWORD>"+
		            "<JUMLAH_DATA>"+param.dataCount+"</JUMLAH_DATA>"+
		            "<STATUS>"+param.status+"</STATUS>"+
		            "<TID>"+param.tid+"</TID>"+
		            "<NO_CONTAINER>{\"NO_CONTAINER\":[\""+param.containerNumber+"\"]}</NO_CONTAINER>"+
		            "<SEAL_NUMBER>{\"SEAL_NUMBER\":[\""+param.sealNumber+"\"]}</SEAL_NUMBER>"+
		            "<URUTAN_GATEIN></URUTAN_GATEIN>"+
		            "<ETICKET_ID></ETICKET_ID>"+
		            "</ass:eticket_getData>"+
		            "</soapenv:Body>"+
		            "</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            /*if(entityResponse.contains("<S:Fault")){*/
            	PushNotifAdapter.PushNotification("associateTicket&&30 Juni 2019 12:00:00&&2000", 
						param.getDriverID(), 
						user);
            /*}
            else{
            	JSONObject data = XML.toJSONObject(entityResponse);
            	modelKojaAssociateTicket.Envelope envelope = new modelKojaAssociateTicket.Envelope();
                envelope = gson.fromJson(data.toString(), modelKojaAssociateTicket.Envelope.class);
                
                mdlResponseAssociateTicket responseAssociateTicket = new mdlResponseAssociateTicket();
    			responseAssociateTicket = gson.fromJson(envelope.getSEnvelope().getSBody().getNs2EticketGetDataResponse().getReturn(), mdlResponseAssociateTicket.class);
    			PushNotifAdapter.PushNotification("associateTicket&&"+responseAssociateTicket.getDATA_ASSOCIATED().getPrintReturn().getPrintOutHeader().getValidThrough()[0]+"&&"+responseAssociateTicket.getDATA_ASSOCIATED().getPrintReturn().getPrintList().getPrintOut().geteTicketId()[0], 
    												param.getDriverID(), 
    												user);
            }*/
        } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "AssociateTicket", getServiceUrl , user);
          }
        
        return;
     }
	
	public static modelKojaGetBillingDetail.DETAILBILLING GetBillingDetail(mdlKojaParam param, String proformaID, String user){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    DETAILBILLING summarydetail = null;
	   /* mdlPaymentFlagging requestPaymentFlagging = new mdlPaymentFlagging();*/
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_GetBillingwsdl\">\r\n" + 
            		"   <soapenv:Header/>\r\n" + 
            		"   <soapenv:Body>\r\n" + 
            		"      <urn:BILLING_GetBillingDetail soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n" + 
            		"         <UserName xsi:type=\"xsd:string\">"+param.username+"</UserName>\r\n" + 
            		"         <Password xsi:type=\"xsd:string\">"+param.password+"</Password>\r\n" + 
            		"         <Creator xsi:type=\"xsd:string\">"+param.getSession()+"</Creator>\r\n" + 
            		"         <fStream xsi:type=\"xsd:string\">{\"PROFORMA_INVOICE_NO\":\""+proformaID+"\",\"TRANSACTIONS_TYPE_ID\":\"\"}</fStream>\r\n" + 
            		"      </urn:BILLING_GetBillingDetail>\r\n" + 
            		"   </soapenv:Body>\r\n" + 
            		"</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            
            
            modelKojaGetBillingDetail.Envelope envelope = new modelKojaGetBillingDetail.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetBillingDetail.Envelope.class);
            
            modelKojaGetBillingDetail.KojaBillingDetail billingDetail = new modelKojaGetBillingDetail.KojaBillingDetail();
            billingDetail = gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetBillingDetailResponse().getReturn().getContent(), modelKojaGetBillingDetail.KojaBillingDetail.class);
            
            
            
//            System.out.println(billingDetail.getDETAILBILLING().getSUMMARYDETAIL().getCOMPONENTGROUPID());
            
            summarydetail = billingDetail.getDETAILBILLING();
            
        } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetBillingDetail", getServiceUrl , user);
        }
        
		return summarydetail;
     }
	
	
	public static modelKojaMainGetCustomData.MainGetCustomData GetCustomData(mdlKojaParam param, String proformaID, String user){   
		String entityResponse = "";
	    String getServiceUrl = "";
	    modelKojaMainGetCustomData.MainGetCustomData summarydetail = null;
	   /* mdlPaymentFlagging requestPaymentFlagging = new mdlPaymentFlagging();*/
        
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.getServiceUrl();
    	    
        	Gson gson = new Gson(); 
            Client client = Client.create();
            
            WebResource webResource = client.resource(getServiceUrl);
            String requestBody = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BILLING_GetBillingwsdl\">\r\n" + 
            		"   <soapenv:Header/>\r\n" + 
            		"   <soapenv:Body>\r\n" + 
            		"      <urn:BILLING_GetBillingDetail soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n" + 
            		"         <UserName xsi:type=\"xsd:string\">"+param.username+"</UserName>\r\n" + 
            		"         <Password xsi:type=\"xsd:string\">"+param.password+"</Password>\r\n" + 
            		"         <Creator xsi:type=\"xsd:string\">"+param.getSession()+"</Creator>\r\n" + 
            		"         <fStream xsi:type=\"xsd:string\">{\"METHOD\":\"getLinkPDFInvoice\", \"PROFORMA_INVOICE_NO\":\""+proformaID+"\"}</fStream>\r\n" + 
            		"      </urn:BILLING_GetBillingDetail>\r\n" + 
            		"   </soapenv:Body>\r\n" + 
            		"</soapenv:Envelope>";
        
            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                  .post(ClientResponse.class,requestBody);        
            
            entityResponse = response.getEntity(String.class);
            JSONObject data = XML.toJSONObject(entityResponse);
            
            
            modelKojaGetBillingDetail.Envelope envelope = new modelKojaGetBillingDetail.Envelope();
            envelope = gson.fromJson(data.toString(), modelKojaGetBillingDetail.Envelope.class);
            
            modelKojaMainGetCustomData.MainGetCustomData billingDetail = new modelKojaMainGetCustomData.MainGetCustomData();
            billingDetail = gson.fromJson(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetBillingDetailResponse().getReturn().getContent(), modelKojaMainGetCustomData.MainGetCustomData.class);
            
            
            
//            System.out.println(billingDetail.getDETAILBILLING().getSUMMARYDETAIL().getCOMPONENTGROUPID());
            
            summarydetail = billingDetail;
            System.out.println(envelope.getSOAPENVEnvelope().getSOAPENVBody().getNs1BILLINGGetBillingDetailResponse().getReturn().getContent());
        } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetCustomData", getServiceUrl , user);
        }
        
		return summarydetail;
     }
	
	public static void KojaContainerTracking(mdlKojaParam param){
		String entityResponse = "";
	    String getServiceUrl = "";
	    
	    List<String> listContainerInKoja = ContainerAdapter.LoadContainerInKoja(param.assoUsername);
        
	    Gson gson = new Gson();
	    List<String> action = new ArrayList<String>();
	    
	    for(String container : listContainerInKoja){
	    	action.add(param.action);
	    }
	    
        String newAction = gson.toJson(action);
        String newContainer = gson.toJson(listContainerInKoja);
	    
	    /*String result = "Sukses";*/
	    String requestBody = "";
	    
        try {
        	if(!listContainerInKoja.isEmpty()){
	        	// Get the base naming context from web.xml
	        	getServiceUrl  = param.getServerUrlAsso();
	        	
	        	gson = new Gson(); 
	        	Client client = Client.create();
	            
	            WebResource webResource = client.resource(getServiceUrl);
	            requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
			            "<soapenv:Header/>"+
			            "<soapenv:Body>"+
			               "<tpk:container_tracking>"+
			                  "<username>"+param.getAssoUsername()+"</username>"+
			                  "<password>"+param.getAssoPassword()+"</password>"+
			                  "<fstream>{\"CONTAINER\":"+newContainer+",\"ACTION\":"+newAction+"}</fstream>"+
			               "</tpk:container_tracking>"+
			            "</soapenv:Body>"+
			         "</soapenv:Envelope>";
	        
	            ClientResponse response  = webResource.type("text/xml").accept("text/xml")
	                                                  .post(ClientResponse.class,requestBody);        
	            
	            entityResponse = response.getEntity(String.class);
	            JSONObject data = XML.toJSONObject(entityResponse);
	            modelKojaContainerTracking.Envelope envelope = new modelKojaContainerTracking.Envelope();
	            envelope = gson.fromJson(data.toString(), modelKojaContainerTracking.Envelope.class);
	            
	            mdlContainerTracking resultContainerTracking = new mdlContainerTracking();
	            resultContainerTracking =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2Container_trackingResponse().getReturn(), mdlContainerTracking.class);
	            
	            for(mdlContainerData containerData : resultContainerTracking.getDATA().getCONTAINER_DATA()){
	            	if(containerData.getSTATUS()){
	            		if(containerData.getSTACK_ON_VESSEL() == null)
	            			containerData.setSTACK_ON_VESSEL("have not been stacked on vessel");
	            		
            			//load vodt id by container number
	            		String vodtID = "";
	            		vodtID = VendorOrderDetailAdapter.LoadVendorOrderDetailByContainer(containerData.CNTR_ID, param.assoUsername).getVendorOrderDetailID();
	            		
	            		//update tps data for vendor order detail
	            		VendorOrderDetailAdapter.UpdateVendorOrderDetailShippingLocation(containerData, vodtID, param.assoUsername);
	            	}
	            	else{
	            		LogAdapter.InsertLogExc("cont :" + containerData.getCNTR_ID() + ", message:" + containerData.getMESSAGE(), 
	            				"KojaContainerTracking", 
	            				getServiceUrl + " " + requestBody, param.assoUsername);
	            	}
	            }
        	}
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaContainerTracking", getServiceUrl + " " + requestBody, param.assoUsername);
          }
        
        return;
     }
	
	//koja get eir
	public static void KojaGetEir(mdlKojaParam param){
		String entityResponse = "";
	    String getServiceUrl = "";
	    
	    List<model.mdlVendorOrderDetail> listVODT = ContainerAdapter.LoadTidByEmptyKojaEir(param.assoUsername);
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.serverUrlAsso;
    	    
        	for(model.mdlVendorOrderDetail vodt : listVODT){
        		Gson gson = new Gson(); 
                Client client = Client.create();
                
                WebResource webResource = client.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
    			            "<soapenv:Header/>"+
    			            "<soapenv:Body>"+
    			               "<tpk:customService>"+
    			                  "<username>"+param.assoUsername+"</username>"+
    			                  "<password>"+param.assoPassword+"</password>"+
    			                  "<fstream>{\"METHOD\":\"getEIR\",\"TID\":\""+vodt.tidNumber+"\"}</fstream>"+
    			               "</tpk:customService>"+
    			            "</soapenv:Body>"+
    			         "</soapenv:Envelope>";
            
                ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);        
                
                entityResponse = response.getEntity(String.class);
                JSONObject data = XML.toJSONObject(entityResponse);
                modelKojaGetEir.Envelope envelope = new modelKojaGetEir.Envelope();
                envelope = gson.fromJson(data.toString(), modelKojaGetEir.Envelope.class);
                
                mdlGetEir eirData = new mdlGetEir();
                eirData =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2CustomServiceResponse().getReturn(), mdlGetEir.class);
                
                if(eirData.LINK == null || !eirData.STATUS.equals("TRUE") || eirData.LINK.equals("")){ //if invalid do this
                	LogAdapter.InsertLogExc("tid :" + vodt.tidNumber + ", message: gagal mendapatkan eir", 
            				"KojaGetEir", 
            				getServiceUrl + " " + requestBody, param.assoUsername);
                }
                else{ //if valid do this
                	//download url from link proccess
            		Date date = new Date();
            		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            		int year  = localDate.getYear();
            		String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
            		int day   = localDate.getDayOfMonth();
            		String destinationPath = param.serverFileLocation + "EIR" + param.serverFileSeparator + year + param.serverFileSeparator + month + param.serverFileSeparator + day + param.serverFileSeparator;
            		String finalPath = destinationPath + vodt.tidNumber + ".pdf";
            		
            		//initiate variable directory
              		File directory = null;
                    directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
                    
                    //check if directory is exist or not, if not, create it first
                    if(!directory.exists() )
                        directory.mkdirs();
                    
                    InputStream inputStream = new URL(eirData.LINK).openStream();
                	Files.copy(inputStream, Paths.get(finalPath), StandardCopyOption.REPLACE_EXISTING);
                	//end of download url from link proccess
                	
                	//change link eir from koja to local file path 
            		eirData.LINK = param.serverIpport + param.serverFileAccess + "EIR" + "/" + year + "/" + month + "/" + day + "/" + vodt.tidNumber + ".pdf";
            		
            		//update eir
                	eirData.vendorOrderDetailID = vodt.vendorOrderDetailID;
                	VendorOrderDetailAdapter.UpdateKojaEir(eirData);
                }
    	    }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetEir", getServiceUrl + " " + requestBody, param.assoUsername);
          }
        
        return;
     }
	
	public static void KojaGetCms(mdlKojaParam param){
		String entityResponse = "";
	    String getServiceUrl = "";
	    
	    List<model.mdlVendorOrderDetail> listVODT = ContainerAdapter.LoadTidByEmptyKojaCms(param.assoUsername);
        
        String requestBody = "";
	    
        try {        
        	// Get the base naming context from web.xml
        	getServiceUrl  = param.serverUrlAsso;
    	    
        	for(model.mdlVendorOrderDetail vodt : listVODT){
        		Gson gson = new Gson(); 
                Client client = Client.create();
                
                WebResource webResource = client.resource(getServiceUrl);
                requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tpk=\"tpkkoja.terminal.service\">"+
    			            "<soapenv:Header/>"+
    			            "<soapenv:Body>"+
    			               "<tpk:customService>"+
    			                  "<username>"+param.assoUsername+"</username>"+
    			                  "<password>"+param.assoPassword+"</password>"+
    			                  "<fstream>{\"METHOD\":\"getCMS\",\"TID\":\""+vodt.tidNumber+"\",\"CNTR_ID\":\""+vodt.containerNumber+"\"}</fstream>"+
    			               "</tpk:customService>"+
    			            "</soapenv:Body>"+
    			         "</soapenv:Envelope>";
            
                ClientResponse response  = webResource.type("text/xml").accept("text/xml")
                                                      .post(ClientResponse.class,requestBody);        
                
                entityResponse = response.getEntity(String.class);
                JSONObject data = XML.toJSONObject(entityResponse);
                modelKojaGetCms.Envelope envelope = new modelKojaGetCms.Envelope();
                envelope = gson.fromJson(data.toString(), modelKojaGetCms.Envelope.class);
                
                mdlGetCms cmsData = new mdlGetCms();
                cmsData =  gson.fromJson(envelope.getSEnvelope().getSBody().getNs2CustomServiceResponse().getReturn(), mdlGetCms.class);
                
                if(cmsData.LINK == null || !cmsData.STATUS.equals("TRUE") || cmsData.LINK.equals("")){ //if invalid do this
                	/*LogAdapter.InsertLogExc("tid :" + vodt.tidNumber + ", message: gagal mendapatkan cms", 
            				"KojaGetCms", 
            				getServiceUrl + " " + requestBody, param.assoUsername);*/
                }
                else{ //if valid do this
                	//download url from link proccess
            		Date date = new Date();
            		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            		int year  = localDate.getYear();
            		String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
            		int day   = localDate.getDayOfMonth();
            		String destinationPath = param.serverFileLocation + "CMS" + param.serverFileSeparator + year + param.serverFileSeparator + month + param.serverFileSeparator + day + param.serverFileSeparator;
            		String finalPath = destinationPath + vodt.tidNumber + ".pdf";
            		
            		//initiate variable directory
              		File directory = null;
                    directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
                    
                    //check if directory is exist or not, if not, create it first
                    if(!directory.exists() )
                        directory.mkdirs();
                    
                    InputStream inputStream = new URL(cmsData.LINK).openStream();
                	Files.copy(inputStream, Paths.get(finalPath), StandardCopyOption.REPLACE_EXISTING);
                	//end of download url from link proccess
                	
                	//change link cms from koja to local file path 
                	cmsData.LINK = param.serverIpport + param.serverFileAccess + "CMS" + "/" + year + "/" + month + "/" + day + "/" + vodt.tidNumber + ".pdf";
            		
            		//update cms
                	cmsData.vendorOrderDetailID = vodt.vendorOrderDetailID;
                	VendorOrderDetailAdapter.UpdateKojaCms(cmsData);
                }
    	    }
          } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "KojaGetCms", getServiceUrl + " " + requestBody, param.assoUsername);
          }
        
        return;
     }
	
}
