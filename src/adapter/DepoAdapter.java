package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlDepo;

public class DepoAdapter {
	public static List<model.mdlDepo> LoadDepo (String user) {
		List<model.mdlDepo> listDepo = new ArrayList<model.mdlDepo>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadDepo()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadDepo", user);

			while(jrs.next()){
				model.mdlDepo depo = new model.mdlDepo();
				depo.setDepoID(jrs.getString("DepoID"));
				depo.setName(jrs.getString("Name"));
				depo.setAddress(jrs.getString("Address"));
				listDepo.add(depo);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDepo", sql , user);
		}

		return listDepo;
	}

	public static model.mdlDepo LoadDepoByID (String lDepoID, String user) {
		model.mdlDepo mdlDepo = new model.mdlDepo();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadDepoByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lDepoID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDepoByID", user);

			while(jrs.next()){
				mdlDepo.setDepoID(jrs.getString("DepoID"));
				mdlDepo.setName(jrs.getString("Name"));
				mdlDepo.setAddress(jrs.getString("Address"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDepoByID", sql , user);
		}

		return mdlDepo;
	}

	public static String InsertDepo(mdlDepo mdlDepo, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			String newDepoID = CreateDepoID(user);
			sql = "{call sp_InsertDepo(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", newDepoID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDepo.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDepo.getAddress()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertDepo", user);

			result = success == true ? "Success Insert Depo" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertDepo", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateDepo(mdlDepo mdlDepo, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateDepo(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDepo.getDepoID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDepo.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlDepo.getAddress()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateDepo", user);

			result = success == true ? "Success Update Depo" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateDepo", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteDepo(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteDepo(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteDepo", user);

			result = success == true ? "Success Delete Depo" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteDepo", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateDepoID(String user){
		String lastDepoID = GetLastDepoID(user);
		String stringIncrement = "0001";

		if ((lastDepoID != null) && (!lastDepoID.equals("")) ){
			String[] partsDepoID = lastDepoID.split("-");
			//get last running number
			String partNumber = partsDepoID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("DEPO-").append(stringIncrement);
		String DepoID = sb.toString();
		return DepoID;
	}

	public static String GetLastDepoID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String DepoID = "";

		try {
			sql = "{call sp_GetLastDepoID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastDepoID", user);

			while(jrs.next()){
				DepoID = jrs.getString("DepoID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastDepoID", sql , user);
		}

		return DepoID;
	}
}
