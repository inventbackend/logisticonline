package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;

public class VendorOrderAdapter {
	public static List<model.mdlVendorOrder> LoadVendorOrder(String vendorID, String dateFrom, String dateTo, String orderType){
		List<model.mdlVendorOrder> listVendorOrder = new ArrayList<model.mdlVendorOrder>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_LoadVendorOrder(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(dateTo, "dd MMM yyyy", "yyyy-MM-dd")));
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderType));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrder", vendorID);

			while(jrs.next()){
				model.mdlVendorOrder mdlVendorOrder = new model.mdlVendorOrder();
				mdlVendorOrder.setVendorOrderID(jrs.getString("VendorOrderID"));
				mdlVendorOrder.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				mdlVendorOrder.setDeliveryOrderID(jrs.getString("DeliveryOrderID"));
				mdlVendorOrder.setOrderManagementID(jrs.getString("OrderManagementID"));
			    mdlVendorOrder.setOrderManagementDetailID(jrs.getString("OrderManagementDetailID"));
			    mdlVendorOrder.setVendorID(jrs.getString("VendorID"));
			    mdlVendorOrder.setVendorName(jrs.getString(5));
			    mdlVendorOrder.setVendorStatus(jrs.getString("VendorStatus"));
			    mdlVendorOrder.setQuantity(jrs.getInt("Quantity"));
			    mdlVendorOrder.setQuantityDelivered(jrs.getInt(8));
			    Double tempPrice = jrs.getDouble("Price");
			    mdlVendorOrder.setPrice(tempPrice.intValue());
			    mdlVendorOrder.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlVendorOrder.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlVendorOrder.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlVendorOrder.setOrderType(jrs.getString("OrderType"));
			    mdlVendorOrder.setItem(jrs.getString("itemDescription"));
			    mdlVendorOrder.setCustomer(jrs.getString(13));
			    mdlVendorOrder.setDistrict(jrs.getString(14));
			    mdlVendorOrder.setDepo(jrs.getString(15));
			    mdlVendorOrder.setPort(jrs.getString(16)+" - "+jrs.getString(17));
			    mdlVendorOrder.setAddress(jrs.getString("Address"));
			    mdlVendorOrder.setShippingLine(jrs.getString(22));
			    mdlVendorOrder.setFactory(jrs.getString(23));
			    mdlVendorOrder.setContainerType(jrs.getString("Description"));
			    	
			    listVendorOrder.add(mdlVendorOrder);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrder", sql , vendorID);
		}
		return listVendorOrder;
	}

	public static model.mdlVendorOrder LoadVendorOrderByID(String vendorOrderID, String user){
		model.mdlVendorOrder mdlVendorOrder = new model.mdlVendorOrder();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call sp_VendorOrderLoadByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderByID", user);

			while(jrs.next()){
				mdlVendorOrder.setVendorOrderID(jrs.getString("VendorOrderID"));
				mdlVendorOrder.setShippingInvoiceID(jrs.getString("ShippingInvoiceID"));
				mdlVendorOrder.setOrderManagementID(jrs.getString("OrderManagementID"));
			    mdlVendorOrder.setOrderManagementDetailID(jrs.getString("OrderManagementDetailID"));
			    mdlVendorOrder.setVendorID(jrs.getString("VendorID"));
			    mdlVendorOrder.setVendorName(jrs.getString(5));
			    mdlVendorOrder.setVendorStatus(jrs.getString("VendorStatus"));
			    mdlVendorOrder.setQuantity(jrs.getInt("Quantity"));
			    mdlVendorOrder.setQuantityDelivered(jrs.getInt(8));
			    Double tempPrice = jrs.getDouble("Price");
			    mdlVendorOrder.setPrice(tempPrice.intValue());
			    mdlVendorOrder.setCreatedBy(jrs.getString("CreatedBy"));
			    mdlVendorOrder.setUpdatedBy(jrs.getString("UpdatedBy"));
			    mdlVendorOrder.setOrderStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMM yyyy"));
			    mdlVendorOrder.setOrderType(jrs.getString("OrderType"));
			    mdlVendorOrder.setItem(jrs.getString("itemDescription"));
			    mdlVendorOrder.setCustomer(jrs.getString(13));
			    mdlVendorOrder.setDistrict(jrs.getString(14));
			    mdlVendorOrder.setDepo(jrs.getString(15));
			    mdlVendorOrder.setPort(jrs.getString(16)+" - "+jrs.getString(17));
			    mdlVendorOrder.setAddress(jrs.getString("Address"));
			    mdlVendorOrder.setShippingLine(jrs.getString(22));
			    mdlVendorOrder.setFactory(jrs.getString(23));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderByID", sql , user);
		}
		return mdlVendorOrder;
	}
	
	public static List<model.mdlOrderManagementDetail> LoadAvailableOrder(String OrderType, String StuffingDateFrom, String StuffingDateTo, String VendorID){
		List<model.mdlOrderManagementDetail> listAvailableOrder = new ArrayList<model.mdlOrderManagementDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			//jika tanggal kosong (default), maka tampilin order yang lebih besar sama dengan hari ini
			if(StuffingDateFrom.contentEquals("") || StuffingDateTo.contentEquals("")){
				sql = "{call sp_LoadPickupOrderByDefault(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", OrderType));
				listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
				listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			}
			else{ //jika tidak, maka akan tampilin order sesuai dengan range tanggal yang dipilih
				sql = "{call sp_LoadPickupOrder_v2(?,?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", OrderType));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(StuffingDateFrom, "dd MMM yyyy", "yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(StuffingDateTo, "dd MMM yyyy", "yyyy-MM-dd")));
				listParam.add(QueryExecuteAdapter.QueryParam("string", VendorID));
			}
			
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadAvailableOrder", VendorID);
			while(jrs.next()){
				model.mdlOrderManagementDetail availableOrder = new model.mdlOrderManagementDetail();
				availableOrder.setOrderManagementID(jrs.getString("OrderManagementID"));
				availableOrder.setOrderManagementDetailID(jrs.getString("OrderManagementDetailID"));
				availableOrder.setCustomer(jrs.getString(13));
				availableOrder.setDistrict(jrs.getString(14));
				availableOrder.setDepo(jrs.getString(15));
				availableOrder.setPort(jrs.getString(16)+" - "+jrs.getString(17));
				availableOrder.setStuffingDate(ConvertDateTimeHelper.formatDate(jrs.getString("StuffingDate"), "yyyy-MM-dd", "dd MMMM yyyy"));
				availableOrder.setTierID(jrs.getString("TierID"));
				availableOrder.setTierDescription(jrs.getString(5));

				availableOrder.setContainerTypeID(jrs.getString("ContainerTypeID"));
				availableOrder.setContainerTypeDescription(jrs.getString(7));
				availableOrder.setItemDescription(jrs.getString("ItemDescription"));
				availableOrder.setOrderStatus(jrs.getString("OrderDetailStatus"));
				availableOrder.setQuantity(jrs.getInt(12));

				Double price = jrs.getDouble("Price");
				Integer OrderDetailPrice = price.intValue();
				availableOrder.setPrice( (OrderDetailPrice/jrs.getInt("Quantity"))*jrs.getInt(12) );
				
				availableOrder.setSi(jrs.getString("ShippingInvoiceID"));
				availableOrder.setFactory(jrs.getString(19));

				listAvailableOrder.add(availableOrder);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadAvailableOrder", sql , VendorID);
		}

		return listAvailableOrder;
	}

	public static String InsertVendorOrder(model.mdlVendorOrder lParam)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		Integer customerOrderDetailTotalQty = 0;
		Integer customerOrderDetailPickedQty = 0;
		String result = "";

		try{
			//get the customer order detail quantity
			sql = "{call sp_LoadOrderManagementDetailByDetailID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.orderManagementDetailID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderDetailQuantity", lParam.vendorID);
			//Globals.gCommand = sql;

			while(rowset.next()){
				customerOrderDetailTotalQty = rowset.getInt("Quantity");
			}

			//get the customer order detail quantity that have been picked
			listParam = new ArrayList<model.mdlQueryExecute>();
			rowset = null;
			sql = "{call sp_LoadOrderDetailPickedQty(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.orderManagementDetailID));
			rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderDetailPickedQuantity", lParam.vendorID);
			//Globals.gCommand = sql;

			while(rowset.next()){
				customerOrderDetailPickedQty = rowset.getInt("Quantity");
			}

			//check if customer order detail quantity that have been picked by vendor is still available or not
			Integer inputQty = lParam.getQuantity(); //quantity input
			Integer lastOrderDetailQty = (customerOrderDetailTotalQty - customerOrderDetailPickedQty) - inputQty; //order detail quantity left after picking
			//if still available
			if( lastOrderDetailQty >= 0 )
			{
				Boolean success = false;
				List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
				model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
				
				//store 'InsertVendorOrder' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_InsertVendorOrder(?,?,?,?,?,?,?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", CreateVendorOrderID(lParam.vendorID)));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementDetailID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.getQuantity()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("integer", lParam.getPrice()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
				//Globals.gCommand = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
				
				if(lastOrderDetailQty == 0){
					//store 'UpdateOrderDetailStatusByID' parameter for transaction
					mdlQueryTransaction = new model.mdlQueryTransaction();
					mdlQueryTransaction.sql = "{call sp_UpdateOrderDetailStatusByID(?,?,?,?)}";
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementDetailID()));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 2));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getVendorID()));
					mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString()));
					//Globals.gCommand = mdlQueryTransaction.sql;
					listmdlQueryTransaction.add(mdlQueryTransaction);
					
					
					//check customer order header process
					Integer customerOrderHeaderQty = 0;
					Integer customerOrderHeaderPickedQty = 0;
					
						//get the customer order header quantity
						listParam = new ArrayList<model.mdlQueryExecute>();
						rowset = null;
						sql = "{call sp_LoadOrderManagementByID(?)}";
						listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementID() ));
						rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderHeaderTotalQuantity", lParam.vendorID);
						//Globals.gCommand = sql;
	
						while(rowset.next()){
							customerOrderHeaderQty = rowset.getInt("TotalQuantity");
						}
						
						//get the customer order header quantity that have been picked
						listParam = new ArrayList<model.mdlQueryExecute>();
						rowset = null;
						sql = "{call sp_LoadOrderHeaderPickedQty(?)}";
						listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementID() ));
						rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetOrderHeaderPickedQuantity", lParam.vendorID);
						//Globals.gCommand = sql;
	
						while(rowset.next()){
							customerOrderHeaderPickedQty = rowset.getInt("Quantity");
						}
						
						//check if customer order total quantity have been fully picked by vendor or not
						Integer lastOrderHeaderQty = (customerOrderHeaderQty - customerOrderHeaderPickedQty) - inputQty; //order header quantity left after picking
						//if it has been fully picked
						if(lastOrderHeaderQty == 0){
							//store 'UpdateOrderHeaderStatusByID' parameter for transaction
							mdlQueryTransaction = new model.mdlQueryTransaction();
							mdlQueryTransaction.sql = "{call sp_UpdateOrderStatus(?,?)}";
							mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", lParam.getOrderManagementID() ));
							mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 2));
							//Globals.gCommand = mdlQueryTransaction.sql;
							listmdlQueryTransaction.add(mdlQueryTransaction);
						}
						//if it has not been fully picked, nothing's need to be processed
				}
					
				success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertVendorOrder", lParam.vendorID);
				result = success == true ? "Success Insert Vendor Order" : "Database Error";
			}
			//if not available
			else {
				result = "Your order cannot be processed because the quantity that you input is more than the available quantity. Please reload the page to get the latest quota";
			}
		}
		catch (Exception e) {
	        LogAdapter.InsertLogExc(e.toString(), "InsertVendorOrder", sql , lParam.getVendorID());
			result = "Database error";
		}

		return result;
	}

	public static String CancelVendorOrder(String vendorOrderID, String orderID, String orderDetailID, String user){
		Boolean success = false;
		List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
		model.mdlQueryTransaction mdlQueryTransaction = new model.mdlQueryTransaction();
		String sql = "";
		String result = "";

		try{
			Boolean allowCancel = AllowCancelVendorOrder(vendorOrderID);

			if (allowCancel){
				//store 'CancelOrderManagement' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_CancelVendorOrder(?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
				//Globals.gCommand = mdlQueryTransaction.sql;
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);

				//store 'CancelOrderManagementDetail' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_CancelVendorOrderDetail(?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID));
				//Globals.gCommand = mdlQueryTransaction.sql;
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
				
				//store 'UpdateOrderManagementStatus' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_UpdateOrderStatus(?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderID));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 1));
				//Globals.gCommand = mdlQueryTransaction.sql;
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);
				
				//store 'UpdateOrderManagementDetailStatus' parameter for transaction
				mdlQueryTransaction = new model.mdlQueryTransaction();
				mdlQueryTransaction.sql = "{call sp_UpdateOrderDetailStatusByID(?,?,?,?)}";
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", orderDetailID));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("int", 1));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", user));
				mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
				//Globals.gCommand = mdlQueryTransaction.sql;
				sql = mdlQueryTransaction.sql;
				listmdlQueryTransaction.add(mdlQueryTransaction);

				success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "CancelVendorOrder", user);
				result = success == true ? "Success Cancel Vendor Order" : "Database Error";
			}else{
				result = "Failed Cancel Vendor Order";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CancelVendorOrder", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateVendorOrderID(String user){
		String lastVendorOrderID = getLastVendorOrderID(user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "000001";

		if ((lastVendorOrderID != null) && (!lastVendorOrderID.equals("")) ){
			String[] partsVendorOrderID = lastVendorOrderID.split("-");
			String partDate = partsVendorOrderID[1];
			String partNumber = partsVendorOrderID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%06d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("VOHD-").append(datetime).append("-").append(stringInc);
		String VendorOrderID = sb.toString();
		return VendorOrderID;
	}

	public static String getLastVendorOrderID(String user){
		String sql="";
		CachedRowSet rowset = null;
		String VendorOrderID = "";
		try{
			sql = "{call sp_GetLastVendorOrderID()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "getLastVendorOrderID", user);

			while(rowset.next())
			{
				VendorOrderID = rowset.getString("VendorOrderID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getLastVendorOrderID", sql, user);
		}

		return VendorOrderID;
	}
	public static Boolean AllowCancelVendorOrder(String vendorID){
		Boolean allow = false;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		Integer countStatus = 1;
		try{
			sql = "{call sp_AllowCancelVendorOrder(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "AllowCancelVendorOrder", vendorID);

			while(jrs.next()){
				countStatus = jrs.getInt(1);
			}

			if(countStatus == 0){
				allow = true;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "AllowCancelVendorOrder", sql , vendorID);
		}
		return allow;
	}

	public static String CheckVendorStatus(String vendorOrderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Integer orderStatus = 0;
		String statusString = "";

		try {
			sql = "{call sp_GetVendorStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "CheckVendorStatus", user);

			while(jrs.next()){
				orderStatus = jrs.getInt("VendorStatus");
			}

	        switch (orderStatus) {
	            case 0:  statusString = "Picked";
	                     break;
	            case 1:  statusString = "Assigned";
                		 break;
	            case 2:  statusString = "On Process";
	                     break;
	            case 3:  statusString = "Delivered";
	                     break;
	            case 10:  statusString = "Cancelled";
	                     break;
	            default: statusString = "";
	                     break;
	        }
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "CheckVendorStatus", sql , user);
		}
		return statusString;
	}

	public static Integer GetVendorStatus(String vendorOrderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		Integer orderStatus = 0;

		try {
			sql = "{call sp_GetVendorStatus(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", vendorOrderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetVendorStatus", user);

			while(jrs.next()){
				orderStatus = jrs.getInt("VendorStatus");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetVendorStatus", sql , user);
		}
		return orderStatus;
	}
	
	public static model.mdlVendorOrderDashboard LoadVendorOrderDashboard(String lVendorID, String lStuffingDateFrom, String lStuffingDateTo, String lOrderType) {
		model.mdlVendorOrderDashboard dashboard = new model.mdlVendorOrderDashboard();
		CachedRowSet jrs = null;
		String sql = "";
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();

		try {
			//get available order by vendor tier
			//sql = "{call sp_DashboardVendorOrder(?,?,?)}";
			
			//get available order by vendor tier and vendor mapping
			sql = "{call sp_DashboardVendorOrder_v2(?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lVendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(lStuffingDateFrom, "dd MMM yyyy", "yyyy-MM-dd") ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", ConvertDateTimeHelper.formatDate(lStuffingDateTo, "dd MMM yyyy", "yyyy-MM-dd") ));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lOrderType));
			
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadVendorOrderDashboard", lVendorID);

			while(jrs.next()){
				dashboard.setTotalAvailableOrder(jrs.getInt("totalAvailableOrder"));
				dashboard.setTotalPickedOrder(jrs.getInt("totalPickedOrder"));
				dashboard.setTotalCancelledOrder(jrs.getInt("totalCancelledOrder"));
				dashboard.setTotalProcessedOrder(jrs.getInt("totalProcessedOrder"));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadVendorOrderDashboard", sql , lVendorID);
		}

		return dashboard;
	}
	
}
