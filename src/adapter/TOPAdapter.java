package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlTOP;

public class TOPAdapter {
	public static List<model.mdlTOP> LoadTOP (String user) {
		List<model.mdlTOP> listTOP = new ArrayList<model.mdlTOP>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadTOP()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "LoadTOP", user);

			while(jrs.next()){
				model.mdlTOP top = new model.mdlTOP();
				top.setTopID(jrs.getString("TOPID"));
				top.setName(jrs.getString("Name"));
				top.setDuration(Integer.parseInt(jrs.getString("Duration")));
				listTOP.add(top);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTOP", sql , user);
		}

		return listTOP;
	}

	public static model.mdlTOP LoadTOPByID (String lTOPID, String user) {
		model.mdlTOP mdlTOP = new model.mdlTOP();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";

		try {
			sql = "{call sp_LoadTOPByID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", lTOPID) );

			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadTOPByID", user);

			while(jrs.next()){
				mdlTOP.setTopID(jrs.getString("TOPID"));
				mdlTOP.setName(jrs.getString("Name"));
				mdlTOP.setDuration(Integer.parseInt(jrs.getString("Duration")));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTOPByID", sql , user);
		}

		return mdlTOP;
	}

	public static String InsertTOP(mdlTOP mdlTOP, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			mdlTOP CheckDuplicateTOP = LoadTOPByID(mdlTOP.getTopID(),user);
			if(CheckDuplicateTOP.getTopID() == null){
				String newTOPID = CreateTOPID(user);
				sql = "{call sp_InsertTOP(?,?,?)}";
				listParam.add(QueryExecuteAdapter.QueryParam("string", newTOPID));
				listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTOP.getName()));
				listParam.add(QueryExecuteAdapter.QueryParam("integer", mdlTOP.getDuration()));

				success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertTOP", user);

				result = success == true ? "Success Insert TOP" : "Database Error";
			}
			//if duplicate
			else{
				result = "TOP sudah ada";
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertTOP", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String UpdateTOP(mdlTOP mdlTOP, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateTOP(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTOP.getTopID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlTOP.getName()));
			listParam.add(QueryExecuteAdapter.QueryParam("int", mdlTOP.getDuration()));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateTOP", user);

			result = success == true ? "Success Update TOP" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateTOP", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String DeleteTOP(String id, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteTOP(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", id));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteTOP", user);

			result = success == true ? "Success Delete TOP" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteTOP", sql , user);
			result = "Database Error";
		}

		return result;
	}

	public static String CreateTOPID(String user){
		String lastTOPID = GetLastTOPID(user);
		String stringIncrement = "0001";

		if ((lastTOPID != null) && (!lastTOPID.equals("")) ){
			String[] partsTOPID = lastTOPID.split("-");
			//get last running number
			String partNumber = partsTOPID[1];

			int inc = Integer.parseInt(partNumber) + 1;
			stringIncrement = String.format("%04d", inc);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("TOPY-").append(stringIncrement);
		String TOPID = sb.toString();
		return TOPID;
	}

	public static String GetLastTOPID(String user){
		CachedRowSet jrs = null;
		String sql = "";
		String TOPID = "";

		try {
			sql = "{call sp_GetLastTOPID()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, "GetLastTOPID", user);

			while(jrs.next()){
				TOPID = jrs.getString("TOPID");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastTOPID", sql , user);
		}

		return TOPID;
	}
}
