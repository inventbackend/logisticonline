package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;

import database.QueryExecuteAdapter;
import model.Globals;

public class RoleAdapter {
	
	public static List<model.mdlRole> LoadRole(String user){	
		
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql="";
		CachedRowSet rowset = null;
		List<model.mdlRole> mdlRoleList = new ArrayList<model.mdlRole>();
		try{
			sql = "{call sp_LoadRole()}";
			rowset = QueryExecuteAdapter.QueryExecute(sql, "LoadRole", user);
		
			while(rowset.next())
			{
				model.mdlRole mdlRole = new model.mdlRole();				
				mdlRole.setRoleID(rowset.getString("RoleID"));
				mdlRole.setRoleName(rowset.getString("Name"));

				mdlRoleList.add(mdlRole);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRole", sql, user);
		}
		
		return mdlRoleList;
	}
}
