
package modelKojaGetBillingDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:BILLING_GetBillingDetailResponse")
    @Expose
    private Ns1BILLINGGetBillingDetailResponse ns1BILLINGGetBillingDetailResponse;

    public Ns1BILLINGGetBillingDetailResponse getNs1BILLINGGetBillingDetailResponse() {
        return ns1BILLINGGetBillingDetailResponse;
    }

    public void setNs1BILLINGGetBillingDetailResponse(Ns1BILLINGGetBillingDetailResponse ns1BILLINGGetBillingDetailResponse) {
        this.ns1BILLINGGetBillingDetailResponse = ns1BILLINGGetBillingDetailResponse;
    }

}
