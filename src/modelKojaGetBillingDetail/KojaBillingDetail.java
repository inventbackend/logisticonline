
package modelKojaGetBillingDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KojaBillingDetail {

    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("PROFORMA_INVOICE_NO")
    @Expose
    private String pROFORMAINVOICENO;
    @SerializedName("INVOICE_NO")
    @Expose
    private Object iNVOICENO;
    @SerializedName("MESSAGE")
    @Expose
    private String mESSAGE;
    @SerializedName("DETAIL_BILLING")
    @Expose
    private DETAILBILLING dETAILBILLING;

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getPROFORMAINVOICENO() {
        return pROFORMAINVOICENO;
    }

    public void setPROFORMAINVOICENO(String pROFORMAINVOICENO) {
        this.pROFORMAINVOICENO = pROFORMAINVOICENO;
    }

    public Object getINVOICENO() {
        return iNVOICENO;
    }

    public void setINVOICENO(Object iNVOICENO) {
        this.iNVOICENO = iNVOICENO;
    }

    public String getMESSAGE() {
        return mESSAGE;
    }

    public void setMESSAGE(String mESSAGE) {
        this.mESSAGE = mESSAGE;
    }

    public DETAILBILLING getDETAILBILLING() {
        return dETAILBILLING;
    }

    public void setDETAILBILLING(DETAILBILLING dETAILBILLING) {
        this.dETAILBILLING = dETAILBILLING;
    }

}
