
package modelKojaGetBillingDetail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SUMMARYDETAIL {

    @SerializedName("COMPONENT_GROUP_ID")
    @Expose
    private List<String> cOMPONENTGROUPID = null;
    @SerializedName("COMPONENT_TARIFF_NAME")
    @Expose
    private List<String> cOMPONENTTARIFFNAME = null;
    @SerializedName("EQSZ_ID")
    @Expose
    private List<String> eQSZID = null;
    @SerializedName("EQTP_ID")
    @Expose
    private List<String> eQTPID = null;
    @SerializedName("EQHT_ID")
    @Expose
    private List<String> eQHTID = null;
    @SerializedName("JUMLAH_CONT")
    @Expose
    private List<String> jUMLAHCONT = null;
    @SerializedName("TOTAL_UNIT")
    @Expose
    private List<String> tOTALUNIT = null;
    @SerializedName("CHARGE")
    @Expose
    private List<String> cHARGE = null;
    @SerializedName("TOTAL")
    @Expose
    private List<String> tOTAL = null;
    @SerializedName("SORT")
    @Expose
    private List<String> sORT = null;
    @SerializedName("COMPONENT_OWNER_ID")
    @Expose
    private List<String> cOMPONENTOWNERID = null;

    public List<String> getCOMPONENTGROUPID() {
        return cOMPONENTGROUPID;
    }

    public void setCOMPONENTGROUPID(List<String> cOMPONENTGROUPID) {
        this.cOMPONENTGROUPID = cOMPONENTGROUPID;
    }

    public List<String> getCOMPONENTTARIFFNAME() {
        return cOMPONENTTARIFFNAME;
    }

    public void setCOMPONENTTARIFFNAME(List<String> cOMPONENTTARIFFNAME) {
        this.cOMPONENTTARIFFNAME = cOMPONENTTARIFFNAME;
    }

    public List<String> getEQSZID() {
        return eQSZID;
    }

    public void setEQSZID(List<String> eQSZID) {
        this.eQSZID = eQSZID;
    }

    public List<String> getEQTPID() {
        return eQTPID;
    }

    public void setEQTPID(List<String> eQTPID) {
        this.eQTPID = eQTPID;
    }

    public List<String> getEQHTID() {
        return eQHTID;
    }

    public void setEQHTID(List<String> eQHTID) {
        this.eQHTID = eQHTID;
    }

    public List<String> getJUMLAHCONT() {
        return jUMLAHCONT;
    }

    public void setJUMLAHCONT(List<String> jUMLAHCONT) {
        this.jUMLAHCONT = jUMLAHCONT;
    }

    public List<String> getTOTALUNIT() {
        return tOTALUNIT;
    }

    public void setTOTALUNIT(List<String> tOTALUNIT) {
        this.tOTALUNIT = tOTALUNIT;
    }

    public List<String> getCHARGE() {
        return cHARGE;
    }

    public void setCHARGE(List<String> cHARGE) {
        this.cHARGE = cHARGE;
    }

    public List<String> getTOTAL() {
        return tOTAL;
    }

    public void setTOTAL(List<String> tOTAL) {
        this.tOTAL = tOTAL;
    }

    public List<String> getSORT() {
        return sORT;
    }

    public void setSORT(List<String> sORT) {
        this.sORT = sORT;
    }

    public List<String> getCOMPONENTOWNERID() {
        return cOMPONENTOWNERID;
    }

    public void setCOMPONENTOWNERID(List<String> cOMPONENTOWNERID) {
        this.cOMPONENTOWNERID = cOMPONENTOWNERID;
    }

}
