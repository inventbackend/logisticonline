
package modelKojaGetProforma;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:BILLING_GetProformaResponse")
    @Expose
    private Ns1BILLINGGetProformaResponse ns1BILLINGGetProformaResponse;

    public Ns1BILLINGGetProformaResponse getNs1BILLINGGetProformaResponse() {
        return ns1BILLINGGetProformaResponse;
    }

    public void setNs1BILLINGGetProformaResponse(Ns1BILLINGGetProformaResponse ns1BILLINGGetProformaResponse) {
        this.ns1BILLINGGetProformaResponse = ns1BILLINGGetProformaResponse;
    }

}
