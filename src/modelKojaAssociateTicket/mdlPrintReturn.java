package modelKojaAssociateTicket;

public class mdlPrintReturn {

	public mdlPrintOutHeader PrintOutHeader;
	public mdlPrintList PrintList;
	
	public mdlPrintOutHeader getPrintOutHeader() {
		return PrintOutHeader;
	}
	public void setPrintOutHeader(mdlPrintOutHeader printOutHeader) {
		PrintOutHeader = printOutHeader;
	}
	public mdlPrintList getPrintList() {
		return PrintList;
	}
	public void setPrintList(mdlPrintList printList) {
		PrintList = printList;
	}
}
