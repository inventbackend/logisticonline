package modelKojaAssociateTicket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

	@SerializedName("ns2:eticket_getDataResponse")
	@Expose
	private Ns2EticketGetDataResponse ns2EticketGetDataResponse;

	public Ns2EticketGetDataResponse getNs2EticketGetDataResponse() {
	return ns2EticketGetDataResponse;
	}

	public void setNs2EticketGetDataResponse(Ns2EticketGetDataResponse ns2EticketGetDataResponse) {
	this.ns2EticketGetDataResponse = ns2EticketGetDataResponse;
	}
	
}
