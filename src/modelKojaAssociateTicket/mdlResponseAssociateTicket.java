package modelKojaAssociateTicket;

public class mdlResponseAssociateTicket {

	public String VALIDATION;
	public String USERNAME;
	public mdlDataAssociated DATA_ASSOCIATED;
	
	public String getVALIDATION() {
		return VALIDATION;
	}
	public void setVALIDATION(String vALIDATION) {
		VALIDATION = vALIDATION;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public mdlDataAssociated getDATA_ASSOCIATED() {
		return DATA_ASSOCIATED;
	}
	public void setDATA_ASSOCIATED(mdlDataAssociated dATA_ASSOCIATED) {
		DATA_ASSOCIATED = dATA_ASSOCIATED;
	}
}
