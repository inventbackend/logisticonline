
package modelKojaMainGetCustomData;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DETAILBILLING {

    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("LINK")
    @Expose
    private String lINK;
    @SerializedName("SUBTOTAL")
    @Expose
    private String sUBTOTAL;
    @SerializedName("PPN")
    @Expose
    private String pPN;
    @SerializedName("TOTAL")
    @Expose
    private String tOTAL;
    @SerializedName("PROFORMA_INVOICE_NO")
    @Expose
    private String pROFORMAINVOICENO;
    @SerializedName("EXPIRED")
    @Expose
    private String eXPIRED;
    @SerializedName("CS1_TIMESTAMP")
    @Expose
    private String cS1TIMESTAMP;
    @SerializedName("TRX_ID")
    @Expose
    private String tRXID;
    @SerializedName("NPWP")
    @Expose
    private String nPWP;
    @SerializedName("BAT_CAT_ID")
    @Expose
    private String bATCATID;
    @SerializedName("CUST_ID")
    @Expose
    private String cUSTID;
    @SerializedName("INV_ID")
    @Expose
    private String iNVID;
    @SerializedName("BAT_STATUS_ID")
    @Expose
    private String bATSTATUSID;
    @SerializedName("BAT_NBR")
    @Expose
    private String bATNBR;
    @SerializedName("CUST_NAME")
    @Expose
    private String cUSTNAME;
    @SerializedName("ADDRESS1")
    @Expose
    private String aDDRESS1;
    @SerializedName("ADDRESS2")
    @Expose
    private String aDDRESS2;
    @SerializedName("ADDRESS3")
    @Expose
    private String aDDRESS3;
    @SerializedName("CUST_TYPE_ID")
    @Expose
    private String cUSTTYPEID;
    @SerializedName("SHIP_ID")
    @Expose
    private String sHIPID;
    @SerializedName("VESSEL_NAME")
    @Expose
    private String vESSELNAME;
    @SerializedName("VOYAGE_NO")
    @Expose
    private String vOYAGENO;
    @SerializedName("BL_NBR")
    @Expose
    private String bLNBR;
    @SerializedName("DO_NBR")
    @Expose
    private Object dONBR;
    @SerializedName("BOOKING_NBR")
    @Expose
    private String bOOKINGNBR;
    @SerializedName("BONGKAR_MUAT")
    @Expose
    private String bONGKARMUAT;
    @SerializedName("ATA")
    @Expose
    private String aTA;
    @SerializedName("BANK_REQUEST")
    @Expose
    private String bANKREQUEST;
    @SerializedName("BANK")
    @Expose
    private String bANK;
    @SerializedName("BAYAR_DATETIME")
    @Expose
    private String bAYARDATETIME;
    @SerializedName("DOC_CODE")
    @Expose
    private String dOCCODE;
    @SerializedName("NO_DOCUMENT")
    @Expose
    private String nODOCUMENT;
    @SerializedName("TGL_DOCUMENT")
    @Expose
    private String tGLDOCUMENT;
    @SerializedName("PAID_THRU")
    @Expose
    private Object pAIDTHRU;
    @SerializedName("DO_EXPIRED")
    @Expose
    private String dOEXPIRED;
    @SerializedName("ETD")
    @Expose
    private String eTD;
    @SerializedName("LINE_ID")
    @Expose
    private String lINEID;
    @SerializedName("USERNAME")
    @Expose
    private String uSERNAME;
    @SerializedName("PASSWORD")
    @Expose
    private String pASSWORD;
    @SerializedName("TRX_TYPE_ID")
    @Expose
    private String tRXTYPEID;
    @SerializedName("TRANSACTIONS_TYPE_ID")
    @Expose
    private String tRANSACTIONSTYPEID;
    @SerializedName("TRANSACTIONS_TYPE_PARENT")
    @Expose
    private Object tRANSACTIONSTYPEPARENT;
    @SerializedName("DESTINATION")
    @Expose
    private Object dESTINATION;
    @SerializedName("SHIPPER")
    @Expose
    private String sHIPPER;
    @SerializedName("PM_CODE")
    @Expose
    private String pMCODE;
    @SerializedName("CUSTOMS_DOCUMENT_NBS_ID")
    @Expose
    private String cUSTOMSDOCUMENTNBSID;
    @SerializedName("SOURCE_FROM")
    @Expose
    private String sOURCEFROM;
    @SerializedName("CLOSING_TIME")
    @Expose
    private String cLOSINGTIME;
    @SerializedName("CS1_OFFICER")
    @Expose
    private String cS1OFFICER;
    @SerializedName("DEO_OFFICER")
    @Expose
    private String dEOOFFICER;
    @SerializedName("DEO_TIMESTAMP")
    @Expose
    private String dEOTIMESTAMP;
    @SerializedName("DEO_TIMESTAMP2")
    @Expose
    private String dEOTIMESTAMP2;
    @SerializedName("IA_OFFICER")
    @Expose
    private String iAOFFICER;
    @SerializedName("IA_TIMESTAMP")
    @Expose
    private String iATIMESTAMP;
    @SerializedName("IA_TIMESTAMP2")
    @Expose
    private String iATIMESTAMP2;
    @SerializedName("B1_OFFICER")
    @Expose
    private String b1OFFICER;
    @SerializedName("B2_OFFICER")
    @Expose
    private String b2OFFICER;
    @SerializedName("CS1_STARTTIME")
    @Expose
    private String cS1STARTTIME;
    @SerializedName("DA_OFFICER")
    @Expose
    private Object dAOFFICER;
    @SerializedName("DA_TIMESTAMP")
    @Expose
    private String dATIMESTAMP;
    @SerializedName("DA_TIMESTAMP2")
    @Expose
    private Object dATIMESTAMP2;
    @SerializedName("B1_TIMESTAMP")
    @Expose
    private String b1TIMESTAMP;
    @SerializedName("B2_TIMESTAMP")
    @Expose
    private String b2TIMESTAMP;
    @SerializedName("INVOICE_DATE")
    @Expose
    private String iNVOICEDATE;
    @SerializedName("JML_CONT")
    @Expose
    private Object jMLCONT;
    @SerializedName("E_TICKET_STATUS")
    @Expose
    private Object eTICKETSTATUS;
    @SerializedName("TRX_PHONE")
    @Expose
    private String tRXPHONE;
    @SerializedName("TRX_EMAIL")
    @Expose
    private String tRXEMAIL;
    @SerializedName("TRX_NAME")
    @Expose
    private Object tRXNAME;
    @SerializedName("KTP")
    @Expose
    private Object kTP;
    @SerializedName("TRX_ADDRESS")
    @Expose
    private Object tRXADDRESS;
    @SerializedName("NO_CONT")
    @Expose
    private List<String> nOCONT = null;
    @SerializedName("EQSZ_ID")
    @Expose
    private List<String> eQSZID = null;
    @SerializedName("EQTP_ID")
    @Expose
    private List<String> eQTPID = null;
    @SerializedName("EQHT_ID")
    @Expose
    private List<String> eQHTID = null;
    @SerializedName("ISO_CODE")
    @Expose
    private List<String> iSOCODE = null;
    @SerializedName("OD_FLAG")
    @Expose
    private List<Object> oDFLAG = null;
    @SerializedName("IMO_CODES")
    @Expose
    private List<Object> iMOCODES = null;
    @SerializedName("LS_LOCATION_TYPE")
    @Expose
    private List<Object> lSLOCATIONTYPE = null;
    @SerializedName("LS_LEVEL_1")
    @Expose
    private List<Object> lSLEVEL1 = null;
    @SerializedName("LS_LEVEL_2")
    @Expose
    private List<Object> lSLEVEL2 = null;
    @SerializedName("LS_LEVEL_3")
    @Expose
    private List<Object> lSLEVEL3 = null;
    @SerializedName("LS_LEVEL_4")
    @Expose
    private List<Object> lSLEVEL4 = null;
    @SerializedName("OWNER")
    @Expose
    private List<Object> oWNER = null;
    @SerializedName("REEFER_IN_REFIGERATION_INDR")
    @Expose
    private List<Object> rEEFERINREFIGERATIONINDR = null;
    @SerializedName("FIRST_STACK")
    @Expose
    private List<Object> fIRSTSTACK = null;
    @SerializedName("GATEOUT_KOJA")
    @Expose
    private List<Object> gATEOUTKOJA = null;
    @SerializedName("STATUS_PAID")
    @Expose
    private List<String> sTATUSPAID = null;
    @SerializedName("LAST_TRANSACTION")
    @Expose
    private List<String> lASTTRANSACTION = null;
    @SerializedName("LAST_TRX_ID")
    @Expose
    private List<String> lASTTRXID = null;
    @SerializedName("LAST_PAID_THRU")
    @Expose
    private List<Object> lASTPAIDTHRU = null;
    @SerializedName("LAST_STOP_PLUG")
    @Expose
    private List<Object> lASTSTOPPLUG = null;
    @SerializedName("STATUS_CONT")
    @Expose
    private List<String> sTATUSCONT = null;
    @SerializedName("WEIGHT")
    @Expose
    private List<String> wEIGHT = null;
    @SerializedName("POL")
    @Expose
    private List<String> pOL = null;
    @SerializedName("POD")
    @Expose
    private List<String> pOD = null;
    @SerializedName("FD")
    @Expose
    private List<String> fD = null;
    @SerializedName("START_PLUG")
    @Expose
    private List<Object> sTARTPLUG = null;
    @SerializedName("LOC_ID")
    @Expose
    private List<Object> lOCID = null;
    @SerializedName("VIEW_IMO")
    @Expose
    private List<Object> vIEWIMO = null;
    @SerializedName("VIEW_UN")
    @Expose
    private List<Object> vIEWUN = null;
    @SerializedName("ZXCV0987654")
    @Expose
    private ZXCV0987654 zXCV0987654;
    @SerializedName("TARIFF_TRANSACTIONS")
    @Expose
    private TARIFFTRANSACTIONS tARIFFTRANSACTIONS;
    @SerializedName("SUMMARY_DETAIL")
    @Expose
    private SUMMARYDETAIL sUMMARYDETAIL;
    @SerializedName("SUMMARY_TRANSACTIONS")
    @Expose
    private SUMMARYTRANSACTIONS sUMMARYTRANSACTIONS;
    @SerializedName("FOOTER")
    @Expose
    private String fOOTER;

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getLINK() {
        return lINK;
    }

    public void setLINK(String lINK) {
        this.lINK = lINK;
    }

    public String getSUBTOTAL() {
        return sUBTOTAL;
    }

    public void setSUBTOTAL(String sUBTOTAL) {
        this.sUBTOTAL = sUBTOTAL;
    }

    public String getPPN() {
        return pPN;
    }

    public void setPPN(String pPN) {
        this.pPN = pPN;
    }

    public String getTOTAL() {
        return tOTAL;
    }

    public void setTOTAL(String tOTAL) {
        this.tOTAL = tOTAL;
    }

    public String getPROFORMAINVOICENO() {
        return pROFORMAINVOICENO;
    }

    public void setPROFORMAINVOICENO(String pROFORMAINVOICENO) {
        this.pROFORMAINVOICENO = pROFORMAINVOICENO;
    }

    public String getEXPIRED() {
        return eXPIRED;
    }

    public void setEXPIRED(String eXPIRED) {
        this.eXPIRED = eXPIRED;
    }

    public String getCS1TIMESTAMP() {
        return cS1TIMESTAMP;
    }

    public void setCS1TIMESTAMP(String cS1TIMESTAMP) {
        this.cS1TIMESTAMP = cS1TIMESTAMP;
    }

    public String getTRXID() {
        return tRXID;
    }

    public void setTRXID(String tRXID) {
        this.tRXID = tRXID;
    }

    public String getNPWP() {
        return nPWP;
    }

    public void setNPWP(String nPWP) {
        this.nPWP = nPWP;
    }

    public String getBATCATID() {
        return bATCATID;
    }

    public void setBATCATID(String bATCATID) {
        this.bATCATID = bATCATID;
    }

    public String getCUSTID() {
        return cUSTID;
    }

    public void setCUSTID(String cUSTID) {
        this.cUSTID = cUSTID;
    }

    public String getINVID() {
        return iNVID;
    }

    public void setINVID(String iNVID) {
        this.iNVID = iNVID;
    }

    public String getBATSTATUSID() {
        return bATSTATUSID;
    }

    public void setBATSTATUSID(String bATSTATUSID) {
        this.bATSTATUSID = bATSTATUSID;
    }

    public String getBATNBR() {
        return bATNBR;
    }

    public void setBATNBR(String bATNBR) {
        this.bATNBR = bATNBR;
    }

    public String getCUSTNAME() {
        return cUSTNAME;
    }

    public void setCUSTNAME(String cUSTNAME) {
        this.cUSTNAME = cUSTNAME;
    }

    public String getADDRESS1() {
        return aDDRESS1;
    }

    public void setADDRESS1(String aDDRESS1) {
        this.aDDRESS1 = aDDRESS1;
    }

    public String getADDRESS2() {
        return aDDRESS2;
    }

    public void setADDRESS2(String aDDRESS2) {
        this.aDDRESS2 = aDDRESS2;
    }

    public String getADDRESS3() {
        return aDDRESS3;
    }

    public void setADDRESS3(String aDDRESS3) {
        this.aDDRESS3 = aDDRESS3;
    }

    public String getCUSTTYPEID() {
        return cUSTTYPEID;
    }

    public void setCUSTTYPEID(String cUSTTYPEID) {
        this.cUSTTYPEID = cUSTTYPEID;
    }

    public String getSHIPID() {
        return sHIPID;
    }

    public void setSHIPID(String sHIPID) {
        this.sHIPID = sHIPID;
    }

    public String getVESSELNAME() {
        return vESSELNAME;
    }

    public void setVESSELNAME(String vESSELNAME) {
        this.vESSELNAME = vESSELNAME;
    }

    public String getVOYAGENO() {
        return vOYAGENO;
    }

    public void setVOYAGENO(String vOYAGENO) {
        this.vOYAGENO = vOYAGENO;
    }

    public String getBLNBR() {
        return bLNBR;
    }

    public void setBLNBR(String bLNBR) {
        this.bLNBR = bLNBR;
    }

    public Object getDONBR() {
        return dONBR;
    }

    public void setDONBR(Object dONBR) {
        this.dONBR = dONBR;
    }

    public String getBOOKINGNBR() {
        return bOOKINGNBR;
    }

    public void setBOOKINGNBR(String bOOKINGNBR) {
        this.bOOKINGNBR = bOOKINGNBR;
    }

    public String getBONGKARMUAT() {
        return bONGKARMUAT;
    }

    public void setBONGKARMUAT(String bONGKARMUAT) {
        this.bONGKARMUAT = bONGKARMUAT;
    }

    public String getATA() {
        return aTA;
    }

    public void setATA(String aTA) {
        this.aTA = aTA;
    }

    public String getBANKREQUEST() {
        return bANKREQUEST;
    }

    public void setBANKREQUEST(String bANKREQUEST) {
        this.bANKREQUEST = bANKREQUEST;
    }

    public String getBANK() {
        return bANK;
    }

    public void setBANK(String bANK) {
        this.bANK = bANK;
    }

    public String getBAYARDATETIME() {
        return bAYARDATETIME;
    }

    public void setBAYARDATETIME(String bAYARDATETIME) {
        this.bAYARDATETIME = bAYARDATETIME;
    }

    public String getDOCCODE() {
        return dOCCODE;
    }

    public void setDOCCODE(String dOCCODE) {
        this.dOCCODE = dOCCODE;
    }

    public String getNODOCUMENT() {
        return nODOCUMENT;
    }

    public void setNODOCUMENT(String nODOCUMENT) {
        this.nODOCUMENT = nODOCUMENT;
    }

    public String getTGLDOCUMENT() {
        return tGLDOCUMENT;
    }

    public void setTGLDOCUMENT(String tGLDOCUMENT) {
        this.tGLDOCUMENT = tGLDOCUMENT;
    }

    public Object getPAIDTHRU() {
        return pAIDTHRU;
    }

    public void setPAIDTHRU(Object pAIDTHRU) {
        this.pAIDTHRU = pAIDTHRU;
    }

    public String getDOEXPIRED() {
        return dOEXPIRED;
    }

    public void setDOEXPIRED(String dOEXPIRED) {
        this.dOEXPIRED = dOEXPIRED;
    }

    public String getETD() {
        return eTD;
    }

    public void setETD(String eTD) {
        this.eTD = eTD;
    }

    public String getLINEID() {
        return lINEID;
    }

    public void setLINEID(String lINEID) {
        this.lINEID = lINEID;
    }

    public String getUSERNAME() {
        return uSERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    public String getPASSWORD() {
        return pASSWORD;
    }

    public void setPASSWORD(String pASSWORD) {
        this.pASSWORD = pASSWORD;
    }

    public String getTRXTYPEID() {
        return tRXTYPEID;
    }

    public void setTRXTYPEID(String tRXTYPEID) {
        this.tRXTYPEID = tRXTYPEID;
    }

    public String getTRANSACTIONSTYPEID() {
        return tRANSACTIONSTYPEID;
    }

    public void setTRANSACTIONSTYPEID(String tRANSACTIONSTYPEID) {
        this.tRANSACTIONSTYPEID = tRANSACTIONSTYPEID;
    }

    public Object getTRANSACTIONSTYPEPARENT() {
        return tRANSACTIONSTYPEPARENT;
    }

    public void setTRANSACTIONSTYPEPARENT(Object tRANSACTIONSTYPEPARENT) {
        this.tRANSACTIONSTYPEPARENT = tRANSACTIONSTYPEPARENT;
    }

    public Object getDESTINATION() {
        return dESTINATION;
    }

    public void setDESTINATION(Object dESTINATION) {
        this.dESTINATION = dESTINATION;
    }

    public String getSHIPPER() {
        return sHIPPER;
    }

    public void setSHIPPER(String sHIPPER) {
        this.sHIPPER = sHIPPER;
    }

    public String getPMCODE() {
        return pMCODE;
    }

    public void setPMCODE(String pMCODE) {
        this.pMCODE = pMCODE;
    }

    public String getCUSTOMSDOCUMENTNBSID() {
        return cUSTOMSDOCUMENTNBSID;
    }

    public void setCUSTOMSDOCUMENTNBSID(String cUSTOMSDOCUMENTNBSID) {
        this.cUSTOMSDOCUMENTNBSID = cUSTOMSDOCUMENTNBSID;
    }

    public String getSOURCEFROM() {
        return sOURCEFROM;
    }

    public void setSOURCEFROM(String sOURCEFROM) {
        this.sOURCEFROM = sOURCEFROM;
    }

    public String getCLOSINGTIME() {
        return cLOSINGTIME;
    }

    public void setCLOSINGTIME(String cLOSINGTIME) {
        this.cLOSINGTIME = cLOSINGTIME;
    }

    public String getCS1OFFICER() {
        return cS1OFFICER;
    }

    public void setCS1OFFICER(String cS1OFFICER) {
        this.cS1OFFICER = cS1OFFICER;
    }

    public String getDEOOFFICER() {
        return dEOOFFICER;
    }

    public void setDEOOFFICER(String dEOOFFICER) {
        this.dEOOFFICER = dEOOFFICER;
    }

    public String getDEOTIMESTAMP() {
        return dEOTIMESTAMP;
    }

    public void setDEOTIMESTAMP(String dEOTIMESTAMP) {
        this.dEOTIMESTAMP = dEOTIMESTAMP;
    }

    public String getDEOTIMESTAMP2() {
        return dEOTIMESTAMP2;
    }

    public void setDEOTIMESTAMP2(String dEOTIMESTAMP2) {
        this.dEOTIMESTAMP2 = dEOTIMESTAMP2;
    }

    public String getIAOFFICER() {
        return iAOFFICER;
    }

    public void setIAOFFICER(String iAOFFICER) {
        this.iAOFFICER = iAOFFICER;
    }

    public String getIATIMESTAMP() {
        return iATIMESTAMP;
    }

    public void setIATIMESTAMP(String iATIMESTAMP) {
        this.iATIMESTAMP = iATIMESTAMP;
    }

    public String getIATIMESTAMP2() {
        return iATIMESTAMP2;
    }

    public void setIATIMESTAMP2(String iATIMESTAMP2) {
        this.iATIMESTAMP2 = iATIMESTAMP2;
    }

    public String getB1OFFICER() {
        return b1OFFICER;
    }

    public void setB1OFFICER(String b1OFFICER) {
        this.b1OFFICER = b1OFFICER;
    }

    public String getB2OFFICER() {
        return b2OFFICER;
    }

    public void setB2OFFICER(String b2OFFICER) {
        this.b2OFFICER = b2OFFICER;
    }

    public String getCS1STARTTIME() {
        return cS1STARTTIME;
    }

    public void setCS1STARTTIME(String cS1STARTTIME) {
        this.cS1STARTTIME = cS1STARTTIME;
    }

    public Object getDAOFFICER() {
        return dAOFFICER;
    }

    public void setDAOFFICER(Object dAOFFICER) {
        this.dAOFFICER = dAOFFICER;
    }

    public String getDATIMESTAMP() {
        return dATIMESTAMP;
    }

    public void setDATIMESTAMP(String dATIMESTAMP) {
        this.dATIMESTAMP = dATIMESTAMP;
    }

    public Object getDATIMESTAMP2() {
        return dATIMESTAMP2;
    }

    public void setDATIMESTAMP2(Object dATIMESTAMP2) {
        this.dATIMESTAMP2 = dATIMESTAMP2;
    }

    public String getB1TIMESTAMP() {
        return b1TIMESTAMP;
    }

    public void setB1TIMESTAMP(String b1TIMESTAMP) {
        this.b1TIMESTAMP = b1TIMESTAMP;
    }

    public String getB2TIMESTAMP() {
        return b2TIMESTAMP;
    }

    public void setB2TIMESTAMP(String b2TIMESTAMP) {
        this.b2TIMESTAMP = b2TIMESTAMP;
    }

    public String getINVOICEDATE() {
        return iNVOICEDATE;
    }

    public void setINVOICEDATE(String iNVOICEDATE) {
        this.iNVOICEDATE = iNVOICEDATE;
    }

    public Object getJMLCONT() {
        return jMLCONT;
    }

    public void setJMLCONT(Object jMLCONT) {
        this.jMLCONT = jMLCONT;
    }

    public Object getETICKETSTATUS() {
        return eTICKETSTATUS;
    }

    public void setETICKETSTATUS(Object eTICKETSTATUS) {
        this.eTICKETSTATUS = eTICKETSTATUS;
    }

    public String getTRXPHONE() {
        return tRXPHONE;
    }

    public void setTRXPHONE(String tRXPHONE) {
        this.tRXPHONE = tRXPHONE;
    }

    public String getTRXEMAIL() {
        return tRXEMAIL;
    }

    public void setTRXEMAIL(String tRXEMAIL) {
        this.tRXEMAIL = tRXEMAIL;
    }

    public Object getTRXNAME() {
        return tRXNAME;
    }

    public void setTRXNAME(Object tRXNAME) {
        this.tRXNAME = tRXNAME;
    }

    public Object getKTP() {
        return kTP;
    }

    public void setKTP(Object kTP) {
        this.kTP = kTP;
    }

    public Object getTRXADDRESS() {
        return tRXADDRESS;
    }

    public void setTRXADDRESS(Object tRXADDRESS) {
        this.tRXADDRESS = tRXADDRESS;
    }

    public List<String> getNOCONT() {
        return nOCONT;
    }

    public void setNOCONT(List<String> nOCONT) {
        this.nOCONT = nOCONT;
    }

    public List<String> getEQSZID() {
        return eQSZID;
    }

    public void setEQSZID(List<String> eQSZID) {
        this.eQSZID = eQSZID;
    }

    public List<String> getEQTPID() {
        return eQTPID;
    }

    public void setEQTPID(List<String> eQTPID) {
        this.eQTPID = eQTPID;
    }

    public List<String> getEQHTID() {
        return eQHTID;
    }

    public void setEQHTID(List<String> eQHTID) {
        this.eQHTID = eQHTID;
    }

    public List<String> getISOCODE() {
        return iSOCODE;
    }

    public void setISOCODE(List<String> iSOCODE) {
        this.iSOCODE = iSOCODE;
    }

    public List<Object> getODFLAG() {
        return oDFLAG;
    }

    public void setODFLAG(List<Object> oDFLAG) {
        this.oDFLAG = oDFLAG;
    }

    public List<Object> getIMOCODES() {
        return iMOCODES;
    }

    public void setIMOCODES(List<Object> iMOCODES) {
        this.iMOCODES = iMOCODES;
    }

    public List<Object> getLSLOCATIONTYPE() {
        return lSLOCATIONTYPE;
    }

    public void setLSLOCATIONTYPE(List<Object> lSLOCATIONTYPE) {
        this.lSLOCATIONTYPE = lSLOCATIONTYPE;
    }

    public List<Object> getLSLEVEL1() {
        return lSLEVEL1;
    }

    public void setLSLEVEL1(List<Object> lSLEVEL1) {
        this.lSLEVEL1 = lSLEVEL1;
    }

    public List<Object> getLSLEVEL2() {
        return lSLEVEL2;
    }

    public void setLSLEVEL2(List<Object> lSLEVEL2) {
        this.lSLEVEL2 = lSLEVEL2;
    }

    public List<Object> getLSLEVEL3() {
        return lSLEVEL3;
    }

    public void setLSLEVEL3(List<Object> lSLEVEL3) {
        this.lSLEVEL3 = lSLEVEL3;
    }

    public List<Object> getLSLEVEL4() {
        return lSLEVEL4;
    }

    public void setLSLEVEL4(List<Object> lSLEVEL4) {
        this.lSLEVEL4 = lSLEVEL4;
    }

    public List<Object> getOWNER() {
        return oWNER;
    }

    public void setOWNER(List<Object> oWNER) {
        this.oWNER = oWNER;
    }

    public List<Object> getREEFERINREFIGERATIONINDR() {
        return rEEFERINREFIGERATIONINDR;
    }

    public void setREEFERINREFIGERATIONINDR(List<Object> rEEFERINREFIGERATIONINDR) {
        this.rEEFERINREFIGERATIONINDR = rEEFERINREFIGERATIONINDR;
    }

    public List<Object> getFIRSTSTACK() {
        return fIRSTSTACK;
    }

    public void setFIRSTSTACK(List<Object> fIRSTSTACK) {
        this.fIRSTSTACK = fIRSTSTACK;
    }

    public List<Object> getGATEOUTKOJA() {
        return gATEOUTKOJA;
    }

    public void setGATEOUTKOJA(List<Object> gATEOUTKOJA) {
        this.gATEOUTKOJA = gATEOUTKOJA;
    }

    public List<String> getSTATUSPAID() {
        return sTATUSPAID;
    }

    public void setSTATUSPAID(List<String> sTATUSPAID) {
        this.sTATUSPAID = sTATUSPAID;
    }

    public List<String> getLASTTRANSACTION() {
        return lASTTRANSACTION;
    }

    public void setLASTTRANSACTION(List<String> lASTTRANSACTION) {
        this.lASTTRANSACTION = lASTTRANSACTION;
    }

    public List<String> getLASTTRXID() {
        return lASTTRXID;
    }

    public void setLASTTRXID(List<String> lASTTRXID) {
        this.lASTTRXID = lASTTRXID;
    }

    public List<Object> getLASTPAIDTHRU() {
        return lASTPAIDTHRU;
    }

    public void setLASTPAIDTHRU(List<Object> lASTPAIDTHRU) {
        this.lASTPAIDTHRU = lASTPAIDTHRU;
    }

    public List<Object> getLASTSTOPPLUG() {
        return lASTSTOPPLUG;
    }

    public void setLASTSTOPPLUG(List<Object> lASTSTOPPLUG) {
        this.lASTSTOPPLUG = lASTSTOPPLUG;
    }

    public List<String> getSTATUSCONT() {
        return sTATUSCONT;
    }

    public void setSTATUSCONT(List<String> sTATUSCONT) {
        this.sTATUSCONT = sTATUSCONT;
    }

    public List<String> getWEIGHT() {
        return wEIGHT;
    }

    public void setWEIGHT(List<String> wEIGHT) {
        this.wEIGHT = wEIGHT;
    }

    public List<String> getPOL() {
        return pOL;
    }

    public void setPOL(List<String> pOL) {
        this.pOL = pOL;
    }

    public List<String> getPOD() {
        return pOD;
    }

    public void setPOD(List<String> pOD) {
        this.pOD = pOD;
    }

    public List<String> getFD() {
        return fD;
    }

    public void setFD(List<String> fD) {
        this.fD = fD;
    }

    public List<Object> getSTARTPLUG() {
        return sTARTPLUG;
    }

    public void setSTARTPLUG(List<Object> sTARTPLUG) {
        this.sTARTPLUG = sTARTPLUG;
    }

    public List<Object> getLOCID() {
        return lOCID;
    }

    public void setLOCID(List<Object> lOCID) {
        this.lOCID = lOCID;
    }

    public List<Object> getVIEWIMO() {
        return vIEWIMO;
    }

    public void setVIEWIMO(List<Object> vIEWIMO) {
        this.vIEWIMO = vIEWIMO;
    }

    public List<Object> getVIEWUN() {
        return vIEWUN;
    }

    public void setVIEWUN(List<Object> vIEWUN) {
        this.vIEWUN = vIEWUN;
    }

    public ZXCV0987654 getZXCV0987654() {
        return zXCV0987654;
    }

    public void setZXCV0987654(ZXCV0987654 zXCV0987654) {
        this.zXCV0987654 = zXCV0987654;
    }

    public TARIFFTRANSACTIONS getTARIFFTRANSACTIONS() {
        return tARIFFTRANSACTIONS;
    }

    public void setTARIFFTRANSACTIONS(TARIFFTRANSACTIONS tARIFFTRANSACTIONS) {
        this.tARIFFTRANSACTIONS = tARIFFTRANSACTIONS;
    }

    public SUMMARYDETAIL getSUMMARYDETAIL() {
        return sUMMARYDETAIL;
    }

    public void setSUMMARYDETAIL(SUMMARYDETAIL sUMMARYDETAIL) {
        this.sUMMARYDETAIL = sUMMARYDETAIL;
    }

    public SUMMARYTRANSACTIONS getSUMMARYTRANSACTIONS() {
        return sUMMARYTRANSACTIONS;
    }

    public void setSUMMARYTRANSACTIONS(SUMMARYTRANSACTIONS sUMMARYTRANSACTIONS) {
        this.sUMMARYTRANSACTIONS = sUMMARYTRANSACTIONS;
    }

    public String getFOOTER() {
        return fOOTER;
    }

    public void setFOOTER(String fOOTER) {
        this.fOOTER = fOOTER;
    }

}
