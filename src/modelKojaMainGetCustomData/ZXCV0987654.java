
package modelKojaMainGetCustomData;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ZXCV0987654 {

    @SerializedName("COMPONENT_GROUP_ID")
    @Expose
    private List<String> cOMPONENTGROUPID = null;
    @SerializedName("COMPONENT_TARIFF_NAME")
    @Expose
    private List<String> cOMPONENTTARIFFNAME = null;
    @SerializedName("TOTAL_UNIT")
    @Expose
    private List<String> tOTALUNIT = null;
    @SerializedName("CHARGE")
    @Expose
    private List<String> cHARGE = null;
    @SerializedName("PROSEN_M1")
    @Expose
    private List<String> pROSENM1 = null;
    @SerializedName("SELISIH_M1")
    @Expose
    private List<String> sELISIHM1 = null;
    @SerializedName("M1_START_DATE")
    @Expose
    private List<String> m1STARTDATE = null;
    @SerializedName("M1_END_DATE")
    @Expose
    private List<String> m1ENDDATE = null;
    @SerializedName("TOTAL_M1")
    @Expose
    private List<String> tOTALM1 = null;
    @SerializedName("PROSEN_M2")
    @Expose
    private List<String> pROSENM2 = null;
    @SerializedName("SELISIH_M2")
    @Expose
    private List<String> sELISIHM2 = null;
    @SerializedName("M2_START_DATE")
    @Expose
    private List<String> m2STARTDATE = null;
    @SerializedName("M2_END_DATE")
    @Expose
    private List<String> m2ENDDATE = null;
    @SerializedName("TOTAL_M2")
    @Expose
    private List<String> tOTALM2 = null;
    @SerializedName("PROSEN_M3")
    @Expose
    private List<String> pROSENM3 = null;
    @SerializedName("SELISIH_M3")
    @Expose
    private List<String> sELISIHM3 = null;
    @SerializedName("M3_START_DATE")
    @Expose
    private List<String> m3STARTDATE = null;
    @SerializedName("M3_END_DATE")
    @Expose
    private List<Object> m3ENDDATE = null;
    @SerializedName("TOTAL_M3")
    @Expose
    private List<String> tOTALM3 = null;
    @SerializedName("PROSEN_M4")
    @Expose
    private List<Object> pROSENM4 = null;
    @SerializedName("SELISIH_M4")
    @Expose
    private List<Object> sELISIHM4 = null;
    @SerializedName("M4_START_DATE")
    @Expose
    private List<Object> m4STARTDATE = null;
    @SerializedName("M4_END_DATE")
    @Expose
    private List<Object> m4ENDDATE = null;
    @SerializedName("TOTAL_M4")
    @Expose
    private List<Object> tOTALM4 = null;
    @SerializedName("TOTAL")
    @Expose
    private List<String> tOTAL = null;
    @SerializedName("REMARKS")
    @Expose
    private List<Object> rEMARKS = null;
    @SerializedName("COMPONENT_GROUP_INVOICE_ID")
    @Expose
    private List<String> cOMPONENTGROUPINVOICEID = null;
    @SerializedName("SORT")
    @Expose
    private List<String> sORT = null;
    @SerializedName("COMPONENT_OWNER_ID")
    @Expose
    private List<String> cOMPONENTOWNERID = null;

    public List<String> getCOMPONENTGROUPID() {
        return cOMPONENTGROUPID;
    }

    public void setCOMPONENTGROUPID(List<String> cOMPONENTGROUPID) {
        this.cOMPONENTGROUPID = cOMPONENTGROUPID;
    }

    public List<String> getCOMPONENTTARIFFNAME() {
        return cOMPONENTTARIFFNAME;
    }

    public void setCOMPONENTTARIFFNAME(List<String> cOMPONENTTARIFFNAME) {
        this.cOMPONENTTARIFFNAME = cOMPONENTTARIFFNAME;
    }

    public List<String> getTOTALUNIT() {
        return tOTALUNIT;
    }

    public void setTOTALUNIT(List<String> tOTALUNIT) {
        this.tOTALUNIT = tOTALUNIT;
    }

    public List<String> getCHARGE() {
        return cHARGE;
    }

    public void setCHARGE(List<String> cHARGE) {
        this.cHARGE = cHARGE;
    }

    public List<String> getPROSENM1() {
        return pROSENM1;
    }

    public void setPROSENM1(List<String> pROSENM1) {
        this.pROSENM1 = pROSENM1;
    }

    public List<String> getSELISIHM1() {
        return sELISIHM1;
    }

    public void setSELISIHM1(List<String> sELISIHM1) {
        this.sELISIHM1 = sELISIHM1;
    }

    public List<String> getM1STARTDATE() {
        return m1STARTDATE;
    }

    public void setM1STARTDATE(List<String> m1STARTDATE) {
        this.m1STARTDATE = m1STARTDATE;
    }

    public List<String> getM1ENDDATE() {
        return m1ENDDATE;
    }

    public void setM1ENDDATE(List<String> m1ENDDATE) {
        this.m1ENDDATE = m1ENDDATE;
    }

    public List<String> getTOTALM1() {
        return tOTALM1;
    }

    public void setTOTALM1(List<String> tOTALM1) {
        this.tOTALM1 = tOTALM1;
    }

    public List<String> getPROSENM2() {
        return pROSENM2;
    }

    public void setPROSENM2(List<String> pROSENM2) {
        this.pROSENM2 = pROSENM2;
    }

    public List<String> getSELISIHM2() {
        return sELISIHM2;
    }

    public void setSELISIHM2(List<String> sELISIHM2) {
        this.sELISIHM2 = sELISIHM2;
    }

    public List<String> getM2STARTDATE() {
        return m2STARTDATE;
    }

    public void setM2STARTDATE(List<String> m2STARTDATE) {
        this.m2STARTDATE = m2STARTDATE;
    }

    public List<String> getM2ENDDATE() {
        return m2ENDDATE;
    }

    public void setM2ENDDATE(List<String> m2ENDDATE) {
        this.m2ENDDATE = m2ENDDATE;
    }

    public List<String> getTOTALM2() {
        return tOTALM2;
    }

    public void setTOTALM2(List<String> tOTALM2) {
        this.tOTALM2 = tOTALM2;
    }

    public List<String> getPROSENM3() {
        return pROSENM3;
    }

    public void setPROSENM3(List<String> pROSENM3) {
        this.pROSENM3 = pROSENM3;
    }

    public List<String> getSELISIHM3() {
        return sELISIHM3;
    }

    public void setSELISIHM3(List<String> sELISIHM3) {
        this.sELISIHM3 = sELISIHM3;
    }

    public List<String> getM3STARTDATE() {
        return m3STARTDATE;
    }

    public void setM3STARTDATE(List<String> m3STARTDATE) {
        this.m3STARTDATE = m3STARTDATE;
    }

    public List<Object> getM3ENDDATE() {
        return m3ENDDATE;
    }

    public void setM3ENDDATE(List<Object> m3ENDDATE) {
        this.m3ENDDATE = m3ENDDATE;
    }

    public List<String> getTOTALM3() {
        return tOTALM3;
    }

    public void setTOTALM3(List<String> tOTALM3) {
        this.tOTALM3 = tOTALM3;
    }

    public List<Object> getPROSENM4() {
        return pROSENM4;
    }

    public void setPROSENM4(List<Object> pROSENM4) {
        this.pROSENM4 = pROSENM4;
    }

    public List<Object> getSELISIHM4() {
        return sELISIHM4;
    }

    public void setSELISIHM4(List<Object> sELISIHM4) {
        this.sELISIHM4 = sELISIHM4;
    }

    public List<Object> getM4STARTDATE() {
        return m4STARTDATE;
    }

    public void setM4STARTDATE(List<Object> m4STARTDATE) {
        this.m4STARTDATE = m4STARTDATE;
    }

    public List<Object> getM4ENDDATE() {
        return m4ENDDATE;
    }

    public void setM4ENDDATE(List<Object> m4ENDDATE) {
        this.m4ENDDATE = m4ENDDATE;
    }

    public List<Object> getTOTALM4() {
        return tOTALM4;
    }

    public void setTOTALM4(List<Object> tOTALM4) {
        this.tOTALM4 = tOTALM4;
    }

    public List<String> getTOTAL() {
        return tOTAL;
    }

    public void setTOTAL(List<String> tOTAL) {
        this.tOTAL = tOTAL;
    }

    public List<Object> getREMARKS() {
        return rEMARKS;
    }

    public void setREMARKS(List<Object> rEMARKS) {
        this.rEMARKS = rEMARKS;
    }

    public List<String> getCOMPONENTGROUPINVOICEID() {
        return cOMPONENTGROUPINVOICEID;
    }

    public void setCOMPONENTGROUPINVOICEID(List<String> cOMPONENTGROUPINVOICEID) {
        this.cOMPONENTGROUPINVOICEID = cOMPONENTGROUPINVOICEID;
    }

    public List<String> getSORT() {
        return sORT;
    }

    public void setSORT(List<String> sORT) {
        this.sORT = sORT;
    }

    public List<String> getCOMPONENTOWNERID() {
        return cOMPONENTOWNERID;
    }

    public void setCOMPONENTOWNERID(List<String> cOMPONENTOWNERID) {
        this.cOMPONENTOWNERID = cOMPONENTOWNERID;
    }

}
