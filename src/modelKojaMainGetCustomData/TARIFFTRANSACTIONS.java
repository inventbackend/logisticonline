
package modelKojaMainGetCustomData;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TARIFFTRANSACTIONS {

    @SerializedName("COMPONENT_GROUP_ID")
    @Expose
    private List<String> cOMPONENTGROUPID = null;
    @SerializedName("COMPONENT_TARIFF_NAME")
    @Expose
    private List<String> cOMPONENTTARIFFNAME = null;
    @SerializedName("TOTAL_UNIT")
    @Expose
    private List<String> tOTALUNIT = null;
    @SerializedName("CHARGE")
    @Expose
    private List<String> cHARGE = null;
    @SerializedName("TOTAL")
    @Expose
    private List<String> tOTAL = null;
    @SerializedName("COMPONENT_OWNER_ID")
    @Expose
    private List<String> cOMPONENTOWNERID = null;

    public List<String> getCOMPONENTGROUPID() {
        return cOMPONENTGROUPID;
    }

    public void setCOMPONENTGROUPID(List<String> cOMPONENTGROUPID) {
        this.cOMPONENTGROUPID = cOMPONENTGROUPID;
    }

    public List<String> getCOMPONENTTARIFFNAME() {
        return cOMPONENTTARIFFNAME;
    }

    public void setCOMPONENTTARIFFNAME(List<String> cOMPONENTTARIFFNAME) {
        this.cOMPONENTTARIFFNAME = cOMPONENTTARIFFNAME;
    }

    public List<String> getTOTALUNIT() {
        return tOTALUNIT;
    }

    public void setTOTALUNIT(List<String> tOTALUNIT) {
        this.tOTALUNIT = tOTALUNIT;
    }

    public List<String> getCHARGE() {
        return cHARGE;
    }

    public void setCHARGE(List<String> cHARGE) {
        this.cHARGE = cHARGE;
    }

    public List<String> getTOTAL() {
        return tOTAL;
    }

    public void setTOTAL(List<String> tOTAL) {
        this.tOTAL = tOTAL;
    }

    public List<String> getCOMPONENTOWNERID() {
        return cOMPONENTOWNERID;
    }

    public void setCOMPONENTOWNERID(List<String> cOMPONENTOWNERID) {
        this.cOMPONENTOWNERID = cOMPONENTOWNERID;
    }

}
