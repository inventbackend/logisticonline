package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import com.sun.rowset.CachedRowSetImpl;

import adapter.LogAdapter;
import model.Globals;

public class QueryExecuteAdapter {

	public static CachedRowSet QueryExecute(String sql, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		CachedRowSet rs = null;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			jrs = pstm.executeQuery();

			rs = new CachedRowSetImpl();
			rs.populate(jrs);

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return rs;
	}

	public static CachedRowSet QueryExecute(String sql, List<model.mdlQueryExecute> queryParam, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		CachedRowSet rs = null;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			if (queryParam.size() >= 0){
				for (int i=1;i<=queryParam.size();i++){
					int arrayCounter = i-1;
					String type = queryParam.get(arrayCounter).paramType;
					if (type.equalsIgnoreCase("string")){
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}else if (type.equalsIgnoreCase("int")){
						int value = (int) queryParam.get(arrayCounter).paramValue;
						pstm.setInt(i, value);
					}else if (type.equalsIgnoreCase("decimal") || type.equalsIgnoreCase("double") ){
						Double value = (Double) queryParam.get(arrayCounter).paramValue;
						pstm.setDouble(i, value);
					}else if (type.equalsIgnoreCase("boolean")){
						Boolean value = (Boolean) queryParam.get(arrayCounter).paramValue;
						pstm.setBoolean(i, value);
					}else{
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}
				}
			}

			jrs = pstm.executeQuery();

			rs = new CachedRowSetImpl();
			rs.populate(jrs);

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return rs;
	}

	public static Boolean QueryManipulate(String sql, List<model.mdlQueryExecute> queryParam, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement(sql);

			if (queryParam.size() >= 0){
				for (int i=1;i<=queryParam.size();i++){
					int arrayCounter = i-1;
					String type = queryParam.get(arrayCounter).paramType;
					if (type.equalsIgnoreCase("string")){
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("integer")){
						int value = (int) queryParam.get(arrayCounter).paramValue;
						pstm.setInt(i, value);
					}else if (type.equalsIgnoreCase("decimal") || type.equalsIgnoreCase("double") ){
						Double value = (Double) queryParam.get(arrayCounter).paramValue;
						pstm.setDouble(i, value);
					}else if (type.equalsIgnoreCase("boolean")){
						Boolean value = (Boolean) queryParam.get(arrayCounter).paramValue;
						pstm.setBoolean(i, value);
					}else{
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}
				}
			}

			pstm.execute();
			success = true;
		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return success;
	}

	public static model.mdlQueryExecute QueryParam(String type, Object value){
		model.mdlQueryExecute param = new model.mdlQueryExecute();
		param.paramType = type;
		param.paramValue = value;

		return param;
	}

	public static Boolean QueryTransaction(List<model.mdlQueryTransaction> listMdlQueryTrans, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;
		String sql;
		List<model.mdlQueryExecute> queryParam;

		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);

			for(model.mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans){
				pstm = null;
				sql = mdlQueryTrans.sql;
				queryParam = new ArrayList<model.mdlQueryExecute>();
				queryParam.addAll(mdlQueryTrans.listParam);
				pstm = connection.prepareStatement(sql);

				for (int i=1;i<=queryParam.size();i++){
					int arrayCounter = i-1;
					String type = queryParam.get(arrayCounter).paramType;
					if (type.equalsIgnoreCase("string")){
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("integer")){
						int value = (int) queryParam.get(arrayCounter).paramValue;
						pstm.setInt(i, value);
					}else if (type.equalsIgnoreCase("decimal") || type.equalsIgnoreCase("double") ){
						Double value = (Double) queryParam.get(arrayCounter).paramValue;
						pstm.setDouble(i, value);
					}else if (type.equalsIgnoreCase("boolean")){
						Boolean value = (Boolean) queryParam.get(arrayCounter).paramValue;
						pstm.setBoolean(i, value);
					}else{
						String value = (String) queryParam.get(arrayCounter).paramValue;
						pstm.setString(i, value);
					}
				}

				pstm.executeUpdate();
			}

			connection.commit(); //commit transaction if all of the proccess is running well
			success = true;
		}catch(Exception ex){
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "Transaction"+function, pstm.toString() , user);
		            }
		        }
			LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
			success = false;
		}finally{
			try{
				if (pstm != null) pstm.close();
				connection.setAutoCommit(true);
				if (connection != null) connection.close();
			}catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}

		return success;
	}

}


