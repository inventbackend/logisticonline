
package modelKojaGetOTP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

    @SerializedName("ns2:sendOTPResponse")
    @Expose
    private Ns2SendOTPResponse ns2SendOTPResponse;

    public Ns2SendOTPResponse getNs2SendOTPResponse() {
        return ns2SendOTPResponse;
    }

    public void setNs2SendOTPResponse(Ns2SendOTPResponse ns2SendOTPResponse) {
        this.ns2SendOTPResponse = ns2SendOTPResponse;
    }

}
