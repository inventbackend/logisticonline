
package modelKojaGetOTP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns2SendOTPResponse {

    @SerializedName("xmlns:ns2")
    @Expose
    private String xmlnsNs2;
    @SerializedName("return")
    @Expose
    private String _return;

    public String getXmlnsNs2() {
        return xmlnsNs2;
    }

    public void setXmlnsNs2(String xmlnsNs2) {
        this.xmlnsNs2 = xmlnsNs2;
    }

    public String getReturn() {
        return _return;
    }

    public void setReturn(String _return) {
        this._return = _return;
    }

}
