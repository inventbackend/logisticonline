
package modelKojaGetOTP;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Envelope {

    @SerializedName("S:Envelope")
    @Expose
    private SEnvelope sEnvelope;

    public SEnvelope getSEnvelope() {
        return sEnvelope;
    }

    public void setSEnvelope(SEnvelope sEnvelope) {
        this.sEnvelope = sEnvelope;
    }

}
