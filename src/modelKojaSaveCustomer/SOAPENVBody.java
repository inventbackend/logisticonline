package modelKojaSaveCustomer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:MAIN_SaveOrganizationResponse")
    @Expose
    private Ns1MAIN_SaveOrganizationResponse ns1MAIN_SaveOrganizationResponse;

    public Ns1MAIN_SaveOrganizationResponse getNs1MAIN_SaveOrganizationResponse() {
        return ns1MAIN_SaveOrganizationResponse;
    }

    public void setNs1MAIN_SaveOrganizationResponse(Ns1MAIN_SaveOrganizationResponse ns1MAIN_SaveOrganizationResponse) {
        this.ns1MAIN_SaveOrganizationResponse = ns1MAIN_SaveOrganizationResponse;
    }

}
