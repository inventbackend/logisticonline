
package modelKojaGetDocNpe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:MAIN_GetDocumentCustomsNGenResponse")
    @Expose
    private Ns1MAINGetDocumentCustomsNGenResponse ns1MAINGetDocumentCustomsNGenResponse;

    public Ns1MAINGetDocumentCustomsNGenResponse getNs1MAINGetDocumentCustomsNGenResponse() {
        return ns1MAINGetDocumentCustomsNGenResponse;
    }

    public void setNs1MAINGetDocumentCustomsNGenResponse(Ns1MAINGetDocumentCustomsNGenResponse ns1MAINGetDocumentCustomsNGenResponse) {
        this.ns1MAINGetDocumentCustomsNGenResponse = ns1MAINGetDocumentCustomsNGenResponse;
    }

}
