package modelKojaGetDocNpe;

public class mdlGetDocNpe {

	public String MESSAGE;
	public String STATUS;
	public String[] NO_CONT;
	
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String[] getNO_CONT() {
		return NO_CONT;
	}
	public void setNO_CONT(String[] nO_CONT) {
		NO_CONT = nO_CONT;
	}
}
