package modelKojaContainerTracking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

    @SerializedName("ns2:container_trackingResponse")
    @Expose
    private Ns2Container_trackingResponse ns2Container_trackingResponse;

    public Ns2Container_trackingResponse getNs2Container_trackingResponse() {
        return ns2Container_trackingResponse;
    }

    public void setNs2Container_trackingResponse(Ns2Container_trackingResponse ns2Container_trackingResponse) {
        this.ns2Container_trackingResponse = ns2Container_trackingResponse;
    }

}
