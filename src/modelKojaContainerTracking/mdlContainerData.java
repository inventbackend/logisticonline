package modelKojaContainerTracking;

public class mdlContainerData {

	public Boolean STATUS;
	public String MESSAGE;
	public String CNTR_ID;
	public String LOCATION_ID;
	public String LOCATION;
	public String MOVEMENT_DATE;
	public String VESSEL_LOCATION;
	public String STACK_ON_VESSEL;
	
	public Boolean getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(Boolean sTATUS) {
		STATUS = sTATUS;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String getCNTR_ID() {
		return CNTR_ID;
	}
	public void setCNTR_ID(String cNTR_ID) {
		CNTR_ID = cNTR_ID;
	}
	public String getLOCATION_ID() {
		return LOCATION_ID;
	}
	public void setLOCATION_ID(String lOCATION_ID) {
		LOCATION_ID = lOCATION_ID;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getMOVEMENT_DATE() {
		return MOVEMENT_DATE;
	}
	public void setMOVEMENT_DATE(String mOVEMENT_DATE) {
		MOVEMENT_DATE = mOVEMENT_DATE;
	}
	public String getVESSEL_LOCATION() {
		return VESSEL_LOCATION;
	}
	public void setVESSEL_LOCATION(String vESSEL_LOCATION) {
		VESSEL_LOCATION = vESSEL_LOCATION;
	}
	public String getSTACK_ON_VESSEL() {
		return STACK_ON_VESSEL;
	}
	public void setSTACK_ON_VESSEL(String sTACK_ON_VESSEL) {
		STACK_ON_VESSEL = sTACK_ON_VESSEL;
	}
	
}
