package modelKojaRequestInquiry;

public class mdlRequestInquiry {

	public String serviceUrl;
	public String transactionDateTime;
	public String requestDateTime;
	public String transactionID;
	public String proformaNo;
	public String bankName;
	public String companyCode;
	public String channelID;
	public String billingCode;
	public String billingDescription;
	public Integer billingAmount;
	public Boolean status;
	public String message;
	
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}
	public String getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
	public String getProformaNo() {
		return proformaNo;
	}
	public void setProformaNo(String proformaNo) {
		this.proformaNo = proformaNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getChannelID() {
		return channelID;
	}
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getBillingCode() {
		return billingCode;
	}
	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}
	public String getBillingDescription() {
		return billingDescription;
	}
	public void setBillingDescription(String billingDescription) {
		this.billingDescription = billingDescription;
	}
	public Integer getBillingAmount() {
		return billingAmount;
	}
	public void setBillingAmount(Integer billingAmount) {
		this.billingAmount = billingAmount;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
