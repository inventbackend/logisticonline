package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillDetail {

	@SerializedName("billCode")
	@Expose
	private String billCode;
	@SerializedName("billName")
	@Expose
	private String billName;
	@SerializedName("billAmount")
	@Expose
	private Integer billAmount;
	
	@SerializedName("reference1")
	@Expose
	private String reference1;
	@SerializedName("reference2")
	@Expose
	private String reference2;
	@SerializedName("reference3")
	@Expose
	private String reference3;
	
	public String getReference3() {
	return reference3;
	}
	
	public void setReference3(String reference3) {
	this.reference3 = reference3;
	}
	
	public String getBillCode() {
	return billCode;
	}
	
	public void setBillCode(String billCode) {
	this.billCode = billCode;
	}
	
	public String getReference1() {
	return reference1;
	}
	
	public void setReference1(String reference1) {
	this.reference1 = reference1;
	}
	
	public String getReference2() {
	return reference2;
	}
	
	public void setReference2(String reference2) {
	this.reference2 = reference2;
	}

	public String getBillName() {
		return billName;
	}

	public void setBillName(String billName) {
		this.billName = billName;
	}

	public Integer getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Integer billAmount) {
		this.billAmount = billAmount;
	}
	

}
