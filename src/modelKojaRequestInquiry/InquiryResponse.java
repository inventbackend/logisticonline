package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InquiryResponse {

@SerializedName("xmlns")
@Expose
private String xmlns;
@SerializedName("inquiryResult")
@Expose
private InquiryResult inquiryResult;

public String getXmlns() {
return xmlns;
}

public void setXmlns(String xmlns) {
this.xmlns = xmlns;
}

public InquiryResult getInquiryResult() {
return inquiryResult;
}

public void setInquiryResult(InquiryResult inquiryResult) {
this.inquiryResult = inquiryResult;
}

}