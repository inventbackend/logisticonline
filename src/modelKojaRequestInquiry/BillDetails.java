package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillDetails {

@SerializedName("BillDetail")
@Expose
private BillDetail billDetail;

public BillDetail getBillDetail() {
return billDetail;
}

public void setBillDetail(BillDetail billDetail) {
this.billDetail = billDetail;
}

}