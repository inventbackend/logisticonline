package modelKojaRequestInquiry;

public class mdlPaymentFlagging {

	public String serviceUrl;
	public String transactionDate;
	public String requestDate;
	public String companyCode;
	public String channelID;
	public String proformaNo;
	public Integer billingAmount;
	public String approvalCode;
	public String bankName;
	
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getChannelID() {
		return channelID;
	}
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getProformaNo() {
		return proformaNo;
	}
	public void setProformaNo(String proformaNo) {
		this.proformaNo = proformaNo;
	}
	public Integer getBillingAmount() {
		return billingAmount;
	}
	public void setBillingAmount(Integer billingAmount) {
		this.billingAmount = billingAmount;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
}
