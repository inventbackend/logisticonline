package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InquiryResult {

@SerializedName("billDetails")
@Expose
private BillDetails billDetails;
@SerializedName("currency")
@Expose
private String currency;
@SerializedName("billInfo4")
@Expose
private String billInfo4;
@SerializedName("status")
@Expose
private Status status;

public BillDetails getBillDetails() {
return billDetails;
}

public void setBillDetails(BillDetails billDetails) {
this.billDetails = billDetails;
}

public String getCurrency() {
return currency;
}

public void setCurrency(String currency) {
this.currency = currency;
}

public String getBillInfo4() {
return billInfo4;
}

public void setBillInfo4(String billInfo4) {
this.billInfo4 = billInfo4;
}

public Status getStatus() {
return status;
}

public void setStatus(Status status) {
this.status = status;
}

}
