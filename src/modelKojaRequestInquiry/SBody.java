package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

@SerializedName("inquiryResponse")
@Expose
private InquiryResponse inquiryResponse;
@SerializedName("paymentResponse")
@Expose
private PaymentResponse paymentResponse;

public InquiryResponse getInquiryResponse() {
return inquiryResponse;
}

public void setInquiryResponse(InquiryResponse inquiryResponse) {
this.inquiryResponse = inquiryResponse;
}

public PaymentResponse getPaymentResponse() {
	return paymentResponse;
}

public void setPaymentResponse(PaymentResponse paymentResponse) {
	this.paymentResponse = paymentResponse;
}

}
