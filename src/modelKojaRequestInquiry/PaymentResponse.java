package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentResponse {

	@SerializedName("xmlns")
	@Expose
	private String xmlns;
	@SerializedName("paymentResult")
	@Expose
	private PaymentResult paymentResult;
	public String getXmlns() {
		return xmlns;
	}
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	public PaymentResult getPaymentResult() {
		return paymentResult;
	}
	public void setPaymentResult(PaymentResult paymentResult) {
		this.paymentResult = paymentResult;
	}
}
