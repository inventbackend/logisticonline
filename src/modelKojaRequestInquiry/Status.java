package modelKojaRequestInquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

@SerializedName("statusDescription")
@Expose
private String statusDescription;
@SerializedName("isError")
@Expose
private Boolean isError;
@SerializedName("errorCode")
@Expose
private String errorCode;

public String getStatusDescription() {
return statusDescription;
}

public void setStatusDescription(String statusDescription) {
this.statusDescription = statusDescription;
}

public Boolean getIsError() {
return isError;
}

public void setIsError(Boolean isError) {
this.isError = isError;
}

public String getErrorCode() {
return errorCode;
}

public void setErrorCode(String errorCode) {
this.errorCode = errorCode;
}

}
