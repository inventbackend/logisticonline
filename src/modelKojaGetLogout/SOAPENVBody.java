
package modelKojaGetLogout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:AUTH_GetLogOutResponse")
    @Expose
    private Ns1AUTHGetLogOutResponse ns1AUTHGetLogOutResponse;

    public Ns1AUTHGetLogOutResponse getNs1AUTHGetLogOutResponse() {
        return ns1AUTHGetLogOutResponse;
    }

    public void setNs1AUTHGetLogOutResponse(Ns1AUTHGetLogOutResponse ns1AUTHGetLogOutResponse) {
        this.ns1AUTHGetLogOutResponse = ns1AUTHGetLogOutResponse;
    }

}
