
package modelKojaTIDValidation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

    @SerializedName("ns2:truck_id_validationResponse")
    @Expose
    private Ns2TruckIdValidationResponse ns2TruckIdValidationResponse;

    public Ns2TruckIdValidationResponse getNs2TruckIdValidationResponse() {
        return ns2TruckIdValidationResponse;
    }

    public void setNs2TruckIdValidationResponse(Ns2TruckIdValidationResponse ns2TruckIdValidationResponse) {
        this.ns2TruckIdValidationResponse = ns2TruckIdValidationResponse;
    }

}
