package modelKojaTIDValidation;

public class mdlTruckData {

	public String MESSAGE;
	public Boolean STATUS;
	public String TID_EXPIRED_DATE;
	public String TID;
	
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public Boolean getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(Boolean sTATUS) {
		STATUS = sTATUS;
	}
	public String getTID_EXPIRED_DATE() {
		return TID_EXPIRED_DATE;
	}
	public void setTID_EXPIRED_DATE(String tID_EXPIRED_DATE) {
		TID_EXPIRED_DATE = tID_EXPIRED_DATE;
	}
	public String getTID() {
		return TID;
	}
	public void setTID(String tID) {
		TID = tID;
	}
}
