package modelKojaTIDValidation;

public class mdlTIDValidation {

	public String VALIDATION_MESSAGE;
	public Boolean VALIDATION_STATUS;
	public String USERNAME;
	public mdlData DATA;
	
	public String getVALIDATION_MESSAGE() {
		return VALIDATION_MESSAGE;
	}
	public void setVALIDATION_MESSAGE(String vALIDATION_MESSAGE) {
		VALIDATION_MESSAGE = vALIDATION_MESSAGE;
	}
	public Boolean getVALIDATION_STATUS() {
		return VALIDATION_STATUS;
	}
	public void setVALIDATION_STATUS(Boolean vALIDATION_STATUS) {
		VALIDATION_STATUS = vALIDATION_STATUS;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public mdlData getDATA() {
		return DATA;
	}
	public void setDATA(mdlData dATA) {
		DATA = dATA;
	}
	
}
