package modelKojaGetLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns1AUTHGetLoginResponse {

    @SerializedName("xmlns:ns1")
    @Expose
    private String xmlnsNs1;
    @SerializedName("return")
    @Expose
    private Return _return;

    public String getXmlnsNs1() {
        return xmlnsNs1;
    }

    public void setXmlnsNs1(String xmlnsNs1) {
        this.xmlnsNs1 = xmlnsNs1;
    }

    public Return getReturn() {
        return _return;
    }

    public void setReturn(Return _return) {
        this._return = _return;
    }

}
