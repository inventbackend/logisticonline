package modelKojaGetLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:AUTH_GetLoginResponse")
    @Expose
    private Ns1AUTHGetLoginResponse ns1AUTHGetLoginResponse;

    public Ns1AUTHGetLoginResponse getNs1AUTHGetLoginResponse() {
        return ns1AUTHGetLoginResponse;
    }

    public void setNs1AUTHGetLoginResponse(Ns1AUTHGetLoginResponse ns1AUTHGetLoginResponse) {
        this.ns1AUTHGetLoginResponse = ns1AUTHGetLoginResponse;
    }

}
