package modelKojaGetLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVEnvelope {

    @SerializedName("xmlns:xsd")
    @Expose
    private String xmlnsXsd;
    @SerializedName("xmlns:SOAP-ENV")
    @Expose
    private String xmlnsSOAPENV;
    @SerializedName("SOAP-ENV:Body")
    @Expose
    private SOAPENVBody sOAPENVBody;
    @SerializedName("SOAP-ENV:encodingStyle")
    @Expose
    private String sOAPENVEncodingStyle;
    @SerializedName("xmlns:xsi")
    @Expose
    private String xmlnsXsi;
    @SerializedName("xmlns:SOAP-ENC")
    @Expose
    private String xmlnsSOAPENC;

    public String getXmlnsXsd() {
        return xmlnsXsd;
    }

    public void setXmlnsXsd(String xmlnsXsd) {
        this.xmlnsXsd = xmlnsXsd;
    }

    public String getXmlnsSOAPENV() {
        return xmlnsSOAPENV;
    }

    public void setXmlnsSOAPENV(String xmlnsSOAPENV) {
        this.xmlnsSOAPENV = xmlnsSOAPENV;
    }

    public SOAPENVBody getSOAPENVBody() {
        return sOAPENVBody;
    }

    public void setSOAPENVBody(SOAPENVBody sOAPENVBody) {
        this.sOAPENVBody = sOAPENVBody;
    }

    public String getSOAPENVEncodingStyle() {
        return sOAPENVEncodingStyle;
    }

    public void setSOAPENVEncodingStyle(String sOAPENVEncodingStyle) {
        this.sOAPENVEncodingStyle = sOAPENVEncodingStyle;
    }

    public String getXmlnsXsi() {
        return xmlnsXsi;
    }

    public void setXmlnsXsi(String xmlnsXsi) {
        this.xmlnsXsi = xmlnsXsi;
    }

    public String getXmlnsSOAPENC() {
        return xmlnsSOAPENC;
    }

    public void setXmlnsSOAPENC(String xmlnsSOAPENC) {
        this.xmlnsSOAPENC = xmlnsSOAPENC;
    }

}
