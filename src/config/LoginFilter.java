package config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.logisticonline.LoginServlet;

/**
 * The purpose of this filter is to prevent users who are not logged in
 * from accessing confidential website areas. 
 */

@WebFilter("/*")
public class LoginFilter implements Filter {

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        HttpSession session = request.getSession(false);
        String path = ((HttpServletRequest) request).getRequestURI();
        
        if (path.startsWith("/LogisticOnline/mainform/")) {
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
        	chain.doFilter(request, response);
        } else if(noNeedsAuthentication(path)) {
        	chain.doFilter(request, response);
        } else if(session == null || session.getAttribute("user") == null) {
        	request.getRequestDispatcher("/Login").forward(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }


    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
    	System.out.println("destroy");
    }
    
  //basic validation of pages that do not require authentication
    private boolean noNeedsAuthentication(String url) {
        String[] validNonAuthenticationUrls =
            { "/Login", "/RegisterVendor", "/Register", "/ForgotPassword", "/Captcha", "/Captcha-Service" };
        for(String validUrl : validNonAuthenticationUrls) {
            if (url.endsWith(validUrl)) {
                return true;
            }
        }
        return false;
    }

}
