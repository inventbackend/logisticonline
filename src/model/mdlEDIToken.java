package model;

public class mdlEDIToken {

	public String token_type;
	public Integer expired_in;
	public String access_token;
	
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	public Integer getExpired_in() {
		return expired_in;
	}
	public void setExpired_in(Integer expired_in) {
		this.expired_in = expired_in;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

}
