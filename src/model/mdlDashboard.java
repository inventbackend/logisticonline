package model;

import java.sql.Time;
import java.util.List;

public class mdlDashboard {

	public String boxTitle;
	public Integer boxValue;
	public String boxInfo;
	
	public Integer totalOrderCompleted;
	public Integer totalOrderOnGoing;
	public Integer totalOrderUnProcessed;
	
	public Integer partaiPerSI;
	public Integer partaiSelesaiPerSI;
	public Integer partaiProcessPerSI;
	public Integer partaiUnprocessPerSI;
	
	public String latitude;
	public String longitude;
	public String trackingDate;
	public String vehicleNumber;
	public String vendorName;
	public String driverName;
	public String driverDeliveryStatus;
	public String shippingInvoiceID;
	public String deliveryOrderID;
	public String factoryName;
	public String itemDescription;
	public String portName;
	public String depoName;
	public String orderType;
	public Integer totalGatePass;
	//model for dashboard2
	public Integer availableOrder; //for vendor
	public Integer totalOrder; //for customer
	public Integer pickedOrder; //for customer
	public Integer unpickedOrder; //for customer
	//model for dashboardOrderList
	public String vendorOrderID;
	public String driverOrderAssignmentID;
	public String stuffingDate;
	public String containerNumber;
	public String sealNumber;
	public String yellowCard;
	public String customerName;
	public String peb;
	public String npe;
	public String userID;
	public Boolean isActiveMap;
	public String containerType;
	public String jsonDriverImage;
	
	public String tpsShpLocation;
	public String tpsShpLocationID;
	public String tpsShpMovementDate;
	public String tpsEir;
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getTrackingDate() {
		return trackingDate;
	}
	public void setTrackingDate(String trackingDate) {
		this.trackingDate = trackingDate;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverDeliveryStatus() {
		return driverDeliveryStatus;
	}
	public void setDriverDeliveryStatus(String driverDeliveryStatus) {
		this.driverDeliveryStatus = driverDeliveryStatus;
	}
	public String getShippingInvoiceID() {
		return shippingInvoiceID;
	}
	public void setShippingInvoiceID(String shippingInvoiceID) {
		this.shippingInvoiceID = shippingInvoiceID;
	}
	public String getDeliveryOrderID() {
		return deliveryOrderID;
	}
	public void setDeliveryOrderID(String deliveryOrderID) {
		this.deliveryOrderID = deliveryOrderID;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public String getDepoName() {
		return depoName;
	}
	public void setDepoName(String depoName) {
		this.depoName = depoName;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Integer getAvailableOrder() {
		return availableOrder;
	}
	public void setAvailableOrder(Integer availableOrder) {
		this.availableOrder = availableOrder;
	}
	public Integer getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(Integer totalOrder) {
		this.totalOrder = totalOrder;
	}
	public Integer getPickedOrder() {
		return pickedOrder;
	}
	public void setPickedOrder(Integer pickedOrder) {
		this.pickedOrder = pickedOrder;
	}
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	public String getYellowCard() {
		return yellowCard;
	}
	public void setYellowCard(String yellowCard) {
		this.yellowCard = yellowCard;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPeb() {
		return peb;
	}
	public void setPeb(String peb) {
		this.peb = peb;
	}
	public String getNpe() {
		return npe;
	}
	public void setNpe(String npe) {
		this.npe = npe;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public Boolean getIsActiveMap() {
		return isActiveMap;
	}
	public void setIsActiveMap(Boolean isActiveMap) {
		this.isActiveMap = isActiveMap;
	}
	public Integer getTotalGatePass() {
		return totalGatePass;
	}
	public void setTotalGatePass(Integer totalGatePass) {
		this.totalGatePass = totalGatePass;
	}
	public String getBoxTitle() {
		return boxTitle;
	}
	public void setBoxTitle(String boxTitle) {
		this.boxTitle = boxTitle;
	}
	public Integer getBoxValue() {
		return boxValue;
	}
	public void setBoxValue(Integer boxValue) {
		this.boxValue = boxValue;
	}
	public Integer getPartaiPerSI() {
		return partaiPerSI;
	}
	public void setPartaiPerSI(Integer partaiPerSI) {
		this.partaiPerSI = partaiPerSI;
	}
	public Integer getPartaiSelesaiPerSI() {
		return partaiSelesaiPerSI;
	}
	public void setPartaiSelesaiPerSI(Integer partaiSelesaiPerSI) {
		this.partaiSelesaiPerSI = partaiSelesaiPerSI;
	}
	public String getDriverOrderAssignmentID() {
		return driverOrderAssignmentID;
	}
	public void setDriverOrderAssignmentID(String driverOrderAssignmentID) {
		this.driverOrderAssignmentID = driverOrderAssignmentID;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public Integer getTotalOrderCompleted() {
		return totalOrderCompleted;
	}
	public void setTotalOrderCompleted(Integer totalOrderCompleted) {
		this.totalOrderCompleted = totalOrderCompleted;
	}
	public Integer getTotalOrderOnGoing() {
		return totalOrderOnGoing;
	}
	public void setTotalOrderOnGoing(Integer totalOrderOnGoing) {
		this.totalOrderOnGoing = totalOrderOnGoing;
	}
	public Integer getTotalOrderUnProcessed() {
		return totalOrderUnProcessed;
	}
	public void setTotalOrderUnProcessed(Integer totalOrderUnProcessed) {
		this.totalOrderUnProcessed = totalOrderUnProcessed;
	}
	public String getVendorOrderID() {
		return vendorOrderID;
	}
	public void setVendorOrderID(String vendorOrderID) {
		this.vendorOrderID = vendorOrderID;
	}
	public String getBoxInfo() {
		return boxInfo;
	}
	public void setBoxInfo(String boxInfo) {
		this.boxInfo = boxInfo;
	}
	public Integer getPartaiProcessPerSI() {
		return partaiProcessPerSI;
	}
	public void setPartaiProcessPerSI(Integer partaiProcessPerSI) {
		this.partaiProcessPerSI = partaiProcessPerSI;
	}
	public Integer getPartaiUnprocessPerSI() {
		return partaiUnprocessPerSI;
	}
	public void setPartaiUnprocessPerSI(Integer partaiUnprocessPerSI) {
		this.partaiUnprocessPerSI = partaiUnprocessPerSI;
	}
	public Integer getUnpickedOrder() {
		return unpickedOrder;
	}
	public void setUnpickedOrder(Integer unpickedOrder) {
		this.unpickedOrder = unpickedOrder;
	}
	public String getContainerType() {
		return containerType;
	}
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}
	public String getJsonDriverImage() {
		return jsonDriverImage;
	}
	public void setJsonDriverImage(String jsonDriverImage) {
		this.jsonDriverImage = jsonDriverImage;
	}
	public String getTpsShpLocation() {
		return tpsShpLocation;
	}
	public void setTpsShpLocation(String tpsShpLocation) {
		this.tpsShpLocation = tpsShpLocation;
	}
	public String getTpsShpLocationID() {
		return tpsShpLocationID;
	}
	public void setTpsShpLocationID(String tpsShpLocationID) {
		this.tpsShpLocationID = tpsShpLocationID;
	}
	public String getTpsShpMovementDate() {
		return tpsShpMovementDate;
	}
	public void setTpsShpMovementDate(String tpsShpMovementDate) {
		this.tpsShpMovementDate = tpsShpMovementDate;
	}
	public String getTpsEir() {
		return tpsEir;
	}
	public void setTpsEir(String tpsEir) {
		this.tpsEir = tpsEir;
	}
	
}
