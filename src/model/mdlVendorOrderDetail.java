package model;

public class mdlVendorOrderDetail {
	public String orderID;
	public String orderDetailID;
	public String vendorOrderDetailID;
	public String vendorOrderID;
	public String driverID;
	public String driverName;
	public String vehicleNumber;
	public Integer vendorDetailStatus;
	public String cancellationReason;
	public String containerNumber;
	public String tidNumber;
	public String SealNumber;
	public String sealNumber;
	public Integer driverTargetAmount;
	public String vendorID;
	public String vendorName;
	public String createdBy;
	public String createdDate;
	public String updatedBy;
	public String updatedDate;
	public String yellowcard;
	public Integer cost;
	public String shippingInstruction;
	
	//for delivery order
	public String customer;
	public String pic;
	public String address;
	public String phone;
	public String commodity;
	public Integer totalParty;
	public String vesselVoyage;
	public String stuffingDate;
	public String port;
	
	
	public String timeInDepo;
	public String timeOutDepo;
	public String timeInFactory;
	public String timeOutFactory;
	public String timeInPort;
	public String timeOutPort;
	
	public String quantity;
	public String totalWeight;
	
	public String tpsShpLocation;
	public String tpsShpLocationID;
	public String tpsShpMovementDate;
	public String tpsEir;

	public String getVendorOrderDetailID() {
		return vendorOrderDetailID;
	}
	public void setVendorOrderDetailID(String vendorOrderDetailID) {
		this.vendorOrderDetailID = vendorOrderDetailID;
	}
	public String getVendorOrderID() {
		return vendorOrderID;
	}
	public void setVendorOrderID(String vendorOrderID) {
		this.vendorOrderID = vendorOrderID;
	}
	public String getDriverID() {
		return driverID;
	}
	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public Integer getVendorDetailStatus() {
		return vendorDetailStatus;
	}
	public void setVendorDetailStatus(Integer vendorDetailStatus) {
		this.vendorDetailStatus = vendorDetailStatus;
	}
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getTidNumber() {
		return tidNumber;
	}
	public void setTidNumber(String tidNumber) {
		this.tidNumber = tidNumber;
	}
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getDriverTargetAmount() {
		return driverTargetAmount;
	}
	public void setDriverTargetAmount(Integer driverTargetAmount) {
		this.driverTargetAmount = driverTargetAmount;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getOrderDetailID() {
		return orderDetailID;
	}
	public void setOrderDetailID(String orderDetailID) {
		this.orderDetailID = orderDetailID;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getCancellationReason() {
		return cancellationReason;
	}
	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	public String getYellowcard() {
		return yellowcard;
	}
	public void setYellowcard(String yellowcard) {
		this.yellowcard = yellowcard;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	
	//for delivery order
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public Integer getTotalParty() {
		return totalParty;
	}
	public void setTotalParty(Integer totalParty) {
		this.totalParty = totalParty;
	}
	public String getVesselVoyage() {
		return vesselVoyage;
	}
	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getTimeInDepo() {
		return timeInDepo;
	}
	public void setTimeInDepo(String timeInDepo) {
		this.timeInDepo = timeInDepo;
	}
	public String getTimeOutDepo() {
		return timeOutDepo;
	}
	public void setTimeOutDepo(String timeOutDepo) {
		this.timeOutDepo = timeOutDepo;
	}
	public String getTimeInFactory() {
		return timeInFactory;
	}
	public void setTimeInFactory(String timeInFactory) {
		this.timeInFactory = timeInFactory;
	}
	public String getTimeOutFactory() {
		return timeOutFactory;
	}
	public void setTimeOutFactory(String timeOutFactory) {
		this.timeOutFactory = timeOutFactory;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getTpsShpLocation() {
		return tpsShpLocation;
	}
	public void setTpsShpLocation(String tpsShpLocation) {
		this.tpsShpLocation = tpsShpLocation;
	}
	public String getTpsShpLocationID() {
		return tpsShpLocationID;
	}
	public void setTpsShpLocationID(String tpsShpLocationID) {
		this.tpsShpLocationID = tpsShpLocationID;
	}
	public String getTpsShpMovementDate() {
		return tpsShpMovementDate;
	}
	public void setTpsShpMovementDate(String tpsShpMovementDate) {
		this.tpsShpMovementDate = tpsShpMovementDate;
	}
	public String getTpsEir() {
		return tpsEir;
	}
	public void setTpsEir(String tpsEir) {
		this.tpsEir = tpsEir;
	}
	public String getTimeInPort() {
		return timeInPort;
	}
	public void setTimeInPort(String timeInPort) {
		this.timeInPort = timeInPort;
	}
	public String getTimeOutPort() {
		return timeOutPort;
	}
	public void setTimeOutPort(String timeOutPort) {
		this.timeOutPort = timeOutPort;
	}
	
	
}
