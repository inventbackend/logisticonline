package model;

import java.util.List;

public class mdlYellowCardParam {

	public String orderManagementID;
	public String shippingInstruction;
	public String doShippingLine;
	public String peb;
	public String npe;
	public String customerID;
	public String customerName;
	public String factoryID;
	public String factoryName;
	public String orderType;
	public String itemDescription;
	public List<mdlOrderManagementDetail> orderDetail;
	public String token;
	
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	public String getDoShippingLine() {
		return doShippingLine;
	}
	public void setDoShippingLine(String doShippingLine) {
		this.doShippingLine = doShippingLine;
	}
	public String getPeb() {
		return peb;
	}
	public void setPeb(String peb) {
		this.peb = peb;
	}
	public String getNpe() {
		return npe;
	}
	public void setNpe(String npe) {
		this.npe = npe;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getFactoryID() {
		return factoryID;
	}
	public void setFactoryID(String factoryID) {
		this.factoryID = factoryID;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public List<mdlOrderManagementDetail> getOrderDetail() {
		return orderDetail;
	}
	public void setOrderDetail(List<mdlOrderManagementDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
