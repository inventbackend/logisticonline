package model;

public class mdlContainerType {
	public String containerTypeID;
	public String description;
	public String iso;
	public Integer tare;

	public String getContainerTypeID() {
		return containerTypeID;
	}
	public void setContainerTypeID(String containerTypeID) {
		this.containerTypeID = containerTypeID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIso() {
		return iso;
	}
	public void setIso(String iso) {
		this.iso = iso;
	}
	public Integer getTare() {
		return tare;
	}
	public void setTare(Integer tare) {
		this.tare = tare;
	}


}
