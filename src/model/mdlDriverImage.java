package model;

public class mdlDriverImage {
	
	private String pathImage;
	
	private String type;

	public mdlDriverImage(String pathImage, String type) {
		super();
		this.pathImage = pathImage;
		this.type = type;
	}

	public mdlDriverImage() {
		super();
	}

	public String getPathImage() {
		return pathImage;
	}

	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}
