package model;

public class mdlTier {
	public String tierID;
	public String description;
	public String longDescription;

	public String getTierID() {
		return tierID;
	}
	public void setTierID(String tierID) {
		this.tierID = tierID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}


}