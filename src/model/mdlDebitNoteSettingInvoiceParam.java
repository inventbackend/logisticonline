package model;

public class mdlDebitNoteSettingInvoiceParam {
	
	private String debitNoteNumber;
	
	private Integer amount;
	
	private Boolean isFinalInvoice;
	
	private Integer customClearance;
	
	private Integer finalRate;
	
	private Integer additionalPrice;
	
	private Integer ppn;
	
	private Integer pph23;

	public String getDebitNoteNumber() {
		return debitNoteNumber;
	}

	public void setDebitNoteNumber(String debitNoteNumber) {
		this.debitNoteNumber = debitNoteNumber;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Boolean getIsFinalInvoice() {
		return isFinalInvoice;
	}

	public void setIsFinalInvoice(Boolean isFinalInvoice) {
		this.isFinalInvoice = isFinalInvoice;
	}

	public Integer getCustomClearance() {
		return customClearance;
	}

	public void setCustomClearance(Integer customClearance) {
		this.customClearance = customClearance;
	}

	public Integer getFinalRate() {
		return finalRate;
	}

	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}

	public Integer getAdditionalPrice() {
		return additionalPrice;
	}

	public void setAdditionalPrice(Integer additionalPrice) {
		this.additionalPrice = additionalPrice;
	}

	public Integer getPpn() {
		return ppn;
	}

	public void setPpn(Integer ppn) {
		this.ppn = ppn;
	}

	public Integer getPph23() {
		return pph23;
	}

	public void setPph23(Integer pph23) {
		this.pph23 = pph23;
	}

	
	
	

}
