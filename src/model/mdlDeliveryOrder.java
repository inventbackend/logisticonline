package model;

import java.util.List;

public class mdlDeliveryOrder {

	public String containerNumber;
	public String doNumber;
	public String customerName;
	public String address;
	public String phone;
	public String siNumber;
	public Integer totalParty;
	public String vesselVoyage;
	public String stuffingDate;
	public String sealNumber;
	public String port;
	public String vehicleNumber;
	public String driverID;
	public String driverName;
	public String commodity;
	public String customerPIC;
	
	public String timeInDepo;
	public String timeOutDepo;
	public String timeInFactory;
	public String timeOutFactory;
	public String timeInPort;
	public String timeOutPort;
	public String totalWeight;
	public String quantity;
	
	public String tidNumber;
	public String oldTidNumber;
	public String kojaInvoiceNo;
	
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getDoNumber() {
		return doNumber;
	}
	public void setDoNumber(String doNumber) {
		this.doNumber = doNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSiNumber() {
		return siNumber;
	}
	public void setSiNumber(String siNumber) {
		this.siNumber = siNumber;
	}
	public Integer getTotalParty() {
		return totalParty;
	}
	public void setTotalParty(Integer totalParty) {
		this.totalParty = totalParty;
	}
	public String getVesselVoyage() {
		return vesselVoyage;
	}
	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public String getCustomerPIC() {
		return customerPIC;
	}
	public void setCustomerPIC(String customerPIC) {
		this.customerPIC = customerPIC;
	}
	public String getTimeInDepo() {
		return timeInDepo;
	}
	public void setTimeInDepo(String timeInDepo) {
		this.timeInDepo = timeInDepo;
	}
	public String getTimeOutDepo() {
		return timeOutDepo;
	}
	public void setTimeOutDepo(String timeOutDepo) {
		this.timeOutDepo = timeOutDepo;
	}
	public String getTimeInFactory() {
		return timeInFactory;
	}
	public void setTimeInFactory(String timeInFactory) {
		this.timeInFactory = timeInFactory;
	}
	public String getTimeOutFactory() {
		return timeOutFactory;
	}
	public void setTimeOutFactory(String timeOutFactory) {
		this.timeOutFactory = timeOutFactory;
	}
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getTidNumber() {
		return tidNumber;
	}
	public void setTidNumber(String tidNumber) {
		this.tidNumber = tidNumber;
	}
	public String getKojaInvoiceNo() {
		return kojaInvoiceNo;
	}
	public void setKojaInvoiceNo(String kojaInvoiceNo) {
		this.kojaInvoiceNo = kojaInvoiceNo;
	}
	public String getDriverID() {
		return driverID;
	}
	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	public String getOldTidNumber() {
		return oldTidNumber;
	}
	public void setOldTidNumber(String oldTidNumber) {
		this.oldTidNumber = oldTidNumber;
	}
	public String getTimeInPort() {
		return timeInPort;
	}
	public void setTimeInPort(String timeInPort) {
		this.timeInPort = timeInPort;
	}
	public String getTimeOutPort() {
		return timeOutPort;
	}
	public void setTimeOutPort(String timeOutPort) {
		this.timeOutPort = timeOutPort;
	}
	
}
