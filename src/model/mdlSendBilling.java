package model;

public class mdlSendBilling {

	public String user;
	public String password;
	public Integer billerId;
	public String productId;
	public String channelId;
	public Boolean status;
	public String errDesc;
	public String serviceUrl;
	public mdlSendBillingData data;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getBillerId() {
		return billerId;
	}
	public void setBillerId(Integer billerId) {
		this.billerId = billerId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public mdlSendBillingData getData() {
		return data;
	}
	public void setData(mdlSendBillingData data) {
		this.data = data;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getErrDesc() {
		return errDesc;
	}
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
}
