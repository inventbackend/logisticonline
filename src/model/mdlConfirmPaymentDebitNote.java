package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class mdlConfirmPaymentDebitNote {
	
	@SerializedName("amount")
	@Expose
	private String amount;
	@SerializedName("amountPaid")
	@Expose
	private String amountPaid;
	@SerializedName("paymentDate")
	@Expose
	private String paymentDate;
	@SerializedName("paymentMethod")
	@Expose
	private String paymentMethod;
	@SerializedName("debitNoteNumber")
	@Expose
	private String debitNoteNumber;
	@SerializedName("checkedDebitNote")
	@Expose
	private String checkedDebitNote;

	public String getAmount() {
	return amount;
	}

	public void setAmount(String amount) {
	this.amount = amount;
	}

	public String getAmountPaid() {
	return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
	this.amountPaid = amountPaid;
	}

	public String getPaymentDate() {
	return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
	this.paymentDate = paymentDate;
	}

	public String getPaymentMethod() {
	return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
	this.paymentMethod = paymentMethod;
	}

	public String getDebitNoteNumber() {
	return debitNoteNumber;
	}

	public void setDebitNoteNumber(String debitNoteNumber) {
	this.debitNoteNumber = debitNoteNumber;
	}

	public String getCheckedDebitNote() {
	return checkedDebitNote;
	}

	public void setCheckedDebitNote(String checkedDebitNote) {
	this.checkedDebitNote = checkedDebitNote;
	}

}
