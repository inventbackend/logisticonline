package model;

public class mdlUser {
	public String UserId;
	public String Staff;
	public String tempStaff;
	public String Username;
	public String Password;
	public String Area;
	public String Role;
	public String RoleName;
	public String Brand;
	public String Type;
	public Integer IsActive;
	public String UserEmail;
	public String unEncryptedPassword;
	
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public Integer getIsActive() {
		return IsActive;
	}
	public void setIsActive(Integer isActive) {
		IsActive = isActive;
	}
	public String getUserEmail() {
		return UserEmail;
	}
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}
	public String getUnEncryptedPassword() {
		return unEncryptedPassword;
	}
	public void setUnEncryptedPassword(String unEncryptedPassword) {
		this.unEncryptedPassword = unEncryptedPassword;
	}
	public String getStaff() {
		return Staff;
	}
	public void setStaff(String staff) {
		Staff = staff;
	}
	public String getTempStaff() {
		return tempStaff;
	}
	public void setTempStaff(String tempStaff) {
		this.tempStaff = tempStaff;
	}
	
	
	
}
