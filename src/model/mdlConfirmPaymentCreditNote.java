package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class mdlConfirmPaymentCreditNote {
	
	@SerializedName("amount")
	@Expose
	private String amount;
	@SerializedName("amountPaid")
	@Expose
	private String amountPaid;
	@SerializedName("paymentDate")
	@Expose
	private String paymentDate;
	@SerializedName("paymentMethod")
	@Expose
	private String paymentMethod;
	@SerializedName("creditNoteNumber")
	@Expose
	private String creditNoteNumber;
	@SerializedName("checkedCredittNote")
	@Expose
	private String checkedCredittNote;

	public String getAmount() {
	return amount;
	}

	public void setAmount(String amount) {
	this.amount = amount;
	}

	public String getAmountPaid() {
	return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
	this.amountPaid = amountPaid;
	}

	public String getPaymentDate() {
	return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
	this.paymentDate = paymentDate;
	}

	public String getPaymentMethod() {
	return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
	this.paymentMethod = paymentMethod;
	}

	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}

	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}

	public String getCheckedCredittNote() {
		return checkedCredittNote;
	}

	public void setCheckedCredittNote(String checkedCredittNote) {
		this.checkedCredittNote = checkedCredittNote;
	}
}
