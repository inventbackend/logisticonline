package model;

public class mdlOnbehalfNpwp {

	public String customerID;
	public String customerName;
	public String onbehalfNpwp;
	public String onbehalfCustomer;
	public String previousNpwp;
	
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOnbehalfNpwp() {
		return onbehalfNpwp;
	}
	public void setOnbehalfNpwp(String onbehalfNpwp) {
		this.onbehalfNpwp = onbehalfNpwp;
	}
	public String getOnbehalfCustomer() {
		return onbehalfCustomer;
	}
	public void setOnbehalfCustomer(String onbehalfCustomer) {
		this.onbehalfCustomer = onbehalfCustomer;
	}
	public String getPreviousNpwp() {
		return previousNpwp;
	}
	public void setPreviousNpwp(String previousNpwp) {
		this.previousNpwp = previousNpwp;
	}
	
}
