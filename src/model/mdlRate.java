package model;

public class mdlRate {
	public String rateID;
	public String name;
	public String districtID;
	public String districtName;
	public String tierID;
	public String tierName;
	public String containerTypeID;
	public String containerTypeName;
	public Integer price;
	public String roleID;
	public String roleName;
	public String customerID;
	public String factoryID;
	public Integer finalPrice;
	public String vendorID;

	public String getRateID() {
		return rateID;
	}
	public void setRateID(String rateID) {
		this.rateID = rateID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDistrictID() {
		return districtID;
	}
	public void setDistrictID(String districtID) {
		this.districtID = districtID;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getTierID() {
		return tierID;
	}
	public void setTierID(String tierID) {
		this.tierID = tierID;
	}
	public String getTierName() {
		return tierName;
	}
	public void setTierName(String tierName) {
		this.tierName = tierName;
	}
	public String getContainerTypeID() {
		return containerTypeID;
	}
	public void setContainerTypeID(String containerTypeID) {
		this.containerTypeID = containerTypeID;
	}
	public String getContainerTypeName() {
		return containerTypeName;
	}
	public void setContainerTypeName(String containerTypeName) {
		this.containerTypeName = containerTypeName;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getFactoryID() {
		return factoryID;
	}
	public void setFactoryID(String factoryID) {
		this.factoryID = factoryID;
	}
	public Integer getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(Integer finalPrice) {
		this.finalPrice = finalPrice;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	
	
}
