package model;

import java.util.List;

public class mdlDebitNote {
	
	public String debitNoteNumber;
	public String customerID;
	public String customerName;
	public String orderManagementID;
	public String date;
	public Integer amount;
	public String status;
	public Integer overdue;
	public String paymentDate;
	public Integer amountPaid;
	public Boolean isFinalInvoice;
	public Boolean isReimburse;
	public Integer finalRate;
	public Integer customClearance;
	public Integer additionalPrice;
	private String debitNoteReimburseDetails;
	private String reimburses;
	private Integer ppn;
	private Integer pph23;
	private String debitNoteNumberDetail;
	
	//for order detail
	String shippingInvoiceID;
	String deliveryOrderID;
	String factoryID;
	String factoryName;
	String factoryAddress;
	String orderType;
	String itemDescription;
	String tier;
	String tierDescription;
	String portID;
	String portName;
	String portUTC;
	String shippingID;
	String shippingName;
	String depoID;
	String depoName;
	String voyageNo;
	String vesselName;
	
	public String getDebitNoteNumber() {
		return debitNoteNumber;
	}
	public void setDebitNoteNumber(String debitNoteNumber) {
		this.debitNoteNumber = debitNoteNumber;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getOverdue() {
		return overdue;
	}
	public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Integer getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(Integer amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	//for order detail
	public String getShippingInvoiceID() {
		return shippingInvoiceID;
	}
	public void setShippingInvoiceID(String shippingInvoiceID) {
		this.shippingInvoiceID = shippingInvoiceID;
	}
	public String getDeliveryOrderID() {
		return deliveryOrderID;
	}
	public void setDeliveryOrderID(String deliveryOrderID) {
		this.deliveryOrderID = deliveryOrderID;
	}
	public String getFactoryID() {
		return factoryID;
	}
	public void setFactoryID(String factoryID) {
		this.factoryID = factoryID;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getFactoryAddress() {
		return factoryAddress;
	}
	public void setFactoryAddress(String factoryAddress) {
		this.factoryAddress = factoryAddress;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getTier() {
		return tier;
	}
	public void setTier(String tier) {
		this.tier = tier;
	}
	public String getTierDescription() {
		return tierDescription;
	}
	public void setTierDescription(String tierDescription) {
		this.tierDescription = tierDescription;
	}
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public String getPortUTC() {
		return portUTC;
	}
	public void setPortUTC(String portUTC) {
		this.portUTC = portUTC;
	}
	public String getShippingID() {
		return shippingID;
	}
	public void setShippingID(String shippingID) {
		this.shippingID = shippingID;
	}
	public String getShippingName() {
		return shippingName;
	}
	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}
	public String getDepoID() {
		return depoID;
	}
	public void setDepoID(String depoID) {
		this.depoID = depoID;
	}
	public String getDepoName() {
		return depoName;
	}
	public void setDepoName(String depoName) {
		this.depoName = depoName;
	}
	public String getVoyageNo() {
		return voyageNo;
	}
	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	public String getVesselName() {
		return vesselName;
	}
	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
	public Boolean getIsFinalInvoice() {
		return isFinalInvoice;
	}
	public void setIsFinalInvoice(Boolean isFinalInvoice) {
		this.isFinalInvoice = isFinalInvoice;
	}
	public Integer getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}
	public Boolean getIsReimburse() {
		return isReimburse;
	}
	public void setIsReimburse(Boolean isReimburse) {
		this.isReimburse = isReimburse;
	}
	public Integer getCustomClearance() {
		return customClearance;
	}
	public void setCustomClearance(Integer customClearance) {
		this.customClearance = customClearance;
	}
	public String getDebitNoteReimburseDetails() {
		return debitNoteReimburseDetails;
	}
	public void setDebitNoteReimburseDetails(String debitNoteReimburseDetails) {
		this.debitNoteReimburseDetails = debitNoteReimburseDetails;
	}
	public String getReimburses() {
		return reimburses;
	}
	public void setReimburses(String reimburses) {
		this.reimburses = reimburses;
	}
	public Integer getAdditionalPrice() {
		return additionalPrice;
	}
	public void setAdditionalPrice(Integer additionalPrice) {
		this.additionalPrice = additionalPrice;
	}
	public Integer getPpn() {
		return ppn;
	}
	public void setPpn(Integer ppn) {
		this.ppn = ppn;
	}
	public Integer getPph23() {
		return pph23;
	}
	public void setPph23(Integer pph23) {
		this.pph23 = pph23;
	}
	public String getDebitNoteNumberDetail() {
		return debitNoteNumberDetail;
	}
	public void setDebitNoteNumberDetail(String debitNoteNumberDetail) {
		this.debitNoteNumberDetail = debitNoteNumberDetail;
	}
}
