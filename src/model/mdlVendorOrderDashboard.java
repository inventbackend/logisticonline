package model;

public class mdlVendorOrderDashboard {

	public Integer totalAvailableOrder;
	public Integer totalPickedOrder;
	public Integer totalCancelledOrder;
	public Integer totalProcessedOrder;
	
	public String boxTitle;
	public String boxImage;
	public Integer boxValue;

	
	
	public Integer getTotalAvailableOrder() {
		return totalAvailableOrder;
	}
	public void setTotalAvailableOrder(Integer totalAvailableOrder) {
		this.totalAvailableOrder = totalAvailableOrder;
	}
	public Integer getTotalPickedOrder() {
		return totalPickedOrder;
	}
	public void setTotalPickedOrder(Integer totalPickedOrder) {
		this.totalPickedOrder = totalPickedOrder;
	}
	public Integer getTotalCancelledOrder() {
		return totalCancelledOrder;
	}
	public void setTotalCancelledOrder(Integer totalCancelledOrder) {
		this.totalCancelledOrder = totalCancelledOrder;
	}
	public Integer getTotalProcessedOrder() {
		return totalProcessedOrder;
	}
	public void setTotalProcessedOrder(Integer totalProcessedOrder) {
		this.totalProcessedOrder = totalProcessedOrder;
	}
	public String getBoxTitle() {
		return boxTitle;
	}
	public void setBoxTitle(String boxTitle) {
		this.boxTitle = boxTitle;
	}
	public String getBoxImage() {
		return boxImage;
	}
	public void setBoxImage(String boxImage) {
		this.boxImage = boxImage;
	}
	public Integer getBoxValue() {
		return boxValue;
	}
	public void setBoxValue(Integer boxValue) {
		this.boxValue = boxValue;
	}
	
	
}
