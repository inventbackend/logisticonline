package model;

import java.util.ArrayList;
import java.util.List;

public class mdlQueryTransaction {

	public String sql;
	public List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
	
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public List<model.mdlQueryExecute> getListParam() {
		return listParam;
	}
	public void setListParam(List<model.mdlQueryExecute> listParam) {
		this.listParam = listParam;
	}
	
}
