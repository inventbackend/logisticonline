package model;

public class mdlReimburse {
	private String reimburseID;
	
	private String name;
	
	private String description;

	public mdlReimburse() {
		super();
	}

	public String getReimburseID() {
		return reimburseID;
	}

	public void setReimburseID(String reimburseID) {
		this.reimburseID = reimburseID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
