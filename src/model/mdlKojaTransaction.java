package model;

public class mdlKojaTransaction {

	public String sessionID;
	public String customerID;
	public String[] isoCode;
	public String transactionTypeID;
	public String documentShippingNo;
	public String shippingLine;
	public String customsDocumentID;
	public String noBlAwb;
	public String documentShippingDate;
	public String voyageNo;
	public String voyageCompanyCode;
	public String[] weight;
	public String documentNo;
	public String vesselID;
	public String pod;
	public String documentDate;
	public String pol;
	public String fd;
	public String[] containerNumber;
	public String logolCustomerID;
	public String logolCustomerName;
	public String logolCustomerAddress;
	public String logolCustomerMail;
	public String logolCustomerPhone;
	public String orderID;
	public String proformaNo;
	public String sisoCode;
	public String sWeight;
	public String sContainerNumber;
	public String npeDoc;
	public String npwp;
	public String shipperName;
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String[] getIsoCode() {
		return isoCode;
	}
	public void setIsoCode(String[] isoCode) {
		this.isoCode = isoCode;
	}
	public String getTransactionTypeID() {
		return transactionTypeID;
	}
	public void setTransactionTypeID(String transactionTypeID) {
		this.transactionTypeID = transactionTypeID;
	}
	public String getDocumentShippingNo() {
		return documentShippingNo;
	}
	public void setDocumentShippingNo(String documentShippingNo) {
		this.documentShippingNo = documentShippingNo;
	}
	public String getShippingLine() {
		return shippingLine;
	}
	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}
	public String getCustomsDocumentID() {
		return customsDocumentID;
	}
	public void setCustomsDocumentID(String customsDocumentID) {
		this.customsDocumentID = customsDocumentID;
	}
	public String getNoBlAwb() {
		return noBlAwb;
	}
	public void setNoBlAwb(String noBlAwb) {
		this.noBlAwb = noBlAwb;
	}
	public String getDocumentShippingDate() {
		return documentShippingDate;
	}
	public void setDocumentShippingDate(String documentShippingDate) {
		this.documentShippingDate = documentShippingDate;
	}
	public String getVoyageNo() {
		return voyageNo;
	}
	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	public String getVoyageCompanyCode() {
		return voyageCompanyCode;
	}
	public void setVoyageCompanyCode(String voyageCompanyCode) {
		this.voyageCompanyCode = voyageCompanyCode;
	}
	public String[] getWeight() {
		return weight;
	}
	public void setWeight(String[] weight) {
		this.weight = weight;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getVesselID() {
		return vesselID;
	}
	public void setVesselID(String vesselID) {
		this.vesselID = vesselID;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getFd() {
		return fd;
	}
	public void setFd(String fd) {
		this.fd = fd;
	}
	public String[] getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String[] containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getLogolCustomerID() {
		return logolCustomerID;
	}
	public void setLogolCustomerID(String logolCustomerID) {
		this.logolCustomerID = logolCustomerID;
	}
	public String getLogolCustomerName() {
		return logolCustomerName;
	}
	public void setLogolCustomerName(String logolCustomerName) {
		this.logolCustomerName = logolCustomerName;
	}
	public String getLogolCustomerAddress() {
		return logolCustomerAddress;
	}
	public void setLogolCustomerAddress(String logolCustomerAddress) {
		this.logolCustomerAddress = logolCustomerAddress;
	}
	public String getLogolCustomerMail() {
		return logolCustomerMail;
	}
	public void setLogolCustomerMail(String logolCustomerMail) {
		this.logolCustomerMail = logolCustomerMail;
	}
	public String getLogolCustomerPhone() {
		return logolCustomerPhone;
	}
	public void setLogolCustomerPhone(String logolCustomerPhone) {
		this.logolCustomerPhone = logolCustomerPhone;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getProformaNo() {
		return proformaNo;
	}
	public void setProformaNo(String proformaNo) {
		this.proformaNo = proformaNo;
	}
	public String getSisoCode() {
		return sisoCode;
	}
	public void setSisoCode(String sisoCode) {
		this.sisoCode = sisoCode;
	}
	public String getsWeight() {
		return sWeight;
	}
	public void setsWeight(String sWeight) {
		this.sWeight = sWeight;
	}
	public String getsContainerNumber() {
		return sContainerNumber;
	}
	public void setsContainerNumber(String sContainerNumber) {
		this.sContainerNumber = sContainerNumber;
	}
	public String getNpeDoc() {
		return npeDoc;
	}
	public void setNpeDoc(String npeDoc) {
		this.npeDoc = npeDoc;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	
}
