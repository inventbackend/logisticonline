package model;

public class mdlVendor {
	public String VendorID;
	public String VendorName;
	public String Address;
	public String Email;
	public String MobilePhone;
	public String OfficePhone;
	public String PIC;
	public String NPWP;
	public String SIUJPT;
	public String GarageAddress;
	public String BankName;
	public String BankAccount;
	public String BankBranch;
	public String Username;
	public String Password;
	public String[] Tier;
	public String province;
	public String postalCode;
	
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getMobilePhone() {
		return MobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		MobilePhone = mobilePhone;
	}
	public String getOfficePhone() {
		return OfficePhone;
	}
	public void setOfficePhone(String officePhone) {
		OfficePhone = officePhone;
	}
	public String getNPWP() {
		return NPWP;
	}
	public void setNPWP(String nPWP) {
		NPWP = nPWP;
	}
	public String getSIUJPT() {
		return SIUJPT;
	}
	public void setSIUJPT(String sIUJPT) {
		SIUJPT = sIUJPT;
	}
	public String getGarageAddress() {
		return GarageAddress;
	}
	public void setGarageAddress(String garageAddress) {
		GarageAddress = garageAddress;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getBankAccount() {
		return BankAccount;
	}
	public void setBankAccount(String bankAccount) {
		BankAccount = bankAccount;
	}
	public String getBankBranch() {
		return BankBranch;
	}
	public void setBankBranch(String bankBranch) {
		BankBranch = bankBranch;
	}
	public String getPIC() {
		return PIC;
	}
	public void setPIC(String pIC) {
		PIC = pIC;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String[] getTier() {
		return Tier;
	}
	public void setTier(String[] tier) {
		Tier = tier;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
}
