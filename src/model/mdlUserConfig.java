package model;

public class mdlUserConfig {

	public String DeviceID;
	public String VendorID;
	public String DriverID;
	public String IpLocal;
	public String PortLocal;
	public String IpPublic;
	public String PortPublic;
	public String IpAlternative;
	public String PortAlternative;
	public String Password;
	
	public String getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getDriverID() {
		return DriverID;
	}
	public void setDriverID(String driverID) {
		DriverID = driverID;
	}
	public String getIpLocal() {
		return IpLocal;
	}
	public void setIpLocal(String ipLocal) {
		IpLocal = ipLocal;
	}
	public String getPortLocal() {
		return PortLocal;
	}
	public void setPortLocal(String portLocal) {
		PortLocal = portLocal;
	}
	public String getIpPublic() {
		return IpPublic;
	}
	public void setIpPublic(String ipPublic) {
		IpPublic = ipPublic;
	}
	public String getPortPublic() {
		return PortPublic;
	}
	public void setPortPublic(String portPublic) {
		PortPublic = portPublic;
	}
	public String getIpAlternative() {
		return IpAlternative;
	}
	public void setIpAlternative(String ipAlternative) {
		IpAlternative = ipAlternative;
	}
	public String getPortAlternative() {
		return PortAlternative;
	}
	public void setPortAlternative(String portAlternative) {
		PortAlternative = portAlternative;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	
}
