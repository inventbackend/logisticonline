package model;

public class mdlVessel {

	public String vesselCode;
	public String vesselName;
	public String voyageCode;
	public String voyageStatus;
	public String voyageCompany;
	public String podCode;
	public String podName;
	public String voyageArrival;
	public String voyageDeparture;
	public String terminal;
	public Integer exportLimit;
	
	public String getVesselCode() {
		return vesselCode;
	}
	public void setVesselCode(String vesselCode) {
		this.vesselCode = vesselCode;
	}
	public String getVesselName() {
		return vesselName;
	}
	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
	public String getVoyageCode() {
		return voyageCode;
	}
	public void setVoyageCode(String voyageCode) {
		this.voyageCode = voyageCode;
	}
	public String getVoyageStatus() {
		return voyageStatus;
	}
	public void setVoyageStatus(String voyageStatus) {
		this.voyageStatus = voyageStatus;
	}
	public String getVoyageCompany() {
		return voyageCompany;
	}
	public void setVoyageCompany(String voyageCompany) {
		this.voyageCompany = voyageCompany;
	}
	public String getPodCode() {
		return podCode;
	}
	public void setPodCode(String podCode) {
		this.podCode = podCode;
	}
	public String getPodName() {
		return podName;
	}
	public void setPodName(String podName) {
		this.podName = podName;
	}
	public String getVoyageArrival() {
		return voyageArrival;
	}
	public void setVoyageArrival(String voyageArrival) {
		this.voyageArrival = voyageArrival;
	}
	public String getVoyageDeparture() {
		return voyageDeparture;
	}
	public void setVoyageDeparture(String voyageDeparture) {
		this.voyageDeparture = voyageDeparture;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public Integer getExportLimit() {
		return exportLimit;
	}
	public void setExportLimit(Integer exportLimit) {
		this.exportLimit = exportLimit;
	}
	
}
