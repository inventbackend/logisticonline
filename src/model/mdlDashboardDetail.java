package model;

public class mdlDashboardDetail {

	public Integer customerOrderAccepted;
	public Integer vendorOrderPicked;
	public Integer depoIn;
	public Integer depoProcess;
	public Integer factoryIn;
	public Integer factoryProcess;
	public Integer deliveredProcess;
	
	public String boxTitle;
	public String boxImage;
	public Integer boxValue;
	public String boxInfo;
	
	public Integer getCustomerOrderAccepted() {
		return customerOrderAccepted;
	}
	public void setCustomerOrderAccepted(Integer customerOrderAccepted) {
		this.customerOrderAccepted = customerOrderAccepted;
	}
	public Integer getVendorOrderPicked() {
		return vendorOrderPicked;
	}
	public void setVendorOrderPicked(Integer vendorOrderPicked) {
		this.vendorOrderPicked = vendorOrderPicked;
	}
	public Integer getDepoProcess() {
		return depoProcess;
	}
	public void setDepoProcess(Integer depoProcess) {
		this.depoProcess = depoProcess;
	}
	public Integer getFactoryProcess() {
		return factoryProcess;
	}
	public void setFactoryProcess(Integer factoryProcess) {
		this.factoryProcess = factoryProcess;
	}
	public Integer getDeliveredProcess() {
		return deliveredProcess;
	}
	public void setDeliveredProcess(Integer deliveredProcess) {
		this.deliveredProcess = deliveredProcess;
	}
	public String getBoxTitle() {
		return boxTitle;
	}
	public void setBoxTitle(String boxTitle) {
		this.boxTitle = boxTitle;
	}
	public String getBoxImage() {
		return boxImage;
	}
	public void setBoxImage(String boxImage) {
		this.boxImage = boxImage;
	}
	public Integer getBoxValue() {
		return boxValue;
	}
	public void setBoxValue(Integer boxValue) {
		this.boxValue = boxValue;
	}
	public Integer getDepoIn() {
		return depoIn;
	}
	public void setDepoIn(Integer depoIn) {
		this.depoIn = depoIn;
	}
	public Integer getFactoryIn() {
		return factoryIn;
	}
	public void setFactoryIn(Integer factoryIn) {
		this.factoryIn = factoryIn;
	}
	public String getBoxInfo() {
		return boxInfo;
	}
	public void setBoxInfo(String boxInfo) {
		this.boxInfo = boxInfo;
	}
	
	
}
