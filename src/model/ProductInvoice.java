package model;

public enum ProductInvoice {
	PACKETA("[\"Trucking\",\"Custom Clearance\",\"Reimburse\"]", 0),
	PACKETB("[\"Trucking\",\"Custom Clearance\"]", 1),
	PACKETC("[\"Trucking\"]", 2),
	PACKETD("[\"Custome Clearance\"]", 3);
    
    
    private String action;
    private int intValue;

	private ProductInvoice(String action, int value) {
		this.action = action;
		intValue = value;
	}
	
	 public String toString() {
	        return action;
	 }
	
}
