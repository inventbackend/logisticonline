package model;

public class mdlVendorVehicle {

	public String VendorID;
	public String VendorName;
	public String VehicleNumber;
	public String brand;
	public String type;
	public String year;
	public String stnkNumber;
	public String stnkExpired;
	public String tidNumber;
	
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getVehicleNumber() {
		return VehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		VehicleNumber = vehicleNumber;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getStnkNumber() {
		return stnkNumber;
	}
	public void setStnkNumber(String stnkNumber) {
		this.stnkNumber = stnkNumber;
	}
	public String getStnkExpired() {
		return stnkExpired;
	}
	public void setStnkExpired(String stnkExpired) {
		this.stnkExpired = stnkExpired;
	}
	public String getTidNumber() {
		return tidNumber;
	}
	public void setTidNumber(String tidNumber) {
		this.tidNumber = tidNumber;
	}
	
	
}
