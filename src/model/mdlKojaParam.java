package model;

public class mdlKojaParam {

	public String serviceUrl;
	public String serviceUrl2;
	public String serviceUrl3;
	public String serviceUrl4;
	public String username;
	public String username2;
	public String password;
	public String password2;
	public String fstreamusername;
	public String fstreampassword;
	public String deviceName;
	public String session;
	public String email;
	public String phone;
	public String phone2;
	public String bankCompanyCode;
	public String bankChannelID;
	public String bankName;
	public String billingUser;
	public String billingPassword;
	public String billingBillerID;
	public String billingProduct;
	public String billingChannel;
	public String billingBillerAccount;
	public String billingBillerName;
	public String billingRemark;
	public String billingClientAccount;
	public String billingClientID;
	public String action;
	
	public String serverUrlAsso;
	public String assoUsername;
	public String assoPassword;
	
	public String serverFileLocation;
	public String serverIpport;
	public String serverFileAccess;
	public String serverFileSeparator;
	
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getServiceUrl2() {
		return serviceUrl2;
	}
	public void setServiceUrl2(String serviceUrl2) {
		this.serviceUrl2 = serviceUrl2;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFstreamusername() {
		return fstreamusername;
	}
	public void setFstreamusername(String fstreamusername) {
		this.fstreamusername = fstreamusername;
	}
	public String getFstreampassword() {
		return fstreampassword;
	}
	public void setFstreampassword(String fstreampassword) {
		this.fstreampassword = fstreampassword;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getUsername2() {
		return username2;
	}
	public void setUsername2(String username2) {
		this.username2 = username2;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getServiceUrl3() {
		return serviceUrl3;
	}
	public void setServiceUrl3(String serviceUrl3) {
		this.serviceUrl3 = serviceUrl3;
	}
	public String getBankCompanyCode() {
		return bankCompanyCode;
	}
	public void setBankCompanyCode(String bankCompanyCode) {
		this.bankCompanyCode = bankCompanyCode;
	}
	public String getBankChannelID() {
		return bankChannelID;
	}
	public void setBankChannelID(String bankChannelID) {
		this.bankChannelID = bankChannelID;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getServiceUrl4() {
		return serviceUrl4;
	}
	public void setServiceUrl4(String serviceUrl4) {
		this.serviceUrl4 = serviceUrl4;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getBillingUser() {
		return billingUser;
	}
	public void setBillingUser(String billingUser) {
		this.billingUser = billingUser;
	}
	public String getBillingPassword() {
		return billingPassword;
	}
	public void setBillingPassword(String billingPassword) {
		this.billingPassword = billingPassword;
	}
	public String getBillingBillerID() {
		return billingBillerID;
	}
	public void setBillingBillerID(String billingBillerID) {
		this.billingBillerID = billingBillerID;
	}
	public String getBillingProduct() {
		return billingProduct;
	}
	public void setBillingProduct(String billingProduct) {
		this.billingProduct = billingProduct;
	}
	public String getBillingChannel() {
		return billingChannel;
	}
	public void setBillingChannel(String billingChannel) {
		this.billingChannel = billingChannel;
	}
	public String getBillingBillerAccount() {
		return billingBillerAccount;
	}
	public void setBillingBillerAccount(String billingBillerAccount) {
		this.billingBillerAccount = billingBillerAccount;
	}
	public String getBillingBillerName() {
		return billingBillerName;
	}
	public void setBillingBillerName(String billingBillerName) {
		this.billingBillerName = billingBillerName;
	}
	public String getBillingRemark() {
		return billingRemark;
	}
	public void setBillingRemark(String billingRemark) {
		this.billingRemark = billingRemark;
	}
	public String getBillingClientAccount() {
		return billingClientAccount;
	}
	public void setBillingClientAccount(String billingClientAccount) {
		this.billingClientAccount = billingClientAccount;
	}
	public String getBillingClientID() {
		return billingClientID;
	}
	public void setBillingClientID(String billingClientID) {
		this.billingClientID = billingClientID;
	}
	public String getServerUrlAsso() {
		return serverUrlAsso;
	}
	public void setServerUrlAsso(String serverUrlAsso) {
		this.serverUrlAsso = serverUrlAsso;
	}
	public String getAssoUsername() {
		return assoUsername;
	}
	public void setAssoUsername(String assoUsername) {
		this.assoUsername = assoUsername;
	}
	public String getAssoPassword() {
		return assoPassword;
	}
	public void setAssoPassword(String assoPassword) {
		this.assoPassword = assoPassword;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getServerIpport() {
		return serverIpport;
	}
	public void setServerIpport(String serverIpport) {
		this.serverIpport = serverIpport;
	}
	public String getServerFileLocation() {
		return serverFileLocation;
	}
	public void setServerFileLocation(String serverFileLocation) {
		this.serverFileLocation = serverFileLocation;
	}
	public String getServerFileAccess() {
		return serverFileAccess;
	}
	public void setServerFileAccess(String serverFileAccess) {
		this.serverFileAccess = serverFileAccess;
	}
	public String getServerFileSeparator() {
		return serverFileSeparator;
	}
	public void setServerFileSeparator(String serverFileSeparator) {
		this.serverFileSeparator = serverFileSeparator;
	}
	
}
