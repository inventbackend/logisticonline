package model;

public class mdlDailyNews {
	public String newsID;
	public String startDateValid;
	public String endDateValid;
	public String vendorID;
	public String newsText;
	public String newsImage;

	public String getNewsID() {
		return newsID;
	}
	public void setNewsID(String newsID) {
		this.newsID = newsID;
	}
	public String getStartDateValid() {
		return startDateValid;
	}
	public void setStartDateValid(String startDateValid) {
		this.startDateValid = startDateValid;
	}
	public String getEndDateValid() {
		return endDateValid;
	}
	public void setEndDateValid(String endDateValid) {
		this.endDateValid = endDateValid;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public String getNewsText() {
		return newsText;
	}
	public void setNewsText(String newsText) {
		this.newsText = newsText;
	}
	public String getNewsImage() {
		return newsImage;
	}
	public void setNewsImage(String newsImage) {
		this.newsImage = newsImage;
	}
}
