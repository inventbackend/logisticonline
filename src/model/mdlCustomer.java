package model;

public class mdlCustomer {
	public String CustomerID;
	public String CustomerName;
	public String PIC1;
	public String PIC2;
	public String PIC3;
	public String CustomerAddress;
	public String Email;
	public String OfficePhone;
	public String MobilePhone;
	public String NPWP;
	public String Domicile;
	public String District;
	public String DistrictID;
	public String TDP;
	public String Username;
	public String Password;
	public String BillingAddress;
	public String TOP;
	public String TOPID;
	public Double CreditLimit;
	public int iCreditLimit;
	public String BankName;
	public String BankAcc;
	public String BankBranch;
	public String postalCode;
	public Boolean isFirstLogin;
	public int ppn;
	public int pph23;
	public String productInvoice;
	public int customClearance;
	
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getPIC1() {
		return PIC1;
	}
	public void setPIC1(String pIC1) {
		PIC1 = pIC1;
	}
	public String getPIC2() {
		return PIC2;
	}
	public void setPIC2(String pIC2) {
		PIC2 = pIC2;
	}
	public String getPIC3() {
		return PIC3;
	}
	public void setPIC3(String pIC3) {
		PIC3 = pIC3;
	}
	public String getCustomerAddress() {
		return CustomerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getOfficePhone() {
		return OfficePhone;
	}
	public void setOfficePhone(String officePhone) {
		OfficePhone = officePhone;
	}
	public String getMobilePhone() {
		return MobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		MobilePhone = mobilePhone;
	}
	public String getNPWP() {
		return NPWP;
	}
	public void setNPWP(String nPWP) {
		NPWP = nPWP;
	}
	public String getDomicile() {
		return Domicile;
	}
	public void setDomicile(String domicile) {
		Domicile = domicile;
	}
	public String getDistrict() {
		return District;
	}
	public void setDistrict(String district) {
		District = district;
	}
	public String getTDP() {
		return TDP;
	}
	public void setTDP(String tDP) {
		TDP = tDP;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getBillingAddress() {
		return BillingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		BillingAddress = billingAddress;
	}
	public String getTOP() {
		return TOP;
	}
	public void setTOP(String tOP) {
		TOP = tOP;
	}
	public Double getCreditLimit() {
		return CreditLimit;
	}
	public void setCreditLimit(Double creditLimit) {
		CreditLimit = creditLimit;
	}
	public int getiCreditLimit() {
		return iCreditLimit;
	}
	public void setiCreditLimit(int iCreditLimit) {
		this.iCreditLimit = iCreditLimit;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getBankAcc() {
		return BankAcc;
	}
	public void setBankAcc(String bankAcc) {
		BankAcc = bankAcc;
	}
	public String getBankBranch() {
		return BankBranch;
	}
	public void setBankBranch(String bankBranch) {
		BankBranch = bankBranch;
	}
	public String getDistrictID() {
		return DistrictID;
	}
	public void setDistrictID(String districtID) {
		DistrictID = districtID;
	}
	public String getTOPID() {
		return TOPID;
	}
	public void setTOPID(String tOPID) {
		TOPID = tOPID;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public Boolean getIsFirstLogin() {
		return isFirstLogin;
	}
	public void setIsFirstLogin(Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}
	public int getPpn() {
		return ppn;
	}
	public void setPpn(int ppn) {
		this.ppn = ppn;
	}
	public int getPph23() {
		return pph23;
	}
	public void setPph23(int pph23) {
		this.pph23 = pph23;
	}
	public String getProductInvoice() {
		return productInvoice;
	}
	public void setProductInvoice(String productInvoice) {
		this.productInvoice = productInvoice;
	}
	public int getCustomClearance() {
		return customClearance;
	}
	public void setCustomClearance(int customClearance) {
		this.customClearance = customClearance;
	}
	
	
	
}
