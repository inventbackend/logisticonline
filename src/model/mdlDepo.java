package model;

public class mdlDepo {
	public String depoID;
	public String name;
	public String address;

	public String getDepoID() {
		return depoID;
	}
	public void setDepoID(String depoID) {
		this.depoID = depoID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
