package model;

public class mdlVendorDriver {

	public String VendorID;
	public String VendorName;
	public String DriverID;
	public String DriverName;
	public String DeviceID;
	public String DriverPassword;
	public String DriverUsername;
	
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getDriverID() {
		return DriverID;
	}
	public void setDriverID(String driverID) {
		DriverID = driverID;
	}
	public String getDriverName() {
		return DriverName;
	}
	public void setDriverName(String driverName) {
		DriverName = driverName;
	}
	public String getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}
	public String getDriverPassword() {
		return DriverPassword;
	}
	public void setDriverPassword(String driverPassword) {
		DriverPassword = driverPassword;
	}
	public String getDriverUsername() {
		return DriverUsername;
	}
	public void setDriverUsername(String driverUsername) {
		DriverUsername = driverUsername;
	}
	
}
