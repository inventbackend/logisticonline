package model;

import java.util.ArrayList;
import java.util.List;

public class mdlInvoice {

	public String debitNoteNumber;
	public String customerID;
	public String customer;
	public String address;
	public String phone;
	public String email;
	public String orderID;
	public String invoiceDate;
	public String dueDate;
	public String siNumber;
	public Integer totalParty;
	public String containerQty;
	public String vesselVoyage;
	public String voyage;
	public String route;
	public String detailTruckingPrice;
	public Integer truckingPrice;
	public Integer customClearance;
	public Integer ppnCustom;
	public Integer subTotal;
	public Integer grandTotal;
	public List<String> containerNumber;
	public String stuffingDate;
	public String signatureName;
	public List<model.mdlDeliveryOrder> listDO = new ArrayList<mdlDeliveryOrder>();
	public Integer pph23;
	public Integer totalPPh23;
	public Integer ppn;
	public Integer totalPPN;
	public Integer additionalPrice;
	public List<mdlDebitNoteReimburseDetail> listReimburseDetail;
	public Integer totalReimburse;
	public Integer total;
	public String productInvoice;
	public Integer finalRate;
	
	//invoice config
	public String invoiceConfigID;
	public String invoiceConfigDesc;
	public String invoiceConfigValue;
	
	
	public String getDebitNoteNumber() {
		return debitNoteNumber;
	}
	public void setDebitNoteNumber(String debitNoteNumber) {
		this.debitNoteNumber = debitNoteNumber;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getSiNumber() {
		return siNumber;
	}
	public void setSiNumber(String siNumber) {
		this.siNumber = siNumber;
	}
	public String getContainerQty() {
		return containerQty;
	}
	public void setContainerQty(String containerQty) {
		this.containerQty = containerQty;
	}
	public String getVesselVoyage() {
		return vesselVoyage;
	}
	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}
	public String getVoyage() {
		return voyage;
	}
	public void setVoyage(String voyage) {
		this.voyage = voyage;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getDetailTruckingPrice() {
		return detailTruckingPrice;
	}
	public void setDetailTruckingPrice(String detailTruckingPrice) {
		this.detailTruckingPrice = detailTruckingPrice;
	}
	public Integer getTruckingPrice() {
		return truckingPrice;
	}
	public void setTruckingPrice(Integer truckingPrice) {
		this.truckingPrice = truckingPrice;
	}
	public Integer getCustomClearance() {
		return customClearance;
	}
	public void setCustomClearance(Integer customClearance) {
		this.customClearance = customClearance;
	}
	public Integer getPpnCustom() {
		return ppnCustom;
	}
	public void setPpnCustom(Integer ppnCustom) {
		this.ppnCustom = ppnCustom;
	}
	public Integer getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}
	public Integer getPph23() {
		return pph23;
	}
	public void setPph23(Integer pph23) {
		this.pph23 = pph23;
	}
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}
	public List<String> getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(List<String> containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public Integer getTotalParty() {
		return totalParty;
	}
	public void setTotalParty(Integer totalParty) {
		this.totalParty = totalParty;
	}
	public String getSignatureName() {
		return signatureName;
	}
	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}
	public List<model.mdlDeliveryOrder> getListDO() {
		return listDO;
	}
	public void setListDO(List<model.mdlDeliveryOrder> listDO) {
		this.listDO = listDO;
	}
	public Integer getTotalPPh23() {
		return totalPPh23;
	}
	public void setTotalPPh23(Integer totalPPh23) {
		this.totalPPh23 = totalPPh23;
	}
	
	
	
	public List<mdlDebitNoteReimburseDetail> getListReimburseDetail() {
		return listReimburseDetail;
	}
	public void setListReimburseDetail(List<mdlDebitNoteReimburseDetail> listReimburseDetail) {
		this.listReimburseDetail = listReimburseDetail;
	}
	public Integer getAdditionalPrice() {
		return additionalPrice;
	}
	public void setAdditionalPrice(Integer additionalPrice) {
		this.additionalPrice = additionalPrice;
	}
	public Integer getPpn() {
		return ppn;
	}
	public void setPpn(Integer ppn) {
		this.ppn = ppn;
	}
	public Integer getTotalPPN() {
		return totalPPN;
	}
	public void setTotalPPN(Integer totalPPN) {
		this.totalPPN = totalPPN;
	}
	public String getProductInvoice() {
		return productInvoice;
	}
	public void setProductInvoice(String productInvoice) {
		this.productInvoice = productInvoice;
	}
	public Integer getTotalReimburse() {
		return totalReimburse;
	}
	public void setTotalReimburse(Integer totalReimburse) {
		this.totalReimburse = totalReimburse;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}
	
	//invoice config kedepannya ga digunakan
	public String getInvoiceConfigID() {
		return invoiceConfigID;
	}
	public void setInvoiceConfigID(String invoiceConfigID) {
		this.invoiceConfigID = invoiceConfigID;
	}
	public String getInvoiceConfigDesc() {
		return invoiceConfigDesc;
	}
	public void setInvoiceConfigDesc(String invoiceConfigDesc) {
		this.invoiceConfigDesc = invoiceConfigDesc;
	}
	public String getInvoiceConfigValue() {
		return invoiceConfigValue;
	}
	public void setInvoiceConfigValue(String invoiceConfigValue) {
		this.invoiceConfigValue = invoiceConfigValue;
	}
	
	
	
}
