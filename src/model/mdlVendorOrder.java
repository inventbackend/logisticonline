package model;

public class mdlVendorOrder {
	public String vendorOrderID;
	public String shippingInvoiceID;
	public String deliveryOrderID;
	public String orderManagementID;
	public String orderManagementDetailID;
	public String customer;
	public String factory;
	public String address;
	public String district;
	public String depo;
	public String port;
	public String shippingLine;
	public String orderStuffingDate;
	public String orderType;
	public String item;
	public String vendorID;
	public String vendorName;
	public String vendorStatus;
	public Integer quantity;
	public Integer quantityDelivered;
	public Integer price;
	public String createdBy;
	public String createdDate;
	public String updatedBy;
	public String updatedDate;
	public String containerType;
	
	public String getVendorOrderID() {
		return vendorOrderID;
	}
	public void setVendorOrderID(String vendorOrderID) {
		this.vendorOrderID = vendorOrderID;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getOrderManagementDetailID() {
		return orderManagementDetailID;
	}
	public void setOrderManagementDetailID(String orderManagementDetailID) {
		this.orderManagementDetailID = orderManagementDetailID;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorStatus() {
		return vendorStatus;
	}
	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getQuantityDelivered() {
		return quantityDelivered;
	}
	public void setQuantityDelivered(Integer quantityDelivered) {
		this.quantityDelivered = quantityDelivered;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getOrderStuffingDate() {
		return orderStuffingDate;
	}
	public void setOrderStuffingDate(String orderStuffingDate) {
		this.orderStuffingDate = orderStuffingDate;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getDepo() {
		return depo;
	}
	public void setDepo(String depo) {
		this.depo = depo;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getShippingInvoiceID() {
		return shippingInvoiceID;
	}
	public void setShippingInvoiceID(String shippingInvoiceID) {
		this.shippingInvoiceID = shippingInvoiceID;
	}
	public String getShippingLine() {
		return shippingLine;
	}
	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public String getContainerType() {
		return containerType;
	}
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}
	public String getDeliveryOrderID() {
		return deliveryOrderID;
	}
	public void setDeliveryOrderID(String deliveryOrderID) {
		this.deliveryOrderID = deliveryOrderID;
	}

}
