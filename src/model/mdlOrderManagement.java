package model;

public class mdlOrderManagement {
	String orderManagementID;
	String shippingInvoiceID;
	String deliveryOrderID;
	String customerID;
	String customerName;
	String factoryID;
	String factoryName;
	String factoryAddress;
	String district;
	String orderStatus;
	String orderType;
	String itemDescription;
	String tier;
	String tierDescription;
	String portID;
	String portName;
	String portUTC;
	String shippingID;
	String shippingName;
	String depoID;
	String depoName;
	Integer totalQuantity;
	Integer totalPrice;
	Integer totalQuantityDelivered;
	Integer totalPriceDelivered;
	String createdBy;
	String updatedBy;
	String orderImage;
	String peb;
	String npe;
	String npeDate;
	String npeDoc;
	String yellowCard;
	String voyageNo;
	String vesselName;
	String[] vendor;
	String pod;
	Integer itemWeight;
	String voyageCo;
	String tpsTransactionID;
	String tpsTransactionDate;
	String tpsApprovalCode;
	String tpsApprovalDate;
	String tpsProforma;
	String tpsPaymentID;
	Integer tpsBillingAmount;
	Integer tpsBillingStatus;
	String tpsInvoiceNo;
	String tpsLinkInvoice;
	String shippingDate;
	Integer voyageExportLimit;
	String voyageClosingDoc;
	String npwp;
	public String orderStuffingDate;
	public String containerTypeDescription;

	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public String getPortUTC() {
		return portUTC;
	}
	public void setPortUTC(String portUTC) {
		this.portUTC = portUTC;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getShippingName() {
		return shippingName;
	}
	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}
	public String getDepoName() {
		return depoName;
	}
	public void setDepoName(String depoName) {
		this.depoName = depoName;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getShippingInvoiceID() {
		return shippingInvoiceID;
	}
	public void setShippingInvoiceID(String shippingInvoiceID) {
		this.shippingInvoiceID = shippingInvoiceID;
	}
	public String getDeliveryOrderID() {
		return deliveryOrderID;
	}
	public void setDeliveryOrderID(String deliveryOrderID) {
		this.deliveryOrderID = deliveryOrderID;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getFactoryID() {
		return factoryID;
	}
	public void setFactoryID(String factoryID) {
		this.factoryID = factoryID;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	public String getShippingID() {
		return shippingID;
	}
	public void setShippingID(String shippingID) {
		this.shippingID = shippingID;
	}
	public String getDepoID() {
		return depoID;
	}
	public void setDepoID(String depoID) {
		this.depoID = depoID;
	}
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Integer getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getTotalQuantityDelivered() {
		return totalQuantityDelivered;
	}
	public void setTotalQuantityDelivered(Integer totalQuantityDelivered) {
		this.totalQuantityDelivered = totalQuantityDelivered;
	}
	public Integer getTotalPriceDelivered() {
		return totalPriceDelivered;
	}
	public void setTotalPriceDelivered(Integer totalPriceDelivered) {
		this.totalPriceDelivered = totalPriceDelivered;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public String getTier() {
		return tier;
	}
	public void setTier(String tier) {
		this.tier = tier;
	}
	public String getTierDescription() {
		return tierDescription;
	}
	public void setTierDescription(String tierDescription) {
		this.tierDescription = tierDescription;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getOrderImage() {
		return orderImage;
	}
	public void setOrderImage(String orderImage) {
		this.orderImage = orderImage;
	}
	public String getPeb() {
		return peb;
	}
	public void setPeb(String peb) {
		this.peb = peb;
	}
	public String getNpe() {
		return npe;
	}
	public void setNpe(String npe) {
		this.npe = npe;
	}
	public String getYellowCard() {
		return yellowCard;
	}
	public void setYellowCard(String yellowCard) {
		this.yellowCard = yellowCard;
	}
	public String getFactoryAddress() {
		return factoryAddress;
	}
	public void setFactoryAddress(String factoryAddress) {
		this.factoryAddress = factoryAddress;
	}
	public String getVoyageNo() {
		return voyageNo;
	}
	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	public String getVesselName() {
		return vesselName;
	}
	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
	public String[] getVendor() {
		return vendor;
	}
	public void setVendor(String[] vendor) {
		this.vendor = vendor;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public Integer getItemWeight() {
		return itemWeight;
	}
	public void setItemWeight(Integer itemWeight) {
		this.itemWeight = itemWeight;
	}
	public String getVoyageCo() {
		return voyageCo;
	}
	public void setVoyageCo(String voyageCo) {
		this.voyageCo = voyageCo;
	}
	public String getTpsTransactionID() {
		return tpsTransactionID;
	}
	public void setTpsTransactionID(String tpsTransactionID) {
		this.tpsTransactionID = tpsTransactionID;
	}
	public String getTpsProforma() {
		return tpsProforma;
	}
	public void setTpsProforma(String tpsProforma) {
		this.tpsProforma = tpsProforma;
	}
	public Integer getTpsBillingAmount() {
		return tpsBillingAmount;
	}
	public void setTpsBillingAmount(Integer tpsBillingAmount) {
		this.tpsBillingAmount = tpsBillingAmount;
	}
	public Integer getTpsBillingStatus() {
		return tpsBillingStatus;
	}
	public void setTpsBillingStatus(Integer tpsBillingStatus) {
		this.tpsBillingStatus = tpsBillingStatus;
	}
	public String getShippingDate() {
		return shippingDate;
	}
	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}
	public String getNpeDate() {
		return npeDate;
	}
	public void setNpeDate(String npeDate) {
		this.npeDate = npeDate;
	}
	public String getTpsTransactionDate() {
		return tpsTransactionDate;
	}
	public void setTpsTransactionDate(String tpsTransactionDate) {
		this.tpsTransactionDate = tpsTransactionDate;
	}
	public String getTpsApprovalCode() {
		return tpsApprovalCode;
	}
	public void setTpsApprovalCode(String tpsApprovalCode) {
		this.tpsApprovalCode = tpsApprovalCode;
	}
	public String getTpsApprovalDate() {
		return tpsApprovalDate;
	}
	public void setTpsApprovalDate(String tpsApprovalDate) {
		this.tpsApprovalDate = tpsApprovalDate;
	}
	public String getTpsPaymentID() {
		return tpsPaymentID;
	}
	public void setTpsPaymentID(String tpsPaymentID) {
		this.tpsPaymentID = tpsPaymentID;
	}
	public String getTpsInvoiceNo() {
		return tpsInvoiceNo;
	}
	public void setTpsInvoiceNo(String tpsInvoiceNo) {
		this.tpsInvoiceNo = tpsInvoiceNo;
	}
	public String getOrderStuffingDate() {
		return orderStuffingDate;
	}
	public void setOrderStuffingDate(String orderStuffingDate) {
		this.orderStuffingDate = orderStuffingDate;
	}
	public String getContainerTypeDescription() {
		return containerTypeDescription;
	}
	public void setContainerTypeDescription(String containerTypeDescription) {
		this.containerTypeDescription = containerTypeDescription;
	}
	public Integer getVoyageExportLimit() {
		return voyageExportLimit;
	}
	public void setVoyageExportLimit(Integer voyageExportLimit) {
		this.voyageExportLimit = voyageExportLimit;
	}
	public String getVoyageClosingDoc() {
		return voyageClosingDoc;
	}
	public void setVoyageClosingDoc(String voyageClosingDoc) {
		this.voyageClosingDoc = voyageClosingDoc;
	}
	public String getTpsLinkInvoice() {
		return tpsLinkInvoice;
	}
	public void setTpsLinkInvoice(String tpsLinkInvoice) {
		this.tpsLinkInvoice = tpsLinkInvoice;
	}
	public String getNpeDoc() {
		return npeDoc;
	}
	public void setNpeDoc(String npeDoc) {
		this.npeDoc = npeDoc;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	
}
