package model;

public class mdlVendorTier {

	public String VendorID;
	public String VendorName;
	public String TierID;
	public String TierName;
	
	public String getVendorID() {
		return VendorID;
	}
	public void setVendorID(String vendorID) {
		VendorID = vendorID;
	}
	public String getTierID() {
		return TierID;
	}
	public void setTierID(String tierID) {
		TierID = tierID;
	}
	public String getVendorName() {
		return VendorName;
	}
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}
	public String getTierName() {
		return TierName;
	}
	public void setTierName(String tierName) {
		TierName = tierName;
	}
	
}
