package model;

import java.util.List;

public class mdlOrderManagementDetail {
	String orderManagementDetailID;
	String orderManagementID;
	String Customer;
	String factory;
	String District;
	String Depo;
	String Port;
	String stuffingDate;
	String tierID;
	String tierDescription;
	String containerTypeID;
	String containerTypeDescription;
	String itemDescription;
	String orderStatus;
	Integer quantity;
	Integer ratePrice;
	Integer price;
	Integer quantityDelivered;
	Integer priceDelivered;
	String createdBy;
	String updatedBy;
	public List<mdlVendorOrderDetail> vendorOrder;
	String si;

	public String getOrderManagementDetailID() {
		return orderManagementDetailID;
	}
	public void setOrderManagementDetailID(String orderManagementDetailID) {
		this.orderManagementDetailID = orderManagementDetailID;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getTierID() {
		return tierID;
	}
	public void setTierID(String tierID) {
		this.tierID = tierID;
	}
	public String getContainerTypeID() {
		return containerTypeID;
	}
	public void setContainerTypeID(String containerTypeID) {
		this.containerTypeID = containerTypeID;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Integer getQuantityDelivered() {
		return quantityDelivered;
	}
	public void setQuantityDelivered(Integer quantityDelivered) {
		this.quantityDelivered = quantityDelivered;
	}
	public Integer getPriceDelivered() {
		return priceDelivered;
	}
	public void setPriceDelivered(Integer priceDelivered) {
		this.priceDelivered = priceDelivered;
	}
	public String getTierDescription() {
		return tierDescription;
	}
	public void setTierDescription(String tierDescription) {
		this.tierDescription = tierDescription;
	}
	public String getContainerTypeDescription() {
		return containerTypeDescription;
	}
	public void setContainerTypeDescription(String containerTypeDescription) {
		this.containerTypeDescription = containerTypeDescription;
	}
	public Integer getRatePrice() {
		return ratePrice;
	}
	public void setRatePrice(Integer ratePrice) {
		this.ratePrice = ratePrice;
	}
	public String getCustomer() {
		return Customer;
	}
	public void setCustomer(String customer) {
		Customer = customer;
	}
	public String getDistrict() {
		return District;
	}
	public void setDistrict(String district) {
		District = district;
	}
	public String getDepo() {
		return Depo;
	}
	public void setDepo(String depo) {
		Depo = depo;
	}
	public String getPort() {
		return Port;
	}
	public void setPort(String port) {
		Port = port;
	}
	public List<mdlVendorOrderDetail> getVendorOrder() {
		return vendorOrder;
	}
	public void setVendorOrder(List<mdlVendorOrderDetail> vendorOrder) {
		this.vendorOrder = vendorOrder;
	}
	public String getSi() {
		return si;
	}
	public void setSi(String si) {
		this.si = si;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	
	
}
