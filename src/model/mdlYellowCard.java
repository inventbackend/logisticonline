package model;

import java.util.List;

public class mdlYellowCard {

	public String shippingInstruction;
	public Boolean status;
	public String desc;
	public List<mdlVendorOrderDetail> yellowCard;
	
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public List<mdlVendorOrderDetail> getYellowCard() {
		return yellowCard;
	}
	public void setYellowCard(List<mdlVendorOrderDetail> yellowCard) {
		this.yellowCard = yellowCard;
	}
}
