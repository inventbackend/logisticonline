package model;

public class mdlTOP {
	public String topID;
	public String name;
	public Integer duration;

	public String getTopID() {
		return topID;
	}
	public void setTopID(String topID) {
		this.topID = topID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

}
