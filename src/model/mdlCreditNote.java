package model;

import java.util.ArrayList;
import java.util.List;

public class mdlCreditNote {

	public String creditNoteNumber;
	public String vendorID;
	public String vendorName;
	public String orderManagementID;
	public String date;
	public Integer amount;
	public String status;
	public String paymentDate;
	public Integer amountPaid;
	
	//for print
	public String address;
	public String phone;
	public String email;
	public String siNumber;
	public String containerQty;
	public String vesselVoyage;
	public String route;
	public String detailTruckingPrice;
	public Integer truckingPrice;
	public Integer pph23;
	public Integer grandTotal;
	public List<String> containerNumber;
	public String stuffingDate;
	public String signatureName;
	public List<model.mdlVendorOrderDetail> listDO = new ArrayList<mdlVendorOrderDetail>();
	
	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}
	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getOrderManagementID() {
		return orderManagementID;
	}
	public void setOrderManagementID(String orderManagementID) {
		this.orderManagementID = orderManagementID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Integer getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(Integer amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	
	//for print
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSiNumber() {
		return siNumber;
	}
	public void setSiNumber(String siNumber) {
		this.siNumber = siNumber;
	}
	public String getContainerQty() {
		return containerQty;
	}
	public void setContainerQty(String containerQty) {
		this.containerQty = containerQty;
	}
	public String getVesselVoyage() {
		return vesselVoyage;
	}
	public void setVesselVoyage(String vesselVoyage) {
		this.vesselVoyage = vesselVoyage;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getDetailTruckingPrice() {
		return detailTruckingPrice;
	}
	public void setDetailTruckingPrice(String detailTruckingPrice) {
		this.detailTruckingPrice = detailTruckingPrice;
	}
	public Integer getTruckingPrice() {
		return truckingPrice;
	}
	public void setTruckingPrice(Integer truckingPrice) {
		this.truckingPrice = truckingPrice;
	}
	public Integer getPph23() {
		return pph23;
	}
	public void setPph23(Integer pph23) {
		this.pph23 = pph23;
	}
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}
	public List<String> getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(List<String> containerNumber) {
		this.containerNumber = containerNumber;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getSignatureName() {
		return signatureName;
	}
	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}
	public List<model.mdlVendorOrderDetail> getListDO() {
		return listDO;
	}
	public void setListDO(List<model.mdlVendorOrderDetail> listDO) {
		this.listDO = listDO;
	}
	
}
