package model;

public class mdlFactory {

	public String CustomerID;
	public String FactoryID;
	public String FactoryName;
	public String pic;
	public String officePhone;
	public String mobilePhone;
	public String province;
	public String Address;
	public String DistrictID;
	public String DistrictName;
	public String TierID;
	public String TierName;
	
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getFactoryID() {
		return FactoryID;
	}
	public void setFactoryID(String factoryID) {
		FactoryID = factoryID;
	}
	public String getFactoryName() {
		return FactoryName;
	}
	public void setFactoryName(String factoryName) {
		FactoryName = factoryName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getDistrictID() {
		return DistrictID;
	}
	public void setDistrictID(String districtID) {
		DistrictID = districtID;
	}
	public String getDistrictName() {
		return DistrictName;
	}
	public void setDistrictName(String districtName) {
		DistrictName = districtName;
	}
	public String getTierID() {
		return TierID;
	}
	public void setTierID(String tierID) {
		TierID = tierID;
	}
	public String getTierName() {
		return TierName;
	}
	public void setTierName(String tierName) {
		TierName = tierName;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
}
