package model;

public class mdlPort {
	public String portID;
	public String portName;
	public String portUTC;
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public String getPortUTC() {
		return portUTC;
	}
	public void setPortUTC(String portUTC) {
		this.portUTC = portUTC;
	}

}
