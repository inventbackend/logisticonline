package model;

import java.util.List;

public class mdlSendBillingData {

	public Integer issuerId;
	public String billingId;
	public String paymentId;
	public String vaid;
	public String billingDate;
	public Integer billingType;
	public String customerId;
	public String customerName;
	public String customerAddress;
	public String customerMail;
	public String customerPhone;
	public String billerAccountNo;
	public String billerName;
	public String currency;
	public Integer amount;
	public Integer tax;
	public Integer totalAmount;
	public String expiredDate;
	public Integer interval;
	public String remark;
	public String signature;
	public String documentNo;
	public String bankClientId;
	public String accountNo;
	public String journalNo;
	public String journalDate;
	public String accountName;
	public List<mdlSendBillingDataDetail> detail;
	
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	public String getBillingDate() {
		return billingDate;
	}
	public void setBillingDate(String billingDate) {
		this.billingDate = billingDate;
	}
	public Integer getBillingType() {
		return billingType;
	}
	public void setBillingType(Integer billingType) {
		this.billingType = billingType;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerMail() {
		return customerMail;
	}
	public void setCustomerMail(String customerMail) {
		this.customerMail = customerMail;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getBillerAccountNo() {
		return billerAccountNo;
	}
	public void setBillerAccountNo(String billerAccountNo) {
		this.billerAccountNo = billerAccountNo;
	}
	public String getBillerName() {
		return billerName;
	}
	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getTax() {
		return tax;
	}
	public void setTax(Integer tax) {
		this.tax = tax;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public Integer getInterval() {
		return interval;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public List<mdlSendBillingDataDetail> getDetail() {
		return detail;
	}
	public void setDetail(List<mdlSendBillingDataDetail> detail) {
		this.detail = detail;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getVaid() {
		return vaid;
	}
	public void setVaid(String vaid) {
		this.vaid = vaid;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getBankClientId() {
		return bankClientId;
	}
	public void setBankClientId(String bankClientId) {
		this.bankClientId = bankClientId;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getJournalNo() {
		return journalNo;
	}
	public void setJournalNo(String journalNo) {
		this.journalNo = journalNo;
	}
	public String getJournalDate() {
		return journalDate;
	}
	public void setJournalDate(String journalDate) {
		this.journalDate = journalDate;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
}
