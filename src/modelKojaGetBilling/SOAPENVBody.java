
package modelKojaGetBilling;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:BILLING_GetBillingResponse")
    @Expose
    private Ns1BILLINGGetBillingResponse ns1BILLINGGetBillingResponse;

    public Ns1BILLINGGetBillingResponse getNs1BILLINGGetBillingResponse() {
        return ns1BILLINGGetBillingResponse;
    }

    public void setNs1BILLINGGetBillingResponse(Ns1BILLINGGetBillingResponse ns1BILLINGGetBillingResponse) {
        this.ns1BILLINGGetBillingResponse = ns1BILLINGGetBillingResponse;
    }

}
