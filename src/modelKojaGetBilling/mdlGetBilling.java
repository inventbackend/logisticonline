package modelKojaGetBilling;

public class mdlGetBilling {

	public String STATUS;
	public String MESSAGE;
	public String PROFORMA_INVOICE_NO;
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String getPROFORMA_INVOICE_NO() {
		return PROFORMA_INVOICE_NO;
	}
	public void setPROFORMA_INVOICE_NO(String pROFORMA_INVOICE_NO) {
		PROFORMA_INVOICE_NO = pROFORMA_INVOICE_NO;
	}
	
}
