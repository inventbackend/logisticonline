package modelKojaGetOwner;

public class mdlOwner {

	public String[] OWNER;
	public String STATUS;
	public String TerminalID;

	public String[] getOWNER() {
		return OWNER;
	}

	public void setOWNER(String[] oWNER) {
		OWNER = oWNER;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getTerminalID() {
		return TerminalID;
	}

	public void setTerminalID(String terminalID) {
		TerminalID = terminalID;
	}
	
}
