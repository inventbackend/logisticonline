
package modelKojaGetOwner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:MAIN_GetOwnerResponse")
    @Expose
    private Ns1MAINGetOwnerResponse ns1MAINGetOwnerResponse;

    public Ns1MAINGetOwnerResponse getNs1MAINGetOwnerResponse() {
        return ns1MAINGetOwnerResponse;
    }

    public void setNs1MAINGetOwnerResponse(Ns1MAINGetOwnerResponse ns1MAINGetOwnerResponse) {
        this.ns1MAINGetOwnerResponse = ns1MAINGetOwnerResponse;
    }

}
