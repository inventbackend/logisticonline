package modelKojaAmmendAssociate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

    @SerializedName("ns2:ammendAssociateResponse")
    @Expose
    private Ns2AmmendAssociateResponse ns2AmmendAssociateResponse;

    public Ns2AmmendAssociateResponse getNs2AmmentAssociateResponse() {
        return ns2AmmendAssociateResponse;
    }

    public void setNs2AmmendAssociateResponse(Ns2AmmendAssociateResponse ns2AmmendAssociateResponse) {
        this.ns2AmmendAssociateResponse = ns2AmmendAssociateResponse;
    }

}
