package modelKojaAmmendAssociate;

import java.util.List;

public class mdlData {

	public String MESSAGE;
	public List<mdlAmmendAssociatedData> AMMEND_DATA;
	
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public List<mdlAmmendAssociatedData> getAMMEND_DATA() {
		return AMMEND_DATA;
	}
	public void setAMMEND_DATA(List<mdlAmmendAssociatedData> aMMEND_DATA) {
		AMMEND_DATA = aMMEND_DATA;
	}
	
}
