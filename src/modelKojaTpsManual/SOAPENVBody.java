
package modelKojaTpsManual;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:CUSTOMS_SaveDocumentTPSOnlineResponse")
    @Expose
    private Ns1CUSTOMSSaveDocumentTPSOnlineResponse ns1CUSTOMSSaveDocumentTPSOnlineResponse;

    public Ns1CUSTOMSSaveDocumentTPSOnlineResponse getNs1CUSTOMSSaveDocumentTPSOnlineResponse() {
        return ns1CUSTOMSSaveDocumentTPSOnlineResponse;
    }

    public void setNs1CUSTOMSSaveDocumentTPSOnlineResponse(Ns1CUSTOMSSaveDocumentTPSOnlineResponse ns1CUSTOMSSaveDocumentTPSOnlineResponse) {
        this.ns1CUSTOMSSaveDocumentTPSOnlineResponse = ns1CUSTOMSSaveDocumentTPSOnlineResponse;
    }

}
