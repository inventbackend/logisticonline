package modelKojaGetCustomer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:MAIN_GetCustomersNewResponse")
    @Expose
    private Ns1MAIN_GetCustomersNewResponse ns1MAIN_GetCustomersNewResponse;

    public Ns1MAIN_GetCustomersNewResponse getNs1MAIN_GetCustomersNewResponse() {
        return ns1MAIN_GetCustomersNewResponse;
    }

    public void setNs1MAIN_GetCustomersNewResponse(Ns1MAIN_GetCustomersNewResponse ns1MAIN_GetCustomersNewResponse) {
        this.ns1MAIN_GetCustomersNewResponse = ns1MAIN_GetCustomersNewResponse;
    }

}
