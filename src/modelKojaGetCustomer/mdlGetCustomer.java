package modelKojaGetCustomer;

import java.util.List;

public class mdlGetCustomer {

	public String STATUS;
	public String MESSAGE;
	public List<String> ID;
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public List<String> getID() {
		return ID;
	}
	public void setID(List<String> iD) {
		ID = iD;
	}
	
}
