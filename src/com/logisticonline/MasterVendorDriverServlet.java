package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/VendorDriver"} , name="VendorDriver")
public class MasterVendorDriverServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public MasterVendorDriverServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, sessionRole, result, resultDesc = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "VendorDriver";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		
    		if(sessionRole.contentEquals("R001")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorDriver> vendorDriverList = new ArrayList<model.mdlVendorDriver>();
    			vendorDriverList.addAll(VendorAdapter.LoadVendorDriver(sessionUser));
        		request.setAttribute("listVendorDriver", vendorDriverList);
        		
        		request.setAttribute("vendorID", "");
    		}
    		else if(sessionRole.contentEquals("R003")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorDriver> vendorDriverList = new ArrayList<model.mdlVendorDriver>();
    			vendorDriverList.addAll(VendorAdapter.LoadVendorDriverByVendor(sessionUser));
        		request.setAttribute("listVendorDriver", vendorDriverList);
        		
        		request.setAttribute("vendorID", sessionUser);
    		}
    		else{
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorDriver> vendorDriverList = new ArrayList<model.mdlVendorDriver>();
        		request.setAttribute("listVendorDriver", vendorDriverList);
        		ButtonStatus = "disabled";
        		
        		request.setAttribute("vendorID", "");
    		}

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("userRole", sessionRole);
    		
    		RequestDispatcher dispacther;
			dispacther = request.getRequestDispatcher("/mainform/pages/master_vendor_driver.jsp");
			dispacther.forward(request, response);
    	}
    	
    	result = "";
		resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

		if(!keyBtn.equals("Delete")) {
			//Declare mdlVendorDriver for global
			model.mdlVendorDriver mdlVendorDriver = new model.mdlVendorDriver();
			mdlVendorDriver.setVendorID(request.getParameter("txtVendorID"));
			mdlVendorDriver.setDriverName(request.getParameter("txtDriverName"));
			mdlVendorDriver.setDeviceID(request.getParameter("txtDeviceID"));
			mdlVendorDriver.setDriverUsername(request.getParameter("txtDriverUsername"));
			mdlVendorDriver.setDriverPassword(request.getParameter("txtDriverPassword"));
			
			if (keyBtn.equals("save")){
				String lResult = VendorAdapter.InsertVendorDriver(mdlVendorDriver,sessionUser);
				if(lResult.contains("Success Insert Vendor Driver")) {
					result = "SuccessInsertVendorDriver";
	            }
	            else {
	            	result = "FailedInsertVendorDriver";
	            	resultDesc = lResult;
	            }
			}
			
			if (keyBtn.equals("update")){
				mdlVendorDriver.setDriverID(request.getParameter("txtDriverID"));
			    String lResult = VendorAdapter.UpdateVendorDriver(mdlVendorDriver,sessionUser);

			    if(lResult.contains("Success Update Vendor Driver")) {
			    	result = "SuccessUpdateVendorDriver";
	            }
	            else {
	            	result = "FailedUpdateVendorDriver";
	            	resultDesc = lResult;
	            }
			}
		}
		else{
			//delete process
	    	//Declare TextBox
	    	String temp_txtVendorID = request.getParameter("temp_txtVendorID");
	    	if(sessionUser.equals(temp_txtVendorID) || sessionRole.equals("R001")){
	    		String temp_txtDriverID = request.getParameter("temp_txtDriverID");
    			
				String lResult = VendorAdapter.DeleteVendorDriver(temp_txtVendorID, temp_txtDriverID,sessionUser);
				if(lResult.contains("Success Delete Vendor Driver")) {
					result =  "SuccessDeleteVendorDriver";
	            }
	            else {
	            	result = "FailedDeleteVendorDriver";
	            	resultDesc = lResult;
	            }
	    	}
	    	else{
	    		result = "FailedDeleteVendorDriver";
            	resultDesc = "Processed not authorized";
	    	}
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
	
}
