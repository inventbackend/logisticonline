package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.FactoryAdapter;
import adapter.VendorAdapter;

@WebServlet(urlPatterns={"/getUser"} , name="getUser")
public class GetUserServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public GetUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String lRoleID = request.getParameter("roleid");
    	if(lRoleID.contentEquals("R002")){
    		List<model.mdlCustomer> customerList = new ArrayList<model.mdlCustomer>();
    		customerList.addAll(CustomerAdapter.LoadUnregisteredCustomer(sessionUser));
    		request.setAttribute("listUser", customerList);
    	}
    	else if(lRoleID.contentEquals("R003")){
    		//seharusnya pakai mdlVendor, tapi di sini pakai mdlCustomer biar seragamin dan gampang untuk parse ke jspnya
    		List<model.mdlCustomer> vendorList = new ArrayList<model.mdlCustomer>();
    		vendorList.addAll(VendorAdapter.LoadUnregisteredVendor(sessionUser));
    		request.setAttribute("listUser", vendorList);
    	}

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getuser.jsp");
		dispacther.forward(request, response);
	}
}
