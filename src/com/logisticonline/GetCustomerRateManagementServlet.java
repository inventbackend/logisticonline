package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import adapter.RateAdapter;

@WebServlet(urlPatterns={"/getCustomerRateManagement"} , name="getCustomerRateManagement")
public class GetCustomerRateManagementServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public GetCustomerRateManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String lCustomerID = request.getParameter("customerid");
    	String lFactoryID = request.getParameter("factoryid");
    	String lDistrictID = request.getParameter("districtid");
    	
    	List<model.mdlRate> rateList = new ArrayList<model.mdlRate>();
    	rateList.addAll(RateAdapter.LoadCustomerFinalRate(lCustomerID, lFactoryID, lDistrictID, sessionUser));
		request.setAttribute("listRate", rateList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getCustomerRateManagement.jsp");
		dispacther.forward(request, response);
	}
    
}
