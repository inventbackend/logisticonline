package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import adapter.VendorAdapter;
import model.Globals;
import model.mdlVendor;

@WebServlet(urlPatterns={"/VendorTier"} , name="VendorTier")
public class MasterVendorTierServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public MasterVendorTierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String condition,VendorID,VendorName, result, resultDesc, tempVendorID, tempVendorName, sessionUser;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	VendorID = request.getParameter("vendorid");
    	VendorName = request.getParameter("vendorname");
    	condition = request.getParameter("condition");
    	
    	if (VendorID == null)
    	{
    		VendorID = tempVendorID;
    		VendorName = tempVendorName;
    		condition = result;
    	}
    	
    	if(VendorID == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Vendor");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		
		List<model.mdlVendorTier> mdlVendorTierList = new ArrayList<model.mdlVendorTier>();
		mdlVendorTierList.addAll(VendorAdapter.LoadVendorTierByVendor(VendorID,sessionUser));
		request.setAttribute("listVendorTier", mdlVendorTierList);

		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(adapter.TierAdapter.LoadTier(sessionUser));
		request.setAttribute("listTier", tierList);
		
		request.setAttribute("condition", condition);
		request.setAttribute("conditionDescription", resultDesc);
		request.setAttribute("temp_vendorName", VendorName);
		request.setAttribute("temp_vendorID", VendorID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_vendor_tier.jsp");
		dispacther.forward(request, response);
		
		result = "";
		condition = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String temp_txtVendorID = request.getParameter("temp_txtVendorID");
		String temp_slTier = request.getParameter("temp_slTier");

		//Declare mdlVendorTier for global
		model.mdlVendorTier mdlVendorTier = new model.mdlVendorTier();
		mdlVendorTier.setVendorID(request.getParameter("txtVendorID"));
		mdlVendorTier.setTierID(request.getParameter("slTier"));

		if (keyBtn.equals("save")){
			String lResult = VendorAdapter.InsertVendorTier(mdlVendorTier,sessionUser);
			if(lResult.contains("Success Insert Vendor Tier")) {
				result = "SuccessInsertVendorTier";
            }
            else {
            	result = "FailedInsertVendorTier";
            	resultDesc = lResult;
            }
		}
		
		if (keyBtn.equals("Delete")){
			String lResult = VendorAdapter.DeleteVendorTier(temp_txtVendorID, temp_slTier, sessionUser);
			if(lResult.contains("Success Delete Vendor Tier")) {
				result =  "SuccessDeleteVendorTier";
            }
            else {
            	result = "FailedDeleteVendorTier";
            	resultDesc = lResult;
            }
		}
		
		tempVendorID = VendorID;
		tempVendorName = VendorName;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
    }
    
}
