package com.logisticonline;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.RateAdapter;

@WebServlet(urlPatterns={"/VendorRate"} , name="VendorRate")
public class VendorRateServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	public VendorRateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, result, resultDescription = "";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	//save or update customer final rate proccess
		model.mdlRate rate = new model.mdlRate();
		rate.setVendorID(request.getParameter("txtVendorID"));
		rate.setContainerTypeID(request.getParameter("slContainerType"));
		rate.setDistrictID(request.getParameter("slDistrict"));
		String tempFinalPrice = request.getParameter("txtFinalRate").replace(".", "");
        Integer txtFinalPrice = tempFinalPrice.equals("") ? 0 : Integer.parseInt(tempFinalPrice);
		rate.setFinalPrice(txtFinalPrice);
		rate.setTierID("TIER-0001");

		if(keyBtn.equals("saveFinalRate") ){
			String lResult = RateAdapter.InsertVendorFinalRate(rate,sessionUser);
			if(lResult.contains("Success Insert Final Rate")) {
				result = "SuccessInsertFinalRate";
            }
            else {
            	result = "FailedInsertFinalRate";
            	resultDescription = lResult;
            }
		}
			
		if (keyBtn.equals("updateFinalRate")){
		    String lResult = RateAdapter.UpdateVendorFinalRate(rate,sessionUser);
		    if(lResult.contains("Success Update Final Rate")) {
		    	result = "SuccessUpdateFinalRate";
            }
            else {
            	result = "FailedUpdateFinalRate";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
	
}
