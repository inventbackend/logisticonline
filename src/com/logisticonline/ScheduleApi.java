package com.logisticonline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;

import adapter.SchedulerAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlKojaParam;

@SuppressWarnings("serial")
public class ScheduleApi extends HttpServlet {
	
	
	public void init() throws ServletException
    {
        System.out.println("---------- Logistic Online Started ----------");
  		
        mdlKojaParam kojaParam = new mdlKojaParam();
  		kojaParam.serviceUrl = getServletContext().getInitParameter("koja_url");
  		kojaParam.username = getServletContext().getInitParameter("koja_username");
  		kojaParam.password = getServletContext().getInitParameter("koja_password");
  		kojaParam.fstreamusername = getServletContext().getInitParameter("koja_fstream_username");
  		kojaParam.fstreampassword = getServletContext().getInitParameter("koja_fstream_password");
  		kojaParam.deviceName = getServletContext().getInitParameter("koja_devicename");
  		kojaParam.serverUrlAsso = getServletContext().getInitParameter("asso_url");
  		kojaParam.assoUsername = getServletContext().getInitParameter("asso_username");
  		kojaParam.assoPassword = getServletContext().getInitParameter("asso_password");
  		kojaParam.action = getServletContext().getInitParameter("koja_service_action");
  		/*APIAdapter.KojaGetVesselVoyage(kojaParam);*/
  		SchedulerAdapter.Scheduler(kojaParam);
  		SchedulerAdapter.SchedulerContainerTracking(kojaParam);
  		
  		//scheduler untuk get eir
  		kojaParam.serverFileSeparator = getServletContext().getInitParameter("param_separator");
		kojaParam.serverFileLocation = getServletContext().getInitParameter("param_file_location");
		kojaParam.serverIpport = getServletContext().getInitParameter("param_server_ipport");
		kojaParam.serverFileAccess = getServletContext().getInitParameter("param_file_access");
  		SchedulerAdapter.SchedulerGetCMSEIR(kojaParam);
    }
}
