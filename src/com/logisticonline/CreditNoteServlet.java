package com.logisticonline;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import adapter.CreditNoteAdapter;
import adapter.DebitNoteAdapter;
import adapter.MenuAdapter;
import adapter.TierAdapter;
import adapter.VendorAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlConfirmPaymentCreditNote;
import model.mdlConfirmPaymentDebitNote;

@WebServlet(urlPatterns={"/CreditNote"} , name="CreditNote")
public class CreditNoteServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public CreditNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String pStatus="", pStuffingDateFrom="", pStuffingDateTo="";
    String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
    	
    	Boolean CheckMenu = false;
    	String MenuURL = "CreditNote";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String queryStringDate = request.getParameter("date");
    		if(queryStringDate != null && queryStringDate.equals("today")){
    			pStatus="Unpaid";
    			pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");;
    			pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");;
    		}
    		
    		List<model.mdlCreditNote> creditNoteList = new ArrayList<model.mdlCreditNote>();
    		Integer totalAmountUnpaid = 0;

    		if (sessionRole.equals("R001")){ //admin
    			creditNoteList.addAll(CreditNoteAdapter.LoadCreditNote("",pStuffingDateFrom,pStuffingDateTo,pStatus));
    			totalAmountUnpaid = CreditNoteAdapter.LoadTotalAmountUnpaid("", pStuffingDateFrom, pStuffingDateTo);
    		}else if (sessionRole.equals("R003")){
    			creditNoteList.addAll(CreditNoteAdapter.LoadCreditNote(sessionUser,pStuffingDateFrom,pStuffingDateTo,pStatus));
    			totalAmountUnpaid = CreditNoteAdapter.LoadTotalAmountUnpaid(sessionUser, pStuffingDateFrom, pStuffingDateTo);
    		}
    		request.setAttribute("listCreditNote", creditNoteList);
    		request.setAttribute("totalAmountUnpaid", totalAmountUnpaid);
    		
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("globalUserID", sessionUser);
    		request.setAttribute("globalUserRole", sessionRole);
    		request.setAttribute("status", pStatus);
        	request.setAttribute("stuffingDateFrom", pStuffingDateFrom);
			request.setAttribute("stuffingDateTo", pStuffingDateTo);
    		
    		RequestDispatcher dispacther;
    		result = "";
			resultDesc = "";
			dispacther = request.getRequestDispatcher("/mainform/pages/credit_note.jsp");
			dispacther.forward(request, response);
    	}
	}
    
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		if(keyBtn.equals("save")){
			//Declare TextBox
			String[] creditNoteNumber = request.getParameterValues("creditNoteNumber[]");
			String paymentDate = ConvertDateTimeHelper.formatDate(request.getParameter("paymentDate"), "dd MMM yyyy", "yyyy-MM-dd");
			
			String lResult = CreditNoteAdapter.ConfirmPayment(creditNoteNumber, paymentDate, sessionUser);
			if(lResult.contains("Success Confirm Payment")) {
				result =  "SuccessConfirm";
            }
            else {
            	result = "FailedConfirm";
            	resultDesc = lResult;
            }
		}
		
		if (keyBtn.equals("saveConfirmPayment")) {
			// Declare TextBox
			String fullPayment = request.getParameter("fullPayment");
			String partialPayment = request.getParameter("partialPayment");

			Gson gson = new Gson();

			Type listType = new TypeToken<ArrayList<mdlConfirmPaymentCreditNote>>() {
			}.getType();
			
			List<mdlConfirmPaymentCreditNote> listFullPayment = gson.fromJson(fullPayment, listType);
			List<mdlConfirmPaymentCreditNote> listPartialPayment = gson.fromJson(partialPayment, listType);
			
			String lResult = CreditNoteAdapter.ConfirmFullPayment(listFullPayment, sessionUser);
			String lResult2 = CreditNoteAdapter.ConfirmPartialPaymentV2(listPartialPayment, sessionUser);
			
			if(lResult.contains("Success Confirm Payment") || lResult2.contains("Success Confirm Partial Payment")) {
				result =  "SuccessConfirm";
            }
            else {
            	result = "FailedConfirm";
            	resultDesc = lResult;
            }

			response.setContentType("application/text");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result + "--" + resultDesc);

		}
		
		if(keyBtn.equals("SavePartialPayment")){
			//Declare TextBox
			String creditNoteNumber = request.getParameter("creditNoteNumber");
			String paymentDate = ConvertDateTimeHelper.formatDate(request.getParameter("paymentDate"), "dd MMM yyyy", "yyyy-MM-dd");
			Double amountPaid = Double.valueOf(request.getParameter("amountPaid"));
			
			String lResult = CreditNoteAdapter.ConfirmPartialPayment(creditNoteNumber, paymentDate, amountPaid, sessionUser);
			if(lResult.contains("Success Confirm Partial Payment")) {
				result =  "SuccessConfirmPartialPayment";
            }
            else {
            	result = "FailedConfirmPartialPayment";
            	resultDesc = lResult;
            }
		}
		
		
		if(keyBtn.equals("search")) {
			//Declare class private string
	    	pStatus = request.getParameter("statusSearch");
	    	pStuffingDateFrom = request.getParameter("stuffingdatefrom");
	    	pStuffingDateTo = request.getParameter("stuffingdateto");
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
    
}
