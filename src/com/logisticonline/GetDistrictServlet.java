package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;

import adapter.DistrictAdapter;

@WebServlet(urlPatterns={"/GetDistrict"} , name="GetDistrict")
public class GetDistrictServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetDistrictServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		List<model.mdlDistrict> DistrictList = new ArrayList<model.mdlDistrict>();
		
		String lProvinceID = request.getParameter("provinceid");
		if(lProvinceID.equals("getAll")){
			DistrictList = DistrictAdapter.LoadDistrict(user);
		}
		else{
			DistrictList = DistrictAdapter.LoadDistrictByProvince(lProvinceID, user);
		}
    	
    	Gson gson = new Gson();
    	
    	String jsonlistDistrict = gson.toJson(DistrictList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistDistrict);
	}
	
}
