package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import adapter.KojaAdapter;

@WebServlet(urlPatterns={"/getVesselVoyage"} , name="getVesselVoyage")
public class GetVesselServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public GetVesselServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		List<model.mdlVessel> listVessel = new ArrayList<model.mdlVessel>();
		String terminal = request.getParameter("terminal");
		
		if(keyBtn.equals("local")){
			listVessel = KojaAdapter.GetVesselVoyageByTerminal(terminal, sessionUser);
		}
		else{
			int i = 0;		
			String[] vesselCode = request.getParameterValues("vesselCode[]");
			String[] vesselName = request.getParameterValues("vesselName[]");
			String[] voyageCode = request.getParameterValues("voyageCode[]");
			String[] voyageStatus = request.getParameterValues("voyageStatus[]");
			String[] voyageCompany = request.getParameterValues("voyageCompany[]");
			String[] podCode = request.getParameterValues("podCode[]");
			String[] podName = request.getParameterValues("podName[]");
			String[] voyageArrival = request.getParameterValues("voyageArrival[]");
			String[] voyageDeparture = request.getParameterValues("voyageDeparture[]");
			KojaAdapter.DeleteVesselVoyage(terminal, sessionUser);
			
			for(String code : vesselCode){
				model.mdlVessel mdlVessel = new model.mdlVessel();
				mdlVessel.vesselCode = code;
				mdlVessel.vesselName = vesselName[i];
				mdlVessel.voyageCode = voyageCode[i];
				mdlVessel.voyageStatus = voyageStatus[i];
				mdlVessel.voyageCompany = voyageCompany[i];
				mdlVessel.podCode = podCode[i];
				mdlVessel.podName = podName[i];
				mdlVessel.voyageArrival = voyageArrival[i];
				mdlVessel.voyageDeparture = voyageDeparture[i];
				mdlVessel.terminal = terminal;
				
				KojaAdapter.InsertVesselVoyage(mdlVessel, sessionUser);
				listVessel.add(mdlVessel);
				i++;
			}
		}
		request.setAttribute("listVessel", listVessel);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getVessel.jsp");
		dispacther.forward(request, response);
	}
    
}
