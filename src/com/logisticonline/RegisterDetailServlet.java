package com.logisticonline;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.ContainerTypeAdapter;
import adapter.FactoryAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/RegisterDetail"} , name="RegisterDetail")
public class RegisterDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public RegisterDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    String CustomerID,CustomerName,tempCustomerID,tempCustomerName,result,resultDesc;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CustomerID = request.getParameter("custid");
    	CustomerName = request.getParameter("custname");
    	
    	if (CustomerID == null)
    	{
	    	CustomerID = tempCustomerID;
	    	CustomerName = tempCustomerName;
    	}
    	
    	if(CustomerID == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Register");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		
		List<model.mdlFactory> mdlFactoryList = new ArrayList<model.mdlFactory>();
		mdlFactoryList.addAll(FactoryAdapter.LoadFactoryByCustomer(CustomerID, CustomerName));
		request.setAttribute("listFactory", mdlFactoryList);
		
		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
		districtList.addAll(adapter.DistrictAdapter.LoadDistrict(CustomerID));
		request.setAttribute("listDistrict", districtList);

		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(adapter.TierAdapter.LoadTier(CustomerID));
		request.setAttribute("listTier", tierList);
		
		request.setAttribute("condition", result);
		request.setAttribute("conditionDescription", resultDesc);
		request.setAttribute("temp_customerName", CustomerName);
		request.setAttribute("temp_customerID", CustomerID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/register_detail.jsp");
		dispacther.forward(request, response);
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String temp_txtCustomerID = request.getParameter("temp_txtCustomerID");
		String temp_txtFactoryID = request.getParameter("temp_txtFactoryID");

		//Declare mdlFactory for global
		model.mdlFactory mdlFactory = new model.mdlFactory();
		mdlFactory.setCustomerID(request.getParameter("txtCustomerID"));
		
		mdlFactory.setFactoryID(request.getParameter("txtFactoryID"));
		if(mdlFactory.getFactoryID() == null || mdlFactory.getFactoryID().contentEquals(""))
			mdlFactory.setFactoryID(FactoryAdapter.CreateFactoryID(CustomerID));
			
		mdlFactory.setFactoryName(request.getParameter("txtFactoryName"));
		mdlFactory.setAddress(request.getParameter("txtAddress"));
		mdlFactory.setDistrictID(request.getParameter("slDistrict"));
		mdlFactory.setTierID(request.getParameter("slTier"));

		if (keyBtn.equals("save")){
			String lResult = FactoryAdapter.RegisterFactory(mdlFactory);
			if(lResult.contains("Success Register Factory")) {
				result = "SuccessRegisterFactory";
            }
            else {
            	result = "FailedRegisterFactory";
            	resultDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = FactoryAdapter.UpdateFactory(mdlFactory,CustomerID);

		    if(lResult.contains("Success Update Factory")) {
		    	result = "SuccessUpdateFactory";
            }
            else {
            	result = "FailedUpdateFactory";
            	resultDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = FactoryAdapter.DeleteFactory(temp_txtCustomerID, temp_txtFactoryID,CustomerID);
			if(lResult.contains("Success Delete Factory")) {
				result =  "SuccessDeleteFactory";
            }
            else {
            	result = "FailedDeleteFactory";
            	resultDesc = lResult;
            }

			doGet(request, response);
		}
		
		tempCustomerID = CustomerID;
		tempCustomerName = CustomerName;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
    }
    
}
