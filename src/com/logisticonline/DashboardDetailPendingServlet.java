package com.logisticonline;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.DashboardAdapter;
import adapter.DriverImageAdapter;
import model.mdlDashboard;
import model.mdlDriverImage;

@WebServlet(urlPatterns={"/DashboardPendingDetail"} , name="DashboardPendingDetail")
public class DashboardDetailPendingServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    public DashboardDetailPendingServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
String sessionUser, sessionRole, noSI, partaiPerSIToday, vendorOrderID, tempSI, containerType = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	sessionUser = (String) session.getAttribute("user");
    	sessionRole = (String) session.getAttribute("userrole");
    	
    	noSI = request.getParameter("noSI");
    	partaiPerSIToday = request.getParameter("ppsiToday");
    	vendorOrderID = request.getParameter("voID"); //for vendor dashboard detail pending
    	containerType = request.getParameter("cttype");
    	request.setAttribute("noSI", noSI);
    	request.setAttribute("partaiPerSIToday", partaiPerSIToday);
    	request.setAttribute("vendorOrderID", vendorOrderID);
    	request.setAttribute("cttype", containerType);
    	
		//    	if (noSI == null)
		//    	{
		//    		if (tempSI == null || tempSI.equals("") ){
		//    			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
		//        		dispacther.forward(request, response);
		//    		}else{
		//    			noSI = tempSI;
		//    		}
		//    	}
    	
    	//get date and time now
		DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		String dateNow = dateFormat.format(date);
		String timeNow = timeFormat.format(date);
		request.setAttribute("dateNow", dateNow);
		request.setAttribute("timeNow", timeNow);
		
		if(sessionUser != null){
			sessionRole = (String) session.getAttribute("userrole");
			
			//role as customer
			if(sessionRole.contentEquals("R002")){
				//Dashboard atas
				List<model.mdlDashboardDetail> listDashboardCustomerDetail = new ArrayList<model.mdlDashboardDetail>();
				
				model.mdlDashboardDetail dashboardCustomerDetail = DashboardAdapter.LoadDashboardCustomerPendingDetail(noSI, sessionUser);
				String[] boxTitleList = {"Processed Order", "Depo In", "Depo Out", "Factory In", "Factory Out", "Delivered"};
				String[] boxImageList = {"Group 4308.png", "Group 4311.png", "Group 4311.png", "Group 4312.png", "Group 4312.png", "port.png"};
				Integer[] boxValueList = {dashboardCustomerDetail.customerOrderAccepted, 
										dashboardCustomerDetail.depoIn,
										dashboardCustomerDetail.depoProcess,
										dashboardCustomerDetail.factoryIn,
										dashboardCustomerDetail.factoryProcess,
										dashboardCustomerDetail.deliveredProcess};
				String[] boxInfoList = {"Processed Order", "Depo In", "Depo Out", "Factory In", "Factory Out", "Delivered"};
				for(int i=0; i<6; i++){
					model.mdlDashboardDetail dashboard = new model.mdlDashboardDetail();
					dashboard.boxTitle = boxTitleList[i];
					dashboard.boxImage = boxImageList[i];
					dashboard.boxValue = boxValueList[i];
					dashboard.boxInfo = boxInfoList[i];
					listDashboardCustomerDetail.add(dashboard);
				}
				request.setAttribute("listDashboardDetail", listDashboardCustomerDetail);
				
				//Dashboard map
				//				List<model.mdlDashboard> listDashboardMap = new ArrayList<model.mdlDashboard>();
				//				listDashboardMap = DashboardAdapter.LoadDashboardCustomerDetailMap(noSI, sessionUser);
				//				request.setAttribute("listMarker", listDashboardMap);
				
				//informasi total order per si
				Integer totalOrderPerSI = DashboardAdapter.LoadTotalOrderPerSI(noSI, sessionUser);
				request.setAttribute("totalOrderPerSI", totalOrderPerSI);
				
				//Dashboard list
				List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
				listDashboard = DashboardAdapter.LoadDashboardPendingDetailList(sessionRole,sessionUser, noSI);
				
				request.setAttribute("listOrderDashboard", listDashboard);
			}
			//role as vendor
			else if(sessionRole.contentEquals("R003")){
				//Dashboard atas
				/*List<model.mdlDashboardDetail> listDashboardVendorDetail = new ArrayList<model.mdlDashboardDetail>();
				
				model.mdlDashboardDetail dashboardVendorDetail = DashboardAdapter.LoadDashboardVendorDetail(vendorOrderID, sessionUser);
				String[] boxTitleList = {"Processed Order", "Depo Processed", "Factory Processed", "Delivered"};
				String[] boxImageList = {"Group 4308.png", "Group 4311.png", "Group 4312.png", "port.png"};
				Integer[] boxValueList = {dashboardVendorDetail.vendorOrderPicked, 
										dashboardVendorDetail.depoProcess, 
										dashboardVendorDetail.factoryProcess,
										dashboardVendorDetail.deliveredProcess};
				for(int i=0; i<4; i++){
					model.mdlDashboardDetail dashboard = new model.mdlDashboardDetail();
					dashboard.boxTitle = boxTitleList[i];
					dashboard.boxImage = boxImageList[i];
					dashboard.boxValue = boxValueList[i];
					listDashboardVendorDetail.add(dashboard);
				}
				request.setAttribute("listDashboardDetail", listDashboardVendorDetail);
				
				//Dashboard list
				List<model.mdlDashboard> listDashboard = new ArrayList<model.mdlDashboard>();
				listDashboard = DashboardAdapter.LoadDashboardDetailList(sessionRole, sessionUser, vendorOrderID);
				request.setAttribute("listOrderDashboard", listDashboard);*/
			}
			
			request.setAttribute("globalUserRole", sessionRole);
		}
    	
    	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/dashboard_detail.jsp");
		dispacther.forward(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//redefine tempSI in here
    }

}
