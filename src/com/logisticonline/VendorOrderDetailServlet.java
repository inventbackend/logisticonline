package com.logisticonline;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.APIAdapter;
import adapter.OrderManagementAdapter;
import adapter.PushNotifAdapter;
import adapter.VendorAdapter;
import adapter.VendorOrderAdapter;
import adapter.VendorOrderDetailAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;
import model.mdlKojaParam;

@WebServlet(urlPatterns={"/VendorOrderDetailList"} , name="VendorOrderDetailList")
public class VendorOrderDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public VendorOrderDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String vendorOrderID, tempVendorOrderID, orderStuffingDate, tempStuffingDate,
	orderType, tempOrderType, customer, tempCustomer, district, address, tempAddress,
	tempDistrict, depo, tempDepo, port, tempPort, vendorOrderQty, tempVendorOrderQty,
	item, tempItem, order, tempOrder, orderDetail, tempOrderDetail, noSI, tempNoSI, factory, tempFactory,
	sessionUser, sessionRole, result, resultDesc = "";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
		/*
		 * if (sessionUser == null) { RequestDispatcher dispacther =
		 * request.getRequestDispatcher("/Dashboard"); dispacther.forward(request,
		 * response); return; }
		 */
		
		vendorOrderID = request.getParameter("vendorOrderID");
		orderStuffingDate = request.getParameter("stuffingDate");
		customer = request.getParameter("customer");
		district = request.getParameter("district");
		depo = request.getParameter("depo");
		port = request.getParameter("port");
		vendorOrderQty = request.getParameter("orderQty");
		orderType = request.getParameter("orderType");
		item = request.getParameter("item");
		order = request.getParameter("order");
		orderDetail = request.getParameter("orderDetail");
		address = request.getParameter("address");
		noSI = request.getParameter("noSI");
		factory = request.getParameter("factory");

    	//check of parameter is null or not, if null, get vendorOrderID from global. if global null,
    	if (vendorOrderID == null)
    	{
    		if (tempVendorOrderID == null || tempVendorOrderID.equals("") ){
    			RequestDispatcher dispacther = request.getRequestDispatcher("/VendorOrderList");
        		dispacther.forward(request, response);
    		}else{
    			vendorOrderID = tempVendorOrderID;
    		}
    	}

    	if(orderStuffingDate == null){
    		orderStuffingDate = tempStuffingDate;
    		customer = tempCustomer;
    		district = tempDistrict;
    		depo = tempDepo;
    		port = tempPort;
    		vendorOrderQty = tempVendorOrderQty;
    		orderType = tempOrderType;
    		item = tempItem;
    		order = tempOrder;
    		orderDetail = tempOrderDetail;
    		address = tempAddress;
    		noSI = tempNoSI;
    		factory = tempFactory;
    	}

		List<model.mdlVendorOrderDetail> vendorOrderDetailList = new ArrayList<model.mdlVendorOrderDetail>();
		vendorOrderDetailList.addAll(VendorOrderDetailAdapter.LoadVendorOrderDetailByID(vendorOrderID, sessionUser));
		request.setAttribute("listVendorOrderDetail", vendorOrderDetailList);
		
		List<model.mdlVendorOrderDetail> nonCancelVODetailList = new ArrayList<model.mdlVendorOrderDetail>();
		nonCancelVODetailList.addAll(VendorOrderDetailAdapter.LoadNonCancelVendorOrderDetailByID(vendorOrderID, sessionUser));

		List<model.mdlVendorDriver> driverList = new ArrayList<model.mdlVendorDriver>();
		if (sessionRole.equals("R001")){ //admin
			driverList.addAll(VendorAdapter.LoadVendorDriver(sessionUser));
    	}else{
    		driverList.addAll(VendorAdapter.LoadVendorDriverByVendor(sessionUser));
    	}
		request.setAttribute("listDriver", driverList);

		List<model.mdlVendorVehicle> vehicleList = new ArrayList<model.mdlVendorVehicle>();
		if (sessionRole.equals("R001")){ //admin
			vehicleList.addAll(VendorAdapter.LoadVendorVehicle(sessionUser));
    	}else{
    		vehicleList.addAll(VendorAdapter.LoadVendorVehicleByVendor(sessionUser));
    	}
		request.setAttribute("listVehicle", vehicleList);

		Boolean allowAdd = VendorOrderDetailAdapter.AllowAddVendorOrderDetail(vendorOrderID, sessionUser);

		request.setAttribute("condition", result);
		request.setAttribute("conditionDescription", resultDesc);

		request.setAttribute("vendorOrderID", vendorOrderID); //send VendorOrderID to jsp via jstl
		Integer vendorStatus = VendorOrderAdapter.GetVendorStatus(vendorOrderID,sessionUser);
		String vendorStatusString = VendorOrderAdapter.CheckVendorStatus(vendorOrderID,sessionUser);
		request.setAttribute("vendorStatus", vendorStatus);
		request.setAttribute("vendorStatusString", vendorStatusString);
		request.setAttribute("orderStuffingDate", orderStuffingDate);
		request.setAttribute("orderType", orderType);
		request.setAttribute("item", item);
		request.setAttribute("customer", customer);
		request.setAttribute("district", district);
		request.setAttribute("depo", depo);
		request.setAttribute("port", port);
		request.setAttribute("address", address);
		request.setAttribute("noSI", noSI);
		request.setAttribute("factory", factory);
		request.setAttribute("allowAdd", allowAdd);
		request.setAttribute("vendorOrderQty", vendorOrderQty);
		request.setAttribute("driverCount", nonCancelVODetailList.size());
		request.setAttribute("globalUserID", sessionUser);
		request.setAttribute("globalUserRole", sessionRole);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/vendor_order_detail.jsp");
		dispacther.forward(request, response);

		result = "";
		resultDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String btnDelete = request.getParameter("btnDelete");

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String vendorOrderID = request.getParameter("txtVendorOrderID");
		String temp_txtVendorOrderID = request.getParameter("temp_txtVendorOrderID");
		String temp_txtVendorOrderDetailID = request.getParameter("temp_txtVendorOrderDetailID");
		String temp_txtDriverID = request.getParameter("temp_txtDriverID");
		String driverID = request.getParameter("slDriverID");
		String vehicleNumber = request.getParameter("slVehicleNumber");
		String containerNumber = request.getParameter("txtContainerNumber").replace(" ", "");
		String sealNumber = request.getParameter("txtSealNumber");

		//Declare mdlVendorOrderDetail for global
		model.mdlVendorOrderDetail mdlVendorOrderDetail = new model.mdlVendorOrderDetail();
		mdlVendorOrderDetail.setOrderID(order);
		mdlVendorOrderDetail.setOrderDetailID(orderDetail);
		mdlVendorOrderDetail.setVendorOrderDetailID(request.getParameter("txtVendorOrderDetailID"));
		mdlVendorOrderDetail.setVendorOrderID(request.getParameter("txtVendorOrderID"));
		mdlVendorOrderDetail.setDriverID(driverID);
		mdlVendorOrderDetail.setVehicleNumber(vehicleNumber);
		mdlVendorOrderDetail.setContainerNumber(containerNumber);
		mdlVendorOrderDetail.setSealNumber(sealNumber);
		mdlVendorOrderDetail.setCreatedBy(sessionUser);
		mdlVendorOrderDetail.setUpdatedBy(sessionUser);
		mdlVendorOrderDetail.setDriverTargetAmount(Integer.valueOf(vendorOrderQty) );
		
		//Declare temporary variables
		if(vendorOrderID == null || vendorOrderID.contentEquals(""))
			tempVendorOrderID = temp_txtVendorOrderID;
		else
			tempVendorOrderID = vendorOrderID;

		tempStuffingDate = orderStuffingDate;
		tempCustomer = customer;
		tempDistrict = district;
		tempDepo = depo;
		tempPort = port;
		tempVendorOrderQty = vendorOrderQty;
		tempOrderType = orderType;
		tempItem = item;
		tempOrder = order;
		tempOrderDetail = orderDetail;
		tempAddress = address;
		tempNoSI = noSI;
		tempFactory = factory;

		//SERVLET MAIN PROCESS
		if (keyBtn.equals("save")){
			//check port id
			String portID = OrderManagementAdapter.LoadOrderManagementByID(order, sessionUser).getPortID();
			Boolean checkProcessValidation = true;
			
			//if port loading is koja, do this
			if(portID.equals("PORT-0002")){
				//for koja validate tid process model parameter
				mdlKojaParam kojaParam = new model.mdlKojaParam();
				kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
				kojaParam.username2 = getServletContext().getInitParameter("asso_username");
				kojaParam.password2 = getServletContext().getInitParameter("asso_password");
				
				String tidNumber = VendorAdapter.GetTidNumberByVendorVehicle(sessionUser, vehicleNumber);
				//check tid is valid or not in koja
				checkProcessValidation = APIAdapter.KojaTIDValidationForDriverAssignment(kojaParam, tidNumber);
			}
			
			//if true, it means that order is allowed to be processed (not koja's order or koja's order with validated tid)
			if(checkProcessValidation){
				String lResult = VendorOrderDetailAdapter.InsertVendorOrderDetail(mdlVendorOrderDetail, sessionUser);

				if(lResult.contains("Success Insert Vendor Order Detail")) {
					result = "SuccessInsertVendorOrderDetail";
					
					//-- push notif to mobile
					PushNotifAdapter.PushNotification("assign", driverID, sessionUser);
				}
	            else {
	            	result = "FailedInsertVendorOrderDetail";
	            	resultDesc = lResult;
	            }
			}
			else{
				result = "FailedInsertVendorOrderDetail";
				resultDesc = "Koja TID Invalid. Please contact administrator";
			}
		}

		if (keyBtn.equals("update")){
			//check port id
			String portID = OrderManagementAdapter.LoadOrderManagementByID(order, sessionUser).getPortID();
			Boolean checkProcessValidation = true;
			
			//if port loading is koja, do this
			if(portID.equals("PORT-0002")){
				//for koja validate tid process model parameter
				mdlKojaParam kojaParam = new model.mdlKojaParam();
				kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
				kojaParam.username2 = getServletContext().getInitParameter("asso_username");
				kojaParam.password2 = getServletContext().getInitParameter("asso_password");
				
				String tidNumber = VendorAdapter.GetTidNumberByVendorVehicle(sessionUser, vehicleNumber);
				//check tid is valid or not in koja
				checkProcessValidation = APIAdapter.KojaTIDValidationForDriverAssignment(kojaParam, tidNumber);
			}
			
			//if true, it means that order is allowed to be processed (not koja's order or koja's order with validated tid)
			if(checkProcessValidation){
				String lResult = VendorOrderDetailAdapter.UpdateVendorOrderDetail(mdlVendorOrderDetail, sessionUser);

			    if(lResult.contains("Success Update Vendor Order Detail")) {
			    	result = "SuccessUpdateVendorOrderDetail";
	            }
	            else {
	            	result = "FailedUpdateVendorOrderDetail";
	            	resultDesc = lResult;
	            }
			}
			else{
				result = "FailedUpdateVendorOrderDetail";
				resultDesc = "Koja TID Invalid. Please contact administrator";
			}
		}
		
		if (btnDelete != null){
			String lResult = VendorOrderDetailAdapter.DeleteVendorOrderDetail(temp_txtVendorOrderID, temp_txtVendorOrderDetailID, temp_txtDriverID, sessionUser);
			if(lResult.contains("Success Delete Vendor Order Detail")) {
				result =  "SuccessDeleteVendorOrderDetail";
				PushNotifAdapter.PushNotification("cancel assign", temp_txtDriverID, sessionUser);
            }
            else {
            	result = "FailedDeleteVendorOrderDetail";
            	resultDesc = lResult;
            }
			doGet(request, response);
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
    }
}
