package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.PortAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Port"} , name="Port")
public class MasterPortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterPortServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Port";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlPort> portList = new ArrayList<model.mdlPort>();
    		portList.addAll(adapter.PortAdapter.LoadPort(sessionUser) );
    		request.setAttribute("listPort", portList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_port.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtPortID = request.getParameter("txtPortID");
        String txtPortName = request.getParameter("txtPortName");
        String txtPortUTC = request.getParameter("txtPortUTC");
		String temp_txtPortID = request.getParameter("temp_txtPortID");

		//Declare mdlPlant for global
		model.mdlPort mdlPort = new model.mdlPort();
		mdlPort.setPortID(txtPortID);
		mdlPort.setPortName(txtPortName);
		mdlPort.setPortUTC(txtPortUTC);

		if (keyBtn.equals("save")){
			String lResult = PortAdapter.InsertPort(mdlPort,sessionUser);
			if(lResult.contains("Success Insert Port")) {
				result = "SuccessInsertPort";
            }
            else {
            	result = "FailedInsertPort";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = PortAdapter.UpdatePort(mdlPort,sessionUser);

		    if(lResult.contains("Success Update Port")) {
		    	result = "SuccessUpdatePort";
            }
            else {
            	result = "FailedUpdatePort";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = PortAdapter.DeletePort(temp_txtPortID,sessionUser);
			if(lResult.contains("Success Delete Port")) {
				result =  "SuccessDeletePort";
            }
            else {
            	result = "FailedDeletePort";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
