package com.logisticonline;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import adapter.OrderManagementAdapter;

@WebServlet(urlPatterns={"/getDocumentation"} , name="getDocumentation")
public class GetDocumentationServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public GetDocumentationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		String dateNow = LocalDateTime.now().toString();
    	
    	List<model.mdlOrderManagement> orderList = new ArrayList<model.mdlOrderManagement>();
    	orderList.addAll(OrderManagementAdapter.LoadDocumentation(dateNow, sessionUser));
		request.setAttribute("listDocumentation", orderList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getDocumentation.jsp");
		dispacther.forward(request, response);
	}
    
}
