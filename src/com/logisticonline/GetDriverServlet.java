package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.DailyNewsAdapter;
import adapter.MenuAdapter;
import adapter.ValidateNull;
import adapter.VendorAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getDriver"} , name="getDriver")
public class GetDriverServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public GetDriverServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String sessionUser = "";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		String newsID = ValidateNull.NulltoStringEmpty(request.getParameter("newsid"));
    	String command = request.getParameter("command");
    	List<model.mdlDailyNewsDetail> driverList = new ArrayList<model.mdlDailyNewsDetail>();
    	Gson gson = new Gson();
    	String json = "";

        if (command.equalsIgnoreCase("update")){
        	driverList.addAll(DailyNewsAdapter.LoadNewsDetail(newsID, sessionUser));
        	String jsonDriverList = gson.toJson(driverList);
        	json = "["+jsonDriverList+"]";
    	}

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
}
