package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.mapping.Array;

import adapter.DebitNoteAdapter;
import adapter.VendorOrderDetailAdapter;

@WebServlet(urlPatterns={"/Invoice"} , name="Invoice")
public class InvoiceServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public InvoiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser,sessionRole = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
		if(sessionUser == null || sessionUser.equals("")){
			RequestDispatcher dispacther = request.getRequestDispatcher("/Login");
			dispacther.forward(request, response);
		}
		else{
			String debitNote = request.getParameter("invoiceNumber");
			model.mdlInvoice mdlInvoice = DebitNoteAdapter.LoadInvoice(debitNote, sessionUser);
	    	
			//check if the user is allowed for access or not
			if(mdlInvoice.getCustomerID().equals(sessionUser) || sessionRole.equals("R001")){
				request.setAttribute("mdlInvoice", mdlInvoice);
				request.setAttribute("listDO", mdlInvoice.getListDO());
				
				RequestDispatcher dispacther;
				dispacther = request.getRequestDispatcher("/mainform/pages/invoice.jsp");
				dispacther.forward(request, response);
			}
			else{
				RequestDispatcher dispacther = request.getRequestDispatcher("/Logout");
				dispacther.forward(request, response);
			}
		}
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    }
    
}
