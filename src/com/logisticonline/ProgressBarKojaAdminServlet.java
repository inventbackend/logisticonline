package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.OrderManagementAdapter;
import helper.LongProcess;

@WebServlet(urlPatterns={"/progressKojaAdmin"} , name="progressKojaAdmin")
public class ProgressBarKojaAdminServlet extends HttpServlet {
	
private static final long serialVersionUID = 1490947492185481844L;
int loading = 0;
String status = "";

	public ProgressBarKojaAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	      throws ServletException, IOException {
		  
		  	HttpSession session = req.getSession(true);
		  	String sessionUser = (String) session.getAttribute("user");
		  	
		  	String orderID = req.getParameter("orderID");
		  	Integer orderTpsStatus = OrderManagementAdapter.LoadTpsDataByOrder(orderID, sessionUser).getTpsBillingStatus();
        	if(orderTpsStatus == 4){
        		loading = 14;
        		status = "Confirm Transaction Success";
        	}
        	else if(orderTpsStatus == 5){
        		loading = 28;
        		status = "Get Billing Success";
        	}
        	else if(orderTpsStatus == 6){
        		loading = 42;
        		status = "Get Proforma Success";
        	}
        	else if(orderTpsStatus == 7){
        		loading = 56;
        		status = "Inquiry Success";
        	}
        	else if(orderTpsStatus == 8){
        		loading = 70;
        		status = "Payment Success";
        	}
        	else if(orderTpsStatus == 9){
        		loading = 85;
        		status = "Flagging Payment Success";
        	}
        	else if(orderTpsStatus == 10){
        		loading = 100;
        		status = "Get Invoice Success";
        	}
		  	
		  	session.setAttribute("longProcess", loading);
		  	
		  	LongProcess longProcess = new LongProcess((int)session.getAttribute("longProcess"));
		  	
		  	int progress = longProcess.getProgress();
		  	model.mdlProgressBar mdlProgressBar = new model.mdlProgressBar();
		  	mdlProgressBar.setPercentage(progress);
		  	mdlProgressBar.setStatus(status);
		  	
		  	String json = "";
		  	Gson gson = new Gson();
		  	json = gson.toJson(mdlProgressBar);

	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(json);
	        
	        if (loading == 100) {
				loading = 0;
			}
	  }
	  
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		      throws ServletException, IOException {
	  }
}
