package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.ContainerTypeAdapter;
import adapter.RateAdapter;

@WebServlet(urlPatterns={"/getContainerType"} , name="getContainerType")
public class GetContainerTypeServlet extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetContainerTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	List<model.mdlContainerType> listContainerType = new ArrayList<model.mdlContainerType>();
    	listContainerType = ContainerTypeAdapter.LoadContainerType(user);
    	
    	Gson gson = new Gson();
    	
    	String jsonlistContainerType = gson.toJson(listContainerType);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistContainerType);
	}
	
}
