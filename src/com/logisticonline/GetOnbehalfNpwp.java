package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.RateAdapter;

@WebServlet(urlPatterns={"/getOnbehalfNpwp"} , name="getOnbehalfNpwp")
public class GetOnbehalfNpwp extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

    public GetOnbehalfNpwp() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String lCustomerID = request.getParameter("customerid");
    	
    	List<model.mdlOnbehalfNpwp> dataList = new ArrayList<model.mdlOnbehalfNpwp>();
    	dataList.addAll(CustomerAdapter.LoadOnbehalfNpwp(lCustomerID, sessionUser));
		request.setAttribute("listData", dataList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getOnbehalfNpwp.jsp");
		dispacther.forward(request, response);
	}
    
}
