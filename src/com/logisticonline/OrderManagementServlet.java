package com.logisticonline;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import adapter.ContainerTypeAdapter;
import adapter.ConvertDateTimeHelper;
import adapter.CustomerAdapter;
import adapter.DashboardAdapter;
import adapter.DepoAdapter;
import adapter.MenuAdapter;
import adapter.ShippingAdapter;
import adapter.TierAdapter;
import adapter.ValidateNull;
import adapter.VendorAdapter;
import adapter.VendorOrderAdapter;
import adapter.WriteFileAdapter;
import model.Globals;
import adapter.OrderManagementAdapter;
import adapter.OrderManagementDetailAdapter;
import adapter.PortAdapter;

@WebServlet(urlPatterns={"/OrderManagement"} , name="OrderManagement")
@MultipartConfig
public class OrderManagementServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

    public OrderManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String docNo, tempDocNo, sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
    	
    	Boolean CheckMenu = false;
    	String MenuURL = "OrderManagement";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlOrderManagement> orderManagementList = new ArrayList<model.mdlOrderManagement>();
    		List<model.mdlOrderManagement> archiveList = new ArrayList<model.mdlOrderManagement>();

    		if (sessionRole.equals("R001")){ //admin
    			orderManagementList.addAll(OrderManagementAdapter.LoadOrderManagement(sessionUser));
    			archiveList.addAll(OrderManagementAdapter.LoadArchive(sessionUser));
    			
    			List<model.mdlCustomer> mdlCustomer = new ArrayList<model.mdlCustomer>();
    			mdlCustomer = CustomerAdapter.LoadCustomer(sessionUser);
    			request.setAttribute("mdlCustomer", mdlCustomer);
    		}else if (sessionRole.equals("R002")){
    			orderManagementList.addAll(OrderManagementAdapter.LoadOrderManagementByCustomer(sessionUser));
    			archiveList.addAll(OrderManagementAdapter.LoadArchiveByCustomer(sessionUser));
    			
    			model.mdlCustomer mdlCustomer = new model.mdlCustomer();
    			mdlCustomer = CustomerAdapter.LoadSingleCustomer(sessionUser);
    			request.setAttribute("mdlCustomer", mdlCustomer);
    			
    			List<model.mdlContainerType> containerTypeList = new ArrayList<model.mdlContainerType>();
    			containerTypeList.addAll(ContainerTypeAdapter.LoadContainerType(sessionUser));
    			request.setAttribute("listContainerType", containerTypeList);
    			
    			// check open or close order
    			boolean statusOrder = true;
    			
    			SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
    			Date date = new Date();
    			String currentTime = helper.ConvertDateTimeHelper.formatDate(date.toString(), "EEE MMM dd HH:mm:ss zzz yyyy", "HH:mm");
    			
    			try {
    				Date startOpenOrder = parser.parse("00:00");
        			Date endOpenOrder = parser.parse("15:00");
        			
    			    Date userDate = parser.parse(currentTime);
    			    if (userDate.after(startOpenOrder) && userDate.before(endOpenOrder)) {
    			    	statusOrder = true;
    			    }
    			} catch (ParseException e) {
    			    // Invalid date was entered
    			}
    			
    			
    			
    			request.setAttribute("statusOrder", statusOrder);
    			
    			//dashboard for order tracking
    			model.mdlDashboard dashboard2 = DashboardAdapter.LoadDashboard2(sessionRole, sessionUser);
				request.setAttribute("customerTotalOrder", dashboard2.totalOrder);
				request.setAttribute("customerProcessedOrder", dashboard2.pickedOrder);
				request.setAttribute("customerUnProcessedOrder", dashboard2.unpickedOrder);
    		}
    		request.setAttribute("listOrderManagement", orderManagementList);
    		request.setAttribute("listArchive", archiveList);

    		List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
			request.setAttribute("listVendor", vendorList);
			
    		List<model.mdlPort> portList = new ArrayList<model.mdlPort>();
    		portList.addAll(PortAdapter.LoadPort(sessionUser));
    		request.setAttribute("listPort", portList);

    		List<model.mdlShipping> shippingList = new ArrayList<model.mdlShipping>();
    		shippingList.addAll(ShippingAdapter.LoadShipping(sessionUser));
    		request.setAttribute("listShipping", shippingList);

			List<model.mdlDepo> depoList = new ArrayList<model.mdlDepo>();
			depoList.addAll(DepoAdapter.LoadDepo(sessionUser));
			request.setAttribute("listDepo", depoList);
			
			List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
			tierList.addAll(TierAdapter.LoadTier(sessionUser));
			request.setAttribute("listTier", tierList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("globalUserID", sessionUser);
    		request.setAttribute("globalUserRole", sessionRole);    	

			RequestDispatcher dispacther;
			//    		if(result == "SuccessInsertOrderManagement" )
			//    		{
			//    			response.sendRedirect(request.getContextPath() + "/OrderManagementDetail?orderManagementID=" + tempDocNo+"&condition="+result);
			//    		}
			//    		else
			//    		{
			dispacther = request.getRequestDispatcher("/mainform/pages/order_management.jsp");
			dispacther.forward(request, response);
    		//    		}
    	}
    	
    	result = "";
		resultDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		String btnDelete = request.getParameter("btnCancel");
		String btnCommit = request.getParameter("btnConfirmCommit");

    	if (sessionUser == null || sessionUser == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String txtOrderManagementID = request.getParameter("txtOrderManagementID");
		String txtOrderManagementDetailID = request.getParameter("txtOrderManagementDetailID");
		String temp_txtOrderManagementID = request.getParameter("temp_txtOrderManagementID");
		String txtShippingInvoiceID = request.getParameter("txtShippingInvoiceID");
		String txtDeliveryOrderID = request.getParameter("txtDeliveryOrderID");
		String txtCustomerID = request.getParameter("txtCustomerID");
		String txtFactoryID = request.getParameter("txtFactoryID");
		String slOrderType = request.getParameter("slOrderType");
		String slTier = request.getParameter("slTier");
		String txtItem = request.getParameter("txtItem");
		String txtPortID = request.getParameter("txtPortID");
		String txtShippingID = request.getParameter("txtShippingID");
		String txtDepoID = request.getParameter("txtDepoID");
		String txtCreatedBy = sessionUser;
		String txtUpdatedBy = sessionUser;
		String txtVoyageNo = request.getParameter("txtVoyageNo");
		String txtVesselName = request.getParameter("txtVesselName");
		String txtPod = request.getParameter("txtPod");
		String txtVoyageCo = request.getParameter("txtVoyageCo");
		String txtShippingDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtShippingDate"), "dd MMM yyyy", "yyyy-MM-dd");
//		String txtPEB = request.getParameter("txtPEB");
//		String txtNPE = request.getParameter("txtNPE");

		String baseFilePath = getServletContext().getInitParameter("param_file_location");
		String baseFileAccess = getServletContext().getInitParameter("param_file_access");
		String baseSeparator = getServletContext().getInitParameter("param_separator");
		
		model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
		mdlOrderManagement.setOrderManagementID(txtOrderManagementID);
		mdlOrderManagement.setShippingInvoiceID(txtShippingInvoiceID);
	    mdlOrderManagement.setDeliveryOrderID(txtDeliveryOrderID);
	    mdlOrderManagement.setCustomerID(txtCustomerID);
	    mdlOrderManagement.setFactoryID(txtFactoryID);
	    mdlOrderManagement.setOrderType(slOrderType);
	    mdlOrderManagement.setTier(slTier);
	    mdlOrderManagement.setItemDescription(txtItem);
	    mdlOrderManagement.setPortID(txtPortID);
	    mdlOrderManagement.setShippingID(txtShippingID);
	    mdlOrderManagement.setDepoID(txtDepoID);
	    mdlOrderManagement.setCreatedBy(txtCreatedBy);
	    mdlOrderManagement.setUpdatedBy(txtUpdatedBy);
//	    mdlOrderManagement.setPeb(ValidateNull.NulltoStringEmpty(txtPEB));
//	    mdlOrderManagement.setNpe(ValidateNull.NulltoStringEmpty(txtNPE));
	    mdlOrderManagement.setPeb("");
	    mdlOrderManagement.setNpe("");
	    mdlOrderManagement.setVoyageNo(txtVoyageNo);
	    mdlOrderManagement.setVesselName(txtVesselName);
	    mdlOrderManagement.setPod(txtPod);
	    mdlOrderManagement.setVoyageCo(txtVoyageCo);
	    mdlOrderManagement.setShippingDate(txtShippingDate);
	    mdlOrderManagement.setVendor(ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("cbVendor")) );
	    
	    
	    String lResultWriteFile = "";
	    if (keyBtn.equals("save")){
	    	String txtOrderImage = Paths.get(request.getPart("txtOrderImage").getSubmittedFileName()).getFileName().toString();
	    	
	    	if(txtOrderManagementID == null || txtOrderManagementID.equals("")){
				txtOrderManagementID = OrderManagementAdapter.CreateOrderManagementID(sessionUser,"E"); //E for Export, I for Import
				mdlOrderManagement.setOrderManagementID(txtOrderManagementID);
			}
	    	
			if(txtOrderImage == null || txtOrderImage.equals("")){
				mdlOrderManagement.setOrderImage("");
			}
			else{
				Date date = new Date();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int year  = localDate.getYear();
				String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
				int day   = localDate.getDayOfMonth();
				String paramFilePath = baseFilePath + "CustomerOrder" + baseSeparator + year + baseSeparator + month + baseSeparator + day + baseSeparator + txtOrderManagementID + baseSeparator;
				String paramFileAccess = baseFileAccess + "CustomerOrder" + "/" + year + "/" + month + "/" + day + "/" + txtOrderManagementID + "/";
				
				Part fileOrderImage = request.getPart("txtOrderImage");
				String filename = Paths.get(fileOrderImage.getSubmittedFileName()).getFileName().toString();
				InputStream originalFile = fileOrderImage.getInputStream();
				String newFile = paramFilePath + filename;
				WriteFileAdapter.Write2(originalFile , paramFilePath, newFile);
				mdlOrderManagement.setOrderImage(paramFileAccess + filename);
			}
			
			if(lResultWriteFile != "Error Write File"){
				Integer txtItemWeight = Integer.valueOf(request.getParameter("txtItemWeight").replace(".",""));
				mdlOrderManagement.setItemWeight(txtItemWeight);
				
				String lResult = OrderManagementAdapter.InsertOrderManagement(mdlOrderManagement);
				if(lResult.contains("Success Insert Order Management")) {
					result = "SuccessInsertOrderManagement";
					tempDocNo = mdlOrderManagement.getOrderManagementID();
	            }
	            else {
	            	result = "FailedInsertOrderManagement";
	            	resultDesc = lResult;
	            }
			}else{
				result = "FailedWriteImage";
		        resultDesc = "Image Error";
			}
		}

    	if (keyBtn.equals("update")){
    		String txtOrderImage = Paths.get(request.getPart("txtOrderImage").getSubmittedFileName()).getFileName().toString();
    		
			if(txtOrderImage == null || txtOrderImage.equals("")){
				mdlOrderManagement.setOrderImage("");
			}else{
				Date date = new Date();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int year  = localDate.getYear();
				String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
				int day   = localDate.getDayOfMonth();
				String paramFilePath = baseFilePath + "CustomerOrder" + baseSeparator + year + baseSeparator + month + baseSeparator + day + baseSeparator + txtOrderManagementID + baseSeparator;
				String paramFileAccess = baseFileAccess + "CustomerOrder" + "/" + year + "/" + month + "/" + day + "/" + txtOrderManagementID + "/";
				
				Part fileOrderImage = request.getPart("txtOrderImage");
				String filename = Paths.get(fileOrderImage.getSubmittedFileName()).getFileName().toString();
				InputStream originalFile = fileOrderImage.getInputStream();
				String newFile = paramFilePath + filename;
				WriteFileAdapter.Write2(originalFile , paramFilePath, newFile);
				mdlOrderManagement.setOrderImage(paramFileAccess + filename);
			}
			
			if(lResultWriteFile != "Error Write File"){
				Integer txtItemWeight = Integer.valueOf(request.getParameter("txtItemWeight").replace(".",""));
				mdlOrderManagement.setItemWeight(txtItemWeight);
				
				String lResult = OrderManagementAdapter.UpdateOrderManagement(mdlOrderManagement);
			    if(lResult.contains("Success Update Order Management")) {
			    	result = "SuccessUpdateOrderManagement";
	            }
	            else {
	            	result = "FailedUpdateOrderManagement";
	            	resultDesc = lResult;
	            }
			}else{
				result = "FailedWriteImage";
		        resultDesc = "Image Error";
			}
		}

    	
		if (btnDelete != null){
			String lResult = OrderManagementAdapter.CancelOrderManagement(temp_txtOrderManagementID, sessionUser);
			if(lResult.contains("Success Cancel Order Management")) {
				result =  "SuccessCancelOrderManagement";
            }
            else {
            	result = "FailedCancelOrderManagement";
            	resultDesc = lResult;
            }
			doGet(request, response);
		}
		
		//schedule section
		if (keyBtn.equals("delete")){
			String lResult = OrderManagementDetailAdapter.DeleteOrderManagementDetail(txtOrderManagementID, txtOrderManagementDetailID, sessionUser);
			if(lResult.contains("Success Delete Order Management Detail")) {
				OrderManagementDetailAdapter.UpdateOrderTotalPrice(txtOrderManagementID,sessionUser);
				result =  "SuccessDeleteSchedule";
            }
            else {
            	result = "FailedDeleteSchedule";
            	resultDesc = lResult;
            }
		}
		
		if (btnCommit != null){
			Integer orderStatus = 1;
			Boolean success = OrderManagementAdapter.UpdateOrderAndDetailStatus(txtOrderManagementID, orderStatus, sessionUser);
			if(success){
				result =  "SuccessCommit";
            }
            else {
            	result = "FailedCommit";
            	resultDesc = "Failed Commit Order";
            }
			doGet(request, response);
		}
		//end of schedule section

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc+"--"+txtOrderManagementID);
		return;
	}
}
