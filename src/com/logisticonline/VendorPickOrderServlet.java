package com.logisticonline;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.DashboardAdapter;
import adapter.MenuAdapter;
import adapter.VendorOrderAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/VendorPickOrder"} , name="VendorPickOrder")
public class VendorPickOrderServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public VendorPickOrderServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    /*pDay="", */
    String pOrderType="", pStuffingDateFrom="", pStuffingDateTo = "", pVendorID="", key="";
    String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
    	Boolean CheckMenu = false;
    	String MenuURL = "VendorPickOrder";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		String queryStringDate = request.getParameter("date");
    		if(queryStringDate != null && queryStringDate.equals("today")){
    			pOrderType="EXPORT"; 
    			/*pDay = "TODAY";*/
    			pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
    			pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
    		}
    		
    		/*if(pOrderType.contentEquals("")){
    		}*/
    		
    		if(sessionRole.contentEquals("R001")){
    			pVendorID = "";
    			request.setAttribute("vendorID", "admin");
    		}
    		else{
    			pVendorID = sessionUser;
    			request.setAttribute("vendorID", pVendorID);
    			
    			//Dashboard atas
    			List<model.mdlVendorOrderDashboard> listDashboard = new ArrayList<model.mdlVendorOrderDashboard>();
    			
    			model.mdlVendorOrderDashboard dashboardDetail = VendorOrderAdapter.LoadVendorOrderDashboard(sessionUser, pStuffingDateFrom, pStuffingDateTo, pOrderType);
    			String[] boxTitleList = {"Available Order", "Picked Order", "Cancelled Order", "Processed Order"};
    			Integer[] boxValueList = {dashboardDetail.totalAvailableOrder, 
				    					dashboardDetail.totalPickedOrder, 
				    					dashboardDetail.totalCancelledOrder,
				    					dashboardDetail.totalProcessedOrder};
    			for(int i=0; i<4; i++){
    				model.mdlVendorOrderDashboard dashboard = new model.mdlVendorOrderDashboard();
    				dashboard.boxTitle = boxTitleList[i];
    				dashboard.boxValue = boxValueList[i];
    				listDashboard.add(dashboard);
    			}
    			request.setAttribute("listDashboard", listDashboard);
    		}
    		
    		//dashboard redirect
			//    		key = request.getParameter("key");
			//    		if(key != null && key.contentEquals("dashboardRedirect")){
			//    			//get next day date
			//    			DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
			//    			Date currentDate = new Date();
			//    			Calendar c = Calendar.getInstance();
			//    	        c.setTime(currentDate);
			//    	        c.add(Calendar.DATE, 1);
			//    	        Date currentDatePlusOne = c.getTime();
			//    			
			//    			pOrderType="EXPORT"; 
			//    			pStuffingDateFrom = dateFormat.format(currentDatePlusOne);
			//    			pStuffingDateTo = dateFormat.format(currentDatePlusOne);
			//    		}
    		
    		List<model.mdlOrderManagementDetail> listPickOrder = new ArrayList<model.mdlOrderManagementDetail>();
    		listPickOrder.addAll(VendorOrderAdapter.LoadAvailableOrder(pOrderType,pStuffingDateFrom,pStuffingDateTo,pVendorID));
        	request.setAttribute("listPickOrder", listPickOrder);
    		
        	String ButtonStatus;
        	if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
			//        	if(ButtonStatus.contentEquals("enabled"))
			//        		ButtonStatus = "";
			//        	else
			//        		ButtonStatus = "none";
        	
        	
        	request.setAttribute("orderType", pOrderType);
        	request.setAttribute("globalUserRole", sessionRole);
        	/*request.setAttribute("day", pDay);*/
        	request.setAttribute("stuffingDateFrom", pStuffingDateFrom);
        	request.setAttribute("stuffingDateTo", pStuffingDateTo);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("condition", result);
    		request.setAttribute("errorDescription", resultDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/vendor_pick_order.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
    	if (sessionUser == null || sessionUser == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if(keyBtn.equals("validate_quantity")) {
			Integer Quantity = Integer.parseInt(request.getParameter("quantity").replace(".", ""));
			Integer LastQuantity = Integer.parseInt(request.getParameter("lastquantity").replace(".", ""));
			String Validate = "";
			
			if( (Quantity > LastQuantity) )
	    		Validate = "disallow";
	    	else if(Quantity == 0)
	    		Validate = "disallow 0";
	    	else
	    		Validate = "allow";
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Validate);
		}
		
		if(keyBtn.equals("save")) {
			//Declare mdlVendorOrder for global
			model.mdlVendorOrder VendorOrder = new model.mdlVendorOrder();
			VendorOrder.setOrderManagementID(request.getParameter("orderid"));
			VendorOrder.setOrderManagementDetailID(request.getParameter("orderdetailid"));
			VendorOrder.setVendorID(request.getParameter("vendorid"));
			VendorOrder.setQuantity(Integer.valueOf(request.getParameter("quantity")));
			VendorOrder.setPrice( (Integer.valueOf(request.getParameter("lastprice"))/Integer.valueOf(request.getParameter("lastquantity")))*Integer.valueOf(request.getParameter("quantity")) );
			
			String lResult = "";
			
			lResult = VendorOrderAdapter.InsertVendorOrder(VendorOrder);
			
			if(lResult.contains("Success Insert Vendor Order")) {  
				result = "SuccessPick";
		    }  
		    else {
		        result = "FailedPick"; 
		        resultDesc = lResult;
		    }
			
			pOrderType = request.getParameter("ordertype");
			pStuffingDateFrom = request.getParameter("stuffingdatefrom");
	    	pStuffingDateTo = request.getParameter("stuffingdateto");
			/*pDay = request.getParameter("day");*/
			/*if(pDay.equals("TODAY")){
				pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
		    	pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
			}
			else{
				//get next day date
    			DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
    			Date currentDate = new Date();
    			Calendar c = Calendar.getInstance();
    	        c.setTime(currentDate);
    	        c.add(Calendar.DATE, 1);
    	        Date currentDatePlusOne = c.getTime();
    			pStuffingDateFrom = dateFormat.format(currentDatePlusOne);
    			pStuffingDateTo = dateFormat.format(currentDatePlusOne);
			}*/
	    	pVendorID = request.getParameter("vendorid");
			
		return;
		}
		
		if(keyBtn.equals("search")) {
			//Declare class private string
	    	pOrderType = request.getParameter("ordertype");
	    	pStuffingDateFrom = request.getParameter("stuffingdatefrom");
	    	pStuffingDateTo = request.getParameter("stuffingdateto");
	    	/*pDay = request.getParameter("day");
	    	if(pDay.equals("TODAY")){
	    		//get today date
				pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
		    	pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
			}
			else{
				//get next day date
    			DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
    			Date currentDate = new Date();
    			Calendar c = Calendar.getInstance();
    	        c.setTime(currentDate);
    	        c.add(Calendar.DATE, 1);
    	        Date currentDatePlusOne = c.getTime();
    			pStuffingDateFrom = dateFormat.format(currentDatePlusOne);
    			pStuffingDateTo = dateFormat.format(currentDatePlusOne);
			}*/
	    	pVendorID = request.getParameter("vendorid");
	    	
	    	return;
		}
		
    }
    
}
