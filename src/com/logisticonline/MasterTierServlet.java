package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.TierAdapter;
import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Tier"} , name="Tier")
public class MasterTierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterTierServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDesc = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Tier";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
    		tierList.addAll(adapter.TierAdapter.LoadTier(sessionUser) );
    		request.setAttribute("listTier", tierList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_tier.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtTierID = request.getParameter("txtTierID");
        String txtDescription = request.getParameter("txtDescription");
        String txtLongDescription = request.getParameter("txtLongDescription");
		String temp_txtTierID = request.getParameter("temp_txtTierID");

		//Declare mdlPlant for global
		model.mdlTier mdlTier = new model.mdlTier();
		mdlTier.setTierID(txtTierID);
		mdlTier.setDescription(txtDescription);
		mdlTier.setLongDescription(txtLongDescription);

		if (keyBtn.equals("save")){
			String lResult = TierAdapter.InsertTier(mdlTier,sessionUser);
			if(lResult.contains("Success Insert Tier")) {
				result = "SuccessInsertTier";
            }
            else {
            	result = "FailedInsertTier";
            	resultDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = TierAdapter.UpdateTier(mdlTier,sessionUser);

		    if(lResult.contains("Success Update Tier")) {
		    	result = "SuccessUpdateTier";
            }
            else {
            	result = "FailedUpdateTier";
            	resultDesc = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = TierAdapter.DeleteTier(temp_txtTierID,sessionUser);
			if(lResult.contains("Success Delete Tier")) {
				result =  "SuccessDeleteTier";
            }
            else {
            	result = "FailedDeleteTier";
            	resultDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}

}
