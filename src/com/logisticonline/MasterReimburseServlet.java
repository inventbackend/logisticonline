package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.RateAdapter;
import adapter.ReimburseAdapter;
import adapter.TOPAdapter;
import model.mdlReimburse;

@WebServlet(urlPatterns={"/Reimburse"} , name="Reimburse")
public class MasterReimburseServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public MasterReimburseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDesc = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		
		Boolean CheckMenu = false;
    	String MenuURL = "Rate";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		
    		//get all master rate list
    		List<mdlReimburse> listReimburse = new ArrayList<mdlReimburse>();
    		listReimburse.addAll(ReimburseAdapter.LoadReimburseAll(sessionUser));
    		request.setAttribute("listReimburse", listReimburse);
    		
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		
    		RequestDispatcher dispacther;
    		dispacther = request.getRequestDispatcher("/mainform/pages/master_reimburse.jsp");
    		dispacther.forward(request, response);
    		
    	}
    	
    	result = "";
		resultDesc = "";
		
		
    	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	
        
        if (keyBtn.equals("Delete")){
        	
        	
        	
        	String temp_txtRemID = request.getParameter("temp_txtRemID");
			String lResult = ReimburseAdapter.DeleteReimburse(temp_txtRemID,sessionUser);
			if(lResult.contains("Success Delete Reimburse")) {
				result =  "SuccessDeleteReimburse";
            }
            else {
            	result = "FailedDeleteTOP";
            	resultDesc = lResult;
            }
    		
    	} else {
    		//Declare TextBox
	    	String txtReimburseID = request.getParameter("txtReimburseID");
	        String txtName = request.getParameter("txtName");
	        String txtDescription = request.getParameter("txtDescription");
	        
	        //Declare mdlTOP for global
			mdlReimburse mdlReimburse = new model.mdlReimburse();
			mdlReimburse.setReimburseID(txtReimburseID);
			mdlReimburse.setName(txtName);
			mdlReimburse.setDescription(txtDescription);
			
			if (keyBtn.equals("save")){
				String lResult = ReimburseAdapter.InsertReimburse(mdlReimburse,sessionUser);
				if(lResult.contains("Success Insert Reimburse")) {
					result = "SuccessInsertReimburse";
	            }
	            else {
	            	result = "FailedInsertReimburse";
	            	resultDesc = lResult;
	            }
			} else if(keyBtn.equals("update")) {
				String lResult = ReimburseAdapter.UpdateReimburse(mdlReimburse,sessionUser);

			    if(lResult.contains("Success Update Reimburse")) {
			    	result = "SuccessUpdateReimburse";
	            }
	            else {
	            	result = "FailedUpdateReimburse";
	            	resultDesc = lResult;
	            }
			}
			
			
    	}
        
        
//    	if (keyBtn.equals("save")){
//    		String txtTOPID = request.getParameter("txtTOPID");
//            String txtName = request.getParameter("txtName");
//    	}
    	
    	
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
	


}
