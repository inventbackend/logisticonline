package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.Base64Adapter;
import adapter.UserAdapter;

@WebServlet(urlPatterns={"/ChangePassword"} , name="ChangePassword")
public class GetChangePassword extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private String result, userID, message = "";

	public GetChangePassword() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userID = request.getParameter("id");
		
		request.setAttribute("condition", result);
		request.setAttribute("message", message);
		
		RequestDispatcher dispacther;
		dispacther = request.getRequestDispatcher("/mainform/pages/change_password.jsp");
		dispacther.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String btnChangePassword = request.getParameter("btnChangePassword");
		
		String lPassword = request.getParameter("password");
		String lRetypePassword = request.getParameter("retypePassword");
		
		if (btnChangePassword != null) {
			
			if(lPassword == null || lRetypePassword == null) {
				result = "ERROR";
				message = "Password or Retype Password is not null";
				
				response.sendRedirect("/LogisticOnline/ChangePassword?id="+userID);
				return;
			}
			
			if(lPassword.equals(lRetypePassword)) {
				// do change password
				
				String enc = Base64Adapter.EncriptBase64(lPassword);
				System.out.println(enc);
				boolean resultChangePassword = UserAdapter.changePassword(enc, Base64Adapter.DecriptBase64(userID));
				
				if(resultChangePassword) {
					result = "SUCCESS";
					message = "Success Change Password";
				}else {
					result = "ERROR";
					message = "Database Error";
				}
				
				response.sendRedirect("/LogisticOnline/ChangePassword?id="+userID);
				return;
			} else {
				// password sama retype password tidak sesuai
				
				response.sendRedirect("/LogisticOnline/ChangePassword?id="+userID);
				return;
			}
        }
		
	}

}
