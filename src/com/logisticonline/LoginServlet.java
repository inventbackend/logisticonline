package com.logisticonline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.APIAdapter;
import adapter.LoginAdapter;
import adapter.RecaptchaAdapter;
import adapter.SchedulerAdapter;
import adapter.UserAdapter;
import model.Globals;
import model.mdlKojaParam;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Login"} , name="Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String sessionUser = "";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		if(sessionUser == null){
			String paramCaptchaSitekey = getServletContext().getInitParameter("param_capthca_sitekey");
			request.setAttribute("captcha_sitekey", paramCaptchaSitekey);
			
			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/login.jsp");
			/*response.addHeader("X
			 * -Frame-Options", "DENY");*/
			dispacther.forward(request, response);
		}
		else{
			response.sendRedirect(request.getContextPath() + "/Dashboard");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lUserId = "";
		String lUserRole = "";
		String lUserName = request.getParameter("userName");  
        String lPassword = request.getParameter("password"); 
        String btnLogin = request.getParameter("btnLogin"); 
        
        if (btnLogin != null)
        {   
        	// get reCAPTCHA request param
//        	String paramCapthaSecretkey = getServletContext().getInitParameter("param_captcha_secretkey");
//    		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
//    		System.out.println(gRecaptchaResponse);
//    		boolean verify = RecaptchaAdapter.verify(gRecaptchaResponse, paramCapthaSecretkey);
    		
    		if(true){
    			if(LoginAdapter.ValidasiLogin(lUserName, lPassword)) {
            		lUserId = UserAdapter.LoadUserId(lUserName, lPassword); //get user's user id
            		
            		Integer lUserStatus = UserAdapter.LoadUserByUserID(lUserId).get(0).IsActive; //get user's status
            		if(lUserStatus == 0){ //if user status 0, berarti akun belum diverify
            			request.setAttribute("condition", "2"); 
                    	RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/login.jsp");
                        rd.forward(request,response);
            		}
            		else if(lUserStatus == 2){ //if user status 2, berarti akun diblok
            			request.setAttribute("condition", "3"); 
                    	RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/login.jsp");
                        rd.forward(request,response);
            		}
            		else{ //selain itu maka akun sudah verify dan bisa login
            			lUserRole = UserAdapter.LoadUserRole(lUserName, lPassword);
                		
                		HttpSession session = request.getSession();
            			session.setAttribute("user", lUserId);
            			session.setAttribute("userrole", lUserRole);
            			session.setAttribute("username", lUserName);
            			session.setAttribute("userlevel", "main");
            			
            			//check if the user's level is staff and do this if it is
            			model.mdlUser staffData = UserAdapter.LoadStaffByLogin(lUserName, lPassword);
            			if(!staffData.Staff.equals("")){
            				session.setAttribute("userlevel", "staff");
            				session.setAttribute("staffRole", staffData.Role);
            			}
            			
            			//setting session to expiry in 30 mins
            			session.setMaxInactiveInterval(30*60);
                		
                        RequestDispatcher rd=request.getRequestDispatcher("/Dashboard");
                        rd.forward(request,response);
            		}
                }  
                else { //kondisi jika username atau password tidak match
                	request.setAttribute("condition", "1");
                	String paramCaptchaSitekey = getServletContext().getInitParameter("param_capthca_sitekey");
        			request.setAttribute("captcha_sitekey", paramCaptchaSitekey);
                	RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/login.jsp");
                    rd.forward(request,response);
                }  
            	return;
    		}
    		else{
//    			PrintWriter out = response.getWriter();
//    			out.print("<script type=\"text/javascript\">");
//    			out.print("alert('You missed the Captcha. Try again');");
//    			out.print("location='"+ request.getContextPath() + "/Login';");
//    			out.print("</script>");
    		}
        }
	}
	
}
