package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.OrderManagementAdapter;
import helper.LongProcess;

@WebServlet(urlPatterns={"/progressKojaShipper"} , name="progressKojaShipper")
public class ProgressBarKojaShipperServlet extends HttpServlet {
	
private static final long serialVersionUID = 1490947492185481844L;
int loading = 0;
String status = "";

	public ProgressBarKojaShipperServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	      throws ServletException, IOException {
		  
		  	HttpSession session = req.getSession(true);
		  	String sessionUser = (String) session.getAttribute("user");
		  	
		  	String orderID = req.getParameter("orderID");
		  	Integer orderTpsStatus = OrderManagementAdapter.LoadTpsDataByOrder(orderID, sessionUser).getTpsBillingStatus();
        	if(orderTpsStatus == 13){
        		loading = 50;
        		status = "Validate TID Success";
        	}
        	else if(orderTpsStatus == 3){
        		loading = 100;
        		status = "Get Doc NPE Success";
        	}
		  	
		  	session.setAttribute("longProcess", loading);
		  	
		  	LongProcess longProcess = new LongProcess((int)session.getAttribute("longProcess"));
		  	
		  	int progress = longProcess.getProgress();
		  	model.mdlProgressBar mdlProgressBar = new model.mdlProgressBar();
		  	mdlProgressBar.setPercentage(progress);
		  	mdlProgressBar.setStatus(status);
		  	
		  	String json = "";
		  	Gson gson = new Gson();
		  	json = gson.toJson(mdlProgressBar);

	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(json);
	        
	        if (loading == 100) {
				loading = 0;
			}
	  }
	  
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		      throws ServletException, IOException {
	  }

}

