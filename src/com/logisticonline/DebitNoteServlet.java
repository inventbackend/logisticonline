package com.logisticonline;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

import adapter.APIAdapter;
import adapter.DebitNoteAdapter;
import adapter.DebitNoteReimburseDetailAdapter;
import adapter.DepoAdapter;
import adapter.MenuAdapter;
import adapter.PortAdapter;
import adapter.ReimburseAdapter;
import adapter.ShippingAdapter;
import adapter.TierAdapter;
import adapter.VendorAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlConfirmPaymentDebitNote;
import model.mdlDebitNoteDetail;
import model.mdlDebitNoteReimburseDetail;
import model.mdlDebitNoteSettingInvoiceParam;
import model.mdlReimburse;

@WebServlet(urlPatterns = { "/DebitNote" }, name = "DebitNote")
public class DebitNoteServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public DebitNoteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	String pStatus = "", pStuffingDateFrom = "", pStuffingDateTo = "";
	String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");

		Boolean CheckMenu = false;
		String MenuURL = "DebitNote";
		if (sessionLevel != null) {
			if (sessionLevel.equals("main"))
				CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
			else if (sessionLevel.equals("staff")) {
				staffRole = (String) session.getAttribute("staffRole");
				CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
			}
		}

		if (CheckMenu == false) {
			RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
			dispacther.forward(request, response);
		} else {
			String queryStringDate = request.getParameter("date");
			if (queryStringDate != null && queryStringDate.equals("today")) {
				pStatus = "Unpaid";
				pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd",
						"dd MMM yyyy");
				;
				pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd",
						"dd MMM yyyy");
				;
			}

			List<model.mdlDebitNote> debitNoteList = new ArrayList<model.mdlDebitNote>();

			Integer totalAmountUnpaid = 0;

			if (sessionRole.equals("R001")) { // admin
				debitNoteList.addAll(DebitNoteAdapter.LoadDebitNote("", pStuffingDateFrom, pStuffingDateTo, pStatus));
				totalAmountUnpaid = DebitNoteAdapter.LoadTotalAmountUnpaid("", pStuffingDateFrom, pStuffingDateTo);
			} else if (sessionRole.equals("R002")) {
				System.out.println(LocalDateTime.now().toString());
				debitNoteList.addAll(DebitNoteAdapter.LoadDebitNoteNotAdmin(sessionUser, pStuffingDateFrom,
						pStuffingDateTo, pStatus));
				totalAmountUnpaid = DebitNoteAdapter.LoadTotalAmountUnpaid(sessionUser, pStuffingDateFrom,
						pStuffingDateTo);
			}
			request.setAttribute("listDebitNote", debitNoteList);
			request.setAttribute("totalAmountUnpaid", totalAmountUnpaid);

			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
			request.setAttribute("listVendor", vendorList);

			List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
			tierList.addAll(TierAdapter.LoadTier(sessionUser));
			request.setAttribute("listTier", tierList);

			String ButtonStatus;
			if (sessionLevel.equals("main"))
				ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
			else
				ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

			request.setAttribute("condition", result);
			request.setAttribute("conditionDescription", resultDesc);
			request.setAttribute("buttonstatus", ButtonStatus);
			request.setAttribute("globalUserID", sessionUser);
			request.setAttribute("globalUserRole", sessionRole);
			request.setAttribute("status", pStatus);
			request.setAttribute("stuffingDateFrom", pStuffingDateFrom);
			request.setAttribute("stuffingDateTo", pStuffingDateTo);

			RequestDispatcher dispacther;
			result = "";
			resultDesc = "";
			dispacther = request.getRequestDispatcher("/mainform/pages/debit_note.jsp");
			dispacther.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");

		// Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null) {
			keyBtn = new String("");
		}

		if (keyBtn.equals("save")) {
			// Declare TextBox
			String[] debitNoteNumber = request.getParameterValues("debitNoteNumber[]");
			String paymentDate = ConvertDateTimeHelper.formatDate(request.getParameter("paymentDate"), "dd MMM yyyy",
					"yyyy-MM-dd");

			String lResult = DebitNoteAdapter.ConfirmPayment(debitNoteNumber, paymentDate, sessionUser);
			if(lResult.contains("Success Confirm Payment")) {
				result =  "SuccessConfirm";
            }
            else {
            	result = "FailedConfirm";
            	resultDesc = lResult;
            }

			response.setContentType("application/text");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result + "--" + resultDesc);

		}

		if (keyBtn.equals("saveConfirmPayment")) {
			// Declare TextBox
			String fullPayment = request.getParameter("fullPayment");
			String partialPayment = request.getParameter("partialPayment");

			Gson gson = new Gson();

			Type listType = new TypeToken<ArrayList<mdlConfirmPaymentDebitNote>>() {
			}.getType();
			
			List<mdlConfirmPaymentDebitNote> listFullPayment = gson.fromJson(fullPayment, listType);
			List<mdlConfirmPaymentDebitNote> listPartialPayment = gson.fromJson(partialPayment, listType);
			
			String lResult = DebitNoteAdapter.ConfirmFullPayment(listFullPayment, sessionUser);
			String lResult2 = DebitNoteAdapter.ConfirmPartialPaymentV2(listPartialPayment, sessionUser);
			
			if(lResult.contains("Success Confirm Payment") || lResult2.contains("Success Confirm Partial Payment")) {
				result =  "SuccessConfirm";
            }
            else {
            	result = "FailedConfirm";
            	resultDesc = lResult;
            }

			response.setContentType("application/text");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result + "--" + resultDesc);

		}

		if (keyBtn.equals("SavePartialPayment")) {
			// Declare TextBox
			String debitNoteNumber = request.getParameter("debitNoteNumber");
			String paymentDate = ConvertDateTimeHelper.formatDate(request.getParameter("paymentDate"), "dd MMM yyyy",
					"yyyy-MM-dd");
			Double amountPaid = Double.valueOf(request.getParameter("amountPaid"));

			String lResult = DebitNoteAdapter.ConfirmPartialPayment(debitNoteNumber, paymentDate, amountPaid,
					sessionUser);
			if (lResult.contains("Success Confirm Partial Payment")) {
				result = "SuccessConfirmPartialPayment";
			} else {
				result = "FailedConfirmPartialPayment";
				resultDesc = lResult;
			}

			response.setContentType("application/text");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result + "--" + resultDesc);

		}

		if (keyBtn.equals("search")) {
			// Declare class private string
			pStatus = request.getParameter("statusSearch");
			pStuffingDateFrom = request.getParameter("stuffingdatefrom");
			pStuffingDateTo = request.getParameter("stuffingdateto");
		}

		if (keyBtn.equals("deleteReimburse")) {
			String dnr = request.getParameter("debitNoteReimburseID");
			;
			DebitNoteReimburseDetailAdapter.DeleteDebitNoteReimburseDetail(dnr, sessionUser);
		}

		if (keyBtn.equals("saveReimburse")) {

			String reimburseID = request.getParameter("idReimburse");
			String reimburseAmount = request.getParameter("reimburseAmount");
			String debitNoteNumber = request.getParameter("debitNoteNumber");

			mdlDebitNoteReimburseDetail debitNoteReimburseDetail = new mdlDebitNoteReimburseDetail();
			debitNoteReimburseDetail.setDebitNoteNumber(debitNoteNumber);
			debitNoteReimburseDetail.setReimburseID(reimburseID);
			debitNoteReimburseDetail.setReimburseAmount(Integer.parseInt(reimburseAmount));

			System.out.println(debitNoteReimburseDetail.getDebitNoteNumber());
			System.out.println(debitNoteReimburseDetail.getReimburseID());
			System.out.println(debitNoteReimburseDetail.getReimburseAmount());

			String resultSave = DebitNoteReimburseDetailAdapter
					.InsertReimburseDebitNoteReimburseDetail(debitNoteReimburseDetail, sessionUser);
			String jsonlistDistrict = null;
			if (resultSave.equalsIgnoreCase("Success Insert Debit Note Reimburse Detail")) {
				mdlReimburse mdlReimburse = ReimburseAdapter.LoadReimburseByID(reimburseID, sessionUser);
				List<mdlDebitNoteReimburseDetail> DistrictList = new ArrayList<>();
				debitNoteReimburseDetail.setNameReimburse(mdlReimburse.getName());
				DistrictList.add(debitNoteReimburseDetail);

				Gson gson = new Gson();
				jsonlistDistrict = gson.toJson(DistrictList);
			}

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonlistDistrict);

		}

		if (keyBtn.equals("updateDebitNoteInvoiceSetting")) {
			Gson gson = new Gson();
//			String debitNoteNumber, int amount, int isFinalInvoice, int customClearance,
//			int finalRate, int additionalPrice, int ppn, int pph, String user
			String debitNoteNumber = request.getParameter("debitNoteNumber");
			String isFinalInvoice = request.getParameter("isFinalInvoice");
			String customClearance = request.getParameter("customClearance");
			String finalRate = request.getParameter("finalRate");
			String additionalPrice = request.getParameter("additionalPrice");
			String ppn = request.getParameter("ppn");
			String pph23 = request.getParameter("pph23");
			String debitNoteDetail = request.getParameter("debitNoteDetail");
			
			
			System.out.println(debitNoteDetail);
			
			// convert json string to model list
			Type debitNoteDetailListType = new TypeToken<ArrayList<mdlDebitNoteDetail>>(){}.getType();
			List<mdlDebitNoteDetail> ss = gson.fromJson(debitNoteDetail, debitNoteDetailListType);
			
			for (mdlDebitNoteDetail mdlDebitNoteDetail : ss) {
				System.out.println(mdlDebitNoteDetail.getFinalRate());
				System.out.println(mdlDebitNoteDetail.getDebitNoteDetailID());
				DebitNoteAdapter.updateFinalRateByDebitNoteNumberDetailID(mdlDebitNoteDetail.getDebitNoteDetailID(), mdlDebitNoteDetail.getFinalRate(), sessionUser);
			}
			
			int amount = DebitNoteAdapter.amountInvoice(request.getParameter("debitNoteNumber"), sessionUser);

			
			mdlDebitNoteSettingInvoiceParam param = new mdlDebitNoteSettingInvoiceParam();
			param.setDebitNoteNumber(debitNoteNumber);
			param.setIsFinalInvoice(isFinalInvoice != null ? Boolean.parseBoolean(isFinalInvoice) : false);
			param.setCustomClearance(customClearance != null ? Integer.parseInt(customClearance) : 0);
			param.setFinalRate(finalRate != null ? Integer.parseInt(finalRate) : 0);
			param.setAdditionalPrice(additionalPrice != null ? Integer.parseInt(additionalPrice) : 0);
			param.setPpn(ppn != null ? Integer.parseInt(ppn) : 0);
			param.setPph23(pph23 != null ? Integer.parseInt(pph23) : 0);
			param.setAmount(amount);

			String resultSave = DebitNoteAdapter.UpdateDebitNoteSettingInvoice(param, sessionUser);
			String jsonlistDistrict = null;
			if (resultSave.equalsIgnoreCase("Success Update Setting Invoice")) {
				List<mdlDebitNoteSettingInvoiceParam> DistrictList = new ArrayList<>();
				DistrictList.add(param);

				jsonlistDistrict = gson.toJson(DistrictList);
			}

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonlistDistrict);
			 

		}

		return;
	}

}
