package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.DepoAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Depo"} , name="Depo")
public class MasterDepoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterDepoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Depo";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlDepo> depoList = new ArrayList<model.mdlDepo>();
    		depoList.addAll(adapter.DepoAdapter.LoadDepo(sessionUser) );
    		request.setAttribute("listDepo", depoList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_depo.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtDepoID = request.getParameter("txtDepoID");
        String txtName = request.getParameter("txtName");
        String txtAddress = request.getParameter("txtAddress");
		String temp_txtDepoID = request.getParameter("temp_txtDepoID");

		//Declare mdlDepo for global
		model.mdlDepo mdlDepo = new model.mdlDepo();
		mdlDepo.setDepoID(txtDepoID);
		mdlDepo.setName(txtName);
		mdlDepo.setAddress(txtAddress);

		if (keyBtn.equals("save")){
			String lResult = DepoAdapter.InsertDepo(mdlDepo,sessionUser);
			if(lResult.contains("Success Insert Depo")) {
				result = "SuccessInsertDepo";
            }
            else {
            	result = "FailedInsertDepo";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = DepoAdapter.UpdateDepo(mdlDepo,sessionUser);

		    if(lResult.contains("Success Update Depo")) {
		    	result = "SuccessUpdateDepo";
            }
            else {
            	result = "FailedUpdateDepo";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = DepoAdapter.DeleteDepo(temp_txtDepoID,sessionUser);
			if(lResult.contains("Success Delete Depo")) {
				result =  "SuccessDeleteDepo";
            }
            else {
            	result = "FailedDeleteDepo";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
