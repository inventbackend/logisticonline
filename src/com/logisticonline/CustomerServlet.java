package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ContainerTypeAdapter;
import adapter.CustomerAdapter;
import adapter.FactoryAdapter;
import adapter.MenuAdapter;
import model.Globals;
import model.mdlCustomer;

@WebServlet(urlPatterns={"/Customer"} , name="Customer")
public class CustomerServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public CustomerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDescription = "";
	//tempCustomerID, tempCustomerName
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu=false;
    	String MenuURL = "Customer";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String ButtonStatus, ShowDetailButtonStatus;
    		
    		if(sessionRole.contentEquals("R001")){
    			List<model.mdlCustomer> customerList = new ArrayList<model.mdlCustomer>();
        		customerList.addAll(CustomerAdapter.LoadCustomer(sessionUser));
        		request.setAttribute("listCustomer", customerList);
        		if(sessionLevel.equals("main"))
        			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
        		else
        			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
        		ShowDetailButtonStatus = ButtonStatus;
    		}
    		else if(sessionRole.contentEquals("R002")){ 
    			List<model.mdlCustomer> customerList = CustomerAdapter.LoadCustomerByID(sessionUser);
    			String pi = "null";
    			
    			for (mdlCustomer mdlCustomer : customerList) {
					pi = mdlCustomer.getProductInvoice();
				}
        		
    			System.out.println(pi);
        		
        		request.setAttribute("listCustomer", customerList);
        		
        		
        		
    			ButtonStatus = "none";
    			if(sessionLevel.equals("main"))
    				ShowDetailButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    			else
    				ShowDetailButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		}
    		else{
    			List<model.mdlCustomer> customerList = new ArrayList<model.mdlCustomer>();
        		request.setAttribute("listCustomer", customerList);
        		ButtonStatus = "none";
        		if(sessionLevel.equals("main"))
    				ShowDetailButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    			else
    				ShowDetailButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		}
    		
    		
    		if(sessionRole.contentEquals("R001") || sessionRole.contentEquals("R002")){
    			List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
        		provinceList.addAll(adapter.DistrictAdapter.LoadProvince(sessionUser) );
        		request.setAttribute("listProvince", provinceList);
        		
				//List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
				//districtList.addAll(adapter.DistrictAdapter.LoadDistrict(sessionUser) );
				//request.setAttribute("listDistrict", districtList);
        		
        		List<model.mdlTOP> topList = new ArrayList<model.mdlTOP>();
        		topList.addAll(adapter.TOPAdapter.LoadTOP(sessionUser) );
        		request.setAttribute("listTOP", topList);
        		
				//List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
				//tierList.addAll(adapter.TierAdapter.LoadTier(sessionUser));
				//request.setAttribute("listTier", tierList);
    		}

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("showDetailButtonStatus", ShowDetailButtonStatus);
    		request.setAttribute("userRole", sessionRole);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_customer.jsp");
    		dispacther.forward(request, response);
			//    		RequestDispatcher dispacther;
			//    		if(result == "SuccessInsertCustomer")
			//    		{
			//    			response.sendRedirect(request.getContextPath() + "/Factory?custid="+tempCustomerID+"&custname="+tempCustomerName+"&condition="+result+"");
			//    		}
			//    		else
			//    		{
			//    			dispacther = request.getRequestDispatcher("/mainform/pages/master_customer.jsp");
			//    			dispacther.forward(request, response);
			//    		}
    	}
    	
    	result = "";
		resultDescription = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}    	

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	
    	if (keyBtn.equals("DeleteFactory")) { //Delete Factory
			String delCustomerID = request.getParameter("delCustomerID");
			if(sessionUser.equals(delCustomerID) || sessionRole.equals("R001")){
				String delFactoryID = request.getParameter("delFactoryID");
				String lResult = FactoryAdapter.DeleteFactory(delCustomerID, delFactoryID, sessionUser);

			    if(lResult.contains("Success Delete Factory")) {
			    	result = "SuccessDeleteFactory";
	            }
	            else {
	            	result = "FailedDeleteFactory";
	            	resultDescription = lResult;
	            }
			}
			else{
				result = "FailedDeleteFactory";
            	resultDescription = "Processed not authorized";
			}
		}
    	else if(keyBtn.equals("DeleteCustomer")) {
    		//delete process
        	String temp_txtCustomerID = request.getParameter("temp_txtCustomerID");
    		String lResult = CustomerAdapter.DeleteCustomer(temp_txtCustomerID, sessionUser);
			if(lResult.contains("Success Delete Customer")) {
				result =  "SuccessDeleteCustomer";
            }
            else {
            	result = "FailedDeleteCustomer";
            	resultDescription = lResult;
            }
			//doGet(request, response);
			//return;
    	}
    	else if(keyBtn.equals("save") || keyBtn.equals("update")){
    		//Declare mdlCustomer for global (save or update proccess)
    		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
    		if(request.getParameter("txtCustomerID") == null || request.getParameter("txtCustomerID").contentEquals(""))
    			mdlCustomer.setCustomerID(CustomerAdapter.CreateCustomerID(sessionUser));
    		else
    			mdlCustomer.setCustomerID(request.getParameter("txtCustomerID"));
	    		mdlCustomer.setCustomerName(request.getParameter("txtCustomerName"));
	    		mdlCustomer.setPIC1(request.getParameter("txtPIC1"));
	    		mdlCustomer.setPIC2(request.getParameter("txtPIC2"));
	    		mdlCustomer.setPIC3(request.getParameter("txtPIC3"));
	    		mdlCustomer.setCustomerAddress(request.getParameter("txtAddress"));
	    		mdlCustomer.setEmail(request.getParameter("txtEmail"));
	    		mdlCustomer.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
	    		mdlCustomer.setMobilePhone(request.getParameter("txtMobilePhone"));
	    		mdlCustomer.setNPWP(request.getParameter("txtNPWP"));
	    		mdlCustomer.setTDP(request.getParameter("txtTDP"));
	    		mdlCustomer.setDomicile(request.getParameter("txtDomicile"));
	    		mdlCustomer.setDistrict("");
	    		mdlCustomer.setPostalCode(request.getParameter("slDistrict"));
	    		mdlCustomer.setBillingAddress(request.getParameter("txtBillingAddress"));
	    		mdlCustomer.setTOP(request.getParameter("slTOP"));
	    		mdlCustomer.setCreditLimit(Double.valueOf(request.getParameter("txtCreditLimit").replace(".", "")));
	    		mdlCustomer.setBankName(request.getParameter("txtBankName"));
	    		mdlCustomer.setBankAcc(request.getParameter("txtBankAcc"));
	    		mdlCustomer.setBankBranch(request.getParameter("txtBankBranch"));
	    		
	    		List<model.mdlCustomer> customerList = new ArrayList<model.mdlCustomer>();
        		customerList.addAll(CustomerAdapter.LoadCustomerByID(sessionUser));
        		int ppn = 0;
        		int pph23 = 0;
        		String productInvoice = null;
        		int customClearance = 0;
        		
        		for (mdlCustomer mdlCustomerRoleUser : customerList) {
					ppn = mdlCustomerRoleUser.getPpn();
					pph23 = mdlCustomerRoleUser.getPph23();
					customClearance = mdlCustomerRoleUser.getCustomClearance();
					productInvoice = mdlCustomerRoleUser.getProductInvoice();
				}
	    		
	    		if(request.getParameter("txtPPN")!=null) {
	    			ppn = Integer.parseInt(request.getParameter("txtPPN"));
	    		}
	    		
	    		if(request.getParameter("txtPPh23")!=null) {
	    			pph23 = Integer.parseInt(request.getParameter("txtPPh23"));
	    		}
	    		
	    		if (request.getParameter("txtModelPricing")!=null) {
					productInvoice = request.getParameter("txtModelPricing");
				}
	    		if (request.getParameter("txtCustomClearance")!=null) {
					customClearance = Integer.parseInt(request.getParameter("txtCustomClearance"));
				}
	    		mdlCustomer.setPpn(ppn);
	    		mdlCustomer.setPph23(pph23);
	    		mdlCustomer.setProductInvoice(productInvoice);
	    		mdlCustomer.setCustomClearance(customClearance);
	    		
	    		
	    		System.out.println("ss");
	    		

    		if (keyBtn.equals("save")){
    			String lResult = CustomerAdapter.InsertCustomer(mdlCustomer, sessionUser);
    			if(lResult.contains("Success Insert Customer")) {
    				result = "SuccessInsertCustomer";
                }
                else {
                	result = "FailedInsertCustomer";
                	resultDescription = lResult;
                }
    		}
    		else if (keyBtn.equals("update")){
    		    String lResult = CustomerAdapter.UpdateCustomer(mdlCustomer, sessionUser);

    		    if(lResult.contains("Success Update Customer")) {
    		    	result = "SuccessUpdateCustomer";
                }
                else {
                	result = "FailedUpdateCustomer";
                	resultDescription = lResult;
                }
    		}
    	}
		
    	//tempCustomerID = mdlCustomer.CustomerID;
    	//tempCustomerName = mdlCustomer.CustomerName;

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
	
}
