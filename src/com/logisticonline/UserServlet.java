package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.Base64Adapter;
import adapter.ContainerTypeAdapter;
import adapter.CustomerAdapter;
import adapter.EmailAdapter;
import adapter.MenuAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import model.Globals;
import model.mdlUser;

@WebServlet(urlPatterns={"/User"} , name="User")
public class UserServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "User";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String ButtonStatus="";
    		String InputStatus="enabled";
    		String ButtonEditStatus="";
    		
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		ButtonEditStatus = ButtonStatus;
    		
    		if(sessionRole.contentEquals("R001")){
    			List<model.mdlUser> userList = new ArrayList<model.mdlUser>();
    			userList.addAll(UserAdapter.LoadUser(sessionUser));
        		request.setAttribute("listUser", userList);
    		}
    		else{
    			List<model.mdlUser> user = new ArrayList<model.mdlUser>();
    			user.addAll(UserAdapter.LoadUserByUserID(sessionUser));
        		request.setAttribute("listUser", user);
        		
        		InputStatus = "disabled";
        		ButtonStatus = "none";
    		}
    		
    		List<model.mdlRole> roleList = new ArrayList<model.mdlRole>();
    		roleList.addAll(RoleAdapter.LoadRole(sessionUser) );
    		request.setAttribute("listRole", roleList);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("buttonEditStatus", ButtonEditStatus);
    		request.setAttribute("inputStatus", InputStatus);
    		request.setAttribute("userRole", sessionRole);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_user.jsp");
    		dispacther.forward(request, response);
    	}
    	
    	result = "";
    	resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
    	String btnDelete = request.getParameter("btnDelete");

    	if (sessionUser == null || sessionUser == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare mdlUser for global
    	model.mdlUser mdlUser = new model.mdlUser();
    			
    	//Declare parameter fot delete process
		String temp_txtUserID = request.getParameter("temp_txtUserID");
		//delete proccess
		if (btnDelete != null){
			if(sessionUser.equals(temp_txtUserID) || sessionRole.equals("R001")){
				mdlUser user = EmailAdapter.LoadUserEmailByUserID(temp_txtUserID, sessionUser);
				String lResult = UserAdapter.DeleteUser(temp_txtUserID, sessionUser);
				if(lResult.contains("Success Delete User")) {
					result =  "SuccessDeleteUser";
					//send email
					if(user.Role != null && (user.Role.contentEquals("R002") || user.Role.contentEquals("R003"))){
						mdlUser.setUserEmail(user.getUserEmail());
						mdlUser.IsActive = 3;
						EmailAdapter.SendEmail(mdlUser, sessionUser);
					}
	            }
	            else {
	            	result = "FailedDeleteUser";
	            	resultDesc = lResult;
	            }
			}
			else{
				result = "FailedDeleteUser";
            	resultDesc = "Processed not authorized";
			}

			doGet(request, response);
			return;
		}

		//save or update proccess
		mdlUser.setUsername(request.getParameter("txtUserName"));
		mdlUser.setPassword(Base64Adapter.EncriptBase64(request.getParameter("txtPassword")));
		mdlUser.setUnEncryptedPassword(request.getParameter("txtPassword"));
		mdlUser.setRole(request.getParameter("slRole"));
		if(request.getParameter("slIsActive").contentEquals("1"))
			mdlUser.setIsActive(1);
		else if(request.getParameter("slIsActive").contentEquals("2"))
			mdlUser.setIsActive(2);
		else if(request.getParameter("slIsActive").contentEquals("0"))
			mdlUser.setIsActive(0);
		mdlUser.setUserId(request.getParameter("txtUserID"));

		if (keyBtn.equals("save")){
			String lResult = UserAdapter.InsertUser(mdlUser, sessionUser);
			if(lResult.contains("Success Insert User")) {
				result = "SuccessInsertUser";
				//send email if account is activated when insert and user role is customer or vendor
				if( (mdlUser.getIsActive() == 1) && (mdlUser.getRole().contentEquals("R002") || mdlUser.getRole().contentEquals("R003")) ){
					mdlUser.setUserEmail(EmailAdapter.LoadUserEmail(mdlUser, sessionUser) );
					EmailAdapter.SendEmail(mdlUser, sessionUser);
				}
            }
            else {
            	result = "FailedInsertUser";
            	resultDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
			if(sessionUser.equals(mdlUser.UserId) || sessionRole.equals("R001")){
				//jika yang mengupdate dari role user shipper/trucker, maka rolenya diset default sesuai sessionnya
				//untuk menghindari terjadinya manipulasi role/account take over
				if(sessionRole.equals("R002") || sessionRole.equals("R003"))
					mdlUser.Role = sessionRole;
				
				//check last user's isactive status
		    	Integer lastStatus = UserAdapter.LoadUserByUserID(mdlUser.UserId).get(0).IsActive;
		    	
			    String lResult = UserAdapter.UpdateUser(mdlUser, sessionUser);
			    if(lResult.contains("Success Update User")) {
			    	result = "SuccessUpdateUser";
			    	
			    	//check if user's status isactive is changed or not
			    	//if changed
			    	if(lastStatus != mdlUser.IsActive){
			    		//send email
						if( (mdlUser.getRole().contentEquals("R002") || mdlUser.getRole().contentEquals("R003")) ){
							mdlUser.setUserEmail(EmailAdapter.LoadUserEmail(mdlUser,sessionUser) );
							EmailAdapter.SendEmail(mdlUser,sessionUser);
						}
			    	}
	            }
	            else {
	            	result = "FailedUpdateUser";
	            	resultDesc = lResult;
	            }
			}
			else{
				result = "FailedUpdateUser";
            	resultDesc = "Processed not authorized";
			}
		}
		
		
		//staff save and update process
		mdlUser.setStaff(request.getParameter("txtStaff"));
		mdlUser.setTempStaff(request.getParameter("txtHiddenStaff"));
		
		if(sessionUser.equals(mdlUser.UserId) || sessionRole.equals("R001")){
			if (keyBtn.equals("saveStaff")){
			    String lResult = UserAdapter.InsertStaff(mdlUser, sessionUser);
			    if(lResult.contains("Success Insert Staff")) {
			    	result = "SuccessInsertStaff";
	            }
	            else {
	            	result = "FailedInsertStaff";
	            	resultDesc = lResult;
	            }
			}
			
			if (keyBtn.equals("updateStaff")){
			    String lResult = UserAdapter.UpdateStaff(mdlUser, sessionUser);
			    if(lResult.contains("Success Update Staff")) {
			    	result = "SuccessUpdateStaff";
	            }
	            else {
	            	result = "FailedUpdateStaff";
	            	resultDesc = lResult;
	            }
			}
			
			if (keyBtn.equals("deleteStaff")){
			    String lResult = UserAdapter.DeleteStaff(mdlUser, sessionUser);
			    if(lResult.contains("Success Delete Staff")) {
			    	result = "SuccessDeleteStaff";
	            }
	            else {
	            	result = "FailedDeleteStaff";
	            	resultDesc = lResult;
	            }
			}
		}
		else{
			result = "FailedAuthorize";
        	resultDesc = "Processed not authorized";
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
	
}
