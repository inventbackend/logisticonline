package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.RateAdapter;
import adapter.TierAdapter;
import adapter.ContainerTypeAdapter;
import adapter.DistrictAdapter;
import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Rate"} , name="Rate")
public class MasterRateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public MasterRateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Rate";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		//get all master rate list
    		List<model.mdlRate> rateList = new ArrayList<model.mdlRate>();
    		rateList.addAll(RateAdapter.LoadRate(sessionUser) );
    		request.setAttribute("listRate", rateList);
    		//get all district for combobox
    		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
    		districtList.addAll(DistrictAdapter.LoadDistrict(sessionUser) );
    		request.setAttribute("listDistrict", districtList);
    		//get all tier for combobox
    		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
    		tierList.addAll(TierAdapter.LoadTier(sessionUser) );
    		request.setAttribute("listTier", tierList);
    		//get all container type for combobox
    		List<model.mdlContainerType> containerTypeList = new ArrayList<model.mdlContainerType>();
    		containerTypeList.addAll(ContainerTypeAdapter.LoadContainerType(sessionUser) );
    		request.setAttribute("listContainerType", containerTypeList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_rate.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

		if (!keyBtn.equals("Delete") ){
			if(keyBtn.equals("save") || keyBtn.equals("update")){
				//Declare TextBox
		    	String txtRateID = request.getParameter("txtRateID");
		        String txtName = request.getParameter("txtName");
		        String txtRole = request.getParameter("txtRole");
		        String txtDistrictID = request.getParameter("txtDistrictID");
		        String txtDistrictName = request.getParameter("txtDistrictName");
		        String txtTierID = request.getParameter("txtTierID");
		        String txtTierName = request.getParameter("txtTierName");
		        String txtContainerTypeID = request.getParameter("txtContainerTypeID");
		        String txtContainerTypeName = request.getParameter("txtContainerTypeName");
		        String tempPrice = request.getParameter("txtPrice").replace(".", "");
		        Integer txtPrice = tempPrice.equals("") ? 0 : Integer.parseInt(tempPrice);
		        
		        //Declare mdlRate for global
				model.mdlRate mdlRate = new model.mdlRate();
				mdlRate.setRateID(txtRateID);
				mdlRate.setName(txtName);
				mdlRate.setDistrictID(txtDistrictID);
				mdlRate.setDistrictName(txtDistrictName);
				mdlRate.setTierID(txtTierID);
				mdlRate.setTierName(txtTierName);
				mdlRate.setContainerTypeID(txtContainerTypeID);
				mdlRate.setContainerTypeName(txtContainerTypeName);
				mdlRate.setPrice(txtPrice);
				mdlRate.setRoleID(txtRole);
				
				if (keyBtn.equals("save")){
					String lResult = RateAdapter.InsertRate(mdlRate,sessionUser);
					if(lResult.contains("Success Insert Rate")) {
						result = "SuccessInsertRate";
		            }
		            else {
		            	result = "FailedInsertRate";
		            	resultDescription = lResult;
		            }
				}
				
				if (keyBtn.equals("update")){
				    String lResult = RateAdapter.UpdateRate(mdlRate,sessionUser);
				    if(lResult.contains("Success Update Rate")) {
				    	result = "SuccessUpdateRate";
		            }
		            else {
		            	result = "FailedUpdateRate";
		            	resultDescription = lResult;
		            }
				}
			}
			else{
				//save or update customer final rate proccess
				model.mdlRate rate = new model.mdlRate();
				rate.setCustomerID(request.getParameter("txtCustomerID"));
				rate.setFactoryID(request.getParameter("txtFactoryID"));
				rate.setContainerTypeID(request.getParameter("slContainerType"));
				rate.setFactoryID(request.getParameter("txtFactoryID"));
				String tempFinalPrice = request.getParameter("txtFinalRate").replace(".", "");
		        Integer txtFinalPrice = tempFinalPrice.equals("") ? 0 : Integer.parseInt(tempFinalPrice);
				rate.setFinalPrice(txtFinalPrice);
				
				if (keyBtn.equals("saveFinalRate")){
					String lResult = RateAdapter.InsertCustomerFinalRate(rate,sessionUser);
					if(lResult.contains("Success Insert Final Rate")) {
						result = "SuccessInsertFinalRate";
		            }
		            else {
		            	result = "FailedInsertFinalRate";
		            	resultDescription = lResult;
		            }
				}
				
				if (keyBtn.equals("updateFinalRate")){
				    String lResult = RateAdapter.UpdateCustomerFinalRate(rate,sessionUser);
				    if(lResult.contains("Success Update Final Rate")) {
				    	result = "SuccessUpdateFinalRate";
		            }
		            else {
		            	result = "FailedUpdateFinalRate";
		            	resultDescription = lResult;
		            }
				}
			}
		}
		else{
			String temp_txtRateID = request.getParameter("temp_txtRateID");
			String lResult = RateAdapter.DeleteRate(temp_txtRateID,sessionUser);
			if(lResult.contains("Success Delete Rate")) {
				result =  "SuccessDeleteRate";
            }
            else {
            	result = "FailedDeleteRate";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
