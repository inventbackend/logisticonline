package com.logisticonline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.Base64Adapter;
import adapter.CustomerAdapter;
import adapter.RecaptchaAdapter;

@WebServlet(urlPatterns={"/GeneralRegister"} , name="GeneralRegister")
public class GeneralRegisterServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public GeneralRegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	model.mdlCustomer customer = new model.mdlCustomer();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispacther;
		if(customer.PIC1 != null)
			response.sendRedirect(request.getContextPath() + "/Register?customer=" + customer);
		else{
			dispacther = request.getRequestDispatcher("/mainform/pages/general_register.jsp");
			dispacther.forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get reCAPTCHA request param
		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
		System.out.println(gRecaptchaResponse);
		boolean verify = RecaptchaAdapter.verify(gRecaptchaResponse,"");
		
		if(verify){
			customer = new model.mdlCustomer();
			customer.setPIC1(request.getParameter("txtPIC1"));
			customer.setEmail(request.getParameter("txtEmail"));
			customer.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
			customer.setPassword(Base64Adapter.EncriptBase64(request.getParameter("txtPassword")));
			            
            doGet(request, response);
            return;	        
		}
		else{
			PrintWriter out = response.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("alert('You missed the Captcha. Try again');");
			out.println("location='"+ request.getContextPath() + "/GeneralRegister';");
			out.println("</script>");
		}
	}

}
