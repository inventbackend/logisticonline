package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.RoleAdapter;
//import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/Role"} , name="Role")
public class UserRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserRoleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Role";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		//User Role
    		List<model.mdlRole> UserRoleList = new ArrayList<model.mdlRole>();
    		UserRoleList.addAll(RoleAdapter.LoadRole(sessionUser));
    		request.setAttribute("listuserrole", UserRoleList);	
    		
    		//-- load all menu list
    		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
    		mdlMenuList.addAll(MenuAdapter.LoadMenu(sessionUser));
    		request.setAttribute("listmenu", mdlMenuList);
    		//end of load all menu list --
    		
    		//-- load editable menu list
    		List<model.mdlMenu> EditableMenulist = new ArrayList<model.mdlMenu>();
    		EditableMenulist.addAll(MenuAdapter.LoadEditableMenu());
    		request.setAttribute("listeditablemenu", EditableMenulist);
    		//end of load editable menu list --
    		
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("conditionDescription", resultDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/user_role.jsp");
    		dispacther.forward(request, response);
    	}
		
    	result = "";
    	resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	sessionUser = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (sessionUser == null || sessionUser == "")
    	{ 
    		return;
    	}
    		
    	//Declare button
    	String keyBtn = ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	String btnDelete = request.getParameter("btnDelete");
    	
    	
		if (keyBtn.equals("saverule")){
			//Declare parameter
			String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    	String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    	String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    	String txtRoleId = request.getParameter("txtRoleId");
	    	String txtRoleName = request.getParameter("txtRoleName");
	    	
			lResult = MenuAdapter.InsertUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu, sessionUser);
	    	
	    	if(lResult.contains("SuccessInsert")) {  
	    		result = "SuccessInsertUserRole";
            }  
            else {
            	result = "FailedInsertUserRole";
            	resultDesc = lResult;
            }
		}
		
		if (keyBtn.equals("updaterule")){
			//Declare parameter
			String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    	String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    	String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    	String txtRoleId = request.getParameter("txtRoleId");
	    	String txtRoleName = request.getParameter("txtRoleName");

			lResult = MenuAdapter.UpdateUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu, sessionUser);

	    	if(lResult.contains("SuccessUpdate")) {  
				result = "SuccessUpdateUserRole";
            }  
            else {
            	result = "FailedUpdateUserRole";
            	resultDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String temp_txtRoleID = ValidateNull.NulltoStringEmpty(request.getParameter("temp_txtRoleID"));
			lResult = MenuAdapter.DeleteUserRule(temp_txtRoleID, sessionUser);
			
			if(lResult.contains("SuccessDelete")) {  
				result =  "SuccessDeleteUserRole";
            } 
            else {
            	result = "FailedDeleteUserRole"; 
            	resultDesc = lResult;
            }
			
			doGet(request, response);
			
			return;
		}
		
		if (keyBtn.equals("saveStaffRole")){
			//Declare parameter
	    	String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    	String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    	String txtRoleId = request.getParameter("txtRoleId");

			lResult = MenuAdapter.SaveStaffRole(txtRoleId, listAllowedMenu, listEditableMenu, sessionUser);

	    	if(lResult.contains("SuccessSave")) {  
				result = "SuccessSaveStaffRole";
            }  
            else {
            	result = "FailedSaveStaffRole";
            	resultDesc = lResult;
            }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}

}
