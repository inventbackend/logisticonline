package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ContainerTypeAdapter;
import adapter.MenuAdapter;
//import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/ContainerType"} , name="ContainerType")
public class MasterContainerTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterContainerTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "ContainerType";    	
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlContainerType> containerTypeList = new ArrayList<model.mdlContainerType>();
    		containerTypeList.addAll(adapter.ContainerTypeAdapter.LoadContainerType(sessionUser) );
    		request.setAttribute("listContainerType", containerTypeList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_containertype.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtContainerTypeID = request.getParameter("txtContainerTypeID");
        String txtDescription = request.getParameter("txtDescription");
        String txtIso = request.getParameter("txtIso");
		String temp_txtContainerTypeID = request.getParameter("temp_txtContainerTypeID");

		//Declare mdlPlant for global
		model.mdlContainerType mdlContainerType = new model.mdlContainerType();
		mdlContainerType.setContainerTypeID(txtContainerTypeID);
		mdlContainerType.setDescription(txtDescription);
		mdlContainerType.setIso(txtIso);

		if (keyBtn.equals("save")){
			Integer txtTare = Integer.valueOf(request.getParameter("txtTare").replace(".",""));
			mdlContainerType.setTare(txtTare);
			
			String lResult = ContainerTypeAdapter.InsertContainerType(mdlContainerType,sessionUser);
			if(lResult.contains("Success Insert Container Type")) {
				result = "SuccessInsertContainerType";
            }
            else {
            	result = "FailedInsertContainerType";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
			Integer txtTare = Integer.valueOf(request.getParameter("txtTare").replace(".",""));
			mdlContainerType.setTare(txtTare);
			
		    String lResult = ContainerTypeAdapter.UpdateContainerType(mdlContainerType,sessionUser);

		    if(lResult.contains("Success Update Container Type")) {
		    	result = "SuccessUpdateContainerType";
            }
            else {
            	result = "FailedUpdateContainerType";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = ContainerTypeAdapter.DeleteContainerType(temp_txtContainerTypeID,sessionUser);
			if(lResult.contains("Success Delete Container Type")) {
				result =  "SuccessDeleteContainerType";
            }
            else {
            	result = "FailedDeleteContainerType";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
