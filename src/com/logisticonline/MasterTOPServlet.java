package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.TOPAdapter;
import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/TOP"} , name="TOP")
public class MasterTOPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterTOPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDesc = "";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "TOP";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlTOP> topList = new ArrayList<model.mdlTOP>();
    		topList.addAll(adapter.TOPAdapter.LoadTOP(sessionUser) );
    		request.setAttribute("listTOP", topList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_top.jsp");
    		dispacther.forward(request, response);
    	}

		result = "";
		resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

		if(!keyBtn.equals("Delete")){
			//Declare TextBox
	    	String txtTOPID = request.getParameter("txtTOPID");
	        String txtName = request.getParameter("txtName");
	        Integer txtDuration = request.getParameter("txtDuration").equalsIgnoreCase("") ? 0 : Integer.parseInt(request.getParameter("txtDuration").replace(".","") );
	        
	        //Declare mdlTOP for global
			model.mdlTOP mdlTOP = new model.mdlTOP();
			mdlTOP.setTopID(txtTOPID);
			mdlTOP.setName(txtName);
			mdlTOP.setDuration(txtDuration);
			
			if (keyBtn.equals("save")){
				String lResult = TOPAdapter.InsertTOP(mdlTOP,sessionUser);
				if(lResult.contains("Success Insert TOP")) {
					result = "SuccessInsertTOP";
	            }
	            else {
	            	result = "FailedInsertTOP";
	            	resultDesc = lResult;
	            }
			}
			else if (keyBtn.equals("update")){
			    String lResult = TOPAdapter.UpdateTOP(mdlTOP,sessionUser);

			    if(lResult.contains("Success Update TOP")) {
			    	result = "SuccessUpdateTOP";
	            }
	            else {
	            	result = "FailedUpdateTOP";
	            	resultDesc = lResult;
	            }
			}
		}
		else{
			String temp_txtTOPID = request.getParameter("temp_txtTOPID");
			String lResult = TOPAdapter.DeleteTOP(temp_txtTOPID,sessionUser);
			if(lResult.contains("Success Delete TOP")) {
				result =  "SuccessDeleteTOP";
            }
            else {
            	result = "FailedDeleteTOP";
            	resultDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
}
