package com.logisticonline;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ContainerTypeAdapter;
import adapter.FactoryAdapter;
import adapter.OrderManagementAdapter;
import adapter.OrderManagementDetailAdapter;
import adapter.TierAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/OrderManagementDetail"} , name="OrderManagementDetail")
public class OrderManagementDetailServlet extends HttpServlet {

private static final long serialVersionUID = 1L;

    public OrderManagementDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String condition, orderManagementID, tempOrderManagementID, stuffingDate, tempStuffingDate, sessionUser, sessionRole, result, resultDesc ="";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
    	
    	orderManagementID = request.getParameter("orderManagementID");
    	condition = request.getParameter("condition");

    	//check of parameter is null or not, if null, get ordermanagementID from global. if global null,
    	if (orderManagementID == null)
    	{
    		if (tempOrderManagementID == null || tempOrderManagementID.equals("") ){
    			RequestDispatcher dispacther = request.getRequestDispatcher("/OrderManagement");
        		dispacther.forward(request, response);
    		}else{
    			orderManagementID = tempOrderManagementID;
    			condition = result;
    		}
    	}

		List<model.mdlOrderManagementDetail> orderManagementDetailList = new ArrayList<model.mdlOrderManagementDetail>();
		orderManagementDetailList.addAll(OrderManagementDetailAdapter.LoadOrderManagementDetailByID(orderManagementID,sessionUser));
		request.setAttribute("listOrderManagementDetail", orderManagementDetailList);

		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(TierAdapter.LoadTier(sessionUser));
		request.setAttribute("listTier", tierList);

		List<model.mdlContainerType> containerTypeList = new ArrayList<model.mdlContainerType>();
		containerTypeList.addAll(ContainerTypeAdapter.LoadContainerType(sessionUser));
		request.setAttribute("listContainerType", containerTypeList);

		request.setAttribute("condition", condition);
		request.setAttribute("conditionDescription", resultDesc);

		request.setAttribute("orderManagementID", orderManagementID); //send OrderManagementID to jsp via jstl
		Integer orderStatus = OrderManagementAdapter.GetOrderStatus(orderManagementID,sessionUser);
		String statusString = OrderManagementAdapter.CheckOrderStatus(orderManagementID,sessionUser);
		request.setAttribute("orderStatus", orderStatus);
		request.setAttribute("statusString", statusString);
		
		String districtID = OrderManagementDetailAdapter.GetRateParamFromOrder(orderManagementID,sessionUser).getDistrict();
		String tierID = OrderManagementDetailAdapter.GetRateParamFromOrder(orderManagementID,sessionUser).getTier();
		request.setAttribute("districtID", districtID); //send districtID to jsp via jstl
		request.setAttribute("tierID", tierID); //send tierID to jsp via jstl
		request.setAttribute("stuffingDateDefault", stuffingDate);
		request.setAttribute("userRole", sessionRole);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/order_management_detail.jsp");
		dispacther.forward(request, response);
		
		result = "";
		condition = "";
		resultDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	tempStuffingDate = stuffingDate;
    	
    	String btnDelete = request.getParameter("btnDelete");
    	String btnCommit = request.getParameter("btnCommit");

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String orderManagementID = request.getParameter("txtOrderManagementID");
		String temp_txtOrderManagementID = request.getParameter("temp_txtOrderManagementID");
		String temp_txtOrderManagementDetailID = request.getParameter("temp_txtOrderManagementDetailID");
		String datetime = ConvertDateTimeHelper.formatDate(request.getParameter("txtStuffingDate"), "dd MMM yyyy", "yyyy-MM-dd");
		String districtID = request.getParameter("districtID");
		String tierID = request.getParameter("slTier");
		String containerTypeID = request.getParameter("slContainerType");
		Integer quantity = request.getParameter("txtQuantity").equals("") ? 0 : Integer.parseInt(request.getParameter("txtQuantity").replace(".",""));

		//Declare mdlFactory for global
		model.mdlOrderManagementDetail mdlOrderManagementDetail = new model.mdlOrderManagementDetail();
		mdlOrderManagementDetail.setOrderManagementID(request.getParameter("txtOrderManagementID"));
		mdlOrderManagementDetail.setOrderManagementDetailID(request.getParameter("txtOrderManagementDetailID"));
		mdlOrderManagementDetail.setStuffingDate(datetime);
		//mdlOrderManagementDetail.setTierID(tierID);
		mdlOrderManagementDetail.setContainerTypeID(containerTypeID);
		//mdlOrderManagementDetail.setItemDescription(request.getParameter("txtItemDescription"));
		mdlOrderManagementDetail.setQuantity(quantity);
		Integer rate = OrderManagementDetailAdapter.GetOrderManagementDetailRate(districtID, tierID, containerTypeID, sessionUser);
		Integer price = quantity * rate;
		mdlOrderManagementDetail.setPrice(price);
		mdlOrderManagementDetail.setCreatedBy(sessionUser);
		mdlOrderManagementDetail.setUpdatedBy(sessionUser);

		if (keyBtn.equals("save")){
			String lResult = OrderManagementDetailAdapter.InsertOrderManagementDetail(mdlOrderManagementDetail,sessionUser);

			if(lResult.contains("Success Insert Order Management Detail")) {
				OrderManagementDetailAdapter.UpdateOrderTotalPrice(orderManagementID,sessionUser);
				result = "SuccessInsertOrderManagementDetail";
            }
            else {
            	result = "FailedInsertOrderManagementDetail";
            	resultDesc = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = OrderManagementDetailAdapter.UpdateOrderManagementDetail(mdlOrderManagementDetail,sessionUser);

		    if(lResult.contains("Success Update Order Management Detail")) {
		    	OrderManagementDetailAdapter.UpdateOrderTotalPrice(orderManagementID,sessionUser);
		    	result = "SuccessUpdateOrderManagementDetail";
            }
            else {
            	result = "FailedUpdateOrderManagementDetail";
            	resultDesc = lResult;
            }
		}

		if (btnCommit != null){
			Integer orderStatus = 1;
			Boolean success = OrderManagementAdapter.UpdateOrderAndDetailStatus(temp_txtOrderManagementID, orderStatus, sessionUser);
			if(success){
				result =  "SuccessCommitOrderManagementDetail";
            }
            else {
            	result = "FailedCommitOrderManagementDetail";
            	resultDesc = "Failed Commit Order Management Detail";
            }
			doGet(request, response);
		}

		if (btnDelete != null){
			String lResult = OrderManagementDetailAdapter.DeleteOrderManagementDetail(temp_txtOrderManagementID, temp_txtOrderManagementDetailID, sessionUser);
			if(lResult.contains("Success Delete Order Management Detail")) {
				OrderManagementDetailAdapter.UpdateOrderTotalPrice(temp_txtOrderManagementID,sessionUser);
				result =  "SuccessDeleteOrderManagementDetail";
            }
            else {
            	result = "FailedDeleteOrderManagementDetail";
            	resultDesc = lResult;
            }
			doGet(request, response);
		}
		
		stuffingDate = ConvertDateTimeHelper.formatDate(datetime, "yyyy-MM-dd", "dd MMM yyyy");
		if(stuffingDate==null || stuffingDate.contentEquals(""))
			stuffingDate = tempStuffingDate;
		
		if(orderManagementID==null || orderManagementID.contentEquals(""))
			tempOrderManagementID = temp_txtOrderManagementID;
		else
			tempOrderManagementID = orderManagementID;
		

		//		response.setContentType("application/text");
		//       response.setCharacterEncoding("UTF-8");
		//       response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);
		return;
    }
    
}
