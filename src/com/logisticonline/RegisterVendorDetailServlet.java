package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.FactoryAdapter;
import adapter.VendorAdapter;
import model.Globals;
import model.mdlVendorTier;

@WebServlet(urlPatterns={"/RegisterVendorDetail"} , name="RegisterVendorDetail")
public class RegisterVendorDetailServlet extends HttpServlet{

private static final long serialVersionUID = 1L;
    
    public RegisterVendorDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    String VendorID,VendorName, tempVendorID, tempVendorName, result, resultDesc = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	VendorID = request.getParameter("vendorid");
    	VendorName = request.getParameter("vendorname");
    	
    	if (VendorID == null)
    	{
    		VendorID = tempVendorID;
    		VendorName = tempVendorName;
    	}
    	
    	if(VendorID == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/RegisterVendor");
    		dispacther.forward(request, response);
    		
    		return;
    	}

    	List<model.mdlVendorTier> mdlVendorTierList = new ArrayList<model.mdlVendorTier>();
    	mdlVendorTierList.addAll(VendorAdapter.LoadVendorTierByVendor(VendorID, VendorName));
		request.setAttribute("listVendorTier", mdlVendorTierList);
		
		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(adapter.TierAdapter.LoadTier(VendorID));
		request.setAttribute("listTier", tierList);
		
		request.setAttribute("condition", result);
		request.setAttribute("conditionDescription", resultDesc);
		request.setAttribute("temp_vendorName", VendorName);
		request.setAttribute("temp_vendorID", VendorID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/register_vendor_detail.jsp");
		dispacther.forward(request, response);
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String btnDelete = request.getParameter("btnDelete");

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String temp_txtVendorID = request.getParameter("temp_txtVendorID");
		String temp_slTier = request.getParameter("temp_slTier");

		//Declare mdlVendorTier for global
		model.mdlVendorTier mdlVendorTier = new model.mdlVendorTier();
		mdlVendorTier.setVendorID(request.getParameter("txtVendorID"));
		mdlVendorTier.setTierID(request.getParameter("slTier"));

		if (keyBtn.equals("save")){
			String lResult = VendorAdapter.InsertVendorTier(mdlVendorTier, VendorID);
			if(lResult.contains("Success Insert Vendor Tier")) {
				result = "SuccessRegisterVendorTier";
            }
            else {
            	result = "FailedRegisterVendorTier";
            	resultDesc = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = VendorAdapter.DeleteVendorTier(temp_txtVendorID, temp_slTier, VendorID);
			if(lResult.contains("Success Delete Vendor Tier")) {
				result =  "SuccessDeleteVendorTier";
            }
            else {
            	result = "FailedDeleteVendorTier";
            	resultDesc = lResult;
            }

			doGet(request, response);
		}
		
		tempVendorID = VendorID;
		tempVendorName = VendorName;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
    }
    
}
