package com.logisticonline;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.DepoAdapter;
import adapter.MenuAdapter;
import adapter.PortAdapter;
import adapter.PushNotifAdapter;
import adapter.ShippingAdapter;
import adapter.VendorOrderAdapter;
import adapter.VendorOrderDetailAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/VendorOrderList"} , name="VendorOrderList")
public class VendorOrderServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

    public VendorOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String pOrderType="", pStuffingDateFrom="", pStuffingDateTo="";
    String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
    	
    	Boolean CheckMenu = false;
    	String MenuURL = "VendorOrderList";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String queryStringDate = request.getParameter("date");
    		if(queryStringDate != null && queryStringDate.equals("today")){
    			pOrderType="EXPORT";
    			pStuffingDateFrom = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");;
    			pStuffingDateTo = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");;
    		}
    		
    		/*if(pOrderType.contentEquals("")){
    		}*/
    		
    		List<model.mdlVendorOrder> vendorOrderList = new ArrayList<model.mdlVendorOrder>();

    		if (sessionRole.equals("R001")){ //admin
    			vendorOrderList.addAll(VendorOrderAdapter.LoadVendorOrder("",pStuffingDateFrom,pStuffingDateTo,pOrderType));
    		}else if (sessionRole.equals("R003")){
    			//Dashboard atas
				//    			List<model.mdlVendorOrderDashboard> listDashboard = new ArrayList<model.mdlVendorOrderDashboard>();
				//    			
				//    			String dateToday = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
				//    			model.mdlVendorOrderDashboard dashboardDetail = VendorOrderAdapter.LoadVendorOrderDashboard(sessionUser, dateToday, pOrderType);
				//    			String[] boxTitleList = {"Available Order", "Picked Order", "Cancelled Order", "Processed Order"};
				//    			Integer[] boxValueList = {dashboardDetail.totalAvailableOrder, 
				//				    					dashboardDetail.totalPickedOrder, 
				//				    					dashboardDetail.totalCancelledOrder,
				//				    					dashboardDetail.totalProcessedOrder};
				//    			for(int i=0; i<4; i++){
				//    				model.mdlVendorOrderDashboard dashboard = new model.mdlVendorOrderDashboard();
				//    				dashboard.boxTitle = boxTitleList[i];
				//    				dashboard.boxValue = boxValueList[i];
				//    				listDashboard.add(dashboard);
				//    			}
				//    			request.setAttribute("listDashboard", listDashboard);
    			
    			vendorOrderList.addAll(VendorOrderAdapter.LoadVendorOrder(sessionUser,pStuffingDateFrom,pStuffingDateTo,pOrderType));
    		}
    		request.setAttribute("listVendorOrder", vendorOrderList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("globalUserID", sessionUser);
    		request.setAttribute("globalUserRole", sessionRole);
    		request.setAttribute("orderType", pOrderType);
        	request.setAttribute("stuffingDateFrom", pStuffingDateFrom);
			request.setAttribute("stuffingDateTo", pStuffingDateTo);
    		
    		RequestDispatcher dispacther;
    		result = "";
			resultDesc = "";
			dispacther = request.getRequestDispatcher("/mainform/pages/vendor_order.jsp");
			dispacther.forward(request, response);
    	}
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		String btnCancel = request.getParameter("btnCancel");

    	if (sessionUser == null || sessionUser == "")
    	{
    		if (btnCancel != null){
    			doGet(request, response);
    		}
    		return;
    	}

		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}

		//Declare TextBox
		String temp_txtVendorOrderID = request.getParameter("temp_txtVendorOrderID");
		String temp_txtOrderID = request.getParameter("temp_txtOrderID");
		String temp_txtOrderDetailID = request.getParameter("temp_txtOrderDetailID");
		
		if (btnCancel != null){
			String lResult = VendorOrderAdapter.CancelVendorOrder(temp_txtVendorOrderID, temp_txtOrderID, temp_txtOrderDetailID, sessionUser);
			if(lResult.contains("Success Cancel Vendor Order")) {
				result =  "SuccessCancelVendorOrder";
				
				List<model.mdlVendorOrderDetail> listVODetail = VendorOrderDetailAdapter.LoadVendorOrderDetailByID(temp_txtVendorOrderID, sessionUser);
				PushNotifAdapter.PushListCancelNotification(listVODetail,sessionUser);
            }
            else {
            	result = "FailedCancelVendorOrder";
            	resultDesc = lResult;
            }
			doGet(request, response);
			return;
		}
		
		
		if(keyBtn.equals("search")) {
			//Declare class private string
	    	pOrderType = request.getParameter("ordertype");
	    	pStuffingDateFrom = request.getParameter("stuffingdatefrom");
	    	pStuffingDateTo = request.getParameter("stuffingdateto");
	    	
	    	return;
		}
	}
    
}
