package com.logisticonline;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.Base64Adapter;
import adapter.EmailAdapter;
import adapter.UserAdapter;
import model.mdlUserForgetPassword;

@WebServlet(urlPatterns={"/ForgotPassword"} , name="ForgotPassword")
public class GetForgotPasswordServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	List<mdlUserForgetPassword> listUser = null;

	public GetForgotPasswordServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private String result, message = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("condition", result);
		request.setAttribute("message", message);
		
		RequestDispatcher dispacther;
		dispacther = request.getRequestDispatcher("/mainform/pages/forgot_password.jsp");
		dispacther.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String btnSendMail = request.getParameter("btnForgetPassword");
		String btnCheckEmail = request.getParameter("btnCheckEmail");
		
		String lEmail = request.getParameter("email");
		
		
//		System.out.println(lEmail);
		
		if (btnCheckEmail != null)
        {
			
			if(lEmail != null) {
//				List<mdlUserForgetPassword> listUser = UserAdapter.checkEmail("linda@amt-indonesia.com");
				listUser = UserAdapter.checkEmail(lEmail);
				System.out.println(listUser.size());
				if(listUser.size() == 0) {
					
					result = "NODATA";
					
				} else {
					result = "SUCCESS";
					request.setAttribute("listUser", listUser);
					request.setAttribute("email", lEmail);
				}
				
				
			} else {
				result = "ERROR";
				message = "Email cant empty!";
			}
			
			doGet(request, response);
            return;
        }
		
		
		if (btnSendMail != null) {
			if(lEmail != null) {
				
				Context web_context;
				String baseUrl = null;
				String username = "";
				String userID = request.getParameter("id");
				System.out.println(userID);
				String encURL = Base64Adapter.EncriptBase64(userID);
				
				if(request.getParameter("id") == null) {
					result = "ERROR";
					message = "Please check email first!";
					doGet(request, response);
					return;
				}
				
				
				// find username by user id
				for(mdlUserForgetPassword user : listUser) {
			        if(user.getUserId().equals(userID)) {
			        	username= user.getUsername();
			        }
			    }
				
				
				try {
					web_context = (Context)new InitialContext().lookup("java:comp/env");
					baseUrl = (String)web_context.lookup("base_url");
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				
				String url = baseUrl+"ChangePassword?id="+encURL;
				
				String to = lEmail;
				String subject = "LOGOL Change Password!";
				String content = "<h3>Change Password </h3><br>"
									+ "username: " + username + "<br><br>"
									+ "<a href='"+url+"'>"+url+"</a>";
				System.out.println(content);
				EmailAdapter.SendEmailByTemplate(to, subject, content);
				
//				response.sendRedirect("/LogisticOnline/ChangePassword?id="+encURL);
				result = "SUCCESS_SEND_MAIL";
				doGet(request, response);
				return;
				
			}
			
        }
	}

}
