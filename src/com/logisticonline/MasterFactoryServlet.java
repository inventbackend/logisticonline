package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Factory"} , name="Factory")
public class MasterFactoryServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public MasterFactoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String condition,CustomerID,CustomerName,tempCustomerID,tempCustomerName,result,resultDescription,sessionUser = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	CustomerID = request.getParameter("custid");
    	CustomerName = request.getParameter("custname");
    	condition = request.getParameter("condition");
    	
    	if (CustomerID == null)
    	{
	    	CustomerID = tempCustomerID;
	    	CustomerName = tempCustomerName;
	    	condition = result;
    	}
    	
    	if(CustomerID == null)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Customer");
    		dispacther.forward(request, response);
    		
    		return;
    	}
		
		List<model.mdlFactory> mdlFactoryList = new ArrayList<model.mdlFactory>();
		mdlFactoryList.addAll(FactoryAdapter.LoadFactoryByCustomer(CustomerID, sessionUser));
		request.setAttribute("listFactory", mdlFactoryList);
		
		List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
		provinceList.addAll(adapter.DistrictAdapter.LoadProvince(sessionUser) );
		request.setAttribute("listProvince", provinceList);
		
		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
		districtList.addAll(adapter.DistrictAdapter.LoadDistrict(sessionUser));
		request.setAttribute("listDistrict", districtList);

		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(adapter.TierAdapter.LoadTier(sessionUser));
		request.setAttribute("listTier", tierList);
		
		request.setAttribute("condition", condition);
		request.setAttribute("conditionDescription", resultDescription);
		request.setAttribute("temp_customerName", CustomerName);
		request.setAttribute("temp_customerID", CustomerID);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_factory.jsp");
		dispacther.forward(request, response);
		
		result = "";
		condition = "";
		resultDescription = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String btnDelete = request.getParameter("btnDelete");

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox for delete process
		String temp_txtCustomerID = request.getParameter("temp_txtCustomerID");
		String temp_txtFactoryID = request.getParameter("temp_txtFactoryID");

		//Declare mdlFactory for global
		model.mdlFactory mdlFactory = new model.mdlFactory();
		mdlFactory.setCustomerID(request.getParameter("txtCustomerID"));
		
		mdlFactory.setFactoryID(request.getParameter("txtFactoryID"));
		if(mdlFactory.getFactoryID() == null || mdlFactory.getFactoryID().contentEquals(""))
			mdlFactory.setFactoryID(FactoryAdapter.CreateFactoryID(sessionUser));
			
		mdlFactory.setFactoryName(request.getParameter("txtFactoryName"));
		mdlFactory.setPic(request.getParameter("txtPIC"));
		mdlFactory.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
		mdlFactory.setMobilePhone(request.getParameter("txtMobilePhone"));
		mdlFactory.setProvince(request.getParameter("slProvince"));
		mdlFactory.setAddress(request.getParameter("txtAddress"));
		mdlFactory.setDistrictID(request.getParameter("slDistrict"));
		mdlFactory.setTierID(request.getParameter("slTier"));

		if (keyBtn.equals("save")){
			String lResult = FactoryAdapter.InsertFactory(mdlFactory,sessionUser);
			if(lResult.contains("Success Insert Factory")) {
				result = "SuccessInsertFactory";
            }
            else {
            	result = "FailedInsertFactory";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = FactoryAdapter.UpdateFactory(mdlFactory,sessionUser);

		    if(lResult.contains("Success Update Factory")) {
		    	result = "SuccessUpdateFactory";
            }
            else {
            	result = "FailedUpdateFactory";
            	resultDescription = lResult;
            }
		}

		if (btnDelete != null){
			String lResult = FactoryAdapter.DeleteFactory(temp_txtCustomerID, temp_txtFactoryID, sessionUser);
			if(lResult.contains("Success Delete Factory")) {
				result =  "SuccessDeleteFactory";
            }
            else {
            	result = "FailedDeleteFactory";
            	resultDescription = lResult;
            }

			doGet(request, response);
		}
		
		tempCustomerID = CustomerID;
		tempCustomerName = CustomerName;
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
    }
    
}
