package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;
import adapter.VendorOrderDetailAdapter;

@WebServlet(urlPatterns={"/getVendorOrderDetailByCustomerOrder"} , name="getVendorOrderDetailByCustomerOrder")
public class GetVendorOrderDetailByCustomerOrder extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public GetVendorOrderDetailByCustomerOrder() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String lOrderDetailID = request.getParameter("orderDetailID");
    	List<model.mdlVendorOrderDetail> vendorOrderDetailList = new ArrayList<model.mdlVendorOrderDetail>();
    	vendorOrderDetailList.addAll(VendorOrderDetailAdapter.LoadVendorOrderDetailByCustomerOrder(lOrderDetailID,sessionUser));
		request.setAttribute("listData", vendorOrderDetailList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/get_vendor_order_detail.jsp");
		dispacther.forward(request, response);
	}
    
}
