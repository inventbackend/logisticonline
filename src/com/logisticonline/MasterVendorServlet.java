package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;
import model.mdlCustomer;

@WebServlet(urlPatterns={"/Vendor"} , name="Vendor")
public class MasterVendorServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public MasterVendorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, sessionRole, result, resultDesc, tempVendorID, tempVendorName = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Vendor";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String ButtonStatus, ShowDetailButtonStatus;
    		
    		if(sessionRole.contentEquals("R001")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		if(sessionLevel.equals("main"))
        			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
        		else
        			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
        		ShowDetailButtonStatus = ButtonStatus;
    		}
    		else if(sessionRole.contentEquals("R003")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendorByID(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		ButtonStatus = "none";
        		if(sessionLevel.equals("main"))
    				ShowDetailButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    			else
    				ShowDetailButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		}
    		else{
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
        		request.setAttribute("listVendor", vendorList);
        		ButtonStatus = "none";
        		if(sessionLevel.equals("main"))
    				ShowDetailButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    			else
    				ShowDetailButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		}
    		
    		if(sessionRole.contentEquals("R001") || sessionRole.contentEquals("R003")){
    			List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
    			provinceList.addAll(adapter.DistrictAdapter.LoadProvince("") );
    			request.setAttribute("listProvince", provinceList);
    		}

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("showDetailButtonStatus", ShowDetailButtonStatus);
    		request.setAttribute("userRole", sessionRole);
    		
    		RequestDispatcher dispacther;
    		if(result == "SuccessInsertVendor")
    		{
    			response.sendRedirect(request.getContextPath() + "/VendorTier?vendorid="+tempVendorID+"&vendorname="+tempVendorName+"&condition="+result);
    		}
    		else
    		{
    			dispacther = request.getRequestDispatcher("/mainform/pages/master_vendor.jsp");
    			dispacther.forward(request, response);
    		}
    	}
    	
    	result = "";
		resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");		
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

		if(!keyBtn.equals("Delete")){
			//Declare mdlVendor for global
			model.mdlVendor mdlVendor = new model.mdlVendor();
			if(request.getParameter("txtVendorID") == null)
				mdlVendor.setVendorID("");
			else
				mdlVendor.setVendorID(request.getParameter("txtVendorID"));
			mdlVendor.setVendorName(request.getParameter("txtVendorName"));
			mdlVendor.setPIC(request.getParameter("txtPIC"));
			mdlVendor.setAddress(request.getParameter("txtAddress"));
			mdlVendor.setEmail(request.getParameter("txtEmail"));
			mdlVendor.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
			mdlVendor.setMobilePhone(request.getParameter("txtMobilePhone"));
			mdlVendor.setNPWP(request.getParameter("txtNPWP"));
			mdlVendor.setSIUJPT(request.getParameter("txtSIUJPT"));
			mdlVendor.setGarageAddress(request.getParameter("txtGarageAddress"));
			mdlVendor.setBankName(request.getParameter("txtBankName"));
			mdlVendor.setBankAccount(request.getParameter("txtBankAcc"));
			mdlVendor.setBankBranch(request.getParameter("txtBankBranch"));
			mdlVendor.setProvince(request.getParameter("slProvince"));
			mdlVendor.setPostalCode(request.getParameter("txtPostalCode"));
			
			if (keyBtn.equals("save")){
				String lResult = VendorAdapter.InsertVendor(mdlVendor,sessionUser);
				if(lResult.contains("Success Insert Vendor")) {
					result = "SuccessInsertVendor";
	            }
	            else {
	            	result = "FailedInsertVendor";
	            	resultDesc = lResult;
	            }
			}

			if (keyBtn.equals("update")){
			    String lResult = VendorAdapter.UpdateVendor(mdlVendor,sessionUser);

			    if(lResult.contains("Success Update Vendor")) {
			    	result = "SuccessUpdateVendor";
	            }
	            else {
	            	result = "FailedUpdateVendor";
	            	resultDesc = lResult;
	            }
			}
			
		    tempVendorID = mdlVendor.VendorID;
	        tempVendorName = mdlVendor.VendorName;
		}
		else{
			//delete process
	    	//Declare TextBox
	    	String temp_txtVendorID = request.getParameter("temp_txtVendorID");
			String lResult = VendorAdapter.DeleteVendor(temp_txtVendorID,sessionUser);
			if(lResult.contains("Success Delete Vendor")) {
				result =  "SuccessDeleteVendor";
            }
            else {
            	result = "FailedDeleteVendor";
            	resultDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
	
}
