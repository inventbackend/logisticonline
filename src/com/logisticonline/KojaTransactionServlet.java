package com.logisticonline;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;

import adapter.APIAdapter;
import adapter.CustomerAdapter;
import adapter.KojaAdapter;
import adapter.OrderManagementAdapter;
import adapter.PushNotifAdapter;
import adapter.ShaAdapter;
import adapter.VendorAdapter;
import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlKojaParam;
import model.mdlSendBilling;
import model.mdlSendBillingDataDetail;
import modelKojaAssociateTicket.mdlAssociateTicketParam;
import modelKojaGetBillingDetail.DETAILBILLING;
import modelKojaGetBillingDetail.SUMMARYDETAIL;
import modelKojaGetOnDemand.mdlGetOnDemandParam;
import modelKojaRequestInquiry.mdlPaymentFlagging;
import modelKojaRequestInquiry.mdlRequestInquiry;

@WebServlet(urlPatterns={"/KojaTransaction"} , name="KojaTransaction")
public class KojaTransactionServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public KojaTransactionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		String result = "";
		String resultType = "";
		String json = "";
		Gson gson = new Gson();
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
			
		 if (keyBtn.equals("checkRequestVesselVoyage")){
			 result = "false";
			 String dateNow = LocalDateTime.now().toString().substring(0, 10);
			 String terminal = request.getParameter("terminalName");
			 String vesselVoyageCreatedDate = KojaAdapter.GetVesselVoyageCreatedDate(terminal, sessionUser);
			 if(!dateNow.equals(vesselVoyageCreatedDate))
				 result = "true";
			 resultType = "text";
		 }
		 else if (keyBtn.equals("updateSession")){
			 String sessionKoja = request.getParameter("sessionKoja");
			 String custID = request.getParameter("custID");
			 result = KojaAdapter.UpdateSession(sessionKoja,custID,sessionUser);
			 resultType = "text";
		 }
		 else if (keyBtn.equals("getSession")){
			 result = KojaAdapter.GetSession(sessionUser);
			 resultType = "text";
		 }
		 else if (keyBtn.equals("getOnDemand")){
			 mdlKojaParam kojaParam = new model.mdlKojaParam();
			 kojaParam.serviceUrl = getServletContext().getInitParameter("koja_url");
			 kojaParam.username = getServletContext().getInitParameter("koja_username");
			 kojaParam.password = getServletContext().getInitParameter("koja_password");
			 kojaParam.fstreamusername = getServletContext().getInitParameter("koja_fstream_username");
			 kojaParam.fstreampassword = getServletContext().getInitParameter("koja_fstream_password");
			 kojaParam.deviceName = getServletContext().getInitParameter("koja_devicename");
			 
			 kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
			 kojaParam.username2 = getServletContext().getInitParameter("asso_username");
			 kojaParam.password2 = getServletContext().getInitParameter("asso_password");
			 
			 mdlGetOnDemandParam getOnDemandParam = new mdlGetOnDemandParam();
			 getOnDemandParam.orderID = request.getParameter("orderID");
			 getOnDemandParam.documentNo = request.getParameter("npe");
			 getOnDemandParam.documentDate = ConvertDateTimeHelper.formatDate(request.getParameter("npeDate"), "dd MMM yyyy", "ddMMyyyy");
			 getOnDemandParam.npwp = getServletContext().getInitParameter("koja_npwp");
			 getOnDemandParam.exportLimit = Integer.valueOf(request.getParameter("exportLimit"));
			 
			 /*result = APIAdapter.KojaGetOnDemand(kojaParam, getOnDemandParam);*/
			 /*Boolean checkExportLimit = KojaAdapter.CheckExportLimit(getOnDemandParam, sessionUser);
			 if(checkExportLimit){*/
				 int x = 0;
				 while (x < 1) {
				 	result = APIAdapter.KojaMainProcessShipper(kojaParam, getOnDemandParam);
				 	if(!result.equals("Sukses"))
				 		x++; 
			     }
			 /*}
			 else{
				 result = "Request failed due to export limit. The drivers have been ordered to reach the nearest storage depot.";
			 }*/
			 
			 resultType = "text";
		 }
		 else if (keyBtn.equals("mainProcess")){
			 mdlKojaParam kojaParam = new model.mdlKojaParam();
			 kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
			 kojaParam.username2 = getServletContext().getInitParameter("asso_username");
			 kojaParam.password2 = getServletContext().getInitParameter("asso_password");
			 kojaParam.serviceUrl = getServletContext().getInitParameter("koja_url");
			 
			 kojaParam.username = getServletContext().getInitParameter("koja_username");
			 kojaParam.password = getServletContext().getInitParameter("koja_password");
			 kojaParam.fstreamusername = getServletContext().getInitParameter("koja_fstream_username");
			 kojaParam.fstreampassword = getServletContext().getInitParameter("koja_fstream_password");
			 kojaParam.deviceName = getServletContext().getInitParameter("koja_devicename");
			 
			 kojaParam.email = getServletContext().getInitParameter("smpay_customer_email");
			 kojaParam.phone = getServletContext().getInitParameter("smpay_customer_sms");
			 
			 kojaParam.serviceUrl3 = getServletContext().getInitParameter("koja_url_flagging");
			 kojaParam.bankCompanyCode = getServletContext().getInitParameter("koja_bank_company_code");
			 kojaParam.bankChannelID = getServletContext().getInitParameter("koja_channelid");
			 kojaParam.bankName = getServletContext().getInitParameter("koja_bank_name");
			 
			 kojaParam.serviceUrl4 = getServletContext().getInitParameter("smpay_send_billing_url");
			 kojaParam.billingUser = getServletContext().getInitParameter("smpay_user");
			 kojaParam.billingPassword = getServletContext().getInitParameter("smpay_password");
			 kojaParam.billingBillerID = getServletContext().getInitParameter("smpay_biller_id");
			 kojaParam.billingProduct = getServletContext().getInitParameter("smpay_product_id");
			 kojaParam.billingChannel = getServletContext().getInitParameter("smpay_channel_id");
			 kojaParam.phone2 = getServletContext().getInitParameter("smpay_customer_phone");
			 kojaParam.billingBillerAccount = getServletContext().getInitParameter("smpay_biller_account");
			 kojaParam.billingBillerName = getServletContext().getInitParameter("smpay_biller_name");
			 kojaParam.billingRemark = getServletContext().getInitParameter("smpay_remark");
			 kojaParam.billingClientID = getServletContext().getInitParameter("smpay_bank_clientid");
			 kojaParam.billingClientAccount = getServletContext().getInitParameter("smpay_client_account");
			 
			 kojaParam.serverFileLocation = getServletContext().getInitParameter("param_file_location");
			 kojaParam.serverIpport = getServletContext().getInitParameter("param_server_ipport");
			 kojaParam.serverFileAccess = getServletContext().getInitParameter("param_file_access");
			 kojaParam.serverFileSeparator = getServletContext().getInitParameter("param_separator");
			 
			 String orderID = request.getParameter("orderID");
			 String npe = request.getParameter("npe");
			 
			 int x = 0;
			 while (x < 1) {
				 // untuk admin
			 	result = APIAdapter.KojaMainProcess(kojaParam, orderID, npe);
			 	if(!result.equals("Sukses"))
			 		x++; 
		     } 
			 
			 resultType = "text";
		 }
		 else if(keyBtn.equals("getOTP")) {
			String pin = request.getParameter("pin");
			String container = request.getParameter("container");
			String orderID = request.getParameter("orderID");
			
			mdlKojaParam kojaParam = new model.mdlKojaParam();
			 kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
			 kojaParam.username2 = getServletContext().getInitParameter("asso_username");
			 kojaParam.password2 = getServletContext().getInitParameter("asso_password");
			 kojaParam.fstreamusername = getServletContext().getInitParameter("koja_fstream_username");
			 kojaParam.phone = getServletContext().getInitParameter("smpay_customer_sms");
			 
			 result = APIAdapter.KojaGetOTP(kojaParam, orderID, pin, container);
			 resultType = "text";
			/*System.out.println(pin);
			System.out.println(container);*/
			
			/*String jsonResult = null;
			Gson gson = new Gson();
			jsonResult = gson.toJson("berhasil");
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResult);*/
		}
		else if(keyBtn.equals("sendOTP")) {
			String otp = request.getParameter("otp");
			String orderID = request.getParameter("orderID");
			
			mdlKojaParam kojaParam = new model.mdlKojaParam();
			kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
			kojaParam.username2 = getServletContext().getInitParameter("asso_username");
			kojaParam.password2 = getServletContext().getInitParameter("asso_password");
			
			result = APIAdapter.KojaAssociateContainer(kojaParam, orderID, otp);
			resultType = "text";
			/*String container = request.getParameter("container");*/
			
			/*System.out.println(otp);
			System.out.println(container);*/
			
			/*String jsonResult = null;
			Gson gson = new Gson();
			jsonResult = gson.toJson("berhasil");
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResult);*/
		}
		 else if (keyBtn.equals("getConfirmTransactionParam")){
			 String orderID = request.getParameter("OrderID");
			 json = gson.toJson(KojaAdapter.GetConfirmTransactionParam(orderID, sessionUser));
			 resultType = "json";
		 }
		 else if (keyBtn.equals("billingProcess")){
			 //-- REQUEST INQUIRY PROCESS
			 mdlRequestInquiry requestInquiryParam = new mdlRequestInquiry();
			 requestInquiryParam.transactionID = request.getParameter("transactionID");
			 requestInquiryParam.serviceUrl = getServletContext().getInitParameter("koja_url_flagging");
			 requestInquiryParam.transactionDateTime = request.getParameter("transactionDate");
			 requestInquiryParam.requestDateTime = request.getParameter("requestDateTime");
			 requestInquiryParam.companyCode = getServletContext().getInitParameter("koja_bank_company_code");
			 requestInquiryParam.channelID = getServletContext().getInitParameter("koja_channelid");
			 requestInquiryParam.proformaNo = request.getParameter("proformaNo");
			 requestInquiryParam.bankName = getServletContext().getInitParameter("koja_bank_name");
			 
			 mdlRequestInquiry requestInquiryResponse = new mdlRequestInquiry();
			 requestInquiryResponse = APIAdapter.RequestInquiry(requestInquiryParam, sessionUser);
			 
			 //-- PAY BILLING PROCESS
			 model.mdlSendBilling mdlSendBilling = new model.mdlSendBilling();
			 mdlSendBilling.serviceUrl = getServletContext().getInitParameter("smpay_send_billing_url");
			 mdlSendBilling.user = getServletContext().getInitParameter("smpay_user");
			 mdlSendBilling.password = getServletContext().getInitParameter("smpay_password");
			 mdlSendBilling.billerId = Integer.valueOf(getServletContext().getInitParameter("smpay_biller_id"));
			 mdlSendBilling.productId = getServletContext().getInitParameter("smpay_product_id");
			 mdlSendBilling.channelId = getServletContext().getInitParameter("smpay_channel_id");
			 mdlSendBilling.data = new model.mdlSendBillingData();
			 
			 mdlSendBilling.data.billingId = request.getParameter("proformaNo");
			 mdlSendBilling.data.billingDate = LocalDateTime.now().toString().replace("T", " ").substring(0, 19);
			 mdlSendBilling.data.expiredDate = LocalDateTime.now().plusHours(1).toString().replace("T", " ").substring(0, 19);
			 mdlSendBilling.data.billingType = 1;
			 mdlSendBilling.data.customerId = "logol";
			 mdlSendBilling.data.customerName = "logol";
			 mdlSendBilling.data.customerAddress = "kelapa gading";
			 mdlSendBilling.data.customerMail = getServletContext().getInitParameter("smpay_customer_email");
			 mdlSendBilling.data.customerPhone = getServletContext().getInitParameter("smpay_customer_phone");
			 mdlSendBilling.data.billerAccountNo = getServletContext().getInitParameter("smpay_biller_account");
			 mdlSendBilling.data.billerName = getServletContext().getInitParameter("smpay_biller_name");
			 mdlSendBilling.data.currency = "IDR";
			 mdlSendBilling.data.amount = requestInquiryResponse.getBillingAmount();
			 mdlSendBilling.data.tax = 0;
			 mdlSendBilling.data.totalAmount = requestInquiryResponse.getBillingAmount();
			 mdlSendBilling.data.interval = 60;
			 mdlSendBilling.data.remark = getServletContext().getInitParameter("smpay_remark");
			 mdlSendBilling.data.signature = ShaAdapter.getSHA(mdlSendBilling.user + mdlSendBilling.billerId + mdlSendBilling.data.billingId);
			 mdlSendBilling.data.documentNo = "-";
			 mdlSendBilling.data.bankClientId = getServletContext().getInitParameter("smpay_bank_clientid");
			 mdlSendBilling.data.accountNo = getServletContext().getInitParameter("smpay_client_account");
			 mdlSendBilling.data.detail = new ArrayList<model.mdlSendBillingDataDetail>();
			 
			 model.mdlSendBillingDataDetail billingDataDetail = new model.mdlSendBillingDataDetail();
			 billingDataDetail.code = requestInquiryResponse.getBillingCode();
			 billingDataDetail.description = requestInquiryResponse.getBillingDescription();
			 billingDataDetail.nominal = requestInquiryResponse.getBillingAmount();
			 mdlSendBilling.data.detail.add(billingDataDetail);
			 
			 model.mdlSendBilling sendBillingResponse = APIAdapter.SendBilling(mdlSendBilling, sessionUser);
			 /*if(sendBillingResponse.status){*/
				 model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
				 mdlOrderManagement.setOrderManagementID(request.getParameter("orderID"));
				 mdlOrderManagement.setTpsTransactionID(request.getParameter("transactionID"));
				 mdlOrderManagement.setTpsTransactionDate(request.getParameter("transactionDate"));
				 mdlOrderManagement.setTpsProforma(request.getParameter("proformaNo"));
				 mdlOrderManagement.setTpsPaymentID(sendBillingResponse.getData().getPaymentId());
				 mdlOrderManagement.setTpsApprovalCode(sendBillingResponse.getData().getJournalNo());
				 mdlOrderManagement.setTpsApprovalDate(sendBillingResponse.getData().getJournalDate());
				 /*mdlOrderManagement.setTpsBillingStatus(1);*/
				 mdlOrderManagement.setTpsBillingAmount(requestInquiryResponse.getBillingAmount());
				 OrderManagementAdapter.UpdateTpsData(mdlOrderManagement, sessionUser);
			/*	 result = "Success" + "--" + sendBillingResponse.errDesc;
			 }
			 else{
				 result = "Failed" + "--" + sendBillingResponse.errDesc;
			 }*/
			//-- END OF PAY BILLING PROCESS
				
				 
			//-- FLAGGING PROCESS
			mdlPaymentFlagging requestPaymentFlagging = new mdlPaymentFlagging();
			LocalDateTime dateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddHHmmss");  
			String requestFlaggingDate = dateTime.format(formatter);  
			
			requestPaymentFlagging.serviceUrl = getServletContext().getInitParameter("koja_url_flagging");
			requestPaymentFlagging.transactionDate = request.getParameter("transactionDate");
			requestPaymentFlagging.requestDate = requestFlaggingDate;
			requestPaymentFlagging.companyCode = getServletContext().getInitParameter("koja_bank_company_code");
			requestPaymentFlagging.channelID = getServletContext().getInitParameter("koja_channelid");
			requestPaymentFlagging.proformaNo = request.getParameter("proformaNo");
			requestPaymentFlagging.billingAmount = requestInquiryResponse.getBillingAmount();
			requestPaymentFlagging.approvalCode = sendBillingResponse.getData().getJournalNo();
			requestPaymentFlagging.bankName = getServletContext().getInitParameter("koja_bank_name");
			APIAdapter.PaymentFlagging(requestPaymentFlagging, sessionUser);
			
			resultType = "text";
		 }
		 else if (keyBtn.equals("associateTicket")){
			 //get tid, container number and seal number and associate it
			 try{
				 CachedRowSet jrs = null;
				 String sql = "";
				 List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
				 sql = "{call sp_VendorOrderDetailLoadByOrder(?)}";
				 listParam.add(QueryExecuteAdapter.QueryParam("string", request.getParameter("orderID")));
				 jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDriverInfoForAssociateTicket", sessionUser);
			
				 while(jrs.next()){
					 mdlAssociateTicketParam associateTicketParam = new mdlAssociateTicketParam();
					 associateTicketParam.serviceUrl = getServletContext().getInitParameter("asso_url");
					 associateTicketParam.username = getServletContext().getInitParameter("asso_username");
					 associateTicketParam.password = getServletContext().getInitParameter("asso_password");
					 associateTicketParam.dataCount = 1;
					 associateTicketParam.status = "I";
					 associateTicketParam.tid = jrs.getString(22);
					 associateTicketParam.containerNumber = jrs.getString("ContainerNumber");
					 associateTicketParam.sealNumber = jrs.getString("SealNumber");
					 associateTicketParam.driverID = jrs.getString("DriverID");
					 
					 APIAdapter.AssociateTicket(associateTicketParam, sessionUser);
				 } 
			 }
			 catch(Exception e){
			 }
			 
			 result = "Billing process success";
			 resultType = "text";
		 }
		 else if (keyBtn.equals("summaryProformaDetail")){
			 mdlKojaParam param = new mdlKojaParam();
			  param.setServiceUrl(getServletContext().getInitParameter("koja_url"));
			  param.setUsername(getServletContext().getInitParameter("koja_username"));
			  param.setPassword(getServletContext().getInitParameter("koja_password"));
			  param.setSession("207524");
			  System.out.println(getServletContext().getInitParameter("koja_url"));
			  System.out.println(getServletContext().getInitParameter("koja_username"));
			  System.out.println(getServletContext().getInitParameter("koja_password"));
			  
			  String proformaID = request.getParameter("proformaID");
			  
			  DETAILBILLING summarydetail = APIAdapter.GetBillingDetail(param, proformaID, "usr");
			  System.out.println(summarydetail.toString());
			  
			  json = gson.toJson(summarydetail);
			 resultType = "json";
		 }
		 
		 else if (keyBtn.equals("downloadInvoice")){
			 mdlKojaParam param = new mdlKojaParam();
			  param.setServiceUrl(getServletContext().getInitParameter("koja_url"));
			  param.setUsername(getServletContext().getInitParameter("koja_username"));
			  param.setPassword(getServletContext().getInitParameter("koja_password"));
			  param.setSession("207524");
			  System.out.println(getServletContext().getInitParameter("koja_url"));
			  System.out.println(getServletContext().getInitParameter("koja_username"));
			  System.out.println(getServletContext().getInitParameter("koja_password"));
			  
			  String proformaID = request.getParameter("proformaID");
			  
			  modelKojaMainGetCustomData.MainGetCustomData summarydetail = APIAdapter.GetCustomData(param, proformaID, "usr");
			  System.out.println(summarydetail.toString());
			  
			  json = gson.toJson(summarydetail);
			 resultType = "json";
		 }
		 
		 
		if(resultType.equals("text")){
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(result);
		}
		else if(resultType.equals("json")){
			response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(json);
		}
	}
    
}
