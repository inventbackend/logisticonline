package com.logisticonline;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.Base64Adapter;
import adapter.CustomerAdapter;
import adapter.LoginAdapter;
import adapter.MenuAdapter;
import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Register"} , name="Register")
public class RegisterServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String result,resultDesc,tempCustomerID,tempCustomerName = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
		provinceList.addAll(adapter.DistrictAdapter.LoadProvince("") );
		request.setAttribute("listProvince", provinceList);
		
		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
		districtList.addAll(adapter.DistrictAdapter.LoadDistrict("") );
		request.setAttribute("listDistrict", districtList);
		
		List<model.mdlTOP> topList = new ArrayList<model.mdlTOP>();
		topList.addAll(adapter.TOPAdapter.LoadTOP("") );
		request.setAttribute("listTOP", topList);
		
		request.setAttribute("condition", result);
		request.setAttribute("conditionDescription", resultDesc);
		
		RequestDispatcher dispacther;
		if(result == "SuccessRegisterCustomer")
		{
			PrintWriter out = response.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("alert('You have finished your registration, we will send you an email if your account has been activated');");
			out.println("location='"+ request.getContextPath() + "/Login';");
			out.println("</script>");
			//response.sendRedirect(request.getContextPath() + "/RegisterDetail?custid="+tempCustomerID+"&custname="+tempCustomerName+"");
			
			//dispacther = request.getRequestDispatcher("/RegisterDetail?custid="+Globals.gReg_CustomerID+"&custname="+Globals.gReg_CustomerName+"");
		}
		else
		{
			dispacther = request.getRequestDispatcher("/mainform/pages/register.jsp");
			dispacther.forward(request, response);
		}
		
		result = "";
		resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
		mdlCustomer.setCustomerID(CustomerAdapter.CreateCustomerID(request.getParameter("txtEmail")));
		mdlCustomer.setCustomerName(request.getParameter("txtCustomerName"));
		mdlCustomer.setPIC1(request.getParameter("txtPIC1"));
		mdlCustomer.setPIC2("");
		mdlCustomer.setPIC3("");
		mdlCustomer.setCustomerAddress(request.getParameter("txtAddress"));
        mdlCustomer.setEmail(request.getParameter("txtEmail"));
        mdlCustomer.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
        mdlCustomer.setMobilePhone(request.getParameter("txtMobilePhone"));
        mdlCustomer.setNPWP("");
        mdlCustomer.setDomicile(request.getParameter("txtDomicile"));
        mdlCustomer.setDistrict(request.getParameter("txtPostalCode"));
        mdlCustomer.setTDP("");
        mdlCustomer.setUsername(request.getParameter("txtUserName"));
        mdlCustomer.setPassword(Base64Adapter.EncriptBase64(request.getParameter("txtPassword")));
        mdlCustomer.setBillingAddress("");
        mdlCustomer.setTOP("");
        //mdlCustomer.setCreditLimit(Double.valueOf(request.getParameter("txtCreditLimit").replace(".", "")));
        mdlCustomer.setCreditLimit(0.0);
        mdlCustomer.setBankName("");
        mdlCustomer.setBankAcc("");
        mdlCustomer.setBankBranch("");
        mdlCustomer.setPostalCode(request.getParameter("txtPostalCode"));
        mdlCustomer.setPpn(10);
        mdlCustomer.setPph23(2);
        mdlCustomer.setProductInvoice("[\"Trucking\",\"Custom Clearance\",\"Reimburse\"]");
        mdlCustomer.setCustomClearance(150000);
		String btnRegister = request.getParameter("btnRegister"); 
        
        if (btnRegister != null)
        {   
        	String lResult = CustomerAdapter.RegisterCustomer(mdlCustomer);
        	
        	if(lResult.contains("Success Register Customer")) {  
        		result = "SuccessRegisterCustomer";
            }  
            else {
            	result = "FailedRegisterCustomer";
            	resultDesc = lResult;
            }
        	
            tempCustomerID = mdlCustomer.CustomerID;
            tempCustomerName = mdlCustomer.CustomerName;
            doGet(request, response);
            return;
        }
	}
	
}
