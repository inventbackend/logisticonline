package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.DistrictAdapter;
import adapter.MenuAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/District"} , name="District")
public class MasterDistrictServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterDistrictServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "District";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
    		provinceList.addAll(adapter.DistrictAdapter.LoadProvince(sessionUser) );
    		request.setAttribute("listProvince", provinceList);
    		
    		List<model.mdlDistrict> districtList = new ArrayList<model.mdlDistrict>();
    		districtList.addAll(adapter.DistrictAdapter.LoadDistrict(sessionUser) );
    		request.setAttribute("listDistrict", districtList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_district.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtDistrictID = request.getParameter("txtDistrictID");
        String txtName = request.getParameter("txtName");
        String slProvince = request.getParameter("slProvince");
		String temp_txtDistrictID = request.getParameter("temp_txtDistrictID");

		//Declare mdlPlant for global
		model.mdlDistrict mdlDistrict = new model.mdlDistrict();
		mdlDistrict.setDistrictID(txtDistrictID);
		mdlDistrict.setName(txtName);
		mdlDistrict.setProvince(slProvince);

		if (keyBtn.equals("save")){
			String lResult = DistrictAdapter.InsertDistrict(mdlDistrict,sessionUser);
			if(lResult.contains("Success Insert District")) {
				result = "SuccessInsertDistrict";
            }
            else {
            	result = "FailedInsertDistrict";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = DistrictAdapter.UpdateDistrict(mdlDistrict,sessionUser);

		    if(lResult.contains("Success Update District")) {
		    	result = "SuccessUpdateDistrict";
            }
            else {
            	result = "FailedUpdateDistrict";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = DistrictAdapter.DeleteDistrict(temp_txtDistrictID,sessionUser);
			if(lResult.contains("Success Delete District")) {
				result =  "SuccessDeleteDistrict";
            }
            else {
            	result = "FailedDeleteDistrict";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
