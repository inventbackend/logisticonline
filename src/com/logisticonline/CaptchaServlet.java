package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.github.cage.Cage;
import com.github.cage.GCage;

import adapter.APIAdapter;
import model.mdlKojaParam;

@WebServlet(urlPatterns={"/Captcha"} , name="Captcha")
public class CaptchaServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1490947492185481844L;
	
	public CaptchaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	      throws ServletException, IOException {
		  HttpSession session = req.getSession(false);
		  
		  mdlKojaParam param = new mdlKojaParam();
		  param.setServiceUrl("http://123.231.237.22/TPSOnlineServicesUAT/index.php");
		  param.setUsername("LOGOL");
		  param.setPassword("LOGOL456");
		  param.setSession("207314");
		  
		  
		  APIAdapter.GetCustomData(param, "201907160002", "usr");
	    RequestDispatcher dispacther;
		dispacther = req.getRequestDispatcher("/mainform/pages/captcha.jsp");
		dispacther.forward(req, resp);
	  }
	  
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		      throws ServletException, IOException {
			RequestDispatcher dispacther;
			boolean showGoodResult;
			boolean showBadResult;
			  
			String sessionToken = CaptchaServiceServlet.getToken(req.getSession(false));
			String requestToken = req.getParameter("captcha");
			
			showGoodResult = sessionToken != null && sessionToken.equals(requestToken);
			
			showBadResult = !showGoodResult;
			
			System.out.println(requestToken);
			
//			resp.setContentType("application/json");
//			resp.setCharacterEncoding("UTF-8");
//			resp.getWriter().write("cok");
			req.setAttribute("showGoodResult", showGoodResult);
			req.setAttribute("showBadResult", showBadResult);
		  
			dispacther = req.getRequestDispatcher("/mainform/pages/captcha.jsp");
			dispacther.forward(req, resp);
	  }
	  

}
