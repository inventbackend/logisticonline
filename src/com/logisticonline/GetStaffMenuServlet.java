package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.UserAdapter;

@WebServlet(urlPatterns={"/getStaffMenu"} , name="getStaffMenu")
public class GetStaffMenuServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

    public GetStaffMenuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
    	String lUserID = request.getParameter("userid");
    	
    	List<model.mdlUser> staffList = new ArrayList<model.mdlUser>();
    	staffList.addAll(UserAdapter.LoadStaffByUser(lUserID, sessionUser));
		request.setAttribute("listStaff", staffList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getStaffForUser.jsp");
		dispacther.forward(request, response);
	}

}
