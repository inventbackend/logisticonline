package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ContainerTypeAdapter;
import adapter.OrderManagementAdapter;
import adapter.OrderManagementDetailAdapter;

@WebServlet(urlPatterns={"/getOrderDetail"} , name="getOrderDetail")
public class GetOrderDetailServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

    public GetOrderDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser, sessionRole = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
    	
    	String lOrderID = request.getParameter("orderid");
    	request.setAttribute("orderManagementID", lOrderID);
    	
    	List<model.mdlOrderManagementDetail> orderManagementDetailList = new ArrayList<model.mdlOrderManagementDetail>();
		orderManagementDetailList.addAll(OrderManagementDetailAdapter.LoadOrderManagementDetailByID(lOrderID,sessionUser));
		request.setAttribute("listOrderManagementDetail", orderManagementDetailList);
		
		Integer orderStatus = OrderManagementAdapter.GetOrderStatus(lOrderID,sessionUser);
		request.setAttribute("orderStatus", orderStatus);
		
		request.setAttribute("userRole", sessionRole);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getOrderDetail.jsp");
		dispacther.forward(request, response);
	}

}
