package com.logisticonline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import adapter.Base64Adapter;
import adapter.CustomerAdapter;
import adapter.ValidateNull;
import adapter.VendorAdapter;
import model.Globals;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

@WebServlet(urlPatterns={"/RegisterVendor"} , name="RegisterVendor")
public class RegisterVendorServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public RegisterVendorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String result, resultDesc, tempVendorID, tempVendorName = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<model.mdlProvince> provinceList = new ArrayList<model.mdlProvince>();
		provinceList.addAll(adapter.DistrictAdapter.LoadProvince("") );
		request.setAttribute("listProvince", provinceList);
		
		List<model.mdlTier> tierList = new ArrayList<model.mdlTier>();
		tierList.addAll(adapter.TierAdapter.LoadTier(""));
		request.setAttribute("listTier", tierList);
		
		request.setAttribute("condition", result);
		request.setAttribute("conditionDescription", resultDesc);
		
		RequestDispatcher dispacther;
		if(result == "SuccessRegisterVendor")
		{
			PrintWriter out = response.getWriter();
			//JOptionPane.showMessageDialog(null, "You have finished your registration, we will send you an email if your account has been activated", "InfoBox: " + "Notification", JOptionPane.INFORMATION_MESSAGE);
			//response.sendRedirect(request.getContextPath() + "/Login");
		   out.println("<script type=\"text/javascript\">");
		   out.println("alert('You have finished your registration, we will send you an email if your account has been activated');");
		   out.println("location='"+ request.getContextPath() + "/Login';");
		   out.println("</script>");
			
			//response.sendRedirect(request.getContextPath() + "/RegisterVendorDetail?vendorid="+Globals.gReg_VendorID+"&vendorname="+Globals.gReg_VendorName+"");			
		}
		else
		{
			//Globals.gCondition = "";
			//Globals.gConditionDesc = "";
			dispacther = request.getRequestDispatcher("/mainform/pages/register_vendor.jsp");
			dispacther.forward(request, response);
		}
		
		result = "";
		resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		model.mdlVendor mdlVendor = new model.mdlVendor();
		mdlVendor.setVendorID(VendorAdapter.CreateVendorID(request.getParameter("txtEmail")));
		mdlVendor.setVendorName(request.getParameter("txtVendorName"));
		mdlVendor.setPIC(request.getParameter("txtPIC"));
		mdlVendor.setAddress(request.getParameter("txtAddress"));
		mdlVendor.setEmail(request.getParameter("txtEmail"));
		mdlVendor.setOfficePhone(request.getParameter("txtOfficePhone").replace("_", ""));
		mdlVendor.setMobilePhone(request.getParameter("txtMobilePhone"));
		mdlVendor.setNPWP(request.getParameter("txtNPWP"));
		mdlVendor.setSIUJPT(request.getParameter("txtSIUJPT"));
		mdlVendor.setGarageAddress(request.getParameter("txtGarageAddress"));
		mdlVendor.setBankName(request.getParameter("txtBankName"));
		mdlVendor.setBankAccount(request.getParameter("txtBankAcc"));
		mdlVendor.setBankBranch(request.getParameter("txtBankBranch"));
		mdlVendor.setUsername(request.getParameter("txtUserName"));
		mdlVendor.setPassword(Base64Adapter.EncriptBase64(request.getParameter("txtPassword")));
		mdlVendor.setTier(ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("cbTier")) );
		mdlVendor.setProvince(request.getParameter("slProvince"));
		mdlVendor.setPostalCode(request.getParameter("txtPostalCode"));
		String btnRegister = request.getParameter("btnRegister");
        
        if (btnRegister != null)
        {   
        	String lResult = VendorAdapter.RegisterVendor(mdlVendor);
        	
        	if(lResult.contains("Success Register Vendor")) {  
				result = "SuccessRegisterVendor";
            }  
            else {
            	result = "FailedRegisterVendor";
            	resultDesc = lResult;
            }
        	
            tempVendorID = mdlVendor.VendorID;
            tempVendorName = mdlVendor.VendorName;
            doGet(request, response);
            return;
        }
	}
	
}
