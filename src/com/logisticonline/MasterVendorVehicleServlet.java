package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.ConvertDateTimeHelper;
import adapter.MenuAdapter;
import adapter.VendorAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/VendorVehicle"} , name="VendorVehicle")
public class MasterVendorVehicleServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public MasterVendorVehicleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionLevel, staffRole, sessionRole, result, resultDesc = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "VendorVehicle";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		
    		if(sessionRole.contentEquals("R001")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorVehicle> vendorVehicleList = new ArrayList<model.mdlVendorVehicle>();
    			vendorVehicleList.addAll(VendorAdapter.LoadVendorVehicle(sessionUser));
        		request.setAttribute("listVendorVehicle", vendorVehicleList);
        		request.setAttribute("vendorID", "");
    		}
    		else if(sessionRole.contentEquals("R003")){
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorVehicle> vendorVehicleList = new ArrayList<model.mdlVendorVehicle>();
    			vendorVehicleList.addAll(VendorAdapter.LoadVendorVehicleByVendor(sessionUser));
        		request.setAttribute("listVendorVehicle", vendorVehicleList);
        		request.setAttribute("vendorID", sessionUser);
    		}
    		else{
    			List<model.mdlVendor> vendorList = new ArrayList<model.mdlVendor>();
    			vendorList.addAll(VendorAdapter.LoadVendor(sessionUser));
        		request.setAttribute("listVendor", vendorList);
        		
    			List<model.mdlVendorVehicle> vendorVehicleList = new ArrayList<model.mdlVendorVehicle>();
        		request.setAttribute("listVendorVehicle", vendorVehicleList);
        		ButtonStatus = "none";
        		request.setAttribute("vendorID", "");
    		}

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("userRole", sessionRole);
    		
    		RequestDispatcher dispacther;
			dispacther = request.getRequestDispatcher("/mainform/pages/master_vendor_vehicle.jsp");
			dispacther.forward(request, response);
    	}
    	
    	result = "";
		resultDesc = "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

		if(!keyBtn.equals("Delete")){
			//Declare mdlVendorVehicle for global
			model.mdlVendorVehicle mdlVendorVehicle = new model.mdlVendorVehicle();
			mdlVendorVehicle.setVendorID(request.getParameter("txtVendorID"));
			mdlVendorVehicle.setVehicleNumber(request.getParameter("txtVehicleNumber"));
			mdlVendorVehicle.setBrand(request.getParameter("txtBrand"));
			mdlVendorVehicle.setType(request.getParameter("txtType"));
			mdlVendorVehicle.setYear(request.getParameter("txtYear"));
			mdlVendorVehicle.setTidNumber(request.getParameter("txtTidNumber"));
			mdlVendorVehicle.setStnkNumber(request.getParameter("txtStnkNumber"));
			mdlVendorVehicle.setStnkExpired(ConvertDateTimeHelper.formatDate(request.getParameter("txtStnkExpired"), "dd MMM yyyy", "yyyy-MM-dd"));
			
			if (keyBtn.equals("save")){
				String lResult = VendorAdapter.InsertVendorVehicle(mdlVendorVehicle,sessionUser);
				if(lResult.contains("Success Insert Vendor Vehicle")) {
					result = "SuccessInsertVendorVehicle";
	            }
	            else {
	            	result = "FailedInsertVendorVehicle";
	            	resultDesc = lResult;
	            }
			}
			else if (keyBtn.equals("update")){
				String lResult = VendorAdapter.UpdateVendorVehicle(mdlVendorVehicle,sessionUser);
				if(lResult.contains("Success Update Vendor Vehicle")) {
					result = "SuccessUpdateVendorVehicle";
	            }
	            else {
	            	result = "FailedUpdateVendorVehicle";
	            	resultDesc = lResult;
	            }
			}
		}
		else{
			//delete process
	    	//Declare TextBox
	    	String temp_txtVendorID = request.getParameter("temp_txtVendorID");
	    	String temp_txtVehicleNumber = request.getParameter("temp_txtVehicleNumber");
	    			
			String lResult = VendorAdapter.DeleteVendorVehicle(temp_txtVendorID, temp_txtVehicleNumber, sessionUser);
			if(lResult.contains("Success Delete Vendor Vehicle")) {
				result =  "SuccessDeleteVendorVehicle";
            }
            else {
            	result = "FailedDeleteVendorVehicle";
            	resultDesc = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);

		return;
	}
	
}
