package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CustomerAdapter;
import adapter.FactoryAdapter;
import adapter.MenuAdapter;
import model.mdlCustomer;

@WebServlet(urlPatterns={"/CustomerOnbehalfNpwp"} , name="CustomerOnbehalfNpwp")
public class CustomerOnbehalfNpwpServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public CustomerOnbehalfNpwpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser, sessionRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}    	

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}
    	
    	model.mdlOnbehalfNpwp data = new model.mdlOnbehalfNpwp();
		data.customerID = request.getParameter("txtCustomerID");
		data.onbehalfCustomer = request.getParameter("txtOnbehalfName");
		data.onbehalfNpwp = request.getParameter("txtOnbehalfNpwp");
		
		if (keyBtn.equals("save")){
			String lResult = CustomerAdapter.InsertOnbehalfNpwp(data, sessionUser);
			if(lResult.contains("Success Insert Npwp")) {
				result = "SuccessInsertNpwp";
            }
            else {
            	result = "FailedInsertNpwp";
            	resultDescription = lResult;
            }
		}
		else if (keyBtn.equals("update")){
			data.previousNpwp = request.getParameter("txtTempNpwp");
		    String lResult = CustomerAdapter.UpdateOnbehalfNpwp(data, sessionUser);

		    if(lResult.contains("Success Update Npwp")) {
		    	result = "SuccessUpdateNpwp";
            }
            else {
            	result = "FailedUpdateNpwp";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
	
}
