package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.CreditNoteAdapter;
import adapter.DebitNoteAdapter;

@WebServlet(urlPatterns={"/CreditNotePrint"} , name="CreditNotePrint")
public class CreditNotePrintServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public CreditNotePrintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser,sessionRole = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
		if(sessionUser == null || sessionUser.equals("")){
			RequestDispatcher dispacther = request.getRequestDispatcher("/Login");
			dispacther.forward(request, response);
		}
		else{
			String creditNote = request.getParameter("cnNumber");
			model.mdlCreditNote mdlCreditNote = CreditNoteAdapter.PrintCreditNote(creditNote, sessionUser);
	    	
			//check if the user is allowed for access or not
			if(mdlCreditNote.getVendorID().equals(sessionUser) || sessionRole.equals("R001")){
				request.setAttribute("mdlCreditNote", mdlCreditNote);
				request.setAttribute("listDO", mdlCreditNote.getListDO());
				
				RequestDispatcher dispacther;
				dispacther = request.getRequestDispatcher("/mainform/pages/credit_note_print.jsp");
				dispacther.forward(request, response);
			}
			else{
				RequestDispatcher dispacther = request.getRequestDispatcher("/Logout");
				dispacther.forward(request, response);
			}
		}
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    
}
