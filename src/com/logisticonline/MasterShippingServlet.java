package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.ShippingAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/Shipping"} , name="Shipping")
public class MasterShippingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MasterShippingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String sessionUser, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Shipping";
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlShipping> shippingList = new ArrayList<model.mdlShipping>();
    		shippingList.addAll(adapter.ShippingAdapter.LoadShipping(sessionUser) );
    		request.setAttribute("listShipping", shippingList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/master_shipping.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	if (sessionUser == null || sessionUser == "")
    	{
    		return;
    	}

    	//Declare button
    	String keyBtn = request.getParameter("key");
    	if (keyBtn == null){
    		keyBtn = new String("");
    	}

    	//Declare TextBox
    	String txtShippingID = request.getParameter("txtShippingID");
        String txtName = request.getParameter("txtName");
        String txtAddress = request.getParameter("txtAddress");
		String temp_txtShippingID = request.getParameter("temp_txtShippingID");

		//Declare mdlShipping for global
		model.mdlShipping mdlShipping = new model.mdlShipping();
		mdlShipping.setShippingID(txtShippingID);
		mdlShipping.setName(txtName);
		mdlShipping.setAddress(txtAddress);

		if (keyBtn.equals("save")){
			String lResult = ShippingAdapter.InsertShipping(mdlShipping,sessionUser);
			if(lResult.contains("Success Insert Shipping")) {
				result = "SuccessInsertShipping";
            }
            else {
            	result = "FailedInsertShipping";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("update")){
		    String lResult = ShippingAdapter.UpdateShipping(mdlShipping,sessionUser);

		    if(lResult.contains("Success Update Shipping")) {
		    	result = "SuccessUpdateShipping";
            }
            else {
            	result = "FailedUpdateShipping";
            	resultDescription = lResult;
            }
		}

		if (keyBtn.equals("Delete")){
			String lResult = ShippingAdapter.DeleteShipping(temp_txtShippingID,sessionUser);
			if(lResult.contains("Success Delete Shipping")) {
				result =  "SuccessDeleteShipping";
            }
            else {
            	result = "FailedDeleteShipping";
            	resultDescription = lResult;
            }
		}

		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDescription);

		return;
	}
}
