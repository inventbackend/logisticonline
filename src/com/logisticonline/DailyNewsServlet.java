package com.logisticonline;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import adapter.MenuAdapter;
import adapter.ValidateNull;
import adapter.VendorAdapter;
import adapter.WriteFileAdapter;
import helper.ConvertDateTimeHelper;
import adapter.DailyNewsAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/News"} , name="News")
@MultipartConfig
public class DailyNewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DailyNewsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDescription = "";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu=false;
    	String MenuURL = "News";
    	
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}

    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		List<model.mdlDailyNews> newsList = new ArrayList<model.mdlDailyNews>();
    		newsList.addAll(DailyNewsAdapter.LoadNews(sessionUser) );
    		request.setAttribute("listNews", newsList);

    		List<model.mdlVendorDriver> driverList = new ArrayList<model.mdlVendorDriver>();
    		driverList.addAll(VendorAdapter.LoadVendorDriverByVendor(sessionUser));
    		request.setAttribute("listDriver", driverList);

    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);

    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDescription);
    		request.setAttribute("buttonstatus", ButtonStatus);

    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/daily_news.jsp");
    		dispacther.forward(request, response);
    	}

    	result = "";
    	resultDescription = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	//	doGet(request, response);
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");

    	String btnDelete = request.getParameter("btnDelete");
    	String btnSave = request.getParameter("btnSave");
    	String btnUpdate = request.getParameter("btnUpdate");

    	if (sessionUser == null || sessionUser == "")
    	{
    		if (btnDelete != null){
    			doGet(request, response);
    		}
    		return;
    	}

    	//Declare TextBox
    	String txtNewsID = request.getParameter("txtNewsID");
    	String temp_txtNewsID = request.getParameter("temp_txtNewsID");
    	String startDateString = ConvertDateTimeHelper.formatDate(request.getParameter("txtStartDateValid"), "dd MMM yyyy", "yyyy-MM-dd");
        String txtStartDateValid = startDateString;
        String endDateString = ConvertDateTimeHelper.formatDate(request.getParameter("txtEndDateValid"), "dd MMM yyyy", "yyyy-MM-dd");
        String txtEndDateValid = endDateString;
        String txtVendorID = sessionUser;
        String txtNewsText = request.getParameter("txtNewsText").replace("\r\n", "<br>");
        String txtNewsImage = Paths.get(request.getPart("txtNewsImage").getSubmittedFileName()).getFileName().toString();

        String[] listDriver = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("cbDriver"));

        String paramFilePath = getServletContext().getInitParameter("param_file_location");
		String paramFileAccess = getServletContext().getInitParameter("param_file_access");
		String paramSeparator = getServletContext().getInitParameter("param_separator");

		//Declare mdlPlant for global
		model.mdlDailyNews mdlDailyNews = new model.mdlDailyNews();
		mdlDailyNews.setNewsID(txtNewsID);
		mdlDailyNews.setStartDateValid(txtStartDateValid);
		mdlDailyNews.setEndDateValid(txtEndDateValid);
		mdlDailyNews.setVendorID(txtVendorID);
		mdlDailyNews.setNewsText(txtNewsText);

		String resultWriteImage = "";
		if (btnSave != null){
			String newNewsID = DailyNewsAdapter.CreateNewsID(sessionUser);
			if(txtNewsImage== null || txtNewsImage.equals("")){
				mdlDailyNews.setNewsImage("");
			}else{
				Part fileNewsImage = request.getPart("txtNewsImage");
				String filename = Paths.get(fileNewsImage.getSubmittedFileName()).getFileName().toString();
				InputStream originalFile = fileNewsImage.getInputStream();
				String newFile = paramFilePath + "News" + paramSeparator + newNewsID + "_" + filename;
				resultWriteImage = WriteFileAdapter.Write(originalFile , newFile);
				mdlDailyNews.setNewsImage(paramFileAccess + "News" + "/" + newNewsID + "_" + filename);
			}

			if(resultWriteImage != "Error Write File"){
				mdlDailyNews.setNewsID(newNewsID);
				String lResult = DailyNewsAdapter.InsertNews(mdlDailyNews, listDriver);

				if(lResult.contains("Success Insert Daily News")) {
					result = "SuccessInsertNews";
	            }
	            else {
	            	result = "FailedInsertNews";
	            	resultDescription = lResult;
	            }
			}else{
				result = "FailedInsertNews";
		        resultDescription = "Image Error";
			}

			doGet(request, response);
		}

		if (btnUpdate != null){
			Boolean updateImage = false;
			if(txtNewsImage == null || txtNewsImage.equals("")){
				mdlDailyNews.setNewsImage("");
			}else{
				updateImage = true;
				Part fileNewsImage = request.getPart("txtNewsImage");
				String filename = Paths.get(fileNewsImage.getSubmittedFileName()).getFileName().toString();
				InputStream originalFile = fileNewsImage.getInputStream();
				String newFile = paramFilePath + "News" + paramSeparator +txtNewsID + "_" + filename;
				resultWriteImage = WriteFileAdapter.Write(originalFile , newFile);
				mdlDailyNews.setNewsImage(paramFileAccess + "News" + "/" + txtNewsID + "_" + filename);
			}

			if(resultWriteImage != "Error Write File"){

				if (updateImage){
					DailyNewsAdapter.DeleteNewsImage(mdlDailyNews.newsID, paramFilePath, paramSeparator, sessionUser);
				}
				String lResult = DailyNewsAdapter.UpdateNews(mdlDailyNews, listDriver, updateImage);


				if(lResult.contains("Success Update Daily News")) {
					result = "SuccessUpdateNews";
	            }
	            else {
	            	result = "FailedUpdateNews";
	            	resultDescription = lResult;
	            }
			}else{
				result = "FailedUpdateNews";
				resultDescription = "Image Error";
			}

			doGet(request, response);
		}

		if (btnDelete != null){
			DailyNewsAdapter.DeleteNewsImage(temp_txtNewsID, paramFilePath, paramSeparator, sessionUser);
			String lResult = DailyNewsAdapter.DeleteNews(temp_txtNewsID, sessionUser);
			if(lResult.contains("Success Delete Daily News")) {
				result =  "SuccessDeleteNews";
            }
            else {
            	result = "FailedDeleteNews";
            	resultDescription = lResult;
            }

			doGet(request, response);
		}

//		response.setContentType("application/text");
//        response.setCharacterEncoding("UTF-8");
//        response.getWriter().write(Globals.gCondition+"--"+Globals.gConditionDesc);

		return;
	}
}
