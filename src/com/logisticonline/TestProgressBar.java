package com.logisticonline;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import helper.LongProcess;

@WebServlet(urlPatterns={"/progressServlet"} , name="progressServlet")
public class TestProgressBar extends HttpServlet {
	
private static final long serialVersionUID = 1490947492185481844L;
int loading = 0;

	public TestProgressBar() {
        super();
        // TODO Auto-generated constructor stub
    }
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	      throws ServletException, IOException {
		  
		  	HttpSession session = req.getSession(true);
		  	loading += 10;
		  
		  	String downloadId = "longProcess_" + req.getParameter("downloadId");
		  	
		  	session.setAttribute("longProcess_12345", loading);
		  	System.out.println(downloadId);
		  	System.out.println(session.getAttribute("longProcess_12345"));
		  	
		  	
		  	LongProcess longProcess = new LongProcess((int)session.getAttribute("longProcess_12345"));
		  	
		  	
		  	System.out.println(longProcess);
		  	int progress = longProcess.getProgress(); System.out.println(progress);
		 

	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(String.valueOf(progress));
	        
	        if (loading == 100) {
				loading = 0;
			}
	        
	  }
	  
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		      throws ServletException, IOException {
		  
	  }

}
