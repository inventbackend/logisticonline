package com.logisticonline;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.google.gson.Gson;

import adapter.APIAdapter;
import adapter.CustomerAdapter;
import adapter.DepoAdapter;
import adapter.KojaAdapter;
import adapter.MenuAdapter;
import adapter.OrderManagementAdapter;
import adapter.OrderManagementDetailAdapter;
import adapter.VendorOrderDetailAdapter;
import adapter.WriteFileAdapter;
import helper.ConvertDateTimeHelper;
import model.mdlKojaParam;
import modelKojaGetOnDemand.mdlGetOnDemandParam;

@WebServlet(urlPatterns={"/Gatepass"} , name="Gatepass")
@MultipartConfig
public class GetYellowCardServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public GetYellowCardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String pPol="";
    String sessionUser, sessionRole, sessionLevel, staffRole, result, resultDesc = "";
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		sessionLevel = (String) session.getAttribute("userlevel");
		
		Boolean CheckMenu = false;
    	String MenuURL = "Gatepass";
    	
    	if(sessionLevel != null){
    		if(sessionLevel.equals("main"))
        		CheckMenu = MenuAdapter.CheckMenu(MenuURL, sessionUser);
        	else if(sessionLevel.equals("staff")){
        		staffRole = (String) session.getAttribute("staffRole");
        		CheckMenu = MenuAdapter.CheckStaffMenu(MenuURL, staffRole);
        	}
    	}
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/Dashboard");
    		dispacther.forward(request, response);
    	}
    	else{
    		String dateNow = LocalDateTime.now().toString();
        	
        	List<model.mdlOrderManagement> orderList = new ArrayList<model.mdlOrderManagement>();
        	
        	if(pPol.equals(""))
        		orderList.addAll(OrderManagementAdapter.LoadDocumentation(dateNow, sessionUser));
        	else
        		orderList.addAll(OrderManagementAdapter.LoadDocumentationByPol(dateNow, pPol, sessionUser, sessionRole));
        	
        	request.setAttribute("listDocumentation", orderList);
        	
        	//load npwp data
        	List<model.mdlOnbehalfNpwp> npwpList = new ArrayList<model.mdlOnbehalfNpwp>();
        	if(sessionRole.equals("R002")){
        		model.mdlCustomer customer = new model.mdlCustomer();
            	customer = CustomerAdapter.LoadSingleCustomer(sessionUser);
            	model.mdlOnbehalfNpwp npwp = new model.mdlOnbehalfNpwp();
            	npwp.onbehalfCustomer = customer.CustomerName;
            	npwp.onbehalfNpwp = customer.NPWP;
            	npwpList.add(npwp);
            	npwpList.addAll(CustomerAdapter.LoadOnbehalfNpwp(sessionUser, sessionUser));
        	}
        	request.setAttribute("listNpwp", npwpList);
    		
    		String ButtonStatus;
    		if(sessionLevel.equals("main"))
    			ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, sessionUser);
    		else
    			ButtonStatus = MenuAdapter.SetStaffMenuButtonStatus(MenuURL, staffRole);
    		
    		request.setAttribute("pol", pPol);
    		request.setAttribute("condition", result);
    		request.setAttribute("conditionDescription", resultDesc);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("globalUserID", sessionUser);
    		request.setAttribute("globalUserRole", sessionRole);
    		
    		RequestDispatcher dispacther;
			dispacther = request.getRequestDispatcher("/mainform/pages/getDocumentation.jsp");
			dispacther.forward(request, response);
    	}
    	
    	result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//Declare TextBox
		String txtSI = request.getParameter("txtSI");
				
		if (keyBtn.equals("UpdateDocumentation")){
			Boolean valid = false;
			String txtPEB = request.getParameter("txtPEB");
			String txtNPE = request.getParameter("txtNPE");
			String txtPortID = request.getParameter("txtPortID");
			String txtNPEDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtNPEDate"), "dd MMM yyyy", "yyyy-MM-dd");
			String slNPWP = request.getParameter("slNPWP");
			
			//-- image process
			String baseFilePath = getServletContext().getInitParameter("param_file_location");
			String baseServerIpPort = getServletContext().getInitParameter("param_server_ipport");
			String baseFileAccess = getServletContext().getInitParameter("param_file_access");
			String baseSeparator = getServletContext().getInitParameter("param_separator");
			Part fileNPE = request.getPart("txtNpeDoc");
			String txtNPEDoc = Paths.get(request.getPart("txtNpeDoc").getSubmittedFileName()).getFileName().toString();
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int year  = localDate.getYear();
			String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue() ), "M", "MMM");
			int day   = localDate.getDayOfMonth();
			String paramFilePath = baseFilePath + "NPE" + baseSeparator + year + baseSeparator + month + baseSeparator + day + baseSeparator + txtNPE + baseSeparator;
			String paramFileAccess = baseServerIpPort + baseFileAccess + "NPE" + "/" + year + "/" + month + "/" + day + "/" + txtNPE + "/";
			
			InputStream originalFile = fileNPE.getInputStream();
			String newFile = paramFilePath + txtNPEDoc;
			WriteFileAdapter.Write2(originalFile , paramFilePath, newFile);
			//end of image process --
			
			model.mdlOrderManagement mdlOrderManagement = new model.mdlOrderManagement();
			mdlOrderManagement.setShippingInvoiceID(txtSI);
			mdlOrderManagement.setPeb(txtPEB);
		    mdlOrderManagement.setNpe(txtNPE);
		    mdlOrderManagement.setNpeDate(txtNPEDate);
		    mdlOrderManagement.setNpeDoc(paramFileAccess + txtNPEDoc);
		    mdlOrderManagement.setNpwp(slNPWP);
		    
		    //check is it koja transaction or not
		    if(txtPortID.equals("PORT-0002")){//if koja, do this
		    	mdlKojaParam kojaParam = new model.mdlKojaParam();
				 kojaParam.serviceUrl = getServletContext().getInitParameter("koja_url");
				 kojaParam.username = getServletContext().getInitParameter("koja_username");
				 kojaParam.password = getServletContext().getInitParameter("koja_password");
				 kojaParam.fstreamusername = getServletContext().getInitParameter("koja_fstream_username");
				 kojaParam.fstreampassword = getServletContext().getInitParameter("koja_fstream_password");
				 kojaParam.deviceName = getServletContext().getInitParameter("koja_devicename");
				 
				 kojaParam.serviceUrl2 = getServletContext().getInitParameter("asso_url");
				 kojaParam.username2 = getServletContext().getInitParameter("asso_username");
				 kojaParam.password2 = getServletContext().getInitParameter("asso_password");
				 
				 mdlGetOnDemandParam getOnDemandParam = new mdlGetOnDemandParam();
				 getOnDemandParam.orderID = request.getParameter("txtOrderID");
				 getOnDemandParam.documentNo = txtNPE;
				 getOnDemandParam.peb = txtPEB;
				 getOnDemandParam.documentDate = ConvertDateTimeHelper.formatDate(request.getParameter("txtNPEDate"), "dd MMM yyyy", "ddMMyyyy");
				 getOnDemandParam.npwp = slNPWP;
				 getOnDemandParam.exportLimit = Integer.valueOf(request.getParameter("txtExportLimit"));
				 
				 //check export limit
				 Boolean checkExportLimit = KojaAdapter.CheckExportLimit(getOnDemandParam, sessionUser);
				 if(checkExportLimit){
					//if export limit not exceed, do this
					//check voyage closing doc date time
					String voyageClosingDoc = request.getParameter("txtVoyageClosingDoc");
					Boolean checkVoyageClosingDoc = KojaAdapter.CheckVoyageClosingDoc(voyageClosingDoc, sessionUser);
					
					if(checkVoyageClosingDoc){
						//if valid, integrate to port API to get on demand/upload manual
						String onDemandResult = APIAdapter.KojaMainProcessShipper(kojaParam, getOnDemandParam);
					 	if(onDemandResult.equals("Sukses"))
					 		valid = true;
					 	else{
					 		result = "FailedUpdateDocumentation";
					 		resultDesc =  onDemandResult;
					 	}
					}
					else{
						//if voyage closing document date time have been passed, do this
						result = "FailedUpdateDocumentation";
				        resultDesc = "Request failed due to voyage closing document date time. Please contact administrator/shipping line";
					}
				 }
				 else{
					//if export limit exceed, do this
					 result = "FailedUpdateDocumentation";
			         resultDesc = "Request failed due to export limit. Please contact administrator/shipping line";
				 }
		    }
		    else
		    	valid = true;
		    
		    
		    if(valid){
		    	String lResult = OrderManagementAdapter.UpdateDocumentation(mdlOrderManagement, sessionUser);
				if(lResult.contains("Success Update Documentation")) {
					result = "SuccessUpdateDocumentation";
		        }
		        else {
		        	result = "FailedUpdateDocumentation";
		        	resultDesc = lResult;
		        }
		    }
		    
		    response.setContentType("application/text");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result+"--"+resultDesc);
		}
		else if(keyBtn.equals("GetYellowCard")){
			String lResult = OrderManagementAdapter.GetYellowCard(txtSI,sessionUser);
			if(lResult.contains("Success Get Yellow Card")) {
				result = "SuccessGetGatepass";
	        }
	        else {
	        	result = "FailedGetGatepass";
	        	resultDesc = lResult;
	        }
		}
		
		if(keyBtn.equals("getContainer")) {
			
			String orderID = request.getParameter("OrderID");
			System.out.println(request.getParameter("OrderID"));
			List<model.mdlDeliveryOrder> listDO = VendorOrderDetailAdapter.LoadVendorOrderDetailByOrder(orderID, sessionUser);
			ArrayList<String> arrContainerNumber=new ArrayList<String>();
			
			for (model.mdlDeliveryOrder list : listDO) {
				arrContainerNumber.add(list.getContainerNumber());
			}
			
			System.out.println(arrContainerNumber.toString());
			String jsonlistDistrict = null;
			Gson gson = new Gson();
			jsonlistDistrict = gson.toJson(arrContainerNumber);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonlistDistrict);
		}
		
		if(keyBtn.equals("search")) {
			//Declare class private string
	    	pPol = request.getParameter("pol");
		}
		
    }
    
}
