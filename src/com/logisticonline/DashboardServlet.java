package com.logisticonline;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.json.XML;

import adapter.APIAdapter;
import adapter.ConvertDateTimeHelper;
import adapter.CustomerAdapter;
import adapter.DashboardAdapter;
import adapter.OrderManagementAdapter;
import model.Globals;
import model.mdlCustomer;
import model.mdlDashboard;

/** Documentation
 * 
 */
@WebServlet(urlPatterns={"/Dashboard"} , name="Dashboard")
public class DashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DashboardServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }

    String sessionUser, sessionRole, result, resultDesc = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		Boolean isFirstLogin = false;
		
		//get date and time now
		DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		String dateNow = dateFormat.format(date);
		String timeNow = timeFormat.format(date);
		request.setAttribute("dateNow", dateNow);
		request.setAttribute("timeNow", timeNow);
		
		if(sessionUser != null){
			sessionRole = (String) session.getAttribute("userrole");
			
			//role as customer
			if(sessionRole.contentEquals("R002")){
				
				List<model.mdlCustomer> customerList = new ArrayList<model.mdlCustomer>();
        		customerList.addAll(CustomerAdapter.LoadCustomerByID(sessionUser));
        		
        		for (mdlCustomer mdlCustomerRoleUser : customerList) {
					System.out.println(mdlCustomerRoleUser.CustomerID);
					isFirstLogin = mdlCustomerRoleUser.isFirstLogin;
				}
        		
				isFirstLogin = false;
				
				
				if(isFirstLogin){
					PrintWriter out = response.getWriter();
					out.println("<script type=\"text/javascript\">");
					out.println("alert('This is your first login, please complete the data of your company');");
					out.println("location='"+ request.getContextPath() + "/Customer';");
					out.println("</script>");
				}
				else{
					//Dashboard atas
					model.mdlDashboard dashboardCustomer1 = DashboardAdapter.LoadDashboardCustomer1(sessionUser);
					List<model.mdlDashboard> listDashboardCustomer1 = new ArrayList<model.mdlDashboard>();
					String[] boxTitleList = {"Total Order", "Total Unprocessed Order", "Total Processed Order", "Total Delivered", "GATEPASS RELEASED"};
					String[] boxInfoList = {"total accumulated today and pending order", 
											"total order picked by trucker but truck is not allocated", 
											"total order received by trucker and truck is allocated", 
											"total order completion",
											""};
					Integer[] boxValueList = {dashboardCustomer1.totalOrder, 
											dashboardCustomer1.totalOrderUnProcessed, 
											dashboardCustomer1.totalOrderOnGoing,
											dashboardCustomer1.totalOrderCompleted,
											dashboardCustomer1.totalGatePass};
					for(int i=0; i<5; i++){
						model.mdlDashboard dashboard = new model.mdlDashboard();
						dashboard.boxTitle = boxTitleList[i];
						dashboard.boxValue = boxValueList[i];
						dashboard.boxInfo = boxInfoList[i];
						listDashboardCustomer1.add(dashboard);
					}
					request.setAttribute("listDashboard1", listDashboardCustomer1);
					
					//Dashboard Bawah
					List<model.mdlDashboard> listDashboardBawah = new ArrayList<model.mdlDashboard>();
					listDashboardBawah = DashboardAdapter.LoadDashboardBawah(sessionRole, sessionUser);
					request.setAttribute("listDashboard2", listDashboardBawah);
					
					//Dashboard Bawah untuk pending order
					List<model.mdlDashboard> listDashboardPending = new ArrayList<model.mdlDashboard>();
					listDashboardPending = DashboardAdapter.LoadDashboardPending(sessionRole, sessionUser);
					request.setAttribute("listDashboardPending", listDashboardPending);
				}
			}
			//role as vendor
			else if(sessionRole.contentEquals("R003")){
				//Dashboard atas
				model.mdlDashboard dashboardVendor1 = DashboardAdapter.LoadDashboardVendor1(sessionUser);
				List<model.mdlDashboard> listDashboardVendor1 = new ArrayList<model.mdlDashboard>();
				String[] boxTitleList = {"Total Order", "Unprocessed Order", "Processed Order", "Total Delivered", "GATEPASS RELEASED"};
				Integer[] boxValueList = {dashboardVendor1.totalOrder, 
										dashboardVendor1.availableOrder, 
										dashboardVendor1.totalOrderOnGoing,
										dashboardVendor1.totalOrderCompleted,
										dashboardVendor1.totalGatePass};
				for(int i=0; i<5; i++){
					model.mdlDashboard dashboard = new model.mdlDashboard();
					dashboard.boxTitle = boxTitleList[i];
					dashboard.boxValue = boxValueList[i];
					listDashboardVendor1.add(dashboard);
				}
				request.setAttribute("listDashboard1", listDashboardVendor1);
				
				//Dashboard Bawah
				List<model.mdlDashboard> listDashboardBawah = new ArrayList<model.mdlDashboard>();
				listDashboardBawah = DashboardAdapter.LoadDashboardBawah(sessionRole, sessionUser);
				request.setAttribute("listDashboard2", listDashboardBawah);
			}
			
			request.setAttribute("globalUserRole", sessionRole);
		}
		
		if(!isFirstLogin){
			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/dashboard.jsp");
			/*response.addHeader("X-Frame-Options", "DENY");*/
			dispacther.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
