package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/site_master"} , name="site_master")
public class SiteMasterServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    public SiteMasterServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    //String[] list_user_area;
    String sessionUser, sessionLevel = "";
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionLevel = (String) session.getAttribute("userlevel");
		//Globals.userrole = (String) session.getAttribute("userrole");
		
		//GetUserPlantSession(request, response);
		
    	//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//if (keyBtn.equals("RestrictedMenu")){
		if (keyBtn.equals("AllowedMenu")){
	    	List<model.mdlMenu> MenuList = new ArrayList<model.mdlMenu>();
	    	//MenuList = UserAdapter.LoadRestrictedMenu(Globals.user);
	    	
	    	if(sessionLevel.equals("main"))
	    		//get the allowed menu for main user level access
	    		MenuList = UserAdapter.LoadAllowedMenuforAccess(sessionUser);
	    	else{
	    		//get the allowed menu for staff user level access
	    		String staffRole = (String) session.getAttribute("staffRole");
	    		MenuList = UserAdapter.LoadStaffAllowedMenu(staffRole, sessionUser);
	    	}
	    	
	    	Gson gson = new Gson();
	    	
	    	String jsonlistMenuID = gson.toJson(MenuList);
	    	
	    	response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(jsonlistMenuID);
		}
	}

//	public void GetUserPlantSession(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		Globals.user_area = "";
//        
//        //setting session for user area/plant access
//		String user_area = UserAdapter.LoadUserArea(Globals.user);
//		if(user_area.contentEquals("") || user_area==null)
//		{
//		}
//		
//		list_user_area = user_area.split(",");
//		int list_user_area_length = list_user_area.length; //set the length of list
//		for(int i=0; i<list_user_area_length;i++)
//		{
//			if(Globals.user_area.contentEquals("")) //if first index
//				Globals.user_area = "'"+list_user_area[i]+"'";
//			else
//				Globals.user_area += ","+"'"+list_user_area[i]+"'";
//		}
//	}

}
