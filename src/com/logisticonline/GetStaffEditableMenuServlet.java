package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.UserAdapter;

@WebServlet(urlPatterns={"/getStaffEditableMenu"} , name="getStaffEditableMenu")
public class GetStaffEditableMenuServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    public GetStaffEditableMenuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
    	
		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
		String lUserID = request.getParameter("userid");
		mdlMenuList.addAll(MenuAdapter.LoadStaffEditableMenu(lUserID,sessionUser));
		request.setAttribute("listEditableMenu", mdlMenuList);

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getStaffEditableMenu.jsp");
		dispacther.forward(request, response);
	}
    
}
