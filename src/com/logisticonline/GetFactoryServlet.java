package com.logisticonline;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.FactoryAdapter;

@WebServlet(urlPatterns={"/getfactory"} , name="getfactory")
public class GetFactoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public GetFactoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String sessionUser, sessionRole = "";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		sessionRole = (String) session.getAttribute("userrole");
		
		String lCustomerID = request.getParameter("customerid");
		List<model.mdlFactory> factoryList = new ArrayList<model.mdlFactory>();
		
		if(sessionUser.equals(lCustomerID) || sessionRole.equals("R001")){
	    	factoryList.addAll(FactoryAdapter.LoadFactoryByCustomer(lCustomerID, sessionUser));
			request.setAttribute("listFactory", factoryList);
		}
		else{
			request.setAttribute("listFactory", factoryList);
		}

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getfactory.jsp");
		dispacther.forward(request, response);
	}
}