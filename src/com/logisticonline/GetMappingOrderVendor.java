package com.logisticonline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import adapter.VendorAdapter;

@WebServlet(urlPatterns={"/getMappingOrderVendor"} , name="getMappingOrderVendor")
public class GetMappingOrderVendor extends HttpServlet{

private static final long serialVersionUID = 1L; 
	
	public GetMappingOrderVendor() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String sessionUser = "";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		sessionUser = (String) session.getAttribute("user");
		
    	String lOrderID = request.getParameter("orderid");
    	List<model.mdlVendor> listVendor = new ArrayList<model.mdlVendor>();
    	Gson gson = new Gson();
    	String json = "";
    	
        listVendor.addAll(VendorAdapter.LoadVendorByOrder(lOrderID, sessionUser));
        json = gson.toJson(listVendor);
    	
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
}
