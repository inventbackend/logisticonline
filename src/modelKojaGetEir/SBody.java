package modelKojaGetEir;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SBody {

	@SerializedName("ns2:customServiceResponse")
    @Expose
    private Ns2CustomServiceResponse ns2CustomServiceResponse;

    public Ns2CustomServiceResponse getNs2CustomServiceResponse() {
        return ns2CustomServiceResponse;
    }

    public void setNs2CustomServiceResponse(Ns2CustomServiceResponse ns2CustomServiceResponse) {
        this.ns2CustomServiceResponse = ns2CustomServiceResponse;
    }
    
}
