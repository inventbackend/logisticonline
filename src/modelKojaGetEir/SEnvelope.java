package modelKojaGetEir;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SEnvelope {

	@SerializedName("xmlns:S")
    @Expose
    private String xmlnsS;
    @SerializedName("S:Body")
    @Expose
    private SBody sBody;

    public String getXmlnsS() {
        return xmlnsS;
    }

    public void setXmlnsS(String xmlnsS) {
        this.xmlnsS = xmlnsS;
    }

    public SBody getSBody() {
        return sBody;
    }

    public void setSBody(SBody sBody) {
        this.sBody = sBody;
    }
    
}
