package modelKojaGetFD;

public class mdlKojaFD {

	public String STATUS;
	public String[] PORT_CODE;
	public String[] PORT_NAME;
	public String TerminalID;
	
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String[] getPORT_CODE() {
		return PORT_CODE;
	}
	public void setPORT_CODE(String[] pORT_CODE) {
		PORT_CODE = pORT_CODE;
	}
	public String[] getPORT_NAME() {
		return PORT_NAME;
	}
	public void setPORT_NAME(String[] pORT_NAME) {
		PORT_NAME = pORT_NAME;
	}
	public String getTerminalID() {
		return TerminalID;
	}
	public void setTerminalID(String terminalID) {
		TerminalID = terminalID;
	}
}
