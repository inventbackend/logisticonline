package modelKojaGetFD;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

@SerializedName("ns1:MAIN_GetFDResponse")
@Expose
private Ns1MAINGetFDResponse ns1MAINGetFDResponse;

public Ns1MAINGetFDResponse getNs1MAINGetFDResponse() {
return ns1MAINGetFDResponse;
}

public void setNs1MAINGetFDResponse(Ns1MAINGetFDResponse ns1MAINGetFDResponse) {
this.ns1MAINGetFDResponse = ns1MAINGetFDResponse;
}

}
