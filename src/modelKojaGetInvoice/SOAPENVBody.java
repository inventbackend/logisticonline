
package modelKojaGetInvoice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns1:BILLING_GetInvoiceResponse")
    @Expose
    private Ns1BILLINGGetInvoiceResponse ns1BILLINGGetInvoiceResponse;

    public Ns1BILLINGGetInvoiceResponse getNs1BILLINGGetInvoiceResponse() {
        return ns1BILLINGGetInvoiceResponse;
    }

    public void setNs1BILLINGGetInvoiceResponse(Ns1BILLINGGetInvoiceResponse ns1BILLINGGetInvoiceResponse) {
        this.ns1BILLINGGetInvoiceResponse = ns1BILLINGGetInvoiceResponse;
    }

}
