﻿var objJsonDistributorData, objJsonOutletData;
var objDistMarkers = [];
var objOutletMarkers = [];
var objSalesMarker = [];
var objJsonCategoryData = [];

//First Deploy
fCategoryData();

function fGetDistributorMarker(strAsmCode) {
    $.ajax({
        type: "POST",
        url: 'WebService/GoogleWebData.asmx/GetDistList',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status) {
            objJsonDistributorData = JSON.parse(data.d);
            var objData = JSON.parse(data.d);

            for (var x = 0; x < objData.length; x++) {
                strId = objData[x].AsmCode;
                strName = objData[x].DistName;
                strInfoWindow = "<b>" + objData[x].DistName + "<\/b><p style='font-size:smaller'>" + objData[x].DistAddress + "<\/p>";
                //strInfoWindow = "";
                strType = "";
                strCategory = "Distributor";

                if (objData[x].DistLat != "" && objData[x].DistLng != "") {
                    // Obtain the attribues of each marker
                    lat = parseFloat(objData[x].DistLat);
                    lng = parseFloat(objData[x].DistLng);

                    if (!isNaN(lat) && !isNaN(lng)) {
                        //Create Marker
                        intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                        fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objDistMarkers)

                        if (typeof (strAsmCode) !== 'undefined') {
                            if (strAsmCode == objData[x].AsmCode) {
                                m_strCenterMap = intLatLng;
                                m_strDistroLoc = intLatLng;
                                objMap.panTo(intLatLng);
                            }
                        }
                    }
                }

                objDepo = objData[x].Depo;
                for (var i = 0; i < objDepo.length; i++) {
                    strId = objData[x].AsmCode;
                    strName = objDepo[i].DistDepoCd;
                    strInfoWindow = "<b>" + objDepo[i].DistDepoCd + "<\/b><p style='font-size:smaller'>" + objDepo[i].DistDepoAddress + "<\/p>";
                    //strInfoWindow = "";
                    strType = objDepo[i].DistDepoCd;
                    strCategory = "Depo";

                    if (objDepo[i].DistDepoLat != "" && objDepo[i].DistDepoLng != "") {
                        // Obtain the attribues of each marker
                        lat = parseFloat(objDepo[i].DistDepoLat);
                        lng = parseFloat(objDepo[i].DistDepoLng);

                        if (!isNaN(lat) && !isNaN(lng)) {
                            //Create Marker
                            intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                            fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objDistMarkers)
                        }
                    }
                }
            }
            fLoadDistributorMarkerSuccess();
        },
        error: fLoadDistributorMarkerError
    });
}


function fGetOutletMarker(strAsmCode, strDsrCode) {
    $.ajax({
        type: "POST",
        url: 'WebService/GoogleWebData.asmx/GetOutletList',
        data: "{'strAsmCode':'" + strAsmCode + "', 'strDsrCode':'" + strDsrCode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status) {
            objJsonOutletData = JSON.parse(data.d);
            var objData = JSON.parse(data.d);

            for (var i = 0; i < objData.length; i++) {
                strId = objData[i].AsmCode;
                strName = objData[i].AsmName;

                objSalesList = objData[i].SalesList
                for (var y = 0; y < objSalesList.length; y++) {
                    strSalesCode = objSalesList[y].SalesCode,
                    strSalesName = objSalesList[y].SalesName,

                    objOutletList = objSalesList[y].OutletList

                    for (var x = 0; x < objOutletList.length; x++) {
                        strId = objOutletList[x].OutletId;
                        strName = objOutletList[x].OutletNm;
                        strInfoWindow = "<div style='width:300px;margin:0 0 20px 20px;height:60px;'><b>" + objOutletList[x].OutletNm + "<\/b><p style='font-size:smaller'> Omset=" + objOutletList[x].Omset + "<\/p><p style='font-size:smaller'><span id=clock style='font-size:smaller'><\/span><\/p><\/div>";
                        //strInfoWindow = "";
                        objJsonOutletData[i].SalesList[y].OutletList[x].StatusMark = "";

                        strType = objOutletList[x].SalesCode;

                        //Outlet Category
                        for (var z = 0; z < objJsonCategoryData.length; z++) {
                            if (objJsonCategoryData[z].OutletCategory == objOutletList[x].OutletCategory) {
                                strCategory = objJsonCategoryData[z].CategoryKey;
                                break;
                            }
                        }

                        if (objOutletList[x].OutletLat != "" && objOutletList[x].OutletLng != "") {
                            // Obtain the attribues of each marker
                            lat = parseFloat(objOutletList[x].OutletLat);
                            lng = parseFloat(objOutletList[x].OutletLng);

                            if (!isNaN(lat) && !isNaN(lng)) {
                                objJsonOutletData[i].SalesList[y].OutletList[x].StatusMark = "Y";

                                //Create Marker
                                intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                                fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objOutletMarkers)
                            }
                        }
                    }
                }
            }

            fLoadOutletMarkerSuccess();
        },
        error: fLoadOutletMarkerError
    });
}

function fGetSalesMarker() {
    fclearOverlays(objSalesMarker);
    objSalesMarker = [];

    $.ajax({
        type: "POST",
        url: 'WebService/GoogleWebData.asmx/GetSalesTracking',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, status) {
            objJsonOutletData = JSON.parse(data.d);
            var objData = JSON.parse(data.d);

            for (var i = 0; i < objData.length; i++) {
                //Json Attribute
                strDsrCode = objData[i].DsrCode;
                strSalesCode = objData[i].SalesCode;
                strSalesName = objData[i].SalesName;
                strSiteId = objData[i].SiteId;

                strDate = objData[i].Tanggal;
                strTime = objData[i].Jam;
                strLng = objData[i].longitude;
                strLat = objData[i].latitude;
                strBattery = objData[i].BatraiStatus;

                strAsmCode = objData[i].AsmCode;
                strAsmName = objData[i].AsmName;

                strCustCode = objData[i].DistCd;
                strCustName = objData[i].DistName;

                if (strLat != "" && strLng != "") {
                    //Marker Attribute
                    strId = strSalesCode;
                    strName = strSalesName + " - " + strDsrCode;
                    strType = strAsmCode;

                    strInfoWindow = "<b>" + strSalesName + " - " + strDsrCode + "<\/b>" +
							    "<p style='font-size:smaller'>" +
							    "ASM : " + strAsmCode + " - " + strAsmName + " <br />" +
							    "Dist : " + strCustCode + " - " + strCustName + " <br />" +
							    "Time : " + strDate + " - " + strTime + " <br />" +
							    "Battery : " + strBattery + " " +
							    "<\/p>";
                    strCategory = "Gimg";

                    // Obtain the attribues of each marker
                    lat = parseFloat(strLat);
                    lng = parseFloat(strLng);

                    if (!isNaN(lat) && !isNaN(lng)) {
                        //Create Marker
                        intLatLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                        fCreateMarker(intLatLng, strId, strName, strInfoWindow, strType, strCategory, objSalesMarker)
                    }
                }
            }

            fLoadSalesMarkerSuccess();
        },
        error: fLoadSalesMarkerError
    });
}

function fGotoDistributorMarker(strAsmCode, strType) {
    with (document.forms[0]) {
        var bolDistro = false;

        for (var i = 0, m; m = objDistMarkers[i]; i++) {
            if (m.id == strAsmCode) {
                if (typeof (strType) !== 'undefined') {
                    if (objDistMarkers[i].type == strType) {
                        google.maps.event.trigger(objDistMarkers[i], "click");
                        intLatLng = objDistMarkers[i].getPosition();

                        m_strCenterMap = intLatLng;
                        m_strDistroLoc = intLatLng;
                        objMap.panTo(intLatLng);

                        bolDistro = true;
                        break;
                    }
                } else {
                    google.maps.event.trigger(objDistMarkers[i], "click");
                    intLatLng = objDistMarkers[i].getPosition();

                    m_strCenterMap = intLatLng;
                    m_strDistroLoc = intLatLng;
                    objMap.panTo(intLatLng);

                    bolDistro = true;
                    break;
                }
            }
        }

        
        //Get Data
        if (bolDistro == false) {
            for (var i = 0; i < objJsonDistributorData.length; i++) {
                if (strAsmCode == objJsonDistributorData[i].AsmCode) {
                    if (typeof (strType) !== 'undefined') {
                        objDepo = objJsonDistributorData[i].Depo;
                        for (var x = 0; x < objDepo.length; x++) {
                            if (objDepo[x].DistDepoCd == strType) {
                                objGeocoder.geocode({ 'address': "Indonesia, " + objDepo[x].DistDepoCity }, function (results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        objMap.setCenter(results[0].geometry.location);
                                    }
                                });
                                break;
                            }
                        }
                    } else {
                        objGeocoder.geocode({ 'address': "Indonesia, " + objJsonDistributorData[i].DistCity }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                objMap.setCenter(results[0].geometry.location);
                            }
                        });
                    }
                    break;
                }
            }
        }
    }
}

//function fCategoryData() {
//    $.ajax({
//        type: "POST",
//        url: 'WebService/GoogleWebData.asmx/GetCategoryOutlet',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data, status) {
//            objJsonCategoryData = JSON.parse(data.d);
//        },
//        error: function (request, status, error) {
//            alert("Error : " + request.statusText);
//        }
//    });
//}