<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Tier Vehicle</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <style type="text/css">	
	#ModalUpdateInsert { overflow-y:scroll }
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Vendor_Vehicle" action = "${pageContext.request.contextPath}/VendorVehicle" method="post">
<input  type="hidden" id="temp_userrole" name="temp_userrole" value="<c:out value="${userRole}"/>" />
<input  type="hidden" id="temp_vendorid" name="temp_vendorid" value="<c:out value="${vendorID}"/>" />

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Vehicle List
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
		
		<c:if test="${condition == 'SuccessInsertVendorVehicle'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Insert success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessUpdateVendorVehicle'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Update success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteVendorVehicle'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Delete success.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteVendorVehicle'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> Add Vehicle</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
		      		<div class="row">
					  	<div id="dvVendorID" class="form-group col-md-12">
			            <label for="lblVendorID" class="control-label">Trucker ID:</label><label id="mrkVendorID" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtVendorID" name="txtVendorID" readonly="readonly">
			          	</div>
			          	<div id="dvVehicleNumber" class="form-group col-md-12">
			            <label for="lblVehicleNumber" class="control-label">Vehicle Number:</label><label id="mrkVehicleNumber" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtVehicleNumber" name="txtVehicleNumber">
			            <input type="hidden" class="form-control" id="txtVehicleNumberHidden" name="txtVehicleNumberHidden">
			          	</div>
			          	<div id="dvBrand" class="form-group col-md-12">
			            <label for="lblBrand" class="control-label">Brand:</label><label id="mrkBrand" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtBrand" name="txtBrand">
			          	</div>
			          	<div id="dvType" class="form-group col-md-12">
			            <label for="lblType" class="control-label">Type:</label><label id="mrkType" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtType" name="txtType">
			          	</div>
			          	<div id="dvYear" class="form-group col-md-12">
			            <label for="lblYear" class="control-label">Year:</label><label id="mrkYear" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtYear" name="txtYear">
			          	</div>
			          	<div id="dvTidNumber" class="form-group col-md-4">
			            <label for="lblTidNumber" class="control-label">TID Number:</label><label id="mrkTidNumber" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtTidNumber" name="txtTidNumber">
			          	</div>
			          	<div id="dvStnkNumber" class="form-group col-md-4">
			            <label for="lblStnkNumber" class="control-label">STNK Number:</label><label id="mrkStnkNumber" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtStnkNumber" name="txtStnkNumber">
			          	</div>
			          	<div id="dvStnkExpired" class="form-group col-md-4">
			            <label for="lblStnkExpired" class="control-label">STNK Expired:</label><label id="mrkStnkExpired" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtStnkExpired" name="txtStnkExpired">
			          	</div>
		          	</div>
		      </div>
		      <div class="modal-footer">		        
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i>Save</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
	<!--modal show vendor data -->
		<div class="modal fade" id="ModalGetVendorID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendorID">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="ModalLabelVendorID"></h4>
					</div>
								<div class="modal-body">
    								<table id="tb_master_vendor" class="table table-bordered table-hover">
			        		<thead style="background-color: #d2d6de;">
				                <tr>
				                <th>Trucker ID</th>
				                <th>Name</th>
				                <th>Address</th>
								<th></th>
				                </tr>
			        		</thead>
			        		<tbody>
			        			<c:forEach items="${listVendor}" var ="vendor">
							        <tr>
							        <td><c:out value="${vendor.vendorID}"/></td>
							        <td><c:out value="${vendor.vendorName}"/></td>
							        <td><c:out value="${vendor.address}"/></td>
							        <td><button type="button" class="btn btn-primary"
						        			data-toggle="modal"
						        			onclick="FuncPassStringVendor('<c:out value="${vendor.vendorID}"/>','<c:out value="${vendor.vendorName}"/>')"
						        			data-dismiss="modal"
							        	><i class="fa fa-fw fa-check"></i></button>
							        </td>
					        		</tr>
			        			</c:forEach>
			        		</tbody>
		        		</table>
								</div>
				</div>
			</div>
		</div>
		<!-- /. end of modal show vendor data -->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Vehicle</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtVendorID" name="temp_txtVendorID"  />
						<input type="hidden" id="temp_txtVehicleNumber" name="temp_txtVehicleNumber"  />
									<p>Are you sure to delete this vehicle data ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_vendor_vehicle" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
		                <c:if test="${userRole eq 'R001'}">
		                	<th>Trucker ID</th>
		                  	<th>Name</th>
		                </c:if>
	                  <th>Vehicle Number</th>
	                  <th>Brand</th>
	                  <th>Type</th>
	                  <th>Year</th>
	                  <th>TID Number</th>
	                  <th>STNK Number</th>
	                  <th>STNK Expired</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listVendorVehicle}" var ="vendorvehicle">
		        	<tr>
			        	<c:if test="${userRole eq 'R001'}">
					        <td><c:out value="${vendorvehicle.vendorID}"/></td>
				        	<td><c:out value="${vendorvehicle.vendorName}"/></td>
				        </c:if>
					<td><c:out value="${vendorvehicle.vehicleNumber}"/></td>
					<td><c:out value="${vendorvehicle.brand}"/></td>
					<td><c:out value="${vendorvehicle.type}"/></td>
					<td><c:out value="${vendorvehicle.year}"/></td>
					<td><c:out value="${vendorvehicle.tidNumber}"/></td>
					<td><c:out value="${vendorvehicle.stnkNumber}"/></td>
					<td><c:out value="${vendorvehicle.stnkExpired}"/></td>
					<td>
		        		<table>
							<td>
								<button style="display: <c:out value="${buttonstatus}"/>" id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
									onclick="FuncButtonUpdate('<c:out value="${vendorvehicle.vendorID}" />')"
									title="edit"
									data-target="#ModalUpdateInsert" 
									data-lvendorid='<c:out value="${vendorvehicle.vendorID}" />'
									data-lvehiclenumber='<c:out value="${vendorvehicle.vehicleNumber}" />'
									data-lbrand='<c:out value="${vendorvehicle.brand}" />'
									data-ltype='<c:out value="${vendorvehicle.type}" />'
									data-lyear='<c:out value="${vendorvehicle.year}" />'
									data-lstnknumber='<c:out value="${vendorvehicle.stnkNumber}" />'
									data-lstnkexpired='<c:out value="${vendorvehicle.stnkExpired}" />'
									data-ltidnumber='<c:out value="${vendorvehicle.tidNumber}" />'
									>
									<i class="fa fa-edit"></i></button>
															
								<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" 
				        		data-lvendorid='<c:out value="${vendorvehicle.vendorID}"/>'
				        		data-lvehiclenumber='<c:out value="${vendorvehicle.vehicleNumber}"/>'
				        		title="delete"
				        		>
				        		<i class="fa fa-trash"></i>
				        	</button>
							</td>
							</table>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- bootstrap datepicker -->
<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_vendor_vehicle').DataTable();
    $('#M002').addClass('active');
	$('#M016').addClass('active');
	
	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	
	$("#dvErrorAlert").hide();
	
	$(":input").inputmask();
  	$("#txtYear").inputmask({"mask": "9999"});
  	
  	$('#txtStnkExpired').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lVendorID = button.data('lvendorid');
		var lVehicleNumber = button.data('lvehiclenumber');
		$("#temp_txtVendorID").val(lVendorID);
		$("#temp_txtVehicleNumber").val(lVehicleNumber);
	})
	</script>

<!-- General Script -->
<script>
	function FuncClear(){
		$('#mrkVendorID').hide();
		$('#mrkVehicleNumber').hide();
		$('#mrkBrand').hide();
		$('#mrkType').hide();
		$('#mrkYear').hide();
		$('#mrkTidNumber').hide();
		$('#mrkStnkNumber').hide();
		$('#mrkStnkExpired').hide();
		
		$('#dvVendorID').removeClass('has-error');
		$('#dvVehicleNumber').removeClass('has-error');
		$('#dvBrand').removeClass('has-error');
		$('#dvType').removeClass('has-error');
		$('#dvYear').removeClass('has-error');
		$('#dvTidNumber').removeClass('has-error');
		$('#dvStnkNumber').removeClass('has-error');
		$('#dvStnkExpired').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
	  	var modal = $(this)
	 	
		//modal.find('.modal-body #txtVendorID').val(button.data('lvendorid'))
	 	modal.find('.modal-body #txtVehicleNumber').val(button.data('lvehiclenumber'))
	 	modal.find('.modal-body #txtVehicleNumberHidden').val(button.data('lvehiclenumber'))
	 	modal.find('.modal-body #txtBrand').val(button.data('lbrand'))
	 	modal.find('.modal-body #txtType').val(button.data('ltype'))
	 	modal.find('.modal-body #txtYear').val(button.data('lyear'))
	 	modal.find('.modal-body #txtTidNumber').val(button.data('ltidnumber'))
	 	modal.find('.modal-body #txtStnkNumber').val(button.data('lstnknumber'))
	 	modal.find('.modal-body #txtStnkExpired').val(button.data('lstnkexpired'))
	})
	
	function FuncButtonNew() {
		$('#txtVehicleNumber').val('');
		$('#txtVehicleNumberHidden').val('');
		$('#txtBrand').val('');
		$('#txtType').val('');
		$('#txtYear').val('');
		$('#txtTidNumber').val('');
		$('#txtStnkNumber').val('');
		$('#txtStnkExpired').val('');
		
		$('#txtVendorID').val(document.getElementById('temp_vendorid').value);
	
		$('#btnSave').show();
		$('#btnUpdate').hide();
		$('#txtVendorID').prop("disabled",false);
		$('#txtVehicleNumber').prop("disabled",false);
		document.getElementById("lblTitleModal").innerHTML = "Add Vehicle";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate(lParamVendorID) {
		$('#btnSave').hide();
		$('#btnUpdate').show();
		$('#txtVendorID').attr("disabled","disabled");
		$('#txtVehicleNumber').attr("disabled","disabled");
		document.getElementById("lblTitleModal").innerHTML = 'Edit Vehicle';
		$('#txtVendorID').val(lParamVendorID);
	
		FuncClear();
	}
	
	function FuncDelete() {
		var temp_txtVendorID = document.getElementById('temp_txtVendorID').value;
		var temp_txtVehicleNumber = document.getElementById('temp_txtVehicleNumber').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorVehicle',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtVendorID":temp_txtVendorID,"temp_txtVehicleNumber":temp_txtVehicleNumber},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/VendorVehicle';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtVendorID = document.getElementById('txtVendorID').value;
		var txtVehicleNumber = document.getElementById('txtVehicleNumber').value;
		var txtBrand = document.getElementById('txtBrand').value;
		var txtType = document.getElementById('txtType').value;
		var txtYear = document.getElementById('txtYear').value;
		var txtTidNumber = document.getElementById('txtTidNumber').value;
		var txtStnkNumber = document.getElementById('txtStnkNumber').value;
		var txtStnkExpired = document.getElementById('txtStnkExpired').value;
		
	    if(!txtVendorID.match(/\S/)) {    	
	    	$('#txtVendorID').focus();
	    	$('#dvVendorID').addClass('has-error');
	    	$('#mrkVendorID').show();
	        return false;
	    } 
	    if(!txtVehicleNumber.match(/\S/)) {    	
	    	$('#txtVehicleNumber').focus();
	    	$('#dvVehicleNumber').addClass('has-error');
	    	$('#mrkVehicleNumber').show();
	        return false;
	    }
	    if(!txtBrand.match(/\S/)) {    	
	    	$('#txtBrand').focus();
	    	$('#dvBrand').addClass('has-error');
	    	$('#mrkBrand').show();
	        return false;
	    }
	    if(!txtType.match(/\S/)) {    	
	    	$('#txtType').focus();
	    	$('#dvType').addClass('has-error');
	    	$('#mrkType').show();
	        return false;
	    }
	    if(!txtYear.match(/\S/)) {    	
	    	$('#txtYear').focus();
	    	$('#dvYear').addClass('has-error');
	    	$('#mrkYear').show();
	        return false;
	    }
	    if(!txtTidNumber.match(/\S/)) {    	
	    	$('#txtTidNumber').focus();
	    	$('#dvTidNumber').addClass('has-error');
	    	$('#mrkTidNumber').show();
	        return false;
	    }
	    if(!txtStnkNumber.match(/\S/)) {    	
	    	$('#txtStnkNumber').focus();
	    	$('#dvStnkNumber').addClass('has-error');
	    	$('#mrkStnkNumber').show();
	        return false;
	    }
	    if(!txtStnkExpired.match(/\S/)) {    	
	    	$('#txtStnkExpired').focus();
	    	$('#dvStnkExpired').addClass('has-error');
	    	$('#mrkStnkExpired').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorVehicle',	
	        type:'POST',
	        data:{"key":lParambtn,"txtVendorID":txtVendorID,"txtVehicleNumber":txtVehicleNumber,
	        	"txtBrand":txtBrand,"txtType":txtType,"txtYear":txtYear,"txtTidNumber":txtTidNumber,
	        	"txtStnkNumber":txtStnkNumber,"txtStnkExpired":txtStnkExpired},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertVendorVehicle')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtVehicleNumber").focus();
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateVendorVehicle')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtVehicleNumber").focus();
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/VendorVehicle';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
function FuncPassStringVendor(lParamVendorID,lParamVendorName){
		$("#txtVendorID").val(lParamVendorID);
	}
	
//on click event for txtVendorID
    $(document).on('click', '#txtVendorID', function(e){
    	e.preventDefault();
		var roleid = document.getElementById('temp_userrole').value;
		var vendorid = document.getElementById('txtVendorID').value;
		if(roleid=='R001'){
			 $('#ModalGetVendorID').modal('toggle');
		}
	 });
	 
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtStnkExpired:focus').length) {
    	$('#txtStnkExpired').click();
    }
});

//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var vehiclenumberdefault = document.getElementById('txtVehicleNumberHidden').value;
  event.preventDefault();
  if (event.keyCode === 13 && (vehiclenumberdefault == null || vehiclenumberdefault == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (vehiclenumberdefault != null || vehiclenumberdefault != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>