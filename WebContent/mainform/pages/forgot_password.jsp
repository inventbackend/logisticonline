<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Forgot Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style>
  	/* enable absolute positioning */
	.inner-addon { 
	    position: relative; 
	}
	
	/* style icon */
	.inner-addon .glyphicon   {
	  position: absolute;
	  padding: 10px;
	  margin-top:-10px;
	  /*pointer-events: none; */
	}
	
	/* align icon */
	.left-addon .glyphicon    { left:  0px;}
	.right-addon .glyphicon   { right: 0px;}
	
	/* add padding  */
	.left-addon input  { padding-left:  10px; }
	.right-addon input { padding-right: 30px; }
	.nopadding {
	   padding: 0 !important;
	   margin: 0 !important;
	}
  </style>
</head>

<body class="hold-transition login-page">

<form action="${pageContext.request.contextPath}/ForgotPassword" method="post">

<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body" style="margin-top:20%">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" alt="Mountain View" style="width:100%;height:100%;"><br>
	<!-- <b>Logistic Online</b> -->
    
  </div>
  
  	<c:if test="${condition == 'SUCCESS_SEND_MAIL'}">
		<div class="alert alert-success alert-dismissible">
		Success, please check email!
		</div>
	</c:if>
	
	<c:if test="${condition == 'ERROR'}">
		<div class="alert alert-danger alert-dismissible">
		${message}
		</div>
	</c:if>
  	
  	<p style="color:black;">Fill email for change password</p>
  	
	<div class="form-group has-feedback inner-addon">
		<div class="input-group input-group-sm">
           <input type="email" class="form-control" placeholder="email" id="email" name="email" value="${email}">
               <span class="input-group-btn">
                 <button type="submit" class="btn btn-info btn-flat" name="btnCheckEmail">Check!</button>
               </span>
         </div>
		<br>
		
		<c:if test="${condition == 'NODATA'}">
			<div class="col-md-12" style="background-color: white; padding:10px 0; margin-bottom: 10px;">
				<div class="col-md-12">
					No data!
				</div>
			</div>
		</c:if>
		
		<c:if test="${condition == 'SUCCESS'}">
			<div class="col-md-12" style="background-color: white; padding:10px 0; margin-bottom: 10px;">
				<c:forEach items="${listUser}" var ="data">
					<div class="form-check col-md-12">
					  <input class="minimal" type="radio" name="id" id="id" value="${data.userId}">
					  <label class="minimal" for="id">
					     ${data.name}
					  </label>
					</div>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<br>
	<br>
	

      	<div class="row">
	        <div class="col-xs-6">
	        </div>
	        <!-- /.col -->
	        <div class="col-xs-2">
	        </div>
	        <!-- /.col -->
	        <div class="col-xs-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat" name="btnForgetPassword">Send</button>
	        </div>
	        <!-- /.col -->
	      </div>

	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<script>
$(function () {
	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
	  checkboxClass: 'icheckbox_minimal-blue',
	  radioClass: 'iradio_minimal-blue'
	});
	
});



	
</script>

</body>
</html>
