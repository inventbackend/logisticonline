<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Credit Note</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  	<link rel="stylesheet" href="mainform/plugins/iCheck/all.css">
	
	<style type="text/css">	
		  #ModalUpdateInsert { overflow-y:scroll }
		
		 /*  css for loading bar */
		 .loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 50px;
		  height: 50px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		  margin-left: 45%;
		}
		
		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}
		
		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
		/* end of css loading bar */
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Form_Credit_Note" name="Form_Credit_Note" action = "${pageContext.request.contextPath}/CreditNote" method="post">
	<input type="hidden" id="temp_string" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Credit Note</h1>
			</section>
			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<c:if test="${condition == 'SuccessConfirm'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Sukses konfirmasi pembayaran.
			   				</div>
						</c:if>
						
						<c:if test="${condition == 'SuccessConfirmPartialPayment'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Sukses konfirmasi pembayaran parsial.
			   				</div>
						</c:if>
						
						<div id="dvSearch" class="row">	
		<div class="col-md-2">
		<label for="message-text" class="control-label">Status</label>
			<select id="slStatusSearch" name="slStatus" class="form-control">
				<c:if test="${status eq 'Unpaid'}">
					<option selected>Unpaid</option>
					<option>Paid</option>
					<option value=''>All</option>
				</c:if>
				<c:if test="${status eq 'Paid'}">
					<option>Unpaid</option>
					<option selected>Paid</option>
					<option value=''>All</option>
				</c:if>
				<c:if test="${status eq ''}">
					<option>Unpaid</option>
					<option>Paid</option>
					<option value='' selected>All</option>
				</c:if>
       		</select>
       	</div>
		 <!-- /.col-md-2 -->
		 
		 <div class="col-md-2">
			<label for="message-text" class="control-label">Invoice Date From</label>
			<input type="text" class="form-control" id="txtStuffingDateFrom" name="txtStuffingDateFrom" value="<c:out value="${stuffingDateFrom}"/>">
       	</div>
		<!-- /.col-md-2 -->
		<div class="col-md-2">
			<label for="message-text" class="control-label">Invoice Date To</label>
			<input type="text" class="form-control" id="txtStuffingDateTo" name="txtStuffingDateTo" value="<c:out value="${stuffingDateTo}"/>">
     	</div>
		<!-- /.col-md-2 -->
		
		<div class="col-md-2">
		<button id="btnSearch" name="btnSearch" type="button" class="btn btn-primary" onclick="FuncButtonSearch('search')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-2 -->
		</div>
		<!-- /.row -->
		
		
		<!--modal update & Insert partial and confirm-->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
							
			<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	       				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	       				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	       				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
							</div>
						
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Payment Confirmation</h4>	
			</div>
				<div class="modal-body">
					<div id="dvloader" class="loader" style="display:none;"></div>
	
								<div class="row">
									<div id="idBodyFormPaymentPaid" class="form-group col-md-12">
									</div>
								</div>
						</div>
						<div class="modal-footer">
							<button id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="savePaymentConfirmation()"><i class="fa fa-check"></i> Confirm</button>
								<button type="button" class="btn btn-danger" onclick="closeModal()"><i class="fa fa-remove"></i> Cancel</button>
						</div>
					</div>
				</div>
			</div>
       	
      					<!--modal update & Insert -->
						<!-- <div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel">Payment Confirmation</h4>	
									</div>
									
	   								<div class="modal-body">
	   									loading bar
										<div id="dvloader" class="loader" style="display:none;"></div>
				
	   									<div class="row">
	   										<div id="dvPaymentDate" class="form-group col-md-12">
	         									<label for="lblPaymentDate" class="control-label">Payment Date</label><label id="mrkPaymentDate" for="recipient-name" class="control-label"></label>	
	         									<input type="text" class="form-control" id="txtPaymentDate" name="txtPaymentDate">							                    
		       								</div>
	   									</div>
	  								</div>
	  								<div class="modal-footer">
	  									<button id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncSave('save')"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div> -->
						
						
						<!--modal partial payment -->
						<div class="modal fade" id="ModalPartialPayment" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlertPartialPayment" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlertPartialPayment"></label>. <label id="lblAlertPartialPaymentDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
									</div>
									
	   								<div class="modal-body">
	   									<!-- loading bar -->
										<div id="dvloaderPartialPayment" class="loader" style="display:none;"></div>
				
	   									<div class="row">
	   										<div id="dvPaymentDate2" class="form-group col-md-12">
	         									<label for="lblPaymentDate2" class="control-label">Payment Date</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>	
	         									<input type="text" class="form-control" id="txtPaymentDate2" name="txtPaymentDate2">							                    
		       								</div>
		       								<div id="dvAmountPaid" class="form-group col-md-12">
	         									<label for="lblAmountPaid" class="control-label">Amount Paid</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>	
	         									<input class="form-control number" id="txtAmountPaid" name="txtAmountPaid">							                    
		       								</div>
		       								<input type="hidden" class="form-control" id="txtCreditNoteNumber" name="txtCreditNoteNumber">
		       								<input type="hidden" class="form-control" id="txtAmount" name="txtAmount">
	   									</div>
	  								</div>
	  								<div class="modal-footer">
	  									<button id="btnSavePartialPayment" name="btnSavePartialPayment" value="btnSavePartialPayment" type="button" class="btn btn-primary" onclick="FuncSavePartialPayment('SavePartialPayment')"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div>
						<br><br>
						
						<c:if test="${globalUserRole eq 'R001'}">
							<button id="btnPayment" name="btnPayment" type="button" class="btn btn-success pull-left" data-toggle="modal"
								style="display:none;"
								data-target="#ModalUpdateInsert"
								>
								<span class="glyphicon glyphicon-check"></span>
								CONFIRM PAYMENT (<label id="lblTotalPayment"></label>)
							</button>
							<div id="newLine" style="display:none"><br><br><br></div>
						</c:if>
							
							<table id="tb_credit_note" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr style="background-color: #003156; color:white;
												font-family: Roboto; font-size: 16px; font-weight: bold;
												">
											<th colspan="2">Total Amount Unpaid</th>
											<th colspan="11" style="text-align: center;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
											</th>
									</tr>
									<tr>
										<th style="display: <c:out value="${buttonstatus}"/>"></th>
										<c:if test="${globalUserRole eq 'R001'}">
											<th>Trucker</th>
										</c:if>
										<th>Order</th>
										<th>Credit Note</th>
										<th>Issued Date</th>
										<th>Amount</th>
										<th>Amount Paid</th>
										<th>Amount Balance</th>
										<th>Payment Date</th>
										<!-- <th>Status</th> -->
										<th>Status Credit Note</th>
										<th style="display: <c:out value="${buttonstatus}"/>"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listCreditNote}" var="cn" varStatus="loop">
										<tr>
											<td style="display: <c:out value="${buttonstatus}"/>">
												<c:if test="${cn.status ne 'Paid'}">
													<div class="checkbox" id="cbCreditNote">
													<label><input title="confirm full payment" 
														type="checkbox"
														class="icheckbox_minimal-red"
														id="credit-note-<c:out value="${loop.index}" />"
														value="<c:out value="${cn.creditNoteNumber}" />&&<c:out value="${cn.amount}" />"
														>
													</label>
													</div>
												</c:if>
											</td>
											<c:if test="${globalUserRole eq 'R001'}">
												<td><c:out value="${cn.vendorName}" /></td>
											</c:if>
											<td><c:out value="${cn.orderManagementID}" /></td>
											<td>
												<a href="CreditNotePrint?cnNumber=<c:out value="${cn.creditNoteNumber}" />" style="color: #144471; font-weight: bold;" title="print"> 
													<c:out value="${cn.creditNoteNumber}" />
												</a>
											</td>
											<td><c:out value="${cn.date}" /></td>
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${cn.amount}" type="currency"/>
											</td>
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${cn.amountPaid}" type="currency"/>
											</td>
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${cn.amount - cn.amountPaid}" type="currency"/>
											</td>
											<td><c:out value="${cn.paymentDate}" /></td>
											<td style="font-size: 18px;">
												<c:if test="${cn.status eq 'Paid'}">
													<span class="label label-primary">
														<c:out value="${cn.status}" />
													</span>
												</c:if>
												<c:if test="${cn.status ne 'Paid'}">
													<span class="label label-danger">
														<c:out value="${cn.status}" />
													</span>
												</c:if>
											</td>
											<%-- <td>
												<button class="btn btn-warning" type="button" id="btnInvoiceSetting" name="btnInvoiceSetting"
													data-target="#ModalInvoiceSetting"
														data-toggle="modal"
														data-lpph23 = '<c:out value="${cn.pph23}" />'
														>
														Performance <i class="fa fa-cog" aria-hidden="true"></i>
													</button>
											</td> --%>
											<td style="display: <c:out value="${buttonstatus}"/>">
												<c:if test="${cn.status ne 'Paid'}">
													<button id="btnPartial" name="btnPartial" type="button" class="btn bg-olive" data-toggle="modal"
														title="Partial Pay"
														data-target="#ModalPartialPayment"
														data-lcnn = '<c:out value="${cn.creditNoteNumber}" />'
														data-lamount = '<c:out value="${cn.amount}" />'
														>
														Pay partially
													</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
								<tfoot style="background-color: #003156; color:white;
											font-family: Roboto; font-size: 16px; font-weight: bold;
											">
						            <tr>
						            	<th colspan="2">Total Amount Unpaid</th>
						            	<c:choose>
    										<c:when test="${globalUserRole eq 'R001'}">
    											<th colspan="9" style="text-align: center;">
													<fmt:setLocale value="id_ID"/>
													<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
												</th>
    										</c:when>    
										    <c:otherwise>
										        <th colspan="7" style="text-align: center;">
													<fmt:setLocale value="id_ID"/>
													<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
												</th>
										    </c:otherwise>
										</c:choose>
									</tr>
						        </tfoot>
							</table>
							
							<br>
							
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>	

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	
	function formatNumberCustom() {
	   	$('input.number').keyup(function(event) {
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;
		
		  // format number
		  $(this).val(function(index, value) {
		    return value
		    .replace(/\D/g, "")
		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		    ;
		  });
		});
    }
	
 	$(function () {
 		$(".select2").select2();
 		
  		$("#tb_credit_note").DataTable({"aaSorting": [],"scrollX": true});
  		$('#M024').addClass('active');
  		
  		//calendar java script
		$('#txtStuffingDateFrom').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtStuffingDateTo').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtPaymentDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    $('#txtPaymentDate2').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    
	    //custom number input type
		$('input.number').keyup(function(event) {
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;
		
		  // format number
		  $(this).val(function(index, value) {
		    return value
		    .replace(/\D/g, "")
		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		    ;
		  });
		});
  	    
  		$("#dvErrorAlert").hide();
  		$("#dvErrorAlertPartialPayment").hide();
  	});

	</script>

<script>
	function FuncClear() {
		$('#mrkPaymentDate').hide();
		$('#mrkPaymentDate2').hide();
		$('#mrkAmountPaid').hide();
		
		$('#dvPaymentDate').removeClass('has-error');
		$('#dvPaymentDate2').removeClass('has-error');
		$('#dvAmountPaid').removeClass('has-error');
	}
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		FuncClear();
		
		$("#dvErrorAlert").hide();
 		var modal = $(this);
 		modal.find(".modal-body #txtPaymentDate").val('');
	});
	
	
	$('#ModalPartialPayment').on('shown.bs.modal', function (event) {
		FuncClear();
		
		$("#dvErrorAlertPartialPayment").hide();
 		var modal = $(this);
 		var button = $(event.relatedTarget);
 		var lCreditNoteNumber = button.data('lcnn');
 		var lAmount = button.data('lamount');
 		
 		document.getElementById("lblTitleModal").innerHTML = 'Partial Payment For Credit Note No. '+ lCreditNoteNumber + '<br>' + '( Total Amount = Rp '+ Intl.NumberFormat(['ban', 'id']).format(lAmount) +',00 )';
 		modal.find(".modal-body #txtPaymentDate2").val('');
 		modal.find(".modal-body #txtAmountPaid").val('');
 		modal.find(".modal-body #txtCreditNoteNumber").val(lCreditNoteNumber);
 		modal.find(".modal-body #txtAmount").val(lAmount);
	});
	

function FuncSave(lParambtn) {
	var checkedCreditNote = new Array();
		$('#cbCreditNote input:checked').each(function() {
			checkedCreditNote.push(this.value);
		});
 	var paymentDate = document.getElementById('txtPaymentDate').value;
 	
 	if(!paymentDate.match(/\S/)) {    	
	    	$('#txtPaymentDate').focus();
	    	$('#dvPaymentDate').addClass('has-error');
	    	$('#mrkPaymentDate').show();
	        return false;
	    } 
 	
 	$('#dvloader').show();
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/CreditNote',	
	        type:'POST',
	        data:{"key":lParambtn,"paymentDate":paymentDate,"creditNoteNumber":checkedCreditNote},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirm')
	        	{
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/CreditNote';  
 	        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloader').hide();
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

function FuncSavePartialPayment(lParambtn) {
	var amountPaid = document.getElementById('txtAmountPaid').value.replace(/\./g,'');
	var amount = document.getElementById('txtAmount').value;
 	var paymentDate = document.getElementById('txtPaymentDate2').value;
 	var creditNoteNumber = document.getElementById('txtCreditNoteNumber').value;
 	
 	if(!paymentDate.match(/\S/)) {    	
	    	$('#txtPaymentDate2').focus();
	    	$('#dvPaymentDate2').addClass('has-error');
	    	$('#mrkPaymentDate2').show();
	        return false;
	    }
	if(!amountPaid.match(/\S/)) {    	
	    	$('#txtAmountPaid').focus();
	    	$('#dvAmountPaid').addClass('has-error');
	    	$('#mrkAmountPaid').show();
	    	document.getElementById("mrkAmountPaid").innerHTML = '*';
	        return false;
	    }
	if(Number(amountPaid) >= Number(amount)){
			$('#txtAmountPaid').focus();
	    	$('#dvAmountPaid').addClass('has-error');
	    	$('#mrkAmountPaid').show();
	    	document.getElementById("mrkAmountPaid").innerHTML = '( Amount paid may not equal or bigger than ' + 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(amount) +',00 )';
	    	return false;
	}  
 	
 	$('#dvloaderPartialPayment').show();
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/CreditNote',	
	        type:'POST',
	        data:{"key":lParambtn,"paymentDate":paymentDate,"amountPaid":amountPaid,"creditNoteNumber":creditNoteNumber},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirmPartialPayment')
	        	{
	        		$('#dvloaderPartialPayment').hide();
	        		$("#dvErrorAlertPartialPayment").show();
	        		document.getElementById("lblAlertPartialPayment").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertPartialPaymentDescription").innerHTML = data.split("--")[1];
	        		$("#ModalPartialPayment").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/CreditNote';  
 	        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloaderPartialPayment').hide();
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

function FuncButtonSearch(lParambtn) {
 	var Status = document.getElementById('slStatusSearch').value;
 	var StuffingDateFrom = document.getElementById('txtStuffingDateFrom').value;
	var StuffingDateTo = document.getElementById('txtStuffingDateTo').value;
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/CreditNote',	
	        type:'POST',
	        data:{"key":lParambtn,"statusSearch":Status,"stuffingdatefrom":StuffingDateFrom, "stuffingdateto":StuffingDateTo},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	var url = '${pageContext.request.contextPath}/CreditNote';  
 	        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

	//click payment checkbox action
	/* $(document).ready(function(){
	
	    $(document).on('click', '#cbCreditNote', function(e){
    	
	    	var checkedCreditNote = new Array();
	    	var total = 0;
			var i;

	    	$('#cbCreditNote input:checked').each(function() {
	    		checkedCreditNote.push(this.value);
	    	});
	    	if(checkedCreditNote.length == 0){
	    		$("#btnPayment").hide();
	    		$("#newLine").hide();
	    	}
	    	else{
	    		$("#btnPayment").show();
	    		$("#newLine").show();
	    		for (i = 0; i < checkedCreditNote.length; i++) {
				    total += Number(checkedCreditNote[i].split("&&")[1]);
				}
				
				document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
	    	}
	    	
	    });
	}); */
	
	var bodyModal = "";
	var checkedDebitNote = [];
	var debitNote = [];

	function closeModal() {
		checkedDebitNote = [];
		bodyModal = "";
		console.log("close")
		$('#ModalUpdateInsert').modal('hide')
	}

	function savePaymentConfirmation(){
		var num = 1;
		var arryPayment = [];
		var arrFullPayment = [];
		var arrPartialPayment = [];
		
		for ( var item in debitNote) {
			var textAmount = $('#row'+num).find("#txtAmount").val().replace(/\./g,'');
			var txtAmountPaid = $('#row'+num).find("#txtAmountPaid").val().replace(/\./g,'');
			var calPaymentDate = $('#row'+num).find("#calPaymentDate"+num).val();
			var selPayment = $( "#selPaymentMethod"+num ).val();
			var txtInvoiceNumber = $('#row'+num).find("#txtInvoiceNumber").val();
			var checkedCreditNote = ""+txtInvoiceNumber+"&&"+textAmount;
			
			console.log(txtAmountPaid)
			
			arryPayment.push({
				"amount":textAmount,
				"amountPaid": txtAmountPaid,
				"paymentDate": calPaymentDate,
				"paymentMethod": selPayment,
				"creditNoteNumber": txtInvoiceNumber,
				"checkedCredittNote": checkedCreditNote
				
			});
			
			
			num++;
			
		}
		
		arrFullPayment = arryPayment.filter(function( obj ) {
		    return obj.paymentMethod === "Full Payment";
		});
		
		arrPartialPayment = arryPayment.filter(function( obj ) {
		    return obj.paymentMethod === "Partial Payment";
		});
		
		console.log(arrFullPayment)
		
		jQuery.ajax({
	        url:'${pageContext.request.contextPath}/CreditNote',	
	        type:'POST',
	        data:{"key":"saveConfirmPayment", "fullPayment":JSON.stringify(arrFullPayment), "partialPayment":JSON.stringify(arrPartialPayment)},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirm')
	        	{
	        		console.log("Confirmation failed")
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/CreditNote';  
		        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloader').hide();
	            console.log('Service call failed!');
	        }
	    });
		
		/* for ( var confirmPayment in arrFullPayment) {
			arrFullPayment[confirmPayment].debitNoteNumber
			
			jQuery.ajax({
		        url:'${pageContext.request.contextPath}/DebitNote',	
		        type:'POST',
		        data:{"key":lParambtn,"paymentDate":paymentDate,"debitNoteNumber":checkedDebitNote},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedConfirm')
		        	{
		        		console.log("Confirmation failed")
		        		$('#dvloader').hide();
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
			        	var url = '${pageContext.request.contextPath}/DebitNote';  
	 	        		$(location).attr('href', url);
		        	}
		        },
		        error:function(data, textStatus, jqXHR){
		        	$('#dvloader').hide();
		            console.log('Service call failed!');
		        }
		    });
		} */
		
		console.log(arryPayment)
		
	}

	function getUnique(arr, comp) {

	  const unique = arr
	       .map(e => e[comp])

	     // store the keys of the unique objects
	    .map((e, i, final) => final.indexOf(e) === i && i)

	    // eliminate the dead keys & store unique objects
	    .filter(e => arr[e]).map(e => arr[e]);

	   return unique;
	}


	//click payment checkbox action
	$(document).ready(function(){
		
		var table = $('#tb_credit_note').DataTable();
		var MyRows = $('#tb_credit_note').find('tbody').find('tr');
		
		$('#tb_credit_note tbody').on( 'click', 'tr', '#cbDebitNote', function () {
			var idxRow = table.row( this ).index()+3;
			var amountVal = $('#tb_credit_note').find('tr:eq('+idxRow+') td:eq(5)').text();
			var amountPaidVal = $('#tb_credit_note').find('tr:eq('+idxRow+') td:eq(6)').text();
			var amountPaidBalanceVal = $('#tb_credit_note').find('tr:eq('+idxRow+') td:eq(7)').text();
			var iNumber = $('#tb_credit_note').find('tr:eq('+idxRow+') td:eq(3)').text();
			console.log(idxRow);
			console.log(iNumber);
			
			var cp = parseInt(amountVal.replace(/\.|,00|Rp| /g, "")) - parseInt(amountPaidVal.replace(/\.|,00|Rp| /g, ""));
			var getIdChekbox = $('#tb_credit_note').find('tr:eq('+idxRow+') td:eq(0) label input').attr('id');
			console.log(getIdChekbox)
			if ($('input#'+getIdChekbox).is(':checked')) {
				debitNote.push({
	    			"index":idxRow,
	    			"invoiceNumber": iNumber.replace(/\s/g,''),
	    			"amount":parseInt(amountVal.replace(/\.|,00|Rp| /g, "")),
	    			"amountPaid":parseInt(amountPaidVal.replace(/\.|,00|Rp| /g, "")),
	    			"amountPaidBalance":parseInt(amountPaidBalanceVal.replace(/\.|,00|Rp| /g, "")),
	    			
	    		});
				
				console.log(getUnique(debitNote,'index'))
			}else {
				var arrDN = debitNote.filter(function( obj ) {
				    return obj.index !== idxRow;
				});
				
				debitNote = arrDN;
			}
			
			var total = 0;
	    	
	    	if(debitNote.length == 0){
	    		$("#btnPayment").hide();
	    		$("#newLine").hide();
	    	}
	    	else{
	    		$("#btnPayment").show();
	    		$("#newLine").show();
	    		
	    		var total = 0;
	    		for (const item of debitNote) {
	    			total += item.amountPaidBalance;
				}
	    		
				document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
	    	}
			
			
		});
		
		$('#btnPayment').on('click', function () {
			var num = 1;
		
			for (const item of debitNote) {
				bodyModal += 	'<div class="col-md-12" style="padding-bottom: 15px;" id="row'+num+'"><div class="col-md-2">'+
							      '<label for="lblPaymentDate2" class="control-label">Invoice Number</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>'+
							      '<input type="text" class="form-control" id="txtInvoiceNumber" name="txtInvoiceNumber" value="'+item.invoiceNumber+'" disabled="disabled">'+
							    '</div>'+
							    '<div class="col-md-2">'+
							      '<label>Select Payment</label>'+
							          '<select class="form-control" id="selPaymentMethod'+num+'">'+
							          	'<option value="" selected disabled>Please select</option>'+
							            '<option value"Full Payment">Full Payment</option>'+
							            '<option value"Partial Payment">Partial Payment</option>'+
							          '</select>'+
							    '</div>'+
							    '<div class="col-md-2">'+
							      '<label for="lblPaymentDate2" class="control-label">Payment Date</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>'+
							      '<input type="text" class="form-control" id="calPaymentDate'+num+'" name="txtPaymentDate2">'+
							    '</div>'+
							    '<div class="col-md-3">'+
							      '<label for="lblAmountPaid" class="control-label">Amount Balance</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>'+
							      '<input class="form-control number" id="txtAmount" name="txtAmount" value="'+item.amountPaidBalance+'" disabled="disabled">'+
							    '</div>'+
							    '<div class="col-md-3">'+
							      '<label for="lblAmountPaid" class="control-label">Amount Paid</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>'+
							      '<input class="form-control number" id="txtAmountPaid" name="txtAmountPaid">'+
							    '</div></div>';
							    
								
				num++;
				
			}
			
			
			
			$( "#idBodyFormPaymentPaid" ).html(bodyModal);
			
			formatNumberCustom();
			for (var i = 0; i < num; i++) {
				console.log(i)
				$('#calPaymentDate'+i).datepicker({
				      format: 'dd M yyyy',
				      autoclose: true
				});
				
				$( "#selPaymentMethod"+i ).change(function() {
					var getID = $(this).parent().parent().attr('id');
					var txtAmount = $("#"+getID).find("#txtAmount").val();
					
					var selPayment = this.value;
					if(selPayment === "Full Payment") {
						$("#"+getID).find("#txtAmountPaid").val(txtAmount);
						$("#"+getID).find("#txtAmountPaid").prop('disabled', true);
					}else {
						$("#"+getID).find("#txtAmountPaid").val(0);
						$("#"+getID).find("#txtAmountPaid").prop('disabled', false);
					}
				});
			}
			
			
			/* $('#ModalUpdateInsert').modal('show') */
		})
		
		

	    /* $(document).on('change', '#cbDebitNote', function(e){
	    	
	    	var total = 0;
			var i;
			var cp = 0;
			
	    	$('#cbDebitNote input:checked').each(function(i) {
	    		checkedDebitNote.push(this.value);
	    		var idxRow =  i;
	    	});
	    	
	    	if(checkedDebitNote.length == 0){
	    		$("#btnPayment").hide();
	    		$("#newLine").hide();
	    	}
	    	else{
	    		$("#btnPayment").show();
	    		$("#newLine").show();
	    		for (i = 0; i < checkedDebitNote.length; i++) {
				    total += (Number(checkedDebitNote[i].split("&&")[1])- Number(checkedDebitNote[i].split("&&")[2]));
				    console.log(checkedDebitNote[i].split("&&")[2])
				}
				
				document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
	    	}
	    	
	    }); */
	});
</script>

</body>

</html>