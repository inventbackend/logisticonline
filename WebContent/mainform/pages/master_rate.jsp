<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Rate</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
 <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Rate" action = "${pageContext.request.contextPath}/Rate" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Rate
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertRate'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan rate.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateRate'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui rate.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteRate'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus rate.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteRate'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus rate. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
		          <div id="dvRateID" class="form-group">
		            <label for="lblRateID" class="control-label">Rate ID:</label><label id="mrkRateID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtRateID" name="txtRateID" readonly disabled>
		          </div>
				<!-- 		          <div id="dvName" class="form-group"> -->
				<!-- 		            <label for="lblName" class="control-label">Rate Name:</label><label id="mrkName" for="lbl-validation" class="control-label"><small>*</small></label> -->
				<!-- 		            <input type="text" class="form-control" id="txtName" name="txtName"> -->
				<!-- 		          </div> -->
		          <div id="dvDistrictID" class="form-group">
		            <label for="lblDistrictID" class="control-label">District:</label><label id="mrkDistrictID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="slDistrictID" name="slDistrictID" class="form-control select2" data-placeholder="Select District" style="width: 100%;">
						<c:forEach items="${listDistrict}" var="district">
							<option value="<c:out value="${district.districtID}" />"><c:out value="${district.name}" /></option>
						</c:forEach>
		        	</select>
		          </div>
		          <div id="dvTierID" class="form-group">
		            <label for="lblTierID" class="control-label">Tier:</label><label id="mrkTierID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="slTierID" name="slTierID" class="form-control select2" data-placeholder="Select Tier" style="width: 100%;">
						<c:forEach items="${listTier}" var="tier">
							<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.description}" /></option>
						</c:forEach>
		        	</select>
		          </div>
		          <div id="dvContainerTypeID" class="form-group">
		            <label for="lblContainerTypeID" class="control-label">Container Type:</label><label id="mrkContainerTypeID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="slContainerTypeID" name="slContainerTypeID" class="form-control select2" data-placeholder="Select Container Type" style="width: 100%;">
						<c:forEach items="${listContainerType}" var="containerType">
							<option value="<c:out value="${containerType.containerTypeID}" />"><c:out value="${containerType.description}" /></option>
						</c:forEach>
		        	</select>
		        	<input id="idtest" style="display:none">
		          </div>
		          <div id="dvPrice" class="form-group">
		            <label for="lblPrice" class="control-label">Rate :</label><label id="mrkPrice" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input class="form-control number" id="txtPrice" name="txtPrice">
		          </div>
		          <div id="dvRole" class="form-group">
		            <label for="lblRole" class="control-label">Role:</label><label id="mrkRole" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="slRole" name="slRole" class="form-control select2" data-placeholder="Select Role" style="width: 100%;">
						<option value="R002">Customer</option>
						<option value="R003">Trucker</option>
		        	</select>
		          </div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Rate</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtRateID" name="temp_txtRateID"  />
									<p>Are you sure to delete this rate ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_rate" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	               	  <th>Role</th>
	                  <th>ID</th>
					<!-- <th>Name</th> -->
	                  <th>District</th>
	                  <th>Tier</th>
	                  <th>Container Type</th>
	                  <th>Rate</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
		        <c:forEach items="${listRate}" var ="rate">
		        	<tr>
		        	<td><c:out value="${rate.roleName}"/></td>
		        	<td><c:out value="${rate.rateID}"/></td>
					<%-- <td><c:out value="${rate.name}"/></td> --%>
					<td><c:out value="${rate.districtName}"/></td>
					<td><c:out value="${rate.tierName}"/></td>
					<td><c:out value="${rate.containerTypeName}"/></td>
					<td>						
						<fmt:setLocale value="id_ID"/>
						<fmt:formatNumber value="${rate.price}" type="currency"/>
					</td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lrateid='<c:out value="${rate.rateID}"/>'
								data-lname='<c:out value="${rate.name}"/>'
								data-ldistrictid='<c:out value="${rate.districtID}"/>'
								data-ltierid='<c:out value="${rate.tierID}"/>'
								data-lcontainertypeid='<c:out value="${rate.containerTypeID}"/>'
								data-lprice='<c:out value="${rate.price}"/>'
								data-lroleid='<c:out value="${rate.roleID}"/>'
								data-lrolename='<c:out value="${rate.roleName}"/>'
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-lrateid='<c:out value="${rate.rateID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
  	//Initialize Select2 Elements
	$(".select2").select2();
	
    $('#tb_master_rate').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
	$('#M004').addClass('active');
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lRateID = button.data('lrateid');
		$("#temp_txtRateID").val(lRateID);
	})
	</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var lrateid = button.data('lrateid');
	  var lname = button.data('lname');
	  var ldistrictid = button.data('ldistrictid');
	  var ltierid = button.data('ltierid');
	  var lcontainertypeid = button.data('lcontainertypeid');
	  var lprice = button.data('lprice');
	  var lroleid = button.data('lroleid');
	  var lrolename = button.data('lrolename');

	  var modal = $(this)
	  modal.find('.modal-body #txtRateID').val(lrateid);
	  modal.find('.modal-body #txtName').val(lname);
	  modal.find('.modal-body #slDistrictID').val(ldistrictid).trigger('change.select2');
	  modal.find('.modal-body #slTierID').val(ltierid).trigger('change.select2');
	  modal.find('.modal-body #slContainerTypeID').val(lcontainertypeid).trigger('change.select2');
	  modal.find('.modal-body #txtPrice').val(lprice);
	  modal.find('.modal-body #txtPrice').val(lprice);
	  modal.find('.modal-body #txtPrice').val(lprice);
	  modal.find('.modal-body #slRole').val(lroleid).trigger('change.select2');	  
	  
	  $("#txtName").focus();
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>
	//custom number input type
	$('input.number').keyup(function(event) {
 	   // skip for arrow keys
 	   if(event.which >= 37 && event.which <= 40) return;
 	   
 	   // format number
 	   $(this).val(function(index, value) {
 	     return value
 	     .replace(/\D/g, "")
 	     .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
 	     ;
 	   });
 	 });

	function FuncClear(){
		$('#mrkRateID').hide();
		$('#mrkRole').hide();
// 		$('#mrkName').hide();
		$('#mrkDistrictID').hide();
 		$('#mrkTierID').hide();
 		$('#mrkContainerTypeID').hide();
 		$('#mrkPrice').hide();
		
		$('#dvRateID').removeClass('has-error');
		$('#dvRole').removeClass('has-error');
// 		$('#dvName').removeClass('has-error');
		$('#dvDistrictID').removeClass('has-error');
		$('#dvTierID').removeClass('has-error');
		$('#dvContainerTypeID').removeClass('has-error');
 		$('#dvPrice').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#dvRateID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		$("slRole").select2("readonly", true);
		document.getElementById("lblTitleModal").innerHTML = "Add Rate";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		$('#dvRateID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		$("slRole").select2("readonly", false);
		document.getElementById("lblTitleModal").innerHTML = 'Edit Rate';
	
		FuncClear();
	}
	
	//function delete rate
	function FuncDelete() {
		var temp_txtRateID = document.getElementById('temp_txtRateID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Rate',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtRateID":temp_txtRateID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/Rate';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtRateID = jQuery('#txtRateID').val();
		var txtRole = jQuery('#slRole').val();
		var txtName = jQuery('#slDistrictID').val();
		var txtDistrictID = jQuery('#slDistrictID').val();
		var txtTierID = jQuery('#slTierID').val();
		var txtContainerTypeID = jQuery('#slContainerTypeID').val();
		var txtPrice = jQuery('#txtPrice').val();
		
		if(!txtRole.match(/\S/)) {    	
	    	$('#slRole').focus();
	    	$('#dvRole').addClass('has-error');
	    	$('#mrkRole').show();
	        return false;
	    }
	    if(!txtName.match(/\S/)) {    	
	    	$('#txtName').focus();
	    	$('#dvName').addClass('has-error');
	    	$('#mrkName').show();
	        return false;
	    }
	    if(!txtPrice.match(/\S/)) {    	
	    	$('#txtPrice').focus();
	    	$('#dvPrice').addClass('has-error');
	    	$('#mrkPrice').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Rate',	
	        type:'POST',
	        data:{"key":lParambtn,"txtRateID":txtRateID,"txtName":txtName,"txtDistrictID":txtDistrictID,
	        		"txtTierID":txtTierID, "txtContainerTypeID":txtContainerTypeID, "txtPrice":txtPrice,
	        		"txtRole":txtRole},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertRate')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan rate";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateRate')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui rate";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Rate';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var rateid = document.getElementById('txtRateID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (rateid == null || rateid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (rateid != null || rateid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>