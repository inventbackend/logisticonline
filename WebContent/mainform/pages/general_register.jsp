<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style>
  	.label-title {
	  	font-family: Helvetica;
		font-size: 18px;
		font-weight: bold; 
		color: black;
	}
	
	.label-content {
	  	font-family: Helvetica;
		font-size: 14px;
		font-weight: normal; 
		color: black;
	}
	
	input[type="text"],
  	input[type="password"],
	select,select2.form-control {
	  background: transparent;
	  border: none;
	  border-bottom: 1px solid #005292;
	  -webkit-box-shadow: none;
	  box-shadow: none;
	  border-radius: 0;
	  color: black;
	  margin-top:-10px;
	  font-family: Helvetica;	  
	}
	
	input[type="text"]:focus,
	input[type="password"]:focus,
	select,select2.form-control:focus {
	  -webkit-box-shadow: none;
	  box-shadow: none;
	}
	
	/* enable absolute positioning */
	.inner-addon { 
	    position: relative; 
	}
	
	/* style icon */
	.inner-addon .glyphicon   {
	  position: absolute;
	  padding: 10px;
	  margin-top:8px;
	  /*pointer-events: none; */
	}
	
	/* align icon */
	.left-addon .glyphicon    { left:  0px;}
	.right-addon .glyphicon   { right: 0px;}
	
	/* add padding  */
	.left-addon input  { padding-left:  30px; }
	.right-addon input { padding-right: 30px; }
	
	.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    	background-color: #005292;
    	font-family: Roboto;
    	font-size: 14px;
    	font-weight:lighter;
    	width: 50%;
		margin-left: 25%;
		margin-right: 25%
	}
	
	a[href] {
		font-family: Roboto;
		font-size: 13px;
		font-weight: lighter; 
		color: #037E91;
		text-align:center;
		display:block;
		margin-top: 5px;
	}
  </style>
  
</head>
<body class="hold-transition login-page">
<form action="${pageContext.request.contextPath}/GeneralRegister" method="post" onsubmit="return checkPassword(this);">

<div class="login-box">
  
  <div class="login-box-body" style="margin-top:-15%;">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" style="width:225px;height:123.71px;"><br>
	<!-- <b>Logistic Online</b> -->
  </div>
			
	<label for="lblRegisterTitle" class="label-title">Daftar Akun</label>
	<!-- <p class="login-box-msg"><b>FORM REGISTER</b></p> -->
    <div class="row" style="margin-top:10px;">
      <div class="form-group has-feedback col-xs-12 inner-addon left-addon">
      	<label for="lblName" class="label-content">Nama Lengkap</label>
      	<i class="glyphicon"><img style="width:22.5px; height:28.05px;" src="mainform/image/icon_register/Group 4301@2x.png"></i>
        <input type="text" class="form-control" id="txtPIC1" name="txtPIC1" required>
      </div>
       <div class="form-group has-feedback col-xs-12 inner-addon left-addon">
       	<label for="lblEmail" class="label-content">Email</label>
       	<i class="glyphicon" style="margin-top:12px;"><img style="width:22.5px; height:17.25px;" src="mainform/image/icon_register/Path 5616@2x.png"></i>
        <input type="text" class="form-control" id="txtEmail" name="txtEmail" required>
      </div>
      <div class="form-group has-feedback col-xs-12 inner-addon left-addon">
      	<label for="lblOfficePhone" class="label-content">Telepon</label>
      	<i class="glyphicon"><img style="width:22.5px; height:24.87px;" src="mainform/image/icon_register/Group 4305@2x.png"></i>
        <input type="text" class="form-control" id="txtOfficePhone" name="txtOfficePhone" required>
      </div>
      <div class="form-group has-feedback col-xs-12 inner-addon">
      	<label for="lblPassword" class="label-content">Kata Sandi</label>
      	<div class="left-addon">      		
      		<i class="glyphicon"><img style="width:22.5px; height:27.88px;" src="mainform/image/icon_register/Group 4303@2x.png"></i>
      		<input type="password" class="form-control left-addon" id="txtPassword" name="txtPassword" required>
        </div>
        <div class="right-addon">
        	<i class="glyphicon"><img id="iconEyeSlash" style="width:32.5px; height:32.22px;" src="mainform/image/icon_register/icons8-hide-400.png"></i>
        	<i class="glyphicon"><img id="iconEye" style="width:32.5px; height:19.22px;" src="mainform/image/icon_register/Group 4306@2x.png"></i>
        </div>
      </div>
      <div class="form-group has-feedback col-xs-12 inner-addon">
      	<label for="lblRePassword" class="label-content">Ketik Ulang Sandi</label>
      	<div class="left-addon">
      		<i class="glyphicon"><img style="width:22.5px; height:27.88px;" src="mainform/image/icon_register/Group 4303@2x.png"></i>
        	<input type="password" class="form-control left-addon" id="txtRePassword" name="txtRePassword" required>
        </div>
        <div class="right-addon">
        	<i class="glyphicon"><img id="iconEyeSlash2" style="width:32.5px; height:32.22px;" src="mainform/image/icon_register/icons8-hide-400.png"></i>
        	<i class="glyphicon"><img id="iconEye2" style="width:32.5px; height:19.22px;" src="mainform/image/icon_register/Group 4306@2x.png"></i>
        </div>
      </div>
      <div class="form-group has-feedback col-xs-6">
      	<label for="lblDaftarSebagai" class="label-content">Daftar Sebagai:</label>
      </div>
	  <div class="form-group has-feedback col-xs-6">
       <select id="slRole" name="slRole" class="form-control select2" style="width: 100%;" required>
			<option value="customer">CUSTOMER</option>
			<option value="vendor">VENDOR</option>
       	</select>
      </div>
    </div>
      
      <div class="row">
      	<div class="col-xs-12 g-recaptcha"
			data-sitekey="6LfG444UAAAAAJzCCoXi2-M_wleaf6FklGdg3fcn" style="margin-left: 2%;">
		</div>
        <div class="col-xs-12" style="margin-top:10px">
        	<button type="submit" class="btn btn-primary btn-block btn-flat" name="btnRegister">Lanjutkan</button>
        	<a href="Login">Sudah Punya Akun?</a>
        </div>
        <!-- /.col -->
      </div>
      
	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    //Initialize Select2 Elements
	$(".select2").select2();
	
	$(":input").inputmask();
  	$("#txtOfficePhone").inputmask({"mask": "999-99999999"});
  	
  	$("#iconEye").hide();
  	$("#iconEye2").hide();
  });
  
  //custom txtpassword
	$(document).ready(function(){
		$(document).on('click', '#iconEyeSlash', function(e){
			 $("#txtPassword").prop('type', 'text');
			 $("#iconEye").show();
			 $("#iconEyeSlash").hide();
		});
		$(document).on('click', '#iconEye', function(e){
			 $("#txtPassword").prop('type', 'password');
			 $("#iconEye").hide();
			 $("#iconEyeSlash").show();
		});
		
		$(document).on('click', '#iconEyeSlash2', function(e){
			 $("#txtRePassword").prop('type', 'text');
			 $("#iconEye2").show();
			 $("#iconEyeSlash2").hide();
		});
		$(document).on('click', '#iconEye2', function(e){
			 $("#txtRePassword").prop('type', 'password');
			 $("#iconEye2").hide();
			 $("#iconEyeSlash2").show();
		});
	});

	function myfunction() {
		$("#txtPassword").prop('type', 'text');
	}
	
	//check password and repassword is match or not before submit
	function checkPassword(theForm) {
		if (theForm.txtPassword.value != theForm.txtRePassword.value)
		{
			alert('Those passwords don\'t match!');
			return false;
		} else {
			return true;
		}
	}
</script>

</body>
</html>
