<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Role</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_User_Role" action = "${pageContext.request.contextPath}/Role" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Role
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertUserRole'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan hak akses.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateUserRole'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui hak akses.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteUserRole'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus hak akses.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteUserRole'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus hak akses. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
		<%-- <button <c:out value="${buttonstatus}"/> type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button> --%>
		<%--  <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert"><i class="fa fa-plus-circle"></i> New</button> --%>
        <br><br>
        <!-- modal update insert Role -->
								<div class="modal fade bs-example-modal-lg"
									id="ModalUpdateInsert" tabindex="-1" role="dialog"
									aria-labelledby="myLargeModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">

											<div id="dvErrorAlert"
												class="alert alert-danger alert-dismissible">
												<button type="button" class="close" data-dismiss="alert"
													aria-hidden="true">&times;</button>
												<h4>
													<i class="icon fa fa-ban"></i> Failed
												</h4>
												<label id="lblAlert"></label>. <label
													id="lblAlertDescription"></label>.
											</div>

											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="exampleModalLabel">
													<label id="lblTitleModalUpdateInsert"
														name="lblTitleModalUpdateInsert"></label>
												</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<label for="inputEmail3" class="col-sm-2 control-label">Role
															Id :</label>
															
													<div class="col-sm-4">
														<input class="form-control" id="txtRoleId">
													</div>

													<label for="inputEmail3" class="col-sm-2 control-label">Role
														Name :</label>

													<div class="col-sm-4">
														<input class="form-control" id="txtRoleName">
													</div>

													<div class="col-md-12">
														<hr>
													</div>


													<div class="col-md-3">
														<div class="input-group">
															<label for="lblMenu" class="control-label">Menu :</label>
															<select multiple id="slMenu" name="slMenu"
																class="form-control" style="height: 350px">
																<c:forEach items="${listmenu}" var="menu">
																	<c:if test="${menu.level == '0'}">
																		<option value="<c:out value="${menu.menuID}" />"><c:out
																				value="${menu.menuName}" /></option>
																	</c:if>
																	<c:if test="${menu.level == '1'}">
																		<option value="<c:out value="${menu.menuID}" />">&nbsp
																			<c:out value="${menu.menuName}" /></option>
																	</c:if>
																	<c:if test="${menu.level == '2'}">
																		<option value="<c:out value="${menu.menuID}" />">&nbsp&nbsp&nbsp&nbsp
																			<c:out value="${menu.menuName}" /></option>
																	</c:if>
																</c:forEach>
															</select>
														</div>

													</div>

													<div class="col-md-3">
														<div class="input-group" style="width: 150px">
															<br> <br>
															<button type="button" class="btn btn-block btn-default"
																onclick="FuncAddMenu()">Add ></button>
															<button type="button" class="btn btn-block btn-default"
																onclick="FuncAddAllMenu()">Add All >></button>
															<button type="button" class="btn btn-block btn-default"
																onclick="FuncRemoveMenu()">< Remove</button>
															<button type="button" class="btn btn-block btn-default"
																onclick="FuncRemoveAllMenu()"><< Remove All</button>
														</div>
													</div>

													<div class="col-md-3">
														<div class="input-group">
															<label for="lblAllowedMenu" class="control-label">Allowed
																Menu :</label> <select multiple id="slAllowedMenu"
																name="slAllowedMenu" class="form-control"
																style="height: 350px">
																<c:forEach items="${listallowedmenu}" var="allowedmenu">
																	<c:if test="${allowedmenu.level == '0'}">
																		<option
																			value="<c:out value="${allowedmenu.menuID}" />"><c:out
																				value="${allowedmenu.menuName}" /></option>
																	</c:if>
																	<c:if test="${allowedmenu.level == '1'}">
																		<option
																			value="<c:out value="${allowedmenu.menuID}" />">&nbsp
																			<c:out value="${allowedmenu.menuName}" /></option>
																	</c:if>
																	<c:if test="${allowedmenu.level == '2'}">
																		<option
																			value="<c:out value="${allowedmenu.menuID}" />">&nbsp&nbsp&nbsp&nbsp
																			<c:out value="${allowedmenu.menuName}" /></option>
																	</c:if>
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="col-md-3">
														<div class="input-group">
															<label for="lblMenuLeverage" class="control-label">Editable
																Menu :</label>

															<div class="panel panel-primary" id="EditableMenuPanel"
																style="overflow: auto; height: 350px;">
																<div class="panel-body fixed-panel">
																	<c:forEach items="${listeditablemenu}" var="crudmenu">
																		<div class="checkbox" id="cbmenu">
																			<label><input type="checkbox"
																				name="cbEditableMenu"
																				id="<c:out value="${crudmenu.menuID}" />"
																				<%-- name="<c:out value="${crudmenu.menuID}" />" --%>
																				value="<c:out value="${crudmenu.menuID}" />">
																				<c:out value="${crudmenu.menuName}" /></label>
																		</div>

																	</c:forEach>

																</div>
															</div>

															<div class="row">
																<div class="col-md-6" style="weight: 200px">
																	<button type="button" class="btn btn-block btn-default"
																		onclick="FuncSelectAll()">Select All</button>
																</div>
																<div class="col-md-6" style="margin-left: -20px">
																	<button type="button" class="btn btn-block btn-default"
																		style="width: 100px" onclick="FuncUnselectAll()">Unselect
																		All</button>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
											
											<div class="modal-footer">
												<button style="display: <c:out value="${buttonstatus}"/>" type="button"
															id="btnSave" name="btnSave" class="btn btn-primary"
															onclick="FuncSaveRule()"><i class="fa fa-check"></i> Save Role</button>
												<button style="display: <c:out value="${buttonstatus}"/>" type="button"
													id="btnUpdate" name="btnUpdate" class="btn btn-primary"
													onclick="FuncUpdateRule()"><i class="fa fa-edit"></i> Update Role</button>	
											</div>
											
										</div>
									</div>
								</div>
								<!-- /.end modal update & Insert -->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Role</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtRoleID" name="temp_txtRoleID"  />
									<p>Are you sure to delete this role ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_userrole"
			class="table table-bordered table-striped table-hover">
			<thead style="background-color: #d2d6de;">
				<tr>
					<th>Role ID</th>
					<th>Role Name</th>
					<!-- <th></th> -->
				</tr>
			</thead>

			<tbody>

				<c:forEach items="${listuserrole}" var="userrole">
					<tr>
						<td><a href=""
							style="color: #144471; font-weight: bold;"
							data-toggle="modal" data-target="#ModalUpdateInsert"
							data-luserroleid='<c:out value="${userrole.roleID}" />'
							data-luserrolename='<c:out value="${userrole.roleName}" />'
							data-lkey="Update"> <c:out value="${userrole.roleID}" />
						</a></td>
						<td><a href=""
							style="color: #144471; font-weight: bold;"
							data-toggle="modal" data-target="#ModalUpdateInsert"
							data-luserroleid='<c:out value="${userrole.roleID}" />'
							data-luserrolename='<c:out value="${userrole.roleName}" />'
							data-lkey="Update"> <c:out
									value="${userrole.roleName}" />
						</a></td>
						<!-- 						<td> -->
						<%-- 						<c:if test="${buttonstatus != 'disabled'}"> --%>
						<!-- 							<a href="" data-toggle="modal" -->
						<!-- 							data-target="#ModalDelete" -->
						<%-- 							data-luserroleid='<c:out value="${userrole.roleID}" />'>Delete</a> --%>
						<%-- 						</c:if> --%>
						<!-- 						</td> -->
					</tr>

				</c:forEach>
			</tbody>
		</table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_userrole').DataTable();
    $('#M002').addClass('active');
    $('#M011').addClass('active');
	
	//shortcut for button 'new'
		// 	    Mousetrap.bind('n', function() {
		// 			//FuncButtonNew(),
		// 	    	$('#ModalUpdateInsert').modal('show')
		// 	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>

<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lRoleID = button.data('luserroleid');
		$("#temp_txtRoleID").val(lRoleID);
	})
</script>
<!-- modal script -->

<script>
$('#ModalUpdateInsert').on(
		'shown.bs.modal',
		function(event) {
			//FuncClearStyleNone();
			$("#dvErrorAlert").hide();

			var button = $(event.relatedTarget);
			var lRoleID = button.data('luserroleid');
			var lRoleName = button.data('luserrolename');
			var lKey = button.data('lkey');
			var modal = $(this);
			$('#btnSave').show();
			$('#btnUpdate').hide();
			$("#txtRoleId").prop('disabled', false);
			$("#txtRoleName").prop('disabled', false);

			//Clear list
			$('#slAllowedMenu option').prop('selected', true);
			$("#slAllowedMenu option:selected").remove();

			$("#EditableMenuPanel :input").prop("checked", false);

			modal.find(".modal-body #txtRoleId").val(lRoleID);
			modal.find(".modal-body #txtRoleName").val(lRoleName);

			if (lKey == "Update") {
				$('#btnSave').hide();
				$('#btnUpdate').show();
				$("#txtRoleId").prop('disabled', true);
				$("#txtRoleName").prop('disabled', true);

				//<<Javascript For 1 json
				$.ajax({
					url : '${pageContext.request.contextPath}/getMenu',
					type : 'POST',
					data : {
						menuid : lRoleID,
						command : lKey
					},
					dataType : 'json'
				}).done(
						function(data) {
							console.log(data);
							var data1 = data[0];
							var data2 = data[1];

							for ( var i in data1) {
								var obj = data1[i];
								var index = 0;
								var id, name, url, type, level;
								for ( var prop in obj) {
									switch (index++) {
									case 0:
										id = obj[prop];
										break;
									case 1:
										name = obj[prop];
										break;
									case 2:
										url = obj[prop];
										break;
									case 3:
										type = obj[prop];
										break;
									case 4:
										level = obj[prop];
										break;
									default:
										break;
									}
								}
								if ($("#slAllowedMenu option[value="
										+ id + "]").length > 0) {
								} else {
									if (level == '0')
										$('#slAllowedMenu').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name
														+ "</option>");
									if (level == '1')
										$('#slAllowedMenu').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
														+ name
														+ "</option>");
									if (level == '2')
										$('#slAllowedMenu').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp"
														+ name
														+ "</option>");
								}
							}

							for ( var i in data2) {
								var obj = data2[i];
								modal
										.find(
												".modal-body #"
														+ obj.MenuID)
										.prop('checked', true);

								var el = document
										.getElementById(obj.MenuID);
								//if (el.style.display = 'none')
								//el.style.display = '';

							}
						}).fail(function() {
					console.log('Service call failed!');
				})
			}
		})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

// function FuncClearStyleNone() {
// 	$('input[name="cbEditableMenu"]').each(function() {
// 		var el = document.getElementById(this.value);
// 		if (el.style.display = 'none')
// 			el.style.display = '';
// 	});
// }

function FuncSelectAll() {
	$("#EditableMenuPanel :input").prop("checked", true);
}

function FuncUnselectAll() {
	$("#EditableMenuPanel :input").prop("checked", false);
}

function FuncSaveRule() {
	var listMenu = document.getElementById('slMenu');
	var listAllowedMenu = document.getElementById('slAllowedMenu');
	var optionAllMenu = new Array();
	var optionVal = new Array();
	var checkVal = new Array();
	var txtRoleId = document.getElementById('txtRoleId').value;
	var txtRoleName = document.getElementById('txtRoleName').value;

	for (x = 0; x < listMenu.length; x++) {
		optionAllMenu.push(listMenu.options[x].value);
	}

	for (i = 0; i < listAllowedMenu.length; i++) {
		optionVal.push(listAllowedMenu.options[i].value);
	}

	$('input[name="cbEditableMenu"]:checked').each(function() {
		checkVal.push(this.value);
	});

	jQuery
			.ajax({
				url : '${pageContext.request.contextPath}/Role',
				type : 'POST',
				dataType : 'text',
				data : {
					"key" : 'saverule',
					"listAllMenu" : optionAllMenu,
					"listAllowedMenu" : optionVal,
					"listEditableMenu" : checkVal,
					"txtRoleId" : txtRoleId,
					"txtRoleName" : txtRoleName
				},
				success : function(data, textStatus, jqXHR) {
					if (data.split("--")[0] == 'FailedInsertUserRole') {
						$("#dvErrorAlert").show();
						document.getElementById("lblAlert").innerHTML = "Gagal menambahkan hak akses";
						document.getElementById("lblAlertDescription").innerHTML = data
								.split("--")[1];
						$("#txtRoleId").focus();
						$("#ModalUpdateInsert").animate({
							scrollTop : 0
						}, 'slow');
						return false;
					} else {
						var url = '${pageContext.request.contextPath}/Role';
						$(location).attr('href', url);
					}
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
				}
			});
	return true;
}

function FuncUpdateRule() {
// 	FuncClearStyleNone()
	var listMenu = document.getElementById('slMenu');
	var listAllowedMenu = document.getElementById('slAllowedMenu');
	var optionAllMenu = new Array();
	var optionVal = new Array();
	var checkVal = new Array();
	var txtRoleId = $("#txtRoleId").val();
	var txtRoleName = $("#txtRoleName").val();

	for (x = 0; x < listMenu.length; x++) {
		optionAllMenu.push(listMenu.options[x].value);
	}

	for (i = 0; i < listAllowedMenu.length; i++) {
		optionVal.push(listAllowedMenu.options[i].value);
	}

	$('input[name="cbEditableMenu"]:checked').each(function() {
		checkVal.push(this.value);
	});

	jQuery
			.ajax({
				url : '${pageContext.request.contextPath}/Role',
				type : 'POST',
				dataType : 'text',
				data : {
					"key" : 'updaterule',
					"listAllMenu" : optionAllMenu,
					"listAllowedMenu" : optionVal,
					"listEditableMenu" : checkVal,
					"txtRoleId" : txtRoleId,
					"txtRoleName" : txtRoleName
				},
				success : function(data, textStatus, jqXHR) {
					if (data.split("--")[0] == 'FailedUpdateUserRole') {
						$("#dvErrorAlert").show();
						document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui hak akses";
						document.getElementById("lblAlertDescription").innerHTML = data
								.split("--")[1];
						$("#txtRoleName").focus();
						$("#ModalUpdateInsert").animate({
							scrollTop : 0
						}, 'slow');
						return false;
					} else {
						var url = '${pageContext.request.contextPath}/Role';
						$(location).attr('href', url);
					}
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
				}
			});
	return true;

}

function FuncAddMenu() {
	var MenuID = document.getElementById('slMenu').value;
	var Command = "Add";

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command
		},
		dataType : 'json'
	})
			.done(
					function(data) {
						console.log(data);
						for ( var i in data) {
							var obj = data[i];
							var index = 0;
							var id, name, url, type, level;
							for ( var prop in obj) {
								switch (index++) {
								case 0:
									id = obj[prop];
									break;
								case 1:
									name = obj[prop];
									break;
								case 2:
									url = obj[prop];
									break;
								case 3:
									type = obj[prop];
									break;
								case 4:
									level = obj[prop];
									break;
								default:
									break;
								}
							}

							if ($("#slAllowedMenu option[value=" + id
									+ "]").length > 0) {
							} else {
								if (level == '0')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">"
													+ name
													+ "</option>");
								if (level == '1')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
													+ name
													+ "</option>");
								if (level == '2')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp"
													+ name
													+ "</option>");
							}

						}

					}).fail(function() {
				console.log('Service call failed!');
			})
}

function FuncAddAllMenu() {

	var MenuID = "All";
	var Command = "Add";

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command
		},
		dataType : 'json'
	})
			.done(
					function(data) {
						console.log(data);
						for ( var i in data) {
							var obj = data[i];
							var index = 0;
							var id, name, url, type, level;
							for ( var prop in obj) {
								switch (index++) {
								case 0:
									id = obj[prop];
									break;
								case 1:
									name = obj[prop];
									break;
								case 2:
									url = obj[prop];
									break;
								case 3:
									type = obj[prop];
									break;
								case 4:
									level = obj[prop];
									break;
								default:
									break;
								}
							}

							if ($("#slAllowedMenu option[value=" + id
									+ "]").length > 0) {

							} else {
								if (level == '0')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">"
													+ name
													+ "</option>");
								if (level == '1')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
													+ name
													+ "</option>");
								if (level == '2')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp"
													+ name
													+ "</option>");
							}

						}

					}).fail(function() {

			})
}

function FuncRemoveMenu() {
	var MenuID = document.getElementById('slAllowedMenu').value;
	var Command = "Remove";

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command
		},
		dataType : 'json'
	}).done(function(data) {
		console.log(data);
		for ( var i in data) {
			var obj = data[i];
			var index = 0;
			var id, name, url, type, level;
			for ( var prop in obj) {
				switch (index++) {
				case 0:
					id = obj[prop];
					break;
				case 1:
					name = obj[prop];
					break;
				case 2:
					url = obj[prop];
					break;
				case 3:
					type = obj[prop];
					break;
				case 4:
					level = obj[prop];
					break;
				default:
					break;
				}
			}

			$("#slAllowedMenu option[value=" + id + "]").remove();
		}

	}).fail(function() {
		console.log('Service call failed!');
	})
}

function FuncRemoveAllMenu() {
	$('#slAllowedMenu option').prop('selected', true);
	$("#slAllowedMenu option:selected").remove();
}
</script>

</body>
</html>