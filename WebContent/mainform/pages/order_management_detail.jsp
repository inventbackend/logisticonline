<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Order Management Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="Form_Order_Management_Detail" name="Form_Order_Management_Detail" action = "${pageContext.request.contextPath}/OrderManagementDetail" method="post">
<input type="hidden" id="orderManagementID" name="orderManagementID" value="<c:out value="${orderManagementID}"/>" />
<input type="hidden" id="districtID" name="districtID" value="<c:out value="${districtID}"/>" />
<input type="hidden" id="tierID" name="tierID" value="<c:out value="${tierID}"/>" />
<input type="hidden" id="orderStatus" name="orderStatus" value="<c:out value="${orderStatus}"/>" />

	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Data Order Detail <br><br>
				<small style="color: black; font-weight: bold;">Order ID : <c:out value="${orderManagementID}"/></small>
				<small style="color: black; font-weight: bold;">Order Status : <c:out value="${statusString}"/></small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<!-- declare docNo from servlet -->
							<c:if test="${condition == 'SuccessInsertOrderManagement'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Insert order success. Please insert detail of the order.
              				</div>
	      					</c:if>  
						
							<c:if test="${condition == 'SuccessInsertOrderManagementDetail'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Insert success.
              				</div>
	      					</c:if> 
	      					<c:if test="${condition == 'FailedInsertOrderManagementDetail'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Insert failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if> 
	      					
	      					<c:if test="${condition == 'SuccessUpdateOrderManagementDetail'}">
					 		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  Update success.
					 		</div>
							</c:if>
							<c:if test="${condition == 'FailedUpdateOrderManagementDetail'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Update failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if>
							
							<c:if test="${condition == 'SuccessCommitOrderManagementDetail'}">
					 		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  Commit success.
					 		</div>
							</c:if>
							<c:if test="${condition == 'FailedCommitOrderManagementDetail'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Commit failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if>
					
							<c:if test="${condition == 'SuccessDeleteOrderManagementDetail'}">
							 	<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  	Delete success.
					 			</div>
							</c:if>
							<c:if test="${condition == 'FailedDeleteOrderManagementDetail'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Delete failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if>
	      					
	      							<!--modal update & Insert -->
									<!-- 									<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel"> -->
									<!--  										<div class="modal-dialog" role="document"> -->
									<!--     										<div class="modal-content"> -->
									    										
									<!--     										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible"> -->
									<!-- 						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
									<!-- 						          				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
									<!-- 						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>. -->
									<!-- 					     					</div> -->
														     					
									<!--       											<div class="modal-header"> -->
									<!--         											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
									<!--         											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	 -->
									        												
									        											
									<!--       											</div> -->
									<!-- 	      								<div class="modal-body"> -->
									<!-- 	          								<div id="dvOrderManagementDetailID" class="form-group"> -->
									<!-- 	            								<label for="recipient-name" class="control-label">Order Detail ID :</label>	 -->
									<!-- 	            								<input type="text" class="form-control" id="txtOrderManagementDetailID" name="txtOrderManagementDetailID" readonly disabled> -->
									<!-- 	          								</div> -->
									<!-- 	          								<div id="dvstuffingDate" class="form-group"> -->
									<!-- 	            								<label for="message-text" class="control-label">Stuffing Date :</label><label id="mrkStuffingDate" for="recipient-name" class="control-label"><small>*</small></label>	 -->
									<!-- 	            								<input type="text" class="form-control" id="txtStuffingDate" name="txtStuffingDate" data-date-format="dd MM yyyy"> -->
									<!-- 	          								</div> -->
									<!-- 	          								<div id="dvTier" class="form-group"> -->
									<!-- 		          								<label for="message-text" class="control-label">Tier :</label><label id="mrkTier" for="recipient-name" class="control-label"><small>*</small></label> -->
									<!-- 										 		<select id="slTier" name="slTier" class="form-control select2" style="width: 100%;"> -->
									<%-- 													<c:forEach items="${listTier}" var="tier"> --%>
									<%-- 														<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.tierID}" /> - <c:out value="${tier.description}" /></option> --%>
									<%-- 													</c:forEach> --%>
									<!-- 										       	</select> -->
									<!-- 									      	</div> -->
									<!-- 	          								<div id="dvContainerType" class="form-group"> -->
									<!-- 		          								<label for="message-text" class="control-label">Container Type :</label><label id="mrkContainerType" for="recipient-name" class="control-label"><small>*</small></label> -->
									<!-- 										 		<select id="slContainerType" name="slContainerType" class="form-control select2" style="width: 100%;"> -->
									<%-- 													<c:forEach items="${listContainerType}" var="containerType"> --%>
									<%-- 														<option value="<c:out value="${containerType.containerTypeID}" />"><c:out value="${containerType.containerTypeID}" /> - <c:out value="${containerType.description}" /></option> --%>
									<%-- 													</c:forEach> --%>
									<!-- 										       	</select> -->
									<!-- 									      	</div> -->
									<!-- 									      	<div id="dvItemDescription" class="form-group"> -->
									<!-- 		         								<label for="lblItemDescription" class="control-label">Item Description :</label><label id="mrkItemDescription" for="recipient-name" class="control-label"><small>*</small></label>	 -->
									<!-- 		         								<input type="text" class="form-control" id="txtItemDescription" name="txtItemDescription"> -->
									<!-- 		       								</div> -->
									<!-- 		       								<div id="dvQuantity" class="form-group"> -->
									<!-- 		         								<label for="lblQuantity" class="control-label">Quantity :</label><label id="mrkQuantity" for="recipient-name" class="control-label"><small>*</small></label>	 -->
									<!-- 		         								<input type="text" class="form-control number" id="txtQuantity" name="txtQuantity"> -->
									<!-- 		       								</div> -->
									<!--       								</div> -->
									      								
									<!--       								<div class="modal-footer"> -->
									<!--         									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button> -->
									<!--         									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button> -->
									<!--         									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
									        								
									<!--       								</div> -->
									<!--     										</div> -->
									<!--   										</div> -->
									<!-- 									</div> -->
									
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Order Detail</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtOrderManagementID" name="temp_txtOrderManagementID"  />
						<input type="hidden" id="temp_txtOrderManagementDetailID" name="temp_txtOrderManagementDetailID"  />
									<p>Are you sure to delete this order detail ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		<!--modal Commit -->
		<div class="modal modal-info" id="ModalCommit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Commit Order</h4>
							</div>
						<div class="modal-body">
									<p>Are you sure to commit this order ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnCommit" name="btnCommit" class="btn btn-outline" >Commit</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		<!--modal show vendor data -->
		<div class="modal fade" id="ModalVendor" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendor">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="ModalLabelVendor"></h4>	
					</div>
 								<div class="modal-body">
 									<div id="dynamic-content-vendor"></div>
								</div>
				</div>
			</div>
		</div>
		<!-- /. end of modal show vendor data -->
							
							<c:if test="${orderStatus eq '0' && userRole ne 'R001'}">
									<!-- <div class="row"> -->
									<!-- 									<div class="col-md-1 pull-right"> -->
									<!-- 										<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right"  -->
									<!-- 													data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"> -->
									<!-- 													<i class="fa fa-plus-circle"></i> New</button> -->
									<!-- 									</div> -->
									<!-- <div class="col-md-0 pull-right"> -->
										<button id="btnCommit" name="btnCommit" type="button" class="btn btn-primary pull-right" 
											style="background-color:#3eab29;"
											data-toggle="modal" data-target="#ModalCommit" 
											data-lordermanagementid='<c:out value="${orderManagementID}"/>' >
											<i class="fa fa-check"></i> Commit
										</button>
									<!-- </div> -->
									<!-- </div> -->
							</c:if>
							
	      					<br><br>
							<!-- form isian order detail -->
							<c:if test="${orderStatus eq '0' && userRole ne 'R001'}">
							<div class="row">
		      					<div id="dvOrderManagementDetailID" class="form-group col-md-3">
	  								<label for="recipient-name" class="control-label">Order Detail ID :</label>	
	  								<input type="text" class="form-control" id="txtOrderManagementDetailID" name="txtOrderManagementDetailID" readonly disabled>
								</div>
	    						<div id="dvstuffingDate" class="form-group col-md-2">
	      							<label for="message-text" class="control-label">Stuffing Date :</label><label id="mrkStuffingDate" for="recipient-name" class="control-label"><small>*</small></label>	
	      							<input type="text" class="form-control" id="txtStuffingDate" name="txtStuffingDate" value="<c:out value="${stuffingDateDefault}"/>">
	    						</div>
	    						<div id="dvContainerType" class="form-group col-md-2">
	     							<label for="message-text" class="control-label">Container Type :</label><label id="mrkContainerType" for="recipient-name" class="control-label"><small>*</small></label>
				 					<select id="slContainerType" name="slContainerType" class="form-control select2" style="width: 100%;">
										<c:forEach items="${listContainerType}" var="containerType">
											<option value="<c:out value="${containerType.containerTypeID}" />"><c:out value="${containerType.containerTypeID}" /> - <c:out value="${containerType.description}" /></option>
										</c:forEach>
							       	</select>
			      				</div>
	  							<div id="dvQuantity" class="form-group col-md-2">
	    							<label for="lblQuantity" class="control-label">Quantity :</label><label id="mrkQuantity" for="recipient-name" class="control-label"><small>*</small></label>	
	    							<input type="text" class="form-control number" id="txtQuantity" name="txtQuantity">
	  							</div>
	  							<div class="form-group col-md-2">
	  								<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput()" style="margin-top: 25px">Save</button>
  								</div>
  							</div>
  							</c:if>
  							<!-- akhir dari form isian order detail -->
	      					
							<table id="tb_order_management_detail" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Order Detail</th>
										<th>Pickup Date</th>
										<th>Container Type</th>
										<th>Status</th>
										<th>Qty</th>
										<th>Rate</th>
										<th>Sub Total</th>
										<th>Qty Delivered</th>
										<th>Rate Delivered</th>
										<th style="width: 103px"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listOrderManagementDetail}" var="orderdetail">
										<tr>
											<td><c:out value="${orderdetail.orderManagementDetailID}" /></td>
											<td><c:out value="${orderdetail.stuffingDate}" /></td>
											<td><c:out value="${orderdetail.containerTypeDescription}" /></td>
											<td>
												<c:if test="${orderdetail.orderStatus == 0}">New</c:if>
												<c:if test="${orderdetail.orderStatus == 1}">Committed</c:if>
												<c:if test="${orderdetail.orderStatus == 2}">Received</c:if>
												<c:if test="${orderdetail.orderStatus == 3}">On Process</c:if>
												<c:if test="${orderdetail.orderStatus == 4}">Finished</c:if>
												<c:if test="${orderdetail.orderStatus == 10}">Cancelled</c:if>
											</td>
											<td><c:out value="${orderdetail.quantity}" /></td>
											<td>
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${orderdetail.ratePrice}" type="currency"/>
											</td>
											<td>
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${orderdetail.price}" type="currency"/>
											</td>
											<td><c:out value="${orderdetail.quantityDelivered}" /></td>
											<td>
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${orderdetail.priceDelivered}" type="currency"/>
											</td>
											<td>
												<button
														type="button" class="btn btn-default"
														title="show vendor"
														data-toggle="modal" 
											        	data-target="#ModalVendor"
											        	data-lordermanagementdetailid='<c:out value="${orderdetail.orderManagementDetailID}"/>'
														><i class="fa fa-truck"></i></button>
											<c:if test="${orderStatus eq '0' && userRole ne 'R001'}">
												<button
														type="button" class="btn btn-info"
														onclick="FuncButtonUpdate('<c:out value="${orderdetail.orderManagementDetailID}"/>',
																				'<c:out value="${orderdetail.stuffingDate}"/>',
																				'<c:out value="${orderdetail.containerTypeID}"/>',
																				'<c:out value="${orderdetail.quantity}"/>')"
														><i class="fa fa-edit"></i></button>
													<button type="button" class="btn btn-danger" data-toggle="modal" 
											        		data-target="#ModalDelete" 
											        		data-lordermanagementid='<c:out value="${orderManagementID}"/>'
											        		data-lordermanagementdetailid='<c:out value="${orderdetail.orderManagementDetailID}"/>'>
											        		<i class="fa fa-trash"></i>
											        </button>
												</c:if>
									        </td>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
		
		$('#M010').addClass('active');
  		$("#tb_order_management_detail").DataTable();
		//$("#dvErrorAlert").hide();
		
		$('#txtStuffingDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    
	    FuncButtonNew();
	    FuncClear();
  	});
  	
	$('input.number').keyup(function(event) {
 	   // skip for arrow keys
 	   if(event.which >= 37 && event.which <= 40) return;
 	   // format number
 	   $(this).val(function(index, value) {
 	     return value
 	     .replace(/\D/g, "")
 	     .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
 	     ;
 	   });
	});
 	
 	//shortcut for button 'new'
	//     Mousetrap.bind('n', function(){
	// 		FuncButtonNew(), 
	// 		$('#ModalUpdateInsert').modal('show')
	// 	});
	</script>

<script>
// 	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
// 		$("#dvErrorAlert").hide();
	
//  		var button = $(event.relatedTarget);
//  		var lOrderDetailID = button.data('lordermanagementdetailid');
//  		var lStuffingDate = button.data('lstuffingdate'); 		
//  		var lTierID = button.data('ltierid');
//  		var lContainerTypeID = button.data('lcontainertypeid');
//  		var lItemDescription = button.data('litemdescription');
//  		var lQuantity = button.data('lquantity');
 		
//  		var modal = $(this);
 		
//  		modal.find(".modal-body #txtOrderManagementDetailID").val(lOrderDetailID);
// 		modal.find(".modal-body #txtStuffingDate").val(lStuffingDate);
//  		modal.find(".modal-body #slTier").val(lTierID).trigger("change");
//  		modal.find(".modal-body #slContainerType").val(lContainerTypeID).trigger("change");
//  		modal.find(".modal-body #txtItemDescription").val(lItemDescription);
//  		modal.find(".modal-body #txtQuantity").val(lQuantity);
 		
//  		$('#txtStuffingDate').datepicker({
// 	      format: 'dd MM yyyy',
// 	      autoclose: true
// 	    });
    
//  		$('#txtStuffingDate').focus();
// 	})

	$('#ModalVendor').on('shown.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lOrderDetailID = button.data('lordermanagementdetailid');
    
    	$('#dynamic-content-vendor').html(''); // leave this div blank
 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getVendorOrderDetailByCustomerOrder',
	          type: 'POST',
	          data: 'orderDetailID='+lOrderDetailID,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-vendor').html(''); // blank before load.
	          $('#dynamic-content-vendor').html(data); // load here
	     })
	     .fail(function(){
	          $('#dynamic-content-vendor').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     });
	})	
	
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lOrderID = button.data('lordermanagementid');
		var lOrderDetailID = button.data('lordermanagementdetailid');
		$("#temp_txtOrderManagementID").val(lOrderID);
		$("#temp_txtOrderManagementDetailID").val(lOrderDetailID);
	});
	
	$('#ModalCommit').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lOrderID = button.data('lordermanagementid');
		$("#temp_txtOrderManagementID").val(lOrderID);
	});
	
</script>

<script>
function FuncClear(){
	jQuery('#mrkStuffingDate').hide();
	//jQuery('#mrkTier').hide();
	jQuery('#mrkContainerType').hide();
	jQuery('#mrkItemDescription').hide();
	jQuery('#mrkQuantity').hide();
	
	jQuery('#dvStuffingDate').removeClass('has-error');
	//jQuery('#dvTier').removeClass('has-error');
	jQuery('#dvContainerType').removeClass('has-error');
	jQuery('#dvItemDescription').removeClass('has-error');
	jQuery('#dvQuantity').removeClass('has-error');
	
// 	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	//$('#dvOrderManagementDetailID').hide();
	$('#txtOrderManagementDetailID').val('');
	$('#slContainerType').val('').trigger("change");
	$('#txtQuantity').val('');
 	
	//document.getElementById("lblTitleModal").innerHTML = "Add Order Detail";
	
	//$('#txtStuffingDate').focus();
	//FuncClear();
}

function FuncButtonUpdate(lOrderDetail, lStuffingDate, lContainerType, lQuantity) {
	//$('#dvOrderManagementDetailID').show();
	// 	$('#btnSave').hide();
	// 	$('#btnUpdate').show();
	//document.getElementById('lblTitleModal').innerHTML = 'Edit Order Detail';
	
	$("#txtOrderManagementDetailID").val(lOrderDetail);
	$("#txtStuffingDate").val(lStuffingDate);
 	$("#slContainerType").val(lContainerType).trigger("change");
 	$("#txtQuantity").val(lQuantity);
	
	// 	$('#txtStuffingDate').focus();
	FuncClear();
}

function FuncValEmptyInput() {
	var txtOrderManagementID = jQuery('#orderManagementID').val();
	var txtOrderManagementDetailID = jQuery('#txtOrderManagementDetailID').val();
		if(txtOrderManagementDetailID == '' || txtOrderManagementDetailID == null)
			var lParamBtn = 'save';
		else
			var lParamBtn = 'update';
	var txtStuffingDate = jQuery('#txtStuffingDate').val();
	var slContainerType = jQuery('#slContainerType').val();
	var txtQuantity = jQuery('#txtQuantity').val();
	var districtID = jQuery('#districtID').val();
	var tierID = jQuery('#tierID').val();

    if(!txtStuffingDate.match(/\S/)) {
    	$("#txtStuffingDate").focus();
    	$('#dvStuffingDate').addClass('has-error');
    	$('#mrkStuffingDate').show();
        return false;
    } 
    if(!slContainerType.match(/\S/)) {
    	$('#slContainerType').focus();
    	$('#dvContainerType').addClass('has-error');
    	$('#mrkContainerType').show();
        return false;
    }
    if(!txtQuantity.match(/\S/)) {    	
    	$('#txtQuantity').focus();
    	$('#dvQuantity').addClass('has-error');
    	$('#mrkQuantity').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/OrderManagementDetail',	
        type:'POST',
        data:{"key":lParamBtn,"txtOrderManagementID":txtOrderManagementID,"txtOrderManagementDetailID":txtOrderManagementDetailID,
        	  "txtStuffingDate":txtStuffingDate, "slTier":tierID, "slContainerType":slContainerType, 
        	   "txtQuantity":txtQuantity, "districtID":districtID},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
		//         	if(data.split("--")[0] == 'FailedInsertOrderManagementDetail')
		//         	{
		//         		$("#dvErrorAlert").show();
		//         		document.getElementById("lblAlert").innerHTML = "Insert failed";
		//         		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		//         		$("#txtStuffingDate").focus();
		//         		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		//         		return false;
		//         	}
		//         	else if(data.split("--")[0] == 'FailedUpdateOrderManagementDetail')
		//         	{
		//         		$("#dvErrorAlert").show();
		//         		document.getElementById("lblAlert").innerHTML = "Update failed";
		//         		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		// 				$("#txtStuffingDate").focus();
		//         		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		//         		return false;
		//         	}
		//         	else
		//         	{
	        	var url = '${pageContext.request.contextPath}/OrderManagementDetail';  
	        	$(location).attr('href', url);
		//			}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtStuffingDate:focus').length) {
    	$('#txtStuffingDate').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slContainerType:focus').length) {
    	$('#slContainerType').click();
    }
});

// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}
</script>

</body>
</html>