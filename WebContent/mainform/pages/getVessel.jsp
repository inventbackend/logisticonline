<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_vessel" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Vessel Code</th>
			<th>Vessel Name</th>
			<th>Voyage Code</th>
			<th>Voyage Status</th>
			<th>Company</th>
			<th>Pod</th>
			<th>Pod Name</th>
			<th>Arrival</th>
			<th>Departure</th>
			<th style="width: 20px"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listVessel}" var ="vessel">
			<tr>
				<td><c:out value="${vessel.vesselCode}" /></td>
				<td><c:out value="${vessel.vesselName}" /></td>
				<td><c:out value="${vessel.voyageCode}" /></td>
				<td><c:out value="${vessel.voyageStatus}" /></td>
				<td><c:out value="${vessel.voyageCompany}" /></td>
				<td><c:out value="${vessel.podCode}" /></td>
				<td><c:out value="${vessel.podName}" /></td>
				<td><c:out value="${vessel.voyageArrival}" /></td>
				<td><c:out value="${vessel.voyageDeparture}" /></td>
				<td><button type="button" class="btn btn-primary"
							data-toggle="modal"
							onclick="FuncPassVesselVoyage('<c:out value="${vessel.vesselCode}"/>',
														  '<c:out value="${vessel.voyageCode}"/>',
														  '<c:out value="${vessel.voyageCompany}"/>',
														  '<c:out value="${vessel.podCode}"/>',
														  '<c:out value="${vessel.voyageDeparture}"/>',
														  '<c:out value="${vessel.exportLimit}"/>')"
							data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_vessel").DataTable({"aaSorting": [],"scrollX": true});
  	});
</script>