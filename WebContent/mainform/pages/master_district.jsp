<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master District</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
 <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_District" action = "${pageContext.request.contextPath}/District" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master District
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertDistrict'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan district.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateDistrict'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui district.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteDistrict'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus district.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteDistrict'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus district. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
		          <div id="dvDistrictID" class="form-group">
		            <label for="lblDistrictID" class="control-label">District ID:</label><label id="mrkDistrictID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDistrictID" name="txtDistrictID" readonly disabled>
		          </div>
		          <div id="dvProvince" class="form-group">
				     <label class="control-label">Province:</label><label id="mrkProvince" for="lbl-validation" class="control-label"><small>*</small></label>
				     <select id="slProvince" name="slProvince" class="form-control select2" style="width: 100%;">
						<c:forEach items="${listProvince}" var="province">
						<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
						</c:forEach>
				 	</select>
				 	<input id="idtest" style="display:none">
	  			  </div>
		          <div id="dvName" class="form-group">
		            <label for="lblName" class="control-label">Name:</label><label id="mrkName" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtName" name="txtName">
		          </div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete District</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtDistrictID" name="temp_txtDistrictID"  />
									<p>Are you sure to delete this district ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_district" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>Province</th>
	                  <th>District ID</th>
	                  <th>District Name</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listDistrict}" var ="district">
		        	<tr>
		        	<td><c:out value="${district.province}"/></td>
		        	<td><c:out value="${district.districtID}"/></td>
					<td><c:out value="${district.name}"/></td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lprovince='<c:out value="${district.province}"/>'
								data-ldistrictid='<c:out value="${district.districtID}"/>'
								data-lname='<c:out value="${district.name}"/>' 
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-ldistrictid='<c:out value="${district.districtID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
  	//Initialize Select2 Elements
	$(".select2").select2();
	$('#slProvince').val('').trigger("change");
	
    $('#tb_master_district').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
	$('#M013').addClass('active');
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lDistrictID = button.data('ldistrictid');
		$("#temp_txtDistrictID").val(lDistrictID);
	})
	</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var ldistrictid = button.data('ldistrictid') // Extract info from data-* attributes
	  var lname = button.data('lname')  // Extract info from data-* attributes
	  var lprovince = button.data('lprovince')
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	//   modal.find('.modal-title').text('New message to ' + recipient)
	  modal.find('.modal-body #txtDistrictID').val(ldistrictid)
	  modal.find('.modal-body #txtName').val(lname)
	  modal.find(".modal-body #slProvince").val(lprovince).trigger("change");
	  
	  $("#slProvince").focus();
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

	function FuncClear(){
		$('#mrkDistrictID').hide();
		$('#mrkName').hide();
		$('#mrkProvince').hide();
		$('#txtDistrictID').prop('disabled', false);
		
		$('#dvtxtDistrictID').removeClass('has-error');
		$('#dvName').removeClass('has-error');
		$('#dvProvince').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#btnSave').show();
		$('#btnUpdate').hide();
		$('#dvDistrictID').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add District";	
	
		FuncClear();
		$('#txtDistrictID').prop('disabled', false);
	}
	
	function FuncButtonUpdate() {
		$('#btnSave').hide();
		$('#btnUpdate').show();
		$('#dvDistrictID').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit District';
	
		FuncClear();
		$('#txtDistrictID').prop('disabled', true);
	}
	
	//function delete rate
	function FuncDelete() {
		var temp_txtDistrictID = document.getElementById('temp_txtDistrictID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/District',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtDistrictID":temp_txtDistrictID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/District';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtDistrictID = document.getElementById('txtDistrictID').value;
		var txtName = document.getElementById('txtName').value;
		var slProvince = document.getElementById('slProvince').value;
	
		var dvDistrictID = document.getElementsByClassName('dvDistrictID');
		var dvName = document.getElementsByClassName('dvName');
		
		if(lParambtn == 'save'){
			$('#txtDistrictID').prop('disabled', false);
			}
			else{
			$('#txtDistrictID').prop('disabled', true);
			}
		
// 	    if(!txtDistrictID.match(/\S/)) {
// 	    	$("#txtDistrictID").focus();
// 	    	$('#dvDistrictID').addClass('has-error');
// 	    	$('#mrkDistrictID').show();
// 	        return false;
// 	    } 
	    if(!slProvince.match(/\S/)) {    	
	    	$('#slProvince').focus();
	    	$('#dvProvince').addClass('has-error');
	    	$('#mrkProvince').show();
	        return false;
	    } 
	    
	    if(!txtName.match(/\S/)) {    	
	    	$('#txtName').focus();
	    	$('#dvName').addClass('has-error');
	    	$('#mrkName').show();
	        return false;
	    } 
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/District',	
	        type:'POST',
	        data:{"key":lParambtn,"txtDistrictID":txtDistrictID,"txtName":txtName,"slProvince":slProvince},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertDistrict')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan district";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtDistrictID").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateDistrict')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui district";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/District';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}
	
//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var districtid = document.getElementById('txtDistrictID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (districtid == null || districtid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (districtid != null || districtid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>