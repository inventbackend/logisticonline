<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style>
  	input[type="text"],
  	input[type="password"],
	select.form-control {
	  background: transparent;
	  border: none;
	  border-bottom: 1px solid #005292;
	  -webkit-box-shadow: none;
	  box-shadow: none;
	  border-radius: 0;
	  font-family: Helvetica;
	}
	
	input[type="text"]:focus,
	input[type="password"]:focus,
	select.form-control:focus {
	  -webkit-box-shadow: none;
	  box-shadow: none;
	}
	
	.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    	background-color: #005292;
    	font-family: Roboto;
    	font-size: 14px;
    	font-weight:lighter;
    	width: 50%;
		margin-left: 25%;
		margin-right: 25%
	}
	
	a[href] {
		font-family: Roboto;
		font-size: 13px;
		font-weight: lighter; 
		color: #037E91;
		text-align:center;
		display:block;
		margin-top: 5px;
	}
  </style>
</head>
<body class="hold-transition login-page">
<form action="${pageContext.request.contextPath}/Login" method="post">

<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body" style="margin-top:20%">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" alt="Mountain View" style="width:100%;height:100%;"><br>
	<!-- <b>Logistic Online</b> -->
  </div>
  
  <div class="row">
		<div class="col-xs-12">
			<!--   error description -->
		  	<c:if test="${condition == 1}">
		      <div class="alert alert-danger alert-dismissible">
		         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		         <h4><i class="icon fa fa-ban"></i> Username or password incorrect</h4>
		      </div>
		    </c:if>
		    <c:if test="${condition == 2}">
		      <div class="alert alert-danger alert-dismissible">
		         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		         <h4><i class="icon fa fa-ban"></i> Your account has not been verified</h4>
		      </div>
		    </c:if>
		    <c:if test="${condition == 3}">
		      <div class="alert alert-danger alert-dismissible">
		         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		         <h4><i class="icon fa fa-ban"></i> Your account has been blocked</h4>
		      </div>
		    </c:if>
      	</div>
	</div>
      
	<!-- <p class="login-box-msg">Sign in to start your session</p> -->
	<div class="row">
		<div class="col-xs-12">
			<p class="pull-left" style="font-family: Helvetica;font-weight: bold; font-size: 18px; color: black;">Masuk Akun</p>
	      	<c:set var="condition" value="${condition}" ></c:set>
      	</div>
	</div>
	
	<div class="row" style="margin-top:10px">
		<div class="col-xs-12">
			 <div class="form-group">
			 	<label for="lblUserId" class="control-label" style="font-family: Roboto; font-size: 14px; font-weight:normal; color: black;">ID User</label>
				<!-- <input type="email" class="form-control" placeholder="Email" name="userId"> -->
		        <input type="text" class="form-control" style="color:black" name="userName" required>
				<!-- <span class="form-control-feedback"><img src="mainform/image/baseline_account_circle_black_18.png" width="75%" style="margin-top:5px"></span> -->
				<!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
		      </div>
		      <div class="form-group">
		      	<label for="lblPassword" class="control-label" style="font-family: Roboto; font-size: 14px; font-weight:normal; color: black;">Kata Sandi</label>
				<!-- <span class="form-control-feedback"><img src="mainform/image/baseline_lock_black_18.png" width="75%" style="margin-top:5px"></span> -->
		        <input type="password" class="form-control" style="color:black" name="password" required>
				<!--  <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
		      </div>
      	</div>
	</div>     
      
      <div class="row">
        <div class="col-xs-12">
        	<button type="submit" class="btn btn-primary btn-block btn-flat" name="btnLogin">Masuk Akun</button>
        	<a href="#">Lupa Kata Sandi?</a>
        	<label for="lblRegister" class="control-label" 
        		style="font-family: Roboto; font-size: 13px; font-weight:normal; color: black;
        				margin-top: 10px; text-align:center; display:block;">
        		Belum Memiliki Akun? <a href="GeneralRegister" style="display: inline">Daftar Sekarang</a>
        	</label>
			<!--           <div class="checkbox icheck"> -->
			<!--             <label> -->
			<!--               <input type="checkbox"> Remember Me -->
			<!--             </label> -->
			<!--           </div> -->
			<!-- 		<a href="Register" class="text-center" style="font-weight: bold; color: black;">Register as customer</a> -->
			<!-- 		<br> -->
			<!-- 		<a href="RegisterVendor" class="text-center" style="font-weight: bold; color: black;">Register as vendor</a> -->
        </div>
        <!-- /.col -->
        
		<!--         <div class="col-xs-2"> -->
		<!--         </div> -->
        <!-- /.col -->
        
		<!--         <div class="col-xs-4"> -->
		<!--         </div> -->
        <!-- /.col -->
      </div>

	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

</body>
</html>
