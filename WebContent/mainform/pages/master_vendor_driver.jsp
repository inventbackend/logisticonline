<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Trucker Driver</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Vendor_Driver" action = "${pageContext.request.contextPath}/VendorDriver" method="post">
<input  type="hidden" id="temp_userrole" name="temp_userrole" value="<c:out value="${userRole}"/>" />
<input  type="hidden" id="temp_vendorid" name="temp_vendorid" value="<c:out value="${vendorID}"/>" />

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Driver List
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
		
		<c:if test="${condition == 'SuccessInsertVendorDriver'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Insert success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteVendorDriver'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Delete success.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteVendorDriver'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> Add Driver</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
				  	<div id="dvVendorID" class="form-group">
		            <label for="lblVendorID" class="control-label">Vendor ID:</label><label id="mrkVendorID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtVendorID" name="txtVendorID" readonly="readonly">
		          	</div>
		          	<div id="dvDriverID" class="form-group">
		            <label for="lblDriverID" class="control-label">Driver ID:</label>
		            <input type="text" class="form-control" id="txtDriverID" name="txtDriverID" readonly="readonly">
		          	</div>
		          	<div id="dvDriverName" class="form-group">
		            <label for="lblDriverName" class="control-label">Driver Name:</label><label id="mrkDriverName" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDriverName" name="txtDriverName">
		            <input type="hidden" class="form-control" id="txtDriverNameHidden" name="txtDriverNameHidden">
		          	</div>
		          	<div id="dvDeviceID" class="form-group" style="display:none;">
		            <label for="lblDeviceID" class="control-label">Device ID:</label><label id="mrkDeviceID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDeviceID" name="txtDeviceID">
		          	</div>
		          	<div id="dvDriverUsername" class="form-group">
		            <label for="lblDriverUsername" class="control-label">Device Username:</label><label id="mrkDriverUsername" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDriverUsername" name="txtDriverUsername">
		          	</div>
		          	<div id="dvDriverPassword" class="form-group">
		            <label for="lblDriverPassword" class="control-label">Device Password:</label><label id="mrkDriverPassword" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDriverPassword" name="txtDriverPassword">
		          	</div>
		      </div>
		      <div class="modal-footer">	
		      	<button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>	        
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
	<!--modal show vendor data -->
		<div class="modal fade" id="ModalGetVendorID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendorID">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="ModalLabelVendorID"></h4>
					</div>
								<div class="modal-body">
    								<table id="tb_master_vendor" class="table table-bordered table-hover">
			        		<thead style="background-color: #d2d6de;">
				                <tr>
				                <th>Vendor ID</th>
				                <th>Vendor Name</th>
				                <th>Address</th>
								<th></th>
				                </tr>
			        		</thead>
			        		<tbody>
			        			<c:forEach items="${listVendor}" var ="vendor">
							        <tr>
							        <td><c:out value="${vendor.vendorID}"/></td>
							        <td><c:out value="${vendor.vendorName}"/></td>
							        <td><c:out value="${vendor.address}"/></td>
							        <td><button type="button" class="btn btn-primary"
						        			data-toggle="modal"
						        			onclick="FuncPassStringVendor('<c:out value="${vendor.vendorID}"/>','<c:out value="${vendor.vendorName}"/>')"
						        			data-dismiss="modal"
							        	><i class="fa fa-fw fa-check"></i></button>
							        </td>
					        		</tr>
			        			</c:forEach>
			        		</tbody>
		        		</table>
								</div>
				</div>
			</div>
		</div>
		<!-- /. end of modal show vendor data -->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Vendor Driver</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtVendorID" name="temp_txtVendorID"  />
						<input type="hidden" id="temp_txtDriverID" name="temp_txtDriverID"  />
									<p>Are you sure to delete this vendor vehicle driver ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_vendor_driver" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>	  
	                  <c:if test="${userRole eq 'R001'}">
	                  	<th>Vendor ID</th>
	                  	<th>Vendor Name</th>
	                  </c:if>              	
	                  <th>Driver ID</th>
	                  <th>Driver Name</th>
	                  <!-- <th>Device ID</th> -->
	                  <th>Username</th>
	                  <th>Password</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listVendorDriver}" var ="vendordriver">
		        	<tr>
		        	<c:if test="${userRole eq 'R001'}">
		        		<td><c:out value="${vendordriver.vendorID}"/></td>
		        		<td><c:out value="${vendordriver.vendorName}"/></td>
		        	</c:if>
					<td><c:out value="${vendordriver.driverID}"/></td>
					<td><c:out value="${vendordriver.driverName}"/></td>
					<%-- <td><c:out value="${vendordriver.deviceID}"/></td> --%>
					<td><c:out value="${vendordriver.driverUsername}"/></td>
					<td><c:out value="${vendordriver.driverPassword}"/></td>
					<td>
		        		<table>
							<td>
								<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								onclick="FuncButtonUpdate()"
								data-toggle="modal"
								data-target="#ModalUpdateInsert"
								data-lvendorid='<c:out value="${vendordriver.vendorID}"/>'
								data-ldriverid='<c:out value="${vendordriver.driverID}"/>'
								data-ldrivername='<c:out value="${vendordriver.driverName}"/>'
								data-ldeviceid='<c:out value="${vendordriver.deviceID}"/>' 
								data-lusername='<c:out value="${vendordriver.driverUsername}"/>'
								data-lpassword='<c:out value="${vendordriver.driverPassword}"/>'
								><i class="fa fa-edit"></i></button>
				        		
								<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" 
				        		data-lvendorid='<c:out value="${vendordriver.vendorID}"/>'
				        		data-ldriverid='<c:out value="${vendordriver.driverID}"/>'
				        		>
				        		<i class="fa fa-trash"></i>
				        		</button>
							</td>
							</table>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_vendor_driver').DataTable();
    $('#M002').addClass('active');
	$('#M017').addClass('active');
	
	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	
	$(":input").inputmask();
  	$("#txtDeviceID").inputmask({"mask": "**:**:**:**:**:**"});
  	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lVendorID = button.data('lvendorid');
		var lDriverID = button.data('ldriverid');
		$("#temp_txtVendorID").val(lVendorID);
		$("#temp_txtDriverID").val(lDriverID);
	})
	</script>

<!-- General Script -->
<script>
	function FuncClear(){
		$('#mrkVendorID').hide();
		$('#mrkDriverName').hide();
		$('#mrkDeviceID').hide();
		$('#mrkDriverUsername').hide();
		$('#mrkDriverPassword').hide();
		
		$('#dvVendorID').removeClass('has-error');
		$('#dvDriverName').removeClass('has-error');
		$('#dvDeviceID').removeClass('has-error');
		$('#dvDriverUsername').removeClass('has-error');
		$('#dvDriverPassword').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
	  	var modal = $(this)
	  	var roleid = document.getElementById('temp_userrole').value;
		if(roleid=='R001'){
			modal.find('.modal-body #txtVendorID').val(button.data('lvendorid'))
		}
		else{
			modal.find('.modal-body #txtVendorID').val(document.getElementById('temp_vendorid').value)
		}
	 	
	 	modal.find('.modal-body #txtDriverID').val(button.data('ldriverid'))
	 	modal.find('.modal-body #txtDriverName').val(button.data('ldrivername'))
	 	modal.find('.modal-body #txtDriverNameHidden').val(button.data('ldrivername'))
	 	modal.find('.modal-body #txtDeviceID').val(button.data('ldeviceid'))
	 	modal.find('.modal-body #txtDriverUsername').val(button.data('lusername'))
	 	modal.find('.modal-body #txtDriverPassword').val(button.data('lpassword'))
	})
	
	function FuncButtonNew() {
		$('#txtVendorID').prop('disabled', false);
		$('#txtDriverID').val('');
		$('#txtDriverName').val('');
		$('#txtDriverNameHidden').val('');
		$('#txtDeviceID').val('');
		$('#txtDriverUsername').val('');
		$('#txtDriverPassword').val('');
		//$('#txtVendorID').val(document.getElementById('temp_vendorid').value);
		$('#txtDriverName').prop('disabled', false);
	
		$('#btnSave').show();
		$('#btnUpdate').hide();
		$('#dvDriverID').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Vendor Driver";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		//$('#txtVendorID').val(document.getElementById('temp_vendorid').value);
		
		$('#txtVendorID').prop('disabled', true);
		$('#dvDriverID').show();
		$('#txtDriverName').prop('disabled', true);
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Vendor Driver';
	
		FuncClear();
	}
	
	function FuncDelete() {
		var temp_txtVendorID = document.getElementById('temp_txtVendorID').value;
		var temp_txtDriverID = document.getElementById('temp_txtDriverID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorDriver',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtVendorID":temp_txtVendorID,"temp_txtDriverID":temp_txtDriverID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/VendorDriver';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtVendorID = document.getElementById('txtVendorID').value;
		var txtDriverID = document.getElementById('txtDriverID').value;
		var txtDriverName = document.getElementById('txtDriverName').value;
		var txtDeviceID = document.getElementById('txtDeviceID').value;
		var txtDriverUsername = document.getElementById('txtDriverUsername').value;
		var txtDriverPassword = document.getElementById('txtDriverPassword').value;
		
	    if(!txtVendorID.match(/\S/)) {    	
	    	$('#txtVendorID').focus();
	    	$('#dvVendorID').addClass('has-error');
	    	$('#mrkVendorID').show();
	        return false;
	    } 
	    if(!txtDriverName.match(/\S/)) {    	
	    	$('#txtDriverName').focus();
	    	$('#dvDriverName').addClass('has-error');
	    	$('#mrkDriverName').show();
	        return false;
	    }
	     /* if(!txtDeviceID.match(/\S/)) {    	
	    	$('#txtDeviceID').focus();
	    	$('#dvDeviceID').addClass('has-error');
	    	$('#mrkDeviceID').show();
	        return false;
	    } */
	    if(!txtDriverUsername.match(/\S/)) {    	
	    	$('#txtDriverUsername').focus();
	    	$('#dvDriverUsername').addClass('has-error');
	    	$('#mrkDriverUsername').show();
	        return false;
	    }
	    if(!txtDriverPassword.match(/\S/)) {    	
	    	$('#txtDriverPassword').focus();
	    	$('#dvDriverPassword').addClass('has-error');
	    	$('#mrkDriverPassword').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorDriver',	
	        type:'POST',
	        data:{"key":lParambtn,"txtVendorID":txtVendorID,"txtDriverID":txtDriverID,"txtDriverName":txtDriverName,
	        	  "txtDeviceID":txtDeviceID,"txtDriverUsername":txtDriverUsername,"txtDriverPassword":txtDriverPassword},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertVendorDriver')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDriverName").focus();
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateVendorDriver')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDriverName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/VendorDriver';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
function FuncPassStringVendor(lParamVendorID,lParamVendorName){
		$("#txtVendorID").val(lParamVendorID);
	}
	
//on click event for txtVendorID
    $(document).on('click', '#txtVendorID', function(e){
    	e.preventDefault();
		var roleid = document.getElementById('temp_userrole').value;
		if(roleid=='R001'){
			 $('#ModalGetVendorID').modal('toggle');
		}
	 });
	 
	 	 
//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var drivernamedefault = document.getElementById('txtDriverNameHidden').value;
  event.preventDefault();
  if (event.keyCode === 13 && (drivernamedefault == null || drivernamedefault == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (drivernamedefault != null || drivernamedefault != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>