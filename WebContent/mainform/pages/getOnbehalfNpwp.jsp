<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table id="tb_onbehalf_npwp" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Customer</th>
			<th>Onbehalf</th>
			<th>Onbehalf Npwp</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listData}" var ="data">
			<tr>
				<td><c:out value="${data.customerName}" /></td>
				<td><c:out value="${data.onbehalfCustomer}" /></td>
				<td><c:out value="${data.onbehalfNpwp}" /></td>
				<td>
					<button
							type="button" class="btn btn-info" title="edit"
							data-toggle="modal"
							data-target="#ModalAddNpwp"
							data-lonbehalf='<c:out value="${data.onbehalfCustomer}"/>'
							data-lonbehalfnpwp='<c:out value="${data.onbehalfNpwp}"/>'
							><i class="fa fa-edit"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_onbehalf_npwp").DataTable({"aaSorting": [],"scrollX": true});
  	});
</script>