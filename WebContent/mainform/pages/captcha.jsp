<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*" import="com.logisticonline.CaptchaServiceServlet" import="com.logisticonline.CaptchaServlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
  

  CaptchaServiceServlet.generateToken(session);
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <title>Captcha Reader</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  </head>
  <body>
<c:if test="${showGoodResult}">
  <h1 style="color: green;">Your kung fu is good!</h1>
</c:if>
<c:if test="${showBadResult}">
  <h1 style="color: red;">This is not right. Try again!</h1>
</c:if>
    <p>Type in the word seen on the picture</p>
    <form method="POST" action="Captcha">
      <input id="kocak" name="captcha" type="text" autocomplete="off" />
      <input type="submit" />
    </form>
    <button type="button" class="btn btn-secondary" id="formTest">Secondary</button>
    <div id="img-captcha">
    	<img alt="captcha image" src="/LogisticOnline/Captcha-Service" />
    </div>
    
    <div class="progress active">
      <div id="progress-koja" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
        <span class="sr-only">40% Complete (success)</span>
      </div>
    </div>
    
    <button id="search" type="button" class="btn btn-primary">Suchen</button>
    <p><br/></p>
    <!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="mainform/plugins/iCheck/icheck.min.js"></script>
	<script>
	$('#search').on('click', function (e) {
		console.log("hai")
      })
      
    
	
	// 10 seconds
	
	
	function setProgressBarKoja(param) {
		$('#progress-koja').width(param+'%');
	}
	$(function () {
		//iCheck for checkbox and radio inputs
		
		
		$('#formTest').click(function(){
			var captcha = $('#kocak').val();
			var refreshprogessbar = setInterval(checkProgress, 10000);
			
			function checkProgress() {/*pass the actual id instead of 12345*/
			    $.getJSON('progressServlet?downloadId=12345', function(progress) {
			        if(progress == 100){clearInterval(refreshprogessbar);}
			        console.log(progress)
			        setProgressBarKoja(progress)
			        
			    });
			}
			/* $.ajax({
				url : '${pageContext.request.contextPath}/Captcha-Service',
				type : 'GET',
				success: function(data, textStatus ){
				       $('img-captcha').append('<img alt="captcha image" src="'+data+'" />')
			    },
			    fail: function(xhr, textStatus, errorThrown){
			       alert('request failed');
			    }
				
			}) */
		})
	});	
	</script>
  </body>
</html>