<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_dynamic_user" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Address</th>
			<th style="width: 20px"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listUser}" var ="user">
			<tr>
				<td><c:out value="${user.customerID}" /></td>
				<td><c:out value="${user.customerName}" /></td>
				<td><c:out value="${user.customerAddress}" /></td>
				<td><button type="button" class="btn btn-primary"
							data-toggle="modal"
							onclick="FuncPassStringUser('<c:out value="${user.customerID}"/>','<c:out value="${user.customerName}"/>')"
							data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_dynamic_user").DataTable();
  	});
</script>