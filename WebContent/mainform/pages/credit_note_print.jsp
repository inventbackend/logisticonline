<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  	<head>
    	<meta charset="UTF-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>LOGOL - Credit Note</title>

		<!-- Icon -->	
		<link rel="icon" href="mainform/image/logistic_icon3.jpg">

    	<!-- Bootstrap -->
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
		<!-- FontAwesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
		
		<style>
			body {
				color: rgba(0,0,0,0.7);
			}
			@media print {
			 	.print-hide {
					visibility: hidden;
			  	}
			}
		</style>

  	</head>
  	<body style="font-family: 'Roboto', sans-serif;line-height:1.4rem;">
    
    <c:set var="cn" value="${mdlCreditNote}"/>
    
		<div class="container" style="margin-top:2rem;">
			<div class="row" style="margin-bottom: -1rem;">
				<div class="col-2">
					<img src="mainform/image/LOGOL.svg" alt="LOGOL">
				</div>
				<div class="col-5">
					<p style="font-size:0.9rem;">
						<b>PT. LOGOL JAKARTA</b><br>
						Rukan Gading Bukit Indah Blok TB No.6,<br>
						Jl. Gading Kirana Raya, Kelapa Gading, 14240<br>
						021 453 4049
					</p>
				</div>
				<div class="col-5" align="right" style="margin-top: -1rem;">
					<button onclick="window.print();return false;" style="border-radius:5rem;color:rgba(0,0,0,0.5);box-shadow:0 1px 5px 0 rgba(0,0,0,0.5);padding: 0 0.5rem;" class="print-hide">
						<i class="fas fa-print mr-2"></i>Print
					</button>
					<h1 style="font-size:3rem;font-weight:700;color:#143F68;margin: 0.2rem 0 -0.3rem 0;">CREDIT NOTE</h1>
					<div style="font-weight:700;color:#143F68;"><c:out value='${cn.creditNoteNumber}'/></div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-6">
					<div style="color:rgba(0,0,0,0.5); font-weight:700;">To</div>
					<div style="color:#143F68;font-weight:700;"><c:out value='${cn.vendorName}'/></div>
					<p>
						<c:out value='${cn.address}'/>
						<br><c:out value='${cn.phone}'/>
						<br><c:out value='${cn.email}'/>
					</p>
				</div>
				<div class="col-2 ml-auto" style="font-weight:700;">
					<div style="color:rgba(0,0,0,0.5);">Order ID</div>
					<div style="color:#143F68;"><c:out value='${cn.orderManagementID}'/></div>
					<br>
					<div style="color:rgba(0,0,0,0.5);">Issued Date</div>
					<div style="color:#143F68;"><c:out value='${cn.date}'/></div>
					<br>
					<!-- <div style="color:rgba(0,0,0,0.5);">Due Date</div>
					<div style="color:#143F68;">19 Mar 2019</div> -->
				</div>
			</div>
			<hr>
			<div class="row" style="font-weight:700;padding:0.5rem 0;">
				<div class="col-2">
					<div style="color:rgba(0,0,0,0.5);">SI Number</div>
					<div style="color:#143F68;"><c:out value='${cn.siNumber}'/></div>
				</div>
				<c:if test="${fn:contains(cn.creditNoteNumber, 'IM')}">
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">B/L Number</div>
						<div style="color:#143F68;">1234567890</div>
					</div>
				</c:if>
				<div class="col-2">
					<div style="color:rgba(0,0,0,0.5);">Container Quantity</div>
					<div style="color:#143F68;"><c:out value='${cn.containerQty}'/></div>
				</div>
				<div class="col-2">
					<div style="color:rgba(0,0,0,0.5);">Vessel / Voyage</div>
					<div style="color:#143F68;"><c:out value='${cn.vesselVoyage}'/></div>
				</div>
				<div class="col-4">
					<div style="color:rgba(0,0,0,0.5);">Route</div>
					<div style="color:#143F68;"><c:out value='${cn.route}'/></div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-12">
					<table width="100%" class="table-striped" style="font-size:1rem;margin-top:-1rem;">
						<tbody>
							<tr>
								<td style="padding:1rem;">Trucking (Export / Import)</td>
								<td style="padding:1rem;"><c:out value='${cn.detailTruckingPrice}'/></td>
								<td align="right" style="padding:1rem;">
									<fmt:setLocale value="id_ID"/>
									<fmt:formatNumber value="${cn.truckingPrice}" type="currency"/>
								</td>
							</tr>
							<tr>
								<td style="padding:1rem;">PPh23 (2%)</td>
								<td></td>
								<td align="right" style="padding:1rem;">
									(<fmt:setLocale value="id_ID"/>
									<fmt:formatNumber value="${cn.pph23}" type="currency"/>)
								</td>
							</tr>
						</tbody>
					</table>
					<div style="background-color:#143F68;font-size:1rem;color:white;font-weight:700;padding:0.8rem 1rem;border-radius:5rem;margin-top:1rem;">
						<div class="row">
							<div class="col-2">
								Grand Total
							</div>
							<div class="col-10" align="right">
								<span class="ml-auto">
									<fmt:setLocale value="id_ID"/>
									<fmt:formatNumber value="${cn.grandTotal}" type="currency"/>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-12">
					<div style="color:rgba(0,0,0,0.5);font-weight:700;">Container Number</div>
					<div style="display:flex;flex-wrap:wrap;">
						<c:forEach items="${cn.containerNumber}" var="ctnumber">
							<div style="padding:0 1rem;"><c:out value='${ctnumber}'/></div>
						</c:forEach>
					</div>
					<br>
					<div style="color:rgba(0,0,0,0.5);font-weight:700;">Stuffing / Unstuffing Date</div>
					<div style="color:#143F68;font-weight:bold;margin-left:1rem;"><c:out value='${cn.stuffingDate}'/></div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-7">
					<div style="color:rgba(0,0,0,0.5);font-weight:700;">Remarks</div>
					<ul style="font-size:0.8rem;">
						<li>Official Receipt shall be issued upon settlement of this Credit Note.</li>
						<li>All cheques should be crossed and made payable to PT. LOGOL INDONESIA.</li>
						<li>All bank charges are under the account of the payer-remitter.</li>
						<li>Interest at rate of 3% per month from above amount being applied for any late payment.</li>
						<li>We only accept payment through our bank account, no cash payment allowed.</li>
						<li>Any discrepancy with this Crebit Note kindly contact our Account Dept within 7 days from date of invoice, otherwise all charges are deemed to be correct.</li>
					</ul>
				</div>
				<div class="col-3 ml-auto" align="center">
					<div style="margin:1rem 0 6rem 0;">Jakarta, <c:out value='${cn.date}'/></div>
					<b><c:out value='${cn.signatureName}'/></b>
				</div>
			</div>
		</div>
		
		
		
		<!-- Surat Jalan Div -->
		<c:forEach items="${listDO}" var="sj">
		
			<div class="container" style="margin-top:50rem;">
				<!-- <div class="row" style="margin-bottom: -1rem;">
					<div class="col-4 ml-auto" align="right" style="margin-top: -1rem;">
						<button onclick="window.print();return false;" style="border-radius:5rem;color:rgba(0,0,0,0.5);box-shadow:0 1px 5px 0 rgba(0,0,0,0.5);padding: 0 0.5rem;" class="print-hide">
							<i class="fas fa-print mr-2"></i>Print
						</button>
					</div>
				</div> -->
				<div class="row" style="margin-bottom:1rem;">
					<div class="col-5">
						<div style="border:1px solid #D2D6DE;padding:0.5rem 1rem 0.2rem 1rem;">
							<h5 style="color:#143F68;font-weight:bold;">SURAT JALAN #<c:out value="${sj.vendorOrderDetailID}" /></h5>
						</div>
						<div style="border:1px solid #D2D6DE;padding:1rem 1rem 0rem 1rem;">
							<div style="color:rgba(0,0,0,0.5);font-weight:700;">To</div>
							<div style="color:#143F68;font-weight:700"><c:out value="${sj.customer}" /></div>
							<p>
								<c:out value="${sj.address}" />
								<br><c:out value="${sj.phone}" />
							</p>
						</div>
					</div>
					<div class="col-4 ml-auto">
						<div style="margin-bottom:1rem;"><img src="mainform/image/LOGOL.svg" alt="LOGOL"></div>
						<p style="font-size:0.9rem;">
							<b>PT. LOGOL JAKARTA</b><br>
							Rukan Gading Bukit Indah Blok TB No.6,<br>
							Jl. Gading Kirana Raya, Kelapa Gading, 14240<br>
							021 453 4049
						</p>
					</div>
				</div>
				<hr>
				<div class="row" style="font-weight:700;margin: 1.5rem -1rem;">
					<div class="col-9" style="display:flex;flex-wrap:wrap;border-right:1px solid #D2D6DE;padding:0.5rem 1rem;">
						<div style="width:25%">
							<div style="color:rgba(0,0,0,0.5);">SI Number</div>
							<div style="color:#143F68;"><c:out value="${sj.shippingInstruction}" /></div>
							<br>
							<div style="color:rgba(0,0,0,0.5);">Container Number</div>
							<div style="color:#143F68;"><c:out value="${sj.containerNumber}" /></div>
						</div>
						<div style="width:25%">
							<div style="color:rgba(0,0,0,0.5);">Volume / Party</div>
							<div style="color:#143F68;"><c:out value="${sj.totalParty}" /></div>
							<br>
							<div style="color:rgba(0,0,0,0.5);">Seal Number</div>
							<div style="color:#143F68;"><c:out value="${sj.sealNumber}" /></div>
						</div>
						<div style="width:25%">
							<div style="color:rgba(0,0,0,0.5);">Vessel / Voyage</div>
							<div style="color:#143F68;"><c:out value="${sj.vesselVoyage}" /></div>
							<br>
							<div style="color:rgba(0,0,0,0.5);">Destination</div>
							<div style="color:#143F68;"><c:out value="${sj.port}" /></div>
						</div>
						<div style="width:25%">
							<div style="color:rgba(0,0,0,0.5);">Stuffing Date</div>
							<div style="color:#143F68;"><c:out value="${sj.stuffingDate}" /></div>
							
						</div>
					</div>
					<div class="col-3" style="display:flex;flex-wrap:wrap;padding:0.5rem 1rem;">
						<div style="width: 50%;">
							<div style="color:rgba(0,0,0,0.5);">License Plate</div>
							<div style="color:#143F68;"><c:out value="${sj.vehicleNumber}" /></div>
							<br>
							<div style="color:rgba(0,0,0,0.5);">Driver Name</div>
							<div style="color:#143F68;"><c:out value="${sj.driverName}" /></div>
						</div>
						<div style="width:50%; display:none;">
							<div style="color:rgba(0,0,0,0.5);">Contact Person</div>
							<div style="color:#143F68;">
								Lagertha
								<br>0812 3456 7890
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row" style="font-weight:700;">
					<div class="col-2">
						<!-- <div style="width:50%"> -->
						<div style="color:rgba(0,0,0,0.5);">Time In Depo</div>
						<div><c:out value="${sj.timeInDepo}" /></div>
						<!-- </div> -->
					</div>
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">Time Out Depo</div>
						<div><c:out value="${sj.timeOutDepo}" /></div>
					</div>
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">Time In Factory</div>
						<div><c:out value="${sj.timeInFactory}" /></div>
					</div>
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">Time Out Factory</div>
						<div><c:out value="${sj.timeOutFactory}" /></div>
					</div>
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">Time In Port</div>
						<div><c:out value="${sj.timeInPort}" /></div>
					</div>
					<div class="col-2">
						<div style="color:rgba(0,0,0,0.5);">Time Out Port</div>
						<div><c:out value="${sj.timeOutPort}" /></div>
					</div>
				</div>
				<hr>
				<div class="row" style="font-weight:700;">
					<div class="col-6" style="display:flex;flex-wrap:wrap;padding:0rem 1rem;">
						<div style="width:50%">
							<div style="color:rgba(0,0,0,0.5);">Commodity</div>
							<div><c:out value="${sj.commodity}" /></div>
						</div>
						<div style="width:50%">
							<div style="color:rgba(0,0,0,0.5);">Gross Weight</div>
							<div><c:out value="${sj.totalWeight}" /></div>
						</div>
					</div>
					<div class="col-6">
						<div style="color:rgba(0,0,0,0.5);">Quantity</div>
						<div><c:out value="${sj.quantity}" /></div>
					</div>
				</div>
				<hr>
				<br>
				<div class="row">
					<div class="col-3" align="center">
						<div style="margin:1rem 0 6rem 0;">Yang Menerima</div>
						<b><c:out value="${sj.pic}" /></b>
					</div>
					<div class="col-3 ml-auto" align="center">
						<div style="margin-bottom:-1rem;">Jakarta, <c:out value="${cn.date}" /></div>
						<div style="margin:1rem 0 6rem 0;">Yang Menyerahkan</div>
						<b><c:out value="${sj.driverName}" /></b>
					</div>
				</div>
			</div>
		</c:forEach>
	  
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		
	</body>
</html>
