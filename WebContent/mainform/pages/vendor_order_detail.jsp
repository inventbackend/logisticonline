<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trucker Driver Assignment</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<style type="text/css">	
	/*  css for loading bar */
	 .loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 42%;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}
	
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	/* end of css loading bar */
  </style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="Form_Vendor_Order_Detail" name="Form_Vendor_Order_Detail" action = "${pageContext.request.contextPath}/VendorOrderDetailList" method="post">
<input type="hidden" id="vendorOrderID" name="vendorOrderID" value="<c:out value="${vendorOrderID}"/>" />
<input type="hidden" id="vendorStatus" name="vendorStatus" value="<c:out value="${vendorStatus}"/>" />

	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Driver Assignment <br><br>
				<%-- 				<small style="color: black; font-weight: bold;">Customer : <c:out value="${customer}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">District : <c:out value="${district}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Depo : <c:out value="${depo}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Port : <c:out value="${port}"/></small> --%>
				<!-- 				<br> -->
				<%-- 				<small style="color: black; font-weight: bold;">Order ID : <c:out value="${vendorOrderID}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Order Status : <c:out value="${vendorStatusString}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Stuffing Date : <c:out value="${orderStuffingDate}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Item : <c:out value="${item}"/> || </small> --%>
				<%-- 				<small style="color: black; font-weight: bold;">Type : <c:out value="${orderType}"/></small> --%>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
					
					<div class="box-header with-border">
			  			<div class="row">
			  				<div class="form-group col-md-3">
 								<label class="control-label">Order</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${vendorOrderID}"/>">
							</div>
			  				<div class="form-group col-md-3">
 								<label class="control-label">Customer</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${customer}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">Factory</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${factory}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">Factory Address</label>	
 								<textarea class="form-control" readonly="readonly" row=3><c:out value="${address}"/></textarea>
							</div>
							<div class="form-group col-md-4">
								<label class="control-label">Stuffing Date</label>	
								<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
	 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${orderStuffingDate}"/>">
								</div>
							</div>
							<div class="form-group col-md-4">
 								<label class="control-label">SI No</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${noSI}"/>">
							</div>
							<div class="form-group col-md-4">
 								<label class="control-label">Order Type</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${orderType}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">District</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${district}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">Commodity</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${item}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">Depo</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${depo}"/>">
							</div>
							<div class="form-group col-md-3">
 								<label class="control-label">Port</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${port}"/>">
							</div>
							<div class="form-group col-md-6">
 								<label class="control-label">Container Quantity Order</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${vendorOrderQty}"/>">
							</div>
							<div class="form-group col-md-6">
 								<label class="control-label">Driver Assigned</label>	
 								<input type="text" class="form-control" readonly="readonly" value="<c:out value="${driverCount}"/>">
							</div>
			  			</div>
			  		</div>
			  		
						<div class="box-body">						
							<c:if test="${condition == 'SuccessInsertVendorOrderDetail'}">
								<div class="alert alert-success alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<h4><i class="icon fa fa-check"></i> Success</h4>
									Insert success.
								</div>
	      					</c:if> 
	      					
	      					<c:if test="${condition == 'SuccessUpdateVendorOrderDetail'}">
						 		<div class="alert alert-success alert-dismissible">
							      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										  	<h4><i class="icon fa fa-check"></i> Success</h4>
						   			  Update success.
						 		</div>
							</c:if>
							<c:if test="${condition == 'SuccessDeleteVendorOrderDetail'}">
							 	<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  	Delete success.
					 			</div>
							</c:if>
							<c:if test="${condition == 'FailedDeleteVendorOrderDetail'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Delete failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if>
	      					
	      						<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	      									<div id="dvloader" class="loader" style="display:none;"></div>
	      									
	          								<div id="dvVendorOrderDetailID" class="form-group">
	            								<label for="recipient-name" class="control-label">Trucker Order Detail ID :</label>	
	            								<input type="text" class="form-control" id="txtVendorOrderDetailID" name="txtVendorOrderDetailID" readonly disabled>
	          								</div>
	          								<div id="dvDriverID" class="form-group">
	          									<label for="lblDriverID" class="control-label">Driver :</label><label id="mrkDriverID" for="lbl-validation" class="control-label"><small>*</small></label>
									            <select id="slDriverID" name="slDriverID" class="form-control select2" data-placeholder="Select Driver" style="width: 100%;">
													<c:forEach items="${listDriver}" var="driver">
														<option value="<c:out value="${driver.driverID}" />"><c:out value="${driver.driverName}" /></option>
													</c:forEach>
									        	</select>
	          								</div>
	          								<div id="dvVehicleNumber" class="form-group">
	          									<label for="lblVehicleNumber" class="control-label">Vehicle Number :</label><label id="mrkVehicleNumber" for="lbl-validation" class="control-label"><small>*</small></label>
									            <select id="slVehicleNumber" name="slDriverID" class="form-control select2" data-placeholder="Select Vehicle Number - Tid Number" style="width: 100%;">
													<c:forEach items="${listVehicle}" var="vehicle">
														<option value="<c:out value="${vehicle.vehicleNumber}" />"><c:out value="${vehicle.vehicleNumber}" /> - <c:out value="${vehicle.tidNumber}" /></option>
													</c:forEach>
									        	</select>
	          								</div>
	          								<div id="dvContainerNumber" class="form-group">
	            								<label for="recipient-name" class="control-label">Container Number :</label>	
	            								<input type="text" class="form-control" id="txtContainerNumber" name="txtContainerNumber" placeholder="AAAA9999999" title="4 huruf kapital diikuti 7 angka">
	          								</div>
	          								<div id="dvSealNumber" class="form-group">
	            								<label for="recipient-name" class="control-label">Seal Number :</label>	
	            								<input type="text" class="form-control" id="txtSealNumber" name="txtSealNumber">
	          								</div>
      								</div>
      								
      								<div class="modal-footer">
      										<c:if test="${globalUserRole ne 'R001' && allowAdd}">
      											<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
      										</c:if>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
        									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Assignment</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtVendorOrderID" name="temp_txtVendorOrderID"  />
						<input type="hidden" id="temp_txtVendorOrderDetailID" name="temp_txtVendorOrderDetailID"  />
						<input type="hidden" id="temp_txtDriverID" name="temp_txtDriverID"  />
									<p>Are you sure to delete this assignment ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
							<div class="row">
							<!-- 								<div class="col-md-2"> -->
							<%-- 									<label style="color: black; font-weight: bold;">Order Quantity : <c:out value="${vendorOrderQty}"/></label> --%>
							<!-- 								</div> -->
							<!-- 								<div class="col-md-2"> -->
							<%-- 									<label style="color: black; font-weight: bold;">Driver Registered : <c:out value="${driverCount}"/></label> --%>
							<!-- 	      						</div> -->
			      					
								<c:if test="${globalUserRole ne 'R001' && allowAdd}">
										<div class="col-md-1 pull-right">
											<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" 
														data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()">
														<i class="fa fa-plus-circle"></i> Assign Driver</button>
										</div>
								</c:if>
							</div>
							
	      					<br>
							<table id="tb_vendor_order_detail" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Trucker Order Detail</th>
										<th>Driver</th>
										<th>Vehicle Number</th>
										<th>Status</th>
										<th>Cancellation Reason</th>
										<th>Container Number</th>
										<th>Seal Number</th>
										<th>Gatepass</th>
										<th style="width: 60px"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listVendorOrderDetail}" var="orderdetail">
										<tr>
											<td><c:out value="${orderdetail.vendorOrderDetailID}" /></td>
											<td><c:out value="${orderdetail.driverName}" /></td>
											<td><c:out value="${orderdetail.vehicleNumber}" /></td>
											<td>
												<c:if test="${orderdetail.vendorDetailStatus == 0}">Assigned</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 1}">On Process</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 2}">On Delivery</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 5}">On Factory</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 6}">On Depo</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 7}">Depo Checkout</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 3}">On Port</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 4}">Delivered</c:if>
												<c:if test="${orderdetail.vendorDetailStatus == 10}">Cancelled</c:if>
											</td>
											<td><c:out value="${orderdetail.cancellationReason}" /></td>
											<td><c:out value="${orderdetail.containerNumber}" /></td>
											<td><c:out value="${orderdetail.sealNumber}" /></td>
											<td><c:out value="${orderdetail.yellowcard}" /></td>
											<td>
											<c:if test="${orderdetail.vendorDetailStatus ne '10' 
														&& orderdetail.vendorDetailStatus ne '4' 
														&& globalUserRole ne 'R001'}">
													<button
														type="button" class="btn btn-info"
														data-toggle="modal"
														onclick="FuncButtonUpdate('<c:out value="${orderdetail.vendorDetailStatus}"/>')" 
														data-target="#ModalUpdateInsert"
														data-lvendororderdetailid='<c:out value="${orderdetail.vendorOrderDetailID}"/>'
														data-ldriverid='<c:out value="${orderdetail.driverID}"/>'
														data-lvehiclenumber='<c:out value="${orderdetail.vehicleNumber}"/>' 
														data-lcontainernumber='<c:out value="${orderdetail.containerNumber}"/>'
														data-lsealnumber='<c:out value="${orderdetail.sealNumber}"/>'
														><i class="fa fa-edit"></i></button>
											</c:if>
											<c:if test="${vendorStatus eq '0' && globalUserRole ne 'R001'}">
													<button type="button" class="btn btn-danger" data-toggle="modal" 
											        		data-target="#ModalDelete" 
											        		data-lvendororderid='<c:out value="${vendorOrderID}"/>'
											        		data-lvendororderdetailid='<c:out value="${orderdetail.vendorOrderDetailID}"/>'
											        		data-ldriverid='<c:out value="${orderdetail.driverID}"/>'
											        		>
											        		<i class="fa fa-trash"></i>
											        </button>
												</c:if>
									        </td>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- InputMask -->
	<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
	<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
		
		$('#M018').addClass('active');
		$('#M020').addClass('active');
  		$("#tb_vendor_order_detail").DataTable();
  		$("#dvErrorAlert").hide();
  		
  		$(":input").inputmask();
  		$("#txtContainerNumber").inputmask({"mask": "aaaa9999999"});
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function(){
		FuncButtonNew(), 
		$('#ModalUpdateInsert').modal('show')
	});
	</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
	
 		var button = $(event.relatedTarget);
 		var lvendororderdetailid = button.data('lvendororderdetailid');
 		var ldriverid = button.data('ldriverid');
 		var lvehiclenumber = button.data('lvehiclenumber');
 		var lcontainernumber = button.data('lcontainernumber');
 		var lsealnumber = button.data('lsealnumber');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtVendorOrderDetailID").val(lvendororderdetailid);
 		modal.find(".modal-body #slDriverID").val(ldriverid).trigger("change");
 		modal.find(".modal-body #slVehicleNumber").val(lvehiclenumber).trigger("change");
 		modal.find(".modal-body #txtContainerNumber").val(lcontainernumber);
 		modal.find(".modal-body #txtSealNumber").val(lsealnumber);
    
 		$('#slDriverID').focus();
	})
	
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lvendororderid = button.data('lvendororderid');
		var lvendororderdetailid = button.data('lvendororderdetailid');
		var ldriverid = button.data('ldriverid');
		$("#temp_txtVendorOrderID").val(lvendororderid);
		$("#temp_txtVendorOrderDetailID").val(lvendororderdetailid);
		$("#temp_txtDriverID").val(ldriverid);
	});
	
</script>

<script>
function FuncClear(){
	jQuery('#mrkDriverID').hide();
	jQuery('#mrkVehicleNumber').hide();
	
	jQuery('#dvDriverID').removeClass('has-error');
	jQuery('#dvVehicleNumber').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	$('#dvVendorOrderDetailID').hide();
	$('#dvContainerNumber').hide();
	$('#dvSealNumber').hide();
	
	$('#txtVendorOrderDetailID').val('');
	$('#slDriverID').val('').trigger("change");
	$('#slVehicleNumber').val('').trigger("change");
 	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Assign Driver";
	
	FuncClear();
}

function FuncButtonUpdate(lParamStatus) {
	$('#dvVendorOrderDetailID').show();
	$('#dvContainerNumber').show();
	$('#dvSealNumber').show();
	
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Driver Assignment';
	
	FuncClear();
	
	if(lParamStatus == '0'){
		$("#dvDriverID").show();
		/* $("#dvVehicleNumber").show(); */
	}else{
		$("#dvDriverID").hide();
		/* $("#dvVehicleNumber").hide(); */
	}
}

function FuncValEmptyInput(lParambtn) {
	$('#dvloader').show();
	
	var txtVendorOrderID = jQuery('#vendorOrderID').val();
	var txtVendorOrderDetailID = jQuery('#txtVendorOrderDetailID').val();
	var slDriverID = jQuery('#slDriverID').val();
	var slVehicleNumber = jQuery('#slVehicleNumber').val();
	var txtContainerNumber = jQuery('#txtContainerNumber').val();
	var txtSealNumber = jQuery('#txtSealNumber').val();;
	
    if(!slDriverID.match(/\S/)) {
    	$('#slDriverID').focus();
    	$('#dvDriverID').addClass('has-error');
    	$('#mrkDriverID').show();
        return false;
    }
    if(!slVehicleNumber.match(/\S/)) {
    	$('#slVehicleNumber').focus();
    	$('#dvVehicleNumber').addClass('has-error');
    	$('#mrkVehicleNumber').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/VendorOrderDetailList',	
        type:'POST',
        data:{"key":lParambtn,"txtVendorOrderID":txtVendorOrderID,"txtVendorOrderDetailID":txtVendorOrderDetailID,
        	  "slDriverID":slDriverID, "slVehicleNumber":slVehicleNumber, "txtContainerNumber":txtContainerNumber, 
        	   "txtSealNumber":txtSealNumber},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertVendorOrderDetail')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Insert failed";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtStuffingDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateVendorOrderDetail')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Update failed";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				$("#txtStuffingDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		$('#dvloader').hide();
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/VendorOrderDetailList';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
            $('#dvloader').hide();
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<script>
// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}
</script>

</body>
</html>