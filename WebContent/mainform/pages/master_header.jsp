<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
//allow access only if session exists
String username = null;
username = (String) session.getAttribute("username");
if(session.getAttribute("user") == null || session.getAttribute("userrole") == null){
	response.sendRedirect("/LogisticOnline/Login");
}else username = (String) session.getAttribute("username");
%>

  <header class="main-header">

    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b style="color:#003156">Logol</b></span>
      <!-- logo for regular state and mobile dev	ices -->
      <span class="logo-lg">
	  <b style="color:#003156">Logistic Online</b>
      </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="mainform/dist/img/320X320 XXXDPI_user.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><%=username %></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="mainform/dist/img/320X320 XXXDPI_user.png" class="img-circle" alt="User Image">

                <p>
                <%=username %>
<!--                   Alexander Pierce - Web Developer -->
<!--                   <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
<!--                 <div class="pull-left"> -->
<!--                   <a href="#" class="btn btn-default btn-flat">Profile</a> -->
<!--                 </div> -->
                
                <form name="Form_Logout" action = "${pageContext.request.contextPath}/Logout" method="post">
                <div class="pull-right">
                  <input type="submit" class="btn btn-default btn-flat" value="Log out" >
<!--                   <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                </div>
                </form>
                
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
			<!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- Sidebar user panel -->
      <div class="user-panel">
      	<img src="mainform/image/logo.png" style="width:200px;height:100px;"><br><br>
      	
        <div class="pull-left image">
          <img src="mainform/dist/img/Group 4291@2x.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p style="color:#003156"><%=username %></p>
          <a style="color:#FF0000"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
		<!--  <li class="header">MAIN NAVIGATION</li> -->
		<li class="header"></li>
        
        <li id="M001" style="display:none"><a href="Dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        
		 <li id="M010" class="treeview" style="display:none">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M010_1" style="display:none"><a href="OrderManagement"><i class="fa fa-circle-o"></i> Export</a></li>
            <li id="M010_2" style="display:none"><a href="#"><i class="fa fa-circle-o"></i> Import</a></li>
          </ul>
        </li>
         
         <li id="M022" style="display:none"><a href="Gatepass"><i class="fa fa-plus-circle"></i> <span>PEB, NPE, Gatepass</span></a></li>
         
         <li id="M018" class="treeview" style="display:none">
          <a href="#">
            <i class="fa fa-group"></i>
            <span><label id="lblM018">Trucker</label></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="M019" style="display:none"><a href="VendorPickOrder?date=today"><i class="fa fa-circle-o"></i> Pick Order</a></li>
            <li id="M020" style="display:none"><a href="VendorOrderList?date=today"><i class="fa fa-circle-o"></i> Order Management</a></li>
          </ul>
        </li>
         
         <li id="M023" style="display:none"><a href="DebitNote?date=today"><i class="fa fa-list-alt"></i> <span>Debit Note</span></a></li>
         <li id="M024" style="display:none"><a href="CreditNote?date=today"><i class="fa fa-list-alt"></i> <span>Credit Note</span></a></li>
         
         <li id="M002" class="treeview" style="display:none">
          <a href="#">
            <i class="fa fa-gear"></i>
            <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li id="MenuCustomerProfile" style="display:none"><a href="Customer"><i class="fa fa-circle-o"></i> <span>Profile</span></a></li>
          	<li id="M003" style="display:none"><a href="Customer"><i class="fa fa-circle-o"></i> Customer</a></li>
          	<li id="M008" style="display:none"><a href="ContainerType"><i class="fa fa-circle-o"></i> Container Type</a></li>
           	<li id="M005" style="display:none"><a href="Depo"><i class="fa fa-circle-o"></i>Depo</a></li>
           	<li id="M013" style="display:none"><a href="District"><i class="fa fa-circle-o"></i> <span>District</span></a></li>
           	<li id="M021" style="display:none"><a href="News"><i class="fa fa-circle-o"></i> <span>News</span></a></li><!--  <i class="fa fa-reorder"></i> -->
           	<li id="M014" style="display:none"><a href="Port"><i class="fa fa-circle-o"></i> <span>Port</span></a></li>
           	<li id="M004" style="display:none"><a href="Rate"><i class="fa fa-circle-o"></i> Rate</a></li>
           	<li id="M011" style="display:none"><a href="Role"><i class="fa fa-circle-o"></i> <span>Role</span></a></li> <!-- <i class="fa fa-user-plus"> </i>-->
          	<li id="M006" style="display:none"><a href="Shipping"><i class="fa fa-circle-o"></i>Shipping</a></li>
          	<li id="M012" style="display:none"><a href="Tier"><i class="fa fa-circle-o"></i> <span>Tier</span></a></li>
          	<li id="M007" style="display:none"><a href="TOP"><i class="fa fa-circle-o"></i>TOP</a></li>
            <li id="M015" style="display:none"><a href="Vendor"><i class="fa fa-circle-o"></i> <span>Trucker Profile</span></a></li>
            <li id="M016" style="display:none"><a href="VendorVehicle"><i class="fa fa-circle-o"></i> <span>Trucker Vehicle</span></a></li>
            <li id="M017" style="display:none"><a href="VendorDriver"><i class="fa fa-circle-o"></i> <span>Trucker Driver</span></a></li>
            <li id="M009" style="display:none"><a href="User"><i class="fa fa-circle-o"></i> <span>User</span></a></li> <!-- <i class="fa fa-check-square-o"></i> -->
          </ul>
        </li>

		<li><a href="Logout"><i class="fa fa-sign-out"></i> <span>Log Out</span></a></li>
		
		<!--         <li class="treeview"> -->
		<!--           <a href="#"> -->
		<!--             <i class="fa fa-users"></i> -->
		<!--             <span>Customers</span> -->
		<!--             <span class="pull-right-container"> -->
		<!--               <i class="fa fa-angle-left pull-right"></i> -->
		<!--             </span> -->
		<!--           </a> -->
		<!--           <ul class="treeview-menu"> -->
		<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Customers List</a></li> -->
		<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Feedback</a></li> -->
		<!--           </ul> -->
		<!--         </li> -->
		<!--         <li class="treeview"> -->
		<!--           <a href="#"> -->
		<!--             <i class="fa fa-briefcase"></i> -->
		<!--             <span>Employees</span> -->
		<!--             <span class="pull-right-container"> -->
		<!--               <i class="fa fa-angle-left pull-right"></i> -->
		<!--             </span> -->
		<!--           </a> -->
		<!--           <ul class="treeview-menu"> -->
		<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Staff</a></li> -->
		<!--             <li><a href="#"><i class="fa fa-circle-o"></i> Permission</a></li> -->
		<!--           </ul> -->
		<!--         </li> -->
		<!--         <li><a href="mainform/pages/dashboard.jsp"><i class="fa fa-shopping-cart"></i> <span>Outlets</span></a></li> -->
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  


 
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script>

$(function () {
	//get restricted menu
	$.ajax({
	    url: '${pageContext.request.contextPath}/site_master',
	    type: 'GET',
		//data: {"key":"RestrictedMenu"},
		data: {"key":"AllowedMenu"},
	    dataType: 'json'
	})
		.done(function(data){
	    console.log(data);
		
	    //for json data type
	    for (var i in data) {
	      var obj = data[i];
	      var index = 0;
	      var menuid;
	      var roleid;
	      for (var prop in obj) {
	          switch (index++) {
	              case 0:
	                  menuid = obj[prop];
	                  break;
	              case 1:
	              	  roleid = obj[prop];
	              	  break;
	              default:
	                  break;
	          }
	      }
		        
			//$("#"+menuid).hide();
			if(menuid=='M003' && roleid!='R001'){
				$("#MenuCustomerProfile").show();
				$("#M002").show();
			}
			else if(menuid=='M018' && roleid!='R001'){
				document.getElementById("lblM018").innerHTML = "Order";
				$("#"+menuid).show();
			}
			else{
				$("#"+menuid).show();
			}
		}           
	   })
	   .fail(function(){
	  	 console.log('Service call failed!');
	   });
  
 return true;
})

</script>