<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<table id="tb_master_factory" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Factory ID</th>
			<th>Name</th>
<!-- 			<th>PIC</th> -->
<!-- 			<th>Office Phone</th> -->
<!-- 			<th>Mobile Phone</th> -->
<!-- 			<th>Province</th> -->
			<th>Address</th>
			<th>District</th>
<!-- 			<th>Tier</th> -->
			<th style="width: 20px"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listFactory}" var ="factory">
			<tr>
				<td><c:out value="${factory.factoryID}" /></td>
				<td><c:out value="${factory.factoryName}" /></td>
<%-- 				<td><c:out value="${factory.pic}" /></td> --%>
<%-- 				<td><c:out value="${factory.officePhone}" /></td> --%>
<%-- 				<td><c:out value="${factory.mobilePhone}" /></td> --%>
<%-- 				<td><c:out value="${factory.province}" /></td> --%>
				<td><c:out value="${factory.address}" /></td>
				<td><c:out value="${factory.districtName}" /></td>
<%-- 				<td><c:out value="${factory.tierName}" /></td> --%>
				<td><button type="button" class="btn btn-primary"
							data-toggle="modal"
							onclick="FuncPassStringFactory('<c:out value="${factory.factoryID}"/>',
															'<c:out value="${factory.factoryName}"/>',
															'<c:out value="${factory.address}"/>')"
							data-dismiss="modal"><i class="fa fa-fw fa-check"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
// 		$("#tb_master_factory").DataTable({"scrollX": true});
		//$("#tb_master_factory").DataTable();
  	});
</script>