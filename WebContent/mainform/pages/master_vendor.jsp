<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Trucker</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <style type="text/css">	
	/*  css for loading bar */
	 .loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 45%;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}
	
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	/* end of css loading bar */
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Vendor" action = "${pageContext.request.contextPath}/Vendor" method="post">
<input type="hidden" id="tempRole" value="<c:out value="${userRole}"/>" />
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Trucker
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
		
		<c:if test="${condition == 'SuccessUpdateVendor'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Update success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteVendor'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Delete success.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteVendor'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
		      
		      	  <!-- MainInfoPanel -->
      			  <label for="recipient-name" class="control-label">Main Info</label>
				  <div class="panel panel-primary" id="MainInfoPanel">
				  <div class="panel-body fixed-panel">
				  
				  	<div id="dvVendorID" class="form-group">
		            <label for="lblVendorID" class="control-label">Trucker ID:</label>
		            <input type="text" class="form-control" id="txtVendorID" name="txtVendorID" readonly disabled>
		          	</div>
		          	<div id="dvVendorName" class="form-group col-xs-6">
		            <label for="lblVendorName" class="control-label">Name:</label><label id="mrkVendorName" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtVendorName" name="txtVendorName">
		          	</div>
		          	<div id="dvPIC" class="form-group col-xs-6">
		            <label for="lblPIC" class="control-label">PIC:</label><label id="mrkPIC" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtPIC" name="txtPIC">
		          	</div>
		          	<div id="dvAddress" class="form-group col-xs-12">
		            <label for="lblAddress" class="control-label">Address:</label><label id="mrkAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtAddress" name="txtAddress">
		          	</div>
		          	<div id="dvEmail" class="form-group col-xs-4">
		            <label for="lblEmail" class="control-label">Email:</label><label id="mrkEmail" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtEmail" name="txtEmail">
		          	</div>
		          	<div id="dvProvince" class="form-group has-feedback col-xs-4">
				      <label class="control-label">Province:</label><label id="mrkProvince" for="lbl-validation" class="control-label"><small>*</small></label>
				       <select id="slProvince" name="slProvince" class="form-control select2" style="width: 100%;">
							<c:forEach items="${listProvince}" var="province">
								<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
							</c:forEach>
				       	</select>
				    </div>
		          	<div id="dvPostalCode" class="form-group col-xs-4">
		            <label for="lblPostalCode" class="control-label">Postal Code:</label><label id="mrkPostalCode" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtPostalCode" name="txtPostalCode">
		          	</div>
		          	<div id="dvOfficePhone" class="form-group col-xs-6">
		            <label for="lblOfficePhone" class="control-label">Office Phone:</label><label id="mrkOfficePhone" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtOfficePhone" name="txtOfficePhone">
		          	</div>
		          	<div class="form-group col-xs-6">
		            <label for="lblMobilePhone" class="control-label">Mobile Phone:</label>
		            <input type="text" class="form-control" id="txtMobilePhone" name="txtMobilePhone">
		          	</div>
		          	<div id="dvGarageAddress" class="form-group col-xs-4">
		            <label for="lblGarageAddress" class="control-label">Garage Address:</label><label id="mrkGarageAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtGarageAddress" name="txtGarageAddress">
		          	</div>
		          	<div id="dvNPWP" class="form-group col-xs-4">
		            <label for="lblNPWP" class="control-label">NPWP:</label><label id="mrkNPWP" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtNPWP" name="txtNPWP">
		          	</div>
		          	<div id="dvSIUJPT" class="form-group col-xs-4">
		            <label for="lblSIUJPT" class="control-label">SIUJPT:</label><label id="mrkSIUJPT" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtSIUJPT" name="txtSIUJPT">
		          	</div>
		          	
				  </div>
				  </div>
				  <!-- /.MainInfoPanel -->
				  
				  <!-- BankingInfoPanel -->
				  <label for="recipient-name" class="control-label">Billing Info</label>
				  <div class="panel panel-primary" id="BillingInfoPanel">
				  <div class="panel-body fixed-panel">
				  	
			        <div id="dvBankName" class="form-group col-xs-4">
			      	<label for="lblBankName" class="control-label">Bank Name:</label><label id="mrkBankName" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankName" name="txtBankName">
			        </div>
			        <div id="dvBankAcc" class="form-group col-xs-4">
			      	<label for="lblBankAcc" class="control-label">Bank Account:</label><label id="mrkBankAcc" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankAcc" name="txtBankAcc">
			        </div>
			        <div id="dvBankBranch" class="form-group col-xs-4">
			      	<label for="lblBankBranch" class="control-label">Bank Branch:</label><label id="mrkBankBranch" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankBranch" name="txtBankBranch">
			        </div>
				  		
				  </div>
				  </div>
				  <!-- /.BankingInfoPanel -->
		          
		          <div class="row"></div>
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Customer</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtVendorID" name="temp_txtVendorID"  />
									<p>Are you sure to delete this trucker ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		<!--modal rate management -->
		<div class="modal fade bs-example-modal-lg" id="ModalRateManagement" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">			
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalRateManagement" name="lblTitleModalRateManagement">Rate Management</label></h4>	
			</div>
		    <div class="modal-body">
		    	<input type="hidden" id="txtTempVendorID" name="txtTempVendorID">
				<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsertRate"><i class="fa fa-plus-circle"></i> Add Final Rate</button><br><br>		 
		      	  <div id="dvloaderRateManagement" class="loader"></div>
		      	  <div id="dynamic-content-rate-management">
		      	  </div>
			</div>
			</div>
			</div>
		</div>
		
		
		<!--modal update insert customer rate management -->
		<div class="modal fade" id="ModalUpdateInsertRate" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			
			<div id="dvErrorAlertRateManagement" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				<label id="lblAlertRateManagement"></label>. <label id="lblAlertRateManagementDescription"></label>.
			</div>
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label>Form Rate Management</label></h4>	
			</div>
		    <div class="modal-body">
		    	<div id="dvDistrict" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">District</label><label id="mrkDistrict" for="recipient-name" class="control-label"><small>*</small></label>	
					<select id="slDistrict" name="slDistrict" class="form-control select2" style="width: 100%;">
				 	</select>
				</div>
				<div id="dvContainerType" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Container Type</label><label id="mrkContainerType" for="recipient-name" class="control-label"><small>*</small></label>	
					<select id="slContainerType" name="slContainerType" class="form-control select2" style="width: 100%;">
				 	</select>
				</div>
				<div id="dvFinalRate" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Final Rate</label><label id="mrkFinalRate" for="recipient-name" class="control-label"><small>*</small></label>	
					<input class="form-control number" id="txtFinalRate" name="txtFinalRate">
					<!-- <input type="hidden" id="txtDistrictID2" name="txtDistrictID2"> -->
				</div>
  				<div class="row"></div>
			</div>
			
			<div class="modal-footer">
				<c:if test="${userRole eq 'R001'}">
					<button type="button" class="btn btn-primary" id="btnSaveRate" name="btnSaveRate" onclick="FuncProccessRate('saveFinalRate','R001')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateRate" name="btnUpdateRate" onclick="FuncProccessRate('updateFinalRate','R001')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			</div>
			</div>
			</div>
		</div>	
        

        <table id="tb_master_vendor" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th style="width: 120px"></th>	                	
	                  <th>ID</th>
	                  <th>Name</th>
	                  <th>PIC</th>
	                  <th>Email</th>
	                  <th>Province</th>
	                  <th>Address</th>
	                  <th>Postal</th>
	                  <th>Phone</th>
	                  <th>Garage Address</th>
	                  <th>NPWP</th>
	                  <th>SIUJPT</th>
	                  <th>Bank</th>
	                  <th>Account</th>
	                  <th>Branch</th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listVendor}" var ="vendor">
		        	<tr>
		        	<td>
		        		<table>
							<td>
								<button style="display: <c:out value="${showDetailButtonStatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lvendorid='<c:out value="${vendor.vendorID}"/>'
								data-lvendorname='<c:out value="${vendor.vendorName}"/>' 
								data-lpic='<c:out value="${vendor.PIC}"/>'
								data-lemail='<c:out value="${vendor.email}"/>' 
								data-lprovince='<c:out value="${vendor.province}"/>'
								data-laddress='<c:out value="${vendor.address}"/>'
								data-lpostalcode='<c:out value="${vendor.postalCode}"/>'
								data-lofficephone='<c:out value="${vendor.officePhone}"/>'
								data-lmobilephone='<c:out value="${vendor.mobilePhone}"/>'
								data-lgarageaddress='<c:out value="${vendor.garageAddress}"/>'
								data-lnpwp='<c:out value="${vendor.NPWP}"/>'
								data-lsiujpt='<c:out value="${vendor.SIUJPT}"/>'
								data-lbankname='<c:out value="${vendor.bankName}"/>' 
								data-lbankacc='<c:out value="${vendor.bankAccount}"/>'
								data-lbankbranch='<c:out value="${vendor.bankBranch}"/>'
								><i class="fa fa-edit"></i></button>
							</td>
							<td>&nbsp</td>
							<td>
								<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-lvendorid='<c:out value="${vendor.vendorID}"/>'>
				        		<i class="fa fa-trash"></i>
				        		</button>
							</td>
							<td>&nbsp</td>
							<td>
								<c:if test="${userRole eq 'R001'}">
						        	<button
										type="button" class="btn btn-warning" title="rate management"
										data-toggle="modal"
										data-target="#ModalRateManagement"
										data-lvendorid='<c:out value="${vendor.vendorID}"/>'
										><i class="fa fa-dollar"></i></button>
						        </c:if>
					        </td>
					        <td>&nbsp</td>
							<td>
								<c:if test="${showDetailButtonStatus == 'none'}">
									<button style="display: <c:out value="${showDetailButtonStatus}"/>" type="button" id="btnShowVendorTier" name="btnShowVendorTier"  class="btn btn-default"><i class="fa fa-list-ul"></i></button>
								</c:if>
								<c:if test="${showDetailButtonStatus != 'none'}">
									<a href="${pageContext.request.contextPath}/VendorTier?vendorid=<c:out value="${vendor.vendorID}" />&vendorname=<c:out value="${vendor.vendorName}" />"><button type="button" id="btnShowVendorTier" name="btnShowVendorTier"  class="btn btn-default" title="show trucker tier"><i class="fa fa-list-ul"></i></button></a>
								</c:if>
							</td>
							</table>
			        </td>
			        <td><c:out value="${vendor.vendorID}"/></td>
		        	<td><c:out value="${vendor.vendorName}"/></td>
					<td><c:out value="${vendor.PIC}"/></td>
					<td><c:out value="${vendor.email}"/></td>
					<td><c:out value="${vendor.province}"/></td>
					<td><c:out value="${vendor.address}"/></td>
					<td><c:out value="${vendor.postalCode}"/></td>
					<td><c:out value="${vendor.officePhone}"/></td>
					<td><c:out value="${vendor.garageAddress}"/></td>
					<td><c:out value="${vendor.NPWP}"/></td>
					<td><c:out value="${vendor.SIUJPT}"/></td>
					<td><c:out value="${vendor.bankName}"/></td>
					<td><c:out value="${vendor.bankAccount}"/></td>
					<td><c:out value="${vendor.bankBranch}"/></td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_vendor').DataTable({
    	"aaSorting": [],
		"scrollX": true
    });
    $('#M002').addClass('active');
	$('#M015').addClass('active');
	
	var userRole = document.getElementById('tempRole').value;
	if(userRole == 'R001'){
		//shortcut for button 'new'
    	Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
    	});
	}
	
	//Initialize Select2 Elements
	$(".select2").select2();
	$('#slProvince').val('').trigger("change");
	
	$(":input").inputmask();
  	$("#txtOfficePhone").inputmask({"mask": "999[9]-99999999", "placeholder": ""});
  	
  	$('#dvloaderRateManagement').hide();
	$("#dvErrorAlert").hide();
	$('#dvErrorAlertRateManagement').hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lVendorID = button.data('lvendorid');
		$("#temp_txtVendorID").val(lVendorID);
	})
</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var modal = $(this)
	  modal.find('.modal-body #txtVendorID').val(button.data('lvendorid'))
	  modal.find('.modal-body #txtVendorName').val(button.data('lvendorname'))
	  modal.find('.modal-body #txtPIC').val(button.data('lpic'))
	  modal.find('.modal-body #txtAddress').val(button.data('laddress'))
	  modal.find('.modal-body #txtEmail').val(button.data('lemail'))
	  
	  if(button.data('lofficephone') != undefined && button.data('lofficephone').split("-")[0].length == 3)
	  		$("#txtOfficePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
	  else
	  		$("#txtOfficePhone").inputmask({"mask": "9999-999999999999", "placeholder": ""});
	  modal.find('.modal-body #txtOfficePhone').val(button.data('lofficephone'))
	  
	  modal.find('.modal-body #txtMobilePhone').val(button.data('lmobilephone'))
	  modal.find('.modal-body #txtGarageAddress').val(button.data('lgarageaddress'))
	  modal.find('.modal-body #txtNPWP').val(button.data('lnpwp'))
	  modal.find('.modal-body #txtSIUJPT').val(button.data('lsiujpt'))
	  modal.find('.modal-body #txtBankName').val(button.data('lbankname'))
	  modal.find('.modal-body #txtBankAcc').val(button.data('lbankacc'))
	  modal.find('.modal-body #txtBankBranch').val(button.data('lbankbranch'))
	  modal.find('.modal-body #slProvince').val(button.data('lprovince')).trigger("change")
	  modal.find('.modal-body #txtPostalCode').val(button.data('lpostalcode'))
	  
	  $("#txtVendorName").focus();
	})
	
	
	//Modal Rate Management
	$('#ModalRateManagement').on('shown.bs.modal', function (event) {
		$('#dvloaderRateManagement').show();
		$('#dynamic-content-rate-management').html('');
		
 		var button = $(event.relatedTarget);
 		var lVendorID = button.data('lvendorid');
 		$('#txtTempVendorID').val(lVendorID);
 		
 		var modal = $(this);
 		
 		$.ajax({
	          url: '${pageContext.request.contextPath}/getVendorRateManagement',
	          type: 'POST',
	          data: {"vendorid":lVendorID},
	          dataType: 'html'
	     })
	     .done(function(data){
	          // console.log(data); 
	          //$('#dynamic-content-rate-management').html(''); // blank before load.
	          $('#dynamic-content-rate-management').html(data); // load here
	          $('#dvloaderRateManagement').hide();
	     })
	     .fail(function(){
	          $('#dynamic-content-rate-management').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     	  $('#dvloaderRateManagement').hide();
	     });
	})
	
	
	//Modal Update Insert Rate Management
	$('#ModalUpdateInsertRate').on('shown.bs.modal', function (event) {
		$('#dvErrorAlertRateManagement').hide();
		FuncClear();
		
 		var button = $(event.relatedTarget);
 		var lFinalRate = button.data('lfinalrate');
 		var lVendorID = document.getElementById("txtTempVendorID").value;
 		var lDistrict = button.data('ldistrictid')
 		var lContainerType = button.data('lcontainertypeid')
 		
 		var modal = $(this);
 		
 		if(lFinalRate == undefined){
 			$("slDistrict").prop("disabled", false);
 			$("slContainerType").prop("disabled", false);
 			$('#btnSaveRate').show();
 			$('#btnUpdateRate').hide();
 		}
 		else{
 			$("slDistrict").prop("disabled", true);
 			$("slContainerType").prop("disabled", true);
	 		$('#btnSaveRate').hide();
 			$('#btnUpdateRate').show();
 		}
	 		
 		modal.find(".modal-body #txtFinalRate").val(lFinalRate);
 		
 		$('#slContainerType').find('option').remove();
 		$('#slDistrict').find('option').remove();
 		
 		//get container type
 		$.ajax({
	          url: '${pageContext.request.contextPath}/getContainerType',
	          type: 'POST',
	          data: {},
	          dataType: 'json'
	     })
	     .done(function(data){
	          // console.log(data);
	           
	          //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slContainerType').append("<option value=\"" + key + "\">" + val + "</option>");
		    }
		    
		    $("#slContainerType").val(lContainerType).trigger("change");
	     })
	     .fail(function(){
	     	console.log('Service call failed!');
	     });
	     
	     
	     //get district
	     $.ajax({
	          url: '${pageContext.request.contextPath}/GetDistrict',
	          type: 'POST',
	          data: {"provinceid":'getAll'},
	          dataType: 'json'
	     })
	     .done(function(data){
	          // console.log(data);
	           
	          //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
		    }
		    
		    $("#slDistrict").val(lDistrict).trigger("change");
	     })
	     .fail(function(){
	     	console.log('Service call failed!');
	     });
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>
	function FuncClear(){
		$('#mrkVendorName').hide();
		$('#mrkPIC').hide();
		$('#mrkAddress').hide();
		$('#mrkEmail').hide();
		$('#mrkOfficePhone').hide();
		$('#mrkGarageAddress').hide();
		$('#mrkNPWP').hide();
		$('#mrkSIUJPT').hide();
		$('#mrkBankName').hide();
		$('#mrkBankAcc').hide();
		$('#mrkBankBranch').hide();
		$('#mrkProvince').hide();
		$('#mrkPostalCode').hide();	
		$('#mrkDistrict').hide();	
		$('#mrkContainerType').hide();	
		$('#mrkFinalRate').hide();	
		
		$('#dvVendorName').removeClass('has-error');
		$('#dvPIC').removeClass('has-error');
		$('#dvAddress').removeClass('has-error');
		$('#dvEmail').removeClass('has-error');
		$('#dvOfficePhone').removeClass('has-error');
		$('#dvGarageAddress').removeClass('has-error');
		$('#dvNPWP').removeClass('has-error');
		$('#dvSIUJPT').removeClass('has-error');
		$('#dvBankName').removeClass('has-error');
		$('#dvBankAcc').removeClass('has-error');
		$('#dvBankBranch').removeClass('has-error');
		$('#dvProvince').removeClass('has-error');
		$('#dvPostalCode').removeClass('has-error');
		$('#dvDistrict').removeClass('has-error');
		$('#dvContainerType').removeClass('has-error');
		$('#dvFinalRate').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
		$('#dvErrorAlertRateManagement').hide();
	}
	
	function FuncButtonNew() {
		$('#dvVendorID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Trucker";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		$('#dvVendorID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Trucker';
	
		FuncClear();
	}
	
	function FuncDelete() {
		var temp_txtVendorID = document.getElementById('temp_txtVendorID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Vendor',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtVendorID":temp_txtVendorID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/Vendor';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtVendorID = document.getElementById('txtVendorID').value;
		var txtVendorName = document.getElementById('txtVendorName').value;
		var txtPIC = document.getElementById('txtPIC').value;
		var txtAddress = document.getElementById('txtAddress').value;
		var txtEmail = document.getElementById('txtEmail').value;
		var txtOfficePhone = document.getElementById('txtOfficePhone').value;
		var txtMobilePhone = document.getElementById('txtMobilePhone').value;
		var txtGarageAddress = document.getElementById('txtGarageAddress').value;
		var txtNPWP = document.getElementById('txtNPWP').value;
		var txtSIUJPT = document.getElementById('txtSIUJPT').value;
		var txtBankName = document.getElementById('txtBankName').value;
		var txtBankAcc = document.getElementById('txtBankAcc').value;
		var txtBankBranch = document.getElementById('txtBankBranch').value;
		var slProvince = document.getElementById('slProvince').value;
		var txtPostalCode = document.getElementById('txtPostalCode').value;
		
	    if(!txtVendorName.match(/\S/)) {    	
	    	$('#txtVendorName').focus();
	    	$('#dvVendorName').addClass('has-error');
	    	$('#mrkVendorName').show();
	        return false;
	    } 
	    if(!txtPIC.match(/\S/)) {    	
	    	$('#txtPIC').focus();
	    	$('#dvPIC').addClass('has-error');
	    	$('#mrkPIC').show();
	        return false;
	    }
	    if(!txtAddress.match(/\S/)) {    	
	    	$('#txtAddress').focus();
	    	$('#dvAddress').addClass('has-error');
	    	$('#mrkAddress').show();
	        return false;
	    } 
	    if(!txtEmail.match(/\S/)) {    	
	    	$('#txtEmail').focus();
	    	$('#dvEmail').addClass('has-error');
	    	$('#mrkEmail').show();
	        return false;
	    }
	    if(!txtOfficePhone.match(/\S/)) {    	
	    	$('#txtOfficePhone').focus();
	    	$('#dvOfficePhone').addClass('has-error');
	    	$('#mrkOfficePhone').show();
	        return false;
	    }
	    if(!txtGarageAddress.match(/\S/)) {    	
	    	$('#txtGarageAddress').focus();
	    	$('#dvGarageAddress').addClass('has-error');
	    	$('#mrkGarageAddress').show();
	        return false;
	    }
	    if(!txtNPWP.match(/\S/)) {    	
	    	$('#txtNPWP').focus();
	    	$('#dvNPWP').addClass('has-error');
	    	$('#mrkNPWP').show();
	        return false;
	    }
	    if(!txtSIUJPT.match(/\S/)) {    	
	    	$('#txtSIUJPT').focus();
	    	$('#dvSIUJPT').addClass('has-error');
	    	$('#mrkSIUJPT').show();
	        return false;
	    } 
	    if(!txtBankName.match(/\S/)) {    	
	    	$('#txtBankName').focus();
	    	$('#dvBankName').addClass('has-error');
	    	$('#mrkBankName').show();
	        return false;
	    }
	    if(!txtBankAcc.match(/\S/)) {    	
	    	$('#txtBankAcc').focus();
	    	$('#dvBankAcc').addClass('has-error');
	    	$('#mrkBankAcc').show();
	        return false;
	    }
	    if(!txtBankBranch.match(/\S/)) {    	
	    	$('#txtBankBranch').focus();
	    	$('#dvBankBranch').addClass('has-error');
	    	$('#mrkBankBranch').show();
	        return false;
	    }
	    if(!slProvince.match(/\S/)) {    	
	    	$('#slProvince').focus();
	    	$('#dvProvince').addClass('has-error');
	    	$('#mrkProvince').show();
	        return false;
	    }
	    if(!txtPostalCode.match(/\S/)) {    	
	    	$('#txtPostalCode').focus();
	    	$('#dvPostalCode').addClass('has-error');
	    	$('#mrkPostalCode').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Vendor',	
	        type:'POST',
	        data:{"key":lParambtn,"txtVendorID":txtVendorID,"txtVendorName":txtVendorName,"txtPIC":txtPIC,
	        	"txtAddress":txtAddress,"txtEmail":txtEmail,"txtOfficePhone":txtOfficePhone,"txtMobilePhone":txtMobilePhone,
	        	"txtNPWP":txtNPWP,"txtSIUJPT":txtSIUJPT,"txtGarageAddress":txtGarageAddress,
	        	"txtBankName":txtBankName,"txtBankAcc":txtBankAcc,"txtBankBranch":txtBankBranch,
	        	"slProvince":slProvince,"txtPostalCode":txtPostalCode
	        	},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertVendor')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert trucker failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtVendorName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateVendor')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update trucker failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtVendorName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Vendor';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
//set tab as click
 	$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slProvince:focus').length) {
    	$('#slProvince').click();
    }
	});
	
// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var vendorid = document.getElementById('txtVendorID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (vendorid == null || vendorid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (vendorid != null || vendorid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});


//function save and update customer rate management
function FuncProccessRate(lParambtn,lParamRole) {
	/* if(lParamRole == 'R001'){
		var txtCustomerID = document.getElementById('txtCustomerID').value;
	}
	else if(lParamRole == 'R002'){
		var txtCustomerID = document.getElementById('txtID').value;
	} */
	
	var txtVendorID = document.getElementById('txtTempVendorID').value;
	var slDistrict = document.getElementById('slDistrict').value;
	var slContainerType = document.getElementById('slContainerType').value;
	var txtFinalRate = document.getElementById('txtFinalRate').value;
    
    if(!slDistrict.match(/\S/)) {
    	$("#slDistrict").focus();
    	$('#dvDistrict').addClass('has-error');
    	$('#mrkDistrict').show();
        return false;
    } 
    
    if(!slContainerType.match(/\S/)) {
    	$("#slContainerType").focus();
    	$('#dvContainerType').addClass('has-error');
    	$('#mrkContainerType').show();
        return false;
    } 
    
    if(!txtFinalRate.match(/\S/)) {
    	$("#txtFinalRate").focus();
    	$('#dvFinalRate').addClass('has-error');
    	$('#mrkFinalRate').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/VendorRate',	
        type:'POST',
        data:{"key":lParambtn,"slDistrict":slDistrict,"txtVendorID":txtVendorID,
        	"txtFinalRate":txtFinalRate, "slContainerType":slContainerType},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertFinalRate')
        	{
        		$("#dvErrorAlertRateManagement").show();
        		document.getElementById("lblAlertRateManagement").innerHTML = "Insert failed";
        		document.getElementById("lblAlertRateManagementDescription").innerHTML = data.split("--")[1];
        		$("#ModalUpdateInsertRate").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateFinalRate')
        	{
        		$("#dvErrorAlertFinalRate").show();
        		document.getElementById("lblAlertRateManagement").innerHTML = "Update failed";
        		document.getElementById("lblAlertRateManagementDescription").innerHTML = data.split("--")[1];
        		$("#ModalUpdateInsertRate").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
        		$('#ModalUpdateInsertRate').modal('toggle');
        		
        			$('#dvloaderRateManagement').show();
        			$('#dynamic-content-rate-management').html(''); // leave this div blank
     
				     $.ajax({
				          url: '${pageContext.request.contextPath}/getVendorRateManagement',
				          type: 'POST',
				          data: {"vendorid":txtVendorID},
				          dataType: 'html'
				     })
				     .done(function(data){
				          // console.log(data);
				          $('#dynamic-content-rate-management').html(''); // blank before load.
				          $('#dynamic-content-rate-management').html(data); // load here
				          $('#dvloaderRateManagement').hide();
				     })
				     .fail(function(){
				          $('#dynamic-content-rate-management').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
     	  				  $('#dvloaderRateManagement').hide();
				     });
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

</body>
</html>