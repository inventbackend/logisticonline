<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<link rel="icon" href="mainform/image/logistic_icon3.jpg">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Debit Note</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  	<link rel="stylesheet" href="mainform/plugins/iCheck/all.css">
	
	<style type="text/css">	
		  #ModalOrderDetail,#ModalUpdateInsert { overflow-y:scroll }
		
			/*  css for loading bar */
		 .loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 50px;
		  height: 50px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		  margin-left: 45%;
		}
		
		.nopadding {
		   padding: 0 !important;
		   margin: 0 !important;
		}
		
		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}
		
		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
		/* end of css loading bar */
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Form_Debit_Note" name="Form_Debit_Note" action = "${pageContext.request.contextPath}/DebitNote" method="post">
	<input type="hidden" id="temp_string" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Debit Note</h1>
			</section>
			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<c:if test="${condition == 'SuccessConfirm'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Sukses konfirmasi pembayaran.
			   				</div>
						</c:if>
						
						<c:if test="${condition == 'SuccessConfirmPartialPayment'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Sukses konfirmasi pembayaran parsial.
			   				</div>
						</c:if>
						
						<div id="dvSearch" class="row">	
		<div class="col-md-2">
		<label for="message-text" class="control-label">Status</label>
			<select id="slStatusSearch" name="slStatus" class="form-control">
				<c:if test="${status eq 'Unpaid'}">
					<option selected>Unpaid</option>
					<option>Paid</option>
					<option value=''>All</option>
				</c:if>
				<c:if test="${status eq 'Paid'}">
					<option>Unpaid</option>
					<option selected>Paid</option>
					<option value=''>All</option>
				</c:if>
				<c:if test="${status eq ''}">
					<option>Unpaid</option>
					<option>Paid</option>
					<option value='' selected>All</option>
				</c:if>
       		</select>
       	</div>
		 <!-- /.col-md-2 -->
		 
		 <div class="col-md-2">
			<label for="message-text" class="control-label">Invoice Date From</label>
			<input type="text" class="form-control" id="txtStuffingDateFrom" name="txtStuffingDateFrom" value="<c:out value="${stuffingDateFrom}"/>">
       	</div>
		<!-- /.col-md-2 -->
		<div class="col-md-2">
			<label for="message-text" class="control-label">Invoice Date To</label>
			<input type="text" class="form-control" id="txtStuffingDateTo" name="txtStuffingDateTo" value="<c:out value="${stuffingDateTo}"/>">
     	</div>
		<!-- /.col-md-2 -->
		
		<div class="col-md-2">
		<button id="btnSearch" name="btnSearch" type="button" class="btn btn-primary" onclick="FuncButtonSearch('search')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-2 -->
		</div>
		<!-- /.row -->
		
		<!--modal update & Insert partial and confirm-->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
							
			<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	       				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	       				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	       				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
							</div>
						
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Payment Confirmation</h4>	
			</div>
				<div class="modal-body">
					<div id="dvloader" class="loader" style="display:none;"></div>
	
								<div class="row">
									<div id="idBodyFormPaymentPaid" class="form-group col-md-12">
									</div>
								</div>
						</div>
						<div class="modal-footer">
							<button id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="savePaymentConfirmation()"><i class="fa fa-check"></i> Confirm</button>
								<button type="button" class="btn btn-danger" onclick="closeModal()"><i class="fa fa-remove"></i> Cancel</button>
						</div>
					</div>
				</div>
			</div>
       	
      					<!--modal update & Insert -->
						<!-- <div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel">Payment Confirmation</h4>	
									</div>
									
	   								<div class="modal-body">
	   									loading bar
										<div id="dvloader" class="loader" style="display:none;"></div>
				
	   									<div class="row">
	   										<div id="dvPaymentDate" class="form-group col-md-12">
	         									<label for="lblPaymentDate" class="control-label">Payment Date</label><label id="mrkPaymentDate" for="recipient-name" class="control-label"></label>	
	         									<input type="text" class="form-control" id="txtPaymentDate" name="txtPaymentDate">							                    
		       								</div>
	   									</div>
	  								</div>
	  								<div class="modal-footer">
	  									<button id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncSave('save')"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div> -->
						
						
						<!--modal partial payment -->
						<div class="modal fade" id="ModalPartialPayment" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlertPartialPayment" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlertPartialPayment"></label>. <label id="lblAlertPartialPaymentDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
									</div>
									
	   								<div class="modal-body">
	   									<!-- loading bar -->
										<div id="dvloaderPartialPayment" class="loader" style="display:none;"></div>
				
	   									<div class="row">
	   										<div id="dvPaymentDate2" class="form-group col-md-12">
	         									<label for="lblPaymentDate2" class="control-label">Payment Date</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>	
	         									<input type="text" class="form-control" id="txtPaymentDate2" name="txtPaymentDate2">							                    
		       								</div>
		       								<div id="dvAmountPaid" class="form-group col-md-12">
	         									<label for="lblAmountPaid" class="control-label">Amount Paid</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>	
	         									<input class="form-control number" id="txtAmountPaid" name="txtAmountPaid">							                    
		       								</div>
		       								<input type="hidden" class="form-control" id="txtDebitNoteNumber" name="txtDebitNoteNumber">
		       								<input type="hidden" class="form-control" id="txtAmount" name="txtAmount">
	   									</div>
	  								</div>
	  								<div class="modal-footer">
	  									<button id="btnSavePartialPayment" name="btnSavePartialPayment" value="btnSavePartialPayment" type="button" class="btn btn-primary" onclick="FuncSavePartialPayment('SavePartialPayment')"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div>
						
						
						<!--modal invoice setting -->
						<div class="modal fade" id="ModalInvoiceSetting" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog modal-md" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlertInvoiceSetting" class="alert alert-danger alert-dismissible" style="display: none;">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlertInvoiceSetting"> </label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalInvoiceSetting" name="lblTitleModalInvoiceSetting"></label></h4>	
									</div>
									
	   								<div class="modal-body">
	   									<!-- loading bar -->
										<div id="dvloaderPartialPayment" class="loader" style="display:none;"></div>
										
	   									<div class="row">
	   										<div class="col-md-12">
		   										<div id="dvPaymentDate2" class="form-group col-md-8">
		   											<input type="hidden" class="form-control" id="txtDebitNoteNumberAdd" name="txtDebitNoteNumberAdd">	
		   											<label for="lblPaymentDate2" class="control-label">Status Invoice</label>
	         										<select id="statusIncoice" name="statusIncoice" class="form-control select2" style="width: 100%;" readonly="readonly">
														<option value="true">Final</option>
														<option value="false">Performance</option>
									        		</select>							                    
			       								</div>
		   									</div>
		   									<div class="col-md-12">
	   											<div class="col-md-12">
	   												<label for="lblPaymentDate2" class="control-label">Final Rate</label>
	   											</div>
	   											
	   										</div>
	   										<div class="col-md-12" id="divDebitNoteDetail">
		   										
		   									</div>
	   										<div class="col-md-12">
		   										<div id="dvPaymentDate2" class="form-group col-md-8">
		   											<label for="lblPaymentDate2" class="control-label">Custom Clearance Final</label>
	         										<input type="text" class="form-control number " id="txtCustomClearanceFinal" name="txtCustomClearanceFinal">							                    
			       								</div>
		   									</div>
	   										<div class="col-md-12">
		   										<div id="dvPaymentDate2" class="form-group col-md-4">
		   											<label for="lblPaymentDate2" class="control-label">PPN</label>
	         										<input type="text" class="form-control" id="txtPPN" name="txtPPN">							                    
			       								</div>
			       								<div id="dvPaymentDate2" class="form-group col-md-4">
			       									<label for="lblPaymentDate2" class="control-label">PPh23</label>	
		         									<input type="text" class="form-control" id="txtPPh23" name="txtPPh23">							                    
			       								</div>
		   									</div>
		   									<div class="col-md-12">
		   										<div id="dvPaymentDate2" class="form-group col-md-8">
		   											<label for="lblPaymentDate2" class="control-label">Additional Price</label>
	         										<input type="text" class="form-control number" id="txtAdditionalPrice" name="txtAdditionalPrice">							                    
			       								</div>
		   									</div>
	   										<div class="col-md-12">
	   											<div class="col-md-12">
	   												<label for="lblPaymentDate2" class="control-label">Reimburse</label>
	   											</div>
	   											
	   										</div>
	   										<div id="reimburse">
	   										
	   										</div>
	   										<div class="col-md-12">
		   										<div id="dvPaymentDate2" class="form-group col-md-4">
	         										<select id="txtReimburseNameAdd" name="txtReimburseNameAdd" class="form-control select2" style="width: 100%;" readonly="readonly">
														
									        		</select>								                    
			       								</div>
			       								<div id="dvPaymentDate2" class="form-group col-md-4">	
		         									<input type="text" class="form-control number" id="txtReimburseAmountAdd" name="txtReimburseAmountAdd">							                    
			       								</div>
			       								<div id="dvPaymentDate2" class="form-group col-md-4">
		         									<button id="btnSavePartialPayment" name="btnSavePartialPayment" value="btnSavePartialPayment" type="button" class="btn btn-success" onclick="addReimburse()"><i class="fa fa-plus"></i> </button>							                    
			       								</div>
		   									</div>
	   										
		   									
	   									</div>
		  							</div>
	  								<div class="modal-footer">
	  									<button id="btnSavePartialPayment" name="btnSavePartialPayment" value="btnSavePartialPayment" type="button" class="btn btn-primary" onclick="FuncUpdateSetting()"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div>
							
							
						<!--modal order detail -->
						<div class="modal fade bs-example-modal-lg" id="ModalOrderDetail" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalOrderDetail" name="lblTitleModalOrderDetail"></label></h4>	
									</div>
									
	   								<div class="modal-body">
		   								<div class="row">
		   									<div class="form-group col-md-6">
		         								<label class="control-label">Factory ID</label>
		         								<small><label id="lblFactoryName" name="lblFactoryName" class="control-label"></label></small>
		         								<input type="text" class="form-control" id="txtFactoryID" name="txtFactoryID" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-6">
		         								<label class="control-label">Factory Address</label>	
		         								<textarea class="form-control" id="txtFactoryAddress" name="txtFactoryAddress" readonly="readonly" row=3></textarea>
		       								</div>
		       								<div class="form-group col-md-4">
		         								<label class="control-label">SI No</label>	
		         								<input type="text" class="form-control" id="txtShippingInvoiceID" name="txtShippingInvoiceID" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-4">
		         								<label class="control-label">Shipping Line</label>												
		         								<input type="text" class="form-control" id="txtShippingName" name="txtShippingName" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-4">
		         								<label class="control-label">Commodity</label>	
		         								<input type="text" class="form-control" id="txtItem" name="txtItem" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-3">
		         								<label class="control-label">DO Number</label>	
		         								<input type="text" class="form-control" id="txtDeliveryOrderID" name="txtDeliveryOrderID" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-3">
		         								<label class="control-label">Vessel Name</label>	
		         								<input type="text" class="form-control" id="txtVesselName" name="txtVesselName" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-3">
		         								<label class="control-label">Voyage No</label>	
		         								<input type="text" class="form-control" id="txtVoyageNo" name="txtVoyageNo" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-3">
		         								<label class="control-label">Port</label>	
		         								<input type="text" class="form-control" id="txtPortName" name="txtPortName" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-4">
	         									<label class="control-label">Depo</label>	
		         								<input type="text" class="form-control" id="txtDepoName" name="txtDepoName" readonly="readonly">
		       								</div>
		       								<div class="form-group col-md-4">
		         								<label class="control-label">Order Type</label>	
		         								<select id="slOrderType" name="slOrderType" class="form-control select2" style="width: 100%;" readonly="readonly">
													<option value="EXPORT">EXPORT</option>
													<option value="IMPORT">IMPORT</option>
									        	</select>
		       								</div>
		       								<div class="form-group col-md-4">
		         								<label class="control-label">Tier</label>	
		         								<select id="slTier" name="slTier" class="form-control select2" style="width: 100%;" readonly="readonly">
													<c:forEach items="${listTier}" var="tier">
														<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.tierID}" /> - <c:out value="${tier.description}" /></option>
													</c:forEach>
											    </select>
		       								</div>
		       								<div class="form-group col-md-12">
									          <label class="control-label">Vendor:</label>	
									       	  	<select id="sVendor" name="sVendor" class="form-control select2" style="width: 100%;" multiple="multiple" readonly="readonly">
													<c:forEach items="${listVendor}" var="vendor">
														<option value="<c:out value="${vendor.vendorID}" />"><c:out value="${vendor.vendorName}" /></option>
													</c:forEach>
											    </select>
									        </div>
		       							</div>
	  								</div>
									<!-- end of order header body -->
	  								
	  								<!-- loader -->
	  								<div id="dvloaderSchedule" class="loader" style="display:none;"></div>
	  								
									<!-- schedule section -->
	  								<div class="modal-body">
	  									<!-- SchedulePanel -->
						      			<label for="recipient-name" class="control-label">Load/Unload Scheduling</label>
										<div class="panel panel-primary">
										<div class="panel-body fixed-panel">
			  								<!-- DataPanel -->
			  								<label for="recipient-name" class="control-label">Schedule</label>
				  							<div class="panel panel-primary">
											<div class="panel-body fixed-panel">
												<div id="dynamic-content-schedule"></div>
											</div>
											</div>
											<!-- end of DataPanel -->
										</div>
										</div>
										<!-- end of MainPanel -->
	  								</div>
	  								<!-- end of order detail body -->
	  								<div class="modal-footer">
										<button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
	  								</div>
									<!-- end of order detail footer -->
									
								</div>
								<!-- end of modal content -->
							</div>
						</div>
						
						
						<!--modal show vendor data -->
						<div class="modal fade" id="ModalVendor" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendor">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="ModalLabelVendor"></h4>	
									</div>
									<div class="modal-body">
										<div id="dynamic-content-vendor"></div>
									</div>
								</div>
							</div>
						</div>
						<!-- /. end of modal show vendor data -->
						<br><br>
						
						<c:if test="${globalUserRole eq 'R001'}">
							<button id="btnPayment" name="btnPayment" type="button" class="btn btn-success pull-left" data-toggle="modal"
								style="display:none;"
								data-target="#ModalUpdateInsert"
								>
								<span class="glyphicon glyphicon-check"></span>
								CONFIRM PAYMENT (<label id="lblTotalPayment"></label>)
							</button>
							<div id="newLine" style="display:none"><br><br><br></div>
						</c:if>
							
							<table id="tb_debit_note" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr style="background-color: #003156; color:white;
											font-family: Roboto; font-size: 16px; font-weight: bold;
											">
										<th colspan="2">Total Amount Unpaid</th>
										<th colspan="11" style="text-align: center;">
											<fmt:setLocale value="id_ID"/>
											<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
										</th>
									</tr>
									<tr>
										<th style="display: <c:out value="${buttonstatus}"/>"></th>
										<c:if test="${globalUserRole eq 'R001'}">
											<th>Customer</th>
										</c:if>
										<th>Order</th>
										<th>Invoice Number</th>
										<th>Invoice Date</th>
										<th>Amount</th>
										<th>Amount Paid</th>
										<th>Amount Balance</th>
										<th>Status</th>
										<th>Payment Date</th>
										<th>Overdue</th>
										<c:if test="${globalUserRole eq 'R001'}">
										<th>Status Invoice</th>
										</c:if>
										<th style="display: <c:out value="${buttonstatus}"/>"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listDebitNote}" var="dn" varStatus="loop">
										<tr>
											<td style="display: <c:out value="${buttonstatus}"/>">
												<c:if test="${dn.status ne 'Paid'}">
													<div class="checkbox" id="cbDebitNote">
													<label><input title="confirm full payment" 
														type="checkbox"
														class="icheckbox_minimal-red"
														id="debit-note-<c:out value="${loop.index}" />"
														value="<c:out value="${dn.debitNoteNumber}" />&&<c:out value="${dn.amount}" />"
														>
													</label>
													</div>
												</c:if>
											</td>
											<c:if test="${globalUserRole eq 'R001'}">
												<td><c:out value="${dn.customerName}" /></td>
											</c:if>
											<td>
												<a href="" style="color: #144471; font-weight: bold;" title="view order detail"
													data-target="#ModalOrderDetail"
													data-toggle="modal"
													data-lorderid='<c:out value="${dn.orderManagementID}" />'
													data-lfctaddr = '<c:out value="${dn.factoryAddress}" />'
													data-lfctid = '<c:out value="${dn.factoryID}" />'
													data-lfctname = '<c:out value="${dn.factoryName}" />'
													data-lsi = '<c:out value="${dn.shippingInvoiceID}" />'
													data-lsl = '<c:out value="${dn.shippingName}" />'
													data-lcommodity = '<c:out value="${dn.itemDescription}" />'
													data-ldo = '<c:out value="${dn.deliveryOrderID}" />'
													data-lvesselname = '<c:out value="${dn.vesselName}" />'
													data-lvoyageno = '<c:out value="${dn.voyageNo}" />'
													data-lport = '<c:out value="${dn.portName}" />'
													data-ldepo = '<c:out value="${dn.depoName}" />'
													data-lordertype = '<c:out value="${dn.orderType}" />'
													data-ltier = '<c:out value="${dn.tier}" />'
													>
													<c:out value="${dn.orderManagementID}" />
												</a>
											</td>
											<td>
												<a href="Invoice?invoiceNumber=<c:out value="${dn.debitNoteNumber}" />" style="color: #144471; font-weight: bold;" title="print"> 
													<c:out value="${dn.debitNoteNumber}" />
												</a>
												
												
												
											</td>
											<!-- invoice date -->
											<td><c:out value="${dn.date}" /></td>
											<!-- amount -->
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${dn.amount}" type="currency"/>
											</td>
											<!-- amount paid -->
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${dn.amountPaid}" type="currency"/>
											</td>
											<!-- amount balance -->
											<td style="font-weight: bold;">
												<fmt:setLocale value="id_ID"/>
												<fmt:formatNumber value="${dn.amount-dn.amountPaid}" type="currency"/>
											</td>
											<!-- status -->
											<td style="font-size: 18px;">
												<c:if test="${dn.status eq 'Paid'}">
													<span class="label label-primary">
														<c:out value="${dn.status}" />
													</span>
												</c:if>
												<c:if test="${dn.status ne 'Paid'}">
													<span class="label label-danger">
														<c:out value="${dn.status}" />
													</span>
												</c:if>
											</td>
											<!-- payment date -->
											<td><c:out value="${dn.paymentDate}" /></td>
											<!-- overdue -->
											<td><c:out value="${dn.overdue}" /> day(s)</td>	
											<c:if test="${globalUserRole eq 'R001'}">
											<td>
												<c:if test="${dn.isFinalInvoice == true}">
													<button class="btn btn-primary disabled" type="button" id="btnInvoiceSetting" name="btnInvoiceSetting"
													data-target="#ModalInvoiceSetting"
														data-toggle="modal"
														data-linvoice='<c:out value="${dn.debitNoteNumber}" />'
														data-lstatusinvoice='<c:out value="${dn.isFinalInvoice}" />'
														data-lisreimburse='<c:out value="${dn.isReimburse}" />'
														data-lcustomclearance='<c:out value="${dn.customClearance}" />'
														data-lfinalrate='<c:out value="${dn.finalRate}" />'
														data-larry='<c:out value="${dn.debitNoteReimburseDetails}" />'
														data-larryReimburse='<c:out value="${dn.reimburses}" />'
														data-larrydebitnotedetail='<c:out value="${dn.debitNoteNumberDetail}" />'
														data-ladditionalPrice='<c:out value="${dn.additionalPrice}" />'
														data-lppn = '<c:out value="${dn.ppn}" />'
														data-lpph23 = '<c:out value="${dn.pph23}" />'
														>
														Final <i class="fa fa-cog" aria-hidden="true"></i>
													</button>
													
												</c:if>
												<c:if test="${dn.isFinalInvoice == false}">
													<button class="btn btn-warning" type="button" id="btnInvoiceSetting" name="btnInvoiceSetting"
													data-target="#ModalInvoiceSetting"
														data-toggle="modal"
														data-linvoice='<c:out value="${dn.debitNoteNumber}" />'
														data-lstatusinvoice='<c:out value="${dn.isFinalInvoice}" />'
														data-lisreimburse='<c:out value="${dn.isReimburse}" />'
														data-lcustomclearance='<c:out value="${dn.customClearance}" />'
														data-lfinalrate='<c:out value="${dn.finalRate}" />'
														data-larry='<c:out value="${dn.debitNoteReimburseDetails}" />'
														data-larryReimburse='<c:out value="${dn.reimburses}" />'
														data-larrydebitnotedetail='<c:out value="${dn.debitNoteNumberDetail}" />'
														data-ladditionalPrice='<c:out value="${dn.additionalPrice}" />'
														data-lppn = '<c:out value="${dn.ppn}" />'
														data-lpph23 = '<c:out value="${dn.pph23}" />'
														>
														Performance <i class="fa fa-cog" aria-hidden="true"></i>
													</button>
												</c:if>
											</td>
											</c:if>
											<td style="display: <c:out value="${buttonstatus}"/>">
												<c:if test="${dn.status ne 'Paid'}">
													<button id="btnPartial" name="btnPartial" type="button" class="btn bg-olive" data-toggle="modal"
														title="Partial Pay"
														data-target="#ModalPartialPayment"
														data-ldnn = '<c:out value="${dn.debitNoteNumber}" />'
														data-lamount = '<c:out value="${dn.amount}" />'
														>
														Pay partially
													</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
								<tfoot style="background-color: #003156; color:white;
											font-family: Roboto; font-size: 16px; font-weight: bold;
											">
						            <tr>
						            	<th colspan="2">Total Amount Unpaid</th>
						            	<c:choose>
    										<c:when test="${globalUserRole eq 'R001'}">
    											<th colspan="11" style="text-align: center;">
													<fmt:setLocale value="id_ID"/>
													<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
												</th>
    										</c:when>    
										    <c:otherwise>
										        <th colspan="9" style="text-align: center;">
													<fmt:setLocale value="id_ID"/>
													<fmt:formatNumber value="${totalAmountUnpaid}" type="currency"/>
												</th>
										    </c:otherwise>
										</c:choose>
									</tr>
						        </tfoot>
							</table>
							
							<br>
							
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>	

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
	
	//custom number input type
	
    function formatNumberCustom() {
	   	$('input.number').keyup(function(event) {
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;
		
		  // format number
		  $(this).val(function(index, value) {
		    return value
		    .replace(/\D/g, "")
		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		    ;
		  });
		});
    }
    
 	$(function () {
 		$(".select2").select2();
 		
  		$("#tb_debit_note").DataTable({"aaSorting": [],"scrollX": true});
  		$('#M023').addClass('active');
  		
  		//calendar java script
		$('#txtStuffingDateFrom').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtStuffingDateTo').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtPaymentDate').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    $('#txtPaymentDate2').datepicker({
	      format: 'dd M yyyy',
	      autoclose: true
	    });
	    
	    //custom number input type
		$('input.number').keyup(function(event) {
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;
		
		  // format number
		  $(this).val(function(index, value) {
		    return value
		    .replace(/\D/g, "")
		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		    ;
		  });
		});
  	    
  		$("#dvErrorAlert").hide();
  		$("#dvErrorAlertPartialPayment").hide();
  	});

	</script>

<script>
	function FuncClear() {
		$('#mrkPaymentDate').hide();
		$('#mrkPaymentDate2').hide();
		$('#mrkAmountPaid').hide();
		
		$('#dvPaymentDate').removeClass('has-error');
		$('#dvPaymentDate2').removeClass('has-error');
		$('#dvAmountPaid').removeClass('has-error');
	}
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		FuncClear();
		
		$("#dvErrorAlert").hide();
 		var modal = $(this);
 		modal.find(".modal-body #txtPaymentDate").val('');
	});
	
	
	$('#ModalPartialPayment').on('shown.bs.modal', function (event) {
		FuncClear();
		
		$("#dvErrorAlertPartialPayment").hide();
 		var modal = $(this);
 		var button = $(event.relatedTarget);
 		var lDebitNoteNumber = button.data('ldnn');
 		var lAmount = button.data('lamount');
 		console.log(lAmount)
 		
 		document.getElementById("lblTitleModal").innerHTML = 'Partial Payment For Invoice No. '+ lDebitNoteNumber + '<br>' + '( Total Amount = Rp '+ Intl.NumberFormat(['ban', 'id']).format(lAmount) +',00 )';
 		modal.find(".modal-body #txtPaymentDate2").val('');
 		modal.find(".modal-body #txtAmountPaid").val('');
 		modal.find(".modal-body #txtDebitNoteNumber").val(lDebitNoteNumber);
 		modal.find(".modal-body #txtAmount").val(lAmount);
 		
	});
	
	var divRemID = 1;
	// modal invoice setting
	$('#ModalInvoiceSetting').on('shown.bs.modal', function (event) {
		
		$("#dvErrorAlertInvoiceSetting").hide();
		var modal = $(this);
 		var lInvoice = $(event.relatedTarget).data("linvoice");
 		var lArry = $(event.relatedTarget).data("larry");
 		var lArryReimburse = $(event.relatedTarget).data("larryreimburse");
 		var lStatusInvoice = $(event.relatedTarget).data("lstatusinvoice");
 		var lIsreimburse = $(event.relatedTarget).data("lisreimburse");
 		var lCustomclearance = $(event.relatedTarget).data("lcustomclearance");
 		var lFinalrate = $(event.relatedTarget).data("lfinalrate");
 		var lAdditionalPrice = $(event.relatedTarget).data("ladditionalprice");
 		var lPPN = $(event.relatedTarget).data("lppn");
 		var lPPh23 = $(event.relatedTarget).data("lpph23");
 		var lDebitNoteDetail = $(event.relatedTarget).data("larrydebitnotedetail");
 		
 		modal.find(".modal-body #statusIncoice").val(lStatusInvoice.toString()).trigger('change.select2');
 		modal.find(".modal-body #txtReimburse").val(lIsreimburse);
 		modal.find(".modal-body #txtFinalRate").val(lFinalrate);
 		modal.find(".modal-body #txtDebitNoteNumberAdd").val(lInvoice);
 		modal.find(".modal-body #txtCustomClearanceFinal").val(lCustomclearance);
 		modal.find(".modal-body #txtAdditionalPrice").val(lAdditionalPrice);
 		modal.find(".modal-body #txtPPh23").val(lPPh23);
 		modal.find(".modal-body #txtPPN").val(lPPN);
 		modal.find(".modal-body #txtPPN").val(lPPN);
 		
 		var reimHTML = "";
 		divRemID = 1;
 		
 		for (var rem in lArry) {
 			var rmID = "rem-detail-"+divRemID;
 			reimHTML += '<div class="col-md-12" id="rem-detail-'+divRemID+'">'+
				 		  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				 		   	'<input type="text" class="form-control" id="txtReimburseName" name="txtReimburseName" value="'+lArry[rem].nameReimburse+'" disabled>'+
				 		  '</div>'+
				 		  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				 		    '<input type="text" class="form-control" id="txtReimburseAmount" name="txtReimburseAmount" value="'+lArry[rem].reimburseAmount+'" disabled>'+						                    
				 		  '</div>'+
				 		  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				 		    '<button id="btnSavePartialPayment" type="button" class="btn" onclick="removeReimburse(\'' + divRemID + '\',\'' +lArry[rem].debitNoteReimburseID+ '\')"><i class="fa fa-minus"></i> </button>'+						                    
				 		  '</div>'+
				 		'</div>';
 			divRemID++;
 		}
 		
 		$( "#reimburse" ).html(reimHTML);
 		
 		var debitNoteDetailHtml = lDebitNoteDetail.map((data, i) => {
 			i++;
 			return '<div class="form-group col-md-12 nopadding">'+
				'<div id="dvPaymentDate2" class="form-group col-md-2">'+
					'<input type="text" class="form-control number" id="txtQuantity" name="txtQuantity" value="1 X '+data.description+'" readonly>'+						                    
				'</div>'+
				'<div id="dvPaymentDate2" class="form-group col-md-6">'+
					'<input type="text" class="form-control number finalrate" id="txtFinalRate" name="txtFinalRate" value="'+data.finalRate+'">'+						                    
				'</div>'+
				'<div id="dvPaymentDate2" class="form-group col-md-2" style="display: none">'+
					'<input type="text" class="form-control number debitNoteDetail" id="txtDebitNoteDetail" name="txtDebitNoteDetail" value="'+data.debitNoteDetailID+'">'+	
				'</div>'+
			'</div>';
 		});
 		
 		console.log(debitNoteDetailHtml)
 		$( "#divDebitNoteDetail" ).html(debitNoteDetailHtml);
 		
 		var reimburse = "";
 		for (var rem in lArryReimburse) {
 			reimburse += '<option value="'+lArryReimburse[rem].reimburseID+'">'+lArryReimburse[rem].name+'</option>';
 		}
 		$( "#txtReimburseNameAdd" ).html(reimburse);
 		
 		
 		
 		
 		document.getElementById("lblTitleModalInvoiceSetting").innerHTML = "Setting Invoice "+lInvoice;
	});
	
	function FuncUpdateSetting(){
		var arrDebitNoteDetail = [];
		var debitNoteDetail = $("#divDebitNoteDetail .nopadding").map((data) => {
			arrDebitNoteDetail.push({
				debitNoteDetailID:$("#divDebitNoteDetail .nopadding").find(".debitNoteDetail").val(),
				finalRate:$("#divDebitNoteDetail .nopadding").find(".finalrate").val()
			});
		});
		
		var param = {
				"key":"updateDebitNoteInvoiceSetting",
				"debitNoteNumber": $("#txtDebitNoteNumberAdd").val(),
				"isFinalInvoice": $("#statusIncoice").val(),
				"customClearance": $("#txtCustomClearanceFinal").val().replace(/\./g,''),
				"finalRate": $("#txtFinalRate").val().replace(/\./g,''),
				"additionalPrice": $("#txtAdditionalPrice").val().replace(/\./g,''),
				"ppn":$("#txtPPN").val(),
				"pph23":$("#txtPPh23").val(),
				"debitNoteDetail": JSON.stringify(arrDebitNoteDetail)
		}
		
		$.ajax({
	          url: '${pageContext.request.contextPath}/DebitNote',
	          type: 'POST',
	          data: param,
	          dataType: 'json'
	     })
	     .done(function(data){
	    	 $("#dvErrorAlertInvoiceSetting").hide();
	    	 window.location.reload();
	     })
	     .fail(function(){
	    	 console.log("save fail!");  
	    	 $("#dvErrorAlertInvoiceSetting").show();
	     });
	}
	
	function addReimburse() {
		
		var remID = $("#txtReimburseNameAdd").val();
		var remAmount = $("#txtReimburseAmountAdd").val().replace(/\./g,'');
		var debitNoteNumber = $("#txtDebitNoteNumberAdd").val();
		
		divRemID++;
		
		$.ajax({
	          url: '${pageContext.request.contextPath}/DebitNote',
	          type: 'POST',
	          data: {"key":"saveReimburse","debitNoteNumber":debitNoteNumber, "idReimburse":remID, "reimburseAmount":remAmount, "nameReimburse":"asd"},
	          dataType: 'json'
	     })
	     .done(function(data){
	    	var reimHTML = '';
			for ( var result in data) {
				reimHTML = '<div class="col-md-12" id="rem-detail-'+divRemID+'">'+
				  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				   	'<input type="text" class="form-control" id="txtReimburseName" name="txtReimburseName" value="'+data[result].nameReimburse+'" disabled>'+
				  '</div>'+
				  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				    '<input type="text" class="form-control" id="txtReimburseAmount" name="txtReimburseAmount" value="'+data[result].reimburseAmount+'" disabled>'+						                    
				  '</div>'+
				  '<div id="dvPaymentDate2" class="form-group col-md-4">'+
				    '<button id="btnSavePartialPayment" type="button" class="btn" onclick="removeReimburse(\'' + divRemID + '\',\'' +data[result].debitNoteReimburseID+ '\')"><i class="fa fa-minus"></i> </button>'+						                    
				  '</div>'+
				'</div>';
			}
	    	  
			$( "#reimburse" ).append(reimHTML);
	     })
	     .fail(function(){
	    	 console.log("save fail!");  
	     });
		
		$("#txtReimburseNameAdd").val("");
		$("#txtReimburseAmountAdd").val("");
		
	}
	
	
	function removeReimburse(param, dnrmID) {
		var key = "deleteReimburse";
		$.ajax({
	          url: '${pageContext.request.contextPath}/DebitNote',
	          type: 'POST',
	          data: {"key":key,"debitNoteReimburseID":dnrmID},
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log("delete berhasil!");
	     })
	     .fail(function(){
	    	 console.log("delete fail!");  
	     });
		
		
		$( "#rem-detail-"+param).remove();
		
	}
	
	
	$('#ModalOrderDetail').on('shown.bs.modal', function (event) {
 		var modal = $(this);
 		var button = $(event.relatedTarget);
 		var lorderid = button.data('lorderid');
 		var lfctaddr = button.data('lfctaddr');
 		var lfctid = button.data('lfctid');
 		var lfctname = button.data('lfctname');
 		var lsi = button.data('lsi');
 		var lsl = button.data('lsl');
 		var lcommodity = button.data('lcommodity');
 		var ldo = button.data('ldo');
 		var lvesselname = button.data('lvesselname');
 		var lvoyageno = button.data('lvoyageno');
 		var lport = button.data('lport');
 		var ldepo = button.data('ldepo');
 		var lordertype = button.data('lordertype');
 		var ltier = button.data('ltier');
 		
 		document.getElementById("lblTitleModalOrderDetail").innerHTML = 'Order Summary '+ lorderid;
 		
 		modal.find(".modal-body #txtFactoryID").val(lfctid);
 		modal.find(".modal-body #txtFactoryAddress").val(lfctaddr);
 		document.getElementById("lblFactoryName").innerHTML = '(' + lfctname + ')';
 		modal.find(".modal-body #txtShippingInvoiceID").val(lsi);
 		modal.find(".modal-body #txtShippingName").val(lsl);
 		modal.find(".modal-body #txtItem").val(lcommodity);
 		modal.find(".modal-body #txtDeliveryOrderID").val(ldo);
 		modal.find(".modal-body #txtVesselName").val(lvesselname);
 		modal.find(".modal-body #txtVoyageNo").val(lvoyageno);
 		modal.find(".modal-body #txtPortName").val(lport);
 		modal.find(".modal-body #txtDepoName").val(ldepo);
 		modal.find(".modal-body #slOrderType").val(lordertype).trigger('change.select2');
 		modal.find(".modal-body #slTier").val(ltier).trigger('change.select2');
 		modal.find(".modal-body #sVendor").val(ltier).trigger('change.select2');
 		
 		$("#VendorPanel :input").prop("checked", false);
 		$.ajax({
				url : '${pageContext.request.contextPath}/getMappingOrderVendor',
				type : 'POST',
				data : {
					orderid : lorderid
				},
				dataType : 'json'
			}).done(
					function(data) {
						console.log(data);
						for ( var i in data) {
							var obj = data[i];								
							modal.find(".modal-body #"+ obj.VendorID).prop('checked', true);
							modal.find(".modal-body #sVendor").val( obj.VendorID ).trigger('change.select2');
						}
					}).fail(function() {
				console.log('Service call failed!');
			})
			
		FuncLoadSchedule(lorderid);
	});
	
	//function load dynamic schedule data
 	function FuncLoadSchedule(lOrderID){
	    $('#dynamic-content-schedule').html(''); // leave this div blank
		$('#dvloaderSchedule').show(); // load ajax loader on button click
	 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getOrderDetail',
	          type: 'POST',
	          data: 'orderid='+lOrderID,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-schedule').html(''); // blank before load.
	          $('#dynamic-content-schedule').html(data); // load here
			  $('#dvloaderSchedule').hide(); // hide loader  
	     })
	     .fail(function(){
	          $('#dynamic-content-schedule').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			  $('#dvloaderSchedule').hide(); // hide loader  
	     });
 	}
 	
 	//show modal vendor/trucker
	$('#ModalVendor').on('shown.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lOrderDetailID = button.data('lordermanagementdetailid');
    
    	$('#dynamic-content-vendor').html(''); // leave this div blank
 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getVendorOrderDetailByCustomerOrder',
	          type: 'POST',
	          data: 'orderDetailID='+lOrderDetailID,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-vendor').html(''); // blank before load.
	          $('#dynamic-content-vendor').html(data); // load here
	     })
	     .fail(function(){
	          $('#dynamic-content-vendor').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     });
	})

function FuncSave(lParambtn) {
	var checkedDebitNote = new Array();
		$('#cbDebitNote input:checked').each(function() {
			checkedDebitNote.push(this.value);
		});
 	var paymentDate = document.getElementById('txtPaymentDate').value;
 	
 	if(!paymentDate.match(/\S/)) {    	
	    	$('#txtPaymentDate').focus();
	    	$('#dvPaymentDate').addClass('has-error');
	    	$('#mrkPaymentDate').show();
	        return false;
	    } 
 	
 	$('#dvloader').show();
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/DebitNote',	
	        type:'POST',
	        data:{"key":lParambtn,"paymentDate":paymentDate,"debitNoteNumber":checkedDebitNote},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirm')
	        	{
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/DebitNote';  
 	        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloader').hide();
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

function FuncSavePartialPayment(lParambtn) {
	var amountPaid = document.getElementById('txtAmountPaid').value.replace(/\./g,'');
	var amount = document.getElementById('txtAmount').value;
 	var paymentDate = document.getElementById('txtPaymentDate2').value;
 	var debitNoteNumber = document.getElementById('txtDebitNoteNumber').value;
 	
 	if(!paymentDate.match(/\S/)) {    	
	    	$('#txtPaymentDate2').focus();
	    	$('#dvPaymentDate2').addClass('has-error');
	    	$('#mrkPaymentDate2').show();
	        return false;
	    }
	if(!amountPaid.match(/\S/)) {    	
	    	$('#txtAmountPaid').focus();
	    	$('#dvAmountPaid').addClass('has-error');
	    	$('#mrkAmountPaid').show();
	    	document.getElementById("mrkAmountPaid").innerHTML = '*';
	        return false;
	    }
	if(Number(amountPaid) >= Number(amount)){
			$('#txtAmountPaid').focus();
	    	$('#dvAmountPaid').addClass('has-error');
	    	$('#mrkAmountPaid').show();
	    	document.getElementById("mrkAmountPaid").innerHTML = '( Amount paid may not equal or bigger than ' + 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(amount) +',00 )';
	    	return false;
	}  
 	
 	$('#dvloaderPartialPayment').show();
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/DebitNote',	
	        type:'POST',
	        data:{"key":lParambtn,"paymentDate":paymentDate,"amountPaid":amountPaid,"debitNoteNumber":debitNoteNumber},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirmPartialPayment')
	        	{
	        		$('#dvloaderPartialPayment').hide();
	        		$("#dvErrorAlertPartialPayment").show();
	        		document.getElementById("lblAlertPartialPayment").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertPartialPaymentDescription").innerHTML = data.split("--")[1];
	        		$("#ModalPartialPayment").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/DebitNote';  
 	        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloaderPartialPayment').hide();
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

function FuncButtonSearch(lParambtn) {
 	var Status = document.getElementById('slStatusSearch').value;
 	var StuffingDateFrom = document.getElementById('txtStuffingDateFrom').value;
	var StuffingDateTo = document.getElementById('txtStuffingDateTo').value;
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/DebitNote',	
	        type:'POST',
	        data:{"key":lParambtn,"statusSearch":Status,"stuffingdatefrom":StuffingDateFrom, "stuffingdateto":StuffingDateTo},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	var url = '${pageContext.request.contextPath}/DebitNote';  
 	        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

//click payment checkbox action
/* $(document).ready(function(){

    $(document).on('click', '#cbDebitNote', function(e){
	
    	var checkedDebitNote = new Array();
    	var total = 0;
		var i;

    	$('#cbDebitNote input:checked').each(function() {
    		checkedDebitNote.push(this.value);
    	});
    	if(checkedDebitNote.length == 0){
    		$("#btnPayment").hide();
    		$("#newLine").hide();
    	}
    	else{
    		$("#btnPayment").show();
    		$("#newLine").show();
    		for (i = 0; i < checkedDebitNote.length; i++) {
			    total += Number(checkedDebitNote[i].split("&&")[1]);
			}
			
			document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
    	}
    	
    });
}); */



var bodyModal = "";
var checkedDebitNote = [];
var debitNote = [];

function closeModal() {
	checkedDebitNote = [];
	bodyModal = "";
	console.log("close")
	$('#ModalUpdateInsert').modal('hide')
}

function savePaymentConfirmation(){
	var num = 1;
	var arryPayment = [];
	var arrFullPayment = [];
	var arrPartialPayment = [];
	
	for ( var item in debitNote) {
		var textAmount = $('#row'+num).find("#txtAmount").val().replace(/\./g,'');
		var txtAmountPaid = $('#row'+num).find("#txtAmountPaid").val().replace(/\./g,'');
		var calPaymentDate = $('#row'+num).find("#calPaymentDate"+num).val();
		var selPayment = $( "#selPaymentMethod"+num ).val();
		var txtInvoiceNumber = $('#row'+num).find("#txtInvoiceNumber").val();
		var checkedDebitNote = ""+txtInvoiceNumber+"&&"+textAmount;
		
		console.log(txtAmountPaid)
		
		arryPayment.push({
			"amount":textAmount,
			"amountPaid": txtAmountPaid,
			"paymentDate": calPaymentDate,
			"paymentMethod": selPayment,
			"debitNoteNumber": txtInvoiceNumber,
			"checkedDebitNote": checkedDebitNote
			
		});
		
		
		num++;
		
	}
	
	arrFullPayment = arryPayment.filter(function( obj ) {
	    return obj.paymentMethod === "Full Payment";
	});
	
	arrPartialPayment = arryPayment.filter(function( obj ) {
	    return obj.paymentMethod === "Partial Payment";
	});
	
	console.log(arrFullPayment)
	
	jQuery.ajax({
        url:'${pageContext.request.contextPath}/DebitNote',	
        type:'POST',
        data:{"key":"saveConfirmPayment", "fullPayment":JSON.stringify(arrFullPayment), "partialPayment":JSON.stringify(arrPartialPayment)},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedConfirm')
        	{
        		console.log("Confirmation failed")
        		$('#dvloader').hide();
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/DebitNote';  
	        		$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
        	$('#dvloader').hide();
            console.log('Service call failed!');
        }
    });
	
	/* for ( var confirmPayment in arrFullPayment) {
		arrFullPayment[confirmPayment].debitNoteNumber
		
		jQuery.ajax({
	        url:'${pageContext.request.contextPath}/DebitNote',	
	        type:'POST',
	        data:{"key":lParambtn,"paymentDate":paymentDate,"debitNoteNumber":checkedDebitNote},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedConfirm')
	        	{
	        		console.log("Confirmation failed")
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Confirmation failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/DebitNote';  
 	        		$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$('#dvloader').hide();
	            console.log('Service call failed!');
	        }
	    });
	} */
	
	console.log(arryPayment)
	
}

function getUnique(arr, comp) {

  const unique = arr
       .map(e => e[comp])

     // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e]).map(e => arr[e]);

   return unique;
}


//click payment checkbox action
$(document).ready(function(){
	
	var table = $('#tb_debit_note').DataTable();
	var MyRows = $('#tb_debit_note').find('tbody').find('tr');
	
	$('.icheckbox_minimal-red').on('change', function() {
		// untuk dapet row index table, ketika row diclick
		var idxRow = $('.icheckbox_minimal-red').index(this) +3;
		
		// untuk dapet value dari row 
		var amountVal = $('#tb_debit_note').find('tr:eq('+idxRow+') td:eq(5)').text().replace(/\.|,00|Rp| /g, "");
		var amountPaidVal = $('#tb_debit_note').find('tr:eq('+idxRow+') td:eq(6)').text().replace(/\.|,00|Rp| /g, "");
		var amountPaidBalanceVal = $('#tb_debit_note').find('tr:eq('+idxRow+') td:eq(7)').text().replace(/\.|,00|Rp| /g, "");
		var iNumber = $('#tb_debit_note').find('tr:eq('+idxRow+') td:eq(3)').text().replace(/\s/g,'');
		var cp = parseInt(amountVal.replace(/\.|,00|Rp| /g, "")) - parseInt(amountPaidVal.replace(/\.|,00|Rp| /g, ""));
		var getIdChekbox = $('#tb_debit_note').find('tr:eq('+idxRow+') td:eq(0) label input').attr('id');
		
		if ($('input#'+getIdChekbox).is(':checked')) {
			debitNote.push({
    			"index":idxRow,
    			"invoiceNumber": iNumber,
    			"amount":parseInt(amountVal),
    			"amountPaid":parseInt(amountPaidVal),
    			"amountPaidBalance":parseInt(amountPaidBalanceVal),
    			
    		});
		}else {
			// remove debit note by idxRow
			var arrDN = debitNote.filter(function( obj ) {
			    return obj.index !== idxRow;
			});
			
			debitNote = arrDN;
		}
		
		var total = 0;
    	if(debitNote.length == 0){
    		$("#btnPayment").hide();
    		$("#newLine").hide();
    		
    	}else{
    		$("#btnPayment").show();
    		$("#newLine").show();
    		
    		var total = 0;
    		for (const item of debitNote) {
    			total += item.amountPaidBalance;
			}
    		
			document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
    	}
		
		// console.log(debitNote)
		
	    // console.log("index ", $('.icheckbox_minimal-red').index(this));
	    // console.log("amountVal", amountVal)
	});
	
	$('#btnPayment').on('click', function () {
		var num = 1;
	
		for (const item of debitNote) {
			bodyModal += 	'<div class="col-md-12" style="padding-bottom: 15px;" id="row'+num+'"><div class="col-md-2">'+
						      '<label for="lblPaymentDate2" class="control-label">Invoice Number</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>'+
						      '<input type="text" class="form-control" id="txtInvoiceNumber" name="txtInvoiceNumber" value="'+item.invoiceNumber+'" disabled="disabled">'+
						    '</div>'+
						    '<div class="col-md-2">'+
						      '<label>Select Payment</label>'+
						          '<select class="form-control" id="selPaymentMethod'+num+'">'+
						          	'<option value="" selected disabled>Please select</option>'+
						            '<option value"Full Payment">Full Payment</option>'+
						            '<option value"Partial Payment">Partial Payment</option>'+
						          '</select>'+
						    '</div>'+
						    '<div class="col-md-2">'+
						      '<label for="lblPaymentDate2" class="control-label">Payment Date</label><label id="mrkPaymentDate2" for="recipient-name" class="control-label"></label>'+
						      '<input type="text" class="form-control" id="calPaymentDate'+num+'" name="txtPaymentDate2">'+
						    '</div>'+
						    '<div class="col-md-3">'+
						      '<label for="lblAmountPaid" class="control-label">Amount Balance</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>'+
						      '<input class="form-control number" id="txtAmount" name="txtAmount" value="'+item.amountPaidBalance+'" disabled="disabled">'+
						    '</div>'+
						    '<div class="col-md-3">'+
						      '<label for="lblAmountPaid" class="control-label">Amount Paid</label><label id="mrkAmountPaid" for="recipient-name" class="control-label"></label>'+
						      '<input class="form-control number" id="txtAmountPaid" name="txtAmountPaid">'+
						    '</div></div>';
						    
							
			num++;
			
		}
		
		
		
		$( "#idBodyFormPaymentPaid" ).html(bodyModal);
		
		formatNumberCustom();
		for (var i = 0; i < num; i++) {
			console.log(i)
			$('#calPaymentDate'+i).datepicker({
			      format: 'dd M yyyy',
			      autoclose: true
			});
			
			$( "#selPaymentMethod"+i ).change(function() {
				var getID = $(this).parent().parent().attr('id');
				var txtAmount = $("#"+getID).find("#txtAmount").val();
				
				var selPayment = this.value;
				if(selPayment === "Full Payment") {
					$("#"+getID).find("#txtAmountPaid").val(txtAmount);
					$("#"+getID).find("#txtAmountPaid").prop('disabled', true);
				}else {
					$("#"+getID).find("#txtAmountPaid").val(0);
					$("#"+getID).find("#txtAmountPaid").prop('disabled', false);
				}
			});
		}
		
		
		/* $('#ModalUpdateInsert').modal('show') */
	})
	
	

    /* $(document).on('change', '#cbDebitNote', function(e){
    	
    	var total = 0;
		var i;
		var cp = 0;
		
    	$('#cbDebitNote input:checked').each(function(i) {
    		checkedDebitNote.push(this.value);
    		var idxRow =  i;
    	});
    	
    	if(checkedDebitNote.length == 0){
    		$("#btnPayment").hide();
    		$("#newLine").hide();
    	}
    	else{
    		$("#btnPayment").show();
    		$("#newLine").show();
    		for (i = 0; i < checkedDebitNote.length; i++) {
			    total += (Number(checkedDebitNote[i].split("&&")[1])- Number(checkedDebitNote[i].split("&&")[2]));
			    console.log(checkedDebitNote[i].split("&&")[2])
			}
			
			document.getElementById("lblTotalPayment").innerHTML = 'Rp '+ Intl.NumberFormat(['ban', 'id']).format(total) +',00';
    	}
    	
    }); */
});
</script>

</body>

</html>