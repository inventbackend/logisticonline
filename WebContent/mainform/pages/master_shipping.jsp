<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Shipping</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Shipping" action = "${pageContext.request.contextPath}/Shipping" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Shipping
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertShipping'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan shipping.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateShipping'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui shipping.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteShipping'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus shipping.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteShipping'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus shipping. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
		          <div id="dvShippingID" class="form-group">
		            <label for="lblShippingID" class="control-label">Shipping ID:</label><label id="mrkShippingID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtShippingID" name="txtShippingID" readonly disabled>
		          </div>
		          <div id="dvName" class="form-group">
		            <label for="lblName" class="control-label">Shipping Name:</label><label id="mrkName" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtName" name="txtName">
		          </div>
		          <div id="dvAddress" class="form-group">
		            <label for="lblAddress" class="control-label">Shipping Address:</label><label id="mrkAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtAddress" name="txtAddress">
		          </div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Shipping</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtShippingID" name="temp_txtShippingID"  />
									<p>Are you sure to delete this shipping ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_shipping" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>ID</th>
	                  <th>Name</th>
	                  <td>Address</td>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listShipping}" var ="shipping">
		        	<tr>
		        	<td><c:out value="${shipping.shippingID}"/></td>
					<td><c:out value="${shipping.name}"/></td>
					<td><c:out value="${shipping.address}"/></td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lshippingid='<c:out value="${shipping.shippingID}"/>'
								data-lname='<c:out value="${shipping.name}"/>'
								data-laddress='<c:out value="${shipping.address}"/>'
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-lshippingid='<c:out value="${shipping.shippingID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_shipping').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
	$('#M006').addClass('active');
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lShippingID = button.data('lshippingid');
		$("#temp_txtShippingID").val(lShippingID);
	})
	</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var lshippingid = button.data('lshippingid')
	  var lname = button.data('lname')
	  var laddress = button.data('laddress')

	  var modal = $(this)
	  modal.find('.modal-body #txtShippingID').val(lshippingid)
	  modal.find('.modal-body #txtName').val(lname)
	  modal.find('.modal-body #txtAddress').val(laddress)
	  
	  $("#txtName").focus();
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

	function FuncClear(){
		$('#mrkShippingID').hide();
		$('#mrkName').hide();
		$('#mrkAddress').hide();
		
		$('#dvName').removeClass('has-error');
		$('#dvAddress').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#dvShippingID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Shipping";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		$('#dvShippingID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Shipping';
	
		FuncClear();
	}
	
	//function delete rate
	function FuncDelete() {
		var temp_txtShippingID = document.getElementById('temp_txtShippingID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Shipping',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtShippingID":temp_txtShippingID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/Shipping';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtShippingID = document.getElementById('txtShippingID').value;
		var txtName = document.getElementById('txtName').value;
		var txtAddress = document.getElementById('txtAddress').value;
	
		var dvShippingID = document.getElementsByClassName('dvShippingID');
		var dvName = document.getElementsByClassName('dvName');
		var dvAddress = document.getElementsByClassName('dvAddress');
		
	    if(!txtName.match(/\S/)) {    	
	    	$('#txtName').focus();
	    	$('#dvName').addClass('has-error');
	    	$('#mrkName').show();
	        return false;
	    }
	    if(!txtAddress.match(/\S/)) {    	
	    	$('#txtAddress').focus();
	    	$('#dvAddress').addClass('has-error');
	    	$('#mrkAddress').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Shipping',	
	        type:'POST',
	        data:{"key":lParambtn,"txtShippingID":txtShippingID,"txtName":txtName,"txtAddress":txtAddress},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertShipping')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan shipping";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateShipping')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui shipping";
	        		document.getElementById("lblAlertName").innerHTML = data.split("--")[1];
	        		$("#txtName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Shipping';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var shippingid = document.getElementById('txtShippingID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (shippingid == null || shippingid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (shippingid != null || shippingid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>