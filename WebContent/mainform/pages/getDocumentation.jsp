<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Gatepass</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
	<style type="text/css">
		/*  css for loading bar */
		 .loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 60px;
		  height: 60px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		  margin-left: 45%;
		}
		
		.loaderModal {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 60px;
		  height: 60px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		  margin-left: 45%;
		}
		
		.clblLoader {
		  margin-left: 25%;
		}
		
		.clblLoaderModal {
		  margin-left: 15%;
		}
		
		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}
		
		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
		/* end of css loading bar */
		
		.select2-container--default .select2-search--inline .select2-search__field{
	    width:initial!important;
	}
	</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Form_Gatepass" name="Form_Gatepass" action = "${pageContext.request.contextPath}/Gatepass" method="post" enctype="multipart/form-data">
	<input type="hidden" name="temp_string" value="" />
	<%-- <input type="hidden" id="tempRole" value="<c:out value="${globalUserRole}"/>" /> --%>
	
	<!-- KOJA SETTING -->
	<input type="hidden" id="kojaUsername" value="<%= config.getServletContext().getInitParameter("koja_username") %>" />
	<input type="hidden" id="kojaPassword" value="<%= config.getServletContext().getInitParameter("koja_password") %>" />
	<input type="hidden" id="kojaStreamUsername" value="<%= config.getServletContext().getInitParameter("koja_fstream_username") %>" />
	<input type="hidden" id="kojaStreamPassword" value="<%= config.getServletContext().getInitParameter("koja_fstream_password") %>" />
	<input type="hidden" id="kojaDevicename" value="<%= config.getServletContext().getInitParameter("koja_devicename") %>" />
	<input type="hidden" id="kojaUrl" value="<%= config.getServletContext().getInitParameter("koja_url") %>" />
	
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Gatepass</h1>
			</section>
			<!-- Main content -->
			<section class="content">			
					
					
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<c:if test="${condition == 'SuccessUpdateDocumentation'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Update document success.
			   				</div>
						</c:if>
						<c:if test="${condition == 'SuccessGetGatepass'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Get gatepass success.
			   				</div>
						</c:if>
						<c:if test="${condition == 'FailedGetGatepass'}">
							<div class="alert alert-danger alert-dismissible">
			    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
			    				Get gatepass failed. <c:out value="${conditionDescription}"/>.
			  				</div>
						</c:if>
						
						<!-- <div id="dvErrorAlertService" class="alert alert-danger alert-dismissible" style="display:none;">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i> Failed</h4>
							<label id="lblAlertService"></label>. <label id="lblAlertServiceDescription"></label>.
						</div> -->
						
				<div id="dvSearch" class="row">	
					<div class="col-md-2">
					<label for="message-text" class="control-label">Port of loading</label>
						<select id="slPolSearch" name="slPolSearch" class="form-control">
							<c:if test="${pol eq ''}">
								<option value='' selected>General</option>
								<option value='PORT-0002'>KOJA</option>
							</c:if>
							<c:if test="${pol eq 'PORT-0002'}">
								<option value=''>General</option>
								<option value='PORT-0002' selected>KOJA</option>
							</c:if>
			       		</select>
			       	</div>
					 <!-- /.col-md-2 -->
					
					<div class="col-md-2">
						<button id="btnSearch" name="btnSearch" type="button" class="btn btn-primary" onclick="FuncButtonSearch('search')" style="margin-top: 25px">SHOW</button>
					</div>
					<!-- /.col-md-2 -->
				</div>
				<!-- /.row -->
				<br>
				        <!-- koja  get otp-->
						<!--modal update documentation -->
						<div class="modal fade" id="ModalGetOTP" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalDocumentation" name="lblTitleModalDocumentation">Get OTP</label></h4>	
							</div>
						    <div class="modal-body" >
						    	<div id="dvloaderGetOTP" class="loaderModal" style="display:none;"></div>
								<label id="lblLoaderGetOTP" class="clblLoaderModal" style="display:none;">please wait, don't refresh the page, this may take several seconds...</label>
								<label id="lblErrorGetOTP" style="display:none;">Get OTP Failed. Please Contact Administrator.</label>
							
								<input type="hidden" class="form-control" id="tempOrderID" name="tempOrderID">
								
						    	<div id="get-otp">
						    	</div>
								
				  				<div class="row"></div>
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnGetOTP" name="btnGetOTP" onclick="FuncGetOTP()">Send</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
							</div>
							</div>
						</div>
						
						
						<!-- koja modal show summary proforma-->
						<div class="modal fade" id="ModalSummaryProforma" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalDocumentation" name="lblTitleModalDocumentation">Ringkasan Proforma</label></h4>	
							</div>
						    <div class="modal-body" >
						    	<div id="dvloaderSummaryProforma" class="loaderModal" style="display:none;"></div>
								<label id="lblErrorSummaryProforma" style="display:none;">Associate fully or partially failed. Please contact administrator.</label>
								
						    	<div id="body-proforma">
						    		<br>
						    		<table class="table table-sm table-hover">
						    		<thead>
						    		
									</thead>
									<tbody id="tbody-proforma">
									  <tr>
									    <td>Penumpukan</td>
									    <td>Mark</td>
									  </tr>
									  <tr>
									    <td>Lift On</td>
									    <td>Jacob</td>
									  </tr>
									  <tr>
									    <td>Cost Recovery</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <td>Cost Recovery</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <td>Pass Pelabuhan</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <td>Biaya Etiket</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <td>Administrasi Nota</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <td>Biaya Materai</td>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <th scope="row">Subtotal</th>
									    <th colspan="2" style="text-align:right">Larry the Bird</th>
									  </tr>
									  <tr>
									    <th scope="row">PPN</th>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									  <tr>
									    <th scope="row">Total</th>
									    <td colspan="2">Larry the Bird</td>
									  </tr>
									</tbody>
								</table>
						    	</div>
								
				  				<div class="row"></div>
							</div>
							</div>
							</div>
						</div>
						
						<!-- koja modal send otp-->
						<div class="modal fade" id="ModalSendOTP" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalDocumentation" name="lblTitleModalDocumentation">Send OTP</label></h4>	
							</div>
						    <div class="modal-body">
						    	<div id="dvloaderSendOTP" class="loaderModal" style="display:none;"></div>
						    	<label id="lblLoaderSendOTP" class="clblLoaderModal" style="display:none;">please wait, don't refresh the page, this may take several seconds...</label>
								<label id="lblErrorSendOTP" style="display:none;">Associate fully or partially failed. Please contact administrator.</label>
								
						    	<div id="send-otp">
						    		<div class="form-group">
										<label for="recipient-name" class="control-label">OTP</label>	
										<input type="text" class="form-control" id="otp" name="otp">
									</div>
									<button type="button" class="btn btn-primary" id="btnSendOTP" name="btnSendOTP" onclick="FuncSendOTP()">Associate</button>
						    	</div>
								
				  				<div class="row"></div>
							</div>
							</div>
							</div>
						</div>
						
						
						<!-- progress properties -->
						<div id="dvloader" class="loader" style="display:none;"></div>
						<label id="lblLoader" class="clblLoader" style="display:none;">please wait, don't refresh the page, this may take several seconds...</label>
						
						<div id="dvProgressKojaShipper" class="progress active" style="display:none; margin-bottom:0px;">
					      <div id="progress-koja-shipper" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					        <span class="sr-only">40% Complete (success)</span>
					      </div>
					    </div>
					    <div id="lblStatusProgressKojaShipper" style="text-align:center; font-weight:bold; margin-bottom:10px;"></div>
					    
					    <div id="dvProgressKojaAdmin" class="progress active" style="display:none; margin-bottom:0px;">
					      <div id="progress-koja-admin" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					        <span class="sr-only">40% Complete (success)</span>
					      </div>
					    </div>
					    <div id="lblStatusProgressKojaAdmin" style="text-align:center; font-weight:bold; margin-bottom:10px;"></div>
					    <!-- end of progress properties -->
					    
					    
						<!--modal update documentation -->
						<div class="modal fade" id="ModalUpdateDocumentation" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-ban"></i> Failed</h4>
								<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
							</div>
								
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalDocumentation" name="lblTitleModalDocumentation">Edit Data</label></h4>	
							</div>
						    <div class="modal-body">
						    	<div id="dvloaderUpdateNpe" class="loaderModal" style="display:none;"></div>
						    	<label id="lblLoaderModal" class="clblLoaderModal" style="display:none;">please wait, don't refresh the page, this may take several seconds...</label>
								
						    	<input type="hidden" class="form-control" id="txtPortID" name="txtPortID" readonly>
						    	<input type="hidden" class="form-control" id="txtOrderID" name="txtOrderID" readonly>
						    	<input type="hidden" class="form-control" id="txtExportLimit" name="txtExportLimit" readonly>
						    	<input type="hidden" class="form-control" id="txtVoyageClosingDoc" name="txtVoyageClosingDoc" readonly>
						    	
								<div class="form-group">
									<label for="recipient-name" class="control-label">Shipping Instruction</label>
									<input type="text" class="form-control" id="txtSI" name="txtSI" readonly>
								</div>
								<div id="dvPEB" class="form-group">
									<label for="recipient-name" class="control-label">PEB</label><label id="mrkPEB" for="recipient-name" class="control-label"><small>*</small></label>	
									<input type="text" class="form-control" id="txtPEB" name="txtPEB">
								</div>
								<div id="dvNPE" class="form-group">
						          <label class="control-label">NPE:</label><label id="mrkNPE" for="recipient-name" class="control-label"><small>*</small></label>
						          <input type="text" class="form-control" id="txtNPE" name="txtNPE">
						       	</div>
						       	<div id="dvNPWP" class="form-group">
						          <label class="control-label">NPWP:</label><label id="mrkNPWP" for="recipient-name" class="control-label"><small>*</small></label>
						          <select id="slNPWP" name="slNPWP" class="form-control select2" style="width: 100%;">
						          	<c:forEach items="${listNpwp}" var="npwp">
										<option value="<c:out value="${npwp.onbehalfNpwp}" />"><c:out value="${npwp.onbehalfCustomer}" /> - <c:out value="${npwp.onbehalfNpwp}" /></option>
									</c:forEach>
						          </select>
						       	</div>
						       	<div id="dvNPEDate" class="form-group">
						          <label class="control-label">NPE Date:</label><label id="mrkNPEDate" for="recipient-name" class="control-label"><small>*</small></label>
						          <input type="text" class="form-control" id="txtNPEDate" name="txtNPEDate">
						       	</div>
						       	<div id="dvNpeDoc" class="form-group">
						            <label for="lblNpeDoc" class="control-label">PDF Attachment :</label><label id="mrkFile" for="recipient-name" class="control-label"><small>*</small></label>
						            <input type="file" class="form-control" id="txtNpeDoc" name="txtNpeDoc">
						            <input type="hidden" class="form-control" id="hdNpeDoc" name="hdNpeDoc" readonly>
						            <button id="btnNpeDoc" name="btnNpeDoc"
										type="button" class="btn btn-info"
										onclick="showNpeDoc()" style="display:none;"
									>Show Document</button>
						        </div>
				  				<div class="row"></div>
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncSave()">Save</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
							</div>
							</div>
						</div>
						<!-- end of modal update documentation -->
							
							<!-- TABLE DATA -->
							<c:if test="${pol eq ''}">
								<table id="tb_gatepass" class="table table-bordered table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Shipping Instruction</th>
											<th>PEB</th>
											<th>NPE</th>
											<th style="width:135px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listDocumentation}" var ="documentation">
											<tr>
												<td><c:out value="${documentation.shippingInvoiceID}" /></td>
												<td><c:out value="${documentation.peb}" /></td>
												<td><c:out value="${documentation.npe}" /></td>
												<td>
													<button
															type="button" class="btn btn-info"
															data-toggle="modal"
															data-target="#ModalUpdateDocumentation"							
															data-lsi='<c:out value="${documentation.shippingInvoiceID}"/>'
															data-lpeb='<c:out value="${documentation.peb}"/>'
															data-lnpe='<c:out value="${documentation.npe}"/>'
															data-lnpedate='<c:out value="${documentation.npeDate}"/>'
															data-lportid='<c:out value="${documentation.portID}"/>'
															data-lorderid='<c:out value="${documentation.orderManagementID}"/>'
															data-lexportlimit='<c:out value="${documentation.voyageExportLimit}"/>'
															data-lclosingdoc='<c:out value="${documentation.voyageClosingDoc}"/>'
															data-lnpedoc='<c:out value="${documentation.npeDoc}"/>'
															data-lnpwp='<c:out value="${documentation.npwp}"/>'
															>Edit</button>
											        <button type="button" class="btn btn-warning" 
											        		title="Request Yellow Card"
											        		onclick="FuncGetYellowCard('<c:out value="${documentation.shippingInvoiceID}"/>')"
											        		>
											        		Req Gatepass
											        </button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
							<c:if test="${pol ne ''}">
								<table id="tb_gatepass" class="table table-bordered table-hover">
									<thead style="background-color: #d2d6de;">
										<tr>
											<th>Shipping Instruction</th>
											<th>PEB</th>
											<th>NPE</th>
											<c:if test="${globalUserRole eq 'R001'}">
												<th>Transaction</th>
												<th>Proforma</th>
												<th>Invoice</th>
												<th>Billing</th>
											</c:if>
											<th>Status</th>
											<th style="width:135px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listDocumentation}" var ="documentation">
											<tr>
												<td><c:out value="${documentation.shippingInvoiceID}" /></td>
												<td><c:out value="${documentation.peb}" /></td>
												<td><c:out value="${documentation.npe}" /></td>
												<c:if test="${globalUserRole eq 'R001'}">
													<td><c:out value="${documentation.tpsTransactionID}" /></td>
													<td>
														<c:if test="${not empty documentation.tpsProforma}">
														<button
															type="button" id="summary-proforma" class="btn btn-info"
															title="Show Proforma" onclick="showSummary('<c:out value="${documentation.tpsProforma}"/>'
												        								  )"
														>
														<c:out value="${documentation.tpsProforma}" />
														</button>
														</c:if>
													</td>
													<td>
														<c:if test="${not empty documentation.tpsInvoiceNo}">
														<button
															type="button"  class="btn btn-info"
															title="Download Invoice" onclick="downloadInvoice('<c:out value="${documentation.tpsLinkInvoice}"/>'
												        								  )"
														><c:out value="${documentation.tpsInvoiceNo}" /></button>
														</c:if>
													</td>
													<td>
														<fmt:setLocale value="id_ID"/>
														<fmt:formatNumber value="${documentation.tpsBillingAmount}" type="currency"/>
													</td>
												</c:if>
												<td>
													<c:if test="${globalUserRole eq 'R001'}">
														<c:if test="${documentation.tpsBillingStatus eq 0}">
															Unprocess
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 1 || documentation.tpsBillingStatus eq 15}">
															Finish
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 2}">
															Waiiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 13}">
															Waiiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 3}">
															Waiiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 4}">
															Get Billing Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 5}">
															Get Proforma Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 6}">
															Get Proforma Success
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 7}">
															Payment Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 8}">
															Payment Flagging Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 9}">
															Get Invoice Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 10}">
															Get Invoice Success
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 11}">
															Get Gatepass Failed
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 12}">
															Gatepass Issued
														</c:if>
													</c:if>
													
													<c:if test="${globalUserRole eq 'R002'}">
														<c:if test="${documentation.tpsBillingStatus eq 0}">
															Unprocess
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 1 || documentation.tpsBillingStatus eq 15}">
															Finish
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 2}">
															Unprocess
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 13}">
															Unprocess
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 3}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 4}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 5}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 6}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 7}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 8}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 9}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 10}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 11}">
															Waiting Confirmation
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 12}">
															Gatepass Issued
														</c:if>
													</c:if>
												</td>
												<td>
													<c:if test="${globalUserRole eq 'R001'}">
														<c:if test="${documentation.tpsBillingStatus ne 0 && 
															documentation.tpsBillingStatus ne 2 && 
															documentation.tpsBillingStatus ne 13 && 
															documentation.tpsBillingStatus ne 1 && 
															documentation.tpsBillingStatus ne 10 && 
															documentation.tpsBillingStatus ne 11 && 
															documentation.tpsBillingStatus ne 12 &&
															documentation.tpsBillingStatus ne 15}">
															<button
															type="button" class="btn btn-info"
															title="Request Gatepass"
												        		onclick="FuncProcess('<c:out value="${documentation.npe}"/>',
												        								  '<c:out value="${documentation.npeDate}"/>',
												        								  '<c:out value="${documentation.orderManagementID}"/>'
												        								  )"
															>
																<c:if test="${documentation.tpsBillingStatus eq 3}">
																	Process
																</c:if>
																<c:if test="${documentation.tpsBillingStatus eq 6}">
																	Inquiry
																</c:if>
																<c:if test="${documentation.tpsBillingStatus ne 3 &&
																				documentation.tpsBillingStatus ne 6}">
																	Retry
																</c:if>
															</button>
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 10 || documentation.tpsBillingStatus eq 11}">
															<button type="button" class="btn btn-success" 
											        		data-toggle="modal"
															data-target="#ModalGetOTP"	
											        		title="Get otp"
											        		onclick="showFormGetOTP('<c:out value="${documentation.orderManagementID}"/>')"
											        		>
											        		Get OTP
											        		</button>
														</c:if>
														<c:if test="${documentation.tpsBillingStatus eq 12}">
															<button type="button" class="btn btn-success" 
											        		data-toggle="modal"
															data-target="#ModalGetOTP"	
											        		title="Reassociate if there is TID change request from trucker"
											        		onclick="showFormGetOTP('<c:out value="${documentation.orderManagementID}"/>')"
											        		>
											        		Reassociate
											        		</button>
											        	</button>
														</c:if>
													</c:if>
													<c:if test="${globalUserRole ne 'R001'}">
														<c:if test="${documentation.tpsBillingStatus eq 0}">
														<button
															type="button" class="btn btn-info"
															data-toggle="modal"
															data-target="#ModalUpdateDocumentation"							
															data-lsi='<c:out value="${documentation.shippingInvoiceID}"/>'
															data-lpeb='<c:out value="${documentation.peb}"/>'
															data-lnpe='<c:out value="${documentation.npe}"/>'
															data-lnpedate='<c:out value="${documentation.npeDate}"/>'
															data-lportid='<c:out value="${documentation.portID}"/>'
															data-lorderid='<c:out value="${documentation.orderManagementID}"/>'
															data-lexportlimit='<c:out value="${documentation.voyageExportLimit}"/>'
															data-lclosingdoc='<c:out value="${documentation.voyageClosingDoc}"/>'
															data-lnpedoc='<c:out value="${documentation.npeDoc}"/>'
															data-lnpwp='<c:out value="${documentation.npwp}"/>'
															>Edit</button>
														</c:if>
														<c:if test="${(documentation.tpsBillingStatus eq 0 || documentation.tpsBillingStatus eq 2 || documentation.tpsBillingStatus eq 13) && documentation.npe ne ''}">
															<button type="button" class="btn btn-warning" 
												        		title="Request Gatepass"
												        		onclick="FuncGetOnDemand('<c:out value="${documentation.npe}"/>',
												        								  '<c:out value="${documentation.npeDate}"/>',
												        								  '<c:out value="${documentation.orderManagementID}"/>',
												        								  '<c:out value="${documentation.voyageExportLimit}"/>'
												        								  )"	
												        		>
												        		<!-- FuncGetPayBilling -->
												        		Req Gatepass
												        	</button>
														</c:if>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>
							
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
 		
  		$("#tb_gatepass").DataTable({
  			"aaSorting": [],
  			"scrollX":true
			});
			
  		$('#M022').addClass('active');
  		
  		$('#txtNPEDate').datepicker({
	      format: 'dd M yyyy',
	      setDate: new Date(),
	      autoclose: true
	    });
	    
  		$("#dvErrorAlert").hide();
  	});
  	
  	function FuncClear(){
		$('#mrkPEB').hide();
		$('#mrkNPE').hide();
		$('#mrkNPEDate').hide();
		$('#mrkFile').hide();
		$('#mrkNPWP').hide();
		
		$('#dvPEB').removeClass('has-error');
		$('#dvNPE').removeClass('has-error');
		$('#dvNPEDate').removeClass('has-error');
		$('#dvNpeDoc').removeClass('has-error');
		$('#dvNPWP').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	//Modal Update Documentaton
	$('#ModalUpdateDocumentation').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
	
 		var button = $(event.relatedTarget);
 		var lSI = button.data('lsi');
 		var lPEB = button.data('lpeb');
 		var lNPE = button.data('lnpe');
 		var lNPEDate = button.data('lnpedate');
 		var lPortID = button.data('lportid');
 		var lNPEDoc = button.data('lnpedoc');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtSI").val(lSI);
		modal.find(".modal-body #txtPEB").val(lPEB);
		modal.find(".modal-body #txtNPE").val(lNPE);
		modal.find(".modal-body #txtNPEDate").val(lNPEDate);
		modal.find(".modal-body #txtPortID").val(lPortID);
		modal.find(".modal-body #txtOrderID").val(button.data('lorderid'));
		modal.find(".modal-body #txtExportLimit").val(button.data('lexportlimit'));
		modal.find(".modal-body #txtVoyageClosingDoc").val(button.data('lclosingdoc'));
		modal.find(".modal-body #slNPWP").val(button.data('lnpwp')).trigger('change.select2');
		
		if(lNPEDoc == ''){
			$("#btnNpeDoc").hide();
		}
		else{
			modal.find(".modal-body #hdNpeDoc").val(lNPEDoc);
			$("#btnNpeDoc").show();
		}
 		
 		$('#txtPEB').focus();
 		
 		FuncClear();
	})
	
	function FuncSave() {
		$("#dvloaderUpdateNpe").show();
		$("#lblLoaderModal").show();
		
		var txtSI = document.getElementById('txtSI').value;
		var txtPEB = document.getElementById('txtPEB').value;
		var txtNPE = document.getElementById('txtNPE').value;
		var txtNPEDate = document.getElementById('txtNPEDate').value;
		var txtPortID = document.getElementById('txtPortID').value;
		var txtOrderID = document.getElementById('txtOrderID').value;
		var txtExportLimit = document.getElementById('txtExportLimit').value;
		var txtVoyageClosingDoc = document.getElementById('txtVoyageClosingDoc').value;
		
		var txtNpeDoc = document.getElementById('txtNpeDoc').value;
		var slNPWP = document.getElementById('slNPWP').value;
		
		if(!txtNpeDoc.match(/\S/)) {
	    	$("#txtNpeDoc").focus();
	    	$('#dvNpeDoc').addClass('has-error');
	    	$('#mrkFile').show();
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
	    }
	    
		//check file docs extension
        var fileName = txtNpeDoc;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
        if(ext == "pdf" )
        {
        } 
        else
        {
        	$("#txtNpeDoc").focus();
	    	$('#dvNpeDoc').addClass('has-error');
	    	$('#mrkFile').show();
	    	alert("Upload pdf files only");
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
        }
        //end of check file docs extension
	    
	    if(!txtPEB.match(/\S/)) {
	    	$("#txtPEB").focus();
	    	$('#dvPEB').addClass('has-error');
	    	$('#mrkPEB').show();
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
	    } 
	    
	    if(!txtNPE.match(/\S/)) {
	    	$("#txtNPE").focus();
	    	$('#dvNPE').addClass('has-error');
	    	$('#mrkNPE').show();
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
	    }
	    
	    if(!txtNPEDate.match(/\S/)) {
	    	$("#txtNPEDate").focus();
	    	$('#dvNPEDate').addClass('has-error');
	    	$('#mrkNPEDate').show();
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
	    }
	    
	    if(!slNPWP.match(/\S/)) {
	    	$("#slNPWP").focus();
	    	$('#dvNPWP').addClass('has-error');
	    	$('#mrkNPWP').show();
	    	$("#dvloaderUpdateNpe").hide();
			$("#lblLoaderModal").hide();
	        return false;
	    }
	    
	    // Get form
		var form = $('#Form_Gatepass')[0];
		// Create an FormData object 
		var dataForm = new FormData(form);
		var key = "UpdateDocumentation";
		// If you want to add an extra field for the FormData
		dataForm.append("key", key);
		//ingat nambah anotasi @MultipartConfig di servlet
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Gatepass',	
	        type:'POST',
	        enctype: 'multipart/form-data',
	        /* data:{"key":"UpdateDocumentation","txtSI":txtSI,"txtPEB":txtPEB,"txtNPE":txtNPE,
	        	  "txtNPEDate":txtNPEDate, "txtPortID":txtPortID, "txtOrderID":txtOrderID, 
	        	  "txtExportLimit":txtExportLimit, "txtVoyageClosingDoc":txtVoyageClosingDoc}, */
	        data: dataForm,
	        processData: false,
	        contentType: false,
	        cache: false,
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedUpdateDocumentation')
	        	{
	        		$("#dvloaderUpdateNpe").hide();
					$("#lblLoaderModal").hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtPEB").focus();
	        		$("#ModalUpdateDocumentation").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		var url = '${pageContext.request.contextPath}/Gatepass';  
					$(location).attr('href', url);	
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	        	$("#dvloaderUpdateNpe").hide();
				$("#lblLoaderModal").hide();
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    return true;
	}
	
	
	function FuncGetYellowCard(lParamSI) {
		$("#dvloader").show();
		$("#lblLoader").show();
		
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Gatepass',	
	        type:'POST',
	        data:{"key":"GetYellowCard","txtSI":lParamSI},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	var url = '${pageContext.request.contextPath}/Gatepass';  
				$(location).attr('href', url);				
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	            $("#dvloader").hide();
	            $("#lblLoader").hide();
	        }
	    });
	    
	    FuncClear();
	    return true;
	}
	
	function FuncButtonSearch(lParambtn) {
	 	var Pol = document.getElementById('slPolSearch').value;
	 	
	 	jQuery.ajax({
		        url:'${pageContext.request.contextPath}/Gatepass',	
		        type:'POST',
		        data:{"key":lParambtn,"pol":Pol},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/Gatepass';  
	 	        	$(location).attr('href', url);
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    return true;
	}
	
	
	
	/* KOJA SCRIPT */
	/* ---------------- */
	var sessionKoja = '';
	var kojaUsername = document.getElementById("kojaUsername").value;
	var kojaPassword = document.getElementById("kojaPassword").value;
	var kojaStreamUsername = document.getElementById("kojaStreamUsername").value;
	var kojaStreamPassword = document.getElementById("kojaStreamPassword").value;
	var kojaDevicename = document.getElementById("kojaDevicename").value;
	var kojaUrl = document.getElementById("kojaUrl").value;
	
	var soapMessageGetLogout = '';
	var soapMessageConfirmTransaction = '';
	var soapMessageGetBilling = '';
	var soapMessageGetProforma = '';
	var soapMessageGetInvoice = '';
	
	var soapMessageGetLogin = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:AUTH_GetLoginwsdl">'+
		 '<soapenv:Header/>'+
		   '<soapenv:Body>'+
		      '<urn:AUTH_GetLogin soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
		         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
		         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
		         '<fStream xsi:type="xsd:string">{"PASSWORD":"'+kojaStreamPassword+'","USERNAME":"'+kojaStreamUsername+'"}</fStream>'+
		         '<deviceName xsi:type="xsd:string">'+kojaDevicename+'</deviceName>'+
		      '</urn:AUTH_GetLogin>'+
		   '</soapenv:Body>'+
		'</soapenv:Envelope>';
		
	function reSoapMessageGetLogout(sessionKoja){
		soapMessageGetLogout = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:AUTH_GetLogOutwsdl">'+
			 '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:AUTH_GetLogOut soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<fStream xsi:type="xsd:string">{"SESSIONID":"'+sessionKoja+'"}</fStream>'+
			      '</urn:AUTH_GetLogOut>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function reSoapMessageConfirmTransaction(lparam){
		soapMessageConfirmTransaction = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:BILLING_ConfirmTransactionwsdl">'+
			 '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:BILLING_ConfirmTransaction soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<Creator xsi:type="xsd:string">'+lparam.sessionID+'</Creator>'+
			         '<fStream xsi:type="xsd:string">{"CERTIFICATED_ID":[""],"OLD_COMPANY_CODE":"","CUST_ID":"'+lparam.customerID+'",'+
			         	'"REFEER_TEMPERATURE":[""],"ISO_CODE":['+lparam.isoCode+'],"TRANSACTIONS_TYPE_ID":"5",'+
			         	'"VOLTAGE_PLUG":[""],"TGL_NHI":"","OVER_RIGHT":[""],"DOCUMENT_SHIPPING_NO":"'+lparam.documentShippingNo+'",'+
			         	'"OLD_POD":[""],"OLD_VOYAGE_NO":"","START_PLUG":[],"OVER_LEFT":[""],"OWNER":["'+lparam.shippingLine+'"],'+
			         	'"CUSTOMS_DOCUMENT_ID":"6","NO_BL_AWB":"'+lparam.noBlAwb+'","WEIGHT_VGM":[""],"OLD_NO_CONT":[""],'+
			         	'"DOCUMENT_SHIPPING_DATE":"'+lparam.documentShippingDate+'","VOYAGE_NO":"'+lparam.voyageNo+'","COMPANY_CODE":"'+lparam.voyageCompanyCode+'",'+
			         	'"NPWP_SERTIFICATED":[""],"WEIGHT":['+lparam.weight+'],"CERTIFICATED_PIC":[""],'+
			         	'"OLD_VESSEL_ID":"","IMO_CODE":[""],"UN_NUMBER":[""],"DOCUMENT_NO":"'+lparam.documentNo+'",'+
			         	'"VESSEL_ID":"'+lparam.vesselID+'","POD":["'+lparam.pod+'"],"CUST_SERTIFICATED":[""],"OLD_FD":[""],'+
			         	'"DOCUMENT_DATE":"'+lparam.documentDate+'","STOP_PLUG":[""],"OVER_FRONT":[""],"OVER_BACK":[""],'+
			         	'"CUST_ID_PPJK":"","PAID_THRU":"","POL":["'+lparam.pol+'"],"TGL_BK_SEGEL_NHI":"","OLD_INVOICE_NO":"",'+
			         	'"OVER_HEIGHT":[""],"FD":["'+lparam.fd+'"],"NO_CONT":['+lparam.containerNumber+']}'+
			         '</fStream>'+
			      '</urn:BILLING_ConfirmTransaction>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function reSoapMessageGetBilling(sessionKoja, transactionID){
		soapMessageGetBilling = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:BILLING_GetBillingwsdl">'+
			   '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:BILLING_GetBilling soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<Creator xsi:type="xsd:string">'+sessionKoja+'</Creator>'+
			         '<fStream xsi:type="xsd:string">{"TRANSACTION_ID":"'+transactionID+'"}</fStream>'+
			      '</urn:BILLING_GetBilling>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function reSoapMessageGetProforma(sessionKoja, transactionID){
		soapMessageGetProforma = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:BILLING_GetProformawsdl">'+
			   '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:BILLING_GetProforma soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<Creator xsi:type="xsd:string">'+sessionKoja+'</Creator>'+
			         '<fStream xsi:type="xsd:string">{"TRANSACTION_ID":"'+transactionID+'"}</fStream>'+
			      '</urn:BILLING_GetProforma>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function reSoapMessageGetInvoice(sessionKoja, transactionID, proformaNo){
		soapMessageGetInvoice = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:BILLING_GetInvoicewsdl">'+
			   '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:BILLING_GetInvoice soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">LOGOL</UserName>'+
			         '<Password xsi:type="xsd:string">LOGOL456</Password>'+
			         '<Creator xsi:type="xsd:string">'+sessionKoja+'</Creator>'+
			         '<fStream xsi:type="xsd:string">{"TRANSACTION_ID":"'+transactionID+'","PROFORMA_INVOICE_NO":"'+proformaNo+'"}</fStream>'+
			      '</urn:BILLING_GetInvoice>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function ajaxGetPayBilling(lparam){
		//store param to soap message confirm transaction
		reSoapMessageConfirmTransaction(lparam);
		
		$.ajax({ // call confirm transaction
			    url: kojaUrl,
			    type: "POST",
			    dataType: "xml",
			    contentType: "text/xml; charset=\"utf-8\"",
			    data: soapMessageConfirmTransaction,
			    success: function(soapResponse){
			        var soap = soapResponse.documentElement;
					var result = soap.getElementsByTagName("return");
					if(result[0].innerHTML == null){
					//if confirm transaction return null, recall main ajax function
						ajaxGetPayBilling(lparam);
					}
					var status = JSON.parse(result[0].innerHTML).STATUS;
					if(status == 'FALSE'){
						//if session expired, logout session
						reSoapMessageGetLogout(lparam.sessionID);
						$.ajax({
						    url: kojaUrl,
						    type: "POST",
						    dataType: "xml",
						    contentType: "text/xml; charset=\"utf-8\"",
						    data: soapMessageGetLogout,
						    success: function(soapResponse){
						         var soap = soapResponse.documentElement;
								 var result = soap.getElementsByTagName("return");
								
								//relogin to get new session
								 $.ajax({
								    url: kojaUrl,
								    type: "POST",
								    dataType: "xml",
								    contentType: "text/xml; charset=\"utf-8\"",
								    data: soapMessageGetLogin,
								    success: function(soapResponse){
								         var soap = soapResponse.documentElement;
										 var result = soap.getElementsByTagName("return");
										 var newSession = JSON.parse(result[0].innerHTML).SESSIONID;
										 var newCustID = JSON.parse(result[0].innerHTML).CUST_ID;
										
										 if(newSession != undefined){
											//store new session in db
											$.ajax({
										          url: '${pageContext.request.contextPath}/KojaTransaction',
										          type: 'POST',
										          data: {"key":'updateSession', "sessionKoja": newSession, "custID": newCustID},
										          dataType: 'text'
										     })
										     .done(function(data){
										     	//after saving new session recall the ajax get pay billing function
										     	lparam.sessionID = newSession;
										     	lparam.customerID = newCustID;
												ajaxGetPayBilling(lparam);
										     })
										     .fail(function(){
										     	console.log('system error');
										     });
										}
										else{
											$("#dvloader").hide();
											alert("Get session failed on confirm transaction");
										}	 
								    }
								});
								//end of relogin
						    }
						});
						//end of logout proccess
					}
					else{
					//if session active, get confirm transaction data
						var transactionid = JSON.parse(result[0].innerHTML).TRANSACTION_ID;
						
						var dateNow = new Date();
						Number.prototype.padLeft = function(base,chr){
						    var  len = (String(base || 10).length - String(this).length)+1;
						    return len > 0? new Array(len).join(chr || '0')+this : this;
						}
				 		var transactionDate = [ (dateNow.getMonth()+1).padLeft(),
				 								dateNow.getDate().padLeft(),
				 								dateNow.getHours().padLeft(),
				 								dateNow.getMinutes().padLeft(),
				 								dateNow.getSeconds().padLeft()
				 							   ].join('');
				 		
						reSoapMessageGetBilling(lparam.sessionID, transactionid);
					 
					     //get billing to get proforma
						 $.ajax({
						    url: kojaUrl,
						    type: "POST",
						    dataType: "xml",
						    contentType: "text/xml; charset=\"utf-8\"",
						    data: soapMessageGetBilling,
						    success: function(soapResponse){
						         var soap = soapResponse.documentElement;
								 var result = soap.getElementsByTagName("return");
								 var proformaNo = JSON.parse(result[0].innerHTML).PROFORMA_INVOICE_NO;
								 reSoapMessageGetProforma(lparam.sessionID, transactionid);
								 
								 //get proforma to confirm the invoice before payment
								 $.ajax({
								    url: kojaUrl,
								    type: "POST",
								    dataType: "xml",
								    contentType: "text/xml; charset=\"utf-8\"",
								    data: soapMessageGetProforma,
								    success: function(soapResponse){
								         var soap = soapResponse.documentElement;
										 var result = soap.getElementsByTagName("return");
									 	 
									 	 //billing process (start from request inquiry to payment flagging)
									 	 var dateNow2 = new Date();
									 	 var transmissionDate = [ (dateNow2.getMonth()+1).padLeft(),
								 								dateNow2.getDate().padLeft(),
								 								dateNow2.getHours().padLeft(),
								 								dateNow2.getMinutes().padLeft(),
								 								dateNow2.getSeconds().padLeft()
								 							   ].join('');
	 	
										$.ajax({
									          url: '${pageContext.request.contextPath}/KojaTransaction',
									          type: 'POST',
									          data: {"key":'billingProcess', "sessionID":lparam.sessionID, 
									          		"sessionCustID":lparam.customerID, "orderID":lparam.orderID,
									          		"transactionID":transactionid, "transactionDate":transactionDate, 
									          		"requestDateTime":transmissionDate, "proformaNo":proformaNo},
									          dataType: 'text'
									     })
									     .done(function(data){
									     	//after billing proccessing, do get invoice
									     	ajaxGetInvoice(lparam, transactionid, proformaNo);
									     })
									     .fail(function(){
									     	console.log('system error');
									     });
									     //end of billing process
								    }
								});
								//end of get proforma								
						    }
						});
						//end of get billing	
				  	 }
			   	 }
			}); //end of confirm transaction
	}//end of function
	
	
	function ajaxGetInvoice(lparam, transactionid, proformaNo){
		//get invoice
     	reSoapMessageGetInvoice(lparam.sessionID, transactionid, proformaNo);
		$.ajax({
		    url: kojaUrl,
		    type: "POST",
		    dataType: "xml",
		    contentType: "text/xml; charset=\"utf-8\"",
		    data: soapMessageGetInvoice,
		    success: function(soapResponse){
		         var soap = soapResponse.documentElement;
				 var result = soap.getElementsByTagName("return");
				 var message = JSON.parse(result[0].innerHTML).MESSAGE;
				 
				 if(message == 'Session expired.'){
				 	//if session expired, logout session
					reSoapMessageGetLogout(lparam.sessionID);
					$.ajax({
					    url: kojaUrl,
					    type: "POST",
					    dataType: "xml",
					    contentType: "text/xml; charset=\"utf-8\"",
					    data: soapMessageGetLogout,
					    success: function(soapResponse){
					         var soap = soapResponse.documentElement;
							 var result = soap.getElementsByTagName("return");
							
							//relogin to get new session
							 $.ajax({
							    url: kojaUrl,
							    type: "POST",
							    dataType: "xml",
							    contentType: "text/xml; charset=\"utf-8\"",
							    data: soapMessageGetLogin,
							    success: function(soapResponse){
							         var soap = soapResponse.documentElement;
									 var result = soap.getElementsByTagName("return");
									 var newSession = JSON.parse(result[0].innerHTML).SESSIONID;
									 var newCustID = JSON.parse(result[0].innerHTML).CUST_ID;
									
									 if(newSession != undefined){
										//store new session in db
										$.ajax({
									          url: '${pageContext.request.contextPath}/KojaTransaction',
									          type: 'POST',
									          data: {"key":'updateSession', "sessionKoja": newSession, "custID": newCustID},
									          dataType: 'text'
									     })
									     .done(function(data){
									     	//after saving new session recall the ajax get invoice function
									     	lparam.sessionID = newSession;
									     	lparam.customerID = newCustID;
											ajaxGetInvoice(lparam, transactionid, proformaNo);
									     })
									     .fail(function(){
									     	console.log('system error');
									     });
									}
									else{
										$("#dvloader").hide();
										alert("Get session failed on get invoice");
									}	 
							    }
							});
							//end of relogin
					    }
					});
					//end of logout proccess
				 }
				 else{
					//if session active, associate e ticket
					$.ajax({
				          url: '${pageContext.request.contextPath}/KojaTransaction',
				          type: 'POST',
				          data: {"key":'associateTicket', "orderID":lparam.orderID},
				          dataType: 'text'
				     })
				     .done(function(data){
				     	//if success, do this
				     	$("#dvloader").hide();
				     	alert(data);
				     	
				     	//reload page
				     	var url = '${pageContext.request.contextPath}/Gatepass';  
	 	        		$(location).attr('href', url);
				     })
				     .fail(function(){
				     	console.log('system error');
				     	$("#dvloader").hide();
				     });
					//end of associate e ticket
				}
			}
		});	
     	//end of get invoice
	}
		
		
	function FuncGetPayBilling(paramOrderID) {
		$("#dvloader").show();
		/* $("#dvErrorAlertService").hide(); */
				    
		//get object param for confirm transaction
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'getConfirmTransactionParam',"OrderID":paramOrderID},
	          dataType: 'json'
	     })
	     .done(function(data){
	     	//data = confirm transaction param
	     	ajaxGetPayBilling(data); /* call ajax get pay billing function */
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloader").hide();
	     });
	}
	
	function setProgressBarKojaShipper(paramPercentage, paramStatus) {
		$('#progress-koja-shipper').width(paramPercentage+'%');
		
		$('#lblStatusProgressKojaShipper').html(paramPercentage + '% - ' + paramStatus);
	}
			
	function FuncGetOnDemand(paramNPE, paramNPEDate, paramOrderID, paramExportLimit) {
		/* $("#dvloader").show(); */
		$("#lblLoader").show();
		$("#dvProgressKojaShipper").show();
		var refreshprogessbar = setInterval(checkProgressKojaShipper, 1000);
		/* $("#dvErrorAlertService").hide(); */
				    
		//get on demand process
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'getOnDemand',"npe":paramNPE, "npeDate":paramNPEDate, 
	          		"orderID": paramOrderID, "exportLimit":paramExportLimit},
	          dataType: 'text'
	     })
	     .done(function(data){
	     	//data = return result
	     	/* $("#dvloader").hide(); */
	     	$("#lblLoader").hide();
	     	alert(data);
	     	
	     	//reload page
	     	var url = '${pageContext.request.contextPath}/Gatepass';  
        	$(location).attr('href', url);
        })
        .fail(function(){
	     	console.log('system error');
	     	/* $("#dvloader").hide(); */
	     	$("#lblLoader").hide();
	     });
	     
	    function checkProgressKojaShipper() {/*pass the actual id*/
			    $.getJSON('progressKojaShipper?orderID='+paramOrderID, function(progress) {
			        if(progress.percentage == 100){clearInterval(refreshprogessbar);}
			        console.log(progress.status);
			        setProgressBarKojaShipper(progress.percentage, progress.status);
			    });
			}
     }

	function showFormGetOTP(param){
		$("#dvloaderGetOTP").show();
		$("#lblErrorGetOTP").hide();
		
		$.ajax({
	          url: '${pageContext.request.contextPath}/Gatepass',
	          type: 'POST',
	          data: {"key":'getContainer',"OrderID":param},
	          dataType: 'json'
	     })
	     .done(function(data){
	     	
	     	var html="";
			var loops = 3;
	     	console.log("success")
	     	console.log(data[0])
	     	
	     	for (var i = 0; i < data.length; i++) {
	     		if(i==0){
	     			html += '<div class="group-pin form-group col-md-4">'+
						  '<label for="recipient-name" class="control-label">PIN:</label><label for="pin" class="control-label"><small>*</small></label>'+
						  '<input type="text" class="pin form-control" name="pin">'+
						'</div>'+
						'<div class="group-container form-group  col-md-8">'+
						  '<label class="control-label">Container:</label><label for="recipient-name" class="control-label"><small>*</small></label>'+
						  '<input type="text" class="container form-control" name="container" value="'+data[i]+'">'+
						'</div>';
	     		}
	     		else{
	     			html += '<div class="group-pin form-group col-md-4">'+
						'</div>'+
						'<div class="group-container form-group  col-md-8">'+
						  '<label class="control-label">Container:</label><label for="recipient-name" class="control-label"><small>*</small></label>'+
						  '<input type="text" class="container form-control" name="container" value="'+data[i]+'">'+
						'</div>';
	     		}
			}
			
			$("#get-otp").html(html);
			$("#tempOrderID").val(param);
			$("#dvloaderGetOTP").hide();
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloaderGetOTP").hide();
	     });
	}
	
	function setProgressBarKojaAdmin(paramPercentage, paramStatus) {
		$('#progress-koja-admin').width(paramPercentage+'%');
		
		$('#lblStatusProgressKojaAdmin').html(paramPercentage + '% - ' + paramStatus);
	}
			
	function FuncProcess(paramNPE, paramNPEDate, paramOrderID) {
		/* $("#dvloader").show(); */
		$("#lblLoader").show();
		$("#dvProgressKojaAdmin").show();
		var refreshprogessbar = setInterval(checkProgressKojaAdmin, 1000);
		/* $("#dvErrorAlertService").hide(); */
				    
		//get on demand process
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'mainProcess',"npe":paramNPE, "npeDate":paramNPEDate, "orderID": paramOrderID},
	          dataType: 'text'
	     })
	     .done(function(data){
	     	//data = return result
	     	/* $("#dvloader").hide(); */
	     	$("#lblLoader").hide();
	     	alert(data);
	     	
	     	//reload page
	     	var url = '${pageContext.request.contextPath}/Gatepass';  
        	$(location).attr('href', url);
	     })
	     .fail(function(){
	     	console.log('system error');
	     	/* $("#dvloader").hide(); */
	     	$("#lblLoader").hide();
	     });
	     
	     function checkProgressKojaAdmin() {/*pass the actual id*/
			    $.getJSON('progressKojaAdmin?orderID='+paramOrderID, function(progress) {
			        if(progress.percentage == 100){clearInterval(refreshprogessbar);}
			        console.log(progress.status);
			        setProgressBarKojaAdmin(progress.percentage, progress.status);
			    });
			}
	}
	
	var arrPin= [];
	var arrContainer = [];
	
	function FuncGetOTP(){
		$("#dvloaderGetOTP").show();
		$("#lblLoaderGetOTP").show();
		
		/* $('#get-otp > .group-pin').children('input.pin').each(function () {
			arrPin= [];
			arrPin.push(this.value)
		}) */
		arrContainer = [];
		arrPin= [];
		$('#get-otp > .group-container').children('input.container').each(function () {
			arrContainer.push(this.value);
			
			$('#get-otp > .group-pin').children('input.pin').each(function () {
				arrPin.push(this.value);
			});
		});
		
		var orderID = document.getElementById("tempOrderID").value;
		
		console.log(arrPin);
		console.log(arrContainer);
		
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'getOTP',"pin":JSON.stringify(arrPin),"container":JSON.stringify(arrContainer),"orderID":orderID},
	          dataType: 'text'
	     })
	     .done(function(data){
	     	$("#lblLoaderGetOTP").hide();
	     	
	     	if(data.split("--")[0]=='Sukses'){
	     		$("#dvloaderGetOTP").hide();
	     		$("#lblErrorGetOTP").hide();
		     	$('#ModalGetOTP').modal('hide')
		     	$('#ModalSendOTP').modal('show')
		     	$("#lblErrorSendOTP").hide();
		     	/* $("#otp").val(data.split("--")[1]); for automatic send otp after fetching*/ 
	     	}
	     	else{
	     		$("#dvloaderGetOTP").hide();
	     		$("#lblErrorGetOTP").show();
	     	}
	     	console.log(data)
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloaderGetOTP").hide();
	     	$("#lblErrorGetOTP").hide();
	     	$("#lblLoaderGetOTP").hide();
	     });
	}
	
	function FuncSendOTP(){
		$("#dvloaderSendOTP").show();
		$("#lblLoaderSendOTP").show();
			
		var otp = $('input#otp').val();
		var orderID = document.getElementById("tempOrderID").value;
		console.log(arrContainer)
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'sendOTP',"otp":otp, "container":JSON.stringify(arrContainer), "orderID":orderID},
	          dataType: 'text'
	     })
	     .done(function(data){
	     	if(data == 'Sukses'){
	     		$("#dvloaderSendOTP").hide();
	     		$("#lblLoaderSendOTP").hide();
	     		$("#lblErrorSendOTP").hide();
	     		alert(data);
	     	
		     	//reload page
		     	var url = '${pageContext.request.contextPath}/Gatepass';  
	        	$(location).attr('href', url);
	     	}
	     	else{
	     		$("#dvloaderSendOTP").hide();
	     		$("#lblLoaderSendOTP").hide();
	     		$("#lblErrorSendOTP").show();
	     	}
	     	console.log(data)
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloaderSendOTP").hide();
	     	$("#lblLoaderSendOTP").hide();
	     	$("#lblErrorGetOTP").hide();
	     });
	}
	
	
	function showSummary(param) {
		$.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'summaryProformaDetail',"proformaID":param},
	          dataType: 'json'
	     })
	     .done(function(data){
	     	console.log(data)
	     	
	     	var html = '';
	     	var summaryDetailHtml = '';
	     	var summaryTransactionHtml = '';
	     	
	     	var summaryDetailValue = data.SUMMARY_DETAIL
	     	var summaryTransactionValue = data.SUMMARY_TRANSACTIONS;
	     	
	     	var subTotal = null;
	     	
	     	for (var i = 0; i < summaryDetailValue["COMPONENT_TARIFF_NAME"].length; i++) {
	     		summaryDetailHtml += '<tr>'+
							     	  '<td>'+summaryDetailValue["COMPONENT_TARIFF_NAME"][i]+'</td>'+
							     	  '<td style="text-align:right">Rp '+summaryDetailValue["TOTAL"][i].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
							     	'</tr>';
							     	
	     		subTotal += parseInt(summaryDetailValue["TOTAL"][i], 10);
	     	}
	     	
	     	for (var i = 0; i < summaryTransactionValue["COMPONENT_TARIFF_NAME"].length; i++) {
	     		summaryTransactionHtml += '<tr>'+
							     	  '<td>'+summaryTransactionValue["COMPONENT_TARIFF_NAME"][i]+'</td>'+
							     	  '<td style="text-align:right">Rp '+summaryTransactionValue["TOTAL"][i].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td>'+
							     	'</tr>';
							     	
	     		subTotal += parseInt(summaryDetailValue["TOTAL"][i], 10);
			}
	     	
	     	var ppnHtml = '<tr>'+
					     	  '<th scope="row">SUBTOTAL</th>'+
					     	  '<th colspan="2" style="text-align:right">Rp '+data["SUBTOTAL"].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</th>'+
					     	'</tr>'+
					     	'<tr>'+
					     	  '<th scope="row">PPN</th>'+
					     	  '<th colspan="2" style="text-align:right">Rp '+data["PPN"].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</th>'+
					     	'</tr>'+
					     	'<tr>'+
					     	  '<th scope="row">TOTAL</th>'+
					     	  '<th colspan="2" style="text-align:right">Rp '+data["TOTAL"].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</th>'+
					     	'</tr>';
	     	
	     	
			html = summaryDetailHtml+summaryTransactionHtml+ppnHtml;
					     	
	     	$('#tbody-proforma').html(html)
	     	$('#ModalSummaryProforma').modal('show');
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloaderGetOTP").hide();
	     });
	}
	
	
	function downloadInvoice(param) {
		/* $.ajax({
	          url: '${pageContext.request.contextPath}/KojaTransaction',
	          type: 'POST',
	          data: {"key":'downloadInvoice',"proformaID":param},
	          dataType: 'json'
	     })
	     .done(function(data){
	    	 console.log(data)
	    	 window.open(data.DETAIL_BILLING.LINK, "_blank");
	     })
	     .fail(function(){
	     	console.log('system error');
	     	$("#dvloaderGetOTP").hide();
	     }); */
	     window.open(param, "_blank");
	}
	
	function showNpeDoc() {
	   var npeDocLink = document.getElementById("hdNpeDoc").value;
	   window.open(npeDocLink, "_blank");
	}
	</script>

</body>
</html>