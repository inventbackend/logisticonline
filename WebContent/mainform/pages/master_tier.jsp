<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Tier</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Tier" action = "${pageContext.request.contextPath}/Tier" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Tier
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertTier'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan tier.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateTier'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui tier.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteTier'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus tier.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteTier'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus tier. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
		          <div id="dvTierID" class="form-group">
		            <label for="lblTierID" class="control-label">Tier ID:</label><label id="mrkTierID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtTierID" name="txtTierID" readonly disabled>
		          </div>
		          <div id="dvDescription" class="form-group">
		            <label for="lblDescription" class="control-label">Name:</label><label id="mrkDescription" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDescription" name="txtDescription">
		          </div>
		          <div id="dvLongDescription" class="form-group">
		            <label for="lblLongDescription" class="control-label">Description:</label>		            
				    <textarea class="form-control" style="resize:none" rows="5" id="txtLongDescription" name="txtLongDescription"></textarea>
		          </div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Tier</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtTierID" name="temp_txtTierID"  />
									<p>Are you sure to delete this tier ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_tier" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>ID</th>
	                  <th>Name</th>
	                  <th>Description</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listTier}" var ="tier">
		        	<tr>
		        	<td><c:out value="${tier.tierID}"/></td>
					<td><c:out value="${tier.description}"/></td>
					<td><c:out value="${tier.longDescription}"/></td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-ltierid='<c:out value="${tier.tierID}"/>'
								data-ldescription='<c:out value="${tier.description}"/>' 
								data-llongdescription='<c:out value="${tier.longDescription}"/>' 
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-ltierid='<c:out value="${tier.tierID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_tier').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
	$('#M012').addClass('active');
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lTierID = button.data('ltierid');
		$("#temp_txtTierID").val(lTierID);
	})
	</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var ltierid = button.data('ltierid') // Extract info from data-* attributes
	  var ldescription = button.data('ldescription')
	  var llongdescription = button.data('llongdescription') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	//   modal.find('.modal-title').text('New message to ' + recipient)
	  modal.find('.modal-body #txtTierID').val(ltierid)
	  modal.find('.modal-body #txtDescription').val(ldescription)
	  modal.find('.modal-body #txtLongDescription').val(llongdescription)
	  
	  $("#txtDescription").focus();
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

	function FuncClear(){
		$('#mrkTierID').hide();
		$('#mrkDescription').hide();
		
		$('#dvtxtTierID').removeClass('has-error');
		$('#dvDescription').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#dvTierID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Tier";	
	
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		$('#dvTierID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Tier';
	
		FuncClear();
	}
	
	function FuncDelete() {
		var temp_txtTierID = document.getElementById('temp_txtTierID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Tier',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtTierID":temp_txtTierID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/Tier';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtTierID = document.getElementById('txtTierID').value;
		var txtDescription = document.getElementById('txtDescription').value;
		var txtLongDescription = document.getElementById('txtLongDescription').value;
	
		var dvTierID = document.getElementsByClassName('dvTierID');
		var dvDescription = document.getElementsByClassName('dvDescription');
	    
	    if(!txtDescription.match(/\S/)) {    	
	    	$('#txtDescription').focus();
	    	$('#dvDescription').addClass('has-error');
	    	$('#mrkDescription').show();
	        return false;
	    } 
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Tier',	
	        type:'POST',
	        data:{"key":lParambtn,"txtTierID":txtTierID,"txtDescription":txtDescription,"txtLongDescription":txtLongDescription},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertTier')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan tier";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDescription").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateTier')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui tier";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDescription").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Tier';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var tierid = document.getElementById('txtTierID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (tierid == null || tierid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (tierid != null || tierid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>