<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Order Management</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
	<style type="text/css">	
	
		#ModalUpdateInsert { overflow-y:scroll }
		
		/* for image modal pop up */
		/* Style the Image Used to Trigger the Modal */
		#myImg {
		    border-radius: 5px;
		    cursor: pointer;
		    transition: 0.3s;
		}
		
		#myImg:hover {opacity: 0.7;}
		
		/* The Modal (background) */
		.imagemodal {
		    display: none; /* Hidden by default */
		    position: fixed; /* Stay in place */
		    z-index: 1; /* Sit on top */
		    padding-top: 100px; /* Location of the box */
		    left: 0;
		    top: 0;
		    width: 100%; /* Full width */
		    height: 100%; /* Full height */
		    overflow: auto; /* Enable scroll if needed */
		    background-color: rgb(0,0,0); /* Fallback color */
		    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
		}
		
		/* Modal Content (Image) */
		.modal-content-image {
		    margin: auto;
		    display: block;
		    width: 80%;
		    max-width: 700px;
		}

		
		/* Caption of Modal Image (Image Text) - Same Width as the Image */
		#caption {
		    margin: auto;
		    display: block;
		    width: 80%;
		    max-width: 700px;
		    text-align: center;
		    color: #ccc;
		    padding: 10px 0;
		    height: 150px;
		}
		
		/* The Close Button */
		.closespanimage {
		    position: absolute;
		    top: 80px;
		    right: 40px;
		    color: #f1f1f1;
		    font-size: 40px;
		    font-weight: bold;
		    transition: 0.3s;
		}
		
		.closespanimage:hover,
		.closespanimage:focus {
		    color: #bbb;
		    text-decoration: none;
		    cursor: pointer;
		}
		/* end of image modal pop up */
		
	/*  css for loading bar */
	 .loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 45%;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}
	
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	/* end of css loading bar */
	
	.select2-container--default .select2-search--inline .select2-search__field{
	    width:initial!important;
	}
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Form_Order_Management" name="Form_Order_Management" action = "${pageContext.request.contextPath}/OrderManagement" method="post" enctype="multipart/form-data">
	<input type="hidden" name="temp_string" value="" />
	<input type="hidden" id="param_src" value="<%= config.getServletContext().getInitParameter("param_src") %>" />
	<input type="hidden" id="tempRole" value="<c:out value="${globalUserRole}"/>" />
	
	<!-- KOJA SETTING -->
	<input type="hidden" id="kojaUsername" value="<%= config.getServletContext().getInitParameter("koja_username") %>" />
	<input type="hidden" id="kojaPassword" value="<%= config.getServletContext().getInitParameter("koja_password") %>" />
	<input type="hidden" id="kojaStreamUsername" value="<%= config.getServletContext().getInitParameter("koja_fstream_username") %>" />
	<input type="hidden" id="kojaStreamPassword" value="<%= config.getServletContext().getInitParameter("koja_fstream_password") %>" />
	<input type="hidden" id="kojaDevicename" value="<%= config.getServletContext().getInitParameter("koja_devicename") %>" />
	<input type="hidden" id="kojaUrl" value="<%= config.getServletContext().getInitParameter("koja_url") %>" />
	
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Order Management</h1>
			</section>
			<!-- Main content -->
			<section class="content">
			
			<c:if test="${globalUserRole eq 'R002'}">
				<!-- dashboard atas -->
				<div class="row">
					<!-- dashboard 1 -->
	 				<div class="col-md-4 col-sm-6 col-xs-12">
			          <div class="info-box">
			            <span class="info-box-icon" style="background-color: #005292;">
			            	<i><img style="margin-top: 10px;"
			            			width="55px" height="55px" 
			            			src="mainform/dist/img/order/Group 4266.png">
			            	</i>
			            </span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" style="color: #000000; font-weight:normal;">Committed</span>
			              <span class="info-box-number" style="margin-top:12px; font-size: 20px;">
			              		<c:out value="${customerTotalOrder}" />
			              </span>
			            </div>
			            <!-- /.info-box-content -->
			          </div>
			          <!-- /.info-box -->
			        </div>
			        <!-- /.col -->
			        
			        
			        <!-- dashboard 2 -->
			        <div class="col-md-4 col-sm-6 col-xs-12">
			          <div class="info-box">
			            <span class="info-box-icon" style="background-color: #009270;">
			            	<i><img style="margin-top: 10px;"
			            			width="55px" height="55px" 
			            			src="mainform/dist/img/order/Group 4269.png">
			            	</i>
			            </span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" style="color: #000000; font-weight:normal;">Picked Order</span>
			              <span class="info-box-number" style="margin-top:12px; font-size: 20px;">
			              		<c:out value="${customerProcessedOrder}" />
			              </span>
			            </div>
			            <!-- /.info-box-content -->
			          </div>
			          <!-- /.info-box -->
			        </div>
			        <!-- /.col -->
			        
			        
			        <!-- dashboard 3 -->
			        <div class="col-md-4 col-sm-6 col-xs-12">
			          <div class="info-box">
			            <span class="info-box-icon bg-red">
			            	<i><img style="margin-top: 10px;"
			            			width="55px" height="55px" 
			            			src="mainform/dist/img/order/Group 4269.png">
			            	</i>
			            </span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" style="color: #000000; font-weight:normal;">Unpicked Order</span>
			              <span class="info-box-number" style="margin-top:12px; font-size: 20px;">
			              		<c:out value="${customerUnProcessedOrder}" />
			              </span>
			            </div>
			            <!-- /.info-box-content -->
			          </div>
			          <!-- /.info-box -->
			        </div>
			        <!-- /.col -->
			        
				</div>
				<!-- akhir dari dashboard atas -->
			</c:if>
					
					
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<c:if test="${condition == 'SuccessUpdateOrderManagement'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Update order success.
			   				</div>
						</c:if>
						<c:if test="${condition == 'SuccessCancelOrderManagement'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Cancel order success.
			   				</div>
						</c:if>
						<c:if test="${condition == 'FailedCancelOrderManagement'}">
							<div class="alert alert-danger alert-dismissible">
			      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
			      				Cancel order failed. <c:out value="${conditionDescription}"/>.
		    				</div>
						</c:if>
						<c:if test="${condition == 'SuccessCommit'}">
					 		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  Commit order success.
					 		</div>
							</c:if>
						<c:if test="${condition == 'FailedCommitOrderManagementDetail'}">
							<div class="alert alert-danger alert-dismissible">
			    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
			    				Commit order failed.
			  				</div>
						</c:if>
						
      					<!--modal update & Insert -->
						<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
									</div>
									
	   								<div class="modal-body">
		   								<div class="row">
		   									<div id="dvFactoryID" class="form-group col-md-6">
		         								<label for="lblFactoryID" class="control-label">Factory ID</label><label id="mrkFactoryID" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<small><label id="lblFactoryName" name="lblFactoryName" class="control-label"></label></small>
		         								<input type="text" class="form-control" id="txtFactoryID" name="txtFactoryID" 
		         								data-toggle="modal" data-target="#ModalGetFactoryID" readonly="readonly">
		       								</div>
		       								<div id="dvFactoryAddress" class="form-group col-md-6">
		         								<label for="lblFactoryAddress" class="control-label">Factory Address</label>	
		         								<textarea class="form-control" id="txtFactoryAddress" name="txtFactoryAddress" readonly="readonly" row=3></textarea>
		       								</div>
		       								<div id="dvOrderManagementID" class="form-group col-md-12" style="display:none;">
		         								<label for="lblOrderManagementID" class="control-label">Order Management ID</label><label id="mrkOrderManagementID" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtOrderManagementID" name="txtOrderManagementID" readonly="readonly">
		       								</div>
		       								<div id="dvShippingInvoiceID" class="form-group col-md-3">
		         								<label for="lblShippingInvoiceID" class="control-label">SI No</label><label id="mrkShippingInvoiceID" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtShippingInvoiceID" name="txtShippingInvoiceID">
		       								</div>
		       								<div id="dvShippingID" class="form-group col-md-3">
		         								<label for="lblShippingID" class="control-label">Shipping Line</label><label id="mrkShippingID" for="recipient-name" class="control-label"><small>*</small></label>	
												<!-- <small><label id="lblShippingName" name="lblShippingName" class="control-label"></label></small> -->
												<input type="hidden" class="form-control" id="txtShippingID" name="txtShippingID">
		         								<input type="text" class="form-control" id="txtShippingName" name="txtShippingName" data-toggle="modal" 
		         								data-target="#ModalGetShippingID" readonly="readonly">
		       								</div>
		       								<div id="dvItem" class="form-group col-md-3">
		         								<label for="lblItem" class="control-label">Commodity</label><label id="mrkItem" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtItem" name="txtItem">
		       								</div>
		       								<div id="dvItemWeight" class="form-group col-md-3">
		         								<label for="lblItemWeight" class="control-label">Weight(Kg)</label><label id="mrkItemWeight" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control number" id="txtItemWeight" name="txtItemWeight">
		       								</div>
		       								<div id="dvDeliveryOrderID" class="form-group col-md-6">
		         								<label for="lblDeliveryOrderID" class="control-label">DO / Booking Number</label><label id="mrkDeliveryOrderID" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtDeliveryOrderID" name="txtDeliveryOrderID">
		       								</div>
		       								<div id="dvPortID" class="form-group col-md-6">
		         								<label for="lblPortID" class="control-label">Port</label><label id="mrkPortID" for="recipient-name" class="control-label"><small>*</small></label>	
												<!-- <small><label id="lblPortName" name="lblPortName" class="control-label"></label></small> -->
		         								<input type="hidden" class="form-control" id="txtPortID" name="txtPortID">
		         								<input type="text" class="form-control" id="txtPortName" name="txtPortName" data-toggle="modal" 
		         								data-target="#ModalGetPortID" readonly="readonly">
		       								</div>
		       								<div id="dvVesselName" class="form-group col-md-3">
		         								<label for="lblVesselName" class="control-label">Vessel Name</label><label id="mrkVesselName" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtVesselName" name="txtVesselName" onclick="FuncGetVesselVoyage()">
		       								</div>
		       								<div id="dvVoyageNo" class="form-group col-md-3">
		         								<label for="lblVoyageNo" class="control-label">Voyage No</label><label id="mrkVoyageNo" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtVoyageNo" name="txtVoyageNo">
		       								</div>
		       								<div id="dvVoyageCo" class="form-group col-md-3" style="display:none;">
		         								<label for="lblVoyageCo" class="control-label">Voyage Company</label><label id="mrkVoyageCo" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtVoyageCo" name="txtVoyageCo">
		       								</div>
		       								<div id="dvPod" class="form-group col-md-3">
		         								<label for="lblPod" class="control-label">Destination Port</label><label id="mrkPod" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<div id="inputPod">
		         									<input type="text" class="form-control" id="txtPod" name="txtPod">
		         								</div>
		         								
		       								</div>
		       								<div id="dvShippingDate" class="form-group col-md-3">
		         								<label for="lblShippingDate" class="control-label">Shipping Date</label><label id="mrkShippingDate" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtShippingDate" name="txtShippingDate">
		       								</div>
		       								<div id="dvDepoID" class="form-group col-md-4">
	         									<label class="control-label">Depo</label><label id="mrkDepoID" class="control-label"><small>*</small></label>	
												<!-- <small><label id="lblDepoName" name="lblDepoName" class="control-label"></label></small> -->
		         								<input type="hidden" class="form-control" id="txtDepoID" name="txtDepoID">
		         								<input type="text" class="form-control" id="txtDepoName" name="txtDepoName" data-toggle="modal" 
		         								data-target="#ModalGetDepoID" readonly="readonly">
		       								</div>
	<!-- 	       								<div id="dvPEB" class="form-group col-md-6"> -->
	<!-- 	         								<label for="lblPEB" class="control-label">PEB</label>	 -->
	<!-- 	         								<input type="text" class="form-control" id="txtPEB" name="txtPEB"> -->
	<!-- 	       								</div> -->
	<!-- 	       								<div id="dvNPE" class="form-group col-md-6"> -->
	<!-- 	         								<label for="lblNPE" class="control-label">NPE</label>	 -->
	<!-- 	         								<input type="text" class="form-control" id="txtNPE" name="txtNPE"> -->
	<!-- 	       								</div> -->
		       								<div id="dvCustomerID" class="form-group col-md-6" style="display:none">
												<c:if test="${globalUserRole eq 'R002'}">
													<label for="lblCustomerID" class="control-label">Customer ID</label><label id="mrkCustomerID" for="recipient-name" class="control-label"><small>*</small></label>
													<small>
														<label id="lblCustomerName" name="lblCustomerName" class="control-label">
															<c:set var="customer" value="${mdlCustomer}"/>
															<c:out value='${customer.customerName}'/>
														</label>
													</small>
													<input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID" value="<c:out value='${globalUserID}'/>" readonly="readonly">
												</c:if>
												<c:if test="${globalUserRole ne 'R002'}">
													<label for="lblCustomerID" class="control-label">Customer ID</label><label id="mrkCustomerID" for="recipient-name" class="control-label"><small>*</small></label>	
			         								<small>
			         									<label id="lblCustomerName" name="lblCustomerName" class="control-label"></label>
		         									</small>
			         								<input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID" data-toggle="modal" 
			         								data-target="#ModalGetCustomerID">
												</c:if>
		       								</div>
		       								<div id="dvOrderType" class="form-group col-md-4">
		         								<label for="lblOrderType" class="control-label">Order Type</label><label id="mrkOrderType" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<select id="slOrderType" name="slOrderType" class="form-control select2" data-placeholder="Select Order Type" style="width: 100%;">
													<option value="EXPORT">EXPORT</option>
													<!-- <option value="IMPORT">IMPORT</option> -->
									        	</select>
		       								</div>
		       								<div id="dvTier" class="form-group col-md-4">
		         								<label for="lblTier" class="control-label">Tier</label><label id="mrkTier" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<select id="slTier" name="slTier" class="form-control select2" style="width: 100%;">
		         										<option value="" disabled>Choose</option>
														<c:forEach items="${listTier}" var="tier">
															<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.tierID}" /> - <c:out value="${tier.description}" /></option>
														</c:forEach>
											       	</select>
		       								</div>
		       								<div id="dvTotalQuantity" class="form-group col-md-6" style="display:none;">
		         								<label for="lblTotalQuantity" class="control-label">Total Quantity</label><label id="mrkTotalQuantity" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtTotalQuantity" name="txtTotalQuantity" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvTotalPrice" class="form-group col-md-6" style="display:none;">
		         								<label for="lblTotalPrice" class="control-label">Total Rate</label><label id="mrkTotalPrice" for="recipient-name" class="control-label"><small>*</small></label>	
		         								<input type="text" class="form-control" id="txtTotalPrice" name="txtTotalPrice" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvVendor" class="form-group col-md-12">
									          <label class="control-label">Vendor Trucker:</label><label id="mrkVendor" for="recipient-name" class="control-label"><small>*</small></label>	
									       	  	<select id="cbVendor" name="cbVendor" class="form-control select2" style="width: 100%;" multiple="multiple" readonly="readonly">
													<c:forEach items="${listVendor}" var="vendor">
														<option value="<c:out value="${vendor.vendorID}" />"><c:out value="${vendor.vendorName}" /></option>
													</c:forEach>
											    </select>
									        </div>
		       								<%-- <div id="dvVendor" class="form-group col-md-12">
									          <label class="control-label">Vendor:</label><label id="mrkVendor" for="recipient-name" class="control-label"><small>*</small></label>	
									       	  	<div class="panel panel-primary" id="VendorPanel"
													style="overflow: auto; height: 80px;">
													<div class="panel-body fixed-panel">
														<div class="row">
															<c:forEach items="${listVendor}" var="vendor">
																<div class="col-xs-4" id="cbvendor">
																	<label><input type="checkbox"
																		name="cbVendor"
																		id="<c:out value="${vendor.vendorID}" />"
																		value="<c:out value="${vendor.vendorID}" />">
																		<c:out value="${vendor.vendorName}" />
																	</label>
																</div>
															</c:forEach>
														</div>
													</div>
												</div>
									        </div> --%>
		       								<div id="dvOrderImage" class="form-group col-md-12">
									            <label for="lblOrderImage" class="control-label">Image Attachment :</label>
									            <input type="file" class="form-control" id="txtOrderImage" name="txtOrderImage">
									        </div>
									          <div class="row" id="ImageBox">
												<div class="col-md-4">
													<div class="box box-default box-solid">
														<div class="box-body">
															<!-- Trigger the Modal -->
															<img id="myImg" class="img-responsive" 
																src="" alt="Photo" style="width:228px;height:110px">
														</div>
												    </div>
											    </div>
											</div>
											<div id="myModal" class="imagemodal">
												<!-- The Close Button -->
											 	<span class="closespanimage">&times;</span>
												<!-- Modal Content (The Image) -->
												<img class="modal-content-image" id="img01">
												<!-- Modal Caption (Image Text) -->
											  	<div id="caption"></div>
											</div>
		       							</div>
	  								</div>
									<!-- end of order header body -->
	  								<div class="modal-footer" id="dvOrderButton">
										<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save and Create Schedule</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
									<!-- end of order header footer -->
	  								
									<!-- loader -->
	  								<div id="dvloader" class="loader"></div>
	  								
									<!-- schedule section -->
	  								<div class="modal-body" id="dvScheduleSection" style="display:none;">
	  									<!-- SchedulePanel -->
						      			<label for="recipient-name" class="control-label">Load/Unload Scheduling</label>
										<div class="panel panel-primary" id="SchedulePanel">
										<div class="panel-body fixed-panel">
											<%-- <button style="display: <c:out value="${buttonstatus}"/>;" type="button" class="btn btn-default" onclick="FuncClearScheduleField()"><i class="fa fa-undo"></i> Reset Form</button><br><br> --%>
											<div class="row" id="dvScheduleForm">
						      					<div id="dvOrderManagementDetailID" class="form-group col-md-3">
					  								<label for="recipient-name" class="control-label">Order Detail ID :</label>	
					  								<input type="text" class="form-control" id="txtOrderManagementDetailID" name="txtOrderManagementDetailID" readonly disabled>
												</div>
					    						<div id="dvStuffingDate" class="form-group col-md-2">
					      							<label for="message-text" class="control-label">Stuffing Date :</label><label id="mrkStuffingDate" for="recipient-name" class="control-label"><small>*</small></label>	
					      							<input type="text" class="form-control" id="txtStuffingDate" name="txtStuffingDate">
					    						</div>
					    						<div id="dvContainerType" class="form-group col-md-3">
					     							<label for="message-text" class="control-label">Container Type :</label><label id="mrkContainerType" for="recipient-name" class="control-label"><small>*</small></label>
								 					<select id="slContainerType" name="slContainerType" class="form-control select2" style="width: 100%;">
														<c:forEach items="${listContainerType}" var="containerType">
															<option value="<c:out value="${containerType.containerTypeID}" />"><c:out value="${containerType.description}" /></option>
														</c:forEach>
											       	</select>
							      				</div>
					  							<div id="dvQuantity" class="form-group col-md-2">
					    							<label for="lblQuantity" class="control-label">Quantity :</label><label id="mrkQuantity" for="recipient-name" class="control-label"><small>*</small></label>	
					    							<input type="text" class="form-control number" id="txtQuantity" name="txtQuantity">
					  							</div>
					  							<div class="form-group col-md-2">
					  								<button style="display: <c:out value="${buttonstatus}"/>; margin-top: 25px;" type="button" class="btn btn-primary" id="btnSaveSchedule" name="btnSaveSchedule" onclick="FuncSaveSchedule()"><i class="fa fa-plus-circle"></i> Confirm</button>
				  								</div>
				  							</div>
											<!-- end of schedule create and update field -->
				  								<!-- DataPanel -->
				  								<label for="recipient-name" class="control-label">Schedule</label>
					  							<div class="panel panel-primary" id="DataPanel">
												<div class="panel-body fixed-panel">
													<div id="dynamic-content-schedule"></div>
												</div>
												</div>
												<!-- end of DataPanel -->
										</div>
										</div>
										<!-- end of MainPanel -->
	  								</div>
	  								<!-- end of order detail body -->
	  								<div class="modal-footer" id="dvScheduleButton" style="display: none;">
										<!-- update order -->
	  									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
	  									
										<!-- schedule button -->
		  								<a href="${pageContext.request.contextPath}/OrderManagement"><button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary" id="btnConfirmSchedule"><i class="fa fa-check"></i> Save</button></a>
	   									<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalCommit" id="btnCommit"><i class="fa fa-cloud-upload"></i> Commit</button>
	  								</div>
									<!-- end of order detail footer -->
									
								</div>
								<!-- end of modal content -->
							</div>
						</div>
									
						<!--modal Cancel -->
						<div class="example-modal">
							<div class="modal modal-danger" id="ModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
   										<div class="modal-header">
	 										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	   										<span aria-hidden="true">&times;</span></button>
	 										<h4 class="modal-title">Alert Cancel Order Management</h4>
   										</div>
       									<div class="modal-body">
       										<input type="hidden" id="temp_txtOrderManagementID" name="temp_txtOrderManagementID" >
       	 									<p>Are you sure to cancel this order management ?</p>
       									</div>
						              <div class="modal-footer">
						              	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
						                <button type="submit" id="btnCancel" name="btnCancel"  class="btn btn-outline" >Cancel</button>
						              </div>
					            </div>
					            <!-- /.modal-content -->
					          </div>
					          <!-- /.modal-dialog -->
					        </div>
					        <!-- /.modal -->
					      </div>

							<!--modal show factory data -->
							<!-- bs-example-modal-lg -->
							<!-- modal-lg -->
							<div class="modal fade" id="ModalGetFactoryID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelFactoryID">
								<div class="modal-dialog " role="document">
									<div class="modal-content">
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="ModalLabelFactoryID"></h4>	
										</div>
	     								<div class="modal-body">
	     									<div id="dvloaderFactory" class="loader"></div>
	     									<div id="dynamic-content-factory"></div>
	    								</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show factory data -->
							
							<c:if test="${globalUserRole ne 'R002'}">
								<!--modal show customer data -->
								<div class="modal fade" id="ModalGetCustomerID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCustomerID">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
	 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	 											<h4 class="modal-title" id="ModalLabelCustomerID"></h4>
											</div>
		    								<div class="modal-body">
		        								<table id="tb_master_customer" class="table table-bordered table-hover">
									        		<thead style="background-color: #d2d6de;">
										                <tr>
										                <th>Customer ID</th>
										                <th>Customer Name</th>
										                <th>Customer Address</th>
										                <th>District Name</th>
														<th style="width: 20px"></th>
										                </tr>
									        		</thead>
									        		<tbody>
									        			<c:forEach items="${mdlCustomer}" var ="cust">
													        <tr>
													        <td><c:out value="${cust.customerID}"/></td>
													        <td><c:out value="${cust.customerName}"/></td>
													        <td><c:out value="${cust.customerAddress}"/></td>
													        <td><c:out value="${cust.district}"/></td>
													        <td><button type="button" class="btn btn-primary"
												        			data-toggle="modal"
												        			onclick="FuncPassStringCustomer('<c:out value="${cust.customerID}"/>','<c:out value="${cust.customerName}"/>')"
												        			data-dismiss="modal"
													        	><i class="fa fa-fw fa-check"></i></button>
													        </td>
											        		</tr>
									        			</c:forEach>
									        		</tbody>
								        		</table>
	   										</div>
										</div>
									</div>
								</div>
								<!-- /. end of modal show customer data -->
							</c:if>
							
							<!--modal show port data -->
							<div class="modal fade" id="ModalGetPortID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelPortID">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="ModalLabelPortID"></h4>
										</div>
	    								<div class="modal-body">
	        								<table id="tb_master_port" class="table table-bordered table-hover">
								        		<thead style="background-color: #d2d6de;">
									                <tr>
									                <th>Port ID</th>
									                <th>Port Name</th>
									                <th>Port UTC</th>
													<th style="width: 20px"></th>
									                </tr>
								        		</thead>
								        		<tbody>
								        			<c:forEach items="${listPort}" var ="port">
												        <tr>
												        <td><c:out value="${port.portID}"/></td>
												        <td><c:out value="${port.portName}"/></td>
												        <td><c:out value="${port.portUTC}"/></td>
												        <td><button type="button" class="btn btn-primary"
											        			data-toggle="modal"
											        			onclick="FuncPassStringPort('<c:out value="${port.portID}"/>','<c:out value="${port.portName}"/> - <c:out value="${port.portUTC}"/>')"
											        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
								        			</c:forEach>
								        		</tbody>
							        		</table>
   										</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show port data -->
										
							<!--modal show shipping data -->
							<div class="modal fade" id="ModalGetShippingID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelShippingID">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="ModalLabelShippingID"></h4>
										</div>
	    								<div class="modal-body">
	        								<table id="tb_master_shipping" class="table table-bordered table-hover">
								        		<thead style="background-color: #d2d6de;">
									                <tr>
									                <th>Shipping ID</th>
									                <th>Shipping Name</th>
									                <th>Shipping Address</th>
													<th style="width: 20px"></th>
									                </tr>
								        		</thead>
								        		<tbody>
								        			<c:forEach items="${listShipping}" var ="shipping">
												        <tr>
												        <td><c:out value="${shipping.shippingID}"/></td>
												        <td><c:out value="${shipping.name}"/></td>
												        <td><c:out value="${shipping.address}"/></td>
												        <td><button type="button" class="btn btn-primary"
											        			data-toggle="modal"
											        			onclick="FuncPassStringShipping('<c:out value="${shipping.shippingID}"/>','<c:out value="${shipping.name}"/>')"
											        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
								        			</c:forEach>
								        		</tbody>
							        		</table>
   										</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show shipping data -->
							
							<!--modal show depo data -->
							<div class="modal fade" id="ModalGetDepoID" tabindex="-1" role="dialog" aria-labelledby="ModalLabelDepoID">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="ModalLabelDepoID"></h4>
										</div>
	    								<div class="modal-body">
	        								<table id="tb_master_depo" class="table table-bordered table-hover">
								        		<thead style="background-color: #d2d6de;">
									                <tr>
									                <th>Depo ID</th>
									                <th>Depo Name</th>
									                <th>Depo Address</th>
													<th style="width: 20px"></th>
									                </tr>
								        		</thead>
								        		<tbody>
								        			<c:forEach items="${listDepo}" var ="depo">
												        <tr>
												        <td><c:out value="${depo.depoID}"/></td>
												        <td><c:out value="${depo.name}"/></td>
												        <td><c:out value="${depo.address}"/></td>
												        <td><button type="button" class="btn btn-primary"
											        			data-toggle="modal"
											        			onclick="FuncPassStringDepo('<c:out value="${depo.depoID}"/>','<c:out value="${depo.name}"/>')"
											        			data-dismiss="modal"
												        	><i class="fa fa-fw fa-check"></i></button>
												        </td>
										        		</tr>
								        			</c:forEach>
								        		</tbody>
							        		</table>
   										</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show shipping data -->
							
							<!--modal show vendor data -->
							<div class="modal fade" id="ModalVendor" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVendor">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="ModalLabelVendor"></h4>	
										</div>
										<div class="modal-body">
											<div id="dynamic-content-vendor"></div>
										</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show vendor data -->
							
							<!--modal show vessel data -->
							<div class="modal fade" id="ModalVessel" tabindex="-1" role="dialog" aria-labelledby="ModalLabelVessel">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="ModalLabelVessel"></h4>	
										</div>
										<div class="modal-body">
											<div id="dvloaderVessel" class="loader"></div>
											<div id="dynamic-content-vessel"></div>
										</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show vessel voyage data -->
							
							<!--modal Delete -->
							<div class="modal modal-danger" id="ModalDeleteSchedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">            											
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		 										<span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Alert Delete Schedule</h4>
										</div>
										<div class="modal-body">
											<input type="hidden" id="temp_txtOrderDetailID" name="temp_txtOrderDetailID"  />
											<p>Are you sure to delete this schedule ?</p>
										</div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
							        <button type="button" id="btnDelete" name="btnDelete"  class="btn btn-outline" onclick="FuncDeleteSchedule()">Delete</button>
							      </div>
							    </div>
							    <!-- /.modal-content -->
							  </div>
							  <!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							
							<!--modal Commit -->
							<div class="modal modal-default" id="ModalCommit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
									<div class="modal-content">
												<div class="modal-header">            											
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					 										<span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title"><i style="color: #FF0000;" class="fa fa-question-circle"></i> Alert Commit Order</h4>
												</div>
											<div class="modal-body">
														<p>Are you sure to commit this order ? Once the order is committed, it cannot be modified</p>
											</div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">NO</button>
							        <button type="submit" id="btnConfirmCommit" name="btnConfirmCommit" class="btn btn-primary" >YES</button>
							      </div>
							    </div>
							    <!-- /.modal-content -->
							  </div>
							  <!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							
							<c:if test="${statusOrder == true}">
								<button style="display: <c:out value="${buttonstatus}"/>" id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()" ><i class="fa fa-plus-circle"></i> New</button><br><br>
							</c:if>
							<c:if test="${statusOrder == false}">
							<div class="col-md-12" style="padding:0;">
								<div style="padding:0;" class="tooltip-wrapper col-md-1 pull-right disabled"  data-toggle="tooltip" data-html="true" title="<b>Open order start at 00:00 - 15:00</b>">
									<button style="display: <c:out value="${buttonstatus}"/>"  type="button" class="btn btn-primary pull-right" disabled><i class="fa fa-plus-circle"></i> New</button><br><br>
								</div>
							</div>
							</c:if>
							
							
							<table id="tb_order_management" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th></th>
										<th>Order</th>
										<th>SI</th>
										<th>DO</th>
										<!-- <th>Stuffing Date</th> -->
										<!-- <th>PEB</th> -->
										<!-- <th>NPE</th> -->
										<th>Customer</th>
										<th>Factory</th>
										<th>Commodity</th>
										<th>Type</th>
										<th>Qty</th>
										<!-- <th>Size Container</th> -->
										<th>Shipping</th>
										<th>Depo</th>
										<th>Status</th>
										<!-- <th>Tier</th> -->
										<!-- <th>Port</th> -->
										<!-- <th>Total Rate</th> -->
										<!-- <th>Total Qty Delivered</th> -->
										<!-- <th>Total Rate Delivered</th> -->
									<!-- <th style="width:103px;"></th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listOrderManagement}" var="order">
										<tr>
											<td>
												<table>
<%-- 													<c:if test="${buttonstatus == 'none'}"> --%>
<%-- 														<c:if test="${globalUserRole eq 'R001'}"> --%>
<%-- 															<td><a href="${pageContext.request.contextPath}/OrderManagementDetail?orderManagementID=<c:out value="${order.orderManagementID}" />"><button type="button" id="btnOrderManagementDetail" name="btnOrderManagementDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></a></td> --%>
<%-- 														</c:if> --%>
<%-- 														<c:if test="${globalUserRole ne 'R001'}"> --%>
<%-- 															<td><button style="display: <c:out value="${buttonstatus}"/>" type="button" id="btnOrderManagementDetail" name="btnOrderManagementDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></td> --%>
<%-- 														</c:if> --%>
<!-- 														<td>&nbsp</td> -->
<%-- 													</c:if> --%>
<%-- 													<c:if test="${buttonstatus != 'none'}"> --%>
<%-- 														<td><a href="${pageContext.request.contextPath}/OrderManagementDetail?orderManagementID=<c:out value="${order.orderManagementID}" />"><button type="button" id="btnOrderManagementDetail" name="btnOrderManagementDetail"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></a></td> --%>
<!-- 														<td>&nbsp</td> -->
<%-- 													</c:if> --%>
												
														<td>
															<button id="btnModalUpdate" name="btnModalUpdate" 
																	type="button" class="btn btn-info" data-toggle="modal" title="edit/view"
																	onclick="FuncButtonUpdate()"
																	data-target="#ModalUpdateInsert" 
																	data-lordermanagementid='<c:out value="${order.orderManagementID}" />'
																	data-lshippinginvoiceid='<c:out value="${order.shippingInvoiceID}" />'
																	data-ldeliveryorderid='<c:out value="${order.deliveryOrderID}" />'
																	data-lpeb='<c:out value="${order.peb}" />'
																	data-lnpe='<c:out value="${order.npe}" />'
																	data-lcustomerid='<c:out value="${order.customerID}" />'
																	data-lcustomername='<c:out value="${order.customerName}" />'
																	data-lfactoryid='<c:out value="${order.factoryID}" />'
																	data-lfactoryname='<c:out value="${order.factoryName}" />'
																	data-lfactoryaddress='<c:out value="${order.factoryAddress}" />'
																	data-lordertype='<c:out value="${order.orderType}" />'
																	data-litem='<c:out value="${order.itemDescription}" />'
																	data-ltier='<c:out value="${order.tier}" />'
																	data-lportid='<c:out value="${order.portID}" />'
																	data-lportname='<c:out value="${order.portName}" /> - <c:out value="${order.portUTC}" />'
																	data-lshippingid='<c:out value="${order.shippingID}" />'
																	data-lshippingname='<c:out value="${order.shippingName}" />'
																	data-ldepoid='<c:out value="${order.depoID}" />'
																	data-ldeponame='<c:out value="${order.depoName}" />'
																	data-ltotalquantity='<c:out value="${order.totalQuantity}" />'
																	data-ltotalprice='<c:out value="${order.totalPrice}" />'
																	data-lorderimage='<c:out value="${fn:replace(order.orderImage,'LogisticOnline/','')}" />'
																	data-lorderstatus='<c:out value="${order.orderStatus}" />'
																	data-lvoyageno='<c:out value="${order.voyageNo}" />'
																	data-lvesselname='<c:out value="${order.vesselName}" />'
																	data-lpod='<c:out value="${order.pod}" />'
																	data-litemweight='<c:out value="${order.itemWeight}" />'
																	data-lvoyageco='<c:out value="${order.voyageCo}" />'
																	data-lshippingdate='<c:out value="${order.shippingDate}" />'
																	>
																<i class="fa fa-edit"></i></button>
														</td>
														<td>&nbsp</td>
													<c:if test="${order.orderStatus eq 0}">
														<td>
																<button style="display: <c:out value="${buttonstatus}"/>" id="btnModalCancel" name="btnModalCancel" type="button" class="btn btn-danger" data-toggle="modal" 
																data-target="#ModalCancel" data-lordermanagementid='<c:out value="${order.orderManagementID}" />' title="cancel">
																<i class="fa fa-ban"></i>
																</button>
														</td>
													</c:if>
												</table>
											</td>
											<td><c:out value="${order.orderManagementID}" /></td>
											<td><c:out value="${order.shippingInvoiceID}" /></td>
											<td><c:out value="${order.deliveryOrderID}" /></td>
											<%-- <td><c:out value="${order.orderStuffingDate}" /></td> --%>
											<%-- <td><c:out value="${order.peb}" /></td> --%>
											<%-- <td><c:out value="${order.npe}" /></td> --%>
											<td><c:out value="${order.customerName}" /></td>
											<td><c:out value="${order.factoryName}" /></td>
											<td><c:out value="${order.itemDescription}" /></td>
											<td><c:out value="${order.orderType}" /></td>
											<td><c:out value="${order.totalQuantity}" /></td>
											<%-- <td><c:out value="${order.containerTypeDescription}" /></td> --%>
											<td><c:out value="${order.shippingName}" /></td>
											<td><c:out value="${order.depoName}" /></td>
											<td>
												<c:if test="${order.orderStatus == 0}">New</c:if>
												<c:if test="${order.orderStatus == 1}">Committed</c:if>
												<c:if test="${order.orderStatus == 2}">Received</c:if>
												<c:if test="${order.orderStatus == 3}">On Process</c:if>
												<c:if test="${order.orderStatus == 4}">Finished</c:if>
												<c:if test="${order.orderStatus == 10}">Cancelled</c:if>
											</td>
											<%-- <td><c:out value="${order.tierDescription}" /></td> --%>
											<%-- <td><c:out value="${order.portName}" /></td> --%>
											<!-- <td> -->
											<%-- <fmt:setLocale value="id_ID"/> --%>
											<%-- <fmt:formatNumber value="${order.totalPrice}" type="currency"/> --%>
											<!-- </td> -->
											<%-- <td><c:out value="${order.totalQuantityDelivered}" /></td> --%>
											<!-- <td> -->
											<%-- <fmt:setLocale value="id_ID"/> --%>
											<%-- <fmt:formatNumber value="${order.totalPriceDelivered}" type="currency"/> --%>
											<!-- </td> -->
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
			
			
			
			<!-- Title Content Archive -->
			<section class="content-header">
				<h1>Archive</h1>
			</section>
			<!-- content archive -->
			<section class="content">
				<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="tb_archive" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th></th>
										<th>Order</th>
										<th>SI</th>
										<th>DO</th>
										<!-- <th>Stuffing Date</th> -->
										<th>Customer</th>
										<th>Factory</th>
										<th>Type</th>
										<th>Qty</th>
										<!-- <th>Size Container</th> -->
										<th>Commodity</th>
										<th>Shipping</th>
										<th>Depo</th>
										<th>Tps Invoice</th>
										<th>Status</th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listArchive}" var="order">
										<tr>
											<td>
												<table>
													<td>
														<button id="btnModalUpdateArchive" name="btnModalUpdateArchive" 
																type="button" class="btn btn-info" data-toggle="modal" title="view"
																onclick="FuncButtonUpdate()"
																data-target="#ModalUpdateInsert" 
																data-lordermanagementid='<c:out value="${order.orderManagementID}" />'
																data-lshippinginvoiceid='<c:out value="${order.shippingInvoiceID}" />'
																data-ldeliveryorderid='<c:out value="${order.deliveryOrderID}" />'
																data-lpeb='<c:out value="${order.peb}" />'
																data-lnpe='<c:out value="${order.npe}" />'
																data-lcustomerid='<c:out value="${order.customerID}" />'
																data-lcustomername='<c:out value="${order.customerName}" />'
																data-lfactoryid='<c:out value="${order.factoryID}" />'
																data-lfactoryname='<c:out value="${order.factoryName}" />'
																data-lfactoryaddress='<c:out value="${order.factoryAddress}" />'
																data-lordertype='<c:out value="${order.orderType}" />'
																data-litem='<c:out value="${order.itemDescription}" />'
																data-ltier='<c:out value="${order.tier}" />'
																data-lportid='<c:out value="${order.portID}" />'
																data-lportname='<c:out value="${order.portName}" /> - <c:out value="${order.portUTC}" />'
																data-lshippingid='<c:out value="${order.shippingID}" />'
																data-lshippingname='<c:out value="${order.shippingName}" />'
																data-ldepoid='<c:out value="${order.depoID}" />'
																data-ldeponame='<c:out value="${order.depoName}" />'
																data-ltotalquantity='<c:out value="${order.totalQuantity}" />'
																data-ltotalprice='<c:out value="${order.totalPrice}" />'
																data-lorderimage='<c:out value="${fn:replace(order.orderImage,'LogisticOnline/','')}" />'
																data-lorderstatus='<c:out value="${order.orderStatus}" />'
																data-lvoyageno='<c:out value="${order.voyageNo}" />'
																data-lvesselname='<c:out value="${order.vesselName}" />'
																data-lpod='<c:out value="${order.pod}" />'
																data-litemweight='<c:out value="${order.itemWeight}" />'
																data-lvoyageco='<c:out value="${order.voyageCo}" />'
																data-lshippingdate='<c:out value="${order.shippingDate}" />'
																>
															<i class="fa fa-edit"></i></button>
													</td>
												</table>
											</td>
											<td><c:out value="${order.orderManagementID}" /></td>
											<td><c:out value="${order.shippingInvoiceID}" /></td>
											<td><c:out value="${order.deliveryOrderID}" /></td>
											<%-- <td><c:out value="${order.orderStuffingDate}" /></td> --%>
											<td><c:out value="${order.customerName}" /></td>
											<td><c:out value="${order.factoryName}" /></td>
											<td><c:out value="${order.orderType}" /></td>
											<td><c:out value="${order.totalQuantity}" /></td>
											<%-- <td><c:out value="${order.containerTypeDescription}" /></td> --%>
											<td><c:out value="${order.itemDescription}" /></td>
											<td><c:out value="${order.shippingName}" /></td>
											<td><c:out value="${order.depoName}" /></td>
											<td>
												<c:if test="${not empty order.tpsLinkInvoice}">
												<button
													type="button"  class="btn btn-info"
													title="Show Invoice" onclick="showInvoice('<c:out value="${order.tpsLinkInvoice}"/>')"
												>show</button>
												</c:if>
											</td>
											<td>
												<c:if test="${order.orderStatus == 0}">New</c:if>
												<c:if test="${order.orderStatus == 1}">Committed</c:if>
												<c:if test="${order.orderStatus == 2}">Received</c:if>
												<c:if test="${order.orderStatus == 3}">On Process</c:if>
												<c:if test="${order.orderStatus == 4}">Finished</c:if>
												<c:if test="${order.orderStatus == 10}">Cancelled</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
			</section>
			<!-- end of archive section -->
			
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	<script src="mainform/plugins/momentjs/moment.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
 		
		$('[data-toggle="tooltip"]').tooltip()
	
  		$("#tb_order_management").DataTable({"aaSorting": [],"scrollX": true});
  		$("#tb_archive").DataTable({"aaSorting": [],"scrollX": true});
  		$("#tb_master_factory").DataTable();
  		$("#tb_master_customer").DataTable();
  		
  		$("#tb_master_factory").DataTable();
  		$("#tb_master_port").DataTable();
  		$("#tb_master_shipping").DataTable();
  		$("#tb_master_depo").DataTable();
  		$('#M010').addClass('active');
  		$('#M010_1').addClass('active');
  		
  		$("#dvErrorAlert").hide();
  		$('#dvloader').hide();
  		$('#dvloaderFactory').hide();
  		$('#dvloaderVessel').hide();
  		
  		$('#txtStuffingDate').datepicker({
	      format: 'dd M yyyy',
	      setDate: new Date(),
	      autoclose: true
	    });
	    
	    $('#txtShippingDate').datepicker({
	      format: 'dd M yyyy',
	      setDate: new Date(),
	      autoclose: true
	    });
  	});
 	
 	function showInvoice(param) {
	     window.open(param, "_blank");
	}
 	
 	var userRole = document.getElementById('tempRole').value;
	if(userRole == 'R002'){
		//shortcut for button 'new'
    	Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
    	});
	}
	</script>
    
<script>
	$('#ModalCancel').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lordermanagementid = button.data('lordermanagementid');
		$("#temp_txtOrderManagementID").val(lordermanagementid);
	})
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
 		var button = $(event.relatedTarget);
 		var modal = $(this);
 		$("#txtStuffingDate").datepicker("update", moment().format('DD-MM-YYYY'));
 		
 		$("#txtShippingInvoiceID").focus();
	});
</script>

<script>
	//check plant and warehouseid is WMEnabled or Not, if Yes, show door and staging area field, also return the 'updatewmfirst' value 
	
	function FuncPassStringFactory(lParamFactoryID,lParamFactoryName,lParamAddress){
		$("#txtFactoryID").val(lParamFactoryID);
		$("#txtFactoryAddress").val(lParamAddress);
		document.getElementById('lblFactoryName').innerHTML = '(' + lParamFactoryName + ')';
	}
	function FuncPassStringPort(lParamPortID,lParamPortName){
		$("#txtPortID").val(lParamPortID);
		$("#txtPortName").val(lParamPortName);
		
		var html = '<input type="text" class="form-control" id="txtPod" name="txtPod">';
		$('#inputPod').html(html);
		//document.getElementById('lblPortName').innerHTML = '(' + lParamPortName + ')';
	}
	function FuncPassStringShipping(lParamShippingID,lParamShippingName){
		$("#txtShippingName").val(lParamShippingName);
		$("#txtShippingID").val(lParamShippingID);
	}
	function FuncPassStringDepo(lParamDepoID,lParamDepoName){
		$("#txtDepoID").val(lParamDepoID);
		$("#txtDepoName").val(lParamDepoName);
		//document.getElementById('lblDepoName').innerHTML = '(' + lParamDepoName + ')';
	}
	function FuncPassStringCustomer(lParamCustomerID,lParamCustomerName){
		$("#txtCustomerID").val(lParamCustomerID);
		document.getElementById('lblCustomerName').innerHTML = '(' + lParamCustomerName + ')';
	}
	function FuncPassVesselVoyage(lParamVesselID,lParamVoyageID,lParamVoyageCo,lParamPod,lParamShippingDate,lParamLimit){
		$("#txtVesselName").val(lParamVesselID);
		$("#txtVoyageNo").val(lParamVoyageID);
		$("#txtVoyageCo").val(lParamVoyageCo);
		
		
		
		$("#txtShippingDate").val(moment(lParamShippingDate, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY'));
		
		var html = '';
		var arrPOD = lParamPod.split(',');
		
		if(arrPOD.length >= 1) {
			html = '<select id="txtPod" name="txtPod" class="form-control select2" style="width: 100%;">'
			for (var i = 0; i < arrPOD.length; i++) {
				
				html += '<option value="'+arrPOD[i]+'">'+arrPOD[i]+'</option>';
				
			}
			html += '</select>'
		}else {
			html += '<input type="text" class="form-control" id="txtPod" name="txtPod">'
		}
		
		
		console.log(html)	
   		
		
		
		$("#inputPod").html(html);
		
		console.log(arrPOD)
			//
		alert("The vessel that you choose has '"+ lParamLimit + "' export container limit left. " +
			  "It might change at any time due to export process. Please contact administrator/shipping line if you consider to add the limit.");
	}
</script>

<script>

function FuncClear(){
	$('#mrkOrderManagementID').hide();
	$('#mrkShippingInvoiceID').hide();
	$('#mrkDeliveryOrderID').hide();
	$('#mrkCustomerID').hide();
	$('#mrkFactoryID').hide();
	$('#mrkOrderType').hide();
	$('#mrkTier').hide();
	$('#mrkItem').hide();
	$('#mrkPortID').hide();
	$('#mrkShippingID').hide();
	$('#mrkDepoID').hide();
	$('#mrkTotalQuantity').hide();
	$('#mrkTotalPrice').hide();
	$('#mrkVoyageNo').hide();
	$('#mrkVesselName').hide();
	$('#mrkVendor').hide();
	$('#mrkPod').hide();
	$('#mrkItemWeight').hide();
	$('#mrkVoyageCo').hide();
	$('#mrkShippingDate').hide();
	
	$('#dvOrderManagementID').removeClass('has-error');
	$('#dvShippingInvoiceID').removeClass('has-error');
	$('#dvDeliveryOrderID').removeClass('has-error');
	$('#dvCustomerID').removeClass('has-error');
	$('#dvFactoryID').removeClass('has-error');
	$('#dvOrderType').removeClass('has-error');
	$('#dvTier').removeClass('has-error');
	$('#dvItem').removeClass('has-error');
	$('#dvPortID').removeClass('has-error');
	$('#dvShippingID').removeClass('has-error');
	$('#dvDepoID').removeClass('has-error');
	$('#dvTotalQuantity').removeClass('has-error');
	$('#dvTotalPrice').removeClass('has-error');
	$('#dvVoyageNo').removeClass('has-error');
	$('#dvVesselName').removeClass('has-error');
	$('#dvVendor').removeClass('has-error');
	$('#dvPod').removeClass('has-error');
	$('#dvItemWeight').removeClass('has-error');
	$('#dvVoyageCo').removeClass('has-error');
	$('#dvShippingDate').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncClearScheduleField() {
	$('#txtOrderManagementDetailID').val('');
	$('#txtStuffingDate').val('');
	$('#txtQuantity').val('');
	$('#slContainerType').val('').trigger("change");
	
	jQuery('#mrkStuffingDate').hide();
	jQuery('#mrkContainerType').hide();
	jQuery('#mrkQuantity').hide();
	jQuery('#dvStuffingDate').removeClass('has-error');
	jQuery('#dvContainerType').removeClass('has-error');
	jQuery('#dvQuantity').removeClass('has-error');
}

function FuncButtonNew() {
	$("#txtStuffingDate").datepicker("update", moment().format('DD-MM-YYYY'));
	jQuery('#txtOrderManagementID').val('');
	jQuery('#txtShippingInvoiceID').val('');
	jQuery('#txtDeliveryOrderID').val('');
	jQuery('#txtFactoryID').val('');
	jQuery('#txtFactoryAddress').val('');
	jQuery('#txtPortID').val('');
	jQuery('#txtPortName').val('');
	jQuery('#txtShippingID').val('');
	jQuery('#txtShippingName').val('');
	jQuery('#txtDepoID').val('');
	jQuery('#txtDepoName').val('');
	jQuery('#txtTotalQuantity').val('');
	jQuery('#txtTotalPrice').val('');
	$('#slTier').val('').trigger("change");
	jQuery('#txtItem').val('');
	jQuery('#txtOrderImage').val('');
	jQuery('#txtVoyageNo').val('');
	jQuery('#txtVesselName').val('');
	jQuery('#txtPod').val('');
	jQuery('#txtItemWeight').val('');
	jQuery('#txtVoyageCo').val('');
	jQuery('#txtShippingDate').val('');
	$('#cbVendor').val("").trigger('change.select2');
	$("#VendorPanel :input").prop("checked", false);
// 	jQuery('#txtPEB').val('');
// 	jQuery('#txtNPE').val('');
	
	jQuery('#dvOrderManagementID').hide();
	jQuery('#dvTotalQuantity').hide();
	jQuery('#dvTotalPrice').hide();

	jQuery('#btnSave').show();
	jQuery('#btnUpdate').hide();
	document.getElementById('lblTitleModal').innerHTML = "Add Order";
	document.getElementById('lblFactoryName').innerHTML = '';
// 	document.getElementById('lblPortName').innerHTML = '';
// 	document.getElementById('lblShippingName').innerHTML = '';
// 	document.getElementById('lblDepoName').innerHTML = '';
	document.getElementById('lblCustomerName').innerHTML = '';
	$('#txtShippingInvoiceID').prop("readonly", false);
	$('#ImageBox').hide();

	$('#dvScheduleSection').hide();
	$('#dvScheduleButton').hide();
	$('#btnConfirmSchedule').show();
	$('#btnCommit').show();
	$('#dvOrderButton').show();
				
	FuncClear();
	
	var lorderid = document.getElementById('txtOrderManagementID').value;
	FuncLoadSchedule(lorderid);
}

function FuncButtonUpdate() {
	
// 	jQuery('#dvOrderManagementID').show();
// 	jQuery('#dvTotalQuantity').show();
// 	jQuery('#dvTotalPrice').show();
	$('#ImageBox').show();
	
	$('#btnSave').hide();
	$('#btnCommit').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Order Management';
	$('#txtShippingInvoiceID').prop("readonly", true);
	
	FuncClear();
	FuncClearScheduleField();
	
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		event.stopImmediatePropagation()
		/* $('#txtStuffingDate').datepicker({
		      format: 'dd M yyyy',
		      defaultDate: new Date(),
		      autoclose: true
	    }); */
		
		$("#txtStuffingDate").datepicker("update", moment().format('DD-MM-YYYY'));
		// $("#txtStuffingDate").datepicker("update");
		
		$("#dvErrorAlert").hide();
 		var button = $(event.relatedTarget);
 		var lordermanagementid = button.data('lordermanagementid');
 		if(lordermanagementid != undefined){
 			var lshippinginvoiceid = button.data('lshippinginvoiceid');
	 		var ldeliveryorderid = button.data('ldeliveryorderid');
	 		var lcustomerid = button.data('lcustomerid');
	 		var lcustomername = button.data('lcustomername') == undefined ? '' : button.data('lcustomername');
	 		var lfactoryid = button.data('lfactoryid');
	 		var lfactoryname = button.data('lfactoryname') == undefined ? '' : button.data('lfactoryname');
	 		var lfactoryaddress = button.data('lfactoryaddress');
	 		var lordertype = button.data('lordertype');
	 		var ltier = button.data('ltier');
	 		var litem = button.data('litem');
	 		var lportid = button.data('lportid');
	 		var lportname = button.data('lportname') == undefined ? '' : button.data('lportname');
	 		var lshippingid = button.data('lshippingid');
	 		var lshippingname = button.data('lshippingname') == undefined ? '' : button.data('lshippingname');
	 		var ldepoid = button.data('ldepoid');
	 		var ldeponame = button.data('ldeponame') == undefined ? '' : button.data('ldeponame');
	 		var ltotalquantity = button.data('ltotalquantity');
	 		var ltotalprice = button.data('ltotalprice');
	 		var lorderimage = button.data('lorderimage');
		 	var lpeb = button.data('lpeb');
		 	var lnpe = button.data('lnpe');
		 	var lorderstatus = button.data('lorderstatus');
		 	var lvoyageno = button.data('lvoyageno');
		 	var lvesselname = button.data('lvesselname');
		 	var lpod = button.data('lpod');
		 	var litemweight = button.data('litemweight');
		 	var lvoyageco = button.data('lvoyageco');
		 	var lshippingdate = button.data('lshippingdate');
		 	var lsrc = document.getElementById('param_src').value;
		 	orderID = lordermanagementid;
	 		
	 		var modal = $(this);
	 		modal.find(".modal-body #txtOrderManagementID").val(lordermanagementid);
	 		modal.find(".modal-body #txtShippingInvoiceID").val(lshippinginvoiceid);
	 		modal.find(".modal-body #txtDeliveryOrderID").val(ldeliveryorderid);
	//  	modal.find(".modal-body #txtPEB").val(lpeb);
	//  	modal.find(".modal-body #txtNPE").val(lnpe);
	 		modal.find(".modal-body #txtCustomerID").val(lcustomerid);
	 		modal.find(".modal-body #txtFactoryID").val(lfactoryid);
	 		modal.find(".modal-body #txtFactoryAddress").val(lfactoryaddress);
	 		modal.find(".modal-body #slOrderType").val(lordertype).trigger('change.select2');
	 		modal.find(".modal-body #slTier").val(ltier).trigger("change.select2");
	 		modal.find(".modal-body #txtItem").val(litem);
	 		modal.find(".modal-body #txtPortID").val(lportid);
	 		modal.find(".modal-body #txtPortName").val(lportname);
	 		modal.find(".modal-body #txtShippingID").val(lshippingid);
	 		modal.find(".modal-body #txtShippingName").val(lshippingname);
	 		modal.find(".modal-body #txtDepoID").val(ldepoid);
	 		modal.find(".modal-body #txtDepoName").val(ldeponame);
	 		modal.find(".modal-body #txtTotalQuantity").val(ltotalquantity);
	 		modal.find(".modal-body #txtTotalPrice").val(ltotalprice);
	 		modal.find(".modal-body #txtTotalQuantity").val(ltotalquantity);
	 		modal.find(".modal-body #txtVoyageNo").val(lvoyageno);
	 		modal.find(".modal-body #txtVesselName").val(lvesselname);
	 		modal.find(".modal-body #txtPod").val(lpod);
	 		modal.find(".modal-body #txtItemWeight").val(litemweight);
	 		modal.find(".modal-body #txtVoyageCo").val(lvoyageco);
	 		modal.find(".modal-body #txtShippingDate").val(lshippingdate);
	 		modal.find('.modal-body #myImg').attr("src",lsrc+lorderimage);
	 		/* modal.find(".modal-body #cbVendor").val(lsrc).trigger('change.select2'); */
	 		if(lorderimage == null || lorderimage == '')
	 			$('#ImageBox').hide();
	 		
			document.getElementById('lblFactoryName').innerHTML = '(' + lfactoryname + ')';
// 	 		document.getElementById('lblPortName').innerHTML = '(' + lportname + ')';
// 	 		document.getElementById('lblShippingName').innerHTML = '(' + lshippingname + ')';
	 		document.getElementById('lblCustomerName').innerHTML = '(' + lcustomername + ')';
// 	 		document.getElementById('lblDepoName').innerHTML = '(' + ldeponame + ')';	
	 		
	 		$("#VendorPanel :input").prop("checked", false);
	 		
	 		 
	 		
	 		// make cheked vendor
	 		var listVendor = new Array();
	 			
	 		$.ajax({
					url : '${pageContext.request.contextPath}/getMappingOrderVendor',
					type : 'POST',
					data : {
						orderid : lordermanagementid
					},
					dataType : 'json'
				}).done(
						function(data) {
							console.log(data);
							for ( var i in data) {
								var obj = data[i];								
								
								listVendor.push(obj.VendorID);
								/* modal.find(".modal-body #"+ obj.VendorID).prop('checked', true); */
								modal.find(".modal-body #cbVendor").val( listVendor ).trigger('change.select2');
							}
							
							for ( var i in data) {
														var obj = data[i];
														var index = 0;
														var BrandId, BrandName;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																BrandId = obj[prop];
																break;
															default:
																break;
															}
														}
														listbrand.push(BrandId);
														$('#slBrand')
																.val(listbrand)
																.trigger(
																		'change.select2')
													}
						}).fail(function() {
					console.log('Service call failed!');
				})
	 		
	 		var userRole = document.getElementById('tempRole').value;
	 		if(lorderstatus == 0 && userRole != 'R001'){
	 			$('#dvScheduleForm').show();
	 			$('#btnUpdate').show();
	 		}
	 		else{
	 			$('#btnCommit').hide();
	 			$('#dvScheduleForm').hide();
	 			$('#btnUpdate').hide();
	 		}
	 		
	 		$('#dvScheduleSection').show();
			$('#dvScheduleButton').show();
			$('#btnConfirmSchedule').hide();
			$('#dvOrderButton').hide();
			FuncLoadSchedule(lordermanagementid);	
 		}
	});
}
				
function FuncValEmptyInput(lParambtn) {
	$('#dvloader').show();
	
	var txtOrderManagementID = document.getElementById('txtOrderManagementID').value;
	var txtShippingInvoiceID = document.getElementById('txtShippingInvoiceID').value;
	var txtDeliveryOrderID = document.getElementById('txtDeliveryOrderID').value;
	var txtCustomerID = document.getElementById('txtCustomerID').value;
	var txtFactoryID = document.getElementById('txtFactoryID').value;
	var slOrderType = document.getElementById('slOrderType').value;
	var slTier = jQuery('#slTier').val();
	var txtItem = document.getElementById('txtItem').value;
	var txtPortID = document.getElementById('txtPortID').value;
	var txtShippingID = document.getElementById('txtShippingID').value;
	var txtDepoID = document.getElementById('txtDepoID').value;
	var txtTotalQuantity = document.getElementById('txtTotalQuantity').value;
	var txtTotalPrice = document.getElementById('txtTotalPrice').value;
	var txtOrderImage = document.getElementById("txtOrderImage").value;
	var txtVoyageNo = document.getElementById("txtVoyageNo").value;
	var txtVesselName = document.getElementById("txtVesselName").value;
	var txtPod = document.getElementById("txtPod").value;
	var txtItemWeight = document.getElementById("txtItemWeight").value;
	var txtVoyageCo = document.getElementById("txtVoyageCo").value;
	var txtShippingDate = document.getElementById("txtShippingDate").value;
	
	/* var checkedVendor = new Array();
		$('#cbVendor input:checked').each(function() {
			checkedVendor.push(this.value);
 	}); */
 	
 	var checkedVendor = $("#cbVendor").val();
    
    if(!txtShippingInvoiceID.match(/\S/)) {    	
    	$('#txtShippingInvoiceID').focus();
    	$('#dvShippingInvoiceID').addClass('has-error');
    	$('#mrkShippingInvoiceID').show();
    	$('#dvloader').hide();
        return false;
    } 
    if(!txtDeliveryOrderID.match(/\S/)) {
    	$('#txtDeliveryOrderID').focus();
    	$('#dvDeliveryOrderID').addClass('has-error');
    	$('#mrkDeliveryOrderID').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtFactoryID.match(/\S/)) {
    	$('#txtFactoryID').focus();
    	$('#dvFactoryID').addClass('has-error');
    	$('#mrkFactoryID').show();
    	$('#dvloader').hide();
        return false;
    }
     if(!txtPortID.match(/\S/)) {
    	$('#txtPortID').focus();
    	$('#dvPortID').addClass('has-error');
    	$('#mrkPortID').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtShippingID.match(/\S/)) {
    	$('#txtShippingID').focus();
    	$('#dvShippingID').addClass('has-error');
    	$('#mrkShippingID').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtDepoID.match(/\S/)) {
    	$('#txtDepoID').focus();
    	$('#dvDepoID').addClass('has-error');
    	$('#mrkDepoID').show();
    	$('#dvloader').hide();
        return false;
    }
    if(slTier === 'undefined' || slTier == null) {
    	$('#slTier').focus();
    	$('#dvTier').addClass('has-error');
    	$('#mrkTier').show();
    	$('#dvloader').hide();
        return false;
    }
    /* if(!slTier.match(/\S/)) {
    	$('#slTier').focus();
    	$('#dvTier').addClass('has-error');
    	$('#mrkTier').show();
    	$('#dvloader').hide();
        return false;
    } */  
    if(!txtItem.match(/\S/)) {
    	$('#txtItem').focus();
    	$('#dvItem').addClass('has-error');
    	$('#mrkItem').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtVoyageNo.match(/\S/)) {
    	$('#txtVoyageNo').focus();
    	$('#dvVoyageNo').addClass('has-error');
    	$('#mrkVoyageNo').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtVesselName.match(/\S/)) {
    	$('#txtVesselName').focus();
    	$('#dvVesselName').addClass('has-error');
    	$('#mrkVesselName').show();
    	$('#dvloader').hide();
        return false;
    }
    if(!txtPod.match(/\S/)) {
    	$('#txtPod').focus();
    	$('#dvPod').addClass('has-error');
    	$('#mrkPod').show();
    	$('#dvloader').hide();
        return false;
    }
     if(!txtItemWeight.match(/\S/)) {
    	$('#txtItemWeight').focus();
    	$('#dvItemWeight').addClass('has-error');
    	$('#mrkItemWeight').show();
    	$('#dvloader').hide();
        return false;
    }
     /* if(!txtVoyageCo.match(/\S/)) {
    	$('#txtVoyageCo').focus();
    	$('#dvVoyageCo').addClass('has-error');
    	$('#mrkVoyageCo').show();
    	$('#dvloader').hide();
        return false;
    } */
    if(!txtShippingDate.match(/\S/)) {
    	$('#txtShippingDate').focus();
    	$('#dvShippingDate').addClass('has-error');
    	$('#mrkShippingDate').show();
    	$('#dvloader').hide();
        return false;
    }
    
    if(checkedVendor.length == 0){
		  $('#cbVendor').focus();
		  $('#dvVendor').addClass('has-error');
		  $('#mrkVendor').show();
		  $('#dvloader').hide();
		  return false;
	}
    
    // Get form
    var form = $('#Form_Order_Management')[0];

	// Create an FormData object 
	var dataForm = new FormData(form);
	
	// If you want to add an extra field for the FormData
    dataForm.append("key", lParambtn);
    
    //ingat nambah anotasi @MultipartConfig di servlet
        
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/OrderManagement',	
        type:'POST',
        enctype: 'multipart/form-data',
		//data:{"key":lParambtn,"txtOrderManagementID":txtOrderManagementID,"txtShippingInvoiceID":txtShippingInvoiceID,"txtDeliveryOrderID":txtDeliveryOrderID,
		//"txtCustomerID":txtCustomerID, "txtFactoryID":txtFactoryID,"slOrderType":slOrderType, "txtPortID":txtPortID,
		//"slTier":slTier,"txtItem":txtItem,"txtShippingID":txtShippingID,"txtDepoID":txtDepoID,"txtTotalQuantity":txtTotalQuantity,"txtTotalPrice":txtTotalPrice,"txtOrderImage":txtOrderImage},
        data: dataForm,
        processData: false,
        contentType: false,
        cache: false,
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	$('#dvloader').hide();
        	
        	if(data.split("--")[0] == 'FailedInsertOrderManagement')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan order management";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateOrderManagement')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui order management";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedWriteImage')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Gagal memproses gambar, tolong coba kembali";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtDate").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'SuccessUpdateOrderManagement')
        	{
        		$('#dvloader').show();
        		var url = '${pageContext.request.contextPath}/OrderManagement';  
				$(location).attr('href', url);	
        	}
        	else
        	{
        		FuncClearScheduleField();
				
				$('#btnSave').hide();
				$('#dvloader').hide();
				$('#dvScheduleSection').show();
				$('#dvScheduleButton').show();
				$('#dvScheduleForm').show();
				$('#dvOrderButton').hide();
				$('#txtOrderManagementID').val(data.split("--")[2]);
				$("#txtStuffingDate").datepicker("update", moment().format('DD-MM-YYYY'));
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

<script>
$(document).ready(function(){
//get warehouse from plant id
    $(document).on('click', '#txtFactoryID', function(e){
    	$('#dvloaderFactory').show();
    	e.preventDefault();
  
	//var plantid = $(this).data('id'); // get id of clicked row
		var customerid = document.getElementById('txtCustomerID').value;
  
     $('#dynamic-content-factory').html(''); // leave this div blank
 
     $.ajax({
          url: '${pageContext.request.contextPath}/getfactory',
          type: 'POST',
          data: 'customerid='+customerid,
          dataType: 'html'
     })
     .done(function(data){
          console.log(data); 
          $('#dynamic-content-factory').html(''); // blank before load.
          $('#dynamic-content-factory').html(data); // load here
          $('#dvloaderFactory').hide();
     })
     .fail(function(){
          $('#dynamic-content-factory').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
          $('#dvloaderFactory').hide();
     });
    });
});
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtFactoryID:focus').length) {
    	$('#txtFactoryID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtPortName:focus').length) {
    	$('#txtPortName').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtShippingName:focus').length) {
    	$('#txtShippingName').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtDepoName:focus').length) {
    	$('#txtDepoName').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtCustomerID:focus').length) {
    	$('#txtCustomerID').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slTier:focus').length) {
    	$('#slTier').click();
    }
});

// automaticaly open the select2 when it gets focus
/* $(document).on('focus', '.select2', function(e) {
	e.stopImmediatePropagation();
    $(this).siblings('select').select2('open');
}); */

$('select.select2').on('select2:closing', function (e) {
  $(e.target).data("select2").$selection.one('focus focusin', function (e) {
    e.stopPropagation();
  });
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

//IMAGE MODAL POP UP
 	// Get the modal
 	var modal = document.getElementById('myModal');

 	// Get the image and insert it inside the modal - use its "alt" text as a caption
 	var img = document.getElementById('myImg');
 	var modalImg = document.getElementById("img01");
 	var captionText = document.getElementById("caption");
 	img.onclick = function(){
 	    modal.style.display = "block";
 	    modalImg.src = this.src;
 	    captionText.innerHTML = this.alt;
 	}
 	
 	// Get the <span> element that closes the modal
 	var span = document.getElementsByClassName("closespanimage")[0];

 	// When the user clicks on <span> (x), close the modal
 	span.onclick = function() { 
 	  modal.style.display = "none";
 	}
 	
 	
 	//function load dynamic schedule data
 	function FuncLoadSchedule(lOrderID){
	    $('#dynamic-content-schedule').html(''); // leave this div blank
		$('#dvloader').show(); // load ajax loader on button click
	 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getOrderDetail',
	          type: 'POST',
	          data: 'orderid='+lOrderID,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-schedule').html(''); // blank before load.
	          $('#dynamic-content-schedule').html(data); // load here
			  $('#dvloader').hide(); // hide loader  
	     })
	     .fail(function(){
	          $('#dynamic-content-schedule').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			  $('#dvloader').hide(); // hide loader  
	     });
 	}
 	
 	//func save schedule
 	function FuncSaveSchedule() {
 		$('#dvloader').show();
 		
		var txtOrderManagementID = jQuery('#txtOrderManagementID').val();
		var txtOrderManagementDetailID = jQuery('#txtOrderManagementDetailID').val();
			if(txtOrderManagementDetailID == '' || txtOrderManagementDetailID == null)
				var lParamBtn = 'save';
			else
				var lParamBtn = 'update';
		var txtStuffingDate = jQuery('#txtStuffingDate').val();
		var slContainerType = jQuery('#slContainerType').val();
		var txtQuantity = jQuery('#txtQuantity').val();
	
	    if(!txtStuffingDate.match(/\S/)) {
	    	$("#txtStuffingDate").focus();
	    	$('#dvStuffingDate').addClass('has-error');
	    	$('#mrkStuffingDate').show();
	    	$('#dvloader').hide();
	        return false;
	    } 
	    if(!slContainerType.match(/\S/)) {
	    	$('#slContainerType').focus();
	    	$('#dvContainerType').addClass('has-error');
	    	$('#mrkContainerType').show();
	    	$('#dvloader').hide();
	        return false;
	    }
	    if(!txtQuantity.match(/\S/)) {    	
	    	$('#txtQuantity').focus();
	    	$('#dvQuantity').addClass('has-error');
	    	$('#mrkQuantity').show();
	    	$('#dvloader').hide();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/OrderManagementDetail',	
	        type:'POST',
	        data:{"key":lParamBtn,"txtOrderManagementID":txtOrderManagementID,"txtOrderManagementDetailID":txtOrderManagementDetailID,
	        	  "txtStuffingDate":txtStuffingDate, "slContainerType":slContainerType, "txtQuantity":txtQuantity},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	      		FuncLoadSchedule(txtOrderManagementID);
	      		FuncClearScheduleField();
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	            $('#dvloader').hide();
	        }
	    });
	    
	    FuncClear();
	    return true;
	}
	
	//show modal vendor/trucker
	$('#ModalVendor').on('shown.bs.modal', function (event) {
 		var button = $(event.relatedTarget);
 		var lOrderDetailID = button.data('lordermanagementdetailid');
    
    	$('#dynamic-content-vendor').html(''); // leave this div blank
 
	     $.ajax({
	          url: '${pageContext.request.contextPath}/getVendorOrderDetailByCustomerOrder',
	          type: 'POST',
	          data: 'orderDetailID='+lOrderDetailID,
	          dataType: 'html'
	     })
	     .done(function(data){
	          console.log(data); 
	          $('#dynamic-content-vendor').html(''); // blank before load.
	          $('#dynamic-content-vendor').html(data); // load here
	     })
	     .fail(function(){
	          $('#dynamic-content-vendor').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     });
	})
	
	//edit schedule
	function FuncUpdateSchedule(lOrderDetail, lStuffingDate, lContainerType, lQuantity) {
		var lStuffingDateConvertToDate = moment(lStuffingDate, "DD MMM YYYY").format('MM-DD-YYYY');
		
		$("#txtStuffingDate").datepicker("update", new Date(lStuffingDateConvertToDate));
		
		$("#txtOrderManagementDetailID").val(lOrderDetail);
		$("#txtStuffingDate").val(lStuffingDate);
	 	$("#slContainerType").val(lContainerType).trigger("change");
	 	$("#txtQuantity").val(lQuantity);
	}
	
	//delete schedule modal
	$('#ModalDeleteSchedule').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lOrderID = button.data('lordermanagementid');
		var lOrderDetailID = button.data('lordermanagementdetailid');
		$("#txtOrderManagementID").val(lOrderID);
		$("#temp_txtOrderDetailID").val(lOrderDetailID);
	});
	
	//delete schedule function
	function FuncDeleteSchedule(){
		$('#dvloader').show();
		
		$('#ModalDeleteSchedule').modal('toggle');
		
		var lOrderID = document.getElementById('txtOrderManagementID').value;;
		var lOrderDetailID = document.getElementById('temp_txtOrderDetailID').value;;
		
		$.ajax({
	          url: '${pageContext.request.contextPath}/OrderManagement',
	          type: 'POST',
	          data: {"key":'delete', "txtOrderManagementID": lOrderID, "txtOrderManagementDetailID": lOrderDetailID},
	          dataType: 'text'
	     })
	     .done(function(data){
	          FuncLoadSchedule(lOrderID);
	          FuncClearScheduleField();
	     })
	     .fail(function(){
	          $('#dynamic-content-schedule').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     });
	}
	
	/* KOJA SCRIPT */
	var sessionKoja = '';
	var kojaUsername = document.getElementById("kojaUsername").value;
	var kojaPassword = document.getElementById("kojaPassword").value;
	var kojaStreamUsername = document.getElementById("kojaStreamUsername").value;
	var kojaStreamPassword = document.getElementById("kojaStreamPassword").value;
	var kojaDevicename = document.getElementById("kojaDevicename").value;
	var kojaUrl = document.getElementById("kojaUrl").value;
	
	var soapMessageGetLogin = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:AUTH_GetLoginwsdl">'+
		 '<soapenv:Header/>'+
		   '<soapenv:Body>'+
		      '<urn:AUTH_GetLogin soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
		         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
		         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
		         '<fStream xsi:type="xsd:string">{"PASSWORD":"'+kojaStreamPassword+'","USERNAME":"'+kojaStreamUsername+'"}</fStream>'+
		         '<deviceName xsi:type="xsd:string">'+kojaDevicename+'</deviceName>'+
		      '</urn:AUTH_GetLogin>'+
		   '</soapenv:Body>'+
		'</soapenv:Envelope>';
		
	var soapMessageGetLogout = '';
	var soapMessageGetVesselVoyage = '';
			
	function reSoapMessageGetLogout(sessionKoja){
		soapMessageGetLogout = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:AUTH_GetLogOutwsdl">'+
			 '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:AUTH_GetLogOut soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<fStream xsi:type="xsd:string">{"SESSIONID":"'+sessionKoja+'"}</fStream>'+
			      '</urn:AUTH_GetLogOut>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function reSoapMessageGetVesselVoyage(sessionKoja){
		soapMessageGetVesselVoyage = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:MAIN_GetVesselVoyagewsdl">'+
			 '<soapenv:Header/>'+
			   '<soapenv:Body>'+
			      '<urn:MAIN_GetVesselVoyage soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
			         '<UserName xsi:type="xsd:string">'+kojaUsername+'</UserName>'+
			         '<Password xsi:type="xsd:string">'+kojaPassword+'</Password>'+
			         '<Creator xsi:type="xsd:string">'+sessionKoja+'</Creator>'+
			         '<fStream xsi:type="xsd:string">{"CATEGORY_ID":"E","VOYID_VOYAGE_CODE":"","VOYID_COMPANY_CODE":"","VESSEL_NAME":"","TERMINAL_ID":"KOJA","VOYID_VESSEL_CODE":""}</fStream>'+
			      '</urn:MAIN_GetVesselVoyage>'+
			   '</soapenv:Body>'+
			'</soapenv:Envelope>';
	}
	
	function ajaxGetVesselVoyage(sessionKoja, terminalName){
		$.ajax({
			    url: kojaUrl,
			    type: "POST",
			    dataType: "xml",
			    contentType: "text/xml; charset=\"utf-8\"",
			    data: soapMessageGetVesselVoyage,
			    success: function(soapResponse){
			        var soap = soapResponse.documentElement;
					var result = soap.getElementsByTagName("return");
					var status = JSON.parse(result[0].innerHTML).STATUS;
					if(status == 'FALSE'){
						//if session expired, logout session
						reSoapMessageGetLogout(sessionKoja);
						$.ajax({
						    url: kojaUrl,
						    type: "POST",
						    dataType: "xml",
						    contentType: "text/xml; charset=\"utf-8\"",
						    data: soapMessageGetLogout,
						    success: function(soapResponse){
						         var soap = soapResponse.documentElement;
								 var result = soap.getElementsByTagName("return");
								
								//relogin to get new session
								 $.ajax({
								    url: kojaUrl,
								    type: "POST",
								    dataType: "xml",
								    contentType: "text/xml; charset=\"utf-8\"",
								    data: soapMessageGetLogin,
								    success: function(soapResponse){
								         var soap = soapResponse.documentElement;
										 var result = soap.getElementsByTagName("return");
										 var newSession = JSON.parse(result[0].innerHTML).SESSIONID;
										 var newCustID = JSON.parse(result[0].innerHTML).CUST_ID;
										 reSoapMessageGetVesselVoyage(newSession);
										
										 if(newSession != undefined){
											//store new session in db
											$.ajax({
										          url: '${pageContext.request.contextPath}/KojaTransaction',
										          type: 'POST',
										          data: {"key":'updateSession', "sessionKoja": newSession, "custID": newCustID},
										          dataType: 'text'
										     })
										     .done(function(data){
										     	//after saving new session recall the ajax get vessel voyage function
												ajaxGetVesselVoyage(newSession, terminalName);
										     })
										     .fail(function(){
										     	console.log('system error');
										     });
										}	 
								    }
								});
								//end of relogin
						    }
						});
						//end of logout proccess
					}
					else{
					//if session active, get vessel data
						var vesselCode = JSON.parse(result[0].innerHTML).VOYID_VESSEL_CODE;
						var vesselName = JSON.parse(result[0].innerHTML).VESSEL_NAME;
						var voyageCode = JSON.parse(result[0].innerHTML).VOYID_VOYAGE_CODE;
						var voyageStatus = JSON.parse(result[0].innerHTML).VOYAGE_STATUS;
						var voyageCompany = JSON.parse(result[0].innerHTML).VOYID_COMPANY_CODE;
						var podCode = JSON.parse(result[0].innerHTML).POD;
						var podName = JSON.parse(result[0].innerHTML).POD_NAME;
						var voyageArrival = JSON.parse(result[0].innerHTML).ARRIVAL_DATETIME;
						var voyageDeparture = JSON.parse(result[0].innerHTML).DEPARTURE_DATETIME;
					 
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getVesselVoyage',
					          type: 'POST',
					          data: {key:'public', terminal:terminalName, vesselCode:vesselCode, vesselName:vesselName, voyageCode:voyageCode,
					          		voyageStatus:voyageStatus, voyageCompany:voyageCompany, podCode:podCode, podName:podName,
					          		voyageArrival:voyageArrival, voyageDeparture:voyageDeparture},
					          dataType: 'html'
					     })
					     .done(function(data){
					          console.log(data); 
					          $('#dynamic-content-vessel').html(''); // blank before load.
					          $('#dynamic-content-vessel').html(data); // load here
					          $('#dvloaderVessel').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-vessel').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					          $('#dvloaderVessel').hide();
					     });	
				  	 }
			   	 }
			});
	}		
	
	function FuncGetVesselVoyage() {
		var portID = document.getElementById("txtPortID").value;
		var teminalName;
		
		if(portID == 'PORT-0002'){
			terminalName = 'KOJA';
			$("#ModalVessel").modal();
			$('#dvloaderVessel').show();
		    $('#dynamic-content-vessel').html('');
		    
		    //check vessel voyage expiration
		    $.ajax({
		          url: '${pageContext.request.contextPath}/KojaTransaction',
		          type: 'POST',
		          data: {"key":'checkRequestVesselVoyage', terminalName:terminalName},
		          dataType: 'text'
		     })
		     .done(function(data){
		     	/* true = expired, false = not expired */
		     	if(data == 'true'){
		     		//if expired recall the service
		     		//get current koja session
					$.ajax({
				          url: '${pageContext.request.contextPath}/KojaTransaction',
				          type: 'POST',
				          data: {"key":'getSession'},
				          dataType: 'text'
				     })
				     .done(function(data){
				     	//data = session
				     	reSoapMessageGetVesselVoyage(data);
			     		ajaxGetVesselVoyage(data,terminalName); //call ajax get vessel voyage function
				     })
				     .fail(function(){
				     	console.log('system error');
				     	$("#ModalVessel").toggle();
						$('#dvloaderVessel').hide();
				     });
		     	}		     	
		     	else{
		     	//if not expired, get the data from the server
		     		$.ajax({
					          url: '${pageContext.request.contextPath}/getVesselVoyage',
					          type: 'POST',
					          data: {key:'local', terminal:terminalName},
					          dataType: 'html'
				     })
				     .done(function(data){
				          console.log(data); 
				          $('#dynamic-content-vessel').html(''); // blank before load.
				          $('#dynamic-content-vessel').html(data); // load here
				          $('#dvloaderVessel').hide();
				     })
				     .fail(function(){
				          $('#dynamic-content-vessel').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				          $('#dvloaderVessel').hide();
				     });	
		     	}
		     })
		     .fail(function(){
		     	console.log('system error');
		     });
		}
	}
</script>

</body>

</html>