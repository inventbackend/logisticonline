<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Customer</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="mainform/plugins/iCheck/all.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <style type="text/css">	
	#ModalUpdateInsert { overflow-y:scroll }
	
	/*  css for loading bar */
	 .loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 50%;
	}
	
	.loaderModal {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 45%;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}
	
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	/* end of css loading bar */
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_Customer" action = "${pageContext.request.contextPath}/Customer" method="post">
<input type="hidden" id="tempRole" value="<c:out value="${userRole}"/>" />
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Customer
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
		
		<c:if test="${condition == 'SuccessUpdateCustomer'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Update customer success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'FailedUpdateCustomer'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Update customer failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteCustomer'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Delete customer success.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteCustomer'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete customer failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
		
		<c:if test="${condition == 'FailedDeleteFactory'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete factory failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <c:if test="${userRole eq 'R002'}">
        <button type="button" class="btn btn-primary pull-right btn-lg" onclick="FuncSaveProfile()"> Save</button>
        </c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
		      
		      	  <!-- MainInfoPanel -->
      			  <label for="recipient-name" class="control-label">Company Info</label>
				  <div class="panel panel-primary" id="MainInfoPanel">
				  <div class="panel-body fixed-panel">
				  
				  	<div id="dvCustomerID" class="form-group col-xs-12">
		            <label for="lblCustomerID" class="control-label">Customer ID:</label>
		            <input type="text" class="form-control" id="txtCustomerID" name="txtCustomerID" readonly disabled>
		            </div>
		          	<div id="dvCustomerName" class="form-group col-xs-3">
		            <label for="lblCustomerName" class="control-label">Name:</label><label id="mrkCustomerName" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtCustomerName" name="txtCustomerName">
		          	</div>
		          	<div id="dvPIC1" class="form-group col-xs-3">
		            <label for="lblPIC1" class="control-label">PIC 1:</label><label id="mrkPIC1" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtPIC1" name="txtPIC1">
		          	</div>
		          	<div class="form-group col-xs-3">
		            <label for="lblPIC2" class="control-label">PIC 2:</label>
		            <input type="text" class="form-control" id="txtPIC2" name="txtPIC2">
		          	</div>
		          	<div class="form-group col-xs-3">
		            <label for="lblPIC3" class="control-label">PIC 3:</label>
		            <input type="text" class="form-control" id="txtPIC3" name="txtPIC3">
		          	</div>
		          	<div id="dvDomicile" class="form-group col-xs-3">
		            <label for="lblDomicile" class="control-label">Province:</label><label id="mrkDomicile" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="txtDomicile" name="txtDomicile" class="form-control select2" style="width: 100%;">
						<c:forEach items="${listProvince}" var="province">
							<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
						</c:forEach>
			       	</select>
		          	</div>
		          	<div id="dvAddress" class="form-group col-xs-3">
		            <label for="lblAddress" class="control-label">Address:</label><label id="mrkAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtAddress" name="txtAddress">
		          	</div>
		          	<div id="dvDistrict" class="form-group col-xs-3">
			            <label for="lblPostalCode" class="control-label">Postal Code:</label><label id="mrkDistrict" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="slDistrict" name="slDistrict">
		          	</div>
		          	<div id="dvEmail" class="form-group col-xs-3">
		            <label for="lblEmail" class="control-label">Email:</label><label id="mrkEmail" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtEmail" name="txtEmail">
		          	</div>
		          	<div id="dvOfficePhone" class="form-group col-xs-3">
		            <label for="lblOfficePhone" class="control-label">Office Phone:</label><label id="mrkOfficePhone" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtOfficePhone" name="txtOfficePhone">
		          	</div>
		          	<div class="form-group col-xs-3">
		            <label for="lblPhoneEmail" class="control-label">Mobile Phone:</label>
		            <input type="text" class="form-control" id="txtMobilePhone" name="txtMobilePhone">
		          	</div>
		          	<div id="dvNPWP" class="form-group col-xs-3">
		            <label for="lblNPWP" class="control-label">NPWP:</label><label id="mrkNPWP" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtNPWP" name="txtNPWP">
		          	</div>
		          	<div id="dvTDP" class="form-group col-xs-3">
		            <label for="lblTDP" class="control-label">TDP:</label><label id="mrkTDP" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtTDP" name="txtTDP">
		          	</div>
					<!-- 		          	<div id="dvDistrict" class="form-group col-xs-6"> -->
					<!-- 		          	<label for="lblDistrict" class="control-label">District:</label><label id="mrkDistrict" for="lbl-validation" class="control-label"><small>*</small></label> -->
					<!-- 			 		<select id="slDistrict" name="slDistrict" class="form-control select2" style="width: 100%;"> -->
					<%-- 						<c:forEach items="${listDistrict}" var="district"> --%>
					<%-- 							<option value="<c:out value="${district.districtID}" />"><c:out value="${district.districtID}" /> - <c:out value="${district.name}" /></option> --%>
					<%-- 						</c:forEach> --%>
					<!-- 			       	</select> -->
					<!-- 			      	</div> -->
		          	
				  </div>
				  </div>
				  <!-- /.MainInfoPanel -->
				  
				  <!-- BillingInfoPanel -->
				  <label for="recipient-name" class="control-label">Billing Info</label>
				  <div class="panel panel-primary" id="BillingInfoPanel">
				  <div class="panel-body fixed-panel">
				  	
				  	<div id="dvBillingAddress" class="form-group col-xs-4">
		            <label for="lblBillingAddress" class="control-label">Billing Address:</label><label id="mrkBillingAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtBillingAddress" name="txtBillingAddress">
		          	</div>
		          	<div id="dvTOP" class="form-group col-xs-4">
		          	<label for="lblTOP" class="control-label">TOP:</label><label id="mrkTOP" for="lbl-validation" class="control-label"><small>*</small></label>
			 		<select id="slTOP" name="slTOP" class="form-control select2" style="width: 100%;">
						<c:forEach items="${listTOP}" var="top">
							<option value="<c:out value="${top.topID}" />"><c:out value="${top.topID}" /> - <c:out value="${top.name}" /></option>
						</c:forEach>
			       	</select>
			      	</div>
			      	<div id="dvCreditLimit" class="form-group col-xs-4">
			      	<label for="lblCreditLimit" class="control-label">Credit Limit:</label><label id="mrkCreditLimit" for="lbl-validation" class="control-label"><small>*</small></label>
			        <input class="form-control number" id="txtCreditLimit" name="txtCreditLimit">
			        </div>
			        <div id="dvBankName" class="form-group col-xs-4">
			      	<label for="lblBankName" class="control-label">Bank Name:</label><label id="mrkBankName" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankName" name="txtBankName">
			        </div>
			        <div id="dvBankAcc" class="form-group col-xs-4">
			      	<label for="lblBankAcc" class="control-label">Bank Account:</label><label id="mrkBankAcc" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankAcc" name="txtBankAcc">
			        </div>
			        <div id="dvBankBranch" class="form-group col-xs-4">
			      	<label for="lblBankBranch" class="control-label">Bank Branch:</label><label id="mrkBankBranch" for="lbl-validation" class="control-label"><small>*</small></label>
			         <input type="text" class="form-control" id="txtBankBranch" name="txtBankBranch">
			        </div>
				  		
				  </div>
				  </div>
				  <!-- /.BillingInfoPanel -->
				  
				  
				  
				  <!-- Custom Customer -->
				  <label for="recipient-name" class="control-label">Custom Customer</label>
				  <div class="panel panel-primary" id="BillingInfoPanel">
				  <div class="panel-body fixed-panel">
				  
				  	
			      	<div class="form-check col-md-4">
			      	<label for="lblBillingAddress" class="control-label">Model Pricing</label>
			      	<br>
					  <input class="minimal" type="checkbox" name="modelPricing" id="mpTrucking" value="Trucking">
					  <label class="minimal" for="id">
					     Trucking
					  </label><br>
					  <input class="minimal" type="checkbox" name="modelPricing" id="mpCustomClearance" value="Custom Clearance">
					  <label class="minimal" for="id">
					     Custom Clearance
					  </label><br>
					  <input class="minimal" type="checkbox" name="modelPricing" id="mpReimburse" value="Reimburse">
					  <label class="minimal" for="id">
					     Reimburse
					  </label>
					</div>
				  	
				  	<div id="dvBillingAddress" class="form-group col-xs-4">
		            <label for="lblBillingAddress" class="control-label">PPN: (%)</label><label id="mrkBillingAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtPPN" name="txtPPN">
		          	</div>
		          	<div id="dvTOP" class="form-group col-xs-4">
		          	<label for="lblTOP" class="control-label">PPh23: (%)</label><label id="mrkTOP" for="lbl-validation" class="control-label"><small>*</small></label>
			 		<input type="text" class="form-control" id="txtPPh23" name="txtPPh23">
			      	</div>
			      	<div id="dvBillingAddress" class="form-group col-xs-4">
		            <label for="lblBillingAddress" class="control-label">Custome Clearance:</label><label id="mrkBillingAddress" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control number" id="txtCustomClearance" name="txtCustomeClearance">
		          	</div>
			      	<div class="form-check col-md-12">
			      	<br>
			      	*Model pricing can only tick: <br>
					  - Trucking, custom clearance, reimburse <br>
					  - Truckiing, custom clearance (trucking and hanling) <br>
					  - Trucking
			      	</div>
			      	
			      	
				  		
				  </div>
				  
				  </div>
				  <!-- /.Custom Customer -->
				  
				  <div class="row"></div>
				  
				  <!--  Factory Data Panel -->
				  <div id="dvFactoryPanel">
					  <h4 class="modal-title"><label>Factory</label></h4>	
					  <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalFactory" onclick="FuncNewFactory()"><i class="fa fa-plus-circle"></i> Add Factory</button><br><br>		 
			      	  <div id="dvloader" class="loader"></div>
			      	  <div id="dynamic-content-factory">
			      	  </div>
		      	  </div>
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Customer</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtCustomerID" name="temp_txtCustomerID"  />
									<p>Are you sure to delete this customer ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		
		<!--modal factory -->
		<div class="modal fade bs-example-modal-lg" id="ModalFactory" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			
			<div id="dvErrorAlertFactory" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				<label id="lblAlertFactory"></label>. <label id="lblAlertFactoryDescription"></label>.
			</div>
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalFactory" name="lblTitleModalFactory"></label></h4>	
			</div>
		    <div class="modal-body">
				<div id="dvFactoryName" class="form-group col-md-6">
					<label for="recipient-name" class="control-label">Factory Name</label><label id="mrkFactoryName" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtFactoryName" name="txtFactoryName">
					<input type="hidden" class="form-control" id="txtFactoryID" name="txtFactoryID">
				</div>
				<div id="dvFactoryPIC" class="form-group col-md-6">
					<label for="recipient-name" class="control-label">PIC</label><label id="mrkFactoryPIC" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtFactoryPIC" name="txtFactoryPIC">
				</div>
				<div id="dvFactoryAddress" class="form-group col-md-12">
					<label for="message-text" class="control-label">Address</label><label id="mrkFactoryAddress" for="recipient-name" class="control-label"><small>*</small></label>	
					<!-- <input type="text" class="form-control" id="txtFactoryAddress" name="txtFactoryAddress"> -->
					<textarea class="form-control" id="txtFactoryAddress" name="txtFactoryAddress" rows="3"></textarea>
				</div>
				<div class="form-group col-md-6">
		          <label class="control-label">Office Phone(optional):</label>
		          <input type="text" class="form-control" id="txtFactoryOfficePhone" name="txtFactoryOfficePhone">
		       	</div>
		       	<div class="form-group col-md-6">
		          <label class="control-label">Mobile Phone(optional):</label>
		          <input type="text" class="form-control" id="txtFactoryMobilePhone" name="txtFactoryMobilePhone">
		       	</div>
				<div id="dvFactoryProvince" class="form-group col-md-6">
				     <label class="control-label">Province:</label><label id="mrkFactoryProvince" for="lbl-validation" class="control-label"><small>*</small></label>
				     <select id="slFactoryProvince" name="slFactoryProvince" 
				     		class="form-control select2" style="width: 100%;">
						<c:forEach items="${listProvince}" var="province">
						<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
						</c:forEach>
				 	</select>
	  			</div>
				<div id="dvFactoryDistrict" class="form-group col-md-6">
 					<label for="message-text" class="control-label">District</label><label id="mrkFactoryDistrict" for="recipient-name" class="control-label"><small>*</small></label>
					<!-- 					<select id="slFactoryDistrict" name="slFactoryDistrict" class="form-control select2" style="width: 100%;"> -->
					<%-- 						<c:forEach items="${listDistrict}" var="district"> --%>
					<%-- 							<option value="<c:out value="${district.districtID}" />"><c:out value="${district.districtID}" /> - <c:out value="${district.name}" /></option> --%>
					<%-- 						</c:forEach> --%>
					<!-- 			      	</select> -->
			      	<select id="slFactoryDistrict" name="slFactoryDistrict" class="form-control" onfocus="FuncValShowDistrict()">
					</select>
		    	</div>
				<!--     			<div id="dvFactoryTier" class="form-group col-md-6" style="display:none"> -->
				<!--  					<label for="message-text" class="control-label">Tier</label><label id="mrkFactoryTier" for="recipient-name" class="control-label"><small>*</small></label> -->
				<!-- 					<select id="slFactoryTier" name="slFactoryTier" class="form-control select2" style="width: 100%;"> -->
				<%-- 						<c:forEach items="${listTier}" var="tier"> --%>
				<%-- 							<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.tierID}" /> - <c:out value="${tier.description}" /></option> --%>
				<%-- 						</c:forEach> --%>
				<!-- 			      	</select> -->
				<!-- 		    	</div> -->
  				<div class="row"></div>
			</div>
			
			<div class="modal-footer">
				<c:if test="${userRole eq 'R001'}">
					<button type="button" class="btn btn-primary" id="btnSaveFactory" name="btnSaveFactory" onclick="FuncProccessFactory('save','R001')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateFactory" name="btnUpdateFactory" onclick="FuncProccessFactory('update','R001')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				<c:if test="${userRole eq 'R002'}">
					<button type="button" class="btn btn-primary" id="btnSaveFactory" name="btnSaveFactory" onclick="FuncProccessFactory('save','R002')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateFactory" name="btnUpdateFactory" onclick="FuncProccessFactory('update','R002')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			</div>
			</div>
			</div>
		</div>
		
		<!--modal Delete Factory -->
		<div class="modal modal-danger" id="ModalDeleteFactory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Factory</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtFactoryCustomerID" name="temp_txtFactoryCustomerID"  />
						<input type="hidden" id="temp_txtFactoryID" name="temp_txtFactoryID"  />
									<p>Are you sure to delete this factory ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <c:if test="${userRole eq 'R001'}">
		        	<button type="button" id="btnDeleteFactory" name="btnDeleteFactory" class="btn btn-outline" onclick="FuncDeleteFactory('R001')">Delete</button>
		        </c:if>
		        <c:if test="${userRole eq 'R002'}">
		        	<button type="button" id="btnDeleteFactory" name="btnDeleteFactory" class="btn btn-outline" onclick="FuncDeleteFactory('R002')">Delete</button>
		        </c:if>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
	
	
		<!--modal rate management -->
		<div class="modal fade bs-example-modal-lg" id="ModalRateManagement" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">			
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalRateManagement" name="lblTitleModalRateManagement">Rate Management</label></h4>	
			</div>
		    <div class="modal-body">
				<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsertRate"><i class="fa fa-plus-circle"></i> Add Final Rate</button><br><br>		 
		      	  <div id="dvloaderRateManagement" class="loader"></div>
		      	  <div id="dynamic-content-rate-management">
		      	  </div>
			</div>
			</div>
			</div>
		</div>
		
		<!--modal update insert customer rate management -->
		<div class="modal fade" id="ModalUpdateInsertRate" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			
			<div id="dvErrorAlertRateManagement" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				<label id="lblAlertRateManagement"></label>. <label id="lblAlertRateManagementDescription"></label>.
			</div>
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label>Form Rate Management</label></h4>	
			</div>
		    <div class="modal-body">
				<div id="dvContainerType" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Container Type</label><label id="mrkContainerType" for="recipient-name" class="control-label"><small>*</small></label>	
					<select id="slContainerType" name="slContainerType" class="form-control select2" style="width: 100%;">
				 	</select>
				</div>
				<div id="dvFinalRate" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Final Rate</label><label id="mrkFinalRate" for="recipient-name" class="control-label"><small>*</small></label>	
					<input class="form-control number" id="txtFinalRate" name="txtFinalRate">
					<input type="hidden" id="txtFactoryID2" name="txtFactoryID2">
					<input type="hidden" id="txtDistrictID2" name="txtDistrictID2">
				</div>
  				<div class="row"></div>
			</div>
			
			<div class="modal-footer">
				<c:if test="${userRole eq 'R001'}">
					<button type="button" class="btn btn-primary" id="btnSaveRate" name="btnSaveRate" onclick="FuncProccessRate('saveFinalRate','R001')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateRate" name="btnUpdateRate" onclick="FuncProccessRate('updateFinalRate','R001')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				<c:if test="${userRole eq 'R002'}">
					<button type="button" class="btn btn-primary" id="btnSaveRate" name="btnSaveRate" onclick="FuncProccessRate('saveFinalRate','R002')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateRate" name="btnUpdateRate" onclick="FuncProccessRate('updateFinalRate','R002')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			</div>
			</div>
			</div>
		</div>
		
		<!--modal add customer/forwarder npwp -->
		<div class="modal fade bs-example-modal-lg" id="ModalOnbehalfNpwp" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">			
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalAddNpwp" name="lblTitleModalAddNpwp">Onbehalf Npwp Management</label></h4>	
			</div>
		    <div class="modal-body">
		    	<input type="hidden" id="txtTempCustomerID" name="txtTempCustomerID">
				<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalAddNpwp"><i class="fa fa-plus-circle"></i> Add On Behalf Npwp</button><br><br>		 
		      	  <div id="dvloaderOnbehalfNpwp" class="loaderModal"></div>
		      	  <div id="dynamic-content-onbehalf-npwp">
		      	  </div>
			</div>
			</div>
			</div>
		</div>
		
		
		<!--modal update insert customer onbehalf npwp -->
		<div class="modal fade" id="ModalAddNpwp" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			
			<div id="dvErrorAlertAddNpwp" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				<label id="lblAlertAddNpwp"></label>. <label id="lblAlertAddNpwpDescription"></label>.
			</div>
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label>Form Add Onbehalf Npwp</label></h4>	
			</div>
		    <div class="modal-body">
		    	<div id="dvOnbehalfName" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Onbehalf Name</label><label id="mrkOnbehalfName" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtOnbehalfName" name="txtOnbehalfName">
				</div>
				<div id="dvOnbehalfNpwp" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Onbehalf Npwp</label><label id="mrkOnbehalfNpwp" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtOnbehalfNpwp" name="txtOnbehalfNpwp">
					<input type="hidden" class="form-control" id="txtTempNpwp" name="txtTempNpwp">
				</div>
  				<div class="row"></div>
			</div>
			
			<div class="modal-footer">
				<c:if test="${userRole eq 'R001'}">
					<button type="button" class="btn btn-primary" id="btnSaveOnbehalf" name="btnSaveOnbehalf" onclick="FuncProccessOnbehalf('save')"><i class="fa fa-check"></i> Save</button>
					<button type="button" class="btn btn-primary" id="btnUpdateOnbehalf" name="btnUpdateOnbehalf" onclick="FuncProccessOnbehalf('update')"><i class="fa fa-edit"></i> Update</button>
				</c:if>
				
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			</div>
			</div>
			</div>
		</div>
			
        
		<c:if test="${userRole eq 'R001'}">
	        <table id="tb_master_customer" class="table table-bordered table-striped table-hover">
		        <thead style="background-color: #d2d6de;">
		                <tr>
		                  <th style="width: 60px"></th>	                	
		                  <th>ID</th>
		                  <th>Name</th>
		                  <th>PIC</th>
		                  <th>Address</th>
		                  <th>Email</th>
		                  <th>Phone</th>
		                  <th>NPWP</th>
		                  <th>TDP</th>
		                  <th>Province</th>
		                  <th>Postal</th>
		                  <th>Billing Address</th>
		                  <th>TOP</th>
		                  <th>Credit Limit</th>
		                  <th>Bank</th>
		                  <th>Account</th>
		                  <th>Branch</th>
		                </tr>
		        </thead>
	        
		        <tbody>
		        
			        <c:forEach items="${listCustomer}" var ="customer">
			        	<tr>
			        	<td>
			        		<table>
								<td>
									<button style="display: <c:out value="${buttonstatus}"/>"
									type="button" class="btn btn-info"
									data-toggle="modal"
									onclick="FuncButtonUpdate()" 
									data-target="#ModalUpdateInsert"
									data-lcustomerid='<c:out value="${customer.customerID}"/>'
									data-lcustomername='<c:out value="${customer.customerName}"/>' 
									data-lpic1='<c:out value="${customer.PIC1}"/>'
									data-lpic2='<c:out value="${customer.PIC2}"/>'
									data-lpic3='<c:out value="${customer.PIC3}"/>'
									data-laddress='<c:out value="${customer.customerAddress}"/>'
									data-lemail='<c:out value="${customer.email}"/>' 
									data-lofficephone='<c:out value="${customer.officePhone}"/>'
									data-lmobilephone='<c:out value="${customer.mobilePhone}"/>'
									data-lnpwp='<c:out value="${customer.NPWP}"/>'
									data-ldomicile='<c:out value="${customer.domicile}"/>'
									data-ltdp='<c:out value="${customer.TDP}"/>' 
									data-ldistrict='<c:out value="${customer.districtID}"/>'
									data-lpostalcode='<c:out value="${customer.postalCode}"/>'
									data-lbillingaddress='<c:out value="${customer.billingAddress}"/>'
									data-ltop='<c:out value="${customer.TOPID}"/>'
									data-lcreditlimit='<c:out value="${customer.iCreditLimit}"/>'
									data-lbankname='<c:out value="${customer.bankName}"/>' 
									data-lbankacc='<c:out value="${customer.bankAcc}"/>'
									data-lbankbranch='<c:out value="${customer.bankBranch}"/>'
									data-lppn='<c:out value="${customer.ppn}"/>'
									data-lpph23='<c:out value="${customer.pph23}"/>'
									data-lproductinvoice='<c:out value="${customer.productInvoice}"/>'
									data-lcustomclearance='<c:out value="${customer.customClearance}"/>'
									><i class="fa fa-edit"></i></button>
								</td>
								<td>&nbsp</td>
								<td>
									<button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
					        		data-target="#ModalDelete" data-lcustomerid='<c:out value="${customer.customerID}"/>'>
					        		<i class="fa fa-trash"></i>
					        	</button>
								</td>
								<!-- <td>&nbsp</td> -->
								<!-- 							<td> -->
								<%-- 								<c:if test="${showDetailButtonStatus == 'none'}"> --%>
								<%-- 									<button style="display: <c:out value="${showDetailButtonStatus}"/>" type="button" id="btnShowFactory" name="btnShowFactory"  class="btn btn-default"><i class="fa fa-list-ul"></i></button> --%>
								<%-- 								</c:if> --%>
								<%-- 								<c:if test="${showDetailButtonStatus != 'none'}"> --%>
								<%-- 									<a href="${pageContext.request.contextPath}/Factory?custid=<c:out value="${customer.customerID}" />&custname=<c:out value="${customer.customerName}" />"><button type="button" id="btnShowFactory" name="btnShowFactory"  class="btn btn-default"><i class="fa fa-list-ul"></i></button></a> --%>
								<%-- 								</c:if> --%>
								<!-- 							</td> -->
								</table>
				        </td>
				        <td><c:out value="${customer.customerID}"/></td>
			        	<td><c:out value="${customer.customerName}"/></td>
						<td><c:out value="${customer.PIC1}"/></td>
						<td><c:out value="${customer.customerAddress}"/></td>
						<td><c:out value="${customer.email}"/></td>
						<td><c:out value="${customer.officePhone}"/></td>
						<td>
							<a href=""
							style="color: #144471; font-weight: bold;"
							data-toggle="modal" data-target="#ModalOnbehalfNpwp"
							title="click to add customer/forwarder onbehalf npwp"
							data-lcustomerid='<c:out value="${customer.customerID}" />'
							data-lkey="addNpwp">
							<c:out value="${customer.NPWP}"/>
							</a>
						</td>
						<td><c:out value="${customer.TDP}"/></td>
						<td><c:out value="${customer.domicile}"/></td>
						<td><c:out value="${customer.postalCode}"/></td>
						<td><c:out value="${customer.billingAddress}"/></td>
						<td><c:out value="${customer.TOP}"/></td>
						<td><fmt:setLocale value="id_ID"/>
							<fmt:formatNumber value="${customer.iCreditLimit}" type="currency"/>
						</td>
						<td><c:out value="${customer.bankName}"/></td>
						<td><c:out value="${customer.bankAcc}"/></td>
						<td><c:out value="${customer.bankBranch}"/></td>
		        		</tr>
			        </c:forEach>
		        
		        </tbody>
	        </table>
        </c:if>
        
        <c:if test="${userRole eq 'R002'}">
        	<c:forEach items="${listCustomer}" var ="customer">
        	
	        	<div class="row">
	        		<div id="dvID" class="form-group col-xs-4">
	        			<label class="control-label">ID:</label>
			          	<input type="text" class="form-control input-lg" id="txtID" name="txtID" value="<c:out value="${customer.customerID}"/>" readonly disabled>
	        		</div> 
	        		<div id="dvName" class="form-group col-xs-4">
	        			<label class="control-label">Name:</label>
			          	<input type="text" class="form-control input-lg" id="txtName" name="txtName" value="<c:out value="${customer.customerName}"/>">
	        		</div> 
	        		<div id="dvEmail2" class="form-group col-xs-4">
	        			<label class="control-label">Email:</label>
			          	<input type="text" class="form-control input-lg" id="email" name="email" value="<c:out value="${customer.email}"/>">
	        		</div>
	        		<div id="dv2PIC1" class="form-group col-xs-4">
	        			<label class="control-label">PIC1:</label>
			          	<input type="text" class="form-control input-lg" id="pic1" name="pic1" value="<c:out value="${customer.PIC1}"/>">
	        		</div>
	        		<div id="dv2PIC2" class="form-group col-xs-4">
	        			<label class="control-label">PIC2:</label>
			          	<input type="text" class="form-control input-lg" id="pic2" name="pic2" value="<c:out value="${customer.PIC2}"/>">
	        		</div>
	        		<div id="dv2PIC3" class="form-group col-xs-4">
	        			<label class="control-label">PIC3:</label>
			          	<input type="text" class="form-control input-lg" id="pic3" name="pic3" value="<c:out value="${customer.PIC3}"/>">
	        		</div> 
	        		<div id="dv2Province" class="form-group col-xs-4">
	        			<label class="control-label">Province:</label>
	        			<input type="hidden" id="hiddenProvince" value="<c:out value="${customer.domicile}"/>">	        			
			          	<select id="province" name="province" class="form-control select2" style="width: 100%;">
							<c:forEach items="${listProvince}" var="province">
								<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
							</c:forEach>
					 	</select>
	        		</div> 
	        		<div id="dv2Address" class="form-group col-xs-4">
	        			<label class="control-label">Address:</label>
			          	<input type="text" class="form-control input-lg" id="address" name="address" value="<c:out value="${customer.customerAddress}"/>">
	        		</div> 
	        		<div id="dv2PostalCode" class="form-group col-xs-4">
	        			<label class="control-label">Postal Code:</label>
			          	<input type="text" class="form-control input-lg" id="postalCode" name="postalCode" value="<c:out value="${customer.postalCode}"/>">
	        		</div>
	        		<div id="dv2OfficePhone" class="form-group col-xs-3">
	        			<label class="control-label">Office Phone:</label>
			          	<input type="text" class="form-control input-lg" id="officePhone" name="officePhone" value="${customer.officePhone}">
	        		</div>
	        		<div id="dv2MobilePhone" class="form-group col-xs-3">
	        			<label class="control-label">Mobile Phone:</label>
			          	<input type="text" class="form-control input-lg" id="mobilePhone" name="mobilePhone" value="<c:out value="${customer.mobilePhone}"/>">
	        		</div>
	        		<div id="dv2NPWP" class="form-group col-xs-3">
	        			<label class="control-label">NPWP:</label>
			          	<input type="text" class="form-control input-lg" id="npwp" name="npwp" value="<c:out value="${customer.NPWP}"/>">
	        		</div>
	        		<div id="dv2TDP" class="form-group col-xs-3">
	        			<label class="control-label">TDP:</label>
			          	<input type="text" class="form-control input-lg" id="tdp" name="tdp" value="<c:out value="${customer.TDP}"/>">
	        		</div>
	        		<div id="dv2BillingAddress" class="form-group col-xs-4">
	        			<label class="control-label">Billing Address:</label>
			          	<input type="text" class="form-control input-lg" id="billingAddress" name="billingAddress" value="<c:out value="${customer.billingAddress}"/>">
	        		</div>
	        		<div id="dv2TOP" class="form-group col-xs-4">
	        			<label class="control-label">Term Of Payment:</label>
	        			<input type="hidden" id="hiddenTOP" value="<c:out value="${customer.TOPID}"/>">
			          	<select id="termOfPayment" name="termOfPayment" class="form-control select2" style="width: 100%;">
							<c:forEach items="${listTOP}" var="top">
								<option value="<c:out value="${top.topID}" />"><c:out value="${top.topID}" /> - <c:out value="${top.name}" /></option>
							</c:forEach>
					 	</select>
	        		</div>
	        		<div id="dv2CreditLimit" class="form-group col-xs-4">
	        			<label class="control-label">Credit Limit:</label>
			          	<input type="text" class="form-control input-lg number" id="creditLimit" name="creditLimit" value="<c:out value="${customer.iCreditLimit}"/>">
	        		</div>
	        		<div id="dv2BankName" class="form-group col-xs-4">
	        			<label class="control-label">Bank Name:</label>
			          	<input type="text" class="form-control input-lg" id="bankName" name="bankName" value="<c:out value="${customer.bankName}"/>"> 
	        		</div>
	        		<div id="dv2BankAccount" class="form-group col-xs-4">
	        			<label class="control-label">Bank Account:</label>
			          	<input type="text" class="form-control input-lg" id="bankAccount" name="bankAccount" value="<c:out value="${customer.bankAcc}"/>">
	        		</div>
	        		<div id="dv2BankBranch" class="form-group col-xs-4">
	        			<label class="control-label">Bank Branch:</label>
			          	<input type="text" class="form-control input-lg" id="bankBranch" name="bankBranch" value="<c:out value="${customer.bankBranch}"/>">
	        		</div>
	        	</div>
        	</c:forEach>
        	<br>
        
			<!-- factory panel  -->
			<section>
		      <h3>
		        Factory
		      </h3>
		    </section>
		    		    
		    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalFactory" onclick="FuncNewFactory()"><i class="fa fa-plus-circle"></i> New</button><br><br>		 
	      	<div id="dvloader2" class="loader"></div>
	      	<div id="dynamic-content-factory2"></div>
        </c:if>
                
        </div>
        <!-- /.box-body -->
  		</div>
  		<!-- /.box -->
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="mainform/plugins/iCheck/icheck.min.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
  	//Initialize Select2 Elements
	$(".select2").select2();
  	
	
	
	//custom number input type
	$('input.number').on('keyup ready', function(event) {
		console.log("ready")
	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40) return;

	  // format number
	  $(this).val(function(index, value) {
	    return value
	    .replace(/\D/g, "")
	    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	    ;
	  });
	});
		
    $('#tb_master_customer').DataTable({
    	"aaSorting": [],
		"scrollX": true
    });
    $('#M002').addClass('active');
	$('#M003').addClass('active');
	$('#MenuCustomerProfile').addClass('active');
	
	//shortcut for button 'new'
    /* Mousetrap.bind('n', function() {
    	var tempRole = document.getElementById('tempRole').value;
    	if(tempRole != 'R002'){
    		FuncButtonNew(),
    		$('#ModalUpdateInsert').modal('show')
    	}
    }); */
	
	$("#dvErrorAlert").hide();
	$("#dvErrorAlertFactory").hide();
	$('#dvErrorAlertRateManagement').hide();
	$('#dvErrorAlertAddNpwp').hide();
	$('#txtDomicile').val('').trigger("change");
	
	$('#slFactoryProvince').val('').trigger("change");
	$('#slFactoryDistrict').val('').trigger("change");
	//$('#slFactoryTier').val('').trigger("change");
	
	$(":input").inputmask();
	$("#officePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
	
  	$("#txtOfficePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
  	$("#txtFactoryOfficePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
  	
  	$('#dvloader').hide();
  	$('#dvloaderRateManagement').hide();
  	
  		//for customer role page
		var tempRole = document.getElementById('tempRole').value;
		if(tempRole == 'R002'){
			var hiddenProvince = document.getElementById('hiddenProvince').value;
			$('#province').val(hiddenProvince).trigger("change");
			
			var hiddenTOP = document.getElementById('hiddenTOP').value;
			$('#termOfPayment').val(hiddenTOP).trigger("change");
		
			$('#dvloader2').show();
			
		  	//load dynamic factory by customer
		  	 var customerid = document.getElementById('txtID').value;
			 /* var customerid = 'CustomerAccess'; */
		     $('#dynamic-content-factory2').html(''); // leave this div blank
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
		          type: 'POST',
		          data: 'customerid='+customerid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          // console.log(data); 
		          $('#dynamic-content-factory2').html(''); // blank before load.
		          $('#dynamic-content-factory2').html(data); // load here
		          $('#dvloader2').hide();
		     })
		     .fail(function(){
		          $('#dynamic-content-factory2').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		     	  $('#dvloader2').hide();
		     });
		}
  });
</script>

<script>
	//Modal Delete Customer
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lCustomerID = button.data('lcustomerid');
		$("#temp_txtCustomerID").val(lCustomerID);
	})
	
	//Modal Delete Factory
	$('#ModalDeleteFactory').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		//var lCustomerID = document.getElementById('txtCustomerID').value;
		var lFactoryID = button.data('lfactoryid');
		//$("#temp_txtFactoryCustomerID").val(lCustomerID);
		$("#temp_txtFactoryID").val(lFactoryID);
	})
	</script>
<!-- modal script -->
<script>
	//Modal Customer
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var modal = $(this)
	  modal.find('.modal-body #txtCustomerID').val(button.data('lcustomerid'))
	  modal.find('.modal-body #txtCustomerName').val(button.data('lcustomername'))
	  modal.find('.modal-body #txtPIC1').val(button.data('lpic1'))
	  modal.find('.modal-body #txtPIC2').val(button.data('lpic2'))
	  modal.find('.modal-body #txtPIC3').val(button.data('lpic3'))
	  modal.find('.modal-body #txtAddress').val(button.data('laddress'))
	  modal.find('.modal-body #txtEmail').val(button.data('lemail'))
	  
	  if(button.data('lofficephone') != undefined && button.data('lofficephone').split("-")[0].length == 3)
			$("#txtOfficePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
	  else
	  		$("#txtOfficePhone").inputmask({"mask": "9999-999999999999", "placeholder": ""});
	  modal.find('.modal-body #txtOfficePhone').val(button.data('lofficephone'))
	  
	  modal.find('.modal-body #txtMobilePhone').val(button.data('lmobilephone'))
	  modal.find('.modal-body #txtNPWP').val(button.data('lnpwp'))
	  modal.find('.modal-body #txtTDP').val(button.data('ltdp'))
	  modal.find('.modal-body #txtDomicile').val(button.data('ldomicile')).trigger("change")
	  modal.find('.modal-body #slDistrict').val(button.data('lpostalcode'))
	  modal.find('.modal-body #slTOP').val(button.data('ltop')).trigger("change")
	  modal.find('.modal-body #txtBillingAddress').val(button.data('lbillingaddress'))
	  modal.find('.modal-body #txtPPN').val(button.data('lppn'))
	  modal.find('.modal-body #txtPPh23').val(button.data('lpph23'))
	  modal.find('.modal-body #txtCustomClearance').val(button.data('lcustomclearance'))
	  var modelPrice = button.data('lproductinvoice');
	  console.log(modelPrice)
	  for ( var listMp in modelPrice) {
		  modal.find('.modal-body #mp'+modelPrice[listMp].replace(/\s/g, '')).iCheck('check');
	  }
	  
	  modal.find(".modal-body #trucking").prop('checked', true);
	  
	  if(button.data('lcreditlimit')==null || button.data('lcreditlimit')=='')
		$('#txtCreditLimit').val('25000000');
	  else
	  	modal.find('.modal-body #txtCreditLimit').val(button.data('lcreditlimit'))
	  
	  modal.find('.modal-body #txtBankName').val(button.data('lbankname'))
	  modal.find('.modal-body #txtBankAcc').val(button.data('lbankacc'))
	  modal.find('.modal-body #txtBankBranch').val(button.data('lbankbranch'))
	  
	  $("#txtCustomerName").focus();
	  
		  //if update, get the factory panel
		  if(button.data('lcustomerid')!=null || button.data('lcustomerid')!=''){
		  		$('#dvloader').show();
			  	//load dynamic factory by customer
				 var customerid = document.getElementById('txtCustomerID').value;
			     $('#dynamic-content-factory').html(''); // leave this div blank
			     $.ajax({
			          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
			          type: 'POST',
			          data: 'customerid='+customerid,
			          dataType: 'html'
			     })
			     .done(function(data){
			          // console.log(data); 
			          $('#dynamic-content-factory').html(''); // blank before load.
			          $('#dynamic-content-factory').html(data); // load here
			          $('#dvloader').hide();
			     })
			     .fail(function(){
			          $('#dynamic-content-factory').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			     	  $('#dvloader').hide();
			     });
		  }
	})
	
	function FuncValShowDistrict(){	
		var FactoryProvince = document.getElementById('slFactoryProvince').value;
	
		FuncClear();
	
		if(!FactoryProvince.match(/\S/)) {    	
	    	$('#slFactoryProvince').focus();
	    	$('#dvFactoryProvince').addClass('has-error');
	    	$('#mrkFactoryProvince').show();
	    	
	    	alert("Fill Province First");
	        return false;
	    } 
	    else{
	    	FuncShowDistrict();
	    }
		
		return true;
	}
	
	//function new and update factory
	function FuncNewFactory() {
		$('#btnSaveFactory').show();
		$('#btnUpdateFactory').hide();
		document.getElementById("lblTitleModalFactory").innerHTML = "Add Factory";
	
		FuncClear();
	}

	function FuncUpdateFactory(lProvince) {
		$('#btnSaveFactory').hide();
		$('#btnUpdateFactory').show();
		document.getElementById('lblTitleModalFactory').innerHTML = 'Edit Factory';
		
		FuncClear();
		$('#slFactoryProvince').val(lProvince).trigger("change");
		FuncShowDistrict();
	}

	//Modal Factory
	$('#ModalFactory').on('shown.bs.modal', function (event) {
		$("#dvErrorAlertFactory").hide();
	
 		var button = $(event.relatedTarget);
 		var lFactoryID = button.data('lfactoryid');
 		var lFactoryName = button.data('lfactoryname');
 		var lPIC = button.data('lpic');
 		var lOfficePhone = button.data('lofficephone');
 		var lMobilePhone = button.data('lmobilephone');
 		var lProvince = button.data('lprovince');
 		var lAddress = button.data('laddress');
 		var lDistrictID = button.data('ldistrictid');
		//var lTierID = button.data('ltierid');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtFactoryID").val(lFactoryID);
		modal.find(".modal-body #txtFactoryName").val(lFactoryName);
		modal.find(".modal-body #txtFactoryPIC").val(lPIC);
		
		if(lOfficePhone != undefined && lOfficePhone.split("-")[0].length == 3)
			$("#txtFactoryOfficePhone").inputmask({"mask": "999[9]-999999999999", "placeholder": ""});
		else
			$("#txtFactoryOfficePhone").inputmask({"mask": "9999-999999999999", "placeholder": ""});
		modal.find(".modal-body #txtFactoryOfficePhone").val(lOfficePhone);
		
		modal.find(".modal-body #txtFactoryMobilePhone").val(lMobilePhone);
		modal.find(".modal-body #slFactoryProvince").val(lProvince).trigger("change");
 		modal.find(".modal-body #txtFactoryAddress").val(lAddress);
 		modal.find(".modal-body #slFactoryDistrict").val(lDistrictID).trigger("change");
		//modal.find(".modal-body #slFactoryTier").val(lTierID).trigger("change");
 		
 		$('#txtFactoryName').focus();
	})
	
	//Modal Rate Management
	$('#ModalRateManagement').on('shown.bs.modal', function (event) {
		$('#dvloaderRateManagement').show();
		
		var userRole = document.getElementById("tempRole").value;
 		var button = $(event.relatedTarget);
 		var lDistrictID = button.data('ldistrictid');
 		$('#txtDistrictID2').val(lDistrictID);
 		
 		var lFactoryID = button.data('lfactoryid');
 		$('#txtFactoryID2').val(lFactoryID);
 		
 		if(userRole == 'R001')
 			var lCustomerID = document.getElementById('txtCustomerID').value;
 		else
 			var lCustomerID = document.getElementById('txtID').value;
 		
 		
 		var modal = $(this);
 		
 		$.ajax({
	          url: '${pageContext.request.contextPath}/getCustomerRateManagement',
	          type: 'POST',
	          data: {"customerid":lCustomerID, "factoryid":lFactoryID, "districtid":lDistrictID},
	          dataType: 'html'
	     })
	     .done(function(data){
	          // console.log(data); 
	          $('#dynamic-content-rate-management').html(''); // blank before load.
	          $('#dynamic-content-rate-management').html(data); // load here
	          $('#dvloaderRateManagement').hide();
	     })
	     .fail(function(){
	          $('#dynamic-content-rate-management').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     	  $('#dvloaderRateManagement').hide();
	     });
	})
	
	//Modal Rate Management
	$('#ModalUpdateInsertRate').on('shown.bs.modal', function (event) {
		$('#dvErrorAlertRateManagement').hide();
		
 		var button = $(event.relatedTarget);
 		var lFinalRate = button.data('lfinalrate');
 		var lDistrictID = document.getElementById("txtDistrictID2").value;
 		var lContainerType = button.data('lcontainertypeid')
 		
 		var modal = $(this);
 		
 		if(lFinalRate == undefined){
 			$("slContainerType").prop("disabled", false);
 			$('#btnSaveRate').show();
 			$('#btnUpdateRate').hide();
 		}
 		else{
 			$("slContainerType").prop("disabled", true);
	 		$('#btnSaveRate').hide();
 			$('#btnUpdateRate').show();
 		}
	 		
 		modal.find(".modal-body #txtFinalRate").val(lFinalRate);
 		
 		$('#slContainerType').find('option').remove();
 		
 		$.ajax({
	          url: '${pageContext.request.contextPath}/getContainerTypeByDistrictRate',
	          type: 'POST',
	          data: {"districtid":lDistrictID},
	          dataType: 'json'
	     })
	     .done(function(data){
	          // console.log(data);
	           
	          //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slContainerType').append("<option value=\"" + key + "\">" + val + "</option>");
		    }
		    
		    $("#slContainerType").val(lContainerType).trigger("change");
	     })
	     .fail(function(){
	     	console.log('Service call failed!');
	     });
	})
	
	
	//Modal Onbehalf Npwp
	$('#ModalOnbehalfNpwp').on('shown.bs.modal', function (event) {
		$('#dvloaderOnbehalfNpwp').show();
		$('#dynamic-content-onbehalf-npwp').html(''); // blank before load.
		
 		var button = $(event.relatedTarget);
 		var lCustomerID = button.data('lcustomerid');
 		$('#txtTempCustomerID').val(lCustomerID);
 		
 		var modal = $(this);
 		
 		$.ajax({
	          url: '${pageContext.request.contextPath}/getOnbehalfNpwp',
	          type: 'POST',
	          data: {"customerid":lCustomerID},
	          dataType: 'html'
	     })
	     .done(function(data){
	          // console.log(data);
	          $('#dynamic-content-onbehalf-npwp').html(data); // load here
	          $('#dvloaderOnbehalfNpwp').hide();
	     })
	     .fail(function(){
	          $('#dynamic-content-onbehalf-npwp').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     	  $('#dvloaderOnbehalfNpwp').hide();
	     });
	})
	
	
	//Modal Add Npwp
	$('#ModalAddNpwp').on('shown.bs.modal', function (event) {
		$('#dvErrorAlertAddNpwp').hide();
		FuncClear();
		
 		var button = $(event.relatedTarget);
 		var lOnbehalfName = button.data('lonbehalf');
 		var lOnbehalfNpwp = button.data('lonbehalfnpwp');
 		
 		var modal = $(this);
 		
 		if(lOnbehalfName == undefined){
 			$('#btnSaveOnbehalf').show();
 			$('#btnUpdateOnbehalf').hide();
 		}
 		else{
	 		$('#btnSaveOnbehalf').hide();
 			$('#btnUpdateOnbehalf').show();
 		}
	 		
 		modal.find(".modal-body #txtOnbehalfName").val(lOnbehalfName);
 		modal.find(".modal-body #txtOnbehalfNpwp").val(lOnbehalfNpwp);
 		modal.find(".modal-body #txtTempNpwp").val(lOnbehalfNpwp);
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>
	function FuncClear(){
		$('#mrkCustomerName').hide();
		$('#mrkPIC1').hide();
		$('#mrkAddress').hide();
		$('#mrkEmail').hide();
		$('#mrkOfficePhone').hide();
		$('#mrkNPWP').hide();
		$('#mrkTDP').hide();
		$('#mrkDomicile').hide();
		$('#mrkDistrict').hide();
		$('#mrkBillingAddress').hide();
		$('#mrkTOP').hide();
		$('#mrkCreditLimit').hide();
		$('#mrkBankName').hide();
		$('#mrkBankAcc').hide();
		$('#mrkBankBranch').hide();
		$('#mrkContainerType').hide();
		$('#mrkFinalRate').hide();
		$('#mrkOnbehalfName').hide();
		$('#mrkOnbehalfNpwp').hide();
		
		$('#dvCustomerName').removeClass('has-error');
		$('#dvPIC1').removeClass('has-error');
		$('#dvAddress').removeClass('has-error');
		$('#dvEmail').removeClass('has-error');
		$('#dvOfficePhone').removeClass('has-error');
		$('#dvNPWP').removeClass('has-error');
		$('#dvTDP').removeClass('has-error');
		$('#dvDomicile').removeClass('has-error');
		$('#dvDistrict').removeClass('has-error');
		$('#dvBillingAddress').removeClass('has-error');
		$('#dvTOP').removeClass('has-error');
		$('#dvCreditLimit').removeClass('has-error');
		$('#dvBankName').removeClass('has-error');
		$('#dvBankAcc').removeClass('has-error');
		$('#dvBankBranch').removeClass('has-error');
		$('#dvContainerType').removeClass('has-error');
		$('#dvFinalRate').removeClass('has-error');
		
		$('#mrkFactoryName').hide();
		$('#mrkFactoryAddress').hide();
		$('#mrkFactoryProvince').hide();
		$('#mrkFactoryPIC').hide();
		$('#mrkFactoryDistrict').hide();
		//$('#mrkFactoryTier').hide();
		
		$('#dvFactoryName').removeClass('has-error');
		$('#dvFactoryAddress').removeClass('has-error');
		$('#dvFactoryDistrict').removeClass('has-error');
		//$('#dvFactoryTier').removeClass('has-error');
		$('#dvFactoryPIC').removeClass('has-error');
		$('#dvFactoryProvince').removeClass('has-error');
		$('#dvOnbehalfName').removeClass('has-error');
		$('#dvOnbehalfNpwp').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
		$("#dvErrorAlertFactory").hide();
		$("#dvErrorAlertRateManagement").hide();
		$("#dvErrorAlertAddNpwp").hide();
	}
	
	function FuncButtonNew() {
		$('#dvCustomerID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Customer";	
	
		$("#dvFactoryPanel").hide();
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		//iCheck for checkbox and radio inputs
		
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		  checkboxClass: 'icheckbox_minimal-blue',
		  radioClass: 'iradio_minimal-blue'
		});
		
		$('#dvCustomerID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Customer';
		
		$("#dvFactoryPanel").show();
		FuncClear();
	}
	
	function FuncShowDistrict(){	
 		$('#slFactoryDistrict').find('option').remove();
   		var param = document.getElementById('slFactoryProvince').value;
 	     
         $.ajax({
              url: '${pageContext.request.contextPath}/GetDistrict',
              type: 'POST',
              data: {provinceid : param},
              dataType: 'json'
         })
         .done(function(data){
              console.log(data);
		
		      //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slFactoryDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
		    }
         })
         .fail(function(){
        	 console.log('Service call failed!');
         });
 	}
	
	//function save and update customer
	function FuncValEmptyInput(lParambtn) {
		var txtCustomerID = document.getElementById('txtCustomerID').value;
		var txtCustomerName = document.getElementById('txtCustomerName').value;
		var txtPIC1 = document.getElementById('txtPIC1').value;
		var txtPIC2 = document.getElementById('txtPIC2').value;
		var txtPIC3 = document.getElementById('txtPIC3').value;
		var txtAddress = document.getElementById('txtAddress').value;
		var txtEmail = document.getElementById('txtEmail').value;
		var txtOfficePhone = document.getElementById('txtOfficePhone').value;
		var txtMobilePhone = document.getElementById('txtMobilePhone').value;
		var txtNPWP = document.getElementById('txtNPWP').value;
		var txtTDP = document.getElementById('txtTDP').value;
		var slDistrict = document.getElementById('slDistrict').value;
		var slTOP = document.getElementById('slTOP').value;
		var txtBillingAddress = document.getElementById('txtBillingAddress').value;
		var txtDomicile = document.getElementById('txtDomicile').value;
		var txtCreditLimit = document.getElementById('txtCreditLimit').value;
		var txtBankName = document.getElementById('txtBankName').value;
		var txtBankAcc = document.getElementById('txtBankAcc').value;
		var txtBankBranch = document.getElementById('txtBankBranch').value;
		var txtPPN = document.getElementById('txtPPN').value;
		var txtPPh23 = document.getElementById('txtPPh23').value;
		var txtCustomClearance = document.getElementById('txtCustomClearance').value;
		var txtModelPricing = function() {
			var arrModalPricing = [];
			$.each($("input[name='modelPricing']:checked"), function(){            
		    	arrModalPricing.push($(this).val());
		    });
			  return JSON.stringify(arrModalPricing);
		};
		
	    if(!txtCustomerName.match(/\S/)) {    	
	    	$('#txtCustomerName').focus();
	    	$('#dvCustomerName').addClass('has-error');
	    	$('#mrkCustomerName').show();
	        return false;
	    } 
	    if(!txtPIC1.match(/\S/)) {    	
	    	$('#txtPIC1').focus();
	    	$('#dvPIC1').addClass('has-error');
	    	$('#mrkPIC1').show();
	        return false;
	    }
	    if(!txtAddress.match(/\S/)) {    	
	    	$('#txtAddress').focus();
	    	$('#dvAddress').addClass('has-error');
	    	$('#mrkAddress').show();
	        return false;
	    } 
	    if(!txtEmail.match(/\S/)) {    	
	    	$('#txtEmail').focus();
	    	$('#dvEmail').addClass('has-error');
	    	$('#mrkEmail').show();
	        return false;
	    }
	    if(!txtOfficePhone.match(/\S/)) {    	
	    	$('#txtOfficePhone').focus();
	    	$('#dvOfficePhone').addClass('has-error');
	    	$('#mrkOfficePhone').show();
	        return false;
	    }
	    if(!txtNPWP.match(/\S/)) {    	
	    	$('#txtNPWP').focus();
	    	$('#dvNPWP').addClass('has-error');
	    	$('#mrkNPWP').show();
	        return false;
	    }
	    if(!txtDomicile.match(/\S/)) {    	
	    	$('#txtDomicile').focus();
	    	$('#dvDomicile').addClass('has-error');
	    	$('#mrkDomicile').show();
	        return false;
	    }
	    if(!txtTDP.match(/\S/)) {    	
	    	$('#txtTDP').focus();
	    	$('#dvTDP').addClass('has-error');
	    	$('#mrkTDP').show();
	        return false;
	    } 
	    if(!slDistrict.match(/\S/)) {    	
	    	$('#slDistrict').focus();
	    	$('#dvDistrict').addClass('has-error');
	    	$('#mrkDistrict').show();
	        return false;
	    }    
	    if(!slTOP.match(/\S/)) {    	
	    	$('#slTOP').focus();
	    	$('#dvTOP').addClass('has-error');
	    	$('#mrkTOP').show();
	        return false;
	    }
	    if(!txtBillingAddress.match(/\S/)) {    	
	    	$('#txtBillingAddress').focus();
	    	$('#dvBillingAddress').addClass('has-error');
	    	$('#mrkBillingAddress').show();
	        return false;
	    } 
	    if(!txtCreditLimit.match(/\S/)) {    	
	    	$('#txtCreditLimit').focus();
	    	$('#dvCreditLimit').addClass('has-error');
	    	$('#mrkCreditLimit').show();
	        return false;
	    }
	    if(!txtBankName.match(/\S/)) {    	
	    	$('#txtBankName').focus();
	    	$('#dvBankName').addClass('has-error');
	    	$('#mrkBankName').show();
	        return false;
	    }
	    if(!txtBankAcc.match(/\S/)) {    	
	    	$('#txtBankAcc').focus();
	    	$('#dvBankAcc').addClass('has-error');
	    	$('#mrkBankAcc').show();
	        return false;
	    }
	    if(!txtBankBranch.match(/\S/)) {    	
	    	$('#txtBankBranch').focus();
	    	$('#dvBankBranch').addClass('has-error');
	    	$('#mrkBankBranch').show();
	        return false;
	    }
	    var p = {"key":lParambtn,"txtCustomerID":txtCustomerID,"txtCustomerName":txtCustomerName,"txtPIC1":txtPIC1,"txtPIC2":txtPIC2,
	        	"txtPIC3":txtPIC3,"txtAddress":txtAddress,"txtEmail":txtEmail,"txtOfficePhone":txtOfficePhone,"txtMobilePhone":txtMobilePhone,
	        	"txtNPWP":txtNPWP,"txtDomicile":txtDomicile,"txtTDP":txtTDP,"slDistrict":slDistrict,"txtBillingAddress":txtBillingAddress,"slTOP":slTOP,
	        	"txtCreditLimit":txtCreditLimit,"txtBankName":txtBankName,"txtBankAcc":txtBankAcc,"txtBankBranch":txtBankBranch,"txtPPN":txtPPN,"txtPPh23":txtPPh23,
	        	"txtCustomClearance":txtCustomClearance,"txtModelPricing":txtModelPricing}
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Customer',	
	        type:'POST',
	        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtCustomerName":txtCustomerName,"txtPIC1":txtPIC1,"txtPIC2":txtPIC2,
	        	"txtPIC3":txtPIC3,"txtAddress":txtAddress,"txtEmail":txtEmail,"txtOfficePhone":txtOfficePhone,"txtMobilePhone":txtMobilePhone,
	        	"txtNPWP":txtNPWP,"txtDomicile":txtDomicile,"txtTDP":txtTDP,"slDistrict":slDistrict,"txtBillingAddress":txtBillingAddress,"slTOP":slTOP,
	        	"txtCreditLimit":txtCreditLimit,"txtBankName":txtBankName,"txtBankAcc":txtBankAcc,"txtBankBranch":txtBankBranch,"txtPPN":txtPPN,"txtPPh23":txtPPh23,
	        	"txtCustomClearance":txtCustomClearance,"txtModelPricing":txtModelPricing},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	console.log(data)
	        	if(data.split("--")[0] == 'FailedInsertCustomer')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert customer failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtCustomerName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateCustomer')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update customer failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtCustomerName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/Customer';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function update customer (for role access : customer)
	function FuncSaveProfile() {
		var txtCustomerID = document.getElementById('txtID').value;
		var txtCustomerName = document.getElementById('txtName').value;
		var txtPIC1 = document.getElementById('pic1').value;
		var txtPIC2 = document.getElementById('pic2').value;
		var txtPIC3 = document.getElementById('pic3').value;
		var txtAddress = document.getElementById('address').value;
		var txtEmail = document.getElementById('email').value;
		var txtOfficePhone = document.getElementById('officePhone').value;
		var txtMobilePhone = document.getElementById('mobilePhone').value;
		var txtNPWP = document.getElementById('npwp').value;
		var txtTDP = document.getElementById('tdp').value;
		var txtPostalCode = document.getElementById('postalCode').value;
		var slTOP = document.getElementById('termOfPayment').value;
		var txtBillingAddress = document.getElementById('billingAddress').value;
		var txtProvince = document.getElementById('province').value;
		var txtCreditLimit = document.getElementById('creditLimit').value;
		var txtBankName = document.getElementById('bankName').value;
		var txtBankAcc = document.getElementById('bankAccount').value;
		var txtBankBranch = document.getElementById('bankBranch').value;
		
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Customer',	
	        type:'POST',
	        data:{"key":'update',"txtCustomerID":txtCustomerID,"txtCustomerName":txtCustomerName,"txtPIC1":txtPIC1,"txtPIC2":txtPIC2,
	        	"txtPIC3":txtPIC3,"txtAddress":txtAddress,"txtEmail":txtEmail,"txtOfficePhone":txtOfficePhone,"txtMobilePhone":txtMobilePhone,
	        	"txtNPWP":txtNPWP,"txtDomicile":txtProvince,"txtTDP":txtTDP,"slDistrict":txtPostalCode,"txtBillingAddress":txtBillingAddress,"slTOP":slTOP,
	        	"txtCreditLimit":txtCreditLimit,"txtBankName":txtBankName,"txtBankAcc":txtBankAcc,"txtBankBranch":txtBankBranch},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/Customer';  
		        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function save and update factory
	function FuncProccessFactory(lParambtn,lParamRole) {
		if(lParamRole == 'R001'){
			var txtCustomerID = document.getElementById('txtCustomerID').value;
		}
		else if(lParamRole == 'R002'){
			var txtCustomerID = document.getElementById('txtID').value;
		}
		
		var txtFactoryID = document.getElementById('txtFactoryID').value;
		var txtFactoryName = document.getElementById('txtFactoryName').value;
		var txtPIC = document.getElementById('txtFactoryPIC').value;
		var txtOfficePhone = document.getElementById('txtFactoryOfficePhone').value;
		var txtMobilePhone = document.getElementById('txtFactoryMobilePhone').value;
		var slProvince = document.getElementById('slFactoryProvince').value;
		var txtAddress = document.getElementById('txtFactoryAddress').value;
		var slDistrict = document.getElementById('slFactoryDistrict').value;
		//var slTier = document.getElementById('slFactoryTier').value;
		
	    if(!txtFactoryName.match(/\S/)) {
	    	$("#txtFactoryName").focus();
	    	$('#dvFactoryName').addClass('has-error');
	    	$('#mrkFactoryName').show();
	        return false;
	    } 
	    
	    if(!txtPIC.match(/\S/)) {
	    	$("#txtFactoryPIC").focus();
	    	$('#dvFactoryPIC').addClass('has-error');
	    	$('#mrkFactoryPIC').show();
	        return false;
	    } 
	    
	    if(!slProvince.match(/\S/)) {
	    	$('#slFactoryProvince').focus();
	    	$('#dvFactoryProvince').addClass('has-error');
	    	$('#mrkFactoryProvince').show();
	        return false;
	    }
	    
	    if(!txtAddress.match(/\S/)) {    	
	    	$('#txtFactoryAddress').focus();
	    	$('#dvFactoryAddress').addClass('has-error');
	    	$('#mrkFactoryAddress').show();
	        return false;
	    } 
	    
	    if(!slDistrict.match(/\S/)) {
	    	$('#slFactoryDistrict').focus();
	    	$('#dvFactoryDistrict').addClass('has-error');
	    	$('#mrkFactoryDistrict').show();
	        return false;
	    }
	    
		// 	    if(!slTier.match(/\S/)) {
		// 	    	$('#slFactoryTier').focus();
		// 	    	$('#dvFactoryTier').addClass('has-error');
		// 	    	$('#mrkFactoryTier').show();
		// 	        return false;
		// 	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Factory',	
	        type:'POST',
	        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtFactoryID":txtFactoryID,"txtFactoryName":txtFactoryName, "txtAddress":txtAddress, 
	        		"txtPIC":txtPIC,"txtOfficePhone":txtOfficePhone,"txtMobilePhone":txtMobilePhone,
	        		"slDistrict":slDistrict, "slTier":'', "slProvince":slProvince},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertFactory')
	        	{
	        		$("#dvErrorAlertFactory").show();
	        		document.getElementById("lblAlertFactory").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertFactoryDescription").innerHTML = data.split("--")[1];
	        		$("#txtFactoryName").focus();
	        		$("#ModalFactory").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateFactory')
	        	{
	        		$("#dvErrorAlertFactory").show();
	        		document.getElementById("lblAlertFactory").innerHTML = "Update failed";
	        		document.getElementById("lblAlertFactoryDescription").innerHTML = data.split("--")[1];
					$("#txtFactoryName").focus();
	        		$("#ModalFactory").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		$('#ModalFactory').modal('toggle');
	        		
	        		if(lParamRole == 'R001'){
	        			$('#dvloader').show();
	        			$('#dynamic-content-factory').html(''); // leave this div blank
	        			
	        			//load dynamic factory by customer
						//var customerid = document.getElementById('txtCustomerID').value;
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
					          type: 'POST',
					          data: 'customerid='+txtCustomerID,
					          dataType: 'html'
					     })
					     .done(function(data){
					          // console.log(data);
					          $('#dynamic-content-factory').html(''); // blank before load.
					          $('#dynamic-content-factory').html(data); // load here
					          $('#dvloader').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-factory').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					     	  $('#dvloader').hide();
					     });
	        		}
	        		else if(lParamRole == 'R002'){
	        			$('#dvloader2').show();
	        			$('#dynamic-content-factory2').html(''); // leave this div blank
	        			
	        			//load dynamic factory by customer
						//var customerid = document.getElementById('txtCustomerID').value;
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
					          type: 'POST',
					          data: 'customerid='+txtCustomerID,
					          dataType: 'html'
					     })
					     .done(function(data){
					          console.log(data);
					          $('#dynamic-content-factory2').html(''); // blank before load.
					          $('#dynamic-content-factory2').html(data); // load here
					          $('#dvloader2').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-factory2').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					     	  $('#dvloader2').hide();
					     });
	        		}
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function save and update customer rate management
	function FuncProccessRate(lParambtn,lParamRole) {
		if(lParamRole == 'R001'){
			var txtCustomerID = document.getElementById('txtCustomerID').value;
		}
		else if(lParamRole == 'R002'){
			var txtCustomerID = document.getElementById('txtID').value;
		}
		
		var txtFactoryID = document.getElementById('txtFactoryID2').value;
		var txtDistrictID = document.getElementById('txtDistrictID2').value;
		var slContainerType = document.getElementById('slContainerType').value;
		var txtFinalRate = document.getElementById('txtFinalRate').value;
	    
	    if(!slContainerType.match(/\S/)) {
	    	$("#slContainerType").focus();
	    	$('#dvContainerType').addClass('has-error');
	    	$('#mrkContainerType').show();
	        return false;
	    } 
	    
	    if(!txtFinalRate.match(/\S/)) {
	    	$("#txtFinalRate").focus();
	    	$('#dvFinalRate').addClass('has-error');
	    	$('#mrkFinalRate').show();
	        return false;
	    }
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Rate',	
	        type:'POST',
	        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtFactoryID":txtFactoryID,
	        	"txtFinalRate":txtFinalRate, "slContainerType":slContainerType},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertFinalRate')
	        	{
	        		$("#dvErrorAlertRateManagement").show();
	        		document.getElementById("lblAlertRateManagement").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertRateManagementDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsertRate").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateFinalRate')
	        	{
	        		$("#dvErrorAlertFinalRate").show();
	        		document.getElementById("lblAlertRateManagement").innerHTML = "Update failed";
	        		document.getElementById("lblAlertRateManagementDescription").innerHTML = data.split("--")[1];
	        		$("#ModalUpdateInsertRate").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		$('#ModalUpdateInsertRate').modal('toggle');
	        		
	        			$('#dvloaderRateManagement').show();
	        			$('#dynamic-content-rate-management').html(''); // leave this div blank
	     
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getCustomerRateManagement',
					          type: 'POST',
					          data: {"customerid":txtCustomerID, "factoryid":txtFactoryID, "districtid":txtDistrictID},
					          dataType: 'html'
					     })
					     .done(function(data){
					          // console.log(data);
					          $('#dynamic-content-rate-management').html(''); // blank before load.
					          $('#dynamic-content-rate-management').html(data); // load here
					          $('#dvloaderRateManagement').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-rate-management').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
	     	  				  $('#dvloaderRateManagement').hide();
					     });
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function delete customer
	function FuncDelete() {
		var temp_txtCustomerID = document.getElementById('temp_txtCustomerID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Customer',	
	        type:'POST',
	        data:{"key":"DeleteCustomer","temp_txtCustomerID":temp_txtCustomerID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/Customer';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function delete factory
	function FuncDeleteFactory(lParamRole) {
		if(lParamRole == 'R001'){
			var txtCustomerID = document.getElementById('txtCustomerID').value;
		}
		else if(lParamRole == 'R002'){
			var txtCustomerID = document.getElementById('txtID').value;
		}
		
		var txtFactoryID = document.getElementById('temp_txtFactoryID').value;	    
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/Customer',	
	        type:'POST',
	        data:{"key":"DeleteFactory","delCustomerID":txtCustomerID,"delFactoryID":txtFactoryID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(lParamRole == 'R001'){
	        		if(data.split("--")[0] == 'FailedDeleteFactory')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Delete Factory Failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
		        		$('#dvloader').show();
		        		$('#ModalDeleteFactory').modal('toggle');
			        	//load dynamic factory by customer
						//var customerid = txtCustomerID;
					     $('#dynamic-content-factory').html(''); // leave this div blank
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
					          type: 'POST',
					          data: 'customerid='+txtCustomerID,
					          dataType: 'html'
					     })
					     .done(function(data){
					          // console.log(data);
					          $('#dynamic-content-factory').html(''); // blank before load.
					          $('#dynamic-content-factory').html(data); // load here
					          $('#dvloader').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-factory').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					     	  $('#dvloader').hide();
					     });
		        	}
	        	}
	        	else if(lParamRole == 'R002'){
	        		if(data.split("--")[0] == 'FailedDeleteFactory')
		        	{
		        		var url = '${pageContext.request.contextPath}/Customer';  
		        		$(location).attr('href', url);
		        	}
		        	else
		        	{
		        		$('#dvloader2').show();
		        		$('#ModalDeleteFactory').modal('toggle');
			        	//load dynamic factory by customer
						//var customerid = txtCustomerID;
					     $('#dynamic-content-factory2').html(''); // leave this div blank
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getFactoryForCustomer',
					          type: 'POST',
					          data: 'customerid='+txtCustomerID,
					          dataType: 'html'
					     })
					     .done(function(data){
					          // console.log(data);
					          $('#dynamic-content-factory2').html(''); // blank before load.
					          $('#dynamic-content-factory2').html(data); // load here
					          $('#dvloader2').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-factory2').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					     	  $('#dvloader2').hide();
					     });
		        	}
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");
var inputFactory = document.getElementById("ModalFactory");

input.addEventListener("keyup", function(event) {
  var customerid = document.getElementById('txtCustomerID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (customerid == null || customerid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (customerid != null || customerid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});

inputFactory.addEventListener("keyup", function(event) {
  var factoryid = document.getElementById('txtFactoryID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (factoryid == null || factoryid == '')) {
    document.getElementById("btnSaveFactory").click();
  }
  else if (event.keyCode === 13 && (factoryid != null || factoryid != '')) {
  	document.getElementById("btnUpdateFactory").click();
  }
});


// custom invoice customer
function validateModalPricing() {
	var resultModalPricing = false;
	var valueModalPriceing = getArrModalPricing().toString();
    var packetA = ["Trucking","Custom Clearance","Reimburse"].toString();
    var packetB = ["Trucking","Custom Clearance"].toString();
    var packetC = ["Trucking"].toString();
    
    if(valueModalPriceing == packetA){
    	resultModalPricing = true
    }else if(valueModalPriceing == packetB){
    	resultModalPricing = true
    }else if(valueModalPriceing == packetC) {
    	resultModalPricing = true
    }
    
    return resultModalPricing;
}

var getArrModalPricing = function() {
	var arrModalPricing = [];
	$.each($("input[name='modelPricing']:checked"), function(){            
    	arrModalPricing.push($(this).val());
    });
	  return arrModalPricing;
};


var isEqual = function (value, other) {

	// Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var compare = function (item1, item2) {

		// Get the object type
		var itemType = Object.prototype.toString.call(item1);

		// If an object or array, compare recursively
		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
			if (!isEqual(item1, item2)) return false;
		}

		// Otherwise, do a simple comparison
		else {

			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) return false;

			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === '[object Function]') {
				if (item1.toString() !== item2.toString()) return false;
			} else {
				if (item1 !== item2) return false;
			}

		}
	};

	// Compare properties
	if (type === '[object Array]') {
		for (var i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) return false;
		}
	} else {
		for (var key in value) {
			if (value.hasOwnProperty(key)) {
				if (compare(value[key], other[key]) === false) return false;
			}
		}
	}

	// If nothing failed, return true
	return true;

};


//function save and update customer on behalf npwp
function FuncProccessOnbehalf(lParambtn) {
	
	var txtCustomerID = document.getElementById('txtTempCustomerID').value;
	var txtOnbehalfName = document.getElementById('txtOnbehalfName').value;
	var txtOnbehalfNpwp = document.getElementById('txtOnbehalfNpwp').value;
	var txtTempNpwp = document.getElementById('txtTempNpwp').value;
    
    if(!txtOnbehalfName.match(/\S/)) {
    	$("#txtOnbehalfName").focus();
    	$('#dvOnbehalfName').addClass('has-error');
    	$('#mrkOnbehalfName').show();
        return false;
    } 
    
    if(!txtOnbehalfNpwp.match(/\S/)) {
    	$("#txtOnbehalfNpwp").focus();
    	$('#dvOnbehalfNpwp').addClass('has-error');
    	$('#mrkOnbehalfNpwp').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/CustomerOnbehalfNpwp',	
        type:'POST',
        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtOnbehalfName":txtOnbehalfName,
        	"txtOnbehalfNpwp":txtOnbehalfNpwp,"txtTempNpwp":txtTempNpwp},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedInsertNpwp')
        	{
        		$("#dvErrorAlertAddNpwp").show();
        		document.getElementById("lblAlertAddNpwp").innerHTML = "Insert failed";
        		document.getElementById("lblAlertAddNpwpDescription").innerHTML = data.split("--")[1];
        		$("#ModalAddNpwp").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateNpwp')
        	{
        		$("#dvErrorAlertAddNpwp").show();
        		document.getElementById("lblAlertAddNpwp").innerHTML = "Update failed";
        		document.getElementById("lblAlertAddNpwpDescription").innerHTML = data.split("--")[1];
        		$("#ModalAddNpwp").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
        		$('#ModalAddNpwp').modal('toggle');
        		
        			$('#dvloaderOnbehalfNpwp').show();
        			$('#dynamic-content-onbehalf-npwp').html(''); // leave this div blank
     
				     $.ajax({
				          url: '${pageContext.request.contextPath}/getOnbehalfNpwp',
				          type: 'POST',
				          data: {"customerid":txtCustomerID},
				          dataType: 'html'
				     })
				     .done(function(data){
				          // console.log(data);
				          $('#dynamic-content-onbehalf-npwp').html(''); // blank before load.
				          $('#dynamic-content-onbehalf-npwp').html(data); // load here
				          $('#dvloaderOnbehalfNpwp').hide();
				     })
				     .fail(function(){
				          $('#dynamic-content-onbehalf-npwp').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
     	  				  $('#dvloaderOnbehalfNpwp').hide();
				     });
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}
</script>

</body>
</html>