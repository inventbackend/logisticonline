<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master User</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <style>	
  #ModalUpdateInsert { overflow-y:scroll }
  
/*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  margin-left: 45%;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */
</style>

</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_User" action = "${pageContext.request.contextPath}/User" method="post">
<input type="hidden" id="tempRole" value="<c:out value="${userRole}"/>" />
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master User
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertUser'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Insert success.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateUser'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Update success.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteUser'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Delete success.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteUser'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Delete failed. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
	      		<!-- loading bar -->
				<div id="dvloader" class="loader" style="display:none;"></div>
				
				<div class="row">
			          <div id="dvRole" class="form-group col-md-12">
			          	<label for="lblRole" class="control-label">Role:</label><label id="mrkRole" for="lbl-validation" class="control-label"><small>*</small></label>
				 		<select id="slRole" name="slRole" class="form-control select2" style="width: 100%;" <c:out value="${inputStatus}"/>>
							<c:forEach items="${listRole}" var="role">
								<option value="<c:out value="${role.roleID}" />"><c:out value="${role.roleID}" /> - <c:out value="${role.roleName}" /></option>
							</c:forEach>
				       	</select>
				      	</div>
			          <div id="dvUserID" class="form-group col-md-12">
			            <label for="lblUserID" class="control-label">User ID:</label><label id="mrkUserID" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtUserID" name="txtUserID">
			          </div>
			          <div id="dvUserName" class="form-group col-md-4">
			            <label for="lblUserName" class="control-label">User Name:</label><label id="mrkUserName" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="text" class="form-control" id="txtUserName" name="txtUserName">
			          </div>
			          <div id="dvPassword" class="form-group col-md-4">
			            <label for="lblPassword" class="control-label">Password:</label><label id="mrkPassword" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="password" class="form-control" id="txtPassword" name="txtPassword">
			          </div>
			         <div id="dvRePassword" class="form-group col-md-4">
			            <label for="lblRePassword" class="control-label">Re-type Password:</label><label id="mrkRePassword" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="password" class="form-control" id="txtRePassword" name="txtRePassword">
			          </div>
			          <div id="dvIsActive" class="form-group col-md-12">
			            <label for="lblIsActive" class="control-label">Status:</label><label id="mrkIsActive" for="lbl-validation" class="control-label"><small>*</small></label>
			            <select id="slIsActive" name="slIsActive" class="form-control" <c:out value="${inputStatus}"/>>
			            <option value="0">Account Not Verified</option>
			            <option value="1">Verify Account</option>
			            <option value="2">Block Account</option>
	                    </select>
			          </div>
		          </div>
		          
		          	  <!--  Staff Data Panel -->
		          	  <br>
					  <div id="dvStaffPanel">
						  <h4 class="modal-title"><label>Staff</label></h4>	
						  <button style="display: <c:out value="${buttonEditStatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalStaff"><i class="fa fa-plus-circle"></i> Add Staff</button><br><br>		 
				      	  <div id="dvloaderStaff" class="loader"></div>
				      	  <div id="dynamic-content-staff">
				      	  </div>
			      	  </div>
		      </div>
		      <div class="modal-footer">
	      			<button style="display: <c:out value="${buttonEditStatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
					<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
			      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		      
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		
							<!--modal show user data -->
							<div class="modal fade" id="ModalGetUserByRole" tabindex="-1" role="dialog" aria-labelledby="ModalLabelUserID">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
 											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 											<h4 class="modal-title" id="ModalLabelUserID"></h4>	
										</div>
	     								<div class="modal-body">
	     									<div id="dynamic-content-user"></div>
	    								</div>
									</div>
								</div>
							</div>
							<!-- /. end of modal show user data -->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete User</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtUserID" name="temp_txtUserID"  />
									<p>Are you sure to delete this user ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
		
		<!--modal Delete Staff -->
		<div class="modal modal-danger" id="ModalDeleteStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Staff</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtStaff" name="temp_txtStaff"  />
									<p>Are you sure to delete this staff ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDeleteStaff" name="btnDeleteStaff"  class="btn btn-outline" onclick="FuncDeleteStaff()">Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
		
		<!--modal staff -->
		<div class="modal fade" id="ModalStaff" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			
			<div id="dvErrorAlertStaff" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				<label id="lblAlertStaff"></label>. <label id="lblAlertStaffDescription"></label>.
			</div>
				
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalStaff" name="lblTitleModalStaff"></label></h4>	
			</div>
		    <div class="modal-body">
				<div id="dvStaff" class="form-group col-md-12">
					<label for="recipient-name" class="control-label">Staff</label><label id="mrkStaff" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtStaff" name="txtStaff">
					<input type="hidden" class="form-control" id="txtHiddenStaff" name="txtHiddenStaff">
				</div>
				<div id="dvStaffUserName" class="form-group col-md-4">
					<label for="recipient-name" class="control-label">Username</label><label id="mrkStaffUserName" for="recipient-name" class="control-label"><small>*</small></label>	
					<input type="text" class="form-control" id="txtStaffUserName" name="txtStaffUserName">
				</div>
				<div id="dvStaffPassword" class="form-group col-md-4">
			            <label for="lblPassword" class="control-label">Password:</label><label id="mrkStaffPassword" for="lbl-validation" class="control-label"><small>*</small></label>
			            <input type="password" class="form-control" id="txtStaffPassword" name="txtStaffPassword">
			          </div>
		         <div id="dvStaffRePassword" class="form-group col-md-4">
		            <label for="lblRePassword" class="control-label">Re-type Password:</label><label id="mrkStaffRePassword" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="password" class="form-control" id="txtStaffRePassword" name="txtStaffRePassword">
		          </div>
		          <div id="dvStaffIsActive" class="form-group col-md-12" style="display:none;">
		            <label for="lblIsActive" class="control-label">Status:</label><label id="mrkStaffIsActive" for="lbl-validation" class="control-label"><small>*</small></label>
		            <select id="slStaffIsActive" name="slStaffIsActive" class="form-control">
		            <option value="1">Enabled</option>
	                   </select>
		          </div>
  				<div class="row"></div>
			</div>
			
			<div class="modal-footer">
				<button style="display: <c:out value="${buttonEditStatus}"/>" type="button" class="btn btn-primary" id="btnSaveStaff" name="btnSaveStaff" onclick="FuncProccessStaff('saveStaff')"><i class="fa fa-check"></i> Save</button>
				<button style="display: <c:out value="${buttonEditStatus}"/>" type="button" class="btn btn-primary" id="btnUpdateStaff" name="btnUpdateStaff" onclick="FuncProccessStaff('updateStaff')"><i class="fa fa-edit"></i> Update</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			</div>
			</div>
			</div>
		</div>
        
  		<!-- modal assign staff role -->
		<div class="modal fade bs-example-modal-lg"
			id="ModalRoleStaff" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="exampleModalLabel">
							<label id="lblTitleModalRoleStaff"
								name="lblTitleModalRoleStaff">Staff Role Management</label>
						</h4>
					</div>
					<div class="modal-body">
						<div id="dvloaderRoleStaff" class="loader"></div>
						
						<div class="row">
							<div class="col-sm-4">
								<input type="hidden" class="form-control" id="txtRoleId">
							</div>

							<div class="col-md-12">
								<!-- <hr> -->
							</div>


							<div class="col-md-3">
								<div class="input-group">
									<label for="lblMenu" class="control-label">Menu :</label>
									<select multiple id="slMenu" name="slMenu"
										class="form-control" style="height: 350px">
									</select>
								</div>
							</div>

							<div class="col-md-3">
								<div class="input-group" style="width: 150px">
									<br> <br>
									<button type="button" class="btn btn-block btn-default"
										onclick="FuncAddMenu()">Add ></button>
									<button type="button" class="btn btn-block btn-default"
										onclick="FuncAddAllMenu()">Add All >></button>
									<button type="button" class="btn btn-block btn-default"
										onclick="FuncRemoveMenu()">< Remove</button>
									<button type="button" class="btn btn-block btn-default"
										onclick="FuncRemoveAllMenu()"><< Remove All</button>
								</div>
							</div>

							<div class="col-md-3">
								<div class="input-group">
									<label for="lblAllowedMenu" class="control-label">Allowed
										Menu :</label> <select multiple id="slAllowedMenu"
										name="slAllowedMenu" class="form-control"
										style="height: 350px">
									</select>
								</div>
							</div>

							<div class="col-md-3">
								<div class="input-group">
									<div id="dynamic-content-editable-menu"></div>

									<div class="row">
										<div class="col-md-6" style="weight: 200px">
											<button type="button" class="btn btn-block btn-default"
												onclick="FuncSelectAll()">Select All</button>
										</div>
										<div class="col-md-6" style="margin-left: -20px">
											<button type="button" class="btn btn-block btn-default"
												style="width: 100px" onclick="FuncUnselectAll()">Unselect
												All</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					
					<div class="modal-footer">
						<button style="display: <c:out value="${buttonEditStatus}"/>" type="button"
									id="btnSaveRole" name="btnSaveRole" class="btn btn-primary"
									onclick="FuncSaveRole()"><i class="fa fa-check"></i> Save</button>	
					</div>
					
				</div>
			</div>
		</div>
		<!-- /.end modal assign staff role -->

        <table id="tb_master_user" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>Username</th>
	                  <th>Role</th>
	                  <th>Status</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listUser}" var ="user">
		        	<tr>
		        	<td><c:out value="${user.username}"/></td>
					<td><c:out value="${user.roleName}"/></td>
					<td>
						<c:if test="${user.isActive == 0}">Not verified</c:if>
						<c:if test="${user.isActive == 1}">Verified</c:if>
						<c:if test="${user.isActive == 2}">Blocked</c:if>
					</td>
					<td>
						<button style="display: <c:out value="${buttonEditStatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								title="edit"
								onclick="FuncButtonUpdate('<c:out value="${user.isActive}"/>','<c:out value="${userRole}"/>')" 
								data-target="#ModalUpdateInsert"
								data-lroleid='<c:out value="${user.role}"/>'
								data-luserid='<c:out value="${user.userId}"/>'
								data-lusername='<c:out value="${user.username}"/>'
								data-lpassword='<c:out value="${user.password}"/>'
								data-lisactive='<c:out value="${user.isActive}"/>' 
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-luserid='<c:out value="${user.userId}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
  	//Initialize Select2 Elements
	$(".select2").select2();
	
    $('#tb_master_user').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
    $('#M009').addClass('active');
	
	var userRole = document.getElementById('tempRole').value;
	if(userRole == 'R001'){
		//shortcut for button 'new'
    	Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
    	});
	}
	
	$("#dvErrorAlert").hide();
	$('#dvloader').hide();
	$('#dvloaderStaff').hide();
	$('#dvloaderRoleStaff').hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lUserID = button.data('luserid');
		$("#temp_txtUserID").val(lUserID);
	})
	
	$('#ModalDeleteStaff').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lStaff = button.data('lstaff');
		$("#temp_txtStaff").val(lStaff);
	})
	</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var lIsActive = button.data('lisactive');
	  
	  var modal = $(this)
	  modal.find('.modal-body #slRole').val(button.data('lroleid')).trigger('change')
	  modal.find('.modal-body #txtUserID').val(button.data('luserid'))
	  modal.find('.modal-body #txtUserName').val(button.data('lusername'))
	  if(button.data('lpassword')==null || button.data('lpassword')==''){
	  	modal.find('.modal-body #txtPassword').val(button.data('lpassword'))
	  	modal.find('.modal-body #txtRePassword').val(button.data('lpassword'))
	  }
	  else{
	  	modal.find('.modal-body #txtPassword').val(atob(unescape(encodeURIComponent(button.data('lpassword')))))
	  	modal.find('.modal-body #txtRePassword').val(atob(unescape(encodeURIComponent(button.data('lpassword')))))
	  }
	  modal.find('.modal-body #slIsActive').val(lIsActive)
	  
	  	//if update, get the staff panel
		  if(button.data('luserid')!=null || button.data('luserid')!=''){
		  		$('#dvloaderStaff').show();
			  	//load dynamic staff by user
				 var userid = document.getElementById('txtUserID').value;
			     $('#dynamic-content-staff').html(''); // leave this div blank
			     $.ajax({
			          url: '${pageContext.request.contextPath}/getStaffForUser',
			          type: 'POST',
			          data: 'userid='+userid,
			          dataType: 'html'
			     })
			     .done(function(data){
			          console.log(data); 
			          $('#dynamic-content-staff').html(''); // blank before load.
			          $('#dynamic-content-staff').html(data); // load here
			          $('#dvloaderStaff').hide();
			     })
			     .fail(function(){
			          $('#dynamic-content-staff').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			     	  $('#dvloaderStaff').hide();
			     });
		  }
	  
	//$("#slRole").focus();
	})
</script>

<!-- General Script -->
<script>

	function FuncClear(){
		$('#mrkRole').hide();
		$('#mrkUserID').hide();
		$('#mrkUserName').hide();
		$('#mrkPassword').hide();
		$('#mrkRePassword').hide();
		$('#mrkIsActive').hide();
		
		$('#mrkStaff').hide();
		$('#mrkStaffUserName').hide();
		$('#mrkStaffPassword').hide();
		$('#mrkStaffRePassword').hide();
		$('#mrkStaffIsActive').hide();
		
		$('#dvRole').removeClass('has-error');
		$('#dvUserID').removeClass('has-error');
		$('#dvUserName').removeClass('has-error');
		$('#dvPassword').removeClass('has-error');
		$('#dvRePassword').removeClass('has-error');
		$('#dvIsActive').removeClass('has-error');
		
		$('#dvStaff').removeClass('has-error');
		$('#dvStaffUserName').removeClass('has-error');
		$('#dvStaffPassword').removeClass('has-error');
		$('#dvStaffRePassword').removeClass('has-error');
		$('#dvStaffIsActive').removeClass('has-error');
		
		$("#dvErrorAlert").hide();
		$("#dvErrorAlertStaff").hide();
	}
	
	function FuncButtonNew() {
		$('#txtUserID').prop('disabled', false);
		$('#txtUserName').prop('disabled', false);
		$('#slIsActive').val('');
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add User";
		$("#dvStaffPanel").hide();	
	
		FuncClear();
	}
	
	function FuncButtonUpdate(lUserStatus, lUserRole) {
	// 		$('#slIsActive').find('option').remove();
	// 		if(lUserRole == 'R001'){
	// 			if(lUserStatus == '0'){
	// 				$('#slIsActive').append("<option value='1'>Verify Account</option>");
	// 			}
	// 			else if(lUserStatus == '1'){
	// 				$('#slIsActive').append("<option value='2'>Block</option>");
	// 			}
	// 			else if(lUserStatus == '2'){
	// 				$('#slIsActive').append("<option value='1'>Unblock</option>");
	// 			}
	// 		}
	// 		else{
	// 			if(lUserStatus == '1'){
	// 				$('#slIsActive').append("<option value='1'>Verified</option>");
	// 			}
	// 		}
		
	
		$('#txtUserID').prop('disabled', true);
		$('#txtUserName').prop('disabled', true);
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit User';
		$("#dvStaffPanel").show();
	
		FuncClear();
	}
	
	function FuncValEmptyInput(lParambtn) {
		var slRole = document.getElementById('slRole').value;
		var txtUserID = document.getElementById('txtUserID').value;
		var txtUserName = document.getElementById('txtUserName').value;
		var txtPassword = document.getElementById('txtPassword').value;
		var txtRePassword = document.getElementById('txtRePassword').value;
		var slIsActive = document.getElementById('slIsActive').value;
		
	    if(!slRole.match(/\S/)) {    	
	    	$('#slRole').focus();
	    	$('#dvRole').addClass('has-error');
	    	$('#mrkRole').show();
	        return false;
	    } 
	    if(!txtUserID.match(/\S/)) {    	
	    	$('#txtUserID').focus();
	    	$('#dvUserID').addClass('has-error');
	    	$('#mrkUserID').show();
	        return false;
	    }
	    if(txtUserID.includes("_")) {    	
	    	$('#txtUserID').focus();
	    	$('#dvUserID').addClass('has-error');
	    	alert("User ID may not contain '_'");
	        return false;
	    }
	    if(!txtUserName.match(/\S/)) {    	
	    	$('#txtUserName').focus();
	    	$('#dvUserName').addClass('has-error');
	    	$('#mrkUserName').show();
	        return false;
	    }
	    if(!txtPassword.match(/\S/)) {    	
	    	$('#txtPassword').focus();
	    	$('#dvPassword').addClass('has-error');
	    	$('#mrkPassword').show();
	        return false;
	    }
	    if(txtPassword != txtRePassword){
    		$('#txtPassword').focus();
    		$('#dvPassword').addClass('has-error');
    		$('#mrkPassword').show();
    		$('#dvRePassword').addClass('has-error');
    		$('#mrkRePassword').show();
    		
    		alert("password not match");
    		return false;
    	}
	    if(!slIsActive.match(/\S/)) {    	
	    	$('#slIsActive').focus();
	    	$('#dvIsActive').addClass('has-error');
	    	$('#mrkIsActive').show();
	        return false;
	    }      
	    
	    $('#dvloader').show();
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/User',	
	        type:'POST',
	        data:{"key":lParambtn,"slRole":slRole,"txtUserID":txtUserID,"txtUserName":txtUserName,"txtPassword":txtPassword,"txtRePassword":txtRePassword,"slIsActive":slIsActive},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertUser')
	        	{
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#slRole").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateUser')
	        	{
	        		$('#dvloader').hide();
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#slRole").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/User';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	            $('#dvloader').hide();
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
// automaticaly open the select2 when it gets focus
// jQuery(document).on('focus', '.select2', function() {
//     jQuery(this).siblings('select').select2('open');
// });

jQuery(document).ready(function() {
    //when the select2 closes advance focus to the next field
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
    
    //GET USER FROM ROLE
    $(document).on('click', '#txtUserID', function(e){
    	e.preventDefault();
		var roleid = document.getElementById('slRole').value;
		if(roleid==null || roleid==''){  	
	    	$('#slRole').focus();
	    	$('#dvRole').addClass('has-error');
	    	$('#mrkRole').show();
	    	
	    	alert("Fill Role First ...!!!");
		}
		else if(roleid!='R001'){
			 $('#ModalGetUserByRole').modal('toggle');
			 $('#dynamic-content-user').html(''); // leave this div blank
		 
		     $.ajax({
		          url: '${pageContext.request.contextPath}/getUser',
		          type: 'POST',
		          data: 'roleid='+roleid,
		          dataType: 'html'
		     })
		     .done(function(data){
		          console.log(data); 
		          $('#dynamic-content-user').html(''); // blank before load.
		          $('#dynamic-content-user').html(data); // load here
		     })
		     .fail(function(){
		          $('#dynamic-content-user').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
		     });
		}
	 });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

function FuncPassStringUser(lParamUserID,lParamUserName){
	$("#txtUserID").val(lParamUserID);
}

	//Modal Staff
	$('#ModalStaff').on('shown.bs.modal', function (event) {
		FuncClear();
	
 		var button = $(event.relatedTarget);
 		var lStaff = button.data('lstaff');
 		var lUsername = button.data('lusername');
 		var lStatus = button.data('lstatus');
 		var lPassword = button.data('lpassword');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtStaff").val(lStaff);
 		modal.find(".modal-body #txtHiddenStaff").val(lStaff);
		modal.find(".modal-body #txtStaffUserName").val(lUsername);
		modal.find(".modal-body #slStaffIsActive").val(lStatus);
		
		if(lPassword==null || lPassword==''){
		  	modal.find('.modal-body #txtStaffPassword').val(lPassword)
		  	modal.find('.modal-body #txtStaffRePassword').val(lPassword)
		}
	  else{
		  	modal.find('.modal-body #txtStaffPassword').val(atob(unescape(encodeURIComponent(lPassword))))
		  	modal.find('.modal-body #txtStaffRePassword').val(atob(unescape(encodeURIComponent(lPassword))))
		  }
 		
 		$('#txtStaff').focus();
 		
 		if(lStaff == undefined){
 			$('#btnSaveStaff').show();
			$('#btnUpdateStaff').hide();
			$('#slStaffIsActive').val(1);
			document.getElementById("lblTitleModalStaff").innerHTML = 'Add Staff';
 		}
 		else{
 			$('#btnSaveStaff').hide();
			$('#btnUpdateStaff').show();
			document.getElementById("lblTitleModalStaff").innerHTML = 'Edit Staff';
 		}
	})
	
	//function save and update staff
	function FuncProccessStaff(lParambtn) {
		var txtUserID = document.getElementById('txtUserID').value;
		var txtStaff = document.getElementById('txtStaff').value;
		var txtHiddenStaff = document.getElementById('txtHiddenStaff').value;
		var txtUserName = document.getElementById('txtStaffUserName').value;
		var txtPassword = document.getElementById('txtStaffPassword').value;
		var txtRePassword = document.getElementById('txtStaffRePassword').value;
		var slIsActive = document.getElementById('slStaffIsActive').value;
	    
	    if(!txtStaff.match(/\S/)) {
	    	$("#txtStaff").focus();
	    	$('#dvStaff').addClass('has-error');
	    	$('#mrkStaff').show();
	        return false;
	    } 
	    
	    if(!txtUserName.match(/\S/)) {
	    	$("#txtStaffUserName").focus();
	    	$('#dvStaffUserName').addClass('has-error');
	    	$('#mrkStaffUserName').show();
	        return false;
	    } 
	    
	    if(!txtPassword.match(/\S/)) {    	
	    	$('#txtStaffPassword').focus();
	    	$('#dvStaffPassword').addClass('has-error');
	    	$('#mrkStaffPassword').show();
	        return false;
	    }
	    if(txtPassword != txtRePassword){
    		$('#txtStaffPassword').focus();
    		$('#dvStaffPassword').addClass('has-error');
    		$('#mrkStaffPassword').show();
    		$('#dvStaffRePassword').addClass('has-error');
    		$('#mrkStaffRePassword').show();
    		
    		alert("password not match");
    		return false;
    	}
    	
    	if(!slIsActive.match(/\S/)) {
	    	$("#slStaffIsActive").focus();
	    	$('#dvStaffIsActive').addClass('has-error');
	    	$('#mrkStaffIsActive').show();
	        return false;
	    } 
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/User',	
	        type:'POST',
	        data:{"key":lParambtn,"txtStaff":txtStaff,"txtHiddenStaff":txtHiddenStaff,"txtUserID":txtUserID, "txtUserName":txtUserName, 
	        		"txtPassword":txtPassword,"slIsActive":slIsActive},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertStaff')
	        	{
	        		$("#dvErrorAlertStaff").show();
	        		document.getElementById("lblAlertStaff").innerHTML = "Insert failed";
	        		document.getElementById("lblAlertStaffDescription").innerHTML = data.split("--")[1];
	        		$("#txtStaff").focus();
	        		$("#ModalStaff").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateStaff')
	        	{
	        		$("#dvErrorAlertStaff").show();
	        		document.getElementById("lblAlertStaff").innerHTML = "Update failed";
	        		document.getElementById("lblAlertStaffDescription").innerHTML = data.split("--")[1];
					$("#txtStaff").focus();
	        		$("#ModalStaff").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedAuthorize')
	        	{
	        		$("#dvErrorAlertStaff").show();
	        		document.getElementById("lblAlertStaff").innerHTML = "Process failed";
	        		document.getElementById("lblAlertStaffDescription").innerHTML = data.split("--")[1];
					$("#txtStaff").focus();
	        		$("#ModalStaff").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		$('#ModalStaff').modal('toggle');
	        		
        			$('#dvloaderStaff').show();
        			$('#dynamic-content-staff').html(''); // leave this div blank
        			
        			//load dynamic staff by user
				     $.ajax({
				          url: '${pageContext.request.contextPath}/getStaffForUser',
				          type: 'POST',
				          data: 'userid='+txtUserID,
				          dataType: 'html'
				     })
				     .done(function(data){
				          console.log(data);
				          $('#dynamic-content-staff').html(''); // blank before load.
				          $('#dynamic-content-staff').html(data); // load here
				          $('#dvloaderStaff').hide();
				     })
				     .fail(function(){
				          $('#dynamic-content-staff').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				     	  $('#dvloaderStaff').hide();
				     });
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//function delete staff
	function FuncDeleteStaff() {
		var txtUserID = document.getElementById('txtUserID').value;
		var txtStaff = document.getElementById('temp_txtStaff').value;	    
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/User',	
	        type:'POST',
	        data:{"key":"deleteStaff","txtUserID":txtUserID,"txtStaff":txtStaff,"slIsActive":'none'},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        		if(data.split("--")[0] == 'FailedDeleteStaff')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Delete Staff Failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateStaff')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Process failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
		        		return false;
		        	}
		        	else
		        	{
		        		$('#dvloaderStaff').show();
		        		$('#ModalDeleteStaff').modal('toggle');
			        	//load dynamic factory by customer
						//var customerid = txtCustomerID;
					     $('#dynamic-content-staff').html(''); // leave this div blank
					     $.ajax({
					          url: '${pageContext.request.contextPath}/getStaffForUser',
					          type: 'POST',
					          data: 'userid='+txtUserID,
					          dataType: 'html'
					     })
					     .done(function(data){
					          console.log(data);
					          $('#dynamic-content-staff').html(''); // blank before load.
					          $('#dynamic-content-staff').html(data); // load here
					          $('#dvloaderStaff').hide();
					     })
					     .fail(function(){
					          $('#dynamic-content-staff').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
					     	  $('#dvloaderStaff').hide();
					     });
		        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
function FuncAddMenu() {
	var MenuID = document.getElementById('slMenu').value;
	var Command = "Add";

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command
		},
		dataType : 'json'
	})
			.done(
					function(data) {
						console.log(data);
						for ( var i in data) {
							var obj = data[i];
							var index = 0;
							var id, name, url, type, level;
							for ( var prop in obj) {
								switch (index++) {
								case 0:
									id = obj[prop];
									break;
								case 1:
									name = obj[prop];
									break;
								case 2:
									url = obj[prop];
									break;
								case 3:
									type = obj[prop];
									break;
								case 4:
									level = obj[prop];
									break;
								default:
									break;
								}
							}

							if ($("#slAllowedMenu option[value=" + id
									+ "]").length > 0) {
							} else {
								if (level == '0')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">"
													+ name
													+ "</option>");
								if (level == '1')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
													+ name
													+ "</option>");
								if (level == '2')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp"
													+ name
													+ "</option>");
							}

						}

					}).fail(function() {
				console.log('Service call failed!');
			})
}

function FuncAddAllMenu() {

	var MenuID = "AllStaffMenu";
	var Command = "Add";
	var UserID = document.getElementById("txtUserID").value;

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command,
			userid : UserID
		},
		dataType : 'json'
	})
			.done(
					function(data) {
						console.log(data);
						for ( var i in data) {
							var obj = data[i];
							var index = 0;
							var id, name, url, type, level;
							for ( var prop in obj) {
								switch (index++) {
								case 0:
									id = obj[prop];
									break;
								case 1:
									name = obj[prop];
									break;
								case 2:
									url = obj[prop];
									break;
								case 3:
									type = obj[prop];
									break;
								case 4:
									level = obj[prop];
									break;
								default:
									break;
								}
							}

							if ($("#slAllowedMenu option[value=" + id
									+ "]").length > 0) {

							} else {
								if (level == '0')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">"
													+ name
													+ "</option>");
								if (level == '1')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
													+ name
													+ "</option>");
								if (level == '2')
									$('#slAllowedMenu').append(
											"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp"
													+ name
													+ "</option>");
							}

						}

					}).fail(function() {

			})
}

function FuncRemoveMenu() {
	var MenuID = document.getElementById('slAllowedMenu').value;
	var Command = "Remove";

	$.ajax({
		url : '${pageContext.request.contextPath}/getMenu',
		type : 'POST',
		data : {
			menuid : MenuID,
			command : Command
		},
		dataType : 'json'
	}).done(function(data) {
		console.log(data);
		for ( var i in data) {
			var obj = data[i];
			var index = 0;
			var id, name, url, type, level;
			for ( var prop in obj) {
				switch (index++) {
				case 0:
					id = obj[prop];
					break;
				case 1:
					name = obj[prop];
					break;
				case 2:
					url = obj[prop];
					break;
				case 3:
					type = obj[prop];
					break;
				case 4:
					level = obj[prop];
					break;
				default:
					break;
				}
			}

			$("#slAllowedMenu option[value=" + id + "]").remove();
		}

	}).fail(function() {
		console.log('Service call failed!');
	})
}

function FuncRemoveAllMenu() {
	$('#slAllowedMenu option').prop('selected', true);
	$("#slAllowedMenu option:selected").remove();
}

function FuncSelectAll() {
	$("#EditableMenuPanel :input").prop("checked", true);
}

function FuncUnselectAll() {
	$("#EditableMenuPanel :input").prop("checked", false);
}

$('#ModalRoleStaff').on(
		'shown.bs.modal',
		function(event) {
			$('#dvloaderRoleStaff').show();

			var button = $(event.relatedTarget);
			var lRoleID = button.data('lroleid');
			var lUserID = document.getElementById('txtUserID').value;
			var modal = $(this);
			
			//Clear list
			$('#slMenu option').prop('selected', true);
			$("#slMenu option:selected").remove();
			$('#slAllowedMenu option').prop('selected', true);
			$("#slAllowedMenu option:selected").remove();

			modal.find(".modal-body #txtRoleId").val(lRoleID);
			
			//Load Menu for Staff
			$.ajax({
				url : '${pageContext.request.contextPath}/getMenu',
				type : 'POST',
				data : {
					menuid : 'AllStaffMenu',
					command : 'Add',
					userid : lUserID
				},
				dataType : 'json'
			})
					.done(
							function(data) {
								console.log(data);
								for ( var i in data) {
									var obj = data[i];
									var index = 0;
									var id, name, url, type, level;
									for ( var prop in obj) {
										switch (index++) {
										case 0:
											id = obj[prop];
											break;
										case 1:
											name = obj[prop];
											break;
										case 2:
											url = obj[prop];
											break;
										case 3:
											type = obj[prop];
											break;
										case 4:
											level = obj[prop];
											break;
										default:
											break;
										}
									}
			
									if ($("#slMenu option[value=" + id
											+ "]").length > 0) {
			
									} else {
										if (level == '0')
											$('#slMenu').append(
													"<option id=\"" + id + "\" value=\"" + id + "\">"
															+ name
															+ "</option>");
										if (level == '1')
											$('#slMenu').append(
													"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
															+ name
															+ "</option>");
									}
			
								}
			
							}).fail(function() {
			
					})
			
			//Load Editable Menu for Staff
			$.ajax({
				          url: '${pageContext.request.contextPath}/getStaffEditableMenu',
				          type: 'POST',
				          data: 'userid='+lUserID,
				          dataType: 'html'
				     })
				     .done(function(data){
				          console.log(data);
				          $('#dynamic-content-editable-menu').html(''); // blank before load.
				          $('#dynamic-content-editable-menu').html(data); // load here
				     })
				     .fail(function(){
				          $('#dynamic-content-editable-menu').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				     });
			
			//Load Allowed Menu for Staff
				$.ajax({
					url : '${pageContext.request.contextPath}/getMenu',
					type : 'POST',
					data : {
						menuid : lRoleID,
						command : 'Update'
					},
					dataType : 'json'
				}).done(
						function(data) {
							console.log(data);
							var data1 = data[0];
							var data2 = data[1];

							for ( var i in data1) {
								var obj = data1[i];
								var index = 0;
								var id, name, url, type, level;
								for ( var prop in obj) {
									switch (index++) {
									case 0:
										id = obj[prop];
										break;
									case 1:
										name = obj[prop];
										break;
									case 2:
										url = obj[prop];
										break;
									case 3:
										type = obj[prop];
										break;
									case 4:
										level = obj[prop];
										break;
									default:
										break;
									}
								}
								if ($("#slAllowedMenu option[value="
										+ id + "]").length > 0) {
								} else {
									if (level == '0')
										$('#slAllowedMenu').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name
														+ "</option>");
									if (level == '1')
										$('#slAllowedMenu').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">&nbsp"
														+ name
														+ "</option>");
								}
							}

							for ( var i in data2) {
								var obj = data2[i];
								modal
										.find(
												".modal-body #"
														+ obj.MenuID)
										.prop('checked', true);
							}
							$('#dvloaderRoleStaff').hide();
						}).fail(function() {
					console.log('Service call failed!');
				})
		})
		
function FuncSaveRole() {
	$("#dvloaderRoleStaff").show();
	
	var listAllowedMenu = document.getElementById('slAllowedMenu');
	var optionVal = new Array();
	var checkVal = new Array();
	var txtRoleId = document.getElementById('txtRoleId').value;

	for (i = 0; i < listAllowedMenu.length; i++) {
		optionVal.push(listAllowedMenu.options[i].value);
	}

	$('input[name="cbEditableMenu"]:checked').each(function() {
		checkVal.push(this.value);
	});

	jQuery
			.ajax({
				url : '${pageContext.request.contextPath}/Role',
				type : 'POST',
				dataType : 'text',
				data : {
					"key" : 'saveStaffRole',
					"listAllowedMenu" : optionVal,
					"listEditableMenu" : checkVal,
					"txtRoleId" : txtRoleId
				},
				success : function(data, textStatus, jqXHR) {
					if (data.split("--")[0] == 'FailedSaveStaffRole') {
						alert("Error! Staff Role Insert Failed. "+ data.split("--")[1]);
						$("#dvloaderRoleStaff").hide();
						return false;
					} else {
						alert("Success! Staff Role Insert Success.");
						$("#dvloaderRoleStaff").hide();
					}
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
					$("#dvloaderRoleStaff").hide();
				}
			});
			
	return true;
}
</script>

</body>
</html>