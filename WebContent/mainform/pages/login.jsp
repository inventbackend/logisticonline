<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style>
  	/* enable absolute positioning */
	.inner-addon { 
	    position: relative; 
	}
	
	/* style icon */
	.inner-addon .glyphicon   {
	  position: absolute;
	  padding: 10px;
	  margin-top:-10px;
	  /*pointer-events: none; */
	}
	
	/* align icon */
	.left-addon .glyphicon    { left:  0px;}
	.right-addon .glyphicon   { right: 0px;}
	
	/* add padding  */
	.left-addon input  { padding-left:  10px; }
	.right-addon input { padding-right: 30px; }
  </style>
</head>

<body class="hold-transition login-page">

<form action="${pageContext.request.contextPath}/Login" method="post">

<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body" style="margin-top:20%">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" alt="Mountain View" style="width:100%;height:100%;"><br>
	<!-- <b>Logistic Online</b> -->
    
  </div>
  
	<!--   error description -->
  	<c:if test="${condition == 1}">
      <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <h4><i class="icon fa fa-ban"></i> Username or password incorrect</h4>
      </div>
    </c:if>
    <c:if test="${condition == 2}">
      <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <h4><i class="icon fa fa-ban"></i> Your account has not been verified</h4>
      </div>
    </c:if>
    <c:if test="${condition == 3}">
      <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <h4><i class="icon fa fa-ban"></i> Your account has been blocked</h4>
      </div>
    </c:if>
    
      
    <p class="login-box-msg">Sign in to start your session</p>
      <c:set var="condition" value="${condition}" ></c:set>
       
      <div class="form-group has-feedback inner-addon">
      	<div class="left-addon">      		
      		<input type="text" class="form-control left-addon" placeholder="user id" id="userName" name="userName">
        </div>
        <div class="right-addon">
        	<i class="glyphicon"><img src="mainform/image/baseline_account_circle_black_18.png" style="width:32.5px; height:32.22px;"></i>
        </div>
      </div>
      
      <div class="form-group has-feedback inner-addon">
      	<div class="left-addon">      		
      		<input type="password" class="form-control left-addon" placeholder="password" id="password" name="password">
        </div>
        <div class="right-addon">
        	<i class="glyphicon"><img id="imgSee" style="width:32.5px; height:32.22px;" src="mainform/image/icon_register/eye.png"></i>
        	<i class="glyphicon"><img id="imgUnsee" style="width:32.5px; height:32.22px;" src="mainform/image/icon_register/visibility.png"></i>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-6">
		<!--           <div class="checkbox icheck"> -->
		<!--             <label> -->
		<!--               <input type="checkbox"> Remember Me -->
		<!--             </label> -->
		<!--           </div> -->
		<a href="ForgotPassword" class="text-center" style="font-weight: bold; color: #003fff;"><u>Forgot password</u></a>
		<br>
		<a href="Register" class="text-center" style="font-weight: bold; color: black;">Register as customer</a>
		<br>
		<a href="RegisterVendor" class="text-center" style="font-weight: bold; color: black;">Register as trucker</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-2">
        </div>
        <!-- /.col -->
        <div class="col-xs-12 g-recaptcha"
			data-sitekey="${captcha_sitekey}">
		</div>
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="btnLogin">Sign In</button>
        </div>
        <!-- /.col -->
      </div>

	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    $('#imgUnsee').hide();
  });
  
  //custom input password
	$(document).ready(function(){
		$(document).on('click', '#imgSee', function(e){
			$('#imgSee').hide();
			$('#imgUnsee').show();
			$('#password').prop('type', 'text');
		});
		
		$(document).on('click', '#imgUnsee', function(e){
			$('#imgSee').show();
			$('#imgUnsee').hide();
			$('#password').prop('type', 'password');
		});
	});
</script>

</body>
</html>
