<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<form action="${pageContext.request.contextPath}/Register" method="post" onsubmit="return checkPassword(this);">

<div class="login-box" style="width:700px">
  
  <div class="login-box-body" style="width:700px; margin-top:-10%;">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" style="width:50%;height:50%;"><br>
	<!-- <b>Logistic Online</b> -->
  </div>
  
			<c:if test="${condition == 'FailedRegisterCustomer'}">
					<div class="alert alert-danger alert-dismissible">
	      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	      				Failed register customer. <c:out value="${conditionDescription}"/>.
    				</div>
			</c:if>
			
			
    <p class="login-box-msg"><b>FORM REGISTER</b></p>
    <div class="row">
      <div class="form-group has-feedback col-xs-12">
        <input type="text" class="form-control" placeholder="Company Name" id="txtCustomerName" name="txtCustomerName" required>
      </div>
       <div class="form-group has-feedback col-xs-6">
        <input type="text" class="form-control" placeholder="Email" id="txtEmail" name="txtEmail" required>
      </div>
      <div class="form-group has-feedback col-xs-6">
        <input type="text" class="form-control" placeholder="PIC" id="txtPIC1" name="txtPIC1" required>
      </div>
		<!--       <div class="form-group has-feedback col-xs-4"> -->
		<!--         <input type="text" class="form-control" placeholder="PIC 2" id="txtPIC2" name="txtPIC2"> -->
		<!--       </div> -->
		<!--       <div class="form-group has-feedback col-xs-4"> -->
		<!--         <input type="text" class="form-control" placeholder="PIC 3" id="txtPIC3" name="txtPIC3"> -->
		<!--       </div> -->
	  <div class="form-group has-feedback col-xs-4">
       <select id="txtDomicile" name="txtDomicile" class="form-control select2" data-placeholder="Province" style="width: 100%;" required>
			<c:forEach items="${listProvince}" var="province">
				<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
			</c:forEach>
       	</select>
      </div>
      <div class="form-group has-feedback col-xs-4">
        <input type="text" class="form-control" placeholder="Address" id="txtAddress" name="txtAddress" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
	<!--  		<select id="slDistrict" name="slDistrict" class="form-control select2" data-placeholder="District" style="width: 100%;" required> -->
	<%-- 			<c:forEach items="${listDistrict}" var="district"> --%>
	<%-- 				<option value="<c:out value="${district.districtID}" />"><c:out value="${district.districtID}" /> - <c:out value="${district.name}" /></option> --%>
	<%-- 			</c:forEach> --%>
	<!--        	</select> -->
       	<input type="text" class="form-control" placeholder="Postal Code" id="txtPostalCode" name="txtPostalCode" required>
      </div>
      <div class="form-group has-feedback col-xs-6">
        <input type="text" class="form-control" placeholder="Office Phone" id="txtOfficePhone" name="txtOfficePhone" required>
      </div>
      <div class="form-group has-feedback col-xs-6">
        <input type="text" class="form-control" placeholder="Mobile Phone" id="txtMobilePhone" name="txtMobilePhone">
      </div>
	<!--       <div class="form-group has-feedback col-xs-6"> -->
	<!--         <input type="text" class="form-control" placeholder="NPWP" id="txtNPWP" name="txtNPWP" required> -->
	<!--       </div> -->
	<!--       <div class="form-group has-feedback col-xs-6"> -->
	<!--         <input type="text" class="form-control" placeholder="TDP" id="txtTDP" name="txtTDP" required> -->
	<!--       </div> -->
      <div class="form-group has-feedback col-xs-4">
        <input type="text" class="form-control" placeholder="Username" id="txtUserName" name="txtUserName" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
        <input type="text" class="form-control" placeholder="Password" id="txtPassword" name="txtPassword" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
        <input type="text" class="form-control" placeholder="Re-type Password" id="txtRePassword" name="txtRePassword" required>
      </div>
	<!--       <div class="form-group has-feedback col-xs-4"> -->
	<!--         <input type="text" class="form-control" placeholder="Billing Address" id="txtBillingAddress" name="txtBillingAddress" required> -->
	<!--       </div> -->
	<!--       <div class="form-group has-feedback col-xs-4"> -->
	<!--         <select id="slTOP" name="slTOP" class="form-control select2" data-placeholder="Term Of Payment" style="width: 100%;"> -->
	<%-- 			<c:forEach items="${listTOP}" var="top"> --%>
	<%-- 				<option value="<c:out value="${top.topID}" />"><c:out value="${top.topID}" /> - <c:out value="${top.name}" /></option> --%>
	<%-- 			</c:forEach> --%>
	<!--        	</select> -->
	<!--       </div> -->
<!--       <div class="form-group has-feedback col-xs-4"> -->
<!--         <input class="form-control number" placeholder="Credit Limit per Week" id="txtCreditLimit" name="txtCreditLimit" required> -->
<!--       </div> -->
	<!--       <div class="form-group has-feedback col-xs-4"> -->
	<!--         <input type="text" class="form-control" placeholder="Bank Name" id="txtBankName" name="txtBankName" required> -->
	<!--       </div> -->
	<!--       <div class="form-group has-feedback col-xs-4"> -->
	<!--         <input type="text" class="form-control" placeholder="Bank Account" id="txtBankAcc" name="txtBankAcc" required> -->
	<!--       </div> -->
	<!--       <div class="form-group has-feedback col-xs-4"> -->
	<!--         <input type="text" class="form-control" placeholder="Bank Branch" id="txtBankBranch" name="txtBankBranch" required> -->
	<!--       </div> -->
      </div>
      
      <div class="row">
        <div class="col-xs-4">
        </div>
        <div class="col-xs-4">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="btnRegister">Register</button>
        </div>
        <!-- /.col -->
      </div>

	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<c:if test="${not empty loginError}">
    <script>
    window.addEventListener("load",function(){
         alert("${loginError}");
    }
    </script>
</c:if>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    //Initialize Select2 Elements
	$(".select2").select2();
	//$('#slDistrict').val('').trigger("change");
	$('#txtDomicile').val('').trigger("change");
	$('#slTOP').val('').trigger("change");
	
	//Initialize default value
	//$('#txtCreditLimit').val('25000000');
	
	$(":input").inputmask();
  	$("#txtOfficePhone").inputmask({"mask": "999[9]-999999999999"});
  });
  
  //custom txtpassword
	$(document).ready(function(){
		$(document).on('focusout', '#txtPassword', function(e){
			 $(this).prop('type', 'password');
		});
		$(document).on('click', '#txtPassword', function(e){
			 $(this).prop('type', 'text');
		});
		
		$(document).on('focusout', '#txtRePassword', function(e){
			 $(this).prop('type', 'password');
		});
		$(document).on('click', '#txtRePassword', function(e){
			 $(this).prop('type', 'text');
		});
	});
	
	//custom number input type
	$('input.number').keyup(function(event) {
 		  // skip for arrow keys
 		  if(event.which >= 37 && event.which <= 40) return;

 		  // format number
 		  $(this).val(function(index, value) {
 		    return value
 		    .replace(/\D/g, "")
 		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
 		    ;
 		  });
 		});
 		
 	//set tab as click
 	$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#txtDomicile:focus').length) {
    	$('#txtDomicile').click();
    }
	});
	
	//  	$(window).keyup(function (e) {
	//     var code = (e.keyCode ? e.keyCode : e.which);
	//     if (code == 9 && $('#slDistrict:focus').length) {
	//     	$('#slDistrict').click();
	//     }
	// 	});
	
	$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slTOP:focus').length) {
    	$('#slTOP').click();
    }
	});
	
	// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}

//check password and repassword is match or not before submit
function checkPassword(theForm) {
	if (theForm.txtPassword.value != theForm.txtRePassword.value)
	{
		alert('Those passwords don\'t match!');
		return false;
	} else {
		return true;
	}
}
</script>

</body>
</html>
