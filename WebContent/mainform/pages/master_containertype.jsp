<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Master Container Type</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form name="Form_ContainerType" action = "${pageContext.request.contextPath}/ContainerType" method="post">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Container Type
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertContainerType'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan container type.
	     				</div>
			</c:if>
		
		<c:if test="${condition == 'SuccessUpdateContainerType'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui container type.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteContainerType'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus container type.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteContainerType'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus container type. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
		          <div id="dvContainerTypeID" class="form-group">
		            <label for="lblContainerTypeID" class="control-label">Container Type ID:</label><label id="mrkContainerTypeID" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtContainerTypeID" name="txtContainerTypeID" readonly disabled>
		          </div>
		          <div id="dvDescription" class="form-group">
		            <label for="lblDescription" class="control-label">Description:</label><label id="mrkDescription" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtDescription" name="txtDescription">
		          </div>
		          <div id="dvIso" class="form-group">
		            <label for="lblIso" class="control-label">Iso code:</label><label id="mrkIso" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control" id="txtIso" name="txtIso">
		          </div>
		          <div id="dvTare" class="form-group">
		            <label for="lblTare" class="control-label">Tare (kg):</label><label id="mrkTare" for="lbl-validation" class="control-label"><small>*</small></label>
		            <input type="text" class="form-control number" id="txtTare" name="txtTare">
		          </div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" value="btnUpdate" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('update')"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Container Type</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtContainerTypeID" name="temp_txtContainerTypeID"  />
									<p>Are you sure to delete this container type ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="button" id="btnDelete" name="btnDelete" onclick="FuncDelete()" class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_master_containertype" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>ID</th>
	                  <th>Description</th>
	                  <th>Iso</th>
	                  <th>Tare</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listContainerType}" var ="containertype">
		        	<tr>
		        	<td><c:out value="${containertype.containerTypeID}"/></td>
					<td><c:out value="${containertype.description}"/></td>
					<td><c:out value="${containertype.iso}"/></td>
					<td><c:out value="${containertype.tare}"/> kg</td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lcontainertypeid='<c:out value="${containertype.containerTypeID}"/>'
								data-ldescription='<c:out value="${containertype.description}"/>' 
								data-liso='<c:out value="${containertype.iso}"/>' 
								data-ltare='<c:out value="${containertype.tare}"/>' 
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-lcontainertypeid='<c:out value="${containertype.containerTypeID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        
	        </tbody>
        </table>
        
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<!-- $("#tb_master_plant").DataTable(); -->
<!-- paging script -->
<script>
  $(function () {
    $('#tb_master_containertype').DataTable({
    	"aaSorting": []
    });
    $('#M002').addClass('active');
	$('#M008').addClass('active');
	
	//custom number input type
	$('input.number').keyup(function(event) {
	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40) return;

	  // format number
	  $(this).val(function(index, value) {
	    return value
	    .replace(/\D/g, "")
	    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	    ;
	  });
	});
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	$('#ModalUpdateInsert').modal('show')
	    	});
	
	$("#dvErrorAlert").hide();
  });
</script>
<script>
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lContainerTypeID = button.data('lcontainertypeid');
		$("#temp_txtContainerTypeID").val(lContainerTypeID);
		//$("#btnDelete").prop('disabled',false);
	})
	</script>
<!-- modal script -->
<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		//$("#btnDelete").prop('disabled',true);
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var lcontainertypeid = button.data('lcontainertypeid') // Extract info from data-* attributes
	  var ldescription = button.data('ldescription') // Extract info from data-* attributes
	  var liso = button.data('liso') // Extract info from data-* attributes
	  var ltare = button.data('ltare') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	//   modal.find('.modal-title').text('New message to ' + recipient)
	  modal.find('.modal-body #txtContainerTypeID').val(lcontainertypeid)
	  modal.find('.modal-body #txtDescription').val(ldescription)
	  modal.find('.modal-body #txtIso').val(liso)
	  modal.find('.modal-body #txtTare').val(ltare)
	  
	  $("#txtDescription").focus();
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>

	function FuncClear(){
		$('#mrkContainerTypeID').hide();
		$('#mrkDescription').hide();
		$('#mrkIso').hide();
		$('#mrkTare').hide();
		
		$('#dvDescription').removeClass('has-error');
		$('#dvIso').removeClass('has-error');
		$('#dvTare').removeClass('has-error');
		$("#dvErrorAlert").hide();
	}
	
	function FuncButtonNew() {
		$('#dvContainerTypeID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		document.getElementById("lblTitleModal").innerHTML = "Add Container Type";
		
		//trigger btnClick On Enter
		// 		$(document).unbind("keyup").keyup(function(e){ 
		// 		    var code = e.which;
		// 		    if(code==13)
		// 		    {
		// 		    	$("#btnSave").click();
		// 		    }
		// 		});
		
		FuncClear();
	}
	
	function FuncButtonUpdate() {
		$('#dvContainerTypeID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		document.getElementById("lblTitleModal").innerHTML = 'Edit Container Type';
		
		//trigger btnClick On Enter
		// 		$(document).unbind("keyup").keyup(function(e){ 
		// 		    var code = e.which;
		// 		    if(code==13)
		// 		    {
		// 		    	$("#btnUpdate").click();
		// 		    }
		// 		});
		
		FuncClear();
	}
	
	//function delete rate
	function FuncDelete() {
		var temp_txtContainerTypeID = document.getElementById('temp_txtContainerTypeID').value;  
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/ContainerType',	
	        type:'POST',
	        data:{"key":"Delete","temp_txtContainerTypeID":temp_txtContainerTypeID},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	       		var url = '${pageContext.request.contextPath}/ContainerType';  
		        $(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	function FuncValEmptyInput(lParambtn) {
		var txtContainerTypeID = document.getElementById('txtContainerTypeID').value;
		var txtDescription = document.getElementById('txtDescription').value;
		var txtIso = document.getElementById('txtIso').value;
		var txtTare = document.getElementById('txtTare').value;
		
	    if(!txtDescription.match(/\S/)) {    	
	    	$('#txtDescription').focus();
	    	$('#dvDescription').addClass('has-error');
	    	$('#mrkDescription').show();
	        return false;
	    }
	    if(!txtIso.match(/\S/)) {    	
	    	$('#txtIso').focus();
	    	$('#dvIso').addClass('has-error');
	    	$('#mrkIso').show();
	        return false;
	    }
	    if(!txtTare.match(/\S/)) {    	
	    	$('#txtTare').focus();
	    	$('#dvTare').addClass('has-error');
	    	$('#mrkTare').show();
	        return false;
	    } 
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/ContainerType',	
	        type:'POST',
	        data:{"key":lParambtn,"txtContainerTypeID":txtContainerTypeID,"txtDescription":txtDescription,"txtIso":txtIso,"txtTare":txtTare},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertContainerType')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan container type";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDescription").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateContainerType')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui container type";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtDescription").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/ContainerType';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
//set default action when button enter is clicked
var input = document.getElementById("ModalUpdateInsert");

input.addEventListener("keyup", function(event) {
  var containertypeid = document.getElementById('txtContainerTypeID').value;
  event.preventDefault();
  if (event.keyCode === 13 && (containertypeid == null || containertypeid == '')) {
    document.getElementById("btnSave").click();
  }
  else if (event.keyCode === 13 && (containertypeid != null || containertypeid != '')) {
  	document.getElementById("btnUpdate").click();
  }
});
</script>

</body>
</html>