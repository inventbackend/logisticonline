<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>LOGOL | Trucker Pick Order</title>

<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<input  type="hidden" id="temp_vendorid" name="temp_vendorid" value="<c:out value="${vendorID}"/>" />
<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		Order Picking
	</h1>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessPick'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Pick order success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedPick'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Pick order failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					
		<div id="dvSearch" class="row">	
		<div class="col-md-2">
		<label for="message-text" class="control-label">Order Type</label>
			<select id="slOrderType" name="slOrderType" class="form-control">
				<c:if test="${orderType eq 'EXPORT'}">
					<option selected>EXPORT</option>
					<option>IMPORT</option>
				</c:if>
				<c:if test="${orderType eq 'IMPORT'}">
					<option>EXPORT</option>
					<option selected>IMPORT</option>
				</c:if>
       		</select>
       	</div>
		 <!-- /.col-md-2 -->
		 
		 <%-- <div class="col-md-2">
		<label for="message-text" class="control-label">Day</label>
			<select id="slDay" name="slDay" class="form-control">
				<c:if test="${day eq 'TODAY'}">
					<option selected>TODAY</option>
					<option>NEXT DAY</option>
				</c:if>
				<c:if test="${day eq 'NEXT DAY'}">
					<option>TODAY</option>
					<option selected>NEXT DAY</option>
				</c:if>
       		</select>
       	</div> --%>
		 <!-- /.col-md-2 -->
		 
		  <div class="col-md-2">
			<label for="message-text" class="control-label">Stuffing Date From</label>
			<input type="text" class="form-control" id="txtStuffingDateFrom" name="txtStuffingDateFrom" value="<c:out value="${stuffingDateFrom}"/>">
       	</div>
		<!-- /.col-md-2 -->
		<div class="col-md-2">
			<label for="message-text" class="control-label">Stuffing Date To</label>
			<input type="text" class="form-control" id="txtStuffingDateTo" name="txtStuffingDateTo" value="<c:out value="${stuffingDateTo}"/>">
     	</div>
		<!-- /.col-md-2 -->
		
		<div class="col-md-2">
		<button id="btnSearch" name="btnSearch" type="button" class="btn btn-primary" onclick="FuncButtonSearch('search')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-2 -->
		</div>
		<!-- /.row -->
       	<br><br>
					
		<!-- modal pop up for add and edit -->
		<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
				  
				  	<div id="dvOrderManagementID" class="form-group">
		            <label for="lblOrderManagementID" class="control-label">Order Management ID:</label>
		            <input type="text" class="form-control" id="txtOrderManagementID" name="txtOrderManagementID" readonly disabled>
		          	</div>
		          	<div id="dvOrderDetail" class="form-group">
		            <label for="lblOrderDetail" class="control-label">Order Detail ID:</label>
		            <input type="text" class="form-control" id="txtOrderDetailID" name="txtOrderDetailID" readonly disabled>
		          	</div>
		          	<div class="form-group">
		            <label for="lblCustomer" class="control-label">Customer:</label>
		            <input type="text" class="form-control" id="txtCustomer" name="txtCustomer" readonly disabled>
		          	</div>
		          	<div class="form-group">
		            <label for="lblFactory" class="control-label">Factory:</label>
		            <input type="text" class="form-control" id="txtFactory" name="txtFactory" readonly disabled>
		          	</div>
		          	<div id="dvStuffingDate" class="form-group">
		            <label for="lblStuffinDate" class="control-label">Stuffing Date:</label>
		            <input type="text" class="form-control" id="txtStuffingDate" name="txtStuffingDate" readonly disabled>
		          	</div>
		          	<div id="dvTier" class="form-group">
		            <label for="lblTier" class="control-label">Tier ID:</label>
		            <small><label id="lblTierDescription" name="lblTierDescription" class="control-label"></label></small>
		            <input type="text" class="form-control" id="txtTierID" name="txtTierID" readonly disabled>
		          	</div>
		          	<div id="dvContainerType" class="form-group">
		            <label for="lblContainerType" class="control-label">Container Type ID:</label>
		            <small><label id="lblContainerTypeDescription" name="lblContainerTypeDescription" class="control-label"></label></small>
		            <input type="text" class="form-control" id="txtContainerTypeID" name="txtContainerTypeID" readonly disabled>
		          	</div>
		          	<div class="form-group">
		            <label for="lblDepo" class="control-label">Depo:</label>
		            <input type="text" class="form-control" id="txtDepo" name="txtDepo" readonly disabled>
		          	</div>
		          	<div class="form-group">
		            <label for="lblPort" class="control-label">Port:</label>
		            <input type="text" class="form-control" id="txtPort" name="txtPort" readonly disabled>
		          	</div>
		          	<div id="dvItem" class="form-group">
		            <label for="lblItem" class="control-label">Commodity:</label>
		            <input type="text" class="form-control" id="txtItem" name="txtItem" readonly disabled>
		          	</div>
		          	<div id="dvQuantity" class="form-group">
			      	<label for="lblQuantity" class="control-label">Quantity:</label><label id="mrkQuantity" for="lbl-validation" class="control-label"><small>*</small></label>
			        <input class="form-control number" id="txtQuantity" name="txtQuantity">
			        <input  type="hidden" id="txtLastQuantity" name="txtLastQuantity">
			        <input  type="hidden" id="txtLastPrice" name="txtLastPrice">
			        </div>
		      </div>
		      <div class="modal-footer">
				<button id="btnSave" name="btnSave" value="btnSave" type="button" class="btn btn-primary" onclick="FuncValEmptyInput('save')"><i class="fa fa-check"></i> Pick</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<c:if test="${globalUserRole eq 'R003'}">
			<!-- dashboard atas -->
			<div class="row">
				<c:forEach items="${listDashboard}" var ="component">
	 				<div class="col-md-3 col-sm-6 col-xs-12">
			          <div class="info-box">
			            <span class="info-box-icon" style="background-color: #005292;">
			            	<i><img style="margin-top: 10px;"
			            			width="55px" height="55px" 
			            			src="mainform/dist/img/order/Group 4275@2x.png">
			            	</i>
			            </span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" style="color: black; font-weight: normal;"><c:out value="${component.boxTitle}" /></span>
			              <span class="info-box-number" style="margin-top:12px; font-size: 20px;">
			              	<c:out value="${component.boxValue}" />
			              </span>
			            </div>
			            <!-- /.info-box-content -->
			          </div>
			          <!-- /.info-box -->
			        </div>
			        <!-- /.col -->
	 			</c:forEach>
			</div>
			<!-- akhir dari dashboard atas -->
		</c:if>
				
							<table id="tb_available_order" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th style="background-color: #09C0DC; color: #FFFFFF;">Pick Order</th>
										<th>Order ID</th>
										<th>SI No</th>
										<!-- <th>Order Detail ID</th> -->
										<th>Customer</th>
										<th>Factory</th>
										<th>District</th>
										<th>Stuffing Date</th>
										<th>Tier</th>
										<th>Container</th>
										<th>Depo</th>
										<th>Port</th>
										<th>Commodity</th>
										<th>Qty</th>
										<!-- <th>Rate</th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listPickOrder}" var="po">
										<tr>
											<td>
											<button id="btnPickOrder" name="btnPickOrder" type="button" class="btn btn-default" data-toggle="modal"
														title="pick"
														data-target="#ModalUpdateInsert" 
														data-lorderid='<c:out value="${po.orderManagementID}" />'
														data-lorderdetailid='<c:out value="${po.orderManagementDetailID}" />'
														data-lcustomer='<c:out value="${po.customer}" />'
														data-ldistrict='<c:out value="${po.district}" />'
														data-lfactory='<c:out value="${po.factory}" />'
														data-ldepo='<c:out value="${po.depo}" />'
														data-lport='<c:out value="${po.port}" />'
														data-lstuffingdate='<c:out value="${po.stuffingDate}" />'
														data-ltierid='<c:out value="${po.tierID}" />'
														data-ltierdesc='<c:out value="${po.tierDescription}" />'
														data-lcontainertypeid='<c:out value="${po.containerTypeID}" />'
														data-lcontainertypedesc='<c:out value="${po.containerTypeDescription}" />'
														data-litemdesc='<c:out value="${po.itemDescription}" />'
														data-lqty='<c:out value="${po.quantity}" />'
														data-lprice='<c:out value="${po.price}" />'
														style="display:<c:out value="${buttonstatus}"/>"
														>
												<img src="mainform/dist/img/order/Group 4317.png">
											</button>
											</td>
											<td><c:out value="${po.orderManagementID}" /></td>
											<td><c:out value="${po.si}" /></td>
											<%-- <td><c:out value="${po.orderManagementDetailID}" /></td> --%>
											<td><c:out value="${po.customer}" /></td>
											<td><c:out value="${po.factory}" /></td>
											<td><c:out value="${po.district}" /></td>
											<td><c:out value="${po.stuffingDate}" /></td>
											<td><c:out value="${po.tierDescription}" /></td>
											<td><c:out value="${po.containerTypeDescription}" /></td>
											<td><c:out value="${po.depo}" /></td>
											<td><c:out value="${po.port}" /></td>
											<td><c:out value="${po.itemDescription}" /></td>
											<td><c:out value="${po.quantity}" /></td>
											<!-- 											<td> -->
											<%-- 												<fmt:setLocale value="id_ID"/> --%>
											<%-- 												<fmt:formatNumber value="${po.price}" type="currency"/> --%>
											<!-- 											</td> -->
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- bootstrap datepicker -->
<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
$(function () {
		$("#tb_available_order").DataTable({
			"aaSorting": [],
			"scrollX": true
		});
		$('#M018').addClass('active');
		$('#M019').addClass('active');
		
		//calendar java script
		$('#txtStuffingDateFrom').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtStuffingDateTo').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  		
  		//custom number input type
		$('input.number').keyup(function(event) {
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;
	
		  // format number
		  $(this).val(function(index, value) {
		    return value
		    .replace(/\D/g, "")
		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		    ;
		  });
		});
		
	});
	
$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  
	  var orderid = button.data('lorderid')
	  var orderdetailid = button.data('lorderdetailid')
	  var stuffingdate = button.data('lstuffingdate')
	  var tierid = button.data('ltierid')
	  var tierdesc = button.data('ltierdesc')
	  var containertypeid = button.data('lcontainertypeid')
	  var containertypedesc = button.data('lcontainertypedesc')
	  var itemdesc = button.data('litemdesc')
	  var qty = button.data('lqty')
	  var price = button.data('lprice')
	  var customer = button.data('lcustomer')	  
	  var factory = button.data('lfactory')
	  var district = button.data('ldistrict')
	  var depo = button.data('ldepo')
	  var port = button.data('lport')
	  
	  var modal = $(this)

	  modal.find('.modal-body #txtOrderManagementID').val(orderid)
	  modal.find('.modal-body #txtOrderDetailID').val(orderdetailid)
	  modal.find('.modal-body #txtStuffingDate').val(stuffingdate)
	  modal.find('.modal-body #txtTierID').val(tierdesc)
	  modal.find('.modal-body #txtContainerTypeID').val(containertypedesc)
	  modal.find('.modal-body #txtItem').val(itemdesc)
	  modal.find('.modal-body #txtQuantity').val(qty)
	  modal.find('.modal-body #txtLastQuantity').val(qty)
	  modal.find('.modal-body #txtLastPrice').val(price)
	  document.getElementById("lblTierDescription").innerHTML = "(" + tierid + ")";
	  document.getElementById("lblContainerTypeDescription").innerHTML = "(" + containertypeid + ")";
	  modal.find('.modal-body #txtCustomer').val(customer)
	  modal.find('.modal-body #txtFactory').val(factory+" - "+district)
	  modal.find('.modal-body #txtDepo').val(depo)
	  modal.find('.modal-body #txtPort').val(port)
	  
	  $('#txtQuantity').focus();
	  FuncClear();
	})
	
function FuncClear(){
	$('#mrkQuantity').hide();
	
	$('#dvQuantity').removeClass('has-error');
}

function FuncValEmptyInput(lParambtn) {
	var OrderType = document.getElementById('slOrderType').value;
	/* var Day = document.getElementById('slDay').value; */
 	var StuffingDateFrom = document.getElementById('txtStuffingDateFrom').value;
 	var StuffingDateTo = document.getElementById('txtStuffingDateTo').value;
 	
	var OrderID = document.getElementById('txtOrderManagementID').value;
	var OrderDetailID = document.getElementById('txtOrderDetailID').value;
	var VendorID = document.getElementById('temp_vendorid').value;
	var Quantity = document.getElementById('txtQuantity').value;
	var LastQuantity = document.getElementById('txtLastQuantity').value;
	var LastPrice = document.getElementById('txtLastPrice').value;
	
	if(!Quantity.match(/\S/)) {
	    	$("#txtQuantity").focus();
	    	$('#dvQuantity').addClass('has-error');
	    	$('#mrkQuantity').show();
	    	document.getElementById('mrkQuantity').innerHTML = '*';
	        return false;
	    }
	    
	 jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorPickOrder',	
	        type:'POST',
	        data:{"key":"validate_quantity", "quantity":Quantity, "lastquantity":LastQuantity},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data=='disallow') {
	    			$('#txtQuantity').focus();
	    	    	$('#dvQuantity').addClass('has-error');
	    	    	$('#mrkQuantity').show();
	    	    	document.getElementById('mrkQuantity').innerHTML = '&nbspmay not bigger than available quantity';
	    	        return false;
	    	    }
	    	    else if(data == 'disallow 0') {
	    			$('#txtQuantity').focus();
	    	    	$('#dvQuantity').addClass('has-error');
	    	    	$('#mrkQuantity').show();
	    	    	document.getElementById('mrkQuantity').innerHTML = '&nbspmay not zero';
	    	        return false;
	    	    }
	        	else {
	         	    jQuery.ajax({
	         	        url:'${pageContext.request.contextPath}/VendorPickOrder',	
	         	        type:'POST',
	         	        data:{"key":lParambtn, "ordertype":OrderType, "stuffingdatefrom":StuffingDateFrom, "stuffingdateto":StuffingDateTo, 
	         	        	"orderid":OrderID, "orderdetailid":OrderDetailID, 
	        				"vendorid":VendorID, "quantity":Quantity, "lastquantity":LastQuantity, 
	        				"lastprice":LastPrice},
	        				/* "day":Day, */
	         	        dataType : 'text',
	         	        success:function(data, textStatus, jqXHR){
	         	        	var url = '${pageContext.request.contextPath}/VendorPickOrder';  
	         	        	$(location).attr('href', url);
	         	        },
	         	        error:function(data, textStatus, jqXHR){
	         	            console.log('Service call failed!');
	         	        }
	         	    });
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
	    
	    FuncClear();
}
 		
function FuncButtonSearch(lParambtn) {
 	var OrderType = document.getElementById('slOrderType').value;
 	/* var Day = document.getElementById('slDay').value; */
	var StuffingDateFrom = document.getElementById('txtStuffingDateFrom').value;
	var StuffingDateTo = document.getElementById('txtStuffingDateTo').value;
 	var VendorID = document.getElementById('temp_vendorid').value;
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorPickOrder',	
	        type:'POST',
	        data:{"key":lParambtn,"ordertype":OrderType,"stuffingdatefrom":StuffingDateFrom, "stuffingdateto":StuffingDateTo,"vendorid":VendorID},
	        /* "day":Day */
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	var url = '${pageContext.request.contextPath}/VendorPickOrder';  
 	        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}
</script>

</body>
</html>