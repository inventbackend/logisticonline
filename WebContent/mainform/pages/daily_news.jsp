<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Daily News</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewnews">
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
	<style type="text/css">
		/* for image modal pop up */
		/* Style the Image Used to Trigger the Modal */
		#myImg {
		    border-radius: 5px;
		    cursor: pointer;
		    transition: 0.3s;
		}
		
		#myImg:hover {opacity: 0.7;}
		
		/* The Modal (background) */
		.imagemodal {
		    display: none; /* Hidden by default */
		    position: fixed; /* Stay in place */
		    z-index: 1; /* Sit on top */
		    padding-top: 100px; /* Location of the box */
		    left: 0;
		    top: 0;
		    width: 100%; /* Full width */
		    height: 100%; /* Full height */
		    overflow: auto; /* Enable scroll if needed */
		    background-color: rgb(0,0,0); /* Fallback color */
		    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
		}
		
		/* Modal Content (Image) */
		.modal-content-image {
		    margin: auto;
		    display: block;
		    width: 80%;
		    max-width: 700px;
		}

		
		/* Caption of Modal Image (Image Text) - Same Width as the Image */
		#caption {
		    margin: auto;
		    display: block;
		    width: 80%;
		    max-width: 700px;
		    text-align: center;
		    color: #ccc;
		    padding: 10px 0;
		    height: 150px;
		}
		
		/* The Close Button */
		.closespanimage {
		    position: absolute;
		    top: 80px;
		    right: 40px;
		    color: #f1f1f1;
		    font-size: 40px;
		    font-weight: bold;
		    transition: 0.3s;
		}
		
		.closespanimage:hover,
		.closespanimage:focus {
		    color: #bbb;
		    text-decoration: none;
		    cursor: pointer;
		}
		/* end of image modal pop up */
		</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="Form_News" name="" action = "News" method="post" enctype="multipart/form-data">

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
  
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daily News
        <small>tables</small>
      </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
  
  		<div class="box">
        <div class="box-body">
        
        <c:if test="${condition == 'SuccessInsertNews'}">
		  <div class="alert alert-success alert-dismissible">
	 				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    				  	<h4><i class="icon fa fa-check"></i> Success</h4>
	       			  	Sukses menambahkan news.
	     				</div>
			</c:if>
			
			<c:if test="${condition == 'FailedInsertNews'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menambahkan news. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessUpdateNews'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses memperbaharui news.
   				</div>
		</c:if>
		
		<c:if test="${condition == 'FailedUpdateNews'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal memperbaharui news. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
		
		<c:if test="${condition == 'SuccessDeleteNews'}">
	  		<div class="alert alert-success alert-dismissible">
			      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
     			  	Sukses menghapus news.
   				</div>
		</c:if>
			
		<c:if test="${condition == 'FailedDeleteNews'}">
			<div class="alert alert-danger alert-dismissible">
      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
      				Gagal menghapus news. <c:out value="${conditionDescription}"/>.
    				</div>
		</c:if>
        
        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
        <br><br>
        <!-- modal pop up for add and edit -->
		<div class="modal fade bs-example-modal-lg" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateInsertLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		    
				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
     				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     				<h4><i class="icon fa fa-ban"></i> Failed</h4>
     				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				</div>
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>
		      </div>
		      <div class="modal-body">
					<!-- <form> -->
					<div class="row">
						<div class="col-md-8">
							<div id="dvNewsID" class="form-group">
				            <label for="lblNewsID" class="control-label">News ID :</label><label id="mrkNewsID" for="lbl-validation" class="control-label"><small>*</small></label>
				            <input type="text" class="form-control" id="txtNewsID" name="txtNewsID" readonly>
				          </div>
				          <div id="dvStartDateValid" class="form-group">
				            <label for="lblStartDateValid" class="control-label">Start Date Valid :</label><label id="mrkStartDateValid" for="lbl-validation" class="control-label"><small>*</small></label>
				            <input type="text" class="form-control" id="txtStartDateValid" name="txtStartDateValid">
				          </div>
				           <div id="dvEndDateValid" class="form-group">
				            <label for="lblEndDateValid" class="control-label">End Date Valid :</label><label id="mrkEndDateValid" for="lbl-validation" class="control-label"><small>*</small></label>
				            <input type="text" class="form-control" id="txtEndDateValid" name="txtEndDateValid">
				          </div>
				          <div id="dvNewsText" class="form-group">
				            <label for="lblNewsText" class="control-label">Text :</label><label id="mrkNewsText" for="lbl-validation" class="control-label"><small>*</small></label>
				            <textarea class="form-control" style="resize:none" rows="5" id="txtNewsText" name="txtNewsText"></textarea>
				          </div>
				          <div id="dvNewsImage" class="form-group">
				            <label for="lblNewsImage" class="control-label">Image :</label><label id="mrkNewsImage" for="lbl-validation" class="control-label"></label>
				            <input type="file" class="form-control" id="txtNewsImage" name="txtNewsImage">
				          </div>
				          <div class="row" id="ImageBox">
							<div class="col-md-4">
								<div class="box box-default box-solid">
									<div class="box-body">
										<!-- Trigger the Modal -->
										<img id="myImg" class="img-responsive" 
											src="" alt="Photo" style="width:228px;height:110px">
									</div>
							    </div>
						    </div>
						</div>
						<div id="myModal" class="imagemodal">
							<!-- The Close Button -->
						 	<span class="closespanimage">&times;</span>
							<!-- Modal Content (The Image) -->
							<img class="modal-content-image" id="img01">
							<!-- Modal Caption (Image Text) -->
						  	<div id="caption"></div>
						</div>
					</div>
					<div class="col-md-4">
						<input type="hidden" id="txtListDriver" name="txtListDriver">
						<div class="input-group">
							<label for="lblMenuLeverage" class="control-label">Driver :</label>
	
							<div class="panel panel-primary" id="DriverPanel"
								style="overflow: auto; height: 350px;">
								<div class="panel-body fixed-panel">
									<c:forEach items="${listDriver}" var="driver">
										<div class="checkbox" id="cbmenu">
											<label><input type="checkbox" name="cbDriver"
												id="<c:out value="${driver.driverID}" />"
												value="<c:out value="${driver.driverID}" />">
												<c:out value="${driver.driverName}" /></label>
										</div>
									</c:forEach>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6" style="weight: 200px">
									<button type="button" class="btn btn-block btn-default"
										onclick="FuncSelectAll()">Select All</button>
								</div>
								<div class="col-md-6" style="margin-left: -20px">
									<button type="button" class="btn btn-block btn-default"
										style="width: 100px" onclick="FuncUnselectAll()">Unselect
										All</button>
								</div>
							</div>							
						</div>
					</div>
				</div>
					<!-- </form> -->
		      </div>
		      <div class="modal-footer">
		        <button style="display: <c:out value="${buttonstatus}"/>" id="btnUpdate" name="btnUpdate" type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Update</button>
				<button style="display: <c:out value="${buttonstatus}"/>" id="btnSave" name="btnSave" type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
		      	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
		      </div>
		    </div>
		  </div>
		</div>	
		<!-- End modal pop up for add and edit-->
		
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Daily News</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtNewsID" name="temp_txtNewsID"  />
									<p>Are you sure to delete this daily news ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->	
        

        <table id="tb_daily_news" class="table table-bordered table-striped table-hover">
	        <thead style="background-color: #d2d6de;">
	                <tr>
	                  <th>News ID</th>
	                  <th>Start Date</th>
	                  <th>End Date</th>
	                  <th>News Text</th>
	                  <th style="width: 60px"></th>
	                </tr>
	        </thead>
        
	        <tbody>
	        
		        <c:forEach items="${listNews}" var ="news">
		        	<tr>
		        	<td><c:out value="${news.newsID}"/></td>
					<td><c:out value="${news.startDateValid}"/></td>
					<td><c:out value="${news.endDateValid}"/></td>
					<td style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width:100px;">
						<c:out value="${news.newsText}"/></td>
					<td>
						<button style="display: <c:out value="${buttonstatus}"/>"
								type="button" class="btn btn-info"
								data-toggle="modal"
								onclick="FuncButtonUpdate()" 
								data-target="#ModalUpdateInsert"
								data-lnewsid='<c:out value="${news.newsID}"/>'
								data-lstartdatevalid='<c:out value="${news.startDateValid}"/>'
								data-lenddatevalid='<c:out value="${news.endDateValid}"/>' 
								data-lnewstext='<c:out value="${news.newsText}"/>' 
								data-lnewsimage='<c:out value="${fn:replace(news.newsImage,'LogisticOnline/','')}" />'
								data-lkey='update'
								><i class="fa fa-edit"></i></button>
				        <button style="display: <c:out value="${buttonstatus}"/>" type="button" class="btn btn-danger" data-toggle="modal" 
				        		data-target="#ModalDelete" data-lnewsid='<c:out value="${news.newsID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
			        </td>
	        		</tr>
		        </c:forEach>
	        </tbody>
        </table>
        </div>
        <!-- /.box-body -->
        
  		</div>
  		<!-- /.box -->
  
  		</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<%@ include file="/mainform/pages/master_footer.jsp" %>

</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
  $(function () {
    jQuery('#tb_daily_news').DataTable();
    jQuery('#M002').addClass('active');
    jQuery('#M021').addClass('active');
	
	//shortcut for button 'new'
	    Mousetrap.bind('n', function() {
	    	FuncButtonNew(),
	    	jQuery('#ModalUpdateInsert').modal('show')
    	});
	
	jQuery("#dvErrorAlert").hide();
	
	$('#txtStartDateValid').datepicker({
      format: 'dd M yyyy',
      autoclose: true
    });
	    
    $('#txtEndDateValid').datepicker({
      format: 'dd M yyyy',
      autoclose: true
    });
  });
</script>
<script>
	jQuery('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lNewsID = button.data('lnewsid');
		jQuery("#temp_txtNewsID").val(lNewsID);
	})
	</script>
<!-- modal script -->
<script>
	jQuery('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
		
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var lnewsid = button.data('lnewsid');
	  var lstartdatevalid = button.data('lstartdatevalid');
	  var lenddatevalid = button.data('lenddatevalid');
	  var lnewstext = button.data('lnewstext');
	  var lnewsimage = button.data('lnewsimage');
	  var lkey = button.data('lkey');
	  
	  var modal = jQuery(this)
	  modal.find('.modal-body #txtNewsID').val(lnewsid)
	  modal.find('.modal-body #txtStartDateValid').val(lstartdatevalid)
	  modal.find('.modal-body #txtEndDateValid').val(lenddatevalid)
	  modal.find('.modal-body #txtNewsText').val(lnewstext)
	  modal.find('.modal-body #myImg').attr("src",lnewsimage); //for local and gcloud
	  /* modal.find('.modal-body #myImg').attr("src",'../'+lnewsimage); */ //for staging
	  
	  if (lkey == "update") {
	  	//<<Javascript For 1 json
		$.ajax({
			url : '${pageContext.request.contextPath}/getDriver',
			type : 'POST',
			data : {
				newsid : lnewsid,
				command : lkey
			},
			dataType : 'json'
		}).done(
				function(data) {
					console.log(data);
					var driverArray = data[0];
					for ( var i in driverArray) {
						var driver = driverArray[i];
						modal.find(".modal-body #"+ driver.driverID).prop('checked', true);
					}
				}).fail(function() {
			console.log('Service call failed!');
		});
	  }
	})
</script>
<!-- end of modal script -->

<!-- General Script -->
<script>
	function FuncClear(){
		jQuery('#mrkNewsID').hide();
		jQuery('#mrkStartDateValid').hide();
		jQuery('#mrkEndDateValid').hide();
		jQuery('#mrkNewsText').hide();
		
		jQuery('#dvtxtNewsID').removeClass('has-error');
		jQuery('#dvStartDateValid').removeClass('has-error');
		jQuery('#dvEndDateValid').removeClass('has-error');
		jQuery('#dvNewsText').removeClass('has-error');
		
		jQuery("#dvErrorAlert").hide();
	}
	
	function FuncSelectAll() {
		$("#DriverPanel :input").prop("checked", true);
	}
	
	function FuncUnselectAll() {
		$("#DriverPanel :input").prop("checked", false);
	}
	
	function FuncButtonNew(){
		$('#dvNewsID').hide();
		$('#btnSave').show();
		$('#btnUpdate').hide();
		$('#ImageBox').hide();
		
		document.getElementById("lblTitleModal").innerHTML = "Add Daily News";	
		
		jQuery('#txtNewsText').val('');
		jQuery('#txtNewsImage').val('');
		
		FuncUnselectAll()
		FuncClear();
	}
	
	function FuncButtonUpdate(){
		$('#dvNewsID').show();
		$('#btnSave').hide();
		$('#btnUpdate').show();
		$('#ImageBox').show();
		
		document.getElementById("lblTitleModal").innerHTML = 'Edit Daily News';
	
		FuncClear();
	}
	
	$('#Form_News').submit(function() {
		var txtStartDateValid = jQuery('#txtStartDateValid').val();
		var txtEndDateValid = jQuery('#txtEndDateValid').val();
		var txtNewsText = jQuery('#txtNewsText').val();
		var temp_NewsID = jQuery('#temp_txtNewsID').val();
		
		//check if the process is delete or not, if not, do validation
		if(temp_NewsID == null){
			FuncClear();
 		
			if(!txtStartDateValid.match(/\S/)) {    	
		    	jQuery('#txtStartDateValid').focus();
		    	jQuery('#dvStartDateValid').addClass('has-error');
		    	jQuery('#mrkStartDateValid').show();
		        return false;
		    }
	     	if(!txtEndDateValid.match(/\S/)) {    	
		    	jQuery('#txtEndDateValid').focus();
		    	jQuery('#dvEndDateValid').addClass('has-error');
		    	jQuery('#mrkEndDateValid').show();
		        return false;
		    }
		    if(!txtNewsText.match(/\S/)) {    	
		    	jQuery('#txtEndDateValid').focus();
		    	jQuery('#dvEndDateValid').addClass('has-error');
		    	jQuery('#mrkEndDateValid').show();
		        return false;
		    }
		}
		
		return true;
	});  
   
	function FuncValEmptyInput(lParambtn) {
		var txtNewsID = jQuery('#txtNewsID').val();
		var txtStartDateValid = jQuery('#txtStartDateValid').val();
		var txtEndDateValid = jQuery('#txtEndDateValid').val();
		var txtNewsText = jQuery('#txtNewsText').val();
		var txtNewsImage = document.getElementById("txtNewsImage").value;
		var checkVal = new Array();
		
		if(!txtStartDateValid.match(/\S/)) {    	
	    	jQuery('#txtStartDateValid').focus();
	    	jQuery('#dvStartDateValid').addClass('has-error');
	    	jQuery('#mrkStartDateValid').show();
	        return false;
	    }
     	if(!txtEndDateValid.match(/\S/)) {    	
	    	jQuery('#txtEndDateValid').focus();
	    	jQuery('#dvEndDateValid').addClass('has-error');
	    	jQuery('#mrkEndDateValid').show();
	        return false;
	    }
	    if(!txtNewsText.match(/\S/)) {    	
	    	jQuery('#txtEndDateValid').focus();
	    	jQuery('#dvEndDateValid').addClass('has-error');
	    	jQuery('#mrkEndDateValid').show();
	        return false;
	    }
	    
	    $('input[name="cbDriver"]:checked').each(function() {
			checkVal.push(this.value);
		});
	    
	    $.ajax({
	        url:'${pageContext.request.contextPath}/News',	
	        type:'POST',
	        enctype: 'multipart/form-data',
	        data:{"key":lParambtn,"txtNewsID":txtNewsID,"txtStartDateValid":txtStartDateValid,"txtEndDateValid":txtEndDateValid,
	        	  "txtNewsText":txtNewsText, "txtNewsImage":txtNewsImage,"listDriver":checkVal},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertNews')
	        	{
	        		jQuery("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal menambahkan daily news";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		jQuery("#txtStartDateValid").focus();
	        		jQuery("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateNews')
	        	{
	        		jQuery("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui daily news";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		jQuery("#txtStartDateValid").focus();
	        		jQuery("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
		        	var url = '${pageContext.request.contextPath}/News';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    
	    FuncClear();
	    
	    return true;
	}
	
	//IMAGE MODAL POP UP
 	// Get the modal
 	var modal = document.getElementById('myModal');

 	// Get the image and insert it inside the modal - use its "alt" text as a caption
 	var img = document.getElementById('myImg');
 	var modalImg = document.getElementById("img01");
 	var captionText = document.getElementById("caption");
 	img.onclick = function(){
 	    modal.style.display = "block";
 	    modalImg.src = this.src;
 	    captionText.innerHTML = this.alt;
 	}
 	
 	// Get the <span> element that closes the modal
 	var span = document.getElementsByClassName("closespanimage")[0];

 	// When the user clicks on <span> (x), close the modal
 	span.onclick = function() { 
 	  modal.style.display = "none";
 	}
</script>

</body>
</html>