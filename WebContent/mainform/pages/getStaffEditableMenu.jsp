<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<label for="lblMenuLeverage" class="control-label">Editable Menu :</label>

<div class="panel panel-primary" id="EditableMenuPanel"
	style="overflow: auto; height: 350px;">
	<div class="panel-body fixed-panel">
	
		<c:forEach items="${listEditableMenu}" var="crudmenu">
			<div class="checkbox" id="cbmenu">
				<label><input type="checkbox"
					name="cbEditableMenu"
					id="<c:out value="${crudmenu.menuID}" />"
					value="<c:out value="${crudmenu.menuID}" />">
					<c:out value="${crudmenu.menuName}" /></label>
			</div>
		</c:forEach>

	</div>
</div>