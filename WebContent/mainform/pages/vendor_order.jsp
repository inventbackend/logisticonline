<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<link rel="icon" href="mainform/image/logistic_icon3.jpg">

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Trucker Order</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
	<style type="text/css">	#ModalUpdateInsert { overflow-y:scroll }</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Form_Vendor_Order" name="Form_Vendor_Order" action = "${pageContext.request.contextPath}/VendorOrderList" method="post">
	<input type="hidden" id="temp_string" name="temp_string" value="" />
	<div class="wrapper">
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Trucker Order Management</h1>
			</section>
			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
						<c:if test="${condition == 'SuccessCancelVendorOrder'}">
					  		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  				  	<h4><i class="icon fa fa-check"></i> Success</h4>
			     			  	Sukses membatalkan vendor order.
			   				</div>
						</c:if>
						<c:if test="${condition == 'FailedCancelVendorOrder'}">
							<div class="alert alert-danger alert-dismissible">
			      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
			      				Gagal membatalkan vendor order. <c:out value="${conditionDescription}"/>.
		    				</div>
						</c:if>
						
						<div id="dvSearch" class="row">	
		<div class="col-md-2">
		<label for="message-text" class="control-label">Order Type</label>
			<select id="slOrderType" name="slOrderType" class="form-control">
				<c:if test="${orderType eq 'EXPORT'}">
					<option selected>EXPORT</option>
					<option>IMPORT</option>
				</c:if>
				<c:if test="${orderType eq 'IMPORT'}">
					<option>EXPORT</option>
					<option selected>IMPORT</option>
				</c:if>
       		</select>
       	</div>
		 <!-- /.col-md-2 -->
		 
		 <div class="col-md-2">
			<label for="message-text" class="control-label">Stuffing Date From</label>
			<input type="text" class="form-control" id="txtStuffingDateFrom" name="txtStuffingDateFrom" value="<c:out value="${stuffingDateFrom}"/>">
       	</div>
		<!-- /.col-md-2 -->
		<div class="col-md-2">
			<label for="message-text" class="control-label">Stuffing Date To</label>
			<input type="text" class="form-control" id="txtStuffingDateTo" name="txtStuffingDateTo" value="<c:out value="${stuffingDateTo}"/>">
     	</div>
		<!-- /.col-md-2 -->
		
		<div class="col-md-2">
		<button id="btnSearch" name="btnSearch" type="button" class="btn btn-primary" onclick="FuncButtonSearch('search')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-2 -->
		</div>
		<!-- /.row -->
       	
      					<!--modal update & Insert -->
						<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
									</div>
									
	   								<div class="modal-body">
	   									<div class="row">
	   										<div id="dvVendorOrderID" class="form-group col-md-4">
	         									<label for="lblVendorOrderID" class="control-label">Trucker Order ID</label><label id="mrkVendorOrderID" for="recipient-name" class="control-label"></label>	
	         									<input type="text" class="form-control" id="txtVendorOrderID" name="txtVendorOrderID" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvOrderManagementID" class="form-group col-md-4">
		         								<label for="lblOrderManagementID" class="control-label">Customer Order ID</label><label id="mrkOrderManagementID" for="recipient-name" class="control-label"></label>	
		         								<input type="text" class="form-control" id="txtOrderManagementID" name="txtOrderManagementID" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvSI" class="form-group col-md-4">
		         								<label for="lblSI" class="control-label">SI No</label>	
		         								<input type="text" class="form-control" id="txtSI" name="txtSI" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvSL" class="form-group col-md-6">
		         								<label for="lblShippingLine" class="control-label">Shipping Line</label>	
		         								<input type="text" class="form-control" id="txtSL" name="txtSL" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvPort" class="form-group col-md-6">
		         								<label for="lblPort" class="control-label">Port</label>	
		         								<input type="text" class="form-control" id="txtPort" name="txtPort" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvDeliveryOrderID" class="form-group" style="display:none;">
		         								<label for="lblOrderManagementDetailID" class="control-label">Order Management Detail ID</label><label id="mrkOrderManagementDetailID" for="recipient-name" class="control-label"></label>	
		         								<input type="text" class="form-control" id="txtOrderManagementDetailID" name="txtOrderManagementDetailID" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvCustomerID" class="form-group col-md-6">
		       									<label for="lblVendorID" class="control-label">Trucker ID</label>
												<small>
													<label id="lblVendorName" name="lblVendorName" class="control-label"></label>
												</small>
												<input type="text" class="form-control" id="txtVendorID" name="txtVendorID" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvVendorStatus" class="form-group col-md-6">
		         								<label for="lblVendorStatus" class="control-label">Trucker Status</label>	
		         								<input type="text" class="form-control" id="txtVendorStatus" name="txtVendorStatus" readonly="readonly" disabled="disabled">
		       								</div>
		       								
		       								<div id="dvQuantity" class="form-group col-md-6">
		         								<label for="lblQuantity" class="control-label">Party</label><label id="mrkQuantity" for="recipient-name" class="control-label"></label>	
		         								<input type="text" class="form-control" id="txtQuantity" name="txtQuantity" readonly="readonly" disabled="disabled">
		       								</div>
		       								<div id="dvPrice" class="form-group col-md-6">
		         								<label for="lblPrice" class="control-label">Rate</label><label id="mrkTotalPrice" for="recipient-name" class="control-label"></label>	
		         								<input type="text" class="form-control" id="txtPrice" name="txtTotalPrice" readonly="readonly" disabled="disabled">
		       								</div>
	   									</div>
	  								</div>
	  								<div class="modal-footer">
	   									<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
	  								</div>
								</div>
							</div>
						</div>
									
						<!--modal Cancel -->
						<div class="example-modal">
							<div class="modal modal-default" id="ModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
   										<div class="modal-header">
	 										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	   										<span aria-hidden="true">&times;</span></button>
	 										<h4 class="modal-title">Alert Cancel Vendor Order</h4>
   										</div>
       									<div class="modal-body">
       										<input type="hidden" id="temp_txtVendorOrderID" name="temp_txtVendorOrderID" >
       										<input type="hidden" id="temp_txtOrderID" name="temp_txtOrderID" >
       										<input type="hidden" id="temp_txtOrderDetailID" name="temp_txtOrderDetailID" >
       	 									<p>Are you sure to cancel this vendor order ?</p>
       									</div>
						              <div class="modal-footer">
						              	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
						                <button type="submit" id="btnCancel" name="btnCancel"  class="btn btn-danger" ><i class="fa fa-ban"></i> Yes</button>
						              </div>
					            </div>
					            <!-- /.modal-content -->
					          </div>
					          <!-- /.modal-dialog -->
					        </div>
					        <!-- /.modal -->
					      </div>
					      <br><br>
					      
<%-- 					    <c:if test="${globalUserRole eq 'R003'}"> --%>
							<!-- dashboard atas -->
<!-- 							<div class="row"> -->
<%-- 								<c:forEach items="${listDashboard}" var ="component"> --%>
<!-- 					 				<div class="col-md-3 col-sm-6 col-xs-12"> -->
<!-- 							          <div class="info-box"> -->
<!-- 							            <span class="info-box-icon" style="background-color: #005292;"> -->
<!-- 							            	<i><img style="margin-top: 10px;" -->
<!-- 							            			width="55px" height="55px"  -->
<!-- 							            			src="mainform/dist/img/order/Group 4275@2x.png"> -->
<!-- 							            	</i> -->
<!-- 							            </span> -->
							
<!-- 							            <div class="info-box-content"> -->
<%-- 							              <span class="info-box-text" style="color: black; font-weight: normal;"><c:out value="${component.boxTitle}" /></span> --%>
<!-- 							              <span class="info-box-number" style="margin-top:12px; font-size: 20px;"> -->
<%-- 							              	<c:out value="${component.boxValue}" /> --%>
<!-- 							              </span> -->
<!-- 							            </div> -->
<!-- 							            /.info-box-content -->
<!-- 							          </div> -->
<!-- 							          /.info-box -->
<!-- 							        </div> -->
<!-- 							        /.col -->
<%-- 					 			</c:forEach> --%>
<!-- 							</div> -->
							<!-- akhir dari dashboard atas -->
<%-- 						</c:if> --%>
							
							<%-- <button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button> --%>
							<table id="tb_vendor_order" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
									<!--<th>Vendor Order ID</th> -->
									<!--<th>Order ID</th> -->
									<!--<th>Order Detail ID</th> -->
										<th style="width:60px;"></th>
										<th>SI</th>
										<th>DO</th>
										<th>Stuffing Date</th>
										<th>Customer</th>
										<th>Factory</th>
										<th>District</th>
										<th>Qty</th>
										<th>Size Container</th>
										<th>Commodity</th>
										<th>Depo</th>
										<th>Port</th>
										<c:if test="${globalUserRole ne 'R003'}">
											<th>Trucker</th>
										</c:if>
										<th>Status</th>
										<th>Type</th>
										<!-- <th>Quantity Delivered</th> -->
										<!-- <th>Rate</th> -->
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listVendorOrder}" var="order">
										<tr>
											<%--<td><c:out value="${order.vendorOrderID}" /></td> --%>
											<%--<td><c:out value="${order.orderManagementID}" /></td> --%>
											<%--<td><c:out value="${order.orderManagementDetailID}" /></td> --%>
											<td>
												<table>
													<c:if test="${buttonstatus == 'none'}">
														<c:if test="${globalUserRole eq 'R001'}">
															<td><a href="${pageContext.request.contextPath}/VendorOrderDetailList
																?vendorOrderID=<c:out value="${order.vendorOrderID}" />
																&stuffingDate=<c:out value="${order.orderStuffingDate}" />
																&orderType=<c:out value="${order.orderType}" />
																&item=<c:out value="${order.item}" />
																&customer=<c:out value="${order.customer}" />
																&district=<c:out value="${order.district}" />
																&depo=<c:out value="${order.depo}" />
																&port=<c:out value="${order.port}" />
																&orderQty=<c:out value="${order.quantity}" />
																&order=<c:out value="${order.orderManagementID}" />
																&orderDetail=<c:out value="${order.orderManagementDetailID}" />
																&address=<c:out value="${order.address}" />
																&noSI=<c:out value="${order.shippingInvoiceID}" />
																&factory=<c:out value="${order.factory}" />
																"
																>
																<button title="see/assign driver" type="button" id="btnVendorOrderDetail" name="btnVendorOrderDetail"  class="btn btn-warning"><i class="fa fa-edit"></i></button></a>
															</td>
														</c:if>
														<c:if test="${globalUserRole ne 'R001'}">
															<td><button title="see/assign driver" style="display: <c:out value="${buttonstatus}"/>" type="button" id="btnVendorOrderDetail" name="btnVendorOrderDetail"  class="btn btn-warning"><i class="fa fa-edit"></i></button></td>
														</c:if>
														<td>&nbsp</td>
													</c:if>
													<c:if test="${buttonstatus != 'none'}">
														<td><a href="${pageContext.request.contextPath}/VendorOrderDetailList
														?vendorOrderID=<c:out value="${order.vendorOrderID}" />
														&stuffingDate=<c:out value="${order.orderStuffingDate}" />
														&orderType=<c:out value="${order.orderType}" />
														&item=<c:out value="${order.item}" />
														&customer=<c:out value="${order.customer}" />
														&district=<c:out value="${order.district}" />
														&depo=<c:out value="${order.depo}" />
														&port=<c:out value="${order.port}" />
														&orderQty=<c:out value="${order.quantity}" />
														&order=<c:out value="${order.orderManagementID}" />
														&orderDetail=<c:out value="${order.orderManagementDetailID}" />
														&address=<c:out value="${order.address}" />
														&noSI=<c:out value="${order.shippingInvoiceID}" />
														&factory=<c:out value="${order.factory}" />
														"
														>
														<button title="see/assign driver" type="button" id="btnVendorOrderDetail" name="btnVendorOrderDetail"  class="btn btn-warning"><i class="fa fa-edit"></i></button></a></td>
														<td>&nbsp</td>
													</c:if>
													<td>
														<c:if test="${globalUserRole eq 'R001'}">
															<button id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
																title="see order id"
																onclick="FuncButtonUpdate()"
																data-target="#ModalUpdateInsert" 
																data-lvendororderid='<c:out value="${order.vendorOrderID}" />'
																data-lordermanagementid='<c:out value="${order.orderManagementID}" />'
																data-lordermanagementdetailid='<c:out value="${order.orderManagementDetailID}" />'
																data-lvendorid='<c:out value="${order.vendorID}" />'
																data-lvendorname='<c:out value="${order.vendorName}" />'
																data-lvendorstatus='<c:out value="${order.vendorStatus}" />'
																data-lquantity='<c:out value="${order.quantity}" />'
																data-lprice='<c:out value="${order.price}" />'
																data-lshippingline='<c:out value="${order.shippingLine}" />'
																data-lsi='<c:out value="${order.shippingInvoiceID}" />'
																data-lport='<c:out value="${order.port}" />'
																>
																<i class="fa fa-eye"></i></button>
														</c:if>
														<c:if test="${globalUserRole ne 'R001'}">
															<button style="display: <c:out value="${buttonstatus}"/>" id="btnModalUpdate" name="btnModalUpdate" type="button" class="btn btn-info" data-toggle="modal" 
																title="see order id"
																onclick="FuncButtonUpdate()"
																data-target="#ModalUpdateInsert" 
																data-lvendororderid='<c:out value="${order.vendorOrderID}" />'
																data-lordermanagementid='<c:out value="${order.orderManagementID}" />'
																data-lordermanagementdetailid='<c:out value="${order.orderManagementDetailID}" />'
																data-lvendorid='<c:out value="${order.vendorID}" />'
																data-lvendorname='<c:out value="${order.vendorName}" />'
																data-lvendorstatus='<c:out value="${order.vendorStatus}" />'
																data-lquantity='<c:out value="${order.quantity}" />'
																data-lprice='<c:out value="${order.price}" />'
																data-lshippingline='<c:out value="${order.shippingLine}" />'
																data-lsi='<c:out value="${order.shippingInvoiceID}" />'
																data-lport='<c:out value="${order.port}" />'
																>
																<i class="fa fa-eye"></i></button>
														</c:if>
													</td>
													<c:if test="${order.vendorStatus eq 0 || order.vendorStatus eq 1}">
														<td>&nbsp</td>
														<td>
															<button style="display: <c:out value="${buttonstatus}"/>" id="btnModalCancel" name="btnModalCancel" type="button" class="btn btn-danger" data-toggle="modal" 
																data-target="#ModalCancel" 
																data-lvendororderid='<c:out value="${order.vendorOrderID}" />'
																data-lorderid='<c:out value="${order.orderManagementID}" />'
																data-lorderdetailid='<c:out value="${order.orderManagementDetailID}" />' 
																title="cancel">
																<i class="fa fa-ban"></i>
															</button>
														</td>
													</c:if>
												</table>
											</td>
											<td><c:out value="${order.shippingInvoiceID}" /></td>
											<td><c:out value="${order.deliveryOrderID}" /></td>
											<td><c:out value="${order.orderStuffingDate}" /></td>
											<td><c:out value="${order.customer}" /></td>
											<td><c:out value="${order.factory}" /></td>
											<td><c:out value="${order.district}" /></td>
											<td><c:out value="${order.quantity}" /></td>
											<td><c:out value="${order.containerType}" /></td>
											<td><c:out value="${order.item}" /></td>
											<td><c:out value="${order.depo}" /></td>
											<td><c:out value="${order.port}" /></td>
											<c:if test="${globalUserRole ne 'R003'}">	
												<td><c:out value="${order.vendorName}" /></td>
											</c:if>
											<td>
												<c:if test="${order.vendorStatus == 0}">Picked</c:if>
												<c:if test="${order.vendorStatus == 1}">Assigned</c:if>
												<c:if test="${order.vendorStatus == 2}">On Process</c:if>
												<c:if test="${order.vendorStatus == 3}">Delivered</c:if>
												<c:if test="${order.vendorStatus == 10}">Cancelled</c:if>
											</td>
											<td><c:out value="${order.orderType}" /></td>
											<%-- <td><c:out value="${order.quantityDelivered}" /></td> --%>
											<!-- <td> -->
											<%-- <fmt:setLocale value="id_ID"/> --%>
											<%-- <fmt:formatNumber value="${order.price}" type="currency"/> --%>
											<!-- </td> -->
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="/mainform/pages/master_footer.jsp"%>
	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	
	
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>	

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
  		$("#tb_vendor_order").DataTable({"aaSorting": [],"scrollX": true});
  		$('#M018').addClass('active');
  		$('#M020').addClass('active');
  		
  		//calendar java script
		$('#txtStuffingDateFrom').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    $('#txtStuffingDateTo').datepicker({
  	      format: 'dd M yyyy',
  	      autoclose: true
  	    });
  	    
  		$("#dvErrorAlert").hide();
  	});

	</script>
    
<script>
	$('#ModalCancel').on('shown.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lvendororderid = button.data('lvendororderid');
		var lorderid = button.data('lorderid');
		var lorderdetailid = button.data('lorderdetailid');
		$("#temp_txtVendorOrderID").val(lvendororderid);
		$("#temp_txtOrderID").val(lorderid);
		$("#temp_txtOrderDetailID").val(lorderdetailid);
	});
</script>

<script>

function FuncButtonUpdate() {
	document.getElementById('lblTitleModal').innerHTML = 'View Trucker Order';
		
	$('#ModalUpdateInsert').on('show.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
															
 		var button = $(event.relatedTarget);
 		var lvendororderid = button.data('lvendororderid');
 		var lordermanagementid = button.data('lordermanagementid');
 		var lordermanagementdetailid = button.data('lordermanagementdetailid');
 		var lvendorid = button.data('lvendorid');
 		var lvendorname = button.data('lvendorname') == undefined ? '' : button.data('lvendorname');
 		var lvendorstatus = button.data('lvendorstatus');
 		var lquantity = button.data('lquantity');
 		var lprice = button.data('lprice');
 		var lshippingline = button.data('lshippingline');
 		var lsi = button.data('lsi');
 		var lport = button.data('lport');

 		var modal = $(this);
 		modal.find(".modal-body #txtVendorOrderID").val(lvendororderid);
 		modal.find(".modal-body #txtOrderManagementID").val(lordermanagementid);
 		modal.find(".modal-body #txtOrderManagementDetailID").val(lordermanagementdetailid);
 		var statusString = "";
 		if (lvendorstatus == '0'){
 			statusString = "Picked";
 		}else if(lvendorstatus == '1'){
 			statusString = "Assigned";
 		}else if(lvendorstatus == '2'){
 			statusString = "On Process";
 		}
 		else if(lvendorstatus == '3'){
 			statusString = "Delivered";
 		}
 		else if(lvendorstatus == '10'){
 			statusString = "Cancelled";
 		}
 		modal.find(".modal-body #txtVendorStatus").val(statusString);
 		modal.find(".modal-body #txtVendorID").val(lvendorid);
 		modal.find(".modal-body #txtQuantity").val(lquantity);
 		modal.find(".modal-body #txtPrice").val(lprice);
 		modal.find(".modal-body #txtSL").val(lshippingline);
 		modal.find(".modal-body #txtSI").val(lsi);
 		modal.find(".modal-body #txtPort").val(lport);
 		
 		document.getElementById('lblVendorName').innerHTML = '(' + lvendorname + ')';
	});
}

function FuncButtonSearch(lParambtn) {
 	var OrderType = document.getElementById('slOrderType').value;
 	var StuffingDateFrom = document.getElementById('txtStuffingDateFrom').value;
	var StuffingDateTo = document.getElementById('txtStuffingDateTo').value;
 	
 	jQuery.ajax({
	        url:'${pageContext.request.contextPath}/VendorOrderList',	
	        type:'POST',
	        data:{"key":lParambtn,"ordertype":OrderType,"stuffingdatefrom":StuffingDateFrom, "stuffingdateto":StuffingDateTo},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	var url = '${pageContext.request.contextPath}/VendorOrderList';  
 	        	$(location).attr('href', url);
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}
</script>

</body>

</html>