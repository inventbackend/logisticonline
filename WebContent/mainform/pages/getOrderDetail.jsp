<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_schedule" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Order Detail ID</th>
			<th>Stuffing Date</th>
			<th>Qty</th>
			<th>Cont. Type</th>
			<th>Status</th>
			<th style="width: 103px"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listOrderManagementDetail}" var ="orderdetail">
			<tr>
				<td><c:out value="${orderdetail.orderManagementDetailID}" /></td>
				<td><c:out value="${orderdetail.stuffingDate}" /></td>
				<td><c:out value="${orderdetail.quantity}" /></td>
				<td><c:out value="${orderdetail.containerTypeDescription}" /></td>
				<td>
					<c:if test="${orderdetail.orderStatus == 0}">New</c:if>
					<c:if test="${orderdetail.orderStatus == 1}">Committed</c:if>
					<c:if test="${orderdetail.orderStatus == 2}">Received</c:if>
					<c:if test="${orderdetail.orderStatus == 3}">On Process</c:if>
					<c:if test="${orderdetail.orderStatus == 4}">Finished</c:if>
					<c:if test="${orderdetail.orderStatus == 10}">Cancelled</c:if>
				</td>
				<td>
					<button
							type="button" class="btn btn-default"
							title="show vendor"
							data-toggle="modal" 
				        	data-target="#ModalVendor"
				        	data-lordermanagementdetailid='<c:out value="${orderdetail.orderManagementDetailID}"/>'>
				        	<i class="fa fa-truck"></i>
					</button>
					<c:if test="${orderStatus eq '0' && userRole ne 'R001'}">
						<button
							type="button" class="btn btn-info" title="edit"
							onclick="FuncUpdateSchedule('<c:out value="${orderdetail.orderManagementDetailID}"/>',
													'<c:out value="${orderdetail.stuffingDate}"/>',
													'<c:out value="${orderdetail.containerTypeID}"/>',
													'<c:out value="${orderdetail.quantity}"/>')">
								<i class="fa fa-edit"></i>
						</button>
						<button type="button" class="btn btn-danger" title="delete"
								data-toggle="modal" data-target="#ModalDeleteSchedule" 
				        		data-lordermanagementid='<c:out value="${orderManagementID}"/>'
				        		data-lordermanagementdetailid='<c:out value="${orderdetail.orderManagementDetailID}"/>'>
				        		<i class="fa fa-trash"></i>
				        </button>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_schedule").DataTable();
  	});
</script>