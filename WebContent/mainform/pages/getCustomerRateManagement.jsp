<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table id="tb_customer_rate_management" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Container Type</th>
			<th>Publish Rate</th>
			<th>Final Rate</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listRate}" var ="rate">
			<tr>
				<td><c:out value="${rate.containerTypeName}" /></td>
				<td>
					<fmt:setLocale value="id_ID"/>
					<fmt:formatNumber value="${rate.price}" type="currency"/>
				</td>
				<td>
					<fmt:setLocale value="id_ID"/>
					<fmt:formatNumber value="${rate.finalPrice}" type="currency"/>
				</td>
				<td>
					<button
							type="button" class="btn btn-info" title="edit"
							data-toggle="modal"
							data-target="#ModalUpdateInsertRate"
							data-lcontainertypeid='<c:out value="${rate.containerTypeID}"/>'
							data-lfinalrate='<c:out value="${rate.finalPrice}"/>'
							><i class="fa fa-edit"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_customer_rate_management").DataTable({"aaSorting": [],"scrollX": true});
  	});
</script>