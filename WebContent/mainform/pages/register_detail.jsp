<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Register Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<form id="RegisterDetail" name="RegisterDetail" action = "${pageContext.request.contextPath}/RegisterDetail" method="post">
<input  type="hidden" id="temp_custid" name="temp_custid" value="<c:out value="${temp_customerID}"/>" />

	<div class="wrapper">

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				Add Factory <br> <br> <small style="color: black; font-weight: bold;">Customer Name : <c:out value="${temp_customerName}"/></small>
			</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-body">
						
						<!-- declare docNo from servlet -->
						<%-- <c:set var="docNo" value="${docNo}"></c:set> --%>
						
							<c:if test="${condition == 'SuccessRegisterCustomer'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Registration success. Please register your factory if available.
              				</div>
	      					</c:if>  
						
							<c:if test="${condition == 'SuccessRegisterFactory'}">
	    					  <div class="alert alert-success alert-dismissible">
          				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             				  	<h4><i class="icon fa fa-check"></i> Success</h4>
                			  	Factory registration success.
              				</div>
	      					</c:if> 
	      					
	      					<c:if test="${condition == 'SuccessUpdateFactory'}">
					 		<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  Update success.
					 		</div>
							</c:if>
					
							<c:if test="${condition == 'SuccessDeleteFactory'}">
							 	<div class="alert alert-success alert-dismissible">
						      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									  	<h4><i class="icon fa fa-check"></i> Success</h4>
					   			  	Delete success.
					 			</div>
							</c:if>
						
							<c:if test="${condition == 'FailedDeleteFactory'}">
								<div class="alert alert-danger alert-dismissible">
				    				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				    				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				    				Delete failed. <c:out value="${conditionDescription}"/>.
				  				</div>
							</c:if>
	      					
	      						<!--modal update & Insert -->
									<div class="modal fade" id="ModalUpdateInsert" role="dialog" aria-labelledby="exampleModalLabel">
 										<div class="modal-dialog" role="document">
    										<div class="modal-content">
    										
    										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
						          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					     					</div>
					     					
      											<div class="modal-header">
        											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        											<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModal" name="lblTitleModal"></label></h4>	
        												
        											
      											</div>
	      								<div class="modal-body">
	        								
	          								<div id="dvFactoryName" class="form-group">
	            								<label for="recipient-name" class="control-label">Factory Name</label><label id="mrkFactoryName" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtFactoryName" name="txtFactoryName">
	            								<input type="hidden" class="form-control" id="txtFactoryID" name="txtFactoryID">
	          								</div>
	          								<div id="dvAddress" class="form-group">
	            								<label for="message-text" class="control-label">Address</label><label id="mrkAddress" for="recipient-name" class="control-label"><small>*</small></label>	
	            								<input type="text" class="form-control" id="txtAddress" name="txtAddress">
	          								</div>
	          								<div id="dvDistrict" class="form-group">
		          								<label for="message-text" class="control-label">District</label><label id="mrkDistrict" for="recipient-name" class="control-label"><small>*</small></label>
										 		<select id="slDistrict" name="slDistrict" class="form-control select2" style="width: 100%;">
													<c:forEach items="${listDistrict}" var="district">
														<option value="<c:out value="${district.districtID}" />"><c:out value="${district.districtID}" /> - <c:out value="${district.name}" /></option>
													</c:forEach>
										       	</select>
									      	</div>
									      	<div id="dvTier" class="form-group">
		          								<label for="message-text" class="control-label">Tier</label><label id="mrkTier" for="recipient-name" class="control-label"><small>*</small></label>
										 		<select id="slTier" name="slTier" class="form-control select2" style="width: 100%;">
													<c:forEach items="${listTier}" var="tier">
														<option value="<c:out value="${tier.tierID}" />"><c:out value="${tier.tierID}" /> - <c:out value="${tier.description}" /></option>
													</c:forEach>
										       	</select>
									      	</div>
      								</div>
      								
      								<div class="modal-footer">
        									<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
        									<button type="button" class="btn btn-primary" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
        									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        								
      								</div>
    										</div>
  										</div>
									</div>
									
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
							<div class="modal-header">            											
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Alert Delete Factory</h4>
							</div>
						<div class="modal-body">
						<input type="hidden" id="temp_txtCustomerID" name="temp_txtCustomerID"  />
						<input type="hidden" id="temp_txtFactoryID" name="temp_txtFactoryID"  />
									<p>Are you sure to delete this factory ?</p>
						</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		        <button type="submit" id="btnDelete" name="btnDelete"  class="btn btn-outline" >Delete</button>
		      </div>
		    </div>
		    <!-- /.modal-content -->
		  </div>
		  <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
						
							<button id="btnBack" name="btnBack" type="button" class="btn btn-primary pull-left" onclick="FuncButtonBack()"> Back to login</button>
							<button id="btnModalNew" name="btnModalNew" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> New</button>
							<br><br>
							<table id="tb_factory" class="table table-bordered table-striped table-hover">
								<thead style="background-color: #d2d6de;">
									<tr>
										<th>Factory Name</th>
										<th>Address</th>
										<th>District</th>
										<th>Description</th>
										<th style="width: 60px"></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listFactory}" var="factory">
										<tr>
											<td><c:out value="${factory.factoryName}" /></td>
											<td><c:out value="${factory.address}" /></td>
											<td><c:out value="${factory.districtName}" /></td>
											<td><c:out value="${factory.tierName}" /></td>
											<td>
												<button
														type="button" class="btn btn-info"
														data-toggle="modal"
														onclick="FuncButtonUpdate()" 
														data-target="#ModalUpdateInsert"
														data-lfactoryid='<c:out value="${factory.factoryID}"/>'
														data-lfactoryname='<c:out value="${factory.factoryName}"/>'
														data-laddress='<c:out value="${factory.address}"/>' 
														data-ldistrictid='<c:out value="${factory.districtID}"/>'
														data-ltierid='<c:out value="${factory.tierID}"/>' 
														><i class="fa fa-edit"></i></button>
										        <button type="button" class="btn btn-danger" data-toggle="modal" 
										        		data-target="#ModalDelete" data-lfactoryid='<c:out value="${factory.factoryID}"/>'>
										        		<i class="fa fa-trash"></i>
										        </button>
									        </td>
										</tr>

									</c:forEach>
								
								</tbody>
							</table>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row --> 
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="/mainform/pages/master_footer.jsp"%>

	</div>
	<!-- ./wrapper -->
</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- bootstrap datepicker -->
	<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<!-- $("#tb_itemlib").DataTable(); -->
	<!-- page script -->
	<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
		
  		$("#tb_factory").DataTable();
  		$("#dvErrorAlert").hide();
  	});
 	
 	//shortcut for button 'new'
    Mousetrap.bind('n', function() {
    	FuncButtonNew(),
    	$('#ModalUpdateInsert').modal('show')
    	});
	</script>

<script>
	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
		$("#dvErrorAlert").hide();
	
 		var button = $(event.relatedTarget);
 		var lFactoryID = button.data('lfactoryid');
 		var lFactoryName = button.data('lfactoryname');
 		var lAddress = button.data('laddress');
 		var lDistrictID = button.data('ldistrictid');
 		var lTierID = button.data('ltierid');
 		
 		var modal = $(this);
 		
 		modal.find(".modal-body #txtFactoryID").val(lFactoryID);
		modal.find(".modal-body #txtFactoryName").val(lFactoryName);
 		modal.find(".modal-body #txtAddress").val(lAddress);
 		modal.find(".modal-body #slDistrict").val(lDistrictID).trigger("change");
 		modal.find(".modal-body #slTier").val(lTierID).trigger("change");
 		
 		$('#txtFactoryName').focus();
	})
	
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lCustomerID = document.getElementById('temp_custid').value;
		var lFactoryID = button.data('lfactoryid');
		$("#temp_txtCustomerID").val(lCustomerID);
		$("#temp_txtFactoryID").val(lFactoryID);
	})
</script>

<script>
function FuncClear(){
	$('#mrkFactoryName').hide();
	$('#mrkAddress').hide();
	$('#mrkDistrict').hide();
	$('#mrkTier').hide();
	
	$('#dvFactoryName').removeClass('has-error');
	$('#dvAddress').removeClass('has-error');
	$('#dvDistrict').removeClass('has-error');
	$('#dvTier').removeClass('has-error');
	
	$("#dvErrorAlert").hide();
}

function FuncButtonNew() {
	
	$('#txtFactoryName').val('');
	$('#txtAddress').val('');
	$('#slDistrict').val('').trigger("change");
 	$('#slTier').val('').trigger("change");
	
	$('#btnSave').show();
	$('#btnUpdate').hide();
	document.getElementById("lblTitleModal").innerHTML = "Add Factory";

	FuncClear();
}

function FuncButtonUpdate() {
	$('#btnSave').hide();
	$('#btnUpdate').show();
	document.getElementById('lblTitleModal').innerHTML = 'Edit Factory';
	
	FuncClear();
}

function FuncValEmptyInput(lParambtn) {
	var txtCustomerID = document.getElementById('temp_custid').value;
	var txtFactoryID = document.getElementById('txtFactoryID').value;
	var txtFactoryName = document.getElementById('txtFactoryName').value;
	var txtAddress = document.getElementById('txtAddress').value;
	var slDistrict = document.getElementById('slDistrict').value;
	var slTier = document.getElementById('slTier').value;

    if(!txtFactoryName.match(/\S/)) {
    	$("#txtFactoryName").focus();
    	$('#dvFactoryName').addClass('has-error');
    	$('#mrkFactoryName').show();
        return false;
    } 
    
    if(!txtAddress.match(/\S/)) {    	
    	$('#txtAddress').focus();
    	$('#dvAddress').addClass('has-error');
    	$('#mrkAddress').show();
        return false;
    } 
    
    if(!slDistrict.match(/\S/)) {
    	$('#slDistrict').focus();
    	$('#dvDistrict').addClass('has-error');
    	$('#mrkDistrict').show();
        return false;
    }
    
    if(!slTier.match(/\S/)) {
    	$('#slTier').focus();
    	$('#dvTier').addClass('has-error');
    	$('#mrkTier').show();
        return false;
    }
    
    jQuery.ajax({
        url:'${pageContext.request.contextPath}/RegisterDetail',	
        type:'POST',
        data:{"key":lParambtn,"txtCustomerID":txtCustomerID,"txtFactoryID":txtFactoryID,"txtFactoryName":txtFactoryName, "txtAddress":txtAddress, 
        		"slDistrict":slDistrict, "slTier":slTier},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	if(data.split("--")[0] == 'FailedRegisterFactory')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Factory registration failed";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
        		$("#txtFactoryName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else if(data.split("--")[0] == 'FailedUpdateFactory')
        	{
        		$("#dvErrorAlert").show();
        		document.getElementById("lblAlert").innerHTML = "Update failed";
        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
				$("#txtFactoryName").focus();
        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
        		return false;
        	}
        	else
        	{
	        	var url = '${pageContext.request.contextPath}/RegisterDetail';  
	        	$(location).attr('href', url);
        	}
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
    
    FuncClear();
    
    return true;
}

function FuncButtonBack(){
	alert("You have finished your registration, we will send you an email if your account has been activated");
	
	var url = '${pageContext.request.contextPath}/Login';  
	$(location).attr('href', url);
}
</script>

<script>
$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slDistrict:focus').length) {
    	$('#slDistrict').click();
    }
});

$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slTier:focus').length) {
    	$('#slTier').click();
    }
});

// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}
</script>

</body>
</html>