<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	
</head>

<body class="hold-transition login-page">
<form action="${pageContext.request.contextPath}/RegisterVendor" method="post" onsubmit="return checkPassword(this);">

<div class="login-box" style="width:700px">
  
  <div class="login-box-body" style="width:700px; margin-top:-10%;">
  
  <div class="login-logo">
    <img src="mainform/image/logo.png" style="width:50%;height:50%;"><br>
	<!-- <b>Logistic Online</b> -->
  </div>
  
			<c:if test="${condition == 'FailedRegisterVendor'}">
					<div class="alert alert-danger alert-dismissible">
	      				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	      				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	      				Failed register vendor. <c:out value="${conditionDescription}"/>.
    				</div>
			</c:if>
			
	<!-- modal show tier info -->
	<div class="modal fade" id="ModalTierInfo" tabindex="-1" role="dialog" aria-labelledby="ModalTierInfoLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
					     					
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="exampleModalLabel"><label>Tier Description</label></h4>
		      </div>
		      <div class="modal-body">
		          <label id="tierInfo"></label>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
		      </div>
		    </div>
		  </div>
		</div>
			
    <p class="login-box-msg"><b>FORM REGISTER</b></p>
    <div class="row">
      <div class="form-group col-xs-4">
      	<label class="control-label">Trucker Name:</label>
        <input type="text" class="form-control" placeholder="Vendor Name" id="txtVendorName" name="txtVendorName" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">PIC:</label>
        <input type="text" class="form-control" placeholder="PIC" id="txtPIC" name="txtPIC" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      <label class="control-label">Email:</label>
        <input type="text" class="form-control" placeholder="Email" id="txtEmail" name="txtEmail" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      <label class="control-label">Province:</label>
       <select id="slProvince" name="slProvince" class="form-control select2" data-placeholder="Province" style="width: 100%;" required>
			<c:forEach items="${listProvince}" var="province">
				<option value="<c:out value="${province.provinceName}" />"><c:out value="${province.provinceName}" /></option>
			</c:forEach>
       	</select>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Address:</label>
        <input type="text" class="form-control" placeholder="Address" id="txtAddress" name="txtAddress" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Postal Code:</label>
        <input type="text" class="form-control" placeholder="Postal Code" id="txtPostalCode" name="txtPostalCode" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Garage Address:</label>
        <input type="text" class="form-control" placeholder="Garage Address" id="txtGarageAddress" name="txtGarageAddress" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Office Phone:</label>
        <input type="text" class="form-control" placeholder="Office Phone" id="txtOfficePhone" name="txtOfficePhone" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Mobile Phone:</label>
        <input type="text" class="form-control" placeholder="Mobile Phone" id="txtMobilePhone" name="txtMobilePhone">
      </div>
      <div class="form-group has-feedback col-xs-6">
      	<label class="control-label">NPWP:</label>
        <input type="text" class="form-control" placeholder="NPWP" id="txtNPWP" name="txtNPWP" required>
      </div>
      <div class="form-group has-feedback col-xs-6">
      	<label class="control-label">SIUJPT:</label>
        <input type="text" class="form-control" placeholder="SIUJPT" id="txtSIUJPT" name="txtSIUJPT" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Username:</label>
        <input type="text" class="form-control" placeholder="Username" id="txtUserName" name="txtUserName" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Password:</label>
        <input type="text" class="form-control" placeholder="Password" id="txtPassword" name="txtPassword" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Re-type password:</label>
        <input type="text" class="form-control" placeholder="Re-type Password" id="txtRePassword" name="txtRePassword" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Bank Name:</label>
        <input type="text" class="form-control" placeholder="Bank Name" id="txtBankName" name="txtBankName" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Account:</label>
        <input type="text" class="form-control" placeholder="Bank Account" id="txtBankAcc" name="txtBankAcc" required>
      </div>
      <div class="form-group has-feedback col-xs-4">
      	<label class="control-label">Branch:</label>
        <input type="text" class="form-control" placeholder="Bank Branch" id="txtBankBranch" name="txtBankBranch" required>
      </div>
      <div class="form-group has-feedback col-xs-12">
          <label class="control-label">Tier:</label>
       	  	<div class="panel panel-primary" id="TierPanel"
				style="overflow: auto; height: 120px;">
				<div class="panel-body fixed-panel">
				
					<div class="row">
						<c:forEach items="${listTier}" var="tier">
							<div class="col-xs-4" id="cbtier">
								<label><input type="checkbox"
									name="cbTier"
									id="<c:out value="${tier.tierID}" />"
									value="<c:out value="${tier.tierID}" />">
									<c:out value="${tier.description}" />
									<a href=""
										style="color: #144471; font-weight: bold;"
										data-toggle="modal" data-target="#ModalTierInfo"
										data-ltierinfo='<c:out value="${tier.longDescription}" />'
										> (see detail)
									</a>
								</label>
							</div>
						</c:forEach>
					</div>
	
				</div>
			</div>
        </div>
      </div>    
      
      <div class="row">
        <div class="col-xs-4">
        </div>
        <div class="col-xs-4">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="btnRegister">Register</button>
        </div>
        <!-- /.col -->
      </div>

	<!-- <a href="#">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<!-- InputMask -->
<script src="mainform/plugins/input-mask/jquery.inputmask.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="mainform/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    //Initialize Select2 Elements
	$(".select2").select2();
	$('#slProvince').val('').trigger("change");
    
    $(":input").inputmask();
  	$("#txtOfficePhone").inputmask({"mask": "999[9]-999999999999"});
  });
  
  //custom txtpassword
	$(document).ready(function(){
		$(document).on('focusout', '#txtPassword', function(e){
			 $(this).prop('type', 'password');
		});
		$(document).on('click', '#txtPassword', function(e){
			 $(this).prop('type', 'text');
		});
		
		$(document).on('focusout', '#txtRePassword', function(e){
			 $(this).prop('type', 'password');
		});
		$(document).on('click', '#txtRePassword', function(e){
			 $(this).prop('type', 'text');
		});
	});

//check password and repassword is match or not before submit
function checkPassword(theForm) {
	if (theForm.txtPassword.value != theForm.txtRePassword.value)
	{
		alert('Those passwords don\'t match!');
		return false;
	} else {
		return true;
	}
}

$('#ModalTierInfo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var lTierInfo = button.data('ltierinfo');
		document.getElementById("tierInfo").innerHTML = lTierInfo;
	})
	
//set tab as click
 	$(window).keyup(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 9 && $('#slProvince:focus').length) {
    	$('#slProvince').click();
    }
	});
	
// automaticaly open the select2 when it gets focus
jQuery(document).on('focus', '.select2', function() {
    jQuery(this).siblings('select').select2('open');
});

// when the select2 closes advance focus to the next field
jQuery(document).ready(function() {
    jQuery(".select2").select2().on("select2:close", function(e) {
        var nextId = getNextFocusableFieldId(jQuery(this).attr('id'));
        // set focus to the next field
        jQuery('#' + nextId).focus().select();
    });
});

// return the id of the next focusable field
function getNextFocusableFieldId(idIn) {
    var focusables = jQuery("input, select, textarea,button");
    var reachedId = false;
    var id = '';
    var nextId = '';
    jQuery.each(focusables, function(index, value) {
        id = jQuery(this).attr('id');
        // if we reached the id last time set the nextId and exit each
        if (reachedId) {
            nextId = id;
            return false;
        }
        // if the ids match set the flag for the next iteration
        if (id == idIn) {
            reachedId = true;
        }
    });
    return nextId;
}
</script>

</body>
</html>
