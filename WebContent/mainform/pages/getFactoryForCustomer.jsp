<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_master_factory" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>PIC</th>
			<th>Office</th>
			<th>Mobile</th>
			<th>Province</th>
			<th>Address</th>
			<th>District</th>
			<!-- <th>Tier</th> -->
			<c:if test="${userRole eq 'R001'}">
				<th style="width: 120px"></th>
			</c:if>
			<c:if test="${userRole eq 'R002'}">
				<th style="width: 60px"></th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listFactory}" var ="factory">
			<tr>
				<td><c:out value="${factory.factoryID}" /></td>
				<td><c:out value="${factory.factoryName}" /></td>
				<td><c:out value="${factory.pic}" /></td>
				<td><c:out value="${factory.officePhone}" /></td>
				<td><c:out value="${factory.mobilePhone}" /></td>
				<td><c:out value="${factory.province}" /></td>
				<td><c:out value="${factory.address}" /></td>
				<td><c:out value="${factory.districtName}" /></td>
				<%-- <td><c:out value="${factory.tierName}" /></td> --%>
				<td>
					<button
							type="button" class="btn btn-info" title="edit"
							data-toggle="modal"
							onclick="FuncUpdateFactory('<c:out value="${factory.province}" />')" 
							data-target="#ModalFactory"							
							data-lfactoryid='<c:out value="${factory.factoryID}"/>'
							data-lfactoryname='<c:out value="${factory.factoryName}"/>'
							data-lpic='<c:out value="${factory.pic}"/>'
							data-lofficephone='<c:out value="${factory.officePhone}"/>'
							data-lmobilephone='<c:out value="${factory.mobilePhone}"/>'
							data-lprovince='<c:out value="${factory.province}"/>'
							data-laddress='<c:out value="${factory.address}"/>' 
							data-ldistrictid='<c:out value="${factory.districtID}"/>'
							data-ltierid='<c:out value="${factory.tierID}"/>' 
							><i class="fa fa-edit"></i></button>
			        <button type="button" class="btn btn-danger" data-toggle="modal" title="delete"
			        		data-target="#ModalDeleteFactory" 
			        		data-lfactoryid='<c:out value="${factory.factoryID}"/>'
			        		>
			        		<i class="fa fa-trash"></i>
			        </button>
			        <c:if test="${userRole eq 'R001'}">
			        	<button
							type="button" class="btn btn-warning" title="rate management"
							data-toggle="modal"
							data-target="#ModalRateManagement"							
							data-lfactoryid='<c:out value="${factory.factoryID}"/>'
							data-ldistrictid='<c:out value="${factory.districtID}"/>'
							><i class="fa fa-dollar"></i></button>
			        </c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_master_factory").DataTable({"aaSorting": [],"scrollX": true});
  	});
</script>