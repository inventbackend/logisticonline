<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Dashboard Detail</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
<!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style type="text/css">
  
	@media (min-width: 992px) {
		.col-half-offset{
		  margin-left:4.166666667%
		}

	}
  	
  	
	.info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 54px;
    width: 54px;
    text-align: center;
    font-size: 45px;
    line-height: 90px;
    background: rgba(0,0,0,0.2);
    }
    
    .info-box {
    padding-top:5px;
    display: block;
    min-height: 66px;
    background: #fff;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 2px;
    margin-bottom: 0px;
	}
	
	.info-box-content {
    padding: 5px 10px;
    margin-left: 70px;
	}
  </style>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="Form_Dashboard" name="Form_Dashboard" action = "DashboardDetail" method="post">

<div class="wrapper">  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    	  
  	  	 <!-- Content Header (Page header) -->
	    <section class="content-header">
		<!-- 	      <h1> -->
		<!-- 	        Dashboard -->
		<!-- 	        <small>Control panel</small> -->
		<!-- 	      </h1> -->
			<div class="user-panel">
			 	<div class="pull-left image">
	          		<img src="mainform/dist/img/Group 4260@2x.png" alt="User Image">
	        	</div>
	        	<div class="pull-left info">
	          		<p style="color:#4E4E4E"><c:out value="${dateNow}"/></p>
	          		<p style="color:#4E4E4E"><c:out value="${timeNow}"/></p>
	        	</div>
	        </div>
	    </section>

    	<!-- Main content -->
	    <section class="content">
	    
	      	<!-- Info boxes -->
	      	
	      	<!--modal invoice setting -->
						<div class="modal fade" id="ModalInvoiceSetting" role="dialog" aria-labelledby="exampleModalLabel">
							<div class="modal-dialog modal-md" role="document">
								<div class="modal-content">
											
									<div id="dvErrorAlertInvoiceSetting" class="alert alert-danger alert-dismissible" style="display: none;">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlertInvoiceSetting"> </label>.
		     						</div>
		     					
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalInvoiceSetting" name="lblTitleModalInvoiceSetting"></label></h4>	
									</div>
									
	   								<div class="modal-body">
	   									<!-- loading bar -->
										<div id="dvloaderPartialPayment" class="loader" style="display:none;"></div>
										
	   									<div class="row">
	   										<div class="col-md-12">
	   											
	   											<table class="table">
												  <thead>
												  	<tr>
												  		<th>No.</th>
												        <th>Type</th>
												        <th>Image</th>
												  	</tr>
												  </thead>
												  <tbody id="tbody-driver-image">
												  </tbody>
												</table>
		   									</div>
	   										
		   									
	   									</div>
		  							</div>
	  								<div class="modal-footer">
	  									<button id="btnSavePartialPayment" name="btnSavePartialPayment" value="btnSavePartialPayment" type="button" class="btn btn-primary" onclick="FuncUpdateSetting()"><i class="fa fa-check"></i> Confirm</button>
	   									<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
	  								</div>
								</div>
							</div>
						</div>
				        
					<!-- dashboard atas -->
					<c:if test="${globalUserRole eq 'R002'}">
						<div class="col-md-12">
							<div class="row" style="background-color:white;">
			 				<c:forEach items="${listDashboardDetail}" var ="component">
				 				<div class="col-md-2  col-sm-4 col-xs-6" style="padding-right: 0px;padding-left: 0px;border-left: 2px solid #f7f3f3;border-top: 2px solid #f7f3f3;">
				 				<span class="info-box-text" style="padding: 5px 10px;background: white;color: #005292; font-weight: bold;"><c:out value="${component.boxTitle}" /></span>
						          <div class="info-box" title="<c:out value="${component.boxInfo}" />">
						            <span class="info-box-icon" style="margin-left: 10px;background-color: #005292;">
						            	<i><img style="margin-top: 10px;"
						            			width="30px" height="30px" 
						            			src="mainform/dist/img/dashboard_detail/<c:out value="${component.boxImage}" />">
						            	</i>
						            </span>
						
						            <div class="info-box-content">
						              
						              <span class="info-box-number" style="text-align: center;margin-top:10px; font-size: 20px;">
						              	<c:out value="${component.boxValue}" />
						              </span>
						            </div>
						            <!-- /.info-box-content -->
						          </div>
						          <!-- /.info-box -->
						        </div>
						        
						        
						        <!-- /.col -->
				 			</c:forEach>
				 			</div>
						</div>
					</c:if>
					
					<c:if test="${globalUserRole eq 'R003'}">
						<div class="col-md-12">
							<div class="row" style="background-color:white;">
			 				<c:forEach items="${listDashboardDetail}" var ="component">
			 					<c:if test="${component.boxTitle eq 'Processed Order'}">
			 						<div class="col-md-2 col-sm-4 col-xs-6" style="padding-right: 0px;padding-left: 0px;border-left: 2px solid #f7f3f3;border-top: 2px solid #f7f3f3;">
						 				<span class="info-box-text" style="padding: 5px 10px;background: white;color: #005292; font-weight: bold;"><c:out value="${component.boxTitle}" /></span>
								          <div class="info-box" title="<c:out value="${component.boxInfo}" />">
								            <span class="info-box-icon" style="margin-left: 10px;background-color: #005292;">
								            	<i><img style="margin-top: 10px;"
								            			width="30px" height="30px" 
								            			src="mainform/dist/img/dashboard_detail/<c:out value="${component.boxImage}" />">
								            	</i>
								            </span>
								
								            <div class="info-box-content">
								              
								              <span class="info-box-number" style="text-align: center;margin-top:10px; font-size: 20px;">
								              	<c:out value="${component.boxValue}" />
								              </span>
								            </div>
								            <!-- /.info-box-content -->
								          </div>
								          <!-- /.info-box -->
								        </div>
								    	<!-- /.col -->
								    	</c:if>
			 					
			 					<c:if test="${component.boxTitle ne 'Processed Order'}">
			 						<div class="col-half-offset col-md-2 col-sm-4 col-xs-6" style="padding-right: 0px;padding-left: 0px;border-left: 2px solid #f7f3f3;border-top: 2px solid #f7f3f3;">
						 				<span class="info-box-text" style="padding: 5px 10px;background: white;color: #005292; font-weight: bold;"><c:out value="${component.boxTitle}" /></span>
								          <div class="info-box" title="<c:out value="${component.boxInfo}" />">
								            <span class="info-box-icon" style="margin-left: 10px;background-color: #005292;">
								            	<i><img style="margin-top: 10px;"
								            			width="30px" height="30px" 
								            			src="mainform/dist/img/dashboard_detail/<c:out value="${component.boxImage}" />">
								            	</i>
								            </span>
								
								            <div class="info-box-content">
								              
								              <span class="info-box-number" style="text-align: center;margin-top:10px; font-size: 20px;">
								              	<c:out value="${component.boxValue}" />
								              </span>
								            </div>
								            <!-- /.info-box-content -->
								          </div>
								          <!-- /.info-box -->
								        </div>
								        
								        
								        <!-- /.col -->
			 					</c:if>
				 			</c:forEach>
				 			</div>
						</div>
					</c:if>
					<!-- akhir dari dashboard atas -->
					
					
					
        
        
					<!-- dashboard bawah -->
					<div class="row">
		 			<div class="col-md-12">
			  		<div class="box">
			  		<div class="box-header with-border">
			  			<div class="row">
			  				<div class="col-md-3">
			  					<h3 class="box-title" style="color:#9B9898;">SI number: </h3>
			  					<label><c:out value="${noSI}" /></label>
			  				</div>
			  				<div class="col-md-3">
			  					<h3 class="box-title" style="color:#9B9898;">Total order today: </h3>
			  					<label><c:out value="${partaiPerSIToday}" /> x <c:out value="${cttype}" /></label>
			  				</div>
			  				<c:if test="${globalUserRole eq 'R002'}">
				  				<div class="col-md-3">
				  					<h3 class="box-title" style="color:#9B9898;">Total order per SI: </h3>
				  					<label><c:out value="${totalOrderPerSI}" /></label>
				  				</div>
			  				</c:if>
			  			</div>
			  		</div>
			        <div class="box-body">
	        
	        			<c:if test="${globalUserRole eq 'R003'}">
	        				<c:set var="vendorOrder" value="${vendorOrder}"/>
	        				
	        				<a href="${pageContext.request.contextPath}/VendorOrderDetailList
								?vendorOrderID=<c:out value="${vendorOrder.vendorOrderID}" />
								&stuffingDate=<c:out value="${vendorOrder.orderStuffingDate}" />
								&orderType=<c:out value="${vendorOrder.orderType}" />
								&item=<c:out value="${vendorOrder.item}" />
								&customer=<c:out value="${vendorOrder.customer}" />
								&district=<c:out value="${vendorOrder.district}" />
								&depo=<c:out value="${vendorOrder.depo}" />
								&port=<c:out value="${vendorOrder.port}" />
								&orderQty=<c:out value="${vendorOrder.quantity}" />
								&order=<c:out value="${vendorOrder.orderManagementID}" />
								&orderDetail=<c:out value="${vendorOrder.orderManagementDetailID}" />
								&address=<c:out value="${vendorOrder.address}" />
								&noSI=<c:out value="${vendorOrder.shippingInvoiceID}" />
								&factory=<c:out value="${vendorOrder.factory}" />
								"
								>
								<button type="button" class="btn btn-primary pull-right"><i class="fa fa-edit"></i> Manage Order</button>
							</a>
			  				<br><br>
			  			</c:if>
			  				
						<!-- dashboard detail order -->
						<table id="tb_order_dashboard" class="table table-bordered table-hover">
						<thead style="background-color: #d2d6de;">
							<tr>
								<!-- <th>SI</th> -->
								<!-- <th>Type</th> -->
								<!-- <th>Item</th> -->
								<th>Date</th>
								<th>Order ID</th>
								<c:if test="${globalUserRole eq 'R002'}">
									<th>Factory</th>
									<th>Trucker</th>
								</c:if>
								<c:if test="${globalUserRole eq 'R003'}">
									<th>Customer</th>
									<th>Factory</th>
								</c:if>
								<th>Driver</th>
								<th>Vehicle No</th>
								<th>Status</th>
								<th>Container No</th>
								<th>Seal No</th>
								<th>Gate Pass</th>
								<c:if test="${globalUserRole eq 'R002'}">
									<th>EIR</th>
									<th>Container Status On Port</th>
									<th>Container Location On Vessel</th>
									<th>Stack On Vessel</th>
								</c:if>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listOrderDashboard}" var ="od">
								<tr>
									<%-- <td><c:out value="${od.shippingInvoiceID}" /></td> --%>
									<%-- <td><c:out value="${od.orderType}" /></td> --%>
									<%-- <td><c:out value="${od.itemDescription}" /></td> --%>
									<td><c:out value="${od.stuffingDate}" /></td>
									<td><c:out value="${od.driverOrderAssignmentID}" /></td>
									<c:if test="${globalUserRole eq 'R002'}">
										<td><c:out value="${od.factoryName}" /></td>
										<td><c:out value="${od.vendorName}" /></td>
									</c:if>
									<c:if test="${globalUserRole eq 'R003'}">
										<td><c:out value="${od.customerName}" /></td>
										<td><c:out value="${od.factoryName}" /></td>
									</c:if>
									<td><c:out value="${od.driverName}" /></td>
									<td>
										<c:if test="${od.isActiveMap}">
											<a href=""
											style="color: #144471; font-weight: bold; border-bottom: 1px dotted blue;"
											title="click to see the position on the map"
											data-toggle="modal" data-target="#ModalMap"
											data-lat='<c:out value="${od.latitude}" />'
											data-lng='<c:out value="${od.longitude}" />'
											> 
												<c:out value="${od.vehicleNumber}" />
											</a>
										</c:if>
										<c:if test="${not od.isActiveMap}">
											<c:out value="${od.vehicleNumber}" />
										</c:if>
									</td>
									<td><c:out value="${od.driverDeliveryStatus}" /></td>
									<td>
									<button class="btn label label-danger" type="button" id="btnInvoiceSetting" name="btnInvoiceSetting"
													data-target="#ModalInvoiceSetting"
														data-toggle="modal"
														data-limagepath='<c:out value="${od.jsonDriverImage}" />'
														>
														<c:out value="${od.containerNumber}"/> <i class="fa fa-cog" aria-hidden="true"></i>
													</button>
									
									<span class="label label-danger"></span>
									</td>
									<td><c:out value="${od.sealNumber}" /></td>
									<td>
										<c:if test="${od.yellowCard eq ''}">
											Not Issued
										</c:if>
										<c:if test="${od.yellowCard ne ''}">
											Issued
										</c:if>
									</td>
									<c:if test="${globalUserRole eq 'R002'}">
										<td>
											<c:if test="${not empty od.tpsEir}">
											<button
												type="button"  class="btn btn-info"
												title="Show EIR" onclick="showEir('<c:out value="${od.tpsEir}"/>'
									        								  )"
											>show</button>
											</c:if>
										</td>
										<td><c:out value="${od.tpsShpLocationID}" /></td>
										<td><c:out value="${od.tpsShpLocation}" /></td>
										<td><c:out value="${od.tpsShpMovementDate}" /></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						<!-- /.dashboard detail order -->
		          
				         <!--modal show map -->
						<div class="modal fade bs-example-modal-lg" id="ModalMap" tabindex="-1" role="dialog" aria-labelledby="ModalLabelMapID">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">						
							
									<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="ModalTitleMapID"><b>ORDER TRACKING</b></h4>	
									</div>
		   								<div class="modal-body">
		   									<div id="googleMap" style="width:100%;height:400px;">
											</div>
		  								</div>
								</div>
							</div>
						</div>
						<!-- /. end of modal show map -->
				
			          </div>
			       	  <!-- /.box-body -->
			 		</div>
			 		<!-- /.box -->
			 		</div>
			       <!-- /.col -->
			       		      
		          
		          </div>
				<!-- /.row -->
				<!-- /. akhir dari dashboard bawah -->
			
	    </section>
	    <!-- /.content -->
	    
  </div>
  <!-- /.content-wrapper -->

  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="mainform/dist/js/pages/dashboard2.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
 	$(function () {
  		$('#M001').addClass('active');
  		$("#tb_order_dashboard").DataTable({
  			"aaSorting": [],
			"scrollX": true
			});
  	});
  	
  	//function map
  	function myMap(lParamLat, lParamLng) {
  		//set markers
		var markerData = [
							<c:forEach items="${listOrderDashboard}" var="marker">
						       {
						       	 "lat":<c:out value="${marker.latitude}" />,
						         "lng":<c:out value="${marker.longitude}" />,
						         "trackingDate":'<c:out value="${marker.trackingDate}" />',
						         "vehicleNumber":'<c:out value="${marker.vehicleNumber}" />',
						         "vendorName":'<c:out value="${marker.vendorName}" />',
						         "driverName":'<c:out value="${marker.driverName}" />',
						         "driverStatus":'<c:out value="${marker.driverDeliveryStatus}" />',
						         "shippingInvoiceID":'<c:out value="${marker.shippingInvoiceID}" />',
						         "deliveryOrderID":'<c:out value="${marker.deliveryOrderID}" />',
						         "factoryName":'<c:out value="${marker.factoryName}" />',
						         "itemDescription":'<c:out value="${marker.itemDescription}" />',
						         "portName":'<c:out value="${marker.portName}" />',
						         "depoName":'<c:out value="${marker.depoName}" />',
						         "orderType":'<c:out value="${marker.orderType}" />'
						       },
							</c:forEach>
					     ];
	
		//set first map loading
		var centerLat = -6.149289, centerLng = 106.897870;
		var listMarker = '<c:out value="${fn:length(listOrderDashboard)}" />';

		if(listMarker != '0'){
			//centerLat = markerData[0].lat; centerLng = markerData[0].lng;
			centerLat = lParamLat; centerLng = lParamLng;
		}
			
		var mapProp= {
		    center:new google.maps.LatLng(centerLat, centerLng),
		    zoom:17,
		};
		
		//show map to page
		var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
		
		//push marker
            for (i = 0; i < markerData.length; i++) {
                var data = markerData[i];
                var start = new google.maps.LatLng(data.lat, data.lng);
                if(data.vendorName != ""){
                	var contentString = 
		                			'<div id="content">' +
			                            '<div id="siteNotice">' +
			                            '</div>' +
			                            '<h3 id="firstHeading" class="firstHeading">' + data.driverName + '(' + data.vehicleNumber + ')' + '</h3>' +
			                            '<h5 id="secondHeading class="secondHeading">Trucker : ' + data.vendorName + ' || ' +
			                            											'SI : ' + data.shippingInvoiceID  + ' || ' +
			                            											'DO : ' + data.deliveryOrderID  +
			                            '</h5>' +
			                            '<h5 id="secondHeading class="secondHeading">Date : ' + data.trackingDate  + ' || ' +
	                            													'Status : ' + data.driverStatus  + ' || ' +
	                            													'Type : ' + data.orderType  +
			                            '</h5>' +
			                            '<h5 id="secondHeading class="secondHeading">Depo : ' + data.depoName  + ' || ' +
	                            													'Factory : ' + data.factoryName  + ' || ' +
	                            													'Port : ' + data.portName  + ' || ' +
	                            													'Item : ' + data.itemDescription  +
			                            '</h5>' +
		                            '</div>';
                }else{
                	var contentString = 
		                			'<div id="content">' +
			                            '<div id="siteNotice">' +
			                            '</div>' +
			                            '<h3 id="firstHeading" class="firstHeading">' + data.driverName + '(' + data.vehicleNumber + ')' + '</h3>' +
			                            '<h5 id="secondHeading class="secondHeading">SI : ' + data.shippingInvoiceID  + ' || ' +
			                            											'DO : ' + data.deliveryOrderID  + ' || ' +
			                            											'Date : ' + data.trackingDate +
			                            '</h5>' +
			                            '<h5 id="secondHeading class="secondHeading">Status : ' + data.driverStatus  + ' || ' +
	                            													'Type : ' + data.orderType  + ' || ' +
	                            													'Item : ' + data.itemDescription  +
			                            '</h5>' +
			                            '<h5 id="secondHeading class="secondHeading">Depo : ' + data.depoName  + ' || ' +
	                            													'Factory : ' + data.factoryName  + ' || ' +
	                            													'Port : ' + data.portName +
			                            '</h5>' +
		                            '</div>';
                }

                 var infowindow = new google.maps.InfoWindow();
                 var currentInfoWindow = null; 
				
				var image = { url: 'mainform/image/delivery-truck.png',
						    // This marker is 20 pixels wide by 32 pixels high.
						    scaledSize: new google.maps.Size(50, 50),
						    // The origin for this image is (0, 0).
						    origin: new google.maps.Point(0, 0),
						    // The anchor for this image is the base of the flagpole at (0, 32).
						    anchor: new google.maps.Point(0, 32)
						    }
						    
		        var marker = new google.maps.Marker({
		          position: start,
		          map: map,
		          title: 'Driver : '+data.driverName,
		          icon: image
		        });
		        
		        google.maps.event.addListener(marker,'click', (function(marker,contentString,infowindow){
				    return function() {
				    	if (currentInfoWindow != null) { 
				        	currentInfoWindow.close(); 
				    	}
				        infowindow.setContent(contentString);
				        infowindow.open(map,marker);
				        currentInfoWindow = infowindow; 
				    };
				})(marker,contentString,infowindow));
            }
	}
</script>

<!-- map script -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAonooVGMb9GaKOCBnt7wUz0bdA8xMVztc&callback=myMap"></script>

<script>
	
$('#ModalMap').on('shown.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var lat = button.data('lat');
	var lng = button.data('lng');
	
	myMap(lat,lng);
})


$('#ModalInvoiceSetting').on('shown.bs.modal', function (event) {
	var modal = $(this);
	var pathImage = $(event.relatedTarget).data("limagepath")
	console.log(pathImage)
	
	var url = window.location.href
	var arr = url.split("/");
	var host = arr[0] + "//" + arr[2]
	
	var html = "";
	var no = 1;
	var host = ""
	for ( var item in pathImage) {
		html += '<tr>'+
	  				'<td>'+no+'</td>'+
	  				'<td>'+pathImage[item].type+'</td>'+
	  				'<td><a href="'+host+'/'+pathImage[item].pathImage+'" target="_blank"><img src="'+host+'/'+pathImage[item].pathImage+'" alt="'+pathImage[item].type+'" height="42" width="42"></a></td>'+
		  		'</tr>';
		  		
		no++;
		
	}
	
	$("#tbody-driver-image").html(html)
});

function showEir(param) {
	     window.open(param, "_blank");
	}
</script>

</body>
</html>
