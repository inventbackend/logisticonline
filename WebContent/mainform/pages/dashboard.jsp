<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/logistic_icon3.jpg">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistic Online | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
<!-- DataTables -->
  <link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style type="text/css">	
	#ModalUpdateDocumentation { overflow-y:scroll }
	
	/*  css for loading bar */
	 .loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  width: 60px;
	  height: 60px;
	  -webkit-animation: spin 2s linear infinite; /* Safari */
	  animation: spin 2s linear infinite;
	  margin-left: 42%;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}
	
	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	/* end of css loading bar */
	
	.col-half-offset{
	  margin-left:4.166666667%
	}
	
	tfoot tr {
	background: lightblue;
	}
	tfoot td {
		font-weight:bold;
	}
  </style>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="Form_Dashboard" name="Form_Dashboard" action = "Dashboard" method="post">

<div class="wrapper">  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
  	  <c:if test="${globalUserRole eq 'R002' || globalUserRole eq 'R003'}">
  	  	 <!-- Content Header (Page header) -->
	    <section class="content-header">
		<!-- 	      <h1> -->
		<!-- 	        Dashboard -->
		<!-- 	        <small>Control panel</small> -->
		<!-- 	      </h1> -->
			<div class="user-panel">
			 	<div class="pull-left image">
	          		<img src="mainform/dist/img/Group 4260@2x.png" alt="User Image">
	        	</div>
	        	<div class="pull-left info">
	          		<p style="color:#4E4E4E"><c:out value="${dateNow}"/></p>
	          		<p style="color:#4E4E4E"><c:out value="${timeNow}"/></p>
	        	</div>
	        </div>
	    </section>
  	  </c:if>

    	<!-- Main content -->
	    <section class="content">
	    
	      	<!-- Info boxes -->
			
				
				<c:if test="${globalUserRole eq 'R001'}">
					<div class="row">
						<!-- dashboard welcome -->
						<div class="col-md-12">
							<div class="box">
				        	<div class="box-body">
				        		<div style="margin-left:31.2vw;margin-top:35vh;">
							 		<label for="user" class="control-label" style="font-size:30px;color:#003156">WELCOME</label>
							 	</div>
							 	<div style="margin-left:31.2vw;margin-top:65vh;">
							 		<label for="user" class="control-label" style="font-size:0px;color:#003156">WELCOME</label>
							 	</div>
						 	</div>
						 	</div>
						 </div>
						 <!-- /.col-md-12 -->
					 </div>
				 </c:if>
		 
		 		<c:if test="${globalUserRole eq 'R002' || globalUserRole eq 'R003'}">
		 			<div class="row">
						<!-- dashboard atas -->
			 			<c:forEach items="${listDashboard1}" var ="component">
			 			
			 				<c:if test="${component.boxTitle == 'GATEPASS RELEASED'}">
			 					<div class="col-md-2 col-half-offset col-sm-6 col-xs-12">
						          <div class="box box-warning" style="border-top-color:#FDF7AD;background-color:#FDF7AD;">
						            <div class="box-header with-border">
						              <h3 class="box-title" style="color:#948B12; font-weight: bold;"><c:out value="${component.boxTitle}" /></h3>
						
						              <div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						              </div>
						              <!-- /.box-tools -->
						            </div>
						            <!-- /.box-header -->
						            <div class="box-body" style="font-weight: bold; font-size:20px; text-align: center; color:#6C6118;">
						              <c:out value="${component.boxValue}" />
						            </div>
						            <!-- /.box-body -->
						          </div>
						          <!-- /.box -->
						        </div>
						        <!-- /.col -->
			 				</c:if>
			 				
			 				<c:if test="${component.boxTitle ne 'GATEPASS RELEASED' && (component.boxTitle eq 'Total Order')}">
			 					<div class="col-md-2 col-sm-6 col-xs-12">
						          <div class="box box-info box-solid" title="<c:out value="${component.boxInfo}" />">
						            <div class="box-header with-border">
						              <h3 class="box-title"><c:out value="${component.boxTitle}" /></h3>
						
						              <div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						              </div>
						              <!-- /.box-tools -->
						            </div>
						            <!-- /.box-header -->
						            <div class="box-body" style="font-weight: bold; font-size:20px; text-align: center;">
						              <c:out value="${component.boxValue}" />
						            </div>
						            <!-- /.box-body -->
						          </div>
						          <!-- /.box -->
					        	</div>
					        	<!-- /.col -->
			 				</c:if>
			 				
			 				<c:if test="${component.boxTitle ne 'GATEPASS RELEASED' && (component.boxTitle ne 'Total Order' && component.boxTitle ne 'Total Delivered')}">
			 					<div class="col-md-2 col-half-offset col-sm-6 col-xs-12">
						          <div class="box box-warning box-solid" title="<c:out value="${component.boxInfo}" />">
						            <div class="box-header with-border">
						              <h3 class="box-title"><c:out value="${component.boxTitle}" /></h3>
						
						              <div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						              </div>
						              <!-- /.box-tools -->
						            </div>
						            <!-- /.box-header -->
						            <div class="box-body" style="font-weight: bold; font-size:20px; text-align: center;">
						              <c:out value="${component.boxValue}" />
						            </div>
						            <!-- /.box-body -->
						          </div>
						          <!-- /.box -->
					        	</div>
					        	<!-- /.col -->
			 				</c:if>
			 				
			 				<c:if test="${component.boxTitle ne 'GATEPASS RELEASED' && component.boxTitle eq 'Total Delivered'}">
			 					<div class="col-md-2 col-half-offset col-sm-6 col-xs-12">
						          <div class="box box-info box-solid" title="<c:out value="${component.boxInfo}" />">
						            <div class="box-header with-border">
						              <h3 class="box-title"><c:out value="${component.boxTitle}" /></h3>
						
						              <div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						              </div>
						              <!-- /.box-tools -->
						            </div>
						            <!-- /.box-header -->
						            <div class="box-body" style="font-weight: bold; font-size:20px; text-align: center;">
						              <c:out value="${component.boxValue}" />
						            </div>
						            <!-- /.box-body -->
						          </div>
						          <!-- /.box -->
					        	</div>
					        	<!-- /.col -->
			 				</c:if>
			 			</c:forEach>
					</div>
					<!-- akhir dari dashboard atas -->
					
        
        
					<!-- dashboard bawah -->
					<div class="row">
		 			<div class="col-md-12 col-sm-6 col-xs-12">
			  		<div class="box">
			  		<div class="box-header with-border">
			  			<h3 class="box-title">Tracking Order</h3>
			  		</div>
			        <div class="box-body">
	        
						<!-- dashboard tracking order -->
						<div class="col-md-12">
						<table id="tb_order_dashboard" class="table table-bordered table-hover">
							<thead style="background-color: #d2d6de;">
								<tr>
									<th>SI Number</th>
									<th>Party</th>
									<c:if test="${globalUserRole eq 'R002'}">
										<th>Unprocessed</th>
										<th>Processed</th>
										<th>Delivered</th>	
									</c:if>
									<c:if test="${globalUserRole eq 'R003'}">
										<th>Order Status</th>
									</c:if>
									<th></th>
								</tr>
							</thead>
							<c:if test="${globalUserRole eq 'R002'}">
								<tfoot>
									<tr>
										<td>Totals</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tfoot>
							</c:if>
							<tbody>
								<c:forEach items="${listDashboard2}" var ="od">
									<tr>
										<td><c:out value="${od.shippingInvoiceID}" /></td>
										<td><c:out value="${od.partaiPerSI}" /> x <c:out value="${od.containerType}" /></td>
										<c:if test="${globalUserRole eq 'R002'}">
											<td><c:out value="${od.partaiUnprocessPerSI}" /></td>
											<td><c:out value="${od.partaiProcessPerSI}" /></td>
											<td><c:out value="${od.partaiSelesaiPerSI}" /></td>
										</c:if>
										<c:if test="${globalUserRole eq 'R003'}">
											<td><c:out value="${od.partaiSelesaiPerSI}" /> order completed</td>
										</c:if>
										<td><a href="${pageContext.request.contextPath}/DashboardDetail?noSI=<c:out value="${od.shippingInvoiceID}" />
																						&ppsiToday=<c:out value="${od.partaiPerSI}" />
																						&voID=<c:out value="${od.vendorOrderID}" />
																						&cttype=<c:out value="${od.containerType}" />">
											<button type="button" id="btnDashboardDetail" name="btnDashboardDetail" class="btn btn-primary">
												See Detail
											</button>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
							</table>
						</div>
						<!-- /.dashboard tracking order -->				
				
			          </div>
			       	  <!-- /.box-body -->
			 		</div>
			 		<!-- /.box -->
			 		</div>
			       <!-- /.col --> 
			       
			       
			       
			       <!-- dashboard pending -->
			       <c:if test="${globalUserRole eq 'R002'}">
		       		<div class="col-md-12 col-sm-6 col-xs-12">
			  		<div class="box">
			  		<div class="box-header with-border">
			  			<h3 class="box-title">Pending Order</h3>
			  		</div>
			        <div class="box-body">
	        
						<!-- dashboard pending order -->
						<div class="col-md-12">
						<table id="tb_order_pending" class="table table-bordered table-hover">
							<thead style="background-color: #d2d6de;">
								<tr>
									<th>SI Number</th>
									<th>Party</th>
									<th>Unprocessed</th>
									<th>Processed</th>
									<th>Delivered</th>
									<th></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td>Totals</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tfoot>
							<tbody>
								<c:forEach items="${listDashboardPending}" var ="dp">
									<tr>
										<td><c:out value="${dp.shippingInvoiceID}" /></td>
										<td><c:out value="${dp.partaiPerSI}" /> x <c:out value="${dp.containerType}" /></td>
										<td><c:out value="${dp.partaiUnprocessPerSI}" /></td>
										<td><c:out value="${dp.partaiProcessPerSI}" /></td>
										<td><c:out value="${dp.partaiSelesaiPerSI}" /></td>
										<td><a href="${pageContext.request.contextPath}/DashboardPendingDetail?noSI=<c:out value="${dp.shippingInvoiceID}" />
																						&ppsiToday=<c:out value="${dp.partaiPerSI}" />
																						&voID=<c:out value="${dp.vendorOrderID}" />
																						&cttype=<c:out value="${dp.containerType}" />">
											<button type="button" id="btnDashboardPendingDetail" name="btnDashboardPendingDetail" class="btn btn-primary">
												See Detail
											</button>
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
							</table>
						</div>
						<!-- /.dashboard pending order -->				
				
			          </div>
			       	  <!-- /.box-body -->
			 		</div>
			 		<!-- /.box -->
			 		</div>
			       <!-- /.col --> 
			       </c:if>
			       
			       
			       
			       <!-- dashboard berita hari ini -->
			       	<div class="col-md-4 col-sm-6 col-xs-12" style="display:none;">
			  	   	<div class="box">
			  	   	<div class="box-header with-border">
	          			<img src="mainform/dist/img/Group 4323@2x.png" style="width:20px;height:20px;">	        
			  			<h3 class="box-title">Today News</h3>
			  	   	</div>
			        <div class="box-body">
				        
						  <div class="col-md-12">
				          </div>
						  <!-- /.col -->
			        </div>
			       	  <!-- /.box-body -->
			 		</div>
			 		<!-- /.box -->
			 		</div>
			       <!-- /.col -->
			       <!-- /. akhir dari dashboard berita hari ini -->
			       		      
		          
		          </div>
				<!-- /.row -->
				<!-- /. akhir dari dashboard bawah -->
	          </c:if>
			
	    </section>
	    <!-- /.content -->
	    
  </div>
  <!-- /.content-wrapper -->

  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="mainform/dist/js/pages/dashboard2.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
 	$(function () {
  		$('#M001').addClass('active');
  		$('#dvloader').hide();
  		$("#tb_order_dashboard").DataTable({
  			"aaSorting": [],
			"scrollX": true,
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api();
				nb_cols = api.columns().nodes().length;
				var j = 1;
				while(j < nb_cols){
					var pageTotal = api
                .column( j, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return Number(a) + Number(b);
                }, 0 );
	          // Update footer
	          $( api.column( j ).footer() ).html(pageTotal);
						j++;
					} 
				}
			});
			
		$("#tb_order_pending").DataTable({
  			"aaSorting": [],
			"scrollX": true,
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api();
				nb_cols = api.columns().nodes().length;
				var j = 1;
				while(j < nb_cols){
					var pageTotal = api
                .column( j, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return Number(a) + Number(b);
                }, 0 );
	          // Update footer
	          $( api.column( j ).footer() ).html(pageTotal);
						j++;
					} 
				}
			});
  	});
</script>

</body>
</html>
