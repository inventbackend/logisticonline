<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_master_staff" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Staff</th>
			<th>Username</th>
			<th style="width: 120px"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listStaff}" var ="staff">
			<tr>
				<td><c:out value="${staff.staff}" /></td>
				<td><c:out value="${staff.username}" /></td>				
				<td>
					<button
							type="button" class="btn btn-info"
							title="edit"
							data-toggle="modal"
							data-target="#ModalStaff"							
							data-lstaff='<c:out value="${staff.staff}"/>'
							data-lusername='<c:out value="${staff.username}"/>'
							data-lstatus='<c:out value="${staff.isActive}"/>'
							data-lpassword='<c:out value="${staff.password}"/>'
							><i class="fa fa-edit"></i></button>
			        <button type="button" class="btn btn-danger" data-toggle="modal" 
			        		title="delete"
			        		data-target="#ModalDeleteStaff"
			        		data-lstaff='<c:out value="${staff.staff}"/>'
			        		>
			        		<i class="fa fa-trash"></i>
			        </button>
			        <button type="button" class="btn btn-warning" data-toggle="modal"
			        		title="role" 
			        		data-target="#ModalRoleStaff" 
			        		data-lroleid='<c:out value="${staff.userId}"/>_<c:out value="${staff.staff}"/>'
			        		>
			        		<i class="fa fa-key"></i>
			        </button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_master_staff").DataTable({"aaSorting": [],"scrollX": true});
  	});
</script>