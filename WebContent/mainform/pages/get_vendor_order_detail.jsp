<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_vendor" class="table table-bordered table-hover">
	<thead style="background-color: #d2d6de;">
		<tr>
			<th>Vendor</th>
			<th>Driver</th>
			<th>Vehicle Number</th>
			<th>Status</th>
			<th>Container Number</th>
			<th>Seal Number</th>
			<th>EIR</th>
			<th>Container Status On Port</th>
			<th>Container Location On Vessel</th>
			<th>Stack On Vessel</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listData}" var ="data">
			<tr>
				<td><c:out value="${data.vendorName}" /></td>
				<td><c:out value="${data.driverName}" /></td>
				<td><c:out value="${data.vehicleNumber}" /></td>
				<td>
					<c:if test="${data.vendorDetailStatus == 0}">Assigned</c:if>
					<c:if test="${data.vendorDetailStatus == 1}">On Process</c:if>
					<c:if test="${data.vendorDetailStatus == 6}">On Depo</c:if>
					<c:if test="${data.vendorDetailStatus == 7}">Depo Checkout</c:if>
					<c:if test="${data.vendorDetailStatus == 5}">On Factory</c:if>
					<c:if test="${data.vendorDetailStatus == 2}">On Delivery</c:if>
					<c:if test="${data.vendorDetailStatus == 3}">On Port</c:if>
					<c:if test="${data.vendorDetailStatus == 4}">Delivered</c:if>
					<c:if test="${data.vendorDetailStatus == 10}">Cancelled</c:if>
				</td>
				<td><c:out value="${data.containerNumber}" /></td>
				<td><c:out value="${data.sealNumber}" /></td>
				<td>
					<c:if test="${not empty data.tpsEir}">
					<button
						type="button"  class="btn btn-info"
						title="Show EIR" onclick="showEir('<c:out value="${data.tpsEir}"/>'
			        								  )"
					>show</button>
					</c:if>
				</td>
				<td><c:out value="${data.tpsShpLocationID}" /></td>
				<td><c:out value="${data.tpsShpLocation}" /></td>
				<td><c:out value="${data.tpsShpMovementDate}" /></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
 	$(function () {
  		$("#tb_vendor").DataTable({
  			"aaSorting": [],
			"scrollX": true
  		});
  	});
  	
  	function showEir(param) {
	     window.open(param, "_blank");
	}
</script>